﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Security.Cryptography;
using Jaeger.Aplication.Base.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Banco.Builder;
using Jaeger.Aplication.Empresa.Contracts;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Builder;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.Repositories;
using MiniExcelLibs.OpenXml;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// 
    /// </summary>
    public class BancoService : BancoCuentaService, IBancoService, IBancoCuentaService {
        #region declaraciones
        protected ISqlMovimientoBancarioRepository movimientoBancarioRepository;
        protected ISqlBeneficiarioRepository beneficiarioRepository;
        protected ISqlMovimientoConceptoRepository conceptoRepository;
        protected ISqlBancoFormaPagoRepository formaPagoRepository;
        protected ISqlMovimientoAuxiliarRepository auxiliarRepository;
        protected IEditaBucketService s3;
        protected IConfigurationService _Empresa;
        #endregion

        public BancoService() : base() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this._Empresa = new Aplication.Empresa.Service.ConfigurationService();
            var parametros = this._Empresa.Get(Domain.Empresa.Enums.ConfigGroupEnum.Bancos);
            this.Configuration = Banco.BancoService.ConfigurationBuilder().Build(parametros);
            this.movimientoBancarioRepository = new SqlSugarMovimientoBancarioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.beneficiarioRepository = new SqlSugarBeneficiarioRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.conceptoRepository = new SqlSugarMovimientoConceptoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.formaPagoRepository = new SqlSugarBancoFormaPagoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.auxiliarRepository = new SqlSugarMovimientoAuxiliarRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService {
                Folder = "banco"
            };
        }

        public IConfiguration Configuration { get; set; }

        /// <summary>
        /// almacenar movmiento bancario
        /// </summary>
        public IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model) {
            if (model.Id == 0) {
                model.Creo = ConfigService.Piloto.Clave;
                model.FechaNuevo = DateTime.Now;
            } else {
                model.Modifica = ConfigService.Piloto.Clave;
                model.FechaModifica = DateTime.Now;
            }

            for (int i = 0; i < model.Comprobantes.Count; i++) {
                if (model.Comprobantes[i].Id == 0)
                    model.Comprobantes[i].Creo = model.Creo;
                else
                    model.Comprobantes[i].Creo = ConfigService.Piloto.Clave;
            }

            for (int i = 0; i < model.Auxiliar.Count; i++) {
                if (model.Auxiliar[i].IdAuxiliar == 0) {
                    model.Auxiliar[i].Tipo = this.s3.ContenType(model.Auxiliar[i].FileName);
                    model.Auxiliar[i].Creo = ConfigService.Piloto.Clave;
                }
            }

            // primero almacenamos el movimiento
            model = this.movimientoBancarioRepository.Save(model);

            // y despues si existen auxiliares los almacenamos con los datos necesarios
            for (int i = 0; i < model.Auxiliar.Count; i++) {
                if (model.Auxiliar[i].IdAuxiliar > 0) {
                    model.Auxiliar[i].URL = this.s3.Upload(model.Auxiliar[i].Base64, model.Auxiliar[i].FileName, string.Format("{0}-{1}{2}", model.Identificador,
                        this.CreateGuid(new string[] { model.Auxiliar[i].Base64 }), System.IO.Path.GetExtension(model.Auxiliar[i].FileName)).ToLower());
                    if (ValidacionService.URL(model.Auxiliar[i].URL)) {
                        model.Auxiliar[i].Tipo = this.s3.ContenType(model.Auxiliar[i].FileName);
                        model.Auxiliar[i].Creo = ConfigService.Piloto.Clave;
                        this.auxiliarRepository.Update(model.Auxiliar[i]);
                    }
                }
            }

            if (model.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                if (model.Id > 0) {
                    //var model2 = this.TransferenciaEntreCuentas(model);
                    var model2 = BancoMovimientoBuilder.TransferenciaEntreCuentas(model);
                    model2 = this.movimientoBancarioRepository.Save(model2);
                }
            }
            return model;
        }

        public IMovimientoBancarioAuxiliarDetailModel Save(IMovimientoBancarioAuxiliarDetailModel auxiliar) {

            auxiliar.Tipo = this.s3.ContenType(auxiliar.FileName);
            auxiliar.URL = this.s3.Upload(auxiliar.Base64, auxiliar.FileName, string.Format("{0}-{1}{2}", auxiliar.Identificador, this.CreateGuid(new string[] { auxiliar.Base64 }), System.IO.Path.GetExtension(auxiliar.FileName)).ToLower());

            if (ValidacionService.URL(auxiliar.URL)) {
                auxiliar.Tipo = this.s3.ContenType(auxiliar.FileName);
                auxiliar.Creo = ConfigService.Piloto.Clave;
                this.auxiliarRepository.Save((MovimientoBancarioAuxiliarDetailModel)auxiliar);
            }

            return auxiliar;
        }

        public List<MovimientoBancarioAuxiliarDetailModel> Save(List<MovimientoBancarioAuxiliarDetailModel> auxiliar, string identificador) {
            for (int i = 0; i < auxiliar.Count; i++) {
                auxiliar[i] = this.Save(auxiliar[i] as IMovimientoBancarioAuxiliarDetailModel) as MovimientoBancarioAuxiliarDetailModel;
            }
            return auxiliar;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(MovimientoConceptoDetailModel)) {
                return this.conceptoRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(BancoFormaPagoModel)) {
                return this.formaPagoRepository.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(BancoCuentaDetailModel)) {
                return this.cuentaBancariaRepository.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(MovimientoBancarioAuxiliarDetailModel)) {
                this.auxiliarRepository.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(MovimientoBancarioComprobanteDetailModel)) {
                // la busqueda se hace en la tabla de CFDI
                // return this.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(BancoFormatoImpresoModel)) {
                //return GetRepresentacionImpresa().ToList<T1>();
            }
            return this.movimientoBancarioRepository.GetList<T1>(conditionals);
        }

        public virtual string Test(BindingList<IMovimientoBancarioDetailModel> dataSource) {
            var layout = new StringBuilder();
            // del conjunto de recibos seleccionamos todos los que sean ingresos y obtenemos la lista de UUID's
            var documentos = dataSource.SelectMany(it => it.Comprobantes).Where(it => it.TipoComprobante == TipoCFDIEnum.Ingreso).Select(it => it.IdDocumento).ToArray();
            // buscar los comprobantes relacionados que coinciden con la lista de comprobantes
            var query = new List<IConditional> {
                new Conditional("_cmppgd_uuid", string.Join(",", documentos), Domain.Base.ValueObjects.ConditionalTypeEnum.In)
            };
            // resultado de la busqueda
            var complementoDePago = this.GetList<MovimientoBancarioComprobantePago>(query).ToList();

            if (complementoDePago != null) {
                if (complementoDePago.Count > 0) {
                    // recorremos todos los recibos
                    for (int iRecibo = 0; iRecibo < dataSource.Count; iRecibo++) {
                        // que no este cancelado
                        if (dataSource[iRecibo].Status != MovimientoBancarioStatusEnum.Cancelado) {
                            // recorrer los documentos relacionados al recibo
                            for (int iDocumento = 0; iDocumento < dataSource[iRecibo].Comprobantes.Count; iDocumento++) {
                                if (dataSource[iRecibo].Comprobantes[iDocumento].TipoComprobante == TipoCFDIEnum.Ingreso) {
                                    // buscar el comprobante que conincida con el uuid y el importe pagado
                                    var isExists = complementoDePago
                                        .Where(it => it.IdDocumentoR == dataSource[iRecibo].Comprobantes[iDocumento].IdDocumento)
                                        .Where(it => it.ImpPago == dataSource[iRecibo].Comprobantes[iDocumento].Abono).ToList();
                                    layout.Append("Identificador --- " + dataSource[iRecibo].Identificador + "\r\n");
                                    if (isExists != null) {
                                        if (isExists.Count() == 1) {
                                            layout.Append("     ├── CI - " + dataSource[iRecibo].Comprobantes[iDocumento].GetOriginalString + "\r\n");
                                            // si el resultado es uno, entonces coincidir con el importe del abono en el caso de un pago a proveedor
                                            if (isExists.FirstOrDefault().ImpPago == dataSource[iRecibo].Comprobantes[iDocumento].Abono) {
                                                // usamos la funcion agregar para evitar duplicados
                                                if (dataSource[iRecibo].Agregar(isExists.FirstOrDefault()) == false) {
                                                    layout.Append("     └── CP: | " + isExists.FirstOrDefault().GetOriginalString + " (Agregado)\r\n");
                                                } else {
                                                    layout.Append("     └── CP: | " + isExists.FirstOrDefault().GetOriginalString + " (Duplicado)\r\n");
                                                }
                                            }
                                        } else if (isExists.Count() == 0) {
                                            layout.Append("     └── CI: | " + dataSource[iRecibo].Comprobantes[iDocumento].GetOriginalString + " (No exite) \r\n");
                                        } else {
                                            layout.Append("     └── CI: | " + dataSource[iRecibo].Comprobantes[iDocumento].GetOriginalString + " (Existe mas de un comprobante que coincide)\r\n");
                                            foreach (var item in isExists) {
                                                layout.Append("         ├── CP: " + item.GetOriginalString + "\r\n");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return layout.ToString();
        }

        /// <summary>
        /// obtener listado de formas de pago
        /// </summary>
        public List<IBancoFormaPagoModel> GetBancoFormaPago(bool onlyActive) {
            return this.GetList<BancoFormaPagoModel>(BancoFormaPagoService.Query().OnlyActive().Build()).ToList<IBancoFormaPagoModel>();
        }

        /// <summary>
        /// obtener listado de contribuyentes con la lista de cuentas bancarias asociadas
        /// </summary>
        /// <param name="relationType">tipo de relacion con el directorio</param>
        /// <returns>lista de objetos ContribuyenteCtaBancoDetailModel</returns>
        public virtual BindingList<BeneficiarioDetailModel> GetBeneficiarios(BindingList<ItemSelectedModel> relationType, string search = "") {
            var _dedos = relationType.Select(it => it.Name).ToList();
            return new BindingList<BeneficiarioDetailModel>(this.beneficiarioRepository.GetBeneficiarios(_dedos, true, search).ToList());
        }

        public bool Aplicar(IMovimientoBancarioDetailModel model) {
            if (model != null) {
                model.FechaModifica = DateTime.Now;
                model.Modifica = ConfigService.Piloto.Clave;
                if (model.Status == MovimientoBancarioStatusEnum.Cancelado) {
                    model.FechaCancela = DateTime.Now;
                    model.Cancela = ConfigService.Piloto.Clave;
                }
                return this.movimientoBancarioRepository.Aplicar(model);
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        private string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            Guid hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

        #region layout banamex
        //public string LayoutBanamex(MovimientoBancarioDetailModel movimiento, string descripcion, bool clabe) {
        //    var cuentaOrigen = this.GetCuenta(movimiento.IdCuentaP);
        //    var beneficiario = this.beneficiarioRepository.GetBeneficiario(movimiento.IdDirectorio);
        //    var cuentaDestino = beneficiario.CuentasBancarias.Where(it => it.IdCuenta == movimiento.IdCuentaT).Single();

        //    if (cuentaOrigen != null && cuentaDestino != null) {
        //        var layout = new Scrapper.Banco.Banamex.LayoutC();
        //        layout.Control.TipoRegistro = 1;
        //        layout.Control.NumeroIdentificacion = DbConvert.ConvertInt32(cuentaOrigen.NumCliente);
        //        layout.Control.FechaPago = DateTime.Now;
        //        layout.Control.SecuenciaArchivo = 1;
        //        layout.Control.NombreEmpresa = cuentaOrigen.Beneficiario;
        //        layout.Control.Descripcion = descripcion;
        //        layout.Control.NaturalezaArchivo = 12;
        //        layout.Control.Instrucciones = "";
        //        layout.Control.Caracteristica = 0;
        //        // registro global de importacion
        //        layout.Global.TipoRegistro = 2;
        //        layout.Global.TipoOperacion = 1;
        //        layout.Global.TipoCuenta = 1;
        //        layout.Global.NumeroSucursal = DbConvert.ConvertInt32(cuentaOrigen.Sucursal);
        //        layout.Global.NumeroCuenta = cuentaOrigen.NumCuenta;
        //        layout.Global.Importe = movimiento.Cargo;

        //        var nuevo = new Scrapper.Banco.Banamex.RegistroCargoOAbono {
        //            TipoRegistro = 3,
        //            TipoOperacion = 0,
        //            ClaveMoneda = 1,
        //            Importe = double.Parse(movimiento.Cargo.ToString()),
        //            TipoCuenta = 1
        //        };

        //        if (clabe == true) {
        //            nuevo.NumeroCuenta = cuentaDestino.Clabe;
        //        } else {
        //            if (cuentaDestino.NumeroDeCuenta == null) {
        //                nuevo.NumeroCuenta = cuentaDestino.Clabe;
        //            } else {
        //                nuevo.NumeroCuenta = cuentaDestino.NumeroDeCuenta;
        //            }
        //        }

        //        if (nuevo.ReferenciaAlfanumerica != null) {
        //            if (nuevo.ReferenciaAlfanumerica.Trim().Length > 0) {
        //                nuevo.ReferenciaAlfanumerica = movimiento.Referencia;
        //            }
        //        }
        //        if (nuevo.ReferenciaAlfanumerica.Trim().Length == 0) {
        //            nuevo.ReferenciaAlfanumerica = "PAGO FAC ";
        //        }

        //        if (movimiento.ReferenciaNumerica != null) {
        //            if (movimiento.ReferenciaNumerica.Trim().Length > 0) {
        //                nuevo.ReferenciaNumerica = cuentaDestino.RefNumerica;
        //            }
        //        }
        //        if (nuevo.ReferenciaNumerica.Length == 0) {
        //            nuevo.ReferenciaNumerica = DateTime.Now.ToString("ddMMyy");
        //        }

        //        nuevo.ClaveBanco = cuentaDestino.Clave;

        //        nuevo.Nombre = beneficiario.Nombre + ",/";
        //        layout.Movimientos.Add(nuevo);
        //        return layout.ToString();
        //    }
        //    return "";
        //}

        //public string LayoutBanamex1(MovimientoBancarioDetailModel movimiento, string descripcion, bool clabe) {
        //    var cuentaOrigen = this.GetCuenta(movimiento.IdCuentaP);
        //    var beneficiario = this.beneficiarioRepository.GetBeneficiario(movimiento.IdDirectorio);
        //    var cuentaDestino = beneficiario.CuentasBancarias.Where(it => it.IdCuenta == movimiento.IdCuentaT).Single();

        //    if (cuentaOrigen != null && cuentaDestino != null) {
        //        var layout = new Scrapper.Banco.Banamex.LayoutC();
        //        layout.Control.TipoRegistro = 1;
        //        layout.Control.NumeroIdentificacion = DbConvert.ConvertInt32(cuentaOrigen.NumCliente);
        //        layout.Control.FechaPago = DateTime.Now;
        //        layout.Control.SecuenciaArchivo = 1;
        //        layout.Control.NombreEmpresa = cuentaOrigen.Beneficiario;
        //        layout.Control.Descripcion = descripcion;
        //        layout.Control.NaturalezaArchivo = 12;
        //        layout.Control.Instrucciones = "";
        //        layout.Control.Caracteristica = 0;
        //        // registro global de importacion
        //        layout.Global.TipoRegistro = 2;
        //        layout.Global.TipoOperacion = 1;
        //        layout.Global.TipoCuenta = 1;
        //        layout.Global.NumeroSucursal = DbConvert.ConvertInt32(cuentaOrigen.Sucursal);
        //        layout.Global.NumeroCuenta = cuentaOrigen.NumCuenta;
        //        layout.Global.Importe = movimiento.Cargo;


        //        StringBuilder stringBuilder = new StringBuilder();
        //        stringBuilder.Append(layout.Control.Create() + "\r\n");
        //        stringBuilder.Append(layout.Global.Create() + "\r\n");

        //        var nuevo = new Scrapper.Banco.Banamex.RegistroCargoOAbono();
        //        nuevo.TipoRegistro = 3;
        //        nuevo.TipoOperacion = 0;
        //        nuevo.ClaveMoneda = 1;
        //        nuevo.Importe = double.Parse(movimiento.Cargo.ToString());
        //        nuevo.TipoCuenta = 1;

        //        if (clabe == true) {
        //            nuevo.NumeroCuenta = cuentaDestino.Clabe;
        //        } else {
        //            if (cuentaDestino.NumeroDeCuenta == null) {
        //                nuevo.NumeroCuenta = cuentaDestino.Clabe;
        //            } else {
        //                nuevo.NumeroCuenta = cuentaDestino.NumeroDeCuenta;
        //            }
        //        }

        //        if (nuevo.ReferenciaAlfanumerica != null) {
        //            if (nuevo.ReferenciaAlfanumerica.Trim().Length > 0) {
        //                nuevo.ReferenciaAlfanumerica = movimiento.Referencia;
        //            }
        //        }
        //        if (nuevo.ReferenciaAlfanumerica.Trim().Length == 0) {
        //            nuevo.ReferenciaAlfanumerica = "PAGO FAC ";
        //        }

        //        if (movimiento.ReferenciaNumerica != null) {
        //            if (movimiento.ReferenciaNumerica.Trim().Length > 0) {
        //                nuevo.ReferenciaNumerica = cuentaDestino.RefNumerica;
        //            }
        //        }
        //        if (nuevo.ReferenciaNumerica.Length == 0) {
        //            nuevo.ReferenciaNumerica = DateTime.Now.ToString("ddMMyy");
        //        }

        //        nuevo.ClaveBanco = cuentaDestino.Clave;

        //        nuevo.Nombre = beneficiario.Nombre + ",/";

        //        stringBuilder.Append(nuevo.Create() + "\r\n");

        //        layout.Totales.NumeroAbonos = 1;
        //        layout.Totales.ImporteTotalAbonos = double.Parse(movimiento.Abono.ToString());
        //        layout.Totales.NumeroCargos = movimiento.Comprobantes.Count;
        //        layout.Totales.ImporteTotalCargos = double.Parse(movimiento.Comprobantes.Sum<MovimientoBancarioComprobanteDetailModel>(c => c.Cargo).ToString());
        //        stringBuilder.Append(layout.Totales.Create() + "\r\n");

        //        return stringBuilder.ToString();
        //    }
        //    return "";
        //}
        #endregion

        /// <summary>
        /// crear tabla
        /// </summary>
        public new void CreateTables() {
            this.movimientoBancarioRepository.CreateTables();
        }

        #region metodos estaticos
        public static bool ExportarExcel(string fileName, string fileTemplete, BindingList<IMovimientoBancarioDetailModel> dataSource) {
            //var fileTemplete = @"D:\bitbucket\jaeger_edita_desktop\documentos\Bancos\templete_bancos.xlsx";
            var d0 = dataSource.Where(it => it.Comprobantes.Count > 0).Select(it => it.Comprobantes).ToList();
            var d1 = new BindingList<MovimientoBancarioComprobanteModel>();
            var d2 = new List<MovimientoBancarioModel>();

            foreach (var item in dataSource) {
                d2.Add(item as MovimientoBancarioModel);
                if (item.Comprobantes.Count > 0) {
                    foreach (var item1 in item.Comprobantes) {
                        d1.Add(item1);
                    }
                }
            }
            // configuracion para la exportacion
            var config = new OpenXmlConfiguration() {
                IgnoreTemplateParameterMissing = false,
                TableStyles = TableStyles.Default,
                FastMode = true
            };
            var data = new Dictionary<string, object>() {
                ["Movimientos"] = DbConvert.ConvertToDataTable(d2),
                ["Comprobantes"] = Domain.Base.Services.DbConvert.ConvertToDataTable(d1)
            };

            // si pasan un templete se carga
            if (System.IO.File.Exists(fileTemplete)) {
                try {
                    MiniExcelLibs.MiniExcel.SaveAsByTemplate(fileName, fileTemplete, data, config);
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine($"Could not save file {fileName} " + ex.Message);
                }
            }

            // probamos el templete por default
            try {
                //var templatePath = this.resource.GetAsBytes("Jaeger.Aplication.Validador.Reports.Validacion.xlsx");
                //MiniExcel.SaveAsByTemplate(fileName, templatePath, data, config);
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// status aplicables a los movimientos bancarios
        /// </summary>
        public static List<MovimientoBancarioStatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<MovimientoBancarioStatusModel, MovimientoBancarioStatusEnum>().ToList();
        }

        public static IBancoMovimientoBuilder Create() {
            return new BancoMovimientoBuilder();
        }

        public new static IMovimientoBancarioQueryBuilder Query() {
            return new MovimientoBancarioQueryBuilder();
        }

        public static IConfigurationBuilder ConfigurationBuilder() {
            return new ConfigurationBuilder();
        }

        public static List<BancoFormatoImpresoModel> GetRepresentacionImpresa() {
            var formatos = new List<BancoFormatoImpresoModel> {
                new BancoFormatoImpresoModel { IdFormato = 0, Nombre = "Movimiento", Reporte = "BancosMovimientoReporte31.rdlc" },
                new BancoFormatoImpresoModel { IdFormato = 1, Nombre = "Cheque", Reporte = "BancosMovimientoChequeReporte10.rdlc" },
                new BancoFormatoImpresoModel { IdFormato = 2, Nombre = "Recibo de Cobro", Reporte = "BancosMovimientoReporte31.rdlc" },
                new BancoFormatoImpresoModel { IdFormato = 3, Nombre = "Recibo de Pago", Reporte = "BancosMovimientoReporte31.rdlc" },
                new BancoFormatoImpresoModel { IdFormato = 4, Nombre = "Recibo de Comisión", Reporte = "BancosMovimientoReporte31.rdlc" },
                new BancoFormatoImpresoModel { IdFormato = 5, Nombre = "Transferencia entre cuentas", Reporte = "BancosTransferenciaCuentasReporte30.rdlc" }
            };
            return formatos;
        }
        #endregion
    }
}
