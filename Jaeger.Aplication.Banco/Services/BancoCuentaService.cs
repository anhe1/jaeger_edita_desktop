﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Banco.Builder;
using Jaeger.Domain.Base.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Banco {
    /// <summary>
    /// servicio para cuentas bancarias
    /// </summary>
    public class BancoCuentaService : IBancoCuentaService {
        #region
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;
        protected ISqlCuentaSaldoRepository cuentaSaldoRepository;
        #endregion

        public BancoCuentaService() {
            this.Onload();
        }

        public virtual void Onload() {
            this.cuentaBancariaRepository = new SqlSugarCuentaBancariaRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.cuentaSaldoRepository = new SqlSugarCuentaSaldoRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener cuenta bancaria
        /// </summary>
        /// <param name="index">indice de la cuenta</param>
        public BancoCuentaDetailModel GetCuenta(int index) {
            return this.cuentaBancariaRepository.GetById(index);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.cuentaBancariaRepository.GetList<T1>(conditionals);
        }

        /// <summary>
        /// obtener catalogo de monedas 
        /// </summary>
        /// <returns></returns>
        public List<MovimientoBancarioMonedaModel> GetMonedas() {
            var monedas = new List<MovimientoBancarioMonedaModel> {
                new MovimientoBancarioMonedaModel("MXN", "Peso Mexicano"),
                new MovimientoBancarioMonedaModel("USD", "Dolar Americano")
            };
            return monedas;
        }

        /// <summary>
        /// almacenar modelo
        /// </summary>
        public IBancoCuentaDetailModel Save(IBancoCuentaDetailModel model) {
            try {
                return this.cuentaBancariaRepository.Save(model);
            } catch (Exception ex) {
                throw new JaegerException("BancoCuentaBancariaService:Save:", ex);
            }
        }

        public BancoCuentaSaldoDetailModel GetSaldo(int idCuenta, DateTime fecha) {
            return this.cuentaSaldoRepository.GetSaldo(idCuenta, fecha);
        }

        public BancoCuentaSaldoDetailModel Save(BancoCuentaSaldoDetailModel model) {
            return this.cuentaSaldoRepository.Save(model);
        }

        /// <summary>
        /// crear tabla
        /// </summary>
        public void CreateTables() {
            this.cuentaBancariaRepository.Create();
            this.cuentaSaldoRepository.Create();
        }

        public static ICuentaBancariaQueryBuilder Query() {
            return new CuentaBancariaQueryBuilder();
        }
    }
}
