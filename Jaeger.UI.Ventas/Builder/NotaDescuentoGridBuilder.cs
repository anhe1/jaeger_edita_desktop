﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Ventas.Builder {
    public class NotaDescuentoGridBuilder : GridViewBuilder, IGridViewBuilder, IDisposable, INotaDescuentoGridBuilder, INotaDescuentoTempletesGridBuilder, INotaDescuentoColumnsGridBuilder {
        public INotaDescuentoTempletesGridBuilder Templetes() {
            return this;
        }

        public INotaDescuentoTempletesGridBuilder Master() {
            _Columns.Clear();
            ColFolio().ColStatus().ColClave().ColCliente().FechaEmision().FechaCancela().ColClaveMotivo().ColIdMotivo().GranTotal().ColIdDocumento().ColReferencia().ColContacto().Nota()
                .ColNumeros().ColFechaNuevo().ColCreo();
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColFolio() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColStatus() {
            var column = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75
            };
            var c0 = column.ConditionalFormattingObjectList;
            c0.Add(this.CanceladoConditionalFormat());
            this._Columns.Add(column);
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColClave() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColCliente() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Cliente",
                Name = "Nombre",
                Width = 280
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder FechaEmision() {
            _Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEmision",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder FechaCancela() {
            _Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaCancela",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener columna de motivo de devoluciones
        /// </summary>
        public INotaDescuentoColumnsGridBuilder ColClaveMotivo() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CvMotivo",
                HeaderText = "CvMotivo",
                Name = "CvMotivo",
                Width = 275
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColIdMotivo() {
            _Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdMotivo",
                HeaderText = "Cv. Motivo",
                Name = "IdMotivo",
                Width = 145
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder GranTotal() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "GTotal",
                FormatString = "{0:n2}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColIdDocumento() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                IsVisible = false,
                Name = "IdDocumento",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 220
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColReferencia() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColContacto() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Contacto",
                HeaderText = "Contacto",
                Name = "Contacto",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleRight
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Nota() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                Width = 200,
                IsVisible = false,
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColNumeros() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NPartida",
                HeaderText = "Partidas",
                Name = "NPartida",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                FormatString = "{0:N0}"
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColFechaNuevo() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder ColCreo() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        #region formatos condiciones
        /// <summary>
        /// formato condicional para registros inactivos
        /// </summary>
        public ExpressionFormattingObject CanceladoConditionalFormat() {
            return new ExpressionFormattingObject("Cancelacion", "IdStatus = 0", true) {
                RowForeColor = Color.DarkGray
            };
        }
        #endregion
    }
}
