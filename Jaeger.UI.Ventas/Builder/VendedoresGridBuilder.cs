﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Ventas.Builder {
    public class VendedoresGridBuilder : GridViewBuilder, IGridViewBuilder, IDisposable, IVendedoresGridBuilder, IVendedoresTempletesGridBuilder, IVendedoresColumnsGridBuilder {
        public VendedoresGridBuilder() : base() { }

        #region templetes
        public IVendedoresTempletesGridBuilder Templetes() {
            return this;
        }

        public IVendedoresTempletesGridBuilder Master() {
            _Columns.Clear();
            Activo().Clave().Nombre().Correo().Telefono().IdComision().Nota().Modifica().FechaModifica();
            return this;
        }

        public IVendedoresTempletesGridBuilder Clientes() {
            _Columns.Clear();
            Activo().Clave().Nombre().Correo().Telefono().IdComision().Nota().Creo().FechaNuevo();
            return this;
        }
        #endregion

        #region vendedores
        public IVendedoresColumnsGridBuilder Activo() {
            _Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo"
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Clave() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 75
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Nombre() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "IdDrctr",
                Width = 250
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Correo() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo",
                Name = "Correo",
                ReadOnly = true,
                Width = 200
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Telefono() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 120
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder IdComision() {
            _Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdComision",
                HeaderText = "Comisión",
                Name = "IdComision",
                ReadOnly = true,
                Width = 240
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Nota() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota1",
                HeaderText = "Nota",
                MaxLength = 50,
                Name = "Nota1",
                Width = 150
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder Modifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 65
            });
            return this;
        }
        public IVendedoresColumnsGridBuilder FechaModifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Sist.",
                Name = "FechaModifica",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }
        #endregion

        #region clientes
        public IVendedoresColumnsGridBuilder Cliente() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Cliente",
                Name = "IdDrctr",
                Width = 250
            });
            return this;
        }

        public IVendedoresColumnsGridBuilder Creo() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                Width = 65
            });
            return this;
        }

        public IVendedoresColumnsGridBuilder FechaNuevo() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }
        #endregion
    }
}
