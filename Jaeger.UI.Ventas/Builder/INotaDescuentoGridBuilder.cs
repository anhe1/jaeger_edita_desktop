﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Ventas.Builder {
    public interface INotaDescuentoGridBuilder : IGridViewBuilder, IDisposable {
        INotaDescuentoTempletesGridBuilder Templetes();
    }

    public interface INotaDescuentoTempletesGridBuilder : IGridViewBuilder {
        INotaDescuentoTempletesGridBuilder Master();
    }

    public interface INotaDescuentoColumnsGridBuilder : IGridViewBuilder {
        INotaDescuentoColumnsGridBuilder ColFolio();

        INotaDescuentoColumnsGridBuilder ColStatus();

        INotaDescuentoColumnsGridBuilder ColClave();

        INotaDescuentoColumnsGridBuilder ColCliente();

        INotaDescuentoColumnsGridBuilder FechaEmision();

        INotaDescuentoColumnsGridBuilder FechaCancela();

        /// <summary>
        /// obtener columna de motivo de devoluciones
        /// </summary>
        INotaDescuentoColumnsGridBuilder ColClaveMotivo();

        INotaDescuentoColumnsGridBuilder ColIdMotivo();

        INotaDescuentoColumnsGridBuilder GranTotal();

        INotaDescuentoColumnsGridBuilder ColIdDocumento();

        INotaDescuentoColumnsGridBuilder ColReferencia();

        INotaDescuentoColumnsGridBuilder ColContacto();

        INotaDescuentoColumnsGridBuilder Nota();

        INotaDescuentoColumnsGridBuilder ColNumeros();

        INotaDescuentoColumnsGridBuilder ColFechaNuevo();

        INotaDescuentoColumnsGridBuilder ColCreo();
    }
}
