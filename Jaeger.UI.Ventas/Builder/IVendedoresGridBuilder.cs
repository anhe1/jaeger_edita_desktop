﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Ventas.Builder {
    public interface IVendedoresGridBuilder : IGridViewBuilder, IDisposable {
        IVendedoresTempletesGridBuilder Templetes();
    }

    public interface IVendedoresTempletesGridBuilder : IGridViewBuilder, IDisposable {
        IVendedoresTempletesGridBuilder Master();
        IVendedoresTempletesGridBuilder Clientes();
    }

    public interface IVendedoresColumnsGridBuilder : IGridViewBuilder, IDisposable {
        IVendedoresColumnsGridBuilder Activo();
        IVendedoresColumnsGridBuilder Clave();
        IVendedoresColumnsGridBuilder Nombre();
        IVendedoresColumnsGridBuilder Correo();
        IVendedoresColumnsGridBuilder Telefono();
        IVendedoresColumnsGridBuilder IdComision();
        IVendedoresColumnsGridBuilder Nota();
        IVendedoresColumnsGridBuilder Modifica();
        IVendedoresColumnsGridBuilder FechaModifica();

        #region clientes
        IVendedoresColumnsGridBuilder Cliente();
        IVendedoresColumnsGridBuilder Creo();
        IVendedoresColumnsGridBuilder FechaNuevo();
        #endregion
    }
}
