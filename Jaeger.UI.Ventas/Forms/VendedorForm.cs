﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.UI.Common.Services;
using Telerik.WinControls;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Contribuyentes.Services;
using Jaeger.Aplication.Ventas;
using Jaeger.Aplication.Ventas.Contracts;

namespace Jaeger.UI.Ventas.Forms {
    public partial class VendedorForm : RadForm {
        protected IVendedorService service;
        protected IComisionService comision;
        private BindingList<ComisionDetailModel> catalogoComision;
        private Vendedor2DetailModel currentVendedor;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;

        public VendedorForm(Domain.Base.ValueObjects.UIAction permisos) {
            InitializeComponent();
            this._permisos = permisos;
            this.currentVendedor = null;
        }

        public VendedorForm(Vendedor2DetailModel item, Domain.Base.ValueObjects.UIAction permisos) {
            InitializeComponent();
            this.currentVendedor = item;
            this._permisos = permisos;
        }

        private void VendedorForm_Load(object sender, EventArgs e) {
            this.service = new VendedorService();
            this.comision = new ComisionService();
            this.PrepararFormulario.RunWorkerAsync();
            this.radGridClientes.Standard();
            this.radGridClientes.AllowEditRow = _permisos.Editar;
            this.TCliente.Enabled = _permisos.Editar;
            this.Comision.Enabled = _permisos.Autorizar;
            this.Comision.SelectedValueChanged += this.Comision_SelectedValueChanged;
        }

        private void Comision_SelectedValueChanged(object sender, EventArgs e) {
            if (!(this.Comision.SelectedItem == null)) {
                var _seleccionado = ((GridViewRowInfo)this.Comision.SelectedItem).DataBoundItem as ComisionDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Activo == false) {
                        this.Comision.SelectedIndex = -1;
                        this.Comision.SelectedValue = null;
                        this.Comision.SelectedItem = null;
                        this.currentVendedor.IdComision = 0;
                        this.currentVendedor.IdComisionD = 0;
                    }
                }
            }
        }

        #region barra de herramintas vendedor
        private void TVendedor_Nuevo_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TVendedor_Guardar_Click(object sender, EventArgs e) {
            this.service.Saveable(this.currentVendedor);
        }

        private void TVndedor_Actualizar_Click(object sender, EventArgs e) {
            if (this.currentVendedor == null) {
                this.currentVendedor = new Vendedor2DetailModel();
            } else {

            }
            this.CreateBinding();
        }

        private void TVendedor_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas clientes
        private void TCliente_Nuevo_Click(object sender, EventArgs e) {
            //var _agregar = new Clientes.ClienteBuscarForm();
            //_agregar.Selected += Agregar_Selected;
            //_agregar.ShowDialog(this);
        }

        private void TCliente_Remover_Click(object sender, EventArgs e) {
            if (this.radGridClientes.CurrentRow != null) {
                var _selccionado = this.radGridClientes.CurrentRow.DataBoundItem as ContribuyenteVendedorModel;
                if (_selccionado != null) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover al cliente seleccionado? Esta acción no se puede revertir.", "Atención",
                        MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                        return;
                    }
                        if (_selccionado.IdVendedor == 0) {
                        this.radGridClientes.Rows.Remove(this.radGridClientes.CurrentRow);
                    } else {
                        _selccionado.Activo = false;
                        _selccionado.SetModified = true;
                        this.radGridClientes.CurrentRow.IsVisible = false;
                    }
                }
            }
        }

        private void Agregar_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                var _cliente = new ContribuyenteVendedorModel {
                    IdCliente = e.IdDirectorio,
                    Nombre = e.Nombre,
                    IdVendedor = this.currentVendedor.IdDirectorio,
                    Clave = e.Clave,
                    SetModified = true
                };
                this.currentVendedor.Clientes.Add(_cliente);
            }
        }
        #endregion

        private void CreateBinding() {
            this.Clave.DataBindings.Clear();
            this.Clave.DataBindings.Add("Text", this.currentVendedor, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Clave.ReadOnly = this.currentVendedor.IdVendedor > 0;

            this.RFC.DataBindings.Clear();
            this.RFC.DataBindings.Add("Text", this.currentVendedor, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RFC.ReadOnly = this.currentVendedor.IdVendedor > 0;

            this.CURP.DataBindings.Clear();
            this.CURP.DataBindings.Add("Text", this.currentVendedor, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CURP.ReadOnly = this.currentVendedor.IdVendedor > 0;

            this.Nombre.DataBindings.Clear();
            this.Nombre.DataBindings.Add("Text", this.currentVendedor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Nombre.ReadOnly = this.currentVendedor.IdVendedor > 0 && !string.IsNullOrEmpty(this.currentVendedor.Nombre);

            this.Telefono.DataBindings.Clear();
            this.Telefono.DataBindings.Add("Text", this.currentVendedor, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Correo.DataBindings.Clear();
            this.Correo.DataBindings.Add("Text", this.currentVendedor, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.DataBindings.Add("Text", this.currentVendedor, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Comision.DataBindings.Clear(); 
            this.Comision.DataBindings.Add("SelectedValue", this.currentVendedor,"IdComision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.radGridClientes.DataSource = this.currentVendedor;
            this.radGridClientes.DataMember = "Clientes";
        }

        private void PrepararFormulario_DoWork(object sender, DoWorkEventArgs e) {
            this.catalogoComision = this.comision.GetComisiones(true);
        }

        private void PrepararFormulario_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Comision.DataSource = this.catalogoComision;
            this.Comision.SelectedIndex = -1;
            this.Comision.SelectedItem = null;
            this.TVendedor.Actualizar.PerformClick();
        }

        private void GridClientes_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (this.radGridClientes.CurrentRow != null) {
                var _selccionado = this.radGridClientes.CurrentRow.DataBoundItem as ContribuyenteVendedorModel;
                if (_selccionado != null) {
                    _selccionado.SetModified = true;
                }
            }
        }
    }
}
