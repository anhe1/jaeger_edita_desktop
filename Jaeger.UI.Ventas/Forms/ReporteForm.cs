﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Util;
using Jaeger.QRCode.Helpers;

namespace Jaeger.UI.Ventas.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources _recursos = new EmbeddedResources("Jaeger.Domain.Ventas");
        private readonly string _dominio = "Jaeger.Domain.Ventas.Reports.";

        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(RemisionComisionPrinter)) {
                this.CrearEstadoCuentaComision();
            } else if (this.CurrentObject.GetType() == typeof(EdoCuentaVendedorPrinter)) {
                this.CrearReporte1();
            } else if (this.CurrentObject.GetType() == typeof(EdoCuentaClientePrinter)) {
                this.CrearReporte2();
            } else if (this.CurrentObject.GetType() == typeof(MovimientoBancarioPrinter)) {
                this.CrearTransferencia();
            } else if (this.CurrentObject.GetType() == typeof(VendedorCarteraPrinter)) {
                this.CrearCarteraClientes();
            }
        }

        private void CrearReporte1() {
            var current = (EdoCuentaVendedorPrinter)this.CurrentObject;
            this.LoadDefinition = this._recursos.GetStream(string.Concat(_dominio, "EdoCuentaVendedorReporte.rdlc"));

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Reporte");
            this.SetParameter("Domicilio", "");
            var d = DbConvert.ConvertToDataTable<EdoCuentaVendedorPrinter>(new List<EdoCuentaVendedorPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearReporte2() {
            var current = (EdoCuentaClientePrinter)this.CurrentObject;
            this.LoadDefinition = this._recursos.GetStream(string.Concat(_dominio, "EdoCuentaClienteReporte.rdlc"));
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Reporte");
            this.SetParameter("Domicilio", "");
            var d = DbConvert.ConvertToDataTable<EdoCuentaClientePrinter>(new List<EdoCuentaClientePrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearEstadoCuentaComision() {
            var current = (RemisionComisionPrinter)this.CurrentObject;
            this.LoadDefinition = this._recursos.GetStream(string.Concat(_dominio, "ComisionVendedorReporte.rdlc"));
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Comision");
            this.SetParameter("Domicilio", "");
            var d1 = DbConvert.ConvertToDataTable<RemisionComisionPrinter>(new List<RemisionComisionPrinter>() { current });
            this.SetDataSource("Comprobante", d1);
            var d = DbConvert.ConvertToDataTable<RemisionComisionDetailModel>(new List<RemisionComisionDetailModel>(current.Conceptos));
            this.SetDataSource("Conceptos", d);
            this.Finalizar();
        }

        private void CrearTransferencia() {
            var current = (MovimientoBancarioPrinter)this.CurrentObject;
            if (current.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                this.LoadDefinition = this.GetResourceStream("BancosTransferenciaCuentasReporte30.rdlc");
            } else if (current.TipoOperacion == BancoTipoOperacionEnum.Cheque) {
                this.LoadDefinition = this.GetResourceStream("BancosMovimientoChequeReporte10.rdlc");
                this.SetDataSource("MovimientoBancarioComprobantes", DbConvert.ConvertToDataTable(current.Comprobantes));
            } else {
                if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboCobro) {
                    this.LoadDefinition = this.GetResourceStream("BancosMovimientoReporte30.rdlc");
                } else if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboPago) {
                    this.LoadDefinition = this.GetResourceStream("BancosMovimientoReporte30.rdlc");
                } else if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboComision) {
                    this.LoadDefinition = this.GetResourceStream("ComisionVendedorRecibo20.rdlc");
                }
                this.SetDataSource("MovimientoBancarioComprobantes", DbConvert.ConvertToDataTable(current.Comprobantes));
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d = DbConvert.ConvertToDataTable<MovimientoBancarioPrinter>(new List<MovimientoBancarioPrinter>() { current });
            this.SetDataSource("MovimientoBancario", d);
            this.Finalizar();
        }

        public void CrearCarteraClientes() {
            foreach (var item in this._recursos.GetList()) {
                Console.WriteLine(item);
            }
            var current = (VendedorCarteraPrinter)this.CurrentObject;
            var si = this._recursos.GetResource(_dominio, "VendedorClientesReport.rdlc");
            this.LoadDefinition = this._recursos.GetStream("Jaeger.Domain.Ventas.Reports.VendedorClientesReport.rdlc");
            this.Procesar();
            this.SetDisplayName("Vendedor");
            var vendedor = Domain.Base.Services.DbConvert.ConvertToDataTable<VendedorCarteraPrinter>(new List<VendedorCarteraPrinter> { current });
            var cartera = Domain.Base.Services.DbConvert.ConvertToDataTable<IContribuyenteVendedorModel>(new List<IContribuyenteVendedorModel>(current.Clientes));
            this.SetDataSource("Vendedor", vendedor);
            this.SetDataSource("Cartera", cartera);
            this.Finalizar();
        }

        private Stream GetResourceStream(string name) {
            return this._recursos.GetStream(string.Concat(this._dominio, name));
        }
    }
}
