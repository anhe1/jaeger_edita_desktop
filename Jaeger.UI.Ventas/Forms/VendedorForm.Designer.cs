﻿namespace Jaeger.UI.Ventas.Forms {
    partial class VendedorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VendedorForm));
            this.TVendedor = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.panel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButtonBeneficiario = new Telerik.WinControls.UI.RadButton();
            this.radButtonActualizar = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Comision = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RFC = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.label6 = new Telerik.WinControls.UI.RadLabel();
            this.Correo = new Telerik.WinControls.UI.RadTextBox();
            this.label14 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.label15 = new Telerik.WinControls.UI.RadLabel();
            this.Telefono = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new Telerik.WinControls.UI.RadLabel();
            this.CURP = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.label2 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new Telerik.WinControls.UI.RadLabel();
            this.PrepararFormulario = new System.ComponentModel.BackgroundWorker();
            this.radGridClientes = new Telerik.WinControls.UI.RadGridView();
            this.TCliente = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridClientes.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TVendedor
            // 
            this.TVendedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.TVendedor.Etiqueta = "Vendedor:";
            this.TVendedor.Location = new System.Drawing.Point(0, 0);
            this.TVendedor.Name = "TVendedor";
            this.TVendedor.ShowActualizar = true;
            this.TVendedor.ShowAutorizar = false;
            this.TVendedor.ShowCerrar = true;
            this.TVendedor.ShowEditar = false;
            this.TVendedor.ShowExportarExcel = false;
            this.TVendedor.ShowFiltro = false;
            this.TVendedor.ShowGuardar = true;
            this.TVendedor.ShowHerramientas = false;
            this.TVendedor.ShowImagen = false;
            this.TVendedor.ShowImprimir = false;
            this.TVendedor.ShowNuevo = false;
            this.TVendedor.ShowRemover = false;
            this.TVendedor.Size = new System.Drawing.Size(668, 30);
            this.TVendedor.TabIndex = 2;
            this.TVendedor.Nuevo.Click += this.TVendedor_Nuevo_Click;
            this.TVendedor.Guardar.Click += this.TVendedor_Guardar_Click;
            this.TVendedor.Actualizar.Click += this.TVndedor_Actualizar_Click;
            this.TVendedor.Cerrar.Click += this.TVendedor_Cerrar_Click;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radButtonBeneficiario);
            this.panel1.Controls.Add(this.radButtonActualizar);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.Comision);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.RFC);
            this.panel1.Controls.Add(this.Nombre);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.Correo);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.Nota);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.Telefono);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.CURP);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Clave);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(668, 139);
            this.panel1.TabIndex = 3;
            // 
            // radButtonBeneficiario
            // 
            this.radButtonBeneficiario.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonBeneficiario.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonBeneficiario.Location = new System.Drawing.Point(409, 32);
            this.radButtonBeneficiario.Name = "radButtonBeneficiario";
            this.radButtonBeneficiario.Size = new System.Drawing.Size(20, 20);
            this.radButtonBeneficiario.TabIndex = 374;
            // 
            // radButtonActualizar
            // 
            this.radButtonActualizar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonActualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonActualizar.Location = new System.Drawing.Point(430, 32);
            this.radButtonActualizar.Name = "radButtonActualizar";
            this.radButtonActualizar.Size = new System.Drawing.Size(20, 20);
            this.radButtonActualizar.TabIndex = 373;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(520, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 96);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 352;
            this.pictureBox1.TabStop = false;
            // 
            // Comision
            // 
            this.Comision.AutoSizeDropDownToBestFit = true;
            this.Comision.DisplayMember = "Descripcion";
            this.Comision.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Comision.NestedRadGridView
            // 
            this.Comision.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Comision.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comision.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Comision.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Comision.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Comision.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Comision.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Comision.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdComision";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdComision";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Comisión";
            gridViewTextBoxColumn2.Name = "Descripción";
            gridViewTextBoxColumn2.Width = 200;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = 0";
            expressionFormattingObject1.Name = "Activo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            this.Comision.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn1});
            this.Comision.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Comision.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Comision.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Comision.EditorControl.Name = "NestedRadGridView";
            this.Comision.EditorControl.ReadOnly = true;
            this.Comision.EditorControl.ShowGroupPanel = false;
            this.Comision.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Comision.EditorControl.TabIndex = 0;
            this.Comision.Location = new System.Drawing.Point(70, 111);
            this.Comision.Name = "Comision";
            this.Comision.NullText = "Comisión";
            this.Comision.Size = new System.Drawing.Size(380, 20);
            this.Comision.TabIndex = 7;
            this.Comision.TabStop = false;
            this.Comision.ValueMember = "IdComision";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(12, 111);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(55, 18);
            this.radLabel7.TabIndex = 118;
            this.radLabel7.Text = "Comisión:";
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.SystemColors.Window;
            this.RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFC.Location = new System.Drawing.Point(167, 6);
            this.RFC.Mask = "^[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]$";
            this.RFC.MaskType = Telerik.WinControls.UI.MaskType.Regex;
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(120, 20);
            this.RFC.TabIndex = 1;
            this.RFC.TabStop = false;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(70, 32);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(333, 20);
            this.Nombre.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 18);
            this.label6.TabIndex = 114;
            this.label6.Text = "Nombre:";
            // 
            // Correo
            // 
            this.Correo.Location = new System.Drawing.Point(283, 58);
            this.Correo.Name = "Correo";
            this.Correo.Size = new System.Drawing.Size(167, 20);
            this.Correo.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(234, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 18);
            this.label14.TabIndex = 108;
            this.label14.Text = "Correo:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(70, 85);
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(380, 20);
            this.Nota.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(11, 86);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 18);
            this.label15.TabIndex = 109;
            this.label15.Text = "Nota:";
            // 
            // Telefono
            // 
            this.Telefono.Location = new System.Drawing.Point(70, 58);
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(158, 20);
            this.Telefono.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 18);
            this.label5.TabIndex = 110;
            this.label5.Text = "Teléfono:";
            // 
            // CURP
            // 
            this.CURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CURP.Location = new System.Drawing.Point(330, 6);
            this.CURP.Name = "CURP";
            this.CURP.Size = new System.Drawing.Size(120, 20);
            this.CURP.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(287, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 18);
            this.label3.TabIndex = 106;
            this.label3.Text = "CURP:";
            // 
            // Clave
            // 
            this.Clave.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Clave.Location = new System.Drawing.Point(71, 6);
            this.Clave.Name = "Clave";
            this.Clave.Size = new System.Drawing.Size(57, 20);
            this.Clave.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(134, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 18);
            this.label2.TabIndex = 104;
            this.label2.Text = "RFC:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(20, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 18);
            this.label1.TabIndex = 103;
            this.label1.Text = "Clave:";
            // 
            // PrepararFormulario
            // 
            this.PrepararFormulario.DoWork += new System.ComponentModel.DoWorkEventHandler(this.PrepararFormulario_DoWork);
            this.PrepararFormulario.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.PrepararFormulario_RunWorkerCompleted);
            // 
            // radGridClientes
            // 
            this.radGridClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridClientes.Location = new System.Drawing.Point(0, 199);
            // 
            // 
            // 
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "Registro Activo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 75;
            gridViewMultiComboBoxColumn1.FieldName = "Nombre";
            gridViewMultiComboBoxColumn1.HeaderText = "Cliente";
            gridViewMultiComboBoxColumn1.Name = "Nombre";
            gridViewMultiComboBoxColumn1.ReadOnly = true;
            gridViewMultiComboBoxColumn1.Width = 250;
            gridViewTextBoxColumn4.FieldName = "Nota";
            gridViewTextBoxColumn4.HeaderText = "Nota";
            gridViewTextBoxColumn4.MaxLength = 50;
            gridViewTextBoxColumn4.Name = "Nota";
            gridViewTextBoxColumn4.Width = 150;
            gridViewTextBoxColumn5.FieldName = "Modifica";
            gridViewTextBoxColumn5.HeaderText = "Usuario";
            gridViewTextBoxColumn5.Name = "Modifica";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 65;
            gridViewTextBoxColumn6.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn6.FieldName = "FechaModifica";
            gridViewTextBoxColumn6.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn6.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn6.Name = "FechaModifica";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 75;
            this.radGridClientes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.radGridClientes.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridClientes.Name = "radGridClientes";
            this.radGridClientes.Size = new System.Drawing.Size(668, 262);
            this.radGridClientes.TabIndex = 0;
            this.radGridClientes.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridClientes_CellEndEdit);
            // 
            // TCliente
            // 
            this.TCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCliente.Etiqueta = "Clientes:";
            this.TCliente.Location = new System.Drawing.Point(0, 169);
            this.TCliente.Name = "TCliente";
            this.TCliente.ShowActualizar = false;
            this.TCliente.ShowAutorizar = false;
            this.TCliente.ShowCerrar = false;
            this.TCliente.ShowEditar = false;
            this.TCliente.ShowExportarExcel = false;
            this.TCliente.ShowFiltro = false;
            this.TCliente.ShowGuardar = false;
            this.TCliente.ShowHerramientas = false;
            this.TCliente.ShowImagen = false;
            this.TCliente.ShowImprimir = false;
            this.TCliente.ShowNuevo = true;
            this.TCliente.ShowRemover = true;
            this.TCliente.Size = new System.Drawing.Size(668, 30);
            this.TCliente.TabIndex = 4;
            this.TCliente.Cerrar.Click += this.TCliente_Nuevo_Click;
            this.TCliente.Remover.Click += this.TCliente_Remover_Click;
            // 
            // VendedorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 461);
            this.Controls.Add(this.radGridClientes);
            this.Controls.Add(this.TCliente);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TVendedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VendedorForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vendedor";
            this.Load += new System.EventHandler(this.VendedorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Comision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Correo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridClientes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TVendedor;
        private Telerik.WinControls.UI.RadPanel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        internal Telerik.WinControls.UI.RadMaskedEditBox RFC;
        internal Telerik.WinControls.UI.RadTextBox Nombre;
        private Telerik.WinControls.UI.RadLabel label6;
        internal Telerik.WinControls.UI.RadTextBox Correo;
        private Telerik.WinControls.UI.RadLabel label14;
        internal Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel label15;
        internal Telerik.WinControls.UI.RadTextBox Telefono;
        private Telerik.WinControls.UI.RadLabel label5;
        internal Telerik.WinControls.UI.RadTextBox CURP;
        private Telerik.WinControls.UI.RadLabel label3;
        internal Telerik.WinControls.UI.RadTextBox Clave;
        private Telerik.WinControls.UI.RadLabel label2;
        private Telerik.WinControls.UI.RadLabel label1;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Comision;
        private System.ComponentModel.BackgroundWorker PrepararFormulario;
        private Telerik.WinControls.UI.RadGridView radGridClientes;
        private Common.Forms.ToolBarStandarControl TCliente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadButton radButtonBeneficiario;
        private Telerik.WinControls.UI.RadButton radButtonActualizar;
    }
}