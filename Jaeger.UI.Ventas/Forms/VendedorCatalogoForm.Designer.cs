﻿namespace Jaeger.UI.Ventas.Forms {
    partial class VendedorCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VendedorCatalogoForm));
            this.TVendedor = new Jaeger.UI.Ventas.Forms.VendedoresGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TVendedor
            // 
            this.TVendedor.Caption = "";
            this.TVendedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TVendedor.Location = new System.Drawing.Point(0, 0);
            this.TVendedor.Name = "TVendedor";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TVendedor.Permisos = uiAction1;
            this.TVendedor.ShowActualizar = true;
            this.TVendedor.ShowAutorizar = false;
            this.TVendedor.ShowAutosuma = false;
            this.TVendedor.ShowCerrar = true;
            this.TVendedor.ShowEditar = true;
            this.TVendedor.ShowExportarExcel = false;
            this.TVendedor.ShowFiltro = true;
            this.TVendedor.ShowHerramientas = false;
            this.TVendedor.ShowImagen = false;
            this.TVendedor.ShowImprimir = true;
            this.TVendedor.ShowNuevo = true;
            this.TVendedor.ShowRemover = true;
            this.TVendedor.ShowSeleccionMultiple = true;
            this.TVendedor.Size = new System.Drawing.Size(964, 529);
            this.TVendedor.TabIndex = 0;
            // 
            // VendedorCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 529);
            this.Controls.Add(this.TVendedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VendedorCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Vendedores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VendedorCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UI.Ventas.Forms.VendedoresGridControl TVendedor;
    }
}