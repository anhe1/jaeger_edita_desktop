﻿namespace Jaeger.UI.Ventas.Forms
{
    partial class ComisionRemisionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn4 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn5 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject2 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn6 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn7 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn41 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn42 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn43 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn44 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn45 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn46 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn47 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn48 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn49 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn50 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn51 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn52 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn53 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn54 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn55 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn56 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn57 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn58 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn59 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn60 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn61 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn62 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject3 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn63 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn64 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn65 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn66 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn67 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn68 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn69 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn70 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn71 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn72 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn73 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn74 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn75 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn76 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn77 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn78 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn79 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn80 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn81 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn82 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn83 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn84 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn85 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn86 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn87 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn88 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn89 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn90 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn91 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn92 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn93 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn94 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn95 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn96 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn97 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn98 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject4 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn99 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn100 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn101 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn102 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn3 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn103 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn104 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn4 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject5 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComisionRemisionForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.GridRemisiones = new Telerik.WinControls.UI.RadGridView();
            this.gridAutorizacion = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridRelacion = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridCobranza = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridConceptos = new Telerik.WinControls.UI.GridViewTemplate();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.GridMovimientos = new Telerik.WinControls.UI.RadGridView();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.MenuContextual = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.ContextMenuSeleccion = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCopiar = new Telerik.WinControls.UI.RadMenuItem();
            this.ContextMenuCrearRecibo = new Telerik.WinControls.UI.RadMenuItem();
            this.TComision = new Jaeger.UI.Ventas.Forms.VendedorComisionToolBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridRemisiones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRemisiones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutorizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCobranza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridMovimientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridMovimientos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1134, 616);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.GridRemisiones);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1134, 306);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // GridRemisiones
            // 
            this.GridRemisiones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridRemisiones.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Folio";
            gridViewTextBoxColumn1.HeaderText = "Folio";
            gridViewTextBoxColumn1.IsPinned = true;
            gridViewTextBoxColumn1.Name = "Folio";
            gridViewTextBoxColumn1.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "IdPedido";
            gridViewTextBoxColumn2.HeaderText = "# Pedido";
            gridViewTextBoxColumn2.IsPinned = true;
            gridViewTextBoxColumn2.Name = "IdPedido";
            gridViewTextBoxColumn2.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 65;
            gridViewComboBoxColumn1.FieldName = "IdStatus";
            gridViewComboBoxColumn1.HeaderText = "Status";
            gridViewComboBoxColumn1.IsPinned = true;
            gridViewComboBoxColumn1.IsVisible = false;
            gridViewComboBoxColumn1.Name = "IdStatus";
            gridViewComboBoxColumn1.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewComboBoxColumn1.ReadOnly = true;
            gridViewComboBoxColumn1.Width = 75;
            gridViewTextBoxColumn3.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn3.HeaderText = "Cliente";
            gridViewTextBoxColumn3.IsPinned = true;
            gridViewTextBoxColumn3.Name = "ReceptorNombre";
            gridViewTextBoxColumn3.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 220;
            gridViewTextBoxColumn4.FieldName = "ReceptorClave";
            gridViewTextBoxColumn4.HeaderText = "Clave";
            gridViewTextBoxColumn4.IsVisible = false;
            gridViewTextBoxColumn4.Name = "ReceptorClave";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn5.FieldName = "ClienteRecibe";
            gridViewTextBoxColumn5.HeaderText = "Recibe";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "ClienteRecibe";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 150;
            gridViewDateTimeColumn1.FieldName = "FechaEmision";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec.\r\nEmisión";
            gridViewDateTimeColumn1.IsVisible = false;
            gridViewDateTimeColumn1.Name = "FechaEmision";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewDateTimeColumn2.FieldName = "FechaEntrega";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn2.HeaderText = "Fec.\r\nEntrega";
            gridViewDateTimeColumn2.Name = "FechaEntrega";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewDateTimeColumn2.Width = 75;
            gridViewDateTimeColumn3.FieldName = "FechaPagare";
            gridViewDateTimeColumn3.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn3.HeaderText = "Fec. Vence";
            gridViewDateTimeColumn3.IsVisible = false;
            gridViewDateTimeColumn3.Name = "FechaPagare";
            gridViewDateTimeColumn3.ReadOnly = true;
            gridViewDateTimeColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn4.FieldName = "FechaCobranza";
            gridViewDateTimeColumn4.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn4.HeaderText = "Fec.\r\nCobranza";
            gridViewDateTimeColumn4.IsVisible = false;
            gridViewDateTimeColumn4.Name = "FechaCobranza";
            gridViewDateTimeColumn4.ReadOnly = true;
            gridViewDateTimeColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn4.Width = 75;
            gridViewDateTimeColumn5.FieldName = "FechaUltPago";
            gridViewDateTimeColumn5.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn5.HeaderText = "Últ.\r\nCobro";
            gridViewDateTimeColumn5.Name = "FechaUltPago";
            gridViewDateTimeColumn5.ReadOnly = true;
            gridViewDateTimeColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn5.Width = 75;
            gridViewTextBoxColumn6.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn6.FieldName = "FechaCancela";
            gridViewTextBoxColumn6.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn6.HeaderText = "Fec.\r\nCancela";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "FechaCancela";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.DataType = typeof(decimal);
            gridViewTextBoxColumn7.FieldName = "SubTotal";
            gridViewTextBoxColumn7.FormatString = "{0:n}";
            gridViewTextBoxColumn7.HeaderText = "SubTotal";
            gridViewTextBoxColumn7.Name = "SubTotal";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "TotalDescuento";
            gridViewTextBoxColumn8.FormatString = "{0:n}";
            gridViewTextBoxColumn8.HeaderText = "Descuento";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "TotalDescuento";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Importe";
            gridViewTextBoxColumn9.FormatString = "{0:N2}";
            gridViewTextBoxColumn9.HeaderText = "STotal - Desc.";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Importe";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "TotalTrasladoIVA";
            gridViewTextBoxColumn10.FormatString = "{0:N2}";
            gridViewTextBoxColumn10.HeaderText = "T. IVA";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "TotalTrasladoIVA";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "Total";
            gridViewTextBoxColumn11.FormatString = "{0:n}";
            gridViewTextBoxColumn11.HeaderText = "Total";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Total";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "TasaIVAPactado";
            gridViewTextBoxColumn12.FormatString = "{0:N2}";
            gridViewTextBoxColumn12.HeaderText = "% IVA \r\nPact.";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "TasaIVAPactado";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "TotalIVAPactado";
            gridViewTextBoxColumn13.FormatString = "{0:N2}";
            gridViewTextBoxColumn13.HeaderText = "IVA Pact.";
            gridViewTextBoxColumn13.Name = "TotalIVAPactado";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "GTotal";
            gridViewTextBoxColumn14.FormatString = "{0:N2}";
            gridViewTextBoxColumn14.HeaderText = "G. Total";
            gridViewTextBoxColumn14.Name = "GTotal";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.FieldName = "FactorPactado";
            gridViewTextBoxColumn15.FormatString = "{0:N4}";
            gridViewTextBoxColumn15.HeaderText = "Fac.\r\nPactado";
            gridViewTextBoxColumn15.Name = "FactorPactado";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.WrapText = true;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "PorCobrarPactado";
            gridViewTextBoxColumn16.FormatString = "{0:N2}";
            gridViewTextBoxColumn16.HeaderText = "X Cobrar\r\nPactado";
            gridViewTextBoxColumn16.Name = "PorCobrarPactadoD";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn16.WrapText = true;
            gridViewTextBoxColumn17.FieldName = "DescuentoTipo";
            gridViewTextBoxColumn17.HeaderText = "Tipo Desc. Aplicado";
            gridViewTextBoxColumn17.Name = "DescuentoTipo";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 120;
            gridViewTextBoxColumn17.WrapText = true;
            gridViewTextBoxColumn18.DataType = typeof(decimal);
            gridViewTextBoxColumn18.FieldName = "DescuentoFactor";
            gridViewTextBoxColumn18.FormatString = "{0:N4}";
            gridViewTextBoxColumn18.HeaderText = "Fac.\r\nDesc.";
            gridViewTextBoxColumn18.Name = "DescuentoFactor";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.DataType = typeof(decimal);
            gridViewTextBoxColumn19.FieldName = "DescuentoTotal";
            gridViewTextBoxColumn19.FormatString = "{0:n}";
            gridViewTextBoxColumn19.HeaderText = "Desc.\r\nActual";
            gridViewTextBoxColumn19.Name = "DescuentoTotal";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn19.Width = 85;
            gridViewTextBoxColumn19.WrapText = true;
            gridViewTextBoxColumn20.DataType = typeof(decimal);
            gridViewTextBoxColumn20.FieldName = "PorCobrar";
            gridViewTextBoxColumn20.FormatString = "{0:N2}";
            gridViewTextBoxColumn20.HeaderText = "Por \r\nCobrar";
            gridViewTextBoxColumn20.Name = "PorCobrar";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn20.Width = 80;
            gridViewTextBoxColumn20.WrapText = true;
            gridViewTextBoxColumn21.DataType = typeof(decimal);
            gridViewTextBoxColumn21.FieldName = "Acumulado";
            gridViewTextBoxColumn21.FormatString = "{0:n}";
            gridViewTextBoxColumn21.HeaderText = "Cobrado";
            gridViewTextBoxColumn21.Name = "Acumulado";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.FieldName = "TotalNotaDescuento";
            gridViewTextBoxColumn22.FormatString = "{0:N2}";
            gridViewTextBoxColumn22.HeaderText = "Nota. \r\nDesc.";
            gridViewTextBoxColumn22.Name = "TotalNotaDescuento";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 75;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.FieldName = "TotalCobrado";
            gridViewTextBoxColumn23.FormatString = "{0:N2}";
            gridViewTextBoxColumn23.HeaderText = "Real\r\nCobrado";
            gridViewTextBoxColumn23.Name = "TotalCobrado";
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 75;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject1.ConditionType = Telerik.WinControls.UI.ConditionTypes.Less;
            conditionalFormattingObject1.Name = "Saldo";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewTextBoxColumn24.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.FieldName = "Saldo";
            gridViewTextBoxColumn24.FormatString = "{0:n}";
            gridViewTextBoxColumn24.HeaderText = "Saldo";
            gridViewTextBoxColumn24.Name = "Saldo";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.Width = 85;
            gridViewTextBoxColumn24.WrapText = true;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.FieldName = "FactorRealCobrado";
            gridViewTextBoxColumn25.FormatString = "{0:N4}";
            gridViewTextBoxColumn25.HeaderText = "F. Real\r\nCobrado";
            gridViewTextBoxColumn25.Name = "FactorRealCobrado";
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn26.FieldName = "ClaveMoneda";
            gridViewTextBoxColumn26.HeaderText = "Moneda";
            gridViewTextBoxColumn26.Name = "ClaveMoneda";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.IsVisible = false;
            gridViewTextBoxColumn27.DataType = typeof(int);
            gridViewTextBoxColumn27.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard;
            gridViewTextBoxColumn27.FieldName = "DiasTranscurridos";
            gridViewTextBoxColumn27.FormatString = "{0:N0}";
            gridViewTextBoxColumn27.HeaderText = "Días\r\nTrans.";
            gridViewTextBoxColumn27.Name = "DiasTranscurridos";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn28.DataType = typeof(int);
            gridViewTextBoxColumn28.FieldName = "DiasUltCobro";
            gridViewTextBoxColumn28.FormatString = "{0:N0}";
            gridViewTextBoxColumn28.HeaderText = "Días Ult.\r\nCobro";
            gridViewTextBoxColumn28.Name = "DiasUltCobro";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn29.DataType = typeof(int);
            gridViewTextBoxColumn29.FieldName = "DiasCobranza";
            gridViewTextBoxColumn29.FormatString = "{0:N0}";
            gridViewTextBoxColumn29.HeaderText = "Días\r\nCobranza";
            gridViewTextBoxColumn29.Name = "DiasCobranza";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn30.FieldName = "Comision";
            gridViewTextBoxColumn30.HeaderText = "Tipo de comisión";
            gridViewTextBoxColumn30.Name = "Comision";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.Width = 150;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.FieldName = "ComisionFactor";
            gridViewTextBoxColumn31.FormatString = "{0:N4}";
            gridViewTextBoxColumn31.HeaderText = "Fac.\r\nComisión";
            gridViewTextBoxColumn31.Name = "ComisionFactor";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 65;
            gridViewTextBoxColumn32.DataType = typeof(decimal);
            gridViewTextBoxColumn32.FieldName = "ComisionFactorActualizado";
            gridViewTextBoxColumn32.FormatString = "{0:N4}";
            gridViewTextBoxColumn32.HeaderText = "Fac.\r\nActualizado";
            gridViewTextBoxColumn32.Name = "ComisionFactorActualizado";
            gridViewTextBoxColumn32.ReadOnly = true;
            gridViewTextBoxColumn32.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn33.DataType = typeof(decimal);
            gridViewTextBoxColumn33.FieldName = "ComisionPorPagar";
            gridViewTextBoxColumn33.FormatString = "{0:N2}";
            gridViewTextBoxColumn33.HeaderText = "Com.\r\nActualizada";
            gridViewTextBoxColumn33.Name = "ComisionPorPagar";
            gridViewTextBoxColumn33.ReadOnly = true;
            gridViewTextBoxColumn33.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Blue;
            expressionFormattingObject1.Expression = "ComisionPagada > ComisionPorPagar";
            expressionFormattingObject1.Name = "Pagar";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.CellForeColor = System.Drawing.Color.Red;
            expressionFormattingObject2.Expression = "ComisionPagada < ComisionPorPagar";
            expressionFormattingObject2.Name = "Pagada";
            expressionFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            gridViewTextBoxColumn34.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn34.ConditionalFormattingObjectList.Add(expressionFormattingObject2);
            gridViewTextBoxColumn34.DataType = typeof(decimal);
            gridViewTextBoxColumn34.FieldName = "ComisionPagada";
            gridViewTextBoxColumn34.FormatString = "{0:N2}";
            gridViewTextBoxColumn34.HeaderText = "Com. \r\nPagar";
            gridViewTextBoxColumn34.Name = "ComisionPagada";
            gridViewTextBoxColumn34.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn35.FieldName = "Nota";
            gridViewTextBoxColumn35.HeaderText = "Observaciones";
            gridViewTextBoxColumn35.IsVisible = false;
            gridViewTextBoxColumn35.Name = "Nota";
            gridViewTextBoxColumn35.ReadOnly = true;
            gridViewTextBoxColumn35.VisibleInColumnChooser = false;
            gridViewTextBoxColumn35.Width = 150;
            gridViewTextBoxColumn36.FieldName = "Creo";
            gridViewTextBoxColumn36.HeaderText = "Creó";
            gridViewTextBoxColumn36.IsVisible = false;
            gridViewTextBoxColumn36.Name = "Creo";
            gridViewTextBoxColumn36.ReadOnly = true;
            gridViewTextBoxColumn36.VisibleInColumnChooser = false;
            gridViewTextBoxColumn36.Width = 75;
            gridViewTextBoxColumn37.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn37.FieldName = "FechaNuevo";
            gridViewTextBoxColumn37.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn37.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn37.IsVisible = false;
            gridViewTextBoxColumn37.Name = "FechaNuevo";
            gridViewTextBoxColumn37.ReadOnly = true;
            gridViewTextBoxColumn37.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn37.VisibleInColumnChooser = false;
            gridViewTextBoxColumn37.Width = 75;
            gridViewTextBoxColumn38.FieldName = "Modifica";
            gridViewTextBoxColumn38.HeaderText = "Modifica";
            gridViewTextBoxColumn38.Name = "Modifica";
            gridViewTextBoxColumn38.ReadOnly = true;
            gridViewTextBoxColumn38.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn38.Width = 75;
            gridViewDateTimeColumn6.FieldName = "FechaModifica";
            gridViewDateTimeColumn6.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn6.HeaderText = "Fec. Mod.";
            gridViewDateTimeColumn6.IsVisible = false;
            gridViewDateTimeColumn6.Name = "FechaModifica";
            gridViewDateTimeColumn6.ReadOnly = true;
            gridViewDateTimeColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn6.Width = 75;
            this.GridRemisiones.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewDateTimeColumn3,
            gridViewDateTimeColumn4,
            gridViewDateTimeColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewDateTimeColumn6});
            this.GridRemisiones.MasterTemplate.ShowRowHeaderColumn = false;
            this.GridRemisiones.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridAutorizacion,
            this.gridRelacion,
            this.gridCobranza,
            this.gridConceptos});
            this.GridRemisiones.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.GridRemisiones.Name = "GridRemisiones";
            this.GridRemisiones.ShowGroupPanel = false;
            this.GridRemisiones.Size = new System.Drawing.Size(1134, 306);
            this.GridRemisiones.TabIndex = 2;
            this.GridRemisiones.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridData_CellBeginEdit);
            this.GridRemisiones.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellEndEdit);
            this.GridRemisiones.CellValidating += new Telerik.WinControls.UI.CellValidatingEventHandler(this.GridData_CellValidating);
            this.GridRemisiones.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.GridData_ContextMenuOpening);
            // 
            // gridAutorizacion
            // 
            this.gridAutorizacion.Caption = "Autorización";
            gridViewComboBoxColumn2.FieldName = "IdAfectacion";
            gridViewComboBoxColumn2.HeaderText = "Afectación";
            gridViewComboBoxColumn2.Name = "IdAfectacion";
            gridViewComboBoxColumn2.Width = 75;
            gridViewComboBoxColumn3.FieldName = "IdStatus";
            gridViewComboBoxColumn3.HeaderText = "Status";
            gridViewComboBoxColumn3.Name = "IdStatus";
            gridViewComboBoxColumn3.Width = 75;
            gridViewTextBoxColumn39.FieldName = "Total";
            gridViewTextBoxColumn39.FormatString = "{0:N2}";
            gridViewTextBoxColumn39.HeaderText = "Total";
            gridViewTextBoxColumn39.Name = "Total";
            gridViewTextBoxColumn39.Width = 75;
            gridViewTextBoxColumn40.FieldName = "Nota";
            gridViewTextBoxColumn40.HeaderText = "Observaciones";
            gridViewTextBoxColumn40.Name = "Nota";
            gridViewTextBoxColumn40.Width = 250;
            gridViewDateTimeColumn7.FieldName = "FechaNuevo";
            gridViewDateTimeColumn7.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn7.Name = "FechaNuevo";
            gridViewDateTimeColumn7.Width = 75;
            gridViewTextBoxColumn41.FieldName = "Creo";
            gridViewTextBoxColumn41.HeaderText = "Creó";
            gridViewTextBoxColumn41.Name = "Creo";
            gridViewTextBoxColumn41.Width = 75;
            this.gridAutorizacion.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewComboBoxColumn2,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40,
            gridViewDateTimeColumn7,
            gridViewTextBoxColumn41});
            this.gridAutorizacion.ViewDefinition = tableViewDefinition1;
            // 
            // gridRelacion
            // 
            this.gridRelacion.Caption = "Remisión Relacionada";
            gridViewTextBoxColumn42.FieldName = "Folio";
            gridViewTextBoxColumn42.HeaderText = "Folio";
            gridViewTextBoxColumn42.Name = "Folio";
            gridViewTextBoxColumn42.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn42.Width = 75;
            gridViewTextBoxColumn43.FieldName = "Serie";
            gridViewTextBoxColumn43.HeaderText = "Serie";
            gridViewTextBoxColumn43.Name = "Serie";
            gridViewTextBoxColumn43.Width = 75;
            gridViewTextBoxColumn44.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn44.HeaderText = "Cliente";
            gridViewTextBoxColumn44.Name = "ReceptorNombre";
            gridViewTextBoxColumn44.Width = 200;
            gridViewTextBoxColumn45.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn45.FieldName = "FechaEmision";
            gridViewTextBoxColumn45.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn45.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn45.Name = "FechaEmision";
            gridViewTextBoxColumn45.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn45.Width = 85;
            gridViewTextBoxColumn46.DataType = typeof(decimal);
            gridViewTextBoxColumn46.FieldName = "Total";
            gridViewTextBoxColumn46.FormatString = "{0:N2}";
            gridViewTextBoxColumn46.HeaderText = "Total";
            gridViewTextBoxColumn46.Name = "Total";
            gridViewTextBoxColumn46.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn46.Width = 85;
            gridViewTextBoxColumn47.FieldName = "IdDocumento";
            gridViewTextBoxColumn47.HeaderText = "IdDocumento";
            gridViewTextBoxColumn47.Name = "IdDocumento";
            gridViewTextBoxColumn47.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn47.Width = 240;
            gridViewTextBoxColumn48.FieldName = "Creo";
            gridViewTextBoxColumn48.HeaderText = "Creó";
            gridViewTextBoxColumn48.Name = "Creo";
            gridViewTextBoxColumn48.Width = 75;
            this.gridRelacion.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn42,
            gridViewTextBoxColumn43,
            gridViewTextBoxColumn44,
            gridViewTextBoxColumn45,
            gridViewTextBoxColumn46,
            gridViewTextBoxColumn47,
            gridViewTextBoxColumn48});
            this.gridRelacion.ViewDefinition = tableViewDefinition2;
            // 
            // gridCobranza
            // 
            this.gridCobranza.Caption = "Cobranza";
            gridViewTextBoxColumn49.FieldName = "Identificador";
            gridViewTextBoxColumn49.HeaderText = "Identificador";
            gridViewTextBoxColumn49.Name = "Identificador";
            gridViewTextBoxColumn49.ReadOnly = true;
            gridViewTextBoxColumn49.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn49.Width = 75;
            gridViewComboBoxColumn4.FieldName = "Status";
            gridViewComboBoxColumn4.HeaderText = "Estado";
            gridViewComboBoxColumn4.Name = "Status";
            gridViewComboBoxColumn4.Width = 85;
            gridViewTextBoxColumn50.FieldName = "BeneficiarioT";
            gridViewTextBoxColumn50.HeaderText = "Persona";
            gridViewTextBoxColumn50.Name = "BeneficiarioT";
            gridViewTextBoxColumn50.ReadOnly = true;
            gridViewTextBoxColumn50.Width = 275;
            gridViewTextBoxColumn51.FieldName = "Concepto";
            gridViewTextBoxColumn51.HeaderText = "Concepto";
            gridViewTextBoxColumn51.Name = "Concepto";
            gridViewTextBoxColumn51.ReadOnly = true;
            gridViewTextBoxColumn51.Width = 240;
            gridViewTextBoxColumn52.FieldName = "ClaveFormaPago";
            gridViewTextBoxColumn52.HeaderText = "Clv. Pago";
            gridViewTextBoxColumn52.Name = "ClaveFormaPago";
            gridViewTextBoxColumn52.ReadOnly = true;
            gridViewTextBoxColumn52.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn53.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn53.FieldName = "FechaDocto";
            gridViewTextBoxColumn53.FormatString = "{0:ddd dd MMM yyyy}";
            gridViewTextBoxColumn53.HeaderText = "Fec. Doc./Pago";
            gridViewTextBoxColumn53.Name = "FechaDocto";
            gridViewTextBoxColumn53.ReadOnly = true;
            gridViewTextBoxColumn53.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn53.Width = 95;
            gridViewTextBoxColumn54.FieldName = "NumDocto";
            gridViewTextBoxColumn54.HeaderText = "No. Docto.";
            gridViewTextBoxColumn54.Name = "NumDocto";
            gridViewTextBoxColumn54.ReadOnly = true;
            gridViewTextBoxColumn54.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn54.Width = 85;
            gridViewTextBoxColumn55.FieldName = "ClaveBancoT";
            gridViewTextBoxColumn55.HeaderText = "Clv. Banco";
            gridViewTextBoxColumn55.Name = "ClaveBancoT";
            gridViewTextBoxColumn55.ReadOnly = true;
            gridViewTextBoxColumn55.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn56.FieldName = "SucursalT";
            gridViewTextBoxColumn56.HeaderText = "Sucursal";
            gridViewTextBoxColumn56.IsVisible = false;
            gridViewTextBoxColumn56.Name = "SucursalT";
            gridViewTextBoxColumn56.ReadOnly = true;
            gridViewTextBoxColumn56.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn56.Width = 85;
            gridViewTextBoxColumn57.FieldName = "CuentaCLABET";
            gridViewTextBoxColumn57.HeaderText = "CLABE";
            gridViewTextBoxColumn57.IsVisible = false;
            gridViewTextBoxColumn57.Name = "CuentaCLABET";
            gridViewTextBoxColumn57.ReadOnly = true;
            gridViewTextBoxColumn57.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn57.Width = 120;
            gridViewTextBoxColumn58.FieldName = "Referencia";
            gridViewTextBoxColumn58.HeaderText = "Referencia";
            gridViewTextBoxColumn58.Name = "Referencia";
            gridViewTextBoxColumn58.Width = 105;
            gridViewTextBoxColumn59.FieldName = "NumAutorizacion";
            gridViewTextBoxColumn59.HeaderText = "Autorización";
            gridViewTextBoxColumn59.Name = "NumAutorizacion";
            gridViewTextBoxColumn59.Width = 105;
            gridViewTextBoxColumn60.DataType = typeof(decimal);
            gridViewTextBoxColumn60.FieldName = "Abono1";
            gridViewTextBoxColumn60.FormatString = "{0:n2}";
            gridViewTextBoxColumn60.HeaderText = "Abono";
            gridViewTextBoxColumn60.IsVisible = false;
            gridViewTextBoxColumn60.Name = "Abono";
            gridViewTextBoxColumn60.ReadOnly = true;
            gridViewTextBoxColumn60.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn60.VisibleInColumnChooser = false;
            gridViewTextBoxColumn60.Width = 95;
            gridViewTextBoxColumn61.DataType = typeof(decimal);
            gridViewTextBoxColumn61.FieldName = "Cargo1";
            gridViewTextBoxColumn61.FormatString = "{0:n2}";
            gridViewTextBoxColumn61.HeaderText = "Importe";
            gridViewTextBoxColumn61.Name = "Cargo";
            gridViewTextBoxColumn61.ReadOnly = true;
            gridViewTextBoxColumn61.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn61.Width = 95;
            expressionFormattingObject3.ApplyToRow = true;
            expressionFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject3.CellForeColor = System.Drawing.Color.Red;
            expressionFormattingObject3.Expression = "Saldo <= 0";
            expressionFormattingObject3.Name = "NewCondition";
            expressionFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn62.ConditionalFormattingObjectList.Add(expressionFormattingObject3);
            gridViewTextBoxColumn62.DataType = typeof(decimal);
            gridViewTextBoxColumn62.FieldName = "Acumulado";
            gridViewTextBoxColumn62.FormatString = "{0:n2}";
            gridViewTextBoxColumn62.HeaderText = "Acumulado";
            gridViewTextBoxColumn62.IsVisible = false;
            gridViewTextBoxColumn62.Name = "Saldo";
            gridViewTextBoxColumn62.ReadOnly = true;
            gridViewTextBoxColumn62.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn62.VisibleInColumnChooser = false;
            gridViewTextBoxColumn62.Width = 85;
            gridViewTextBoxColumn63.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn63.FieldName = "FechaAplicacion";
            gridViewTextBoxColumn63.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn63.HeaderText = "Fec. Aplicación";
            gridViewTextBoxColumn63.Name = "FechaAplicacion";
            gridViewTextBoxColumn63.ReadOnly = true;
            gridViewTextBoxColumn63.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn63.Width = 85;
            gridViewTextBoxColumn64.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn64.FieldName = "FechaEmision";
            gridViewTextBoxColumn64.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn64.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn64.Name = "FechaEmision";
            gridViewTextBoxColumn64.ReadOnly = true;
            gridViewTextBoxColumn64.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn64.Width = 85;
            gridViewTextBoxColumn65.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn65.HeaderText = "Fec. Vence";
            gridViewTextBoxColumn65.IsVisible = false;
            gridViewTextBoxColumn65.Name = "column11";
            gridViewTextBoxColumn65.ReadOnly = true;
            gridViewTextBoxColumn65.Width = 85;
            gridViewTextBoxColumn66.FieldName = "Cancela";
            gridViewTextBoxColumn66.HeaderText = "Canceló";
            gridViewTextBoxColumn66.Name = "Cancela";
            gridViewTextBoxColumn66.ReadOnly = true;
            gridViewTextBoxColumn66.Width = 85;
            conditionalFormattingObject2.ApplyToRow = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.Name = "PorComprobar";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.TValue1 = "true";
            conditionalFormattingObject2.TValue2 = "true";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewCheckBoxColumn1.FieldName = "PorComprobar";
            gridViewCheckBoxColumn1.HeaderText = "Por Comprobar";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "PorComprobar";
            gridViewCheckBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn67.FieldName = "Creo";
            gridViewTextBoxColumn67.HeaderText = "Creó";
            gridViewTextBoxColumn67.Name = "Creo";
            gridViewTextBoxColumn67.ReadOnly = true;
            gridViewTextBoxColumn67.Width = 85;
            gridViewTextBoxColumn68.FieldName = "Nota";
            gridViewTextBoxColumn68.HeaderText = "Nota";
            gridViewTextBoxColumn68.IsVisible = false;
            gridViewTextBoxColumn68.Name = "Nota";
            gridViewTextBoxColumn68.Width = 200;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Silver;
            conditionalFormattingObject3.Name = "Afecta Saldo";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "FALSE";
            conditionalFormattingObject3.TValue2 = "0";
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewCheckBoxColumn2.FieldName = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn2.HeaderText = "Afecta Saldo";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            this.gridCobranza.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn49,
            gridViewComboBoxColumn4,
            gridViewTextBoxColumn50,
            gridViewTextBoxColumn51,
            gridViewTextBoxColumn52,
            gridViewTextBoxColumn53,
            gridViewTextBoxColumn54,
            gridViewTextBoxColumn55,
            gridViewTextBoxColumn56,
            gridViewTextBoxColumn57,
            gridViewTextBoxColumn58,
            gridViewTextBoxColumn59,
            gridViewTextBoxColumn60,
            gridViewTextBoxColumn61,
            gridViewTextBoxColumn62,
            gridViewTextBoxColumn63,
            gridViewTextBoxColumn64,
            gridViewTextBoxColumn65,
            gridViewTextBoxColumn66,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn67,
            gridViewTextBoxColumn68,
            gridViewCheckBoxColumn2});
            this.gridCobranza.ViewDefinition = tableViewDefinition3;
            // 
            // gridConceptos
            // 
            this.gridConceptos.Caption = "Conceptos";
            gridViewTextBoxColumn69.DataType = typeof(decimal);
            gridViewTextBoxColumn69.FieldName = "Cantidad";
            gridViewTextBoxColumn69.FormatString = "{0:N2}";
            gridViewTextBoxColumn69.HeaderText = "Cantidad";
            gridViewTextBoxColumn69.Name = "Cantidad";
            gridViewTextBoxColumn69.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn69.Width = 65;
            gridViewTextBoxColumn70.FieldName = "Unidad";
            gridViewTextBoxColumn70.HeaderText = "Unidad";
            gridViewTextBoxColumn70.Name = "Unidad";
            gridViewTextBoxColumn71.FieldName = "Catalogo";
            gridViewTextBoxColumn71.HeaderText = "Catálogo";
            gridViewTextBoxColumn71.Name = "Catalogo";
            gridViewTextBoxColumn71.Width = 100;
            gridViewTextBoxColumn72.FieldName = "Producto";
            gridViewTextBoxColumn72.HeaderText = "Producto";
            gridViewTextBoxColumn72.Name = "Producto";
            gridViewTextBoxColumn72.Width = 110;
            gridViewTextBoxColumn73.FieldName = "Marca";
            gridViewTextBoxColumn73.HeaderText = "Marca";
            gridViewTextBoxColumn73.Name = "Marca";
            gridViewTextBoxColumn73.Width = 100;
            gridViewTextBoxColumn74.FieldName = "Descripcion";
            gridViewTextBoxColumn74.HeaderText = "Descripción";
            gridViewTextBoxColumn74.Name = "Descripcion";
            gridViewTextBoxColumn74.Width = 150;
            gridViewTextBoxColumn75.FieldName = "Especificacion";
            gridViewTextBoxColumn75.HeaderText = "Especificación";
            gridViewTextBoxColumn75.Name = "Especificacion";
            gridViewTextBoxColumn75.Width = 100;
            gridViewTextBoxColumn76.FieldName = "Tamanio";
            gridViewTextBoxColumn76.HeaderText = "Tamaño";
            gridViewTextBoxColumn76.Name = "Tamanio";
            gridViewTextBoxColumn76.Width = 100;
            gridViewTextBoxColumn77.DataType = typeof(decimal);
            gridViewTextBoxColumn77.FieldName = "Unitario";
            gridViewTextBoxColumn77.FormatString = "{0:N2}";
            gridViewTextBoxColumn77.HeaderText = "P. Unitario";
            gridViewTextBoxColumn77.Name = "Unitario";
            gridViewTextBoxColumn77.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn77.Width = 75;
            gridViewTextBoxColumn78.DataType = typeof(decimal);
            gridViewTextBoxColumn78.FieldName = "SubTotal";
            gridViewTextBoxColumn78.FormatString = "{0:N2}";
            gridViewTextBoxColumn78.HeaderText = "Importe 1";
            gridViewTextBoxColumn78.IsVisible = false;
            gridViewTextBoxColumn78.Name = "SubTotal";
            gridViewTextBoxColumn78.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn78.Width = 75;
            gridViewTextBoxColumn79.DataType = typeof(decimal);
            gridViewTextBoxColumn79.FieldName = "Descuento";
            gridViewTextBoxColumn79.FormatString = "{0:N2}";
            gridViewTextBoxColumn79.HeaderText = "Descuento";
            gridViewTextBoxColumn79.Name = "Descuento";
            gridViewTextBoxColumn79.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn79.Width = 75;
            gridViewTextBoxColumn80.DataType = typeof(decimal);
            gridViewTextBoxColumn80.FieldName = "Importe";
            gridViewTextBoxColumn80.FormatString = "{0:N2}";
            gridViewTextBoxColumn80.HeaderText = "Sub Total";
            gridViewTextBoxColumn80.IsVisible = false;
            gridViewTextBoxColumn80.Name = "Importe";
            gridViewTextBoxColumn80.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn80.Width = 75;
            gridViewTextBoxColumn81.DataType = typeof(decimal);
            gridViewTextBoxColumn81.FieldName = "TasaIVA";
            gridViewTextBoxColumn81.FormatString = "{0:P0}";
            gridViewTextBoxColumn81.HeaderText = "% IVA";
            gridViewTextBoxColumn81.Name = "TasaIVA";
            gridViewTextBoxColumn81.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn82.DataType = typeof(decimal);
            gridViewTextBoxColumn82.FieldName = "TasladoIVA";
            gridViewTextBoxColumn82.FormatString = "{0:N2}";
            gridViewTextBoxColumn82.HeaderText = "T. IVA";
            gridViewTextBoxColumn82.Name = "TasladoIVA";
            gridViewTextBoxColumn82.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn82.Width = 75;
            gridViewTextBoxColumn83.DataType = typeof(decimal);
            gridViewTextBoxColumn83.FieldName = "Total";
            gridViewTextBoxColumn83.FormatString = "{0:N2}";
            gridViewTextBoxColumn83.HeaderText = "Sub Total";
            gridViewTextBoxColumn83.Name = "Total";
            gridViewTextBoxColumn83.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn83.Width = 75;
            gridViewTextBoxColumn84.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn84.HeaderText = "Identificador";
            gridViewTextBoxColumn84.Name = "NoIdentificacion";
            gridViewTextBoxColumn84.Width = 100;
            this.gridConceptos.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn69,
            gridViewTextBoxColumn70,
            gridViewTextBoxColumn71,
            gridViewTextBoxColumn72,
            gridViewTextBoxColumn73,
            gridViewTextBoxColumn74,
            gridViewTextBoxColumn75,
            gridViewTextBoxColumn76,
            gridViewTextBoxColumn77,
            gridViewTextBoxColumn78,
            gridViewTextBoxColumn79,
            gridViewTextBoxColumn80,
            gridViewTextBoxColumn81,
            gridViewTextBoxColumn82,
            gridViewTextBoxColumn83,
            gridViewTextBoxColumn84});
            this.gridConceptos.ViewDefinition = tableViewDefinition4;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.GridMovimientos);
            this.splitPanel2.Location = new System.Drawing.Point(0, 310);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1134, 306);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // GridMovimientos
            // 
            this.GridMovimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridMovimientos.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GridMovimientos.MasterTemplate.AllowAddNewRow = false;
            this.GridMovimientos.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn85.FieldName = "Identificador";
            gridViewTextBoxColumn85.HeaderText = "Identificador";
            gridViewTextBoxColumn85.Name = "Identificador";
            gridViewTextBoxColumn85.ReadOnly = true;
            gridViewTextBoxColumn85.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn85.Width = 75;
            gridViewComboBoxColumn5.FieldName = "Status";
            gridViewComboBoxColumn5.HeaderText = "Estado";
            gridViewComboBoxColumn5.Name = "Status";
            gridViewComboBoxColumn5.Width = 85;
            gridViewTextBoxColumn86.FieldName = "BeneficiarioT";
            gridViewTextBoxColumn86.HeaderText = "Persona";
            gridViewTextBoxColumn86.Name = "BeneficiarioT";
            gridViewTextBoxColumn86.ReadOnly = true;
            gridViewTextBoxColumn86.Width = 275;
            gridViewTextBoxColumn87.FieldName = "Concepto";
            gridViewTextBoxColumn87.HeaderText = "Concepto";
            gridViewTextBoxColumn87.Name = "Concepto";
            gridViewTextBoxColumn87.ReadOnly = true;
            gridViewTextBoxColumn87.Width = 240;
            gridViewTextBoxColumn88.FieldName = "ClaveFormaPago";
            gridViewTextBoxColumn88.HeaderText = "Clv. Pago";
            gridViewTextBoxColumn88.Name = "ClaveFormaPago";
            gridViewTextBoxColumn88.ReadOnly = true;
            gridViewTextBoxColumn88.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn89.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn89.FieldName = "FechaDocto";
            gridViewTextBoxColumn89.FormatString = "{0:ddd dd MMM yyyy}";
            gridViewTextBoxColumn89.HeaderText = "Fec. Doc./Pago";
            gridViewTextBoxColumn89.Name = "FechaDocto";
            gridViewTextBoxColumn89.ReadOnly = true;
            gridViewTextBoxColumn89.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn89.Width = 95;
            gridViewTextBoxColumn90.FieldName = "NumDocto";
            gridViewTextBoxColumn90.HeaderText = "No. Docto.";
            gridViewTextBoxColumn90.Name = "NumDocto";
            gridViewTextBoxColumn90.ReadOnly = true;
            gridViewTextBoxColumn90.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn90.Width = 85;
            gridViewTextBoxColumn91.FieldName = "ClaveBancoT";
            gridViewTextBoxColumn91.HeaderText = "Clv. Banco";
            gridViewTextBoxColumn91.Name = "ClaveBancoT";
            gridViewTextBoxColumn91.ReadOnly = true;
            gridViewTextBoxColumn91.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn92.FieldName = "SucursalT";
            gridViewTextBoxColumn92.HeaderText = "Sucursal";
            gridViewTextBoxColumn92.IsVisible = false;
            gridViewTextBoxColumn92.Name = "SucursalT";
            gridViewTextBoxColumn92.ReadOnly = true;
            gridViewTextBoxColumn92.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn92.Width = 85;
            gridViewTextBoxColumn93.FieldName = "CuentaCLABET";
            gridViewTextBoxColumn93.HeaderText = "CLABE";
            gridViewTextBoxColumn93.IsVisible = false;
            gridViewTextBoxColumn93.Name = "CuentaCLABET";
            gridViewTextBoxColumn93.ReadOnly = true;
            gridViewTextBoxColumn93.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn93.Width = 120;
            gridViewTextBoxColumn94.FieldName = "Referencia";
            gridViewTextBoxColumn94.HeaderText = "Referencia";
            gridViewTextBoxColumn94.Name = "Referencia";
            gridViewTextBoxColumn94.Width = 105;
            gridViewTextBoxColumn95.FieldName = "NumAutorizacion";
            gridViewTextBoxColumn95.HeaderText = "Autorización";
            gridViewTextBoxColumn95.Name = "NumAutorizacion";
            gridViewTextBoxColumn95.Width = 105;
            gridViewTextBoxColumn96.DataType = typeof(decimal);
            gridViewTextBoxColumn96.FieldName = "Abono1";
            gridViewTextBoxColumn96.FormatString = "{0:n2}";
            gridViewTextBoxColumn96.HeaderText = "Abono";
            gridViewTextBoxColumn96.IsVisible = false;
            gridViewTextBoxColumn96.Name = "Abono";
            gridViewTextBoxColumn96.ReadOnly = true;
            gridViewTextBoxColumn96.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn96.VisibleInColumnChooser = false;
            gridViewTextBoxColumn96.Width = 95;
            gridViewTextBoxColumn97.DataType = typeof(decimal);
            gridViewTextBoxColumn97.FieldName = "Cargo1";
            gridViewTextBoxColumn97.FormatString = "{0:n2}";
            gridViewTextBoxColumn97.HeaderText = "Importe";
            gridViewTextBoxColumn97.Name = "Cargo";
            gridViewTextBoxColumn97.ReadOnly = true;
            gridViewTextBoxColumn97.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn97.Width = 95;
            expressionFormattingObject4.ApplyToRow = true;
            expressionFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject4.CellForeColor = System.Drawing.Color.Red;
            expressionFormattingObject4.Expression = "Saldo <= 0";
            expressionFormattingObject4.Name = "NewCondition";
            expressionFormattingObject4.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn98.ConditionalFormattingObjectList.Add(expressionFormattingObject4);
            gridViewTextBoxColumn98.DataType = typeof(decimal);
            gridViewTextBoxColumn98.FieldName = "Acumulado";
            gridViewTextBoxColumn98.FormatString = "{0:n2}";
            gridViewTextBoxColumn98.HeaderText = "Acumulado";
            gridViewTextBoxColumn98.IsVisible = false;
            gridViewTextBoxColumn98.Name = "Saldo";
            gridViewTextBoxColumn98.ReadOnly = true;
            gridViewTextBoxColumn98.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn98.VisibleInColumnChooser = false;
            gridViewTextBoxColumn98.Width = 85;
            gridViewTextBoxColumn99.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn99.FieldName = "FechaAplicacion";
            gridViewTextBoxColumn99.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn99.HeaderText = "Fec. Aplicación";
            gridViewTextBoxColumn99.Name = "FechaAplicacion";
            gridViewTextBoxColumn99.ReadOnly = true;
            gridViewTextBoxColumn99.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn99.Width = 85;
            gridViewTextBoxColumn100.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn100.FieldName = "FechaEmision";
            gridViewTextBoxColumn100.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn100.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn100.Name = "FechaEmision";
            gridViewTextBoxColumn100.ReadOnly = true;
            gridViewTextBoxColumn100.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn100.Width = 85;
            gridViewTextBoxColumn101.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn101.HeaderText = "Fec. Vence";
            gridViewTextBoxColumn101.IsVisible = false;
            gridViewTextBoxColumn101.Name = "column11";
            gridViewTextBoxColumn101.ReadOnly = true;
            gridViewTextBoxColumn101.Width = 85;
            gridViewTextBoxColumn102.FieldName = "Cancela";
            gridViewTextBoxColumn102.HeaderText = "Canceló";
            gridViewTextBoxColumn102.Name = "Cancela";
            gridViewTextBoxColumn102.ReadOnly = true;
            gridViewTextBoxColumn102.Width = 85;
            conditionalFormattingObject4.ApplyToRow = true;
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.Name = "PorComprobar";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.TValue1 = "true";
            conditionalFormattingObject4.TValue2 = "true";
            gridViewCheckBoxColumn3.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewCheckBoxColumn3.FieldName = "PorComprobar";
            gridViewCheckBoxColumn3.HeaderText = "Por Comprobar";
            gridViewCheckBoxColumn3.IsVisible = false;
            gridViewCheckBoxColumn3.Name = "PorComprobar";
            gridViewCheckBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn103.FieldName = "Creo";
            gridViewTextBoxColumn103.HeaderText = "Creó";
            gridViewTextBoxColumn103.Name = "Creo";
            gridViewTextBoxColumn103.ReadOnly = true;
            gridViewTextBoxColumn103.Width = 85;
            gridViewTextBoxColumn104.FieldName = "Nota";
            gridViewTextBoxColumn104.HeaderText = "Nota";
            gridViewTextBoxColumn104.IsVisible = false;
            gridViewTextBoxColumn104.Name = "Nota";
            gridViewTextBoxColumn104.Width = 200;
            conditionalFormattingObject5.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject5.CellForeColor = System.Drawing.Color.Silver;
            conditionalFormattingObject5.Name = "Afecta Saldo";
            conditionalFormattingObject5.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject5.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject5.TValue1 = "FALSE";
            conditionalFormattingObject5.TValue2 = "0";
            gridViewCheckBoxColumn4.ConditionalFormattingObjectList.Add(conditionalFormattingObject5);
            gridViewCheckBoxColumn4.FieldName = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn4.HeaderText = "Afecta Saldo";
            gridViewCheckBoxColumn4.IsVisible = false;
            gridViewCheckBoxColumn4.Name = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn4.ReadOnly = true;
            gridViewCheckBoxColumn4.VisibleInColumnChooser = false;
            this.GridMovimientos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn85,
            gridViewComboBoxColumn5,
            gridViewTextBoxColumn86,
            gridViewTextBoxColumn87,
            gridViewTextBoxColumn88,
            gridViewTextBoxColumn89,
            gridViewTextBoxColumn90,
            gridViewTextBoxColumn91,
            gridViewTextBoxColumn92,
            gridViewTextBoxColumn93,
            gridViewTextBoxColumn94,
            gridViewTextBoxColumn95,
            gridViewTextBoxColumn96,
            gridViewTextBoxColumn97,
            gridViewTextBoxColumn98,
            gridViewTextBoxColumn99,
            gridViewTextBoxColumn100,
            gridViewTextBoxColumn101,
            gridViewTextBoxColumn102,
            gridViewCheckBoxColumn3,
            gridViewTextBoxColumn103,
            gridViewTextBoxColumn104,
            gridViewCheckBoxColumn4});
            this.GridMovimientos.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.GridMovimientos.Name = "GridMovimientos";
            this.GridMovimientos.ShowGroupPanel = false;
            this.GridMovimientos.Size = new System.Drawing.Size(1134, 306);
            this.GridMovimientos.TabIndex = 3;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            // 
            // MenuContextual
            // 
            this.MenuContextual.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ContextMenuSeleccion,
            this.ContextMenuCopiar,
            this.ContextMenuCrearRecibo});
            // 
            // ContextMenuSeleccion
            // 
            this.ContextMenuSeleccion.Name = "ContextMenuSeleccion";
            this.ContextMenuSeleccion.Text = "Selección múltiple";
            this.ContextMenuSeleccion.UseCompatibleTextRendering = false;
            this.ContextMenuSeleccion.Click += new System.EventHandler(this.ContextMenuSeleccion_Click);
            // 
            // ContextMenuCopiar
            // 
            this.ContextMenuCopiar.Name = "ContextMenuCopiar";
            this.ContextMenuCopiar.Text = "Copiar";
            this.ContextMenuCopiar.UseCompatibleTextRendering = false;
            this.ContextMenuCopiar.Click += new System.EventHandler(this.ContextMenuCopiar_Click);
            // 
            // ContextMenuCrearRecibo
            // 
            this.ContextMenuCrearRecibo.Name = "ContextMenuCrearRecibo";
            this.ContextMenuCrearRecibo.Text = "Crear recibo de comisión";
            this.ContextMenuCrearRecibo.Click += new System.EventHandler(this.ContextMenuCrearRecibo_Click);
            // 
            // TComision
            // 
            this.TComision.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComision.Location = new System.Drawing.Point(0, 0);
            this.TComision.Name = "TComision";
            this.TComision.Size = new System.Drawing.Size(1134, 30);
            this.TComision.TabIndex = 1;
            // 
            // ComisionRemisionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 646);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TComision);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComisionRemisionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Comisión por Remisiones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComisionRemisionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridRemisiones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRemisiones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutorizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCobranza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridMovimientos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridMovimientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        protected internal Telerik.WinControls.UI.RadGridView GridMovimientos;
        private Telerik.WinControls.UI.RadContextMenu MenuContextual;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuSeleccion;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCopiar;
        private Telerik.WinControls.UI.RadMenuItem ContextMenuCrearRecibo;
        public Telerik.WinControls.UI.RadGridView GridRemisiones;
        private Telerik.WinControls.UI.GridViewTemplate gridAutorizacion;
        private Telerik.WinControls.UI.GridViewTemplate gridRelacion;
        private Telerik.WinControls.UI.GridViewTemplate gridCobranza;
        private Telerik.WinControls.UI.GridViewTemplate gridConceptos;
        private VendedorComisionToolBarControl TComision;
    }
}
