﻿namespace Jaeger.UI.Ventas.Forms {
    partial class EstadoCuentaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Bar = new Telerik.WinControls.UI.RadCommandBar();
            this.cboVendedores = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblPorStatus = new Telerik.WinControls.UI.CommandBarLabel();
            this.PorStatus = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.LabelDocumento = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostItem1 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ExportarExcel = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).BeginInit();
            this.Bar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Bar
            // 
            this.Bar.Controls.Add(this.cboVendedores);
            this.Bar.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bar.Location = new System.Drawing.Point(0, 0);
            this.Bar.Name = "Bar";
            this.Bar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.Bar.Size = new System.Drawing.Size(1118, 30);
            this.Bar.TabIndex = 5;
            // 
            // cboVendedores
            // 
            this.cboVendedores.AutoSizeDropDownToBestFit = true;
            this.cboVendedores.DisplayMember = "Nombre";
            // 
            // cboVendedores.NestedRadGridView
            // 
            this.cboVendedores.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboVendedores.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVendedores.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboVendedores.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboVendedores.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdDirectorio";
            gridViewTextBoxColumn1.HeaderText = "IdDirectorio";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdDirectorio";
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            this.cboVendedores.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.cboVendedores.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboVendedores.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboVendedores.EditorControl.Name = "NestedRadGridView";
            this.cboVendedores.EditorControl.ReadOnly = true;
            this.cboVendedores.EditorControl.ShowGroupPanel = false;
            this.cboVendedores.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboVendedores.EditorControl.TabIndex = 0;
            this.cboVendedores.Location = new System.Drawing.Point(266, 4);
            this.cboVendedores.Name = "cboVendedores";
            this.cboVendedores.Size = new System.Drawing.Size(229, 20);
            this.cboVendedores.TabIndex = 2;
            this.cboVendedores.TabStop = false;
            this.cboVendedores.ValueMember = "IdRlcdrc";
            // 
            // BarRowElement
            // 
            this.BarRowElement.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.BarRowElement.Text = "";
            this.BarRowElement.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElement.UseCompatibleTextRendering = false;
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblPorStatus,
            this.PorStatus,
            this.LabelDocumento,
            this.HostItem1,
            this.Separator2,
            this.Actualizar,
            this.Filtro,
            this.AutoSuma,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.commandBarStripElement1.Name = "ToolBar";
            // 
            // 
            // 
            this.commandBarStripElement1.OverflowButton.Enabled = false;
            this.commandBarStripElement1.StretchHorizontally = true;
            this.commandBarStripElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadCommandBarOverflowButton)(this.commandBarStripElement1.GetChildAt(2))).Enabled = false;
            // 
            // lblPorStatus
            // 
            this.lblPorStatus.DisplayName = "commandBarLabel1";
            this.lblPorStatus.Name = "lblPorStatus";
            this.lblPorStatus.Text = "Por Status";
            // 
            // PorStatus
            // 
            this.PorStatus.AutoCompleteDisplayMember = "Descripcion";
            this.PorStatus.AutoCompleteValueMember = "Id";
            this.PorStatus.DisplayMember = "Descripcion";
            this.PorStatus.DisplayName = "commandBarDropDownList1";
            this.PorStatus.DropDownAnimationEnabled = true;
            this.PorStatus.MaxDropDownItems = 0;
            this.PorStatus.Name = "PorStatus";
            this.PorStatus.Text = "";
            this.PorStatus.ValueMember = "Id";
            // 
            // LabelDocumento
            // 
            this.LabelDocumento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.LabelDocumento.DisplayName = "Etiqueta";
            this.LabelDocumento.Name = "LabelDocumento";
            this.LabelDocumento.Text = "Vendedor:";
            this.LabelDocumento.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.LabelDocumento.UseCompatibleTextRendering = false;
            // 
            // HostItem1
            // 
            this.HostItem1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostItem1.DisplayName = "Comprobantes";
            this.HostItem1.MinSize = new System.Drawing.Size(250, 15);
            this.HostItem1.Name = "HostItem1";
            this.HostItem1.Text = "Comprobantes";
            this.HostItem1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostItem1.UseCompatibleTextRendering = false;
            // 
            // Separator2
            // 
            this.Separator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.UseCompatibleTextRendering = false;
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.CPLite.Properties.Resources.refresh_16;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.UseCompatibleTextRendering = false;
            // 
            // Filtro
            // 
            this.Filtro.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.CPLite.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Filtro.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.UseCompatibleTextRendering = false;
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "Auto suma";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ExportarExcel});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ExportarExcel
            // 
            this.ExportarExcel.Name = "ExportarExcel";
            this.ExportarExcel.Text = "Exportar Excel";
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.CPLite.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(1118, 452);
            this.GridData.TabIndex = 2;
            // 
            // EstadoCuentaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 482);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.Bar);
            this.Name = "EstadoCuentaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Estado de Cuenta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EstadoCuentaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).EndInit();
            this.Bar.ResumeLayout(false);
            this.Bar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadCommandBar Bar;
        private Telerik.WinControls.UI.RadMultiColumnComboBox cboVendedores;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarLabel LabelDocumento;
        private Telerik.WinControls.UI.CommandBarHostItem HostItem1;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        private Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        private Telerik.WinControls.UI.CommandBarButton Cerrar;
        private Telerik.WinControls.UI.CommandBarButton Imprimir;
        private Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        public Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarLabel lblPorStatus;
        private Telerik.WinControls.UI.CommandBarDropDownList PorStatus;
        private Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        private Telerik.WinControls.UI.RadMenuItem ExportarExcel;
    }
}