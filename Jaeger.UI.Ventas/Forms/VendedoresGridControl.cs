﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Ventas.Builder;

namespace Jaeger.UI.Ventas.Forms {
    internal class VendedoresGridControl : UI.Common.Forms.GridStandarControl {
        public GridViewTemplate gridClientes = new GridViewTemplate();

        public VendedoresGridControl() : base() {
            this.Load += VendedoresGridControl_Load;
        }

        private void VendedoresGridControl_Load(object sender, EventArgs e) {
            IVendedoresGridBuilder view = new VendedoresGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            this.gridClientes.Columns.AddRange(view.Templetes().Clientes().Build());
            this.gridClientes.HierarchyDataProvider = new GridViewEventDataProvider(this.gridClientes);
        }
    }
}
