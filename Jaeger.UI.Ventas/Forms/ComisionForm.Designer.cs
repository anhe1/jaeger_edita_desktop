﻿
namespace Jaeger.UI.Ventas.Forms {
    partial class ComisionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.FechaFin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.VentaMaxima = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.VentaMinima = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.Orden = new Telerik.WinControls.UI.RadSpinEditor();
            this.Prioridad = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridDescuento = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridMulta = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentaMaxima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentaMinima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Orden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.FechaFin);
            this.radGroupBox1.Controls.Add(this.FechaInicio);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.VentaMaxima);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.VentaMinima);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.Orden);
            this.radGroupBox1.Controls.Add(this.Prioridad);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.Nota);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.Descripcion);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "General";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(850, 83);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.Text = "General";
            // 
            // FechaFin
            // 
            this.FechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaFin.Location = new System.Drawing.Point(610, 45);
            this.FechaFin.Name = "FechaFin";
            this.FechaFin.Size = new System.Drawing.Size(85, 20);
            this.FechaFin.TabIndex = 76;
            this.FechaFin.TabStop = false;
            this.FechaFin.Text = "18/10/2021";
            this.FechaFin.Value = new System.DateTime(2021, 10, 18, 22, 26, 17, 103);
            // 
            // FechaInicio
            // 
            this.FechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaInicio.Location = new System.Drawing.Point(610, 22);
            this.FechaInicio.Name = "FechaInicio";
            this.FechaInicio.Size = new System.Drawing.Size(85, 20);
            this.FechaInicio.TabIndex = 75;
            this.FechaInicio.TabStop = false;
            this.FechaInicio.Text = "18/10/2021";
            this.FechaInicio.Value = new System.DateTime(2021, 10, 18, 22, 26, 17, 103);
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(542, 46);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(46, 18);
            this.radLabel7.TabIndex = 74;
            this.radLabel7.Text = "Fec. Fin:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(542, 22);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(58, 18);
            this.radLabel8.TabIndex = 73;
            this.radLabel8.Text = "Fec. Inicio:";
            // 
            // VentaMaxima
            // 
            this.VentaMaxima.Location = new System.Drawing.Point(465, 45);
            this.VentaMaxima.Name = "VentaMaxima";
            this.VentaMaxima.Size = new System.Drawing.Size(69, 20);
            this.VentaMaxima.TabIndex = 72;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(397, 46);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(64, 18);
            this.radLabel6.TabIndex = 71;
            this.radLabel6.Text = "Venta Max.:";
            // 
            // VentaMinima
            // 
            this.VentaMinima.Location = new System.Drawing.Point(465, 21);
            this.VentaMinima.Name = "VentaMinima";
            this.VentaMinima.Size = new System.Drawing.Size(69, 20);
            this.VentaMinima.TabIndex = 70;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(397, 22);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(62, 18);
            this.radLabel5.TabIndex = 69;
            this.radLabel5.Text = "Venta Min.:";
            // 
            // Orden
            // 
            this.Orden.Location = new System.Drawing.Point(761, 45);
            this.Orden.Name = "Orden";
            this.Orden.Size = new System.Drawing.Size(55, 20);
            this.Orden.TabIndex = 68;
            this.Orden.TabStop = false;
            this.Orden.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Prioridad
            // 
            this.Prioridad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Prioridad.AutoSizeDropDownHeight = true;
            this.Prioridad.AutoSizeDropDownToBestFit = true;
            this.Prioridad.DisplayMember = "Descripcion";
            this.Prioridad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Prioridad.NestedRadGridView
            // 
            this.Prioridad.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Prioridad.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Prioridad.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Prioridad.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Prioridad.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Prioridad.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Prioridad.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.Prioridad.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.Prioridad.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Prioridad.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Prioridad.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Prioridad.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Prioridad.EditorControl.Name = "NestedRadGridView";
            this.Prioridad.EditorControl.ReadOnly = true;
            this.Prioridad.EditorControl.ShowGroupPanel = false;
            this.Prioridad.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Prioridad.EditorControl.TabIndex = 0;
            this.Prioridad.Location = new System.Drawing.Point(760, 22);
            this.Prioridad.Name = "Prioridad";
            this.Prioridad.Size = new System.Drawing.Size(78, 20);
            this.Prioridad.TabIndex = 67;
            this.Prioridad.TabStop = false;
            this.Prioridad.ValueMember = "Id";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(701, 22);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(54, 18);
            this.radLabel4.TabIndex = 66;
            this.radLabel4.Text = "Prioridad:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(701, 46);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(40, 18);
            this.radLabel2.TabIndex = 65;
            this.radLabel2.Text = "Orden:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(83, 45);
            this.Nota.MaxLength = 100;
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(310, 20);
            this.Nota.TabIndex = 3;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 46);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Nota:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 21);
            this.Descripcion.MaxLength = 128;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(310, 20);
            this.Descripcion.TabIndex = 1;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 22);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(67, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Descripción:";
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowAutorizar = false;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = false;
            this.TCatalogo.ShowExportarExcel = false;
            this.TCatalogo.ShowFiltro = false;
            this.TCatalogo.ShowGuardar = true;
            this.TCatalogo.ShowHerramientas = false;
            this.TCatalogo.ShowImagen = false;
            this.TCatalogo.ShowImprimir = false;
            this.TCatalogo.ShowNuevo = false;
            this.TCatalogo.ShowRemover = false;
            this.TCatalogo.Size = new System.Drawing.Size(850, 30);
            this.TCatalogo.TabIndex = 6;
            this.TCatalogo.Nuevo.Click += this.TCatalogo_Nuevo_Click;
            this.TCatalogo.Guardar.Click += this.TCatalogo_Guardar_Click;
            this.TCatalogo.Actualizar.Click += this.TCatalogo_Actualizar_Click;
            this.TCatalogo.Cerrar.Click += this.TCatalogo_Cerrar_Click;
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 113);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(850, 223);
            this.radSplitContainer2.TabIndex = 8;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.dataGridDescuento);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(423, 223);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // dataGridDescuento
            // 
            this.dataGridDescuento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridDescuento.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridDescuento.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn3.DataType = typeof(decimal);
            gridViewTextBoxColumn3.FieldName = "Inicio";
            gridViewTextBoxColumn3.FormatString = "{0:N4}";
            gridViewTextBoxColumn3.HeaderText = "Rango - 1";
            gridViewTextBoxColumn3.Name = "Inicio";
            gridViewTextBoxColumn3.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.DataType = typeof(decimal);
            gridViewTextBoxColumn4.FieldName = "Final";
            gridViewTextBoxColumn4.FormatString = "{0:N4}";
            gridViewTextBoxColumn4.HeaderText = "Rango - 2";
            gridViewTextBoxColumn4.Name = "Final";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.DataType = typeof(decimal);
            gridViewTextBoxColumn5.FieldName = "Factor";
            gridViewTextBoxColumn5.FormatString = "{0:N4}";
            gridViewTextBoxColumn5.HeaderText = "Factor";
            gridViewTextBoxColumn5.Name = "Factor";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Descripcion";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "FechaNuevo";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "FechaNuevo";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.FieldName = "Creo";
            gridViewTextBoxColumn8.HeaderText = "Creó";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "Creo";
            gridViewTextBoxColumn8.Width = 65;
            this.dataGridDescuento.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.dataGridDescuento.MasterTemplate.EnableFiltering = true;
            this.dataGridDescuento.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor1.PropertyName = "Inicio";
            this.dataGridDescuento.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.dataGridDescuento.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridDescuento.Name = "dataGridDescuento";
            this.dataGridDescuento.ShowGroupPanel = false;
            this.dataGridDescuento.Size = new System.Drawing.Size(423, 223);
            this.dataGridDescuento.TabIndex = 2;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.dataGridMulta);
            this.splitPanel4.Location = new System.Drawing.Point(427, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(423, 223);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // dataGridMulta
            // 
            this.dataGridMulta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridMulta.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridMulta.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Inicio";
            gridViewTextBoxColumn9.FormatString = "{0:N4}";
            gridViewTextBoxColumn9.HeaderText = "Rango - 1";
            gridViewTextBoxColumn9.Name = "Inicio";
            gridViewTextBoxColumn9.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Final";
            gridViewTextBoxColumn10.FormatString = "{0:N4}";
            gridViewTextBoxColumn10.HeaderText = "Rango - 2";
            gridViewTextBoxColumn10.Name = "Final";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "Factor";
            gridViewTextBoxColumn11.FormatString = "{0:N4}";
            gridViewTextBoxColumn11.HeaderText = "Factor";
            gridViewTextBoxColumn11.Name = "Factor";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            gridViewTextBoxColumn12.Width = 200;
            gridViewTextBoxColumn13.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn13.FieldName = "FechaNuevo";
            gridViewTextBoxColumn13.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn13.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "FechaNuevo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn14.FieldName = "Creo";
            gridViewTextBoxColumn14.HeaderText = "Creó";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "Creo";
            gridViewTextBoxColumn14.Width = 65;
            this.dataGridMulta.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.dataGridMulta.MasterTemplate.EnableFiltering = true;
            this.dataGridMulta.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor2.PropertyName = "Inicio";
            this.dataGridMulta.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.dataGridMulta.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.dataGridMulta.Name = "dataGridMulta";
            this.dataGridMulta.ShowGroupPanel = false;
            this.dataGridMulta.Size = new System.Drawing.Size(423, 223);
            this.dataGridMulta.TabIndex = 3;
            // 
            // ComisionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 336);
            this.Controls.Add(this.radSplitContainer2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.TCatalogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComisionForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Comisión";
            this.Load += new System.EventHandler(this.ComisionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentaMaxima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VentaMinima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Orden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Prioridad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadSpinEditor Orden;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Prioridad;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Common.Forms.ToolBarStandarControl TCatalogo;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox VentaMaxima;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox VentaMinima;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView dataGridDescuento;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadGridView dataGridMulta;
        private Telerik.WinControls.UI.RadDateTimePicker FechaInicio;
        private Telerik.WinControls.UI.RadDateTimePicker FechaFin;
    }
}