﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.Ventas.Forms {
    public partial class ComisionCatalogoForm : RadForm {
        protected IComisionService service;
        private BindingList<ComisionDetailModel> comisiones;
        private readonly RadMenuItem _registroInactivo = new RadMenuItem { Text = "Mostrar registros inactivos", CheckOnClick = true };
        private Domain.Base.ValueObjects.UIAction _permisos;

        public ComisionCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ComisionCatalogoForm_Load(object sender, EventArgs e) {
            this.dataGridCatalogo.Standard();
            this.dataGridDescuento.Standard();
            this.dataGridMulta.Standard();

            this.TCatalogo.Herramientas.Items.Add(_registroInactivo);

            this.TCatalogo.Nuevo.Enabled = this._permisos.Agregar;
            this.TCatalogo.Editar.Enabled = this._permisos.Editar;
            this.TCatalogo.Remover.Enabled = this._permisos.Remover;
        }

        private void TCatalogo_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new ComisionForm();
            _nuevo.ShowDialog(this);
        }

        private void TCatalogo_Editar_Click(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var _seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as ComisionDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Activo == true) {
                        var _editar = new ComisionForm(this.service, _seleccionado);
                        _editar.ShowDialog(this);
                    } else {
                        RadMessageBox.Show(this, "Properties.Resources.Msg_Comision_NoEditable", "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                }
            }
        }

        private void TCatalogo_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridCatalogo.CurrentRow != null) {
                var _seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as ComisionDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Activo == true) {
                        if (RadMessageBox.Show(this, "Properties.Resources.Msg_Comision_Remover", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                            using (var espera = new Waiting2Form(this.Remover)) {
                                espera.ShowDialog(this);
                            }
                        }
                    }
                }
            }
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            this.dataGridCatalogo.DataSource = this.comisiones;
            this.dataGridDescuento.DataSource = this.comisiones;
            this.dataGridMulta.DataSource = this.comisiones;
            this.dataGridDescuento.DataMember = "Descuento";
            this.dataGridMulta.DataMember = "Multa";
        }

        private void TCatalogo_Filtro_Click(object sender, EventArgs e) {
            this.dataGridCatalogo.ShowFilteringRow = this.TCatalogo.Filtro.ToggleState != ToggleState.On;
            if (this.dataGridCatalogo.ShowFilteringRow == false)
                this.dataGridCatalogo.FilterDescriptors.Clear();
        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region metodos privados
        private void Consultar() {
            this.comisiones = this.service.GetComisiones(!this._registroInactivo.IsChecked);
        }

        private void Remover() {
            var _seleccionado = this.dataGridCatalogo.CurrentRow.DataBoundItem as ComisionDetailModel;
            if (_seleccionado != null) {
                _seleccionado.Activo = false;
                _seleccionado = this.service.Save(_seleccionado);
            }
        }
        #endregion
    }
}
