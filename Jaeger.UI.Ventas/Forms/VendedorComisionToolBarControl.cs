﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Ventas.Forms {
    public partial class VendedorComisionToolBarControl : UserControl {

        public VendedorComisionToolBarControl() {
            InitializeComponent();
        }

        private void VendedorComisionToolBarControl_Load(object sender, EventArgs e) {
            this.HostItem1.HostedItem = this.cboVendedores.MultiColumnComboBoxElement;
            this.Ejercicio.HostedItem = this.SpinEjercicio.SpinElement;

            this.SpinEjercicio.Minimum = 2014;
            this.SpinEjercicio.Maximum = DateTime.Now.Year;
            this.SpinEjercicio.Value = DateTime.Now.Year;
        }

        public int GetEjercicio() {
            return int.Parse(this.SpinEjercicio.Value.ToString());
        }
    }
}
