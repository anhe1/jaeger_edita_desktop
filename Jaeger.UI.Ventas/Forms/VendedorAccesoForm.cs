﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;

namespace Jaeger.UI.Ventas.Forms {
    public partial class VendedorAccesoForm : RadForm {
        protected IVendedorService service;
        private Vendedor2DetailModel vendedor;

        public VendedorAccesoForm(Vendedor2DetailModel vendedor) {
            InitializeComponent();
            this.vendedor = vendedor;
        }

        private void VendedorAccesoForm_Load(object sender, EventArgs e) {
            this.service = new VendedorService();
            this.CreateBinding();
        }

        private void ButtonConfirmar_Click(object sender, EventArgs e) {
            this.ButtonConfirmar.Tag = false;
            using (var espera = new Common.Forms.Waiting2Form(this.Crear)) {
                espera.Text = "Espere un momento...";
                espera.ShowDialog(this);
            }
            if ((bool)this.ButtonConfirmar.Tag == false) {
                RadMessageBox.Show(this, "No fue posible crear el acceso, reporte el error al Adminsitrador", "Atención", MessageBoxButtons.OK);
            }
            this.ButtonCerrar.PerformClick();
        }

        private void ButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.txtClave.DataBindings.Clear();
            this.txtClave.DataBindings.Add("Text", this.vendedor, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNombre.DataBindings.Clear();
            this.txtNombre.DataBindings.Add("Text", this.vendedor, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Crear() {
            //this.ButtonConfirmar.Tag = this.service.Acceso(vendedor.IdDirectorio, this.txtConfirmar.Text);
        }
    }
}
