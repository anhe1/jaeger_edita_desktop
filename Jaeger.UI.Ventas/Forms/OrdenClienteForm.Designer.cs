﻿namespace Jaeger.UI.Ventas.Forms
{
    partial class OrdenClienteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewRatingColumn gridViewRatingColumn1 = new Telerik.WinControls.UI.GridViewRatingColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.panelGeneral = new Telerik.WinControls.UI.RadPanel();
            this.CboResidenciaFiscal = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.CboVendedor = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.dtpFechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpFechaPedido = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txbContacto = new Telerik.WinControls.UI.RadTextBox();
            this.txbNoPedido = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.cboCliente = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radSplitContainer = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanelConceptos = new Telerik.WinControls.UI.SplitPanel();
            this.radGridConceptos = new Telerik.WinControls.UI.RadGridView();
            this.splitPanelOrdenProduccion = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridOrdenP = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridDoctos = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.toolBarStandarControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.toolBarStandarControl2 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.toolBarStandarControl3 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.toolBarStandarControl4 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelGeneral)).BeginInit();
            this.panelGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNoPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer)).BeginInit();
            this.radSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanelConceptos)).BeginInit();
            this.splitPanelConceptos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridConceptos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanelOrdenProduccion)).BeginInit();
            this.splitPanelOrdenProduccion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridOrdenP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridOrdenP.MasterTemplate)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridDoctos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridDoctos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // panelGeneral
            // 
            this.panelGeneral.Controls.Add(this.CboResidenciaFiscal);
            this.panelGeneral.Controls.Add(this.CboVendedor);
            this.panelGeneral.Controls.Add(this.radCheckBox1);
            this.panelGeneral.Controls.Add(this.RadLabel13);
            this.panelGeneral.Controls.Add(this.radTextBox2);
            this.panelGeneral.Controls.Add(this.radLabel7);
            this.panelGeneral.Controls.Add(this.radTextBox3);
            this.panelGeneral.Controls.Add(this.radLabel9);
            this.panelGeneral.Controls.Add(this.radTextBox1);
            this.panelGeneral.Controls.Add(this.radLabel6);
            this.panelGeneral.Controls.Add(this.dtpFechaEntrega);
            this.panelGeneral.Controls.Add(this.dtpFechaPedido);
            this.panelGeneral.Controls.Add(this.txbContacto);
            this.panelGeneral.Controls.Add(this.txbNoPedido);
            this.panelGeneral.Controls.Add(this.radLabel3);
            this.panelGeneral.Controls.Add(this.radLabel8);
            this.panelGeneral.Controls.Add(this.cboCliente);
            this.panelGeneral.Controls.Add(this.radLabel4);
            this.panelGeneral.Controls.Add(this.radLabel5);
            this.panelGeneral.Controls.Add(this.radLabel2);
            this.panelGeneral.Controls.Add(this.radLabel1);
            this.panelGeneral.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelGeneral.Location = new System.Drawing.Point(0, 30);
            this.panelGeneral.Name = "panelGeneral";
            this.panelGeneral.Size = new System.Drawing.Size(1169, 117);
            this.panelGeneral.TabIndex = 1;
            // 
            // CboResidenciaFiscal
            // 
            this.CboResidenciaFiscal.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.CboResidenciaFiscal.AutoSizeDropDownToBestFit = true;
            this.CboResidenciaFiscal.DisplayMember = "Completo";
            // 
            // CboResidenciaFiscal.NestedRadGridView
            // 
            this.CboResidenciaFiscal.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboResidenciaFiscal.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboResidenciaFiscal.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboResidenciaFiscal.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Completo";
            gridViewTextBoxColumn1.HeaderText = "Domicilio";
            gridViewTextBoxColumn1.Name = "Completo";
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1});
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboResidenciaFiscal.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboResidenciaFiscal.EditorControl.Name = "NestedRadGridView";
            this.CboResidenciaFiscal.EditorControl.ReadOnly = true;
            this.CboResidenciaFiscal.EditorControl.ShowGroupPanel = false;
            this.CboResidenciaFiscal.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboResidenciaFiscal.EditorControl.TabIndex = 0;
            this.CboResidenciaFiscal.Location = new System.Drawing.Point(127, 60);
            this.CboResidenciaFiscal.Name = "CboResidenciaFiscal";
            this.CboResidenciaFiscal.NullText = "Lugar de Embarque";
            this.CboResidenciaFiscal.Size = new System.Drawing.Size(352, 20);
            this.CboResidenciaFiscal.TabIndex = 361;
            this.CboResidenciaFiscal.TabStop = false;
            this.CboResidenciaFiscal.ValueMember = "Id";
            // 
            // CboVendedor
            // 
            this.CboVendedor.AutoFilter = true;
            this.CboVendedor.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.CboVendedor.AutoSizeDropDownToBestFit = true;
            // 
            // CboVendedor.NestedRadGridView
            // 
            this.CboVendedor.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboVendedor.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboVendedor.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboVendedor.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboVendedor.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboVendedor.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboVendedor.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "IdVendedor";
            gridViewTextBoxColumn2.HeaderText = "IdVendedor";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "IdVendedor";
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn4.FieldName = "Nombre";
            gridViewTextBoxColumn4.HeaderText = "Nombre";
            gridViewTextBoxColumn4.Name = "Nombre";
            this.CboVendedor.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.CboVendedor.EditorControl.MasterTemplate.EnableFiltering = true;
            this.CboVendedor.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboVendedor.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboVendedor.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CboVendedor.EditorControl.Name = "NestedRadGridView";
            this.CboVendedor.EditorControl.ReadOnly = true;
            this.CboVendedor.EditorControl.ShowGroupPanel = false;
            this.CboVendedor.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboVendedor.EditorControl.TabIndex = 0;
            this.CboVendedor.Location = new System.Drawing.Point(567, 34);
            this.CboVendedor.Name = "CboVendedor";
            this.CboVendedor.NullText = "Vendedor";
            this.CboVendedor.Size = new System.Drawing.Size(164, 20);
            this.CboVendedor.TabIndex = 360;
            this.CboVendedor.TabStop = false;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(889, 88);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(102, 18);
            this.radCheckBox1.TabIndex = 353;
            this.radCheckBox1.Text = "Requiere factura";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(13, 61);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(106, 18);
            this.RadLabel13.TabIndex = 355;
            this.RadLabel13.Text = "Lugar de Embarque:";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(100, 86);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.NullText = "Observaciones";
            this.radTextBox2.Size = new System.Drawing.Size(379, 20);
            this.radTextBox2.TabIndex = 352;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(13, 87);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(81, 18);
            this.radLabel7.TabIndex = 351;
            this.radLabel7.Text = "Observaciones:";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(567, 87);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.NullText = "Condiciones de Entrega";
            this.radTextBox3.Size = new System.Drawing.Size(164, 20);
            this.radTextBox3.TabIndex = 350;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(486, 88);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(79, 18);
            this.radLabel9.TabIndex = 349;
            this.radLabel9.Text = "Cond. Entrega:";
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(567, 60);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.NullText = "Condición de Pago";
            this.radTextBox1.Size = new System.Drawing.Size(164, 20);
            this.radTextBox1.TabIndex = 350;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(486, 61);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(66, 18);
            this.radLabel6.TabIndex = 349;
            this.radLabel6.Text = "Cond. Pago:";
            // 
            // dtpFechaEntrega
            // 
            this.dtpFechaEntrega.Location = new System.Drawing.Point(809, 34);
            this.dtpFechaEntrega.Name = "dtpFechaEntrega";
            this.dtpFechaEntrega.Size = new System.Drawing.Size(182, 20);
            this.dtpFechaEntrega.TabIndex = 348;
            this.dtpFechaEntrega.TabStop = false;
            this.dtpFechaEntrega.Text = "domingo, 14 de junio de 2020";
            this.dtpFechaEntrega.Value = new System.DateTime(2020, 6, 14, 22, 20, 9, 193);
            // 
            // dtpFechaPedido
            // 
            this.dtpFechaPedido.Location = new System.Drawing.Point(809, 8);
            this.dtpFechaPedido.Name = "dtpFechaPedido";
            this.dtpFechaPedido.Size = new System.Drawing.Size(182, 20);
            this.dtpFechaPedido.TabIndex = 348;
            this.dtpFechaPedido.TabStop = false;
            this.dtpFechaPedido.Text = "domingo, 14 de junio de 2020";
            this.dtpFechaPedido.Value = new System.DateTime(2020, 6, 14, 22, 20, 9, 193);
            // 
            // txbContacto
            // 
            this.txbContacto.Location = new System.Drawing.Point(73, 34);
            this.txbContacto.Name = "txbContacto";
            this.txbContacto.NullText = "Nombre del contacto";
            this.txbContacto.Size = new System.Drawing.Size(406, 20);
            this.txbContacto.TabIndex = 347;
            // 
            // txbNoPedido
            // 
            this.txbNoPedido.Location = new System.Drawing.Point(567, 8);
            this.txbNoPedido.Name = "txbNoPedido";
            this.txbNoPedido.NullText = "No. Control";
            this.txbNoPedido.Size = new System.Drawing.Size(164, 20);
            this.txbNoPedido.TabIndex = 347;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(13, 35);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(54, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Contacto:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(737, 35);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(69, 18);
            this.radLabel8.TabIndex = 0;
            this.radLabel8.Text = "Fec. Entrega:";
            // 
            // cboCliente
            // 
            this.cboCliente.AutoFilter = true;
            this.cboCliente.DisplayMember = "Nombre";
            // 
            // cboCliente.NestedRadGridView
            // 
            this.cboCliente.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboCliente.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCliente.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboCliente.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboCliente.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboCliente.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboCliente.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Id";
            gridViewTextBoxColumn5.HeaderText = "Id";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Id";
            gridViewTextBoxColumn6.FieldName = "Nombre";
            filterDescriptor1.IsFilterEditor = true;
            filterDescriptor1.Operator = Telerik.WinControls.Data.FilterOperator.StartsWith;
            filterDescriptor1.PropertyName = "Nombre";
            gridViewTextBoxColumn6.FilterDescriptor = filterDescriptor1;
            gridViewTextBoxColumn6.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn6.Name = "Nombre";
            gridViewTextBoxColumn6.Width = 200;
            gridViewTextBoxColumn7.FieldName = "RFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "RFC";
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "Clave";
            gridViewTextBoxColumn8.HeaderText = "Clave";
            gridViewTextBoxColumn8.Name = "Clave";
            this.cboCliente.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.cboCliente.EditorControl.MasterTemplate.EnableFiltering = true;
            this.cboCliente.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboCliente.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1});
            this.cboCliente.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboCliente.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.cboCliente.EditorControl.Name = "NestedRadGridView";
            this.cboCliente.EditorControl.ReadOnly = true;
            this.cboCliente.EditorControl.ShowGroupPanel = false;
            this.cboCliente.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboCliente.EditorControl.TabIndex = 0;
            this.cboCliente.Location = new System.Drawing.Point(62, 8);
            this.cboCliente.Name = "cboCliente";
            this.cboCliente.NullText = "Cliente";
            this.cboCliente.Size = new System.Drawing.Size(417, 20);
            this.cboCliente.TabIndex = 346;
            this.cboCliente.TabStop = false;
            this.cboCliente.ValueMember = "ID";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(737, 9);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 18);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Fec. Pedido:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(486, 35);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(58, 18);
            this.radLabel5.TabIndex = 0;
            this.radLabel5.Text = "Vendedor:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(486, 9);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(61, 18);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "No. Orden:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(13, 9);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(43, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Cliente:";
            // 
            // radSplitContainer
            // 
            this.radSplitContainer.Controls.Add(this.splitPanelConceptos);
            this.radSplitContainer.Controls.Add(this.splitPanelOrdenProduccion);
            this.radSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer.Location = new System.Drawing.Point(0, 147);
            this.radSplitContainer.Name = "radSplitContainer";
            this.radSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer.Size = new System.Drawing.Size(1169, 570);
            this.radSplitContainer.TabIndex = 2;
            this.radSplitContainer.TabStop = false;
            // 
            // splitPanelConceptos
            // 
            this.splitPanelConceptos.Controls.Add(this.radGridConceptos);
            this.splitPanelConceptos.Controls.Add(this.toolBarStandarControl2);
            this.splitPanelConceptos.Location = new System.Drawing.Point(0, 0);
            this.splitPanelConceptos.Name = "splitPanelConceptos";
            // 
            // 
            // 
            this.splitPanelConceptos.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanelConceptos.Size = new System.Drawing.Size(1169, 283);
            this.splitPanelConceptos.TabIndex = 0;
            this.splitPanelConceptos.TabStop = false;
            this.splitPanelConceptos.Text = "splitPanel1";
            // 
            // radGridConceptos
            // 
            this.radGridConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridConceptos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn9.DataType = typeof(int);
            gridViewTextBoxColumn9.FieldName = "IdConcepto";
            gridViewTextBoxColumn9.HeaderText = "IdConcepto";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "IdConcepto";
            gridViewTextBoxColumn9.VisibleInColumnChooser = false;
            gridViewTextBoxColumn10.DataType = typeof(int);
            gridViewTextBoxColumn10.FieldName = "IdOrdenCliente";
            gridViewTextBoxColumn10.HeaderText = "IdOrdenCliente";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "IdOrdenCliente";
            gridViewTextBoxColumn10.VisibleInColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Secuencia";
            gridViewTextBoxColumn11.HeaderText = "Sec.";
            gridViewTextBoxColumn11.Name = "Secuencia";
            gridViewTextBoxColumn12.FieldName = "NoIdentificacion";
            gridViewTextBoxColumn12.HeaderText = "No. Identificación";
            gridViewTextBoxColumn12.Name = "NoIdentificacion";
            gridViewTextBoxColumn12.Width = 110;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Cantidad";
            gridViewTextBoxColumn13.HeaderText = "Cantidad";
            gridViewTextBoxColumn13.Name = "Cantidad";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 85;
            gridViewTextBoxColumn14.FieldName = "UnidadMedida";
            gridViewTextBoxColumn14.HeaderText = "U. M.";
            gridViewTextBoxColumn14.Name = "UnidadMedida";
            gridViewTextBoxColumn14.Width = 85;
            gridViewTextBoxColumn15.FieldName = "Concepto";
            gridViewTextBoxColumn15.HeaderText = "Concepto";
            gridViewTextBoxColumn15.Name = "Concepto";
            gridViewTextBoxColumn15.Width = 400;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "ValorUnitario";
            gridViewTextBoxColumn16.FormatString = "{0:n}";
            gridViewTextBoxColumn16.HeaderText = "Unitario";
            gridViewTextBoxColumn16.Name = "ValorUnitario";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "SubTotal";
            gridViewTextBoxColumn17.FormatString = "{0:n}";
            gridViewTextBoxColumn17.HeaderText = "Importe";
            gridViewTextBoxColumn17.Name = "SubTotal";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 85;
            gridViewDateTimeColumn1.FieldName = "FechaEntrega";
            gridViewDateTimeColumn1.HeaderText = "Fec. Entrega";
            gridViewDateTimeColumn1.Name = "FechaEntrega";
            gridViewDateTimeColumn1.Width = 85;
            gridViewRatingColumn1.FieldName = "Prioridad";
            gridViewRatingColumn1.HeaderText = "Prioridad";
            gridViewRatingColumn1.Name = "Prioridad";
            gridViewRatingColumn1.Width = 102;
            gridViewTextBoxColumn18.FieldName = "Nota";
            gridViewTextBoxColumn18.HeaderText = "Observaciones";
            gridViewTextBoxColumn18.MaxLength = 64;
            gridViewTextBoxColumn18.Name = "Nota";
            gridViewTextBoxColumn18.Width = 150;
            this.radGridConceptos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewDateTimeColumn1,
            gridViewRatingColumn1,
            gridViewTextBoxColumn18});
            this.radGridConceptos.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridConceptos.Name = "radGridConceptos";
            this.radGridConceptos.Size = new System.Drawing.Size(1169, 253);
            this.radGridConceptos.TabIndex = 1;
            // 
            // splitPanelOrdenProduccion
            // 
            this.splitPanelOrdenProduccion.Controls.Add(this.radPageView1);
            this.splitPanelOrdenProduccion.Location = new System.Drawing.Point(0, 287);
            this.splitPanelOrdenProduccion.Name = "splitPanelOrdenProduccion";
            // 
            // 
            // 
            this.splitPanelOrdenProduccion.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanelOrdenProduccion.Size = new System.Drawing.Size(1169, 283);
            this.splitPanelOrdenProduccion.TabIndex = 1;
            this.splitPanelOrdenProduccion.TabStop = false;
            this.splitPanelOrdenProduccion.Text = "splitPanel2";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Controls.Add(this.radPageViewPage4);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1169, 283);
            this.radPageView1.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radGridOrdenP);
            this.radPageViewPage1.Controls.Add(this.toolBarStandarControl3);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(123F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1148, 235);
            this.radPageViewPage1.Text = "Orden de Producción";
            // 
            // radGridOrdenP
            // 
            this.radGridOrdenP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridOrdenP.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn19.DataType = typeof(int);
            gridViewTextBoxColumn19.FieldName = "OrdenProduccion";
            gridViewTextBoxColumn19.HeaderText = "No. Orden";
            gridViewTextBoxColumn19.Name = "column1";
            gridViewTextBoxColumn20.HeaderText = "Status";
            gridViewTextBoxColumn20.Name = "column2";
            gridViewTextBoxColumn21.HeaderText = "Cliente";
            gridViewTextBoxColumn21.Name = "column3";
            gridViewTextBoxColumn21.Width = 220;
            gridViewTextBoxColumn22.HeaderText = "Pedido";
            gridViewTextBoxColumn22.Name = "column4";
            gridViewTextBoxColumn22.Width = 220;
            gridViewTextBoxColumn23.HeaderText = "Vendedor";
            gridViewTextBoxColumn23.Name = "column5";
            gridViewTextBoxColumn24.HeaderText = "Cantidad";
            gridViewTextBoxColumn24.Name = "column6";
            gridViewTextBoxColumn25.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn25.FormatString = "{0:d}";
            gridViewTextBoxColumn25.HeaderText = "Fec. Pedido";
            gridViewTextBoxColumn25.Name = "column7";
            gridViewTextBoxColumn25.Width = 75;
            gridViewTextBoxColumn26.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn26.FormatString = "{0:d}";
            gridViewTextBoxColumn26.HeaderText = "Fec. OP";
            gridViewTextBoxColumn26.Name = "column8";
            gridViewTextBoxColumn26.Width = 75;
            gridViewTextBoxColumn27.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn27.FormatString = "{0:d}";
            gridViewTextBoxColumn27.HeaderText = "Fec. Lib.";
            gridViewTextBoxColumn27.Name = "column9";
            gridViewTextBoxColumn27.Width = 75;
            gridViewTextBoxColumn28.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn28.FormatString = "{0:d}";
            gridViewTextBoxColumn28.HeaderText = "Fec. Solicitud";
            gridViewTextBoxColumn28.Name = "column10";
            gridViewTextBoxColumn28.Width = 75;
            gridViewTextBoxColumn29.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn29.FormatString = "{0:d}";
            gridViewTextBoxColumn29.HeaderText = "Fec. Acuerdo";
            gridViewTextBoxColumn29.Name = "column11";
            gridViewTextBoxColumn29.Width = 75;
            gridViewTextBoxColumn30.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn30.FormatString = "{0:d}";
            gridViewTextBoxColumn30.HeaderText = "Fec. VOBO";
            gridViewTextBoxColumn30.Name = "column12";
            gridViewTextBoxColumn30.Width = 75;
            gridViewTextBoxColumn31.HeaderText = "Jerarquia";
            gridViewTextBoxColumn31.Name = "column13";
            gridViewTextBoxColumn32.HeaderText = "Nota";
            gridViewTextBoxColumn32.Name = "column14";
            gridViewTextBoxColumn32.Width = 100;
            this.radGridOrdenP.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.radGridOrdenP.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.radGridOrdenP.Name = "radGridOrdenP";
            this.radGridOrdenP.Size = new System.Drawing.Size(1148, 205);
            this.radGridOrdenP.TabIndex = 1;
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(68F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(1148, 235);
            this.radPageViewPage2.Text = "Cotización";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.radGridDoctos);
            this.radPageViewPage3.Controls.Add(this.toolBarStandarControl4);
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(80F, 28F);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(1148, 235);
            this.radPageViewPage3.Text = "Documentos";
            // 
            // radGridDoctos
            // 
            this.radGridDoctos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridDoctos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn33.FieldName = "Titulo";
            gridViewTextBoxColumn33.HeaderText = "Título";
            gridViewTextBoxColumn33.Name = "Titulo";
            gridViewTextBoxColumn33.Width = 120;
            gridViewTextBoxColumn34.FieldName = "Descripcion";
            gridViewTextBoxColumn34.HeaderText = "Descripción";
            gridViewTextBoxColumn34.Name = "Descripcion";
            gridViewTextBoxColumn34.Width = 420;
            gridViewTextBoxColumn35.FieldName = "Content";
            gridViewTextBoxColumn35.HeaderText = "Tipo";
            gridViewTextBoxColumn35.Name = "Tipo";
            gridViewTextBoxColumn35.Width = 90;
            gridViewCommandColumn1.FieldName = "Url";
            gridViewCommandColumn1.HeaderText = "Documento";
            gridViewCommandColumn1.Name = "Descargar";
            gridViewCommandColumn1.Width = 85;
            this.radGridDoctos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewCommandColumn1});
            this.radGridDoctos.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.radGridDoctos.Name = "radGridDoctos";
            this.radGridDoctos.ShowGroupPanel = false;
            this.radGridDoctos.Size = new System.Drawing.Size(1148, 205);
            this.radGridDoctos.TabIndex = 3;
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.ItemSize = new System.Drawing.SizeF(95F, 28F);
            this.radPageViewPage4.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(1148, 235);
            this.radPageViewPage4.Text = "Plan de Entrega";
            // 
            // toolBarStandarControl1
            // 
            this.toolBarStandarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl1.Etiqueta = "";
            this.toolBarStandarControl1.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl1.Name = "toolBarStandarControl1";
            this.toolBarStandarControl1.ShowActualizar = true;
            this.toolBarStandarControl1.ShowAutorizar = false;
            this.toolBarStandarControl1.ShowCerrar = true;
            this.toolBarStandarControl1.ShowEditar = false;
            this.toolBarStandarControl1.ShowFiltro = false;
            this.toolBarStandarControl1.ShowGuardar = true;
            this.toolBarStandarControl1.ShowHerramientas = false;
            this.toolBarStandarControl1.ShowImagen = false;
            this.toolBarStandarControl1.ShowImprimir = true;
            this.toolBarStandarControl1.ShowNuevo = true;
            this.toolBarStandarControl1.ShowRemover = true;
            this.toolBarStandarControl1.Size = new System.Drawing.Size(1169, 30);
            this.toolBarStandarControl1.TabIndex = 3;
            // 
            // toolBarStandarControl2
            // 
            this.toolBarStandarControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl2.Etiqueta = "";
            this.toolBarStandarControl2.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl2.Name = "toolBarStandarControl2";
            this.toolBarStandarControl2.ShowActualizar = true;
            this.toolBarStandarControl2.ShowAutorizar = false;
            this.toolBarStandarControl2.ShowCerrar = false;
            this.toolBarStandarControl2.ShowEditar = false;
            this.toolBarStandarControl2.ShowFiltro = true;
            this.toolBarStandarControl2.ShowGuardar = false;
            this.toolBarStandarControl2.ShowHerramientas = false;
            this.toolBarStandarControl2.ShowImagen = false;
            this.toolBarStandarControl2.ShowImprimir = false;
            this.toolBarStandarControl2.ShowNuevo = true;
            this.toolBarStandarControl2.ShowRemover = true;
            this.toolBarStandarControl2.Size = new System.Drawing.Size(1169, 30);
            this.toolBarStandarControl2.TabIndex = 2;
            // 
            // toolBarStandarControl3
            // 
            this.toolBarStandarControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl3.Etiqueta = "";
            this.toolBarStandarControl3.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl3.Name = "toolBarStandarControl3";
            this.toolBarStandarControl3.ShowActualizar = true;
            this.toolBarStandarControl3.ShowAutorizar = false;
            this.toolBarStandarControl3.ShowCerrar = false;
            this.toolBarStandarControl3.ShowEditar = false;
            this.toolBarStandarControl3.ShowFiltro = true;
            this.toolBarStandarControl3.ShowGuardar = false;
            this.toolBarStandarControl3.ShowHerramientas = false;
            this.toolBarStandarControl3.ShowImagen = false;
            this.toolBarStandarControl3.ShowImprimir = false;
            this.toolBarStandarControl3.ShowNuevo = true;
            this.toolBarStandarControl3.ShowRemover = true;
            this.toolBarStandarControl3.Size = new System.Drawing.Size(1148, 30);
            this.toolBarStandarControl3.TabIndex = 2;
            // 
            // toolBarStandarControl4
            // 
            this.toolBarStandarControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl4.Etiqueta = "";
            this.toolBarStandarControl4.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl4.Name = "toolBarStandarControl4";
            this.toolBarStandarControl4.ShowActualizar = false;
            this.toolBarStandarControl4.ShowAutorizar = false;
            this.toolBarStandarControl4.ShowCerrar = false;
            this.toolBarStandarControl4.ShowEditar = false;
            this.toolBarStandarControl4.ShowFiltro = true;
            this.toolBarStandarControl4.ShowGuardar = false;
            this.toolBarStandarControl4.ShowHerramientas = false;
            this.toolBarStandarControl4.ShowImagen = false;
            this.toolBarStandarControl4.ShowImprimir = false;
            this.toolBarStandarControl4.ShowNuevo = true;
            this.toolBarStandarControl4.ShowRemover = true;
            this.toolBarStandarControl4.Size = new System.Drawing.Size(1148, 30);
            this.toolBarStandarControl4.TabIndex = 4;
            // 
            // OrdenClienteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 717);
            this.Controls.Add(this.radSplitContainer);
            this.Controls.Add(this.panelGeneral);
            this.Controls.Add(this.toolBarStandarControl1);
            this.Name = "OrdenClienteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "OrdenCliente";
            this.Load += new System.EventHandler(this.OrdenClienteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelGeneral)).EndInit();
            this.panelGeneral.ResumeLayout(false);
            this.panelGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboResidenciaFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFechaPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txbNoPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer)).EndInit();
            this.radSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanelConceptos)).EndInit();
            this.splitPanelConceptos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridConceptos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanelOrdenProduccion)).EndInit();
            this.splitPanelOrdenProduccion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridOrdenP.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridOrdenP)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridDoctos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridDoctos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel panelGeneral;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer;
        private Telerik.WinControls.UI.SplitPanel splitPanelConceptos;
        private Telerik.WinControls.UI.RadGridView radGridConceptos;
        private Telerik.WinControls.UI.SplitPanel splitPanelOrdenProduccion;
        private Telerik.WinControls.UI.RadGridView radGridOrdenP;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox cboCliente;
        private Telerik.WinControls.UI.RadTextBox txbNoPedido;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txbContacto;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFechaPedido;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFechaEntrega;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboVendedor;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboResidenciaFiscal;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadGridView radGridDoctos;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl2;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl3;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl1;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl4;
    }
}
