﻿using System;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Common.Services;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Forms;
using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using System.Linq;

namespace Jaeger.UI.Ventas.Forms {
    public partial class EstadoCuentaForm : RadForm {
        Jaeger.Aplication.Almacen.PT.Contracts.IRemisionadoService d1 = new RemisionadoService();
        private BindingList<RemisionSingleModel> remisionSingleModelList;
        protected internal IDirectorioService service;
        public EstadoCuentaForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void EstadoCuentaForm_Load(object sender, EventArgs e) {
            this.HostItem1.HostedItem = this.cboVendedores.MultiColumnComboBoxElement;
            this.GridData.GridCommon();
            //this.GridData.Columns.AddRange(Jaeger.UI.Almacen.PT.Services.GridRemisionService.GetColumns(true));
            this.PorStatus.DisplayName = "Descriptor";
            this.PorStatus.ValueMember = "Id";
            this.PorStatus.DataSource = Aplication.Almacen.PT.Services.RemisionService.GetStatus();

            this.cboVendedores.AutoFilter = true;

            FilterDescriptor descriptor = new FilterDescriptor(this.cboVendedores.DisplayMember, FilterOperator.StartsWith, string.Empty);
            this.cboVendedores.EditorControl.FilterDescriptors.Add(descriptor);
            this.service = new Aplication.CPLite.Services.DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.cboVendedores.DataSource = this.service.GetList<ContribuyenteDomicilioSingleModel>();
            this.Actualizar.Click += TRemision_Actualizar_Click;
            this.AutoSuma.Click += TRemision_AutoSuma_Click;
            this.Filtro.Click += this.TRemision_Filtro_Click;
            this.ExportarExcel.Click += ExportarExcel_Click;
            this.Cerrar.Click += TRemision_Cerrar_Click;
        }

        private void ExportarExcel_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.GridData);
            exportar.ShowDialog(this);
        }

        private void TRemision_AutoSuma_Click(object sender, EventArgs e) {
            this.GridData.TelerikGridAutoSum(!(this.AutoSuma.ToggleState == ToggleState.On));
        }

        private void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.remisionSingleModelList;
        }

        public virtual void TRemision_Filtro_Click(object sender, EventArgs e) {
            this.GridData.ActivateFilterRow(((CommandBarToggleButton)sender).ToggleState);
        }

        private void TRemision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consulta() {
            var selectedValue = this.cboVendedores.SelectedItem as GridViewRowInfo;
            var seletedValue2 = this.PorStatus.SelectedValue;
            if (selectedValue != null) {
                var _seleccionado = selectedValue.DataBoundItem as ContribuyenteDomicilioSingleModel;
                var d0 = Aplication.Almacen.PT.Services.RemisionadoService.Create().WithIdReceptor(_seleccionado.IdDirectorio).WithIdStatus((int)seletedValue2).Build();
                this.remisionSingleModelList = new BindingList<RemisionSingleModel>(
                        this.d1.GetList<RemisionSingleModel>(d0).ToList()
                    );
            }
        }
    }
}
