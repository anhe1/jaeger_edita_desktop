﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.Ventas.Forms {
    public partial class VendedorCatalogoForm : RadForm {
        #region declaraciones
        protected IVendedoresService service;
        protected IComisionService comision;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        private BindingList<Vendedor2DetailModel> vendedores;
        private BindingList<ComisionDetailModel> catalogoComision;
        private readonly RadMenuItem _accesoWeb = new RadMenuItem { Text = "Acceso Web" };
        #endregion

        public VendedorCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void VendedorCatalogoForm_Load(object sender, EventArgs e) {
            this.TVendedor.ShowHerramientas = true;
            this.TVendedor.Herramientas.Items.Add(_accesoWeb);
            this.TVendedor.Actualizar.Click += this.TVendedor_Actualizar_Click;
            this.TVendedor.Nuevo.Click += this.TVendedor_Agregar_Click;
            this.TVendedor.Editar.Click += this.TVendedor_Editar_Click;
            this.TVendedor.Remover.Click += this.TVendedor_Remover_Click;
            this.TVendedor.Imprimir.Click += this.TVendedor_Imprimir_Click;
            this.TVendedor.Cerrar.Click += this.TVendedor_Cerrar_Click;
            this.TVendedor.GridData.RowSourceNeeded += GridRemisiones_RowSourceNeeded;
        }

        #region barra de herramientas vendedor
        private void TVendedor_Agregar_Click(object sender, EventArgs e) {
            var editar = new VendedorForm(this._permisos);
            editar.ShowDialog(this);
        }

        private void TVendedor_Editar_Click(object sender, EventArgs e) {
            if (this.TVendedor.GridData.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.GetClientes)) {
                    espera.Text = "Consultando autorizaciones ...";
                    espera.ShowDialog(this);
                }
                var _seleccionado = this.TVendedor.GridData.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                var editar = new VendedorForm(_seleccionado, this._permisos);
                editar.ShowDialog(this);
            }
        }

        private void TVendedor_Remover_Click(object sender, EventArgs e) {
            if (this.TVendedor.GridData.CurrentRow != null) {
                var _seleccionado = this.TVendedor.GridData.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                if (_seleccionado != null) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover al vendedor seleccionado? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {

                    }
                }
            }
        }

        private void TVendedor_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            var IdTamanio = this.TVendedor.GridData.Columns["IdComision"] as GridViewComboBoxColumn;
            IdTamanio.DataSource = this.catalogoComision;
            IdTamanio.DisplayMember = "Descripcion";
            IdTamanio.ValueMember = "IdComision";

            this.TVendedor.GridData.DataSource = this.vendedores;
        }

        protected void TVendedor_Imprimir_Click(object sender, EventArgs eventArgs) {
            if (this.TVendedor.GridData.CurrentRow != null) {
                var seleccionado = this.TVendedor.GridData.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                if (seleccionado != null) {
                    using (var espera = new Waiting1Form(this.GetClientes)) {
                        espera.Text = "Consultando autorizaciones ...";
                        espera.ShowDialog(this);
                    }
                    var reporte = new ReporteForm(new VendedorCarteraPrinter(seleccionado));
                    reporte.ShowDialog(this);
                }
            }
        }

        private void TVendedor_Acceso_Click(object sender, EventArgs e) {
            if (this.TVendedor.GridData.CurrentRow != null) {
                var edicion = this.TVendedor.GridData.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                if (edicion != null) {
                    var editar = new VendedorAccesoForm(edicion);
                    editar.ShowDialog(this);
                }
            }
        }

        private void TVendedor_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void GridRemisiones_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var _rowData = e.ParentRow.DataBoundItem as Vendedor2DetailModel;
            if (_rowData != null) {
                if (e.Template.Caption == this.TVendedor.gridClientes.Caption) {
                    using (var espera = new Waiting1Form(this.GetClientes)) {
                        espera.Text = "Consultando autorizaciones ...";
                        espera.ShowDialog(this);
                    }

                    foreach (var item in _rowData.Clientes) {
                        var _row = e.Template.Rows.NewRow();
                        _row.Cells["Activo"].Value = item.Activo;
                        _row.Cells["Clave"].Value = item.Clave;
                        _row.Cells["Nombre"].Value = item.Nombre;
                        _row.Cells["Nota"].Value = item.Nota;
                        _row.Cells["Creo"].Value = item.Modifica;
                        _row.Cells["FechaNuevo"].Value = item.FechaModifica;
                        e.SourceCollection.Add(_row);
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.vendedores = this.service.GetList(!this.TVendedor.RegistroInactivo.IsChecked);
            this.catalogoComision = this.comision.GetComisiones(true);
        }

        private void GetClientes() {
            var _seleccionado = this.TVendedor.GridData.CurrentRow.DataBoundItem as Vendedor2DetailModel;
            if (_seleccionado != null) {
                var d1 = this.service.GetById(_seleccionado.IdVendedor);
                if (d1 != null) {
                    _seleccionado.Clientes = d1.Clientes;
                }
            }
        }
        #endregion
    }
}
