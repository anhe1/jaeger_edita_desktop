﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Ventas;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Util;

namespace Jaeger.UI.Ventas.Forms {
    public partial class OrdenClienteForm : RadForm {
        private BackgroundWorker preparar;
        protected IOrdenClienteService service;
        private OrdenClienteDetailModel ordenCliente;
        private BindingList<ClienteDetailModel> clientes;

        public OrdenClienteForm() {
            InitializeComponent();
            this.ordenCliente = new OrdenClienteDetailModel();
        }

        public OrdenClienteForm(OrdenClienteDetailModel item) {
            InitializeComponent();
            this.ordenCliente = item;
        }

        private void OrdenClienteForm_Load(object sender, EventArgs e) {
            this.radGridConceptos.Standard();
            this.radGridOrdenP.Standard();
            this.radGridDoctos.Standard();

            this.service = new OrdenClienteService();
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += PrepararDoWork;
            this.preparar.RunWorkerCompleted += PrepararRunWorkerCompleted;
            this.preparar.RunWorkerAsync();
            
            this.CreateBinding();
        }

        private void cboCliente_SelectedValueChanged(object sender, EventArgs e) {
            var seleccionado = this.cboCliente.SelectedItem as GridViewRowInfo;
            if (!(seleccionado == null)) {
                var cliente = seleccionado.DataBoundItem as ClienteDetailModel;
                if (cliente != null) {
                    this.ordenCliente.IdCliente = cliente.IdDirectorio;
                    this.ordenCliente.Cliente = cliente.Nombre;
                    this.CboVendedor.DataSource = cliente.Vendedores;
                    this.CboResidenciaFiscal.Text = "";
                    this.CboResidenciaFiscal.DataSource = cliente.Domicilios;
                }
            }
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ....";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void ToolBarButtonImprimir_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void PrepararDoWork(object sender, DoWorkEventArgs e) {
            this.clientes = this.service.GetClientes();
        }

        private void PrepararRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            // catalogo formas de pago
            this.cboCliente.DataSource = this.clientes;
        }

        private void ToolBarButtonConceptoAgregar_Click(object sender, EventArgs e) {
            this.ordenCliente.Conceptos.AddNew();
        }

        private void ToolBarButtonConceptoRemover_Click(object sender, EventArgs e) {
            var seleccionado = this.radGridConceptos.CurrentRow.DataBoundItem as OrdenClienteConceptoDetailModel;
            if (seleccionado != null) {
                if (RadMessageBox.Show(this, "Properties.Resources.OrdenCliente_Concepto_Remover", "Properties.Resources.Message_Title_Question", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                    return;
                if (seleccionado.IdConcepto == 0) {
                    this.ordenCliente.Conceptos.Remove(seleccionado);
                }
                else {
                    seleccionado.Activo = false;
                }
            }
        }

        private void ToolBarDoctosButtonAgregar_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() { Filter = "*.*|*.*", InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString() };
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            var infoFile = new System.IO.FileInfo(openFileDialog.FileName);
            if (infoFile.Exists) {
                var fContent = new FileContentType();

                var doctoFile = new OrdenClienteDoctoModel();
                doctoFile.Titulo = infoFile.Name;
                doctoFile.Content = fContent.ContentType(infoFile.FullName);
                doctoFile.Tag = infoFile.FullName;
                if (this.ordenCliente.Documentos == null) {
                    this.ordenCliente.Documentos = new BindingList<OrdenClienteDoctoModel>();
                    this.radGridDoctos.DataSource = this.ordenCliente.Documentos;
                }
                this.ordenCliente.Documentos.Add(doctoFile);
            }
        }

        private void ToolBarDoctosButtonQuitar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonOPAgregar_Click(object sender, EventArgs e) {
            if (this.radGridConceptos.CurrentRow != null) {
                var seleccionado = this.radGridConceptos.CurrentRow.DataBoundItem as OrdenClienteConceptoDetailModel;
                seleccionado.OrdenProduccion.Add(new OrdenClienteOrdenProduccionModel());
            }
        }

        private void ToolBarButtonOPRemover_Click(object sender, EventArgs e) {
            if (this.radGridOrdenP.CurrentRow != null) {
                var seleccionado = this.radGridOrdenP.CurrentRow.DataBoundItem as OrdenClienteOrdenProduccionModel;
                if (seleccionado.Id == 0) {
                    this.radGridOrdenP.Rows.Remove(this.radGridOrdenP.CurrentRow);
                }
                else {
                    seleccionado.Activo = false;
                }
            }
        }

        private void ToolBarButtonOPBuscar_Click(object sender, EventArgs e) {

        }

        private void CreateBinding() {
            //this.ToolBarTextNoCtrl.DataBindings.Clear();
            //this.ToolBarTextNoCtrl.DataBindings.Add("Text", this.ordenCliente, "Identificador", true, DataSourceUpdateMode.OnPropertyChanged);

            this.cboCliente.DataBindings.Clear();
            //this.cboCliente.DataBindings.Add("SelectedValue", this.ordenCliente, "IdCliente", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboCliente.DataBindings.Add("Text", this.ordenCliente, "Cliente", true, DataSourceUpdateMode.OnPropertyChanged);
            this.cboCliente.SelectedValueChanged += cboCliente_SelectedValueChanged;
            this.txbNoPedido.DataBindings.Clear();
            this.txbNoPedido.DataBindings.Add("Text", this.ordenCliente, "NoPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.txbContacto.DataBindings.Clear();
            this.txbContacto.DataBindings.Add("Text", this.ordenCliente, "Comprador", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtpFechaPedido.DataBindings.Clear();
            this.dtpFechaPedido.DataBindings.Add("Value", this.ordenCliente, "FechaPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtpFechaEntrega.DataBindings.Clear();
            this.dtpFechaEntrega.SetToNullValue();
            this.dtpFechaEntrega.NullableValue = null;
            this.dtpFechaEntrega.DataBindings.Add("Value", this.ordenCliente, "FechaEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CboVendedor.DataBindings.Clear();
            this.CboVendedor.DataBindings.Add("Text", this.ordenCliente, "Vendedor", true, DataSourceUpdateMode.OnPropertyChanged);

            this.radGridConceptos.DataSource = this.ordenCliente.Conceptos;
            this.radGridOrdenP.DataMember = "OrdenProduccion";
            this.radGridOrdenP.DataSource = this.ordenCliente.Conceptos;
            this.radGridConceptos.AllowEditRow = true;
            this.radGridDoctos.DataSource = this.ordenCliente.Documentos;
            this.radGridDoctos.AllowEditRow = this.ordenCliente.Id == 0;
            this.radGridOrdenP.AllowEditRow = this.ordenCliente.Id == 0;

            //if (this.ordenCliente.IdOrden == 0)
            //    this.Text = "Nueva Ord. Cliente";
            //else
            //    this.Text = string.Concat("Ord. Cliente: ", this.ToolBarTextNoCtrl.Text);
        }

        private void Consultar() {

        }

        private void Guardar() {
            this.ordenCliente = this.service.Save(this.ordenCliente);
        }
    }
}
