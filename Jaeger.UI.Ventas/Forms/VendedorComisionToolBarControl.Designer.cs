﻿namespace Jaeger.UI.Ventas.Forms {
    partial class VendedorComisionToolBarControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Bar = new Telerik.WinControls.UI.RadCommandBar();
            this.SpinEjercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.cboVendedores = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.TComision = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LabelDocumento = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostItem1 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.labelEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.Ejercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Calcular = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Autosuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).BeginInit();
            this.Bar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Bar
            // 
            this.Bar.Controls.Add(this.SpinEjercicio);
            this.Bar.Controls.Add(this.cboVendedores);
            this.Bar.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bar.Location = new System.Drawing.Point(0, 0);
            this.Bar.Name = "Bar";
            this.Bar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.Bar.Size = new System.Drawing.Size(1020, 55);
            this.Bar.TabIndex = 5;
            // 
            // SpinEjercicio
            // 
            this.SpinEjercicio.Location = new System.Drawing.Point(367, 4);
            this.SpinEjercicio.Name = "SpinEjercicio";
            this.SpinEjercicio.Size = new System.Drawing.Size(55, 20);
            this.SpinEjercicio.TabIndex = 4;
            this.SpinEjercicio.TabStop = false;
            this.SpinEjercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cboVendedores
            // 
            this.cboVendedores.AutoSizeDropDownToBestFit = true;
            this.cboVendedores.DisplayMember = "Nombre";
            this.cboVendedores.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // cboVendedores.NestedRadGridView
            // 
            this.cboVendedores.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboVendedores.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVendedores.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboVendedores.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboVendedores.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdRlcdrc";
            gridViewTextBoxColumn1.HeaderText = "IdRlcdrc";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdRlcdrc";
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            this.cboVendedores.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.cboVendedores.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboVendedores.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboVendedores.EditorControl.Name = "NestedRadGridView";
            this.cboVendedores.EditorControl.ReadOnly = true;
            this.cboVendedores.EditorControl.ShowGroupPanel = false;
            this.cboVendedores.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboVendedores.EditorControl.TabIndex = 0;
            this.cboVendedores.Location = new System.Drawing.Point(73, 3);
            this.cboVendedores.Name = "cboVendedores";
            this.cboVendedores.Size = new System.Drawing.Size(229, 20);
            this.cboVendedores.TabIndex = 2;
            this.cboVendedores.TabStop = false;
            this.cboVendedores.ValueMember = "IdRlcdrc";
            // 
            // BarRowElement
            // 
            this.BarRowElement.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.TComision});
            this.BarRowElement.Text = "";
            this.BarRowElement.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarRowElement.UseCompatibleTextRendering = false;
            // 
            // TComision
            // 
            this.TComision.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TComision.DisplayName = "Herramientas";
            this.TComision.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LabelDocumento,
            this.HostItem1,
            this.labelEjercicio,
            this.Ejercicio,
            this.Separator2,
            this.Actualizar,
            this.Calcular,
            this.Filtro,
            this.Autosuma,
            this.Imprimir,
            this.Cerrar});
            this.TComision.Name = "TComision";
            this.TComision.StretchHorizontally = true;
            this.TComision.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TComision.UseCompatibleTextRendering = false;
            // 
            // LabelDocumento
            // 
            this.LabelDocumento.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.LabelDocumento.DisplayName = "Etiqueta";
            this.LabelDocumento.Name = "LabelDocumento";
            this.LabelDocumento.Text = "Vendedor:";
            this.LabelDocumento.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.LabelDocumento.UseCompatibleTextRendering = false;
            // 
            // HostItem1
            // 
            this.HostItem1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostItem1.DisplayName = "Comprobantes";
            this.HostItem1.MinSize = new System.Drawing.Size(250, 15);
            this.HostItem1.Name = "HostItem1";
            this.HostItem1.Text = "Comprobantes";
            this.HostItem1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.HostItem1.UseCompatibleTextRendering = false;
            // 
            // labelEjercicio
            // 
            this.labelEjercicio.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.labelEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.labelEjercicio.Name = "labelEjercicio";
            this.labelEjercicio.Text = "Ejercicio:";
            this.labelEjercicio.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.labelEjercicio.UseCompatibleTextRendering = false;
            // 
            // Ejercicio
            // 
            this.Ejercicio.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Ejercicio.DisplayName = "Ejercicio";
            this.Ejercicio.MaxSize = new System.Drawing.Size(52, 20);
            this.Ejercicio.MinSize = new System.Drawing.Size(52, 20);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Text = "";
            this.Ejercicio.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Ejercicio.UseCompatibleTextRendering = false;
            // 
            // Separator2
            // 
            this.Separator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Separator2.UseCompatibleTextRendering = false;
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Ventas.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Actualizar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Actualizar.UseCompatibleTextRendering = false;
            // 
            // Calcular
            // 
            this.Calcular.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Calcular.DisplayName = "Calcular";
            this.Calcular.DrawText = true;
            this.Calcular.Image = global::Jaeger.UI.Ventas.Properties.Resources.accounting_16px;
            this.Calcular.Name = "Calcular";
            this.Calcular.Text = "Calcular";
            this.Calcular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Calcular.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Calcular.UseCompatibleTextRendering = false;
            // 
            // Filtro
            // 
            this.Filtro.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Ventas.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Filtro.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Filtro.UseCompatibleTextRendering = false;
            // 
            // Autosuma
            // 
            this.Autosuma.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Autosuma.DisplayName = "Auto Suma";
            this.Autosuma.DrawText = true;
            this.Autosuma.Image = global::Jaeger.UI.Ventas.Properties.Resources.sigma_16px;
            this.Autosuma.Name = "Autosuma";
            this.Autosuma.Text = "Auto Suma";
            this.Autosuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Autosuma.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Autosuma.UseCompatibleTextRendering = false;
            // 
            // Imprimir
            // 
            this.Imprimir.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Ventas.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Imprimir.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Imprimir.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Ventas.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // VendedorComisionToolBarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Bar);
            this.Name = "VendedorComisionToolBarControl";
            this.Size = new System.Drawing.Size(1020, 30);
            this.Load += new System.EventHandler(this.VendedorComisionToolBarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).EndInit();
            this.Bar.ResumeLayout(false);
            this.Bar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar Bar;
        private Telerik.WinControls.UI.RadSpinEditor SpinEjercicio;
        public Telerik.WinControls.UI.RadMultiColumnComboBox cboVendedores;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement TComision;
        private Telerik.WinControls.UI.CommandBarLabel LabelDocumento;
        private Telerik.WinControls.UI.CommandBarHostItem HostItem1;
        private Telerik.WinControls.UI.CommandBarLabel labelEjercicio;
        private Telerik.WinControls.UI.CommandBarHostItem Ejercicio;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        public Telerik.WinControls.UI.CommandBarButton Actualizar;
        public Telerik.WinControls.UI.CommandBarButton Calcular;
        public Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        public Telerik.WinControls.UI.CommandBarToggleButton Autosuma;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
    }
}
