﻿namespace Jaeger.UI.Ventas.Forms
{
    partial class OrdenClienteCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn2 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridData = new Telerik.WinControls.UI.RadGridView();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gridData
            // 
            this.gridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn7.DataType = typeof(int);
            gridViewTextBoxColumn7.FieldName = "IdOrden";
            gridViewTextBoxColumn7.HeaderText = "O. C.";
            gridViewTextBoxColumn7.Name = "IdOrden";
            gridViewTextBoxColumn7.Width = 75;
            gridViewMultiComboBoxColumn2.FieldName = "Cliente";
            gridViewMultiComboBoxColumn2.HeaderText = "IdCliente";
            gridViewMultiComboBoxColumn2.Name = "IdCliente";
            gridViewMultiComboBoxColumn2.Width = 220;
            gridViewTextBoxColumn8.FieldName = "NoPedido";
            gridViewTextBoxColumn8.HeaderText = "NoPedido";
            gridViewTextBoxColumn8.Name = "NoPedido";
            gridViewTextBoxColumn8.Width = 110;
            gridViewTextBoxColumn9.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn9.FieldName = "FechaRequerida";
            gridViewTextBoxColumn9.HeaderText = "Fec. Req.";
            gridViewTextBoxColumn9.Name = "FechaRequerida";
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn10.FieldName = "FechaVoBo";
            gridViewTextBoxColumn10.HeaderText = "Fec. Vo.Bo.";
            gridViewTextBoxColumn10.Name = "FechaVoBo";
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.FieldName = "Vendedor";
            gridViewTextBoxColumn11.HeaderText = "Vendedor";
            gridViewTextBoxColumn11.Name = "Vendedor";
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.Width = 75;
            this.gridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewMultiComboBoxColumn2,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.gridData.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridData.Name = "gridData";
            this.gridData.Size = new System.Drawing.Size(1124, 528);
            this.gridData.TabIndex = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutosuma = false;
            this.ToolBar.ShowCancelar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowEjercicio = true;
            this.ToolBar.ShowFiltro = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowPeriodo = true;
            this.ToolBar.Size = new System.Drawing.Size(1124, 30);
            this.ToolBar.TabIndex = 2;
            this.ToolBar.Editar.Click += this.ToolBarButtonEditar_Click;
            this.ToolBar.Cancelar.Click += this.ToolBarButtonCancelar_Click;
            this.ToolBar.Actualizar.Click += this.ToolBarButtonActualizar_Click;
            this.ToolBar.Filtro.Click += this.ToolBarButtonFiltro_Click;
            this.ToolBar.Cerrar.Click += this.ToolBarButtonCerrar_Click;
            // 
            // OrdenClienteCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 558);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.ToolBar);
            this.Name = "OrdenClienteCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Clientes: Orden de Cliente";
            this.Load += new System.EventHandler(this.OrdenesClienteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView gridData;
        private Common.Forms.ToolBarCommonControl ToolBar;
    }
}
