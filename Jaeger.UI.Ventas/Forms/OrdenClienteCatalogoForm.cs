﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Aplication.Ventas;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Ventas.Forms {
    public partial class OrdenClienteCatalogoForm : RadForm {
        protected IOrdenClienteService service;
        private BindingList<OrdenClienteModel> datos;

        public OrdenClienteCatalogoForm() {
            InitializeComponent();
        }

        private void OrdenesClienteForm_Load(object sender, EventArgs e) {
            this.gridData.Standard();
            this.service = new OrdenClienteService();
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            var nuevo = new OrdenClienteForm() { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        private void ToolBarButtonEditar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Editar)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var editar = this.Tag as OrdenClienteDetailModel;
            if (editar != null) {
                var edicion = new OrdenClienteForm(editar) { MdiParent = this.ParentForm };
                edicion.Show();
            }
            this.Tag = null;
        }

        private void ToolBarButtonCancelar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonFiltro_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonExportar_Click(object sender, EventArgs e) {

        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Actualizando...";
                espera.ShowDialog(this);
            }
            this.gridData.DataSource = this.datos;
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonCrear_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Crear)) {
                espera.Text = "Creando tablas necesarias...";
                espera.ShowDialog(this);
            }
        }

        private void Crear() {
            this.service.Create();
        }

        private void Consultar() {
            this.datos = this.service.GetList(this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
        }

        private void Editar() {
            if (this.gridData.CurrentRow != null) {
                var seleccionado = this.gridData.CurrentRow.DataBoundItem as OrdenClienteModel;
                if (seleccionado != null) {
                    var editar = this.service.GetById(seleccionado.Id);
                    if (editar != null)
                        this.Tag = editar;
                    else
                        this.Tag = null;
                }
            }
        }
    }
}
