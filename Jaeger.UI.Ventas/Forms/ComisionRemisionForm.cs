﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Ventas.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.UI.Ventas.Forms {
    public partial class ComisionRemisionForm : RadForm {
        protected IComisionService service;
        private BindingList<RemisionComisionDetailModel> _DataSource;
        private Domain.Base.ValueObjects.UIAction _Permisos;
        private BindingList<Vendedor2DetailModel> _Vendedores;
        private int _CurrentIndex;

        public ComisionRemisionForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        public virtual void ComisionRemisionForm_Load(object sender, EventArgs e) {
            this.GridRemisiones.Standard();
            this.GridMovimientos.Standard();

            this.GridRemisiones.AllowEditRow = this._Permisos.Autorizar;
            this.ContextMenuCrearRecibo.Enabled = this._Permisos.Agregar;

            using (var espera = new Waiting1Form(this.Vendedores)) {
                espera.Text = "Cargando lista de vendedores, espere un momento ...";
                espera.ShowDialog(this);
            }
            this.TComision.Cerrar.Click += this.TComision_Cerrar_Click;
            this.TComision.Actualizar.Click += this.TComision_Actualizar_Click;
            this.TComision.Filtro.Click += this.TComision_Filtrar_CheckStateChanged;
            this.TComision.Autosuma.Click += this.TComision_Autosuma_Click;
            this.TComision.Calcular.Click += this.TComision_Calcular_Click;
            this.TComision.Imprimir.Click += this.TComision_Imprimir_Click;
            this.TComision.cboVendedores.DataSource = this._Vendedores;
            this.Tag = null;
        }

        #region barra de herramientas
        private void TComision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Consultado ...";
                espera.ShowDialog(this);
            }

            this.GridRemisiones.DataSource = this._DataSource;
            this.GridMovimientos.DataMember = "Movimientos";
            this.GridMovimientos.DataSource = this._DataSource;
        }

        private void TComision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TComision_Filtrar_CheckStateChanged(object sender, EventArgs e) {
            var _toggle = ((CommandBarToggleButton)sender).ToggleState;
            this.GridRemisiones.ActivateFilter(_toggle);
            this.GridMovimientos.ActivateFilter(_toggle);
        }

        private void TComision_Autosuma_Click(object sender, EventArgs e) {
            this.GridRemisiones.AutoSum(!(this.TComision.Autosuma.ToggleState == ToggleState.On));
            this.GridMovimientos.AutoSum(!(this.TComision.Autosuma.ToggleState == ToggleState.On));
        }

        private void TComision_Imprimir_Click(object sender, EventArgs e) {
            if (this.GridRemisiones.ChildRows.Count > 0) {
                var seleccion = new List<RemisionComisionDetailModel>();
                seleccion.AddRange(this.GridRemisiones.ChildRows.Select(x => x.DataBoundItem as RemisionComisionDetailModel));
                var d = new RemisionComisionPrinter {
                    Vendedor = seleccion.FirstOrDefault().Vendedor,
                    Conceptos = seleccion
                };
                var imprimir = new ReporteForm(d);
                imprimir.Show();
            }
        }

        private void TComision_Calcular_Click(object sender, EventArgs e) {
            var selectedValue = this.TComision.cboVendedores.SelectedItem as GridViewRowInfo;
            if (selectedValue != null) {
                var _seleccionado = selectedValue.DataBoundItem as Vendedor2DetailModel;
                if (_seleccionado.IdComision <= 0) {
                    RadMessageBox.Show(this, "Properties.Resources.Msg_Comision_Vendedor_Error", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }

                if (this._CurrentIndex != _seleccionado.IdDirectorio) {
                    RadMessageBox.Show(this, "Properties.Resources.Msg_Comision_Vendedor_Error1", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    this.TComision.Actualizar.PerformClick();
                    return;
                }

                using (var espera = new Waiting1Form(this.CalcularComisiones)) {
                    espera.Text = "Calculando, espere ...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region menu contextual
        private void ContextMenuSeleccion_Click(object sender, EventArgs e) {
            this.ContextMenuSeleccion.IsChecked = !this.ContextMenuSeleccion.IsChecked;
            this.GridRemisiones.MultiSelect = this.ContextMenuSeleccion.IsChecked;
        }

        private void ContextMenuCopiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridMovimientos.CurrentCell.Value.ToString());
        }

        private void ContextMenuCrearRecibo_Click(object sender, EventArgs e) {
            var _seleccion = this.GridRemisiones.SelectedRows.Where(it => it.IsSelected == true).Select(x => x.DataBoundItem as RemisionComisionDetailModel).ToList();
            if (_seleccion != null) {
                if (_seleccion.Count > 0) {
                    if (_seleccion.Where(it => it.ComisionPagada == 0).Count() > 0) {
                        RadMessageBox.Show("Algunos documentos no tiene asignado una comisión por pagar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                    var _recibo = new MovimientoBancarioDetailModel {
                        FechaDocto = DateTime.Now,
                        FechaAplicacion = DateTime.Now
                    };
                    foreach (var item in _seleccion) {
                        _recibo.Comprobantes.Add(new MovimientoBancarioComprobanteDetailModel {
                            TipoComprobante = Domain.Banco.ValueObjects.TipoCFDIEnum.Ingreso,
                            TipoDocumento = Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision,
                            SubTipoComprobante = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido,
                            EmisorNombre = Aplication.Base.Services.ConfigService.Synapsis.Empresa.RazonSocial,
                            EmisorRFC = item.EmisorRFC,
                            ReceptorNombre = item.ReceptorNombre,
                            ReceptorRFC = item.ReceptorRFC,
                            Cargo = item.ComisionPagada,
                            IdDocumento = item.IdDocumento,
                            Folio = item.Folio.ToString(),
                            Serie = item.Serie,
                            Activo = true,
                            Total = item.Total,
                            Acumulado = item.Acumulado,
                            Estado = item.IdStatus > 0 ? "Vigente" : "Cancelado",
                            ClaveFormaPago = item.ClaveFormaPago,
                            ClaveMetodoPago = item.ClaveMetodoPago,
                            ClaveMoneda = item.ClaveMoneda,
                            FechaEmision = item.FechaEmision,
                            IdComprobante = item.IdRemision,
                            NumParcialidad = 0,
                            Version = item.Version
                        });
                    }
                    //var _nuevo = new ComisionReciboForm(_recibo) {
                    //    Text = "Recibo de comisión"
                    //};
                    //_nuevo.ShowDialog(this);
                }
            }
        }
        #endregion

        #region acciones del grid
        protected virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            TelerikGridExtension.ContextMenuOpening(this.GridRemisiones, e, this.MenuContextual);
        }

        private void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "ComisionPagada") {
                    var seleccionado = (RemisionComisionModel)this.GridRemisiones.ReturnRowSelected();
                    if (seleccionado != null) {
                    }
                }
            }
        }

        private void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "ComisionPagada") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.Aplicar)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.TComision.Tag != null) {
                            if ((bool)this.TComision.Tag == false) {
                                MessageBox.Show("Error");
                                this.TComision.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        private void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Row == null)
                return;

            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "ComisionPagada") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        } else {
                            e.Cancel = false;
                            e.Column.Tag = "Actualizar";
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Vendedores() {
            this._Vendedores = this.service.GetList(true);
        }

        private void Consultar() {
            var selectedValue = this.TComision.cboVendedores.SelectedItem as GridViewRowInfo;
            if (selectedValue != null) {
                var _seleccionado = selectedValue.DataBoundItem as Vendedor2DetailModel;
                this._CurrentIndex = _seleccionado.IdDirectorio;
                this._DataSource = this.service.GetComisionesPorPagar(this.TComision.GetEjercicio(), _seleccionado.IdDirectorio);
            }
        }

        private void Aplicar() {
            var seleccionado = (RemisionComisionDetailModel)this.GridRemisiones.ReturnRowSelected();
            this.TComision.Tag = this.service.Saveable(seleccionado).IdRemision > 0;
        }

        private void CalcularComisiones() {
            var selectedValue = this.TComision.cboVendedores.SelectedItem as GridViewRowInfo;
            var _seleccionado = selectedValue.DataBoundItem as Vendedor2DetailModel;
            var cat = this.service.GetComision(_seleccionado.IdComision);
            this.service.Calcular(cat, this._DataSource);
        }
        #endregion
    }
}
