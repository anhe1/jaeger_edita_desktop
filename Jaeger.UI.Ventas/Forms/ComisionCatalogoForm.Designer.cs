﻿namespace Jaeger.UI.Ventas.Forms
{
    partial class ComisionCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor3 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComisionCatalogoForm));
            this.dataGridCatalogo = new Telerik.WinControls.UI.RadGridView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridDescuento = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.dataGridMulta = new Telerik.WinControls.UI.RadGridView();
            this.TCatalogo = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridCatalogo
            // 
            this.dataGridCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridCatalogo.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridCatalogo.MasterTemplate.AllowAddNewRow = false;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "NewCondition";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            conditionalFormattingObject1.TValue1 = "0";
            conditionalFormattingObject1.TValue2 = "0";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Orden";
            gridViewTextBoxColumn1.FormatString = "{0:N0}";
            gridViewTextBoxColumn1.HeaderText = "Orden";
            gridViewTextBoxColumn1.Name = "Orden";
            gridViewTextBoxColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 300;
            gridViewTextBoxColumn3.DataType = typeof(decimal);
            gridViewTextBoxColumn3.FieldName = "VentaMinima";
            gridViewTextBoxColumn3.FormatString = "{0:N2}";
            gridViewTextBoxColumn3.HeaderText = "Venta Min.";
            gridViewTextBoxColumn3.Name = "VentaMinima";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.DataType = typeof(decimal);
            gridViewTextBoxColumn4.FieldName = "VentaMaxima";
            gridViewTextBoxColumn4.HeaderText = "Venta Máx.";
            gridViewTextBoxColumn4.Name = "VentaMaxima";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn4.Width = 75;
            gridViewDateTimeColumn1.FieldName = "FechaInicio";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Ini.";
            gridViewDateTimeColumn1.Name = "FechaInicio";
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 75;
            gridViewDateTimeColumn2.FieldName = "FechaFin";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Fin";
            gridViewDateTimeColumn2.Name = "FechaFin";
            gridViewDateTimeColumn2.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Nota";
            gridViewTextBoxColumn5.HeaderText = "Nota";
            gridViewTextBoxColumn5.Name = "Nota";
            gridViewTextBoxColumn5.Width = 200;
            gridViewTextBoxColumn6.FieldName = "Creo";
            gridViewTextBoxColumn6.HeaderText = "Creó";
            gridViewTextBoxColumn6.Name = "Creo";
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "FechaNuevo";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn7.Name = "FechaNuevo";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            this.dataGridCatalogo.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.dataGridCatalogo.MasterTemplate.EnableFiltering = true;
            this.dataGridCatalogo.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor1.PropertyName = "Orden";
            this.dataGridCatalogo.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.dataGridCatalogo.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridCatalogo.Name = "dataGridCatalogo";
            this.dataGridCatalogo.ShowGroupPanel = false;
            this.dataGridCatalogo.Size = new System.Drawing.Size(1115, 296);
            this.dataGridCatalogo.TabIndex = 1;
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 30);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1115, 595);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.dataGridCatalogo);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1115, 296);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 300);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1115, 295);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1115, 295);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.dataGridDescuento);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(556, 295);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // dataGridDescuento
            // 
            this.dataGridDescuento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridDescuento.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridDescuento.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "Inicio";
            gridViewTextBoxColumn8.FormatString = "{0:N4}";
            gridViewTextBoxColumn8.HeaderText = "Rango - 1";
            gridViewTextBoxColumn8.Name = "Inicio";
            gridViewTextBoxColumn8.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 75;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Final";
            gridViewTextBoxColumn9.FormatString = "{0:N4}";
            gridViewTextBoxColumn9.HeaderText = "Rango - 2";
            gridViewTextBoxColumn9.Name = "Final";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 75;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Factor";
            gridViewTextBoxColumn10.FormatString = "{0:N4}";
            gridViewTextBoxColumn10.HeaderText = "Factor";
            gridViewTextBoxColumn10.Name = "Factor";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 75;
            gridViewTextBoxColumn11.FieldName = "Descripcion";
            gridViewTextBoxColumn11.HeaderText = "Descripción";
            gridViewTextBoxColumn11.Name = "Descripcion";
            gridViewTextBoxColumn11.Width = 150;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.FieldName = "Creo";
            gridViewTextBoxColumn13.HeaderText = "Creó";
            gridViewTextBoxColumn13.Name = "Creo";
            gridViewTextBoxColumn13.Width = 65;
            this.dataGridDescuento.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.dataGridDescuento.MasterTemplate.EnableFiltering = true;
            this.dataGridDescuento.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor2.PropertyName = "Inicio";
            this.dataGridDescuento.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.dataGridDescuento.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridDescuento.Name = "dataGridDescuento";
            this.dataGridDescuento.ShowGroupPanel = false;
            this.dataGridDescuento.Size = new System.Drawing.Size(556, 295);
            this.dataGridDescuento.TabIndex = 2;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.dataGridMulta);
            this.splitPanel4.Location = new System.Drawing.Point(560, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(555, 295);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // dataGridMulta
            // 
            this.dataGridMulta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridMulta.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridMulta.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Inicio";
            gridViewTextBoxColumn14.FormatString = "{0:N4}";
            gridViewTextBoxColumn14.HeaderText = "Rango - 1";
            gridViewTextBoxColumn14.Name = "Inicio";
            gridViewTextBoxColumn14.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 75;
            gridViewTextBoxColumn15.DataType = typeof(decimal);
            gridViewTextBoxColumn15.FieldName = "Final";
            gridViewTextBoxColumn15.FormatString = "{0:N4}";
            gridViewTextBoxColumn15.HeaderText = "Rango - 2";
            gridViewTextBoxColumn15.Name = "Final";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn16.DataType = typeof(decimal);
            gridViewTextBoxColumn16.FieldName = "Factor";
            gridViewTextBoxColumn16.FormatString = "{0:N4}";
            gridViewTextBoxColumn16.HeaderText = "Factor";
            gridViewTextBoxColumn16.Name = "Factor";
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 75;
            gridViewTextBoxColumn17.FieldName = "Descripcion";
            gridViewTextBoxColumn17.HeaderText = "Descripción";
            gridViewTextBoxColumn17.Name = "Descripcion";
            gridViewTextBoxColumn17.Width = 150;
            gridViewTextBoxColumn18.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn18.FieldName = "FechaNuevo";
            gridViewTextBoxColumn18.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn18.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn18.Name = "FechaNuevo";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn18.Width = 75;
            gridViewTextBoxColumn19.FieldName = "Creo";
            gridViewTextBoxColumn19.HeaderText = "Creó";
            gridViewTextBoxColumn19.Name = "Creo";
            gridViewTextBoxColumn19.Width = 65;
            this.dataGridMulta.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.dataGridMulta.MasterTemplate.EnableFiltering = true;
            this.dataGridMulta.MasterTemplate.ShowFilteringRow = false;
            sortDescriptor3.PropertyName = "Inicio";
            this.dataGridMulta.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor3});
            this.dataGridMulta.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.dataGridMulta.Name = "dataGridMulta";
            this.dataGridMulta.ShowGroupPanel = false;
            this.dataGridMulta.Size = new System.Drawing.Size(555, 295);
            this.dataGridMulta.TabIndex = 3;
            // 
            // TCatalogo
            // 
            this.TCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCatalogo.Etiqueta = "";
            this.TCatalogo.Location = new System.Drawing.Point(0, 0);
            this.TCatalogo.Name = "TCatalogo";
            this.TCatalogo.ShowActualizar = true;
            this.TCatalogo.ShowAutorizar = false;
            this.TCatalogo.ShowCerrar = true;
            this.TCatalogo.ShowEditar = true;
            this.TCatalogo.ShowExportarExcel = false;
            this.TCatalogo.ShowFiltro = true;
            this.TCatalogo.ShowGuardar = false;
            this.TCatalogo.ShowHerramientas = true;
            this.TCatalogo.ShowImagen = false;
            this.TCatalogo.ShowImprimir = false;
            this.TCatalogo.ShowNuevo = true;
            this.TCatalogo.ShowRemover = true;
            this.TCatalogo.Size = new System.Drawing.Size(1115, 30);
            this.TCatalogo.TabIndex = 1;
            this.TCatalogo.Nuevo.Click += this.TCatalogo_Nuevo_Click;
            this.TCatalogo.Editar.Click += this.TCatalogo_Editar_Click;
            this.TCatalogo.Remover.Click += this.TCatalogo_Remover_Click;
            this.TCatalogo.Actualizar.Click += this.TCatalogo_Actualizar_Click;
            this.TCatalogo.Filtro.Click += this.TCatalogo_Filtro_Click;
            this.TCatalogo.Cerrar.Click += this.TCatalogo_Cerrar_Click;
            // 
            // ComisionCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 625);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.TCatalogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ComisionCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Catálogo de Comisiones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComisionCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView dataGridCatalogo;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView dataGridDescuento;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadGridView dataGridMulta;
        private Common.Forms.ToolBarStandarControl TCatalogo;
    }
}
