﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Ventas.Forms {
    public partial class ComisionForm : RadForm {
        protected IComisionService _Service;
        private ComisionDetailModel currentComision = null;

        public ComisionForm() {
            InitializeComponent();
        }

        public ComisionForm(IComisionService service, ComisionDetailModel model) {
            InitializeComponent();
            this._Service = service;
            this.currentComision = model;
        }

        private void ComisionForm_Load(object sender, EventArgs e) {
            this.dataGridDescuento.Standard();
            this.dataGridMulta.Standard();
            this.dataGridDescuento.AllowEditRow = true;
            this.dataGridMulta.AllowEditRow = true;
            this.TCatalogo.Actualizar.PerformClick();
        }

        private void TCatalogo_Actualizar_Click(object sender, EventArgs e) {
            if (this.currentComision == null) {
                this.currentComision = new ComisionDetailModel();
            }
            
            //this.Prioridad.DataSource = this.service.GetPrioridad();
            this.Prioridad.DisplayMember = "Descripcion";
            this.Prioridad.ValueMember = "Id";
            this.CreateBinding();
        }

        private void TCatalogo_Guardar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TCatalogo_Nuevo_Click(object sender, EventArgs e) {

        }

        private void TCatalogo_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.currentComision,"Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Nota.DataBindings.Clear();
            this.Nota.DataBindings.Add("Text", this.currentComision, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
            //this.Prioridad.DataBindings.Clear();
            //this.Prioridad.DataBindings.Add("SelectedValue", this.currentComision, "IdPrioridad", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Orden.DataBindings.Clear();
            this.Orden.DataBindings.Add("Value", this.currentComision, "Orden", true, DataSourceUpdateMode.OnPropertyChanged);
            this.VentaMinima.DataBindings.Clear();
            this.VentaMinima.DataBindings.Add("Text", this.currentComision,"VentaMinima", true, DataSourceUpdateMode.OnPropertyChanged);
            this.VentaMaxima.DataBindings.Clear();
            this.VentaMaxima.DataBindings.Add("Text", this.currentComision, "VentaMaxima", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaInicio.DataBindings.Clear();
            this.FechaInicio.DataBindings.Add("Value", this.currentComision, "FechaInicio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaFin.DataBindings.Clear();
            this.FechaFin.DataBindings.Add("Value", this.currentComision, "FechaFin", true, DataSourceUpdateMode.OnPropertyChanged);
            this.dataGridDescuento.DataSource = this.currentComision;
            this.dataGridMulta.DataSource = this.currentComision;
            this.dataGridDescuento.DataMember = "Descuento";
            this.dataGridMulta.DataMember = "Multa";
        }

        private void Guardar() {
            this._Service.Save(this.currentComision);
        }
    }
}
