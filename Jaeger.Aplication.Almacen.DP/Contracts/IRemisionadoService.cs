﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.DP.Contracts {
    public interface IRemisionadoService : IRemisionService {
        BindingList<T1> GetList<T1>(int year, int month = 0) where T1 : class, new();
        BindingList<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
