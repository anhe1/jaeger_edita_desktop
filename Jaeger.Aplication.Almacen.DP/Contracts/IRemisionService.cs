﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.DP.Entities;

namespace Jaeger.Aplication.Almacen.DP.Contracts {
    public interface IRemisionService {
        RemisionDetailModel GetNew();

        RemisionDetailModel GetRemision(int index);

        RemisionDetailModel Save(RemisionDetailModel model);

        bool Cancelar(RemisionCancelacionModel model);

        bool SetStatus(RemisionStatusModel model);

        RemisionDetailPrinter GetPrinter(int index);

        BindingList<RemisionConceptoDetailModel> GetPartidas(int index);
    }
}
