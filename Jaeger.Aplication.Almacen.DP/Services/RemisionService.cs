﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jaeger.Domain.Almacen.DP.Builder;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Almacen.DP.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Almacen.DP.Services {
    public class RemisionService {
        #region metodos estaticos
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, RemisionStatusEnum>().ToList();
        }

        public static List<RemisionUsoModel> GetUsos() {
            return EnumerationExtension.GetEnumToClass<RemisionUsoModel, RemisionUsoEnum>().ToList();
        }

        public static List<RemisionMetodoEnvioModel> GetMetodoEnvio() {
            return new List<RemisionMetodoEnvioModel>() {
                new RemisionMetodoEnvioModel(0, "No necesario"),
                new RemisionMetodoEnvioModel(1, "Transporte Interno"),
                new RemisionMetodoEnvioModel(2, "Transporte Externo"),
            };
        }

        public static RemisionMetodoEnvioModel GetMetodoEnvio(int index) {
            var response = GetMetodoEnvio();
            return response.Where(it => it.Id == index).FirstOrDefault();
        }

        public static List<RemisionMotivoCancelacionModel> GetMotivosCancelacion() {
            return EnumerationExtension.GetEnumToClass<RemisionMotivoCancelacionModel, RemisionMotivoCancelacionEnum>().ToList();
        }
        #endregion

        #region metodos estaticos
        public static IRemisionQueryBuilder Query() {
            return new RemisionQueryBuilder();
        }
        #endregion
    }
}
