﻿using System;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Abstractions {
    public abstract class Categoria : Clasificacion, ICategoriaModel {
        public Categoria() {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        public new int IdCategoria {
            get {
                return base.IdCategoria;
            }
            set { base.IdCategoria = value; }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public new bool Activo {
            get {
                return base.Activo;
            }
            set { base.Activo = value; }
        }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        public new int IdSubCategoria {
            get {
                return base.IdSubCategoria;
            }
            set { base.IdSubCategoria = value; }
        }

        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        public new int IdAlmacen {
            get {
                return base.IdAlmacen;
            }
            set { base.IdAlmacen = value; }
        }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        public new int Secuencia {
            get {
                return base.Secuencia;
            }
            set { base.Secuencia = value; }
        }

        /// <summary>
        /// obtener o establecer clave de la categoria
        /// </summary>
        public new string Clave {
            get {
                return base.Clave;
            }
            set { base.Clave = value; }
        }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        public new string Descripcion {
            get { return base.Descripcion; }
            set { base.Descripcion = value; }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador de la categoria
        /// </summary>
        public new string Creo {
            get {
                return base.Creo;
            }
            set { base.Creo = value; }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion 
        /// </summary>
        public new DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set { base.FechaNuevo = value; }
        }

        /// <summary>
        /// obtener descripcion de la categoria
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public override string Descriptor {
            get { return string.Format("{0}.{1}", this.IdCategoria.ToString("#00"), this.IdSubCategoria.ToString("#00")); }
        }
    }
}
