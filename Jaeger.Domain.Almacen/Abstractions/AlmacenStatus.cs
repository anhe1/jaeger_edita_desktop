﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Abstractions {
    public abstract class AlmacenStatus : BasePropertyChangeImplementation {
        public AlmacenStatus() {
            this.FechaNuevo = DateTime.Now;
        }

        public int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del documento antes del cambio
        /// </summary>
        public int IdStatus { get; set; }
        
        /// <summary>
        /// obtener o establecer el indice del status autorizado
        /// </summary>
        public int IdStatusB { get; set; }

        /// <summary>
        /// obtener o establecer indice de la clave del motivo de modificacion del status
        /// </summary>
        public int IdCveMotivo { get; set; }
        
        /// <summary>
        /// obtener o establecer desripcion del motivo del cambio de status (clave+descripcion)
        /// </summary>
        public string CveMotivo { get; set; }
        
        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        public string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        public DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        public string Creo { get; set; }
    }
}
