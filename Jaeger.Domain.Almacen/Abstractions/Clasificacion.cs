﻿using System;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Abstractions {
    /// <summary>
    /// clase abstracta para categorias
    /// </summary>
    public abstract class Clasificacion : BasePropertyChangeImplementation, IClasificacionModel {
        public Clasificacion() {
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        public int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        public int IdSubCategoria { get; set; }

        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        public int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        public int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer clave de la categoria
        /// </summary>
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador de la categoria
        /// </summary>
        public string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion 
        /// </summary>
        public DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener descripcion de la categoria
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public virtual string Descriptor {
            get { return string.Format("{0}.{1}", this.IdCategoria.ToString("#00"), this.IdSubCategoria.ToString("#00")); }
        }
    }
}
