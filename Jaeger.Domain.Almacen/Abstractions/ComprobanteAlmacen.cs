﻿using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Abstractions {
    public abstract class ComprobanteAlmacen : Base.Abstractions.BasePropertyChangeImplementation, IComprobanteAlmacen {
        public int IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer si el objeto es de solo lectura
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// obtener o establecer el documento debe afectar los saldos
        /// </summary>
        public bool IsSum { get; set; }

        public AlmacenEnum Tipo { get; set; }

        public TipoMovimientoEnum TipoMovimiento { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del tipo de operacion
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer el formato de impresion del documento
        /// </summary>
        public string FormatoImpreso { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia de tipos de objetos utilizados como receptores del tipo de movimiento
        /// </summary>
        public string Receptores { get; set; }

        /// <summary>
        /// obtener el descriptor del tipo de documento {Clave:Descripcion}
        /// </summary>
        public abstract string Descriptor { get; }
    }
}
