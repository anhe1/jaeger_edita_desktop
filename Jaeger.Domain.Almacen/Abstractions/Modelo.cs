﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Abstractions {
    public abstract class Modelo : BasePropertyChangeImplementation {
        /// <summary>
        /// obtener o establecer indice del modelo
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer indice del producto
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de categorias
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer descripcion del modelo
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected string Descripcion { get; set; }
        
        /// <summary>
        /// obtener o establecer marca
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected string Marca { get; set; }

        /// <summary>
        /// obtener o establecer especificacion
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected string Especificacion { get; set; }

        /// <summary>
        /// obtener o establecer codigo de barras
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected string Codigo { get; set; }
    }
}
