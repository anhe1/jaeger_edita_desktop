﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Abstractions {
    public abstract class Producto : BasePropertyChangeImplementation {
        /// <summary>
        /// obtener o establecer indice del producto
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de categorias
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        [SqlSugar.SugarColumn(IsIgnore = true)]
        protected string Nombre { get; set; }
    }
}
