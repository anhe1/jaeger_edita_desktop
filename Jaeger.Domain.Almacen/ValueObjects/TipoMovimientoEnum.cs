﻿/// develop:
/// purpose: almacen de materia prima: tipos de movientos para los registros de los productos del almacen
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.ValueObjects {
    /// <summary>
    /// obtener o establecer tipo de movimiento del almacen
    /// </summary>
    public enum TipoMovimientoEnum {
        [Description("No definido")]
        None = 0,
        [Description("Ingreso")]
        Ingreso = 1,
        [Description("Egreso")]
        Egreso = 2,
        [Description("Traslado")]
        Traslado = 3
    }
}
