﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.ValueObjects {
    /// <summary>
    /// visibilidad en la tienda web
    /// </summary>
    public enum VisibilidadEnum {
        [Description("Ninguno")]
        Ninguno = 0,
        [Description("Mayoristas")]
        Distribuidores = 1,
        [Description("Minoristas")]
        TiendaWeb = 2,
        [Description("Ambos")]
        Ambos = 3
    }
}
