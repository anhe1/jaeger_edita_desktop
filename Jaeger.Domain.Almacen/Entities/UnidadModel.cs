﻿/// develop: anhe1 081120191127
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("CTUND", "almacen de materia prima: catalogo de unidades")]
    public class UnidadModel : Unidad, Base.Contracts.IUnidad, Base.Contracts.IEntityBase, IUnidadModel {
        #region declaraciones
        private string creo;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string _modifica;
        private int _IdAlmacen;
        private string _ClvUnidadSAT;
        #endregion

        public UnidadModel() {
            this.Activo = true;
            this.fechaNuevo = DateTime.Now;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice de la unidad
        /// </summary>
        [DataNames("ctund_id", "CTUND_ID")]
        [SugarColumn(ColumnName = "ctund_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdUnidad {
            get {
                return base.IdUnidad; 
            }
            set {
                base.IdUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("ctund_a", "CTUND_A")]
        [SugarColumn(ColumnName = "ctund_a", ColumnDescription = "registro activo", Length = 1)]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de almacen
        /// </summary>
        [DataNames("ctund_alm_id", "CTUND_ALM_ID")]
        [SugarColumn(ColumnName = "ctund_alm_id", ColumnDescription = "indice del almacen al que pertenece la unidad")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set { this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la unidad
        /// </summary>
        [DataNames("ctund_nom", "CTUND_NOM")]
        [SugarColumn(ColumnName = "ctund_nom", ColumnDescription = "nombre de la unidad", Length = 32)]
        public new string Descripcion {
            get {
                return base.Descripcion;
            }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor de conversion de unidad
        /// </summary>
        [DataNames("ctund_fac", "CTUND_FAC")]
        [SugarColumn(ColumnName = "ctund_fac", ColumnDescription = "factor de la unidad", Length = 16, DecimalDigits = 4)]
        public new decimal Factor {
            get { return base.Factor; }
            set {
                base.Factor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DataNames("ctund_clvund", "CTUND_CLVUND")]
        [SugarColumn(ColumnName = "ctund_clvund", ColumnDescription = "clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.", Length = 3)]
        public string ClaveUnidad {
            get {
                return this._ClvUnidadSAT;
            }
            set {
                this._ClvUnidadSAT = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ctund_usr_n", "CTUND_USR_N")]
        [SugarColumn(ColumnName = "ctund_usr_n", ColumnDescription = "clave del usuario que creo el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("ctund_fn", "CTUND_FN")]
        [SugarColumn(ColumnName = "ctund_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("ctund_usr_m", "CTUND_USR_M")]
        [SugarColumn(ColumnName = "ctund_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this._modifica; }
            set {
                this._modifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LSTSYS_FM)
        /// </summary>
        [DataNames("ctund_fm ", "CTUND_FM")]
        [SugarColumn(ColumnName = "ctund_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true, IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region propiedades ignoradas
        /// <summary>
        /// obtener o establecer el almacen al que pertenece la unidad
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum Almacen {
            get {
                if (this.IdAlmacen == 1)
                    return AlmacenEnum.MP;
                else if (this.IdAlmacen == 2)
                    return AlmacenEnum.PT;
                return AlmacenEnum.NA;
            }
            set {
                this.IdAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new string Descriptor {
            get { return base.Descriptor; }
        }

        [SugarColumn(IsIgnore = true)]
        public bool SetModified { get; set; }
        #endregion
    }
}
