﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// modelos, publicacion para la tienda web
    /// </summary>
    [SugarTable("CTMDLT")]
    public class PublicacionModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _IdModelo;
        private int _Secuencia;
        private string _Contenido;
        private string _Descripcion;
        private string _Etiquetas;
        private string _Modifica;
        private DateTime? _FechaModifica;
        private bool _IsAvariable;
        private int _IdCategoria;
        #endregion

        public PublicacionModel() { }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("CTMDLT_CTMDL_ID")]
        [SugarColumn(ColumnName = "CTMDLT_CTMDL_ID", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true)]
        public int IdModelo {
            get {
                return this._IdModelo;
            }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el modelo esta disponible
        /// </summary>
        [DataNames("CTMDLT_VIS")]
        [SugarColumn(ColumnName = "CTMDLT_VIS", ColumnDescription = "", DefaultValue = "0", IsNullable = false, IsIgnore = true)]
        public bool IsAvailable {
            get { return this._IsAvariable; }
            set {
                this._IsAvariable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        [DataNames("CTMDLT_SEC")]
        [SugarColumn(ColumnName = "CTMDLT_SEC", ColumnDescription = "indice de relacion con la tabla de categorias", IsNullable = false, IsIgnore = true)]
        public int Secuencia {
            get { return this._Secuencia; }
            set {
                this._Secuencia = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLT_CTCLS_ID")]
        [SugarColumn(ColumnName = "CTMDLT_CTCLS_ID", ColumnDescription = "", IsNullable = false, IsIgnore = true)]
        public int IdCategoria {
            get { return this._IdCategoria; }
            set { this._IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// </summary>           
        [DataNames("CTMDLT_DSCRC")]
        [SugarColumn(ColumnName = "CTMDLT_DSCRC", ColumnDescription = "alm:|mp,pt,tw| desc: descripcion corta", Length = 1000, IsNullable = true)]
        public string Descripcion {
            get {
                return this._Descripcion;
            }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Etiquetas alm:|mp,pt,tw| desc: etiquetas de busqueda
        /// </summary>           
        [DataNames("CTMDLT_ETQTS")]
        [SugarColumn(ColumnName = "CTMDLT_ETQTS", ColumnDescription = "alm:|mp,pt,tw| desc: etiquetas de busqueda", Length = 254, IsNullable = true)]
        public string Etiquetas {
            get {
                return this._Etiquetas;
            }
            set {
                this._Etiquetas = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLT_DSCR")]
        [SugarColumn(ColumnName = "CTMDLT_DSCR", ColumnDescription = "indice principal de la tabla")]
        public string Contenido {
            get { return this._Contenido; }
            set {
                this._Contenido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que modifica
        /// </summary>           
        [DataNames("CTMDLT_USR_M")]
        [SugarColumn(ColumnName = "CTMDLT_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true)]
        public string Modifica {
            get {
                return this._Modifica;
            }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLT_FM")]
        [SugarColumn(ColumnName = "CTMDLT_FM", ColumnDescription = "fecha de creacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this._FechaModifica >= firstGoodDate)
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
