﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// modelo de detalle de modelo, incluye lista de medios
    /// </summary>
    [SugarTable("CTMDL", "catalogo de modelos de productos")]
    public class ModeloDetailModel : ModeloModel, IModeloDetailModel, IModeloModel, ICloneable, IDataErrorInfo {
        #region declaraciones
        private ModeloExistenciaModel _Existencia;
        private BindingList<MedioModel> _Medios;
        private BindingList<ModeloProveedorModel> _Proveedores;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ModeloDetailModel() {
            this.FechaNuevo = DateTime.Now;
            this.Activo = true;
            this._Medios = new BindingList<MedioModel>();
            this._Existencia = new ModeloExistenciaModel();
            this._Proveedores = new BindingList<ModeloProveedorModel>();
        }

        /// <summary>
        /// obtener o establecer el almacen al que pertenece Materia Prima o Producto terminado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum Almacen {
            get {
                if (this.IdAlmacen == 1)
                    return AlmacenEnum.MP;
                else
                    return AlmacenEnum.PT;
            }
            set {
                this.IdAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es un producto o servicio
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ProdServTipoEnum Tipo {
            get {
                if (this.IdTipo == (int)ProdServTipoEnum.Producto)
                    return ProdServTipoEnum.Producto;
                else if (this.IdTipo == (int)ProdServTipoEnum.Producto)
                    return ProdServTipoEnum.Servicio;
                else
                    return ProdServTipoEnum.NoDefinido;
            }
            set {
                this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string AutorizadoText {
            get {
                if (this.Autorizado > 0)
                    return "Autorizado";
                else
                    return "Pendiente";
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<MedioModel> Medios {
            get {
                return this._Medios;
            }
            set {
                this._Medios = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ModeloExistenciaModel Existencias {
            get { return this._Existencia; }
            set { this._Existencia = value; }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ModeloProveedorModel> Proveedores {
            get { return this._Proveedores; }
            set { this._Proveedores = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTESPC_NOM")]
        [SugarColumn(IsIgnore = true)]
        public string Variante { get; set; }

        #region exitencias
        /// <summary>
        /// Desc:existencia
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("CTMDLX_EXT")]
        [SugarColumn(ColumnName = "CTMDLX_EXT", ColumnDescription = "obtener o establecer la cantidad de la existencia", Length = 11, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal Existencia {
            get {
                return this._Existencia.Existencia;
            }
            set {
                this._Existencia.Existencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock minimo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("CTMDLX_MIN")]
        [SugarColumn(ColumnName = "CTMDLX_MIN", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el stock minimo del almacen", Length = 11, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal Minimo {
            get {
                return this._Existencia.Minimo;
            }
            set {
                this._Existencia.Minimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Stock maximo
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("CTMDLX_MAX")]
        [SugarColumn(ColumnName = "CTMDLX_MAX", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el stock maximo del almacen", Length = 11, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal Maximo {
            get {
                return this._Existencia.Maximo;
            }
            set {
                this._Existencia.Maximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reorden
        /// Default:0.0000
        /// Nullable:True
        /// </summary>           
        [DataNames("CTMDLX_REORD")]
        [SugarColumn(ColumnName = "CTMDLX_REORD", ColumnDescription = "alm:|mp,pt,tw| desc: obtener o establecer el stock maximo del almacen", Length = 11, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal ReOrden {
            get {
                return this._Existencia.ReOrden;
            }
            set {
                this._Existencia.ReOrden = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region metodos
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (this.Unitario <= 0 | this.IdTipo == 0) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "Unitario") {
                    return "¡Valor unitario no válido!";
                } else if (columnName == "IdTipo") {
                    return "No se selecciono el tipo de Producto o Servicio";
                }
                return string.Empty;
            }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
        #endregion
    }
}
