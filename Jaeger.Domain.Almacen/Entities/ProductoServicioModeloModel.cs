﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("CTPRD")]
    public class ProductoServicioModeloModel : ModeloDetailModel, IModeloDetailModel, IModeloModel, IProductoServicioModel {
        private string _Producto;

        public ProductoServicioModeloModel() : base() { }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        [DataNames("CTPRD_NOM")]
        [SugarColumn(ColumnName = "CTPRD_NOM", ColumnDescription = "descripcion del producto", Length = 128)]
        public string Nombre {
            get {
                return this._Producto;
            }
            set {
                this._Producto = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string ProductoDescripcion {
            get {
                if (this.Nombre != null)
                    return string.Format("{0} {1}", this.Nombre, this.Descripcion);
                return this.Descripcion;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new string NoIdentificacion {
            get {
                if (string.IsNullOrEmpty(base.NoIdentificacion))
                    return string.Format("MP-{0:00}-{1:0000}-{2:00000}", this.IdCategoria, this.IdProducto, this.IdModelo);
                return base.NoIdentificacion;
            }
        }
    }
}
