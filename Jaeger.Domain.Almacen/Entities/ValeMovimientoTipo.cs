﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    public class ValeMovimientoTipo : BaseSingleModel {
        public ValeMovimientoTipo() : base() {

        }

        public ValeMovimientoTipo(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
