﻿using System;
using SqlSugar;
using System.Text.RegularExpressions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;
///CTMDL_RETISRF Factor de IESP Retenido  Varchar(6)
///CTMDL_RETIVAF Varchar(5) Factor IVA Retenido 
namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// modelo de detalle de modelo, incluye lista de medios
    /// </summary>
    [SugarTable("CTMDL", "catalogo de modelos de productos")]
    public class ModeloModel : Abstractions.Modelo, IModeloModel {
        #region declaraciones
        private int _IdAlmacen;
        private int autorizado;
        private bool activo;
        private int idPrecio;
        private int tipoInt;
        private int visible;
        private int unidadXY;
        private int unidadZ;
        private int idUnidad;
        private decimal unitario;
        private string factorTrasladoIVA;
        private decimal? valorTrasladoIVA;
        private string factorTrasladoIEPS;
        private decimal? valorTrasladoIEPS;
        private decimal? valorRetencionIVA;
        private string factorRetencionIEPS;
        private decimal? valorRetencionIEPS;
        private decimal? valorRetecionISR;
        private decimal largo;
        private decimal ancho;
        private decimal alto;
        private decimal peso;
        private string claveUnidad;
        private string claveProdServ;
        private string unidad;
        private string numRequerimiento;
        private string ctaPredial;
        private string noIdentificacion;
        private string creo;
        private string modifica;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string _clave;
        private string _claveObjetoImpuesto;
        private int _Secuencia;
        private bool _IsAvariable;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ModeloModel() {
            this.FechaNuevo = DateTime.Now;
            this.Activo = true;
            this.unitario = 0;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla _ctlmdl_id
        /// </summary>
        [DataNames("CTMDL_ID")]
        [SugarColumn(ColumnName = "CTMDL_ID", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdModelo {
            get {
                return base.IdModelo;
            }
            set {
                base.IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("CTMDL_A")]
        [SugarColumn(ColumnName = "CTMDL_A", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>           
        [DataNames("CTMDL_CTCLS_ID")]
        [SugarColumn(ColumnName = "CTMDL_CTCLS_ID", ColumnDescription = "indice de relacion con la tabla de categorias", IsNullable = false)]
        public new int IdCategoria {
            get {
                return base.IdCategoria;
            }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de productos
        /// </summary>
        [DataNames("CTMDL_CTPRD_ID")]
        [SugarColumn(ColumnName = "CTMDL_CTPRD_ID", ColumnDescription = "indice de relacion con la tabla de productos", IsNullable = false)]
        public new int IdProducto {
            get {
                return base.IdProducto;
            }
            set {
                base.IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de almacen
        /// </summary>
        [DataNames("CTMDL_ALM_ID")]
        [SugarColumn(ColumnName = "CTMDL_ALM_ID", ColumnDescription = "almacen al que pertenece")]
        public int IdAlmacen {
            get {
                return this._IdAlmacen;
            }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distinción de producto o servicio.
        /// </summary>
        [DataNames("CTMDL_TIPO")]
        [SugarColumn(ColumnName = "CTMDL_TIPO", ColumnDescription = "tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distinción de producto o servicio.")]
        public int IdTipo {
            get {
                return this.tipoInt;
            }
            set {
                this.tipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// </summary>           
        [DataNames("CTMDL_DSCRC")]
        [SugarColumn(ColumnName = "CTMDL_DSCRC", ColumnDescription = "descripcion del modelo o producto", Length = 1000, IsNullable = true)]
        public new string Descripcion {
            get {
                return base.Descripcion;
            }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer marca del producto
        /// </summary>
        [DataNames("CTMDL_MRC")]
        [SugarColumn(ColumnName = "CTMDL_MRC", ColumnDescription = "marca o fabricante", Length = 128, IsNullable = true)]
        public new string Marca {
            get {
                return base.Marca;
            }
            set {
                base.Marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer especificacion del modelo
        /// </summary>
        [DataNames("CTMDL_ESPC")]
        [SugarColumn(ColumnName = "CTMDL_ESPC", ColumnDescription = "espcificaciones", IsNullable = true, Length = 128)]
        public new string Especificacion {
            get {
                return base.Especificacion;
            }
            set {
                base.Especificacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDL_LARGO")]
        [SugarColumn(ColumnName = "CTMDL_LARGO", ColumnDescription = "largo", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Largo {
            get {
                return this.largo;
            }
            set {
                this.largo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        [DataNames("CTMDL_ANCHO")]
        [SugarColumn(ColumnName = "CTMDL_ANCHO", ColumnDescription = "ancho", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Ancho {
            get {
                return this.ancho;
            }
            set {
                this.ancho = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en largo y ancho
        /// </summary>
        [DataNames("CTMDL_UNDDXY")]
        [SugarColumn(ColumnName = "CTMDL_UNDDXY", ColumnDescription = "indice de la unidad utilizada en largo y ancho", IsNullable = false)]
        public int UnidadXY {
            get {
                return this.unidadXY;
            }
            set {
                this.unidadXY = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        [DataNames("CTMDL_ALTO")]
        [SugarColumn(ColumnName = "CTMDL_ALTO", ColumnDescription = "alto o calibre", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Alto {
            get {
                return this.alto;
            }
            set {
                this.alto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad utilizada en alto o calibre
        /// </summary>
        [DataNames("CTMDL_UNDDZ")]
        [SugarColumn(ColumnName = "CTMDL_UNDDZ", ColumnDescription = "indice de la unidad utiliza en alto o calibre eje Z", IsNullable = false)]
        public int UnidadZ {
            get {
                return this.unidadZ;
            }
            set {
                this.unidadZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        [DataNames("CTMDL_PESO")]
        [SugarColumn(ColumnName = "CTMDL_PESO", ColumnDescription = "alto o calibre", IsNullable = true, Length = 14, DecimalDigits = 4)]
        public decimal Peso {
            get {
                return this.peso;
            }
            set {
                this.peso = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDL_CTPRC_ID")]
        [SugarColumn(ColumnName = "CTMDL_CTPRC_ID", ColumnDescription = "indice de relacion con la tabla de precios", IsNullable = true)]
        public int IdPrecio {
            get {
                return this.idPrecio;
            }
            set {
                this.idPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        [DataNames("CTMDL_UNTR")]
        [SugarColumn(ColumnName = "CTMDL_UNTR", ColumnDescription = "precio unitario", Length = 11, DecimalDigits = 4)]
        public decimal Unitario {
            get {
                return this.unitario;
            }
            set {
                this.unitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [DataNames("CTMDL_UNDDA")]
        [SugarColumn(ColumnName = "CTMDL_UNDDA", ColumnDescription = "indice de la unidad de almacenamiento en el el almacen", IsNullable = false)]
        public int IdUnidad {
            get {
                return this.idUnidad;
            }
            set {
                this.idUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [DataNames("CTMDL_UNDD")]
        [SugarColumn(ColumnName = "CTMDL_UNDD", ColumnDescription = "unidad personalizada", Length = 20, IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Código de barras
        /// </summary>           
        [DataNames("CTMDL_CDG")]
        [SugarColumn(ColumnName = "CTMDL_CDG", ColumnDescription = "codigo de barras", Length = 12, IsNullable = true)]
        public new string Codigo {
            get {
                return base.Codigo;
            }
            set {
                base.Codigo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDL_ATRZD")]
        [SugarColumn(ColumnName = "CTMDL_ATRZD", ColumnDescription = "precio autorizado", DefaultValue = "0")]
        public int Autorizado {
            get { return this.autorizado; }
            set {
                this.autorizado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        [DataNames("CTMDL_SEC")]
        [SugarColumn(ColumnName = "CTMDL_SEC", ColumnDescription = "indice de relacion con la tabla de categorias", IsNullable = false, IsIgnore = true)]
        public int Secuencia {
            get { return this._Secuencia; }
            set {
                this._Secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)
        /// </summary>
        [DataNames("CTMDL_VIS")]
        [SugarColumn(ColumnName = "CTMDL_VIS", ColumnDescription = "para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)")]
        public int IdVisible {
            get {
                return this.visible;
            }
            set {
                this.visible = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el modelo esta disponible
        /// </summary>
        [DataNames("CTMDL_DIS")]
        [SugarColumn(ColumnName = "CTMDL_DIS", ColumnDescription = "", DefaultValue = "0", IsNullable = false, IsIgnore = true)]
        public bool IsAvailable {
            get { return this._IsAvariable; }
            set {
                this._IsAvariable = value;
                this.OnPropertyChanged();
            }
        }

        #region impuestos
        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        [DataNames("CTMDL_TRSIVAF")]
        [SugarColumn(ColumnName = "CTMDL_TRSIVAF", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IVA", IsNullable = true, Length = 6)]
        public string FactorTrasladoIVA {
            get {
                if (this.factorTrasladoIVA == "N/A")
                    return null;
                return this.factorTrasladoIVA;
            }
            set {
                if (value == "N/A")
                    this.ValorTrasladoIVA = null;
                this.factorTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        [DataNames("CTMDL_TRSIVA")]
        [SugarColumn(ColumnName = "CTMDL_TRSIVA", ColumnDescription = "obtener o establecer el valor del impuesto trasladado IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIVA {
            get {
                return this.valorTrasladoIVA;
            }
            set {
                this.valorTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA ó CUOTA
        /// </summary>
        [DataNames("CTMDL_TRSIEPSF")]
        [SugarColumn(ColumnName = "CTMDL_TRSIEPSF", ColumnDescription = "obtener o establecer el factor del impuesto trasladado IEPS", IsNullable = true, Length = 6)]
        public string FactorTrasladoIEPS {
            get {
                if (this.factorTrasladoIEPS == "N/A")
                    return null;
                return this.factorTrasladoIEPS;
            }
            set {
                if (value == "N/A")
                    this.ValorTrasladoIEPS = null;
                this.factorTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        [DataNames("CTMDL_TRSIEPS")]
        [SugarColumn(ColumnName = "CTMDL_TRSIEPS", ColumnDescription = "obtener o establecer el valor del impuesto trasladado IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorTrasladoIEPS {
            get {
                return this.valorTrasladoIEPS;
            }
            set {
                this.valorTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        [DataNames("CTMDL_RETIVA")]
        [SugarColumn(ColumnName = "CTMDL_RETIVA", ColumnDescription = "obtener o establecer el valor del impuesto retenido IVA", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIVA {
            get {
                return this.valorRetencionIVA;
            }
            set {
                this.valorRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA ó CUOTA
        /// </summary>
        [DataNames("CTMDL_RETIEPSF")]
        [SugarColumn(ColumnName = "CTMDL_RETIEPSF", ColumnDescription = "obtener o establecer el factor del impuesto retenido IEPS", IsNullable = true, Length = 6)]
        public string FactorRetencionIEPS {
            get {
                if (this.factorRetencionIEPS == "N/A")
                    return null;
                return this.factorRetencionIEPS;
            }
            set {
                if (value == "N/A")
                    this.ValorRetencionIEPS = null;
                this.factorRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA ó RANGO si el factor es CUOTA
        /// </summary>
        [DataNames("CTMDL_RETIEPS")]
        [SugarColumn(ColumnName = "CTMDL_RETIEPS", ColumnDescription = "obtener o establecer el valor del impuesto retenido IEPS", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetencionIEPS {
            get {
                return this.valorRetencionIEPS;
            }
            set {
                this.valorRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        [DataNames("CTMDL_RETISR")]
        [SugarColumn(ColumnName = "CTMDL_RETISR", ColumnDescription = "obtener o establecer el valor del impuesto retenido ISR", IsNullable = true, Length = 6, DecimalDigits = 6)]
        public decimal? ValorRetecionISR {
            get {
                return this.valorRetecionISR;
            }
            set {
                this.valorRetecionISR = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region claves SAT
        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DataNames("CTMDL_CLVUND")]
        [SugarColumn(ColumnName = "CTMDL_CLVUND", ColumnDescription = "clave de unidad SAT", Length = 3, IsNullable = true)]
        public string ClaveUnidad {
            get {
                return this.claveUnidad;
            }
            set {
                this.claveUnidad = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [DataNames("CTMDL_CLVPRDS")]
        [SugarColumn(ColumnName = "CTMDL_CLVPRDS", ColumnDescription = "clave del producto o del servicio SAT", Length = 8, IsNullable = true)]
        public string ClaveProdServ {
            get {
                return this.claveProdServ;
            }
            set {
                if (value == null)
                    this.claveProdServ = "";
                else
                    this.claveProdServ = Regex.Replace(value.ToString(), "[^\\d]", "");
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        [DataNames("CTMDL_CLVOBJ")]
        [SugarColumn(ColumnName = "CTMDL_CLVOBJ", ColumnDescription = "establecer si la operacion es objeto o no de impuesto", Length = 2)]
        public string ObjetoImp {
            get { return this._claveObjetoImpuesto; }
            set {
                this._claveObjetoImpuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número del pedimento que ampara la importación del bien que se expresa en el siguiente formato: 
        /// últimos 2 dígitos del año de validación seguidos por dos espacios, 2 dígitos de la aduana de despacho seguidos por dos espacios, 
        /// 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo 
        /// que se trate de un pedimento consolidado iniciado en el año inmediato anterior o del pedimento original de una rectificación, 
        /// seguido de 6 dígitos de la numeración progresiva por aduana.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}" longitud: 21
        /// </summary>
        [DataNames("CTMDL_NUMREQ")]
        [SugarColumn(ColumnName = "CTMDL_NUMREQ", ColumnDescription = "numero de requerimiento de aduana", Length = 50, IsNullable = true)]
        public string NumRequerimiento {
            get {
                return this.numRequerimiento;
            }
            set {
                this.numRequerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para asentar el número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar 
        /// los datos de identificación del certificado de participación inmobiliaria no amortizable.
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado de participación 
        /// inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}", Longitud: 21
        /// </summary>
        [DataNames("CTMDL_CTAPRE")]
        [SugarColumn(ColumnName = "CTMDL_CTAPRE", ColumnDescription = "número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificación del certificado de participación inmobiliaria no amortizable.", Length = 20, IsNullable = true)]
        public string CtaPredial {
            get {
                return this.ctaPredial;
            }
            set {
                this.ctaPredial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del estándar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,100}"
        [DataNames("CTMDL_SKU")]
        [SugarColumn(ColumnName = "CTMDL_SKU", ColumnDescription = "desc: obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor (en cfdi: NoIdentificacion)", Length = 100, IsNullable = true)]
        public string NoIdentificacion {
            get {
                if (string.IsNullOrEmpty(this.noIdentificacion))
                    return string.Format("{0}-{1:00}-{2:0000}-{3:00000}", Enum.GetName(typeof(AlmacenEnum), this.IdAlmacen), this.IdCategoria, this.IdProducto, this.IdModelo);
                return this.noIdentificacion;
            }
            set {
                this.noIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsExistSKU {
            get { return string.IsNullOrEmpty(this.noIdentificacion); }
        }
        #endregion

        #region control
        /// <summary>
        /// Desc:clave del usuario que crea el registro
        /// </summary>           
        [DataNames("CTMDL_USR_N")]
        [SugarColumn(ColumnName = "CTMDL_USR_N", ColumnDescription = "clave del usuario que crea el registro", IsNullable = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("CTMDL_FN")]
        [SugarColumn(ColumnName = "CTMDL_FN", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave del usuario que modifica
        /// </summary>           
        [DataNames("CTMDL_USR_M")]
        [SugarColumn(ColumnName = "CTMDL_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDL_FM")]
        [SugarColumn(ColumnName = "CTMDL_FM", ColumnDescription = "fecha de creacion del registro", IsNullable = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region campos a eliminar
        /// <summary>
        /// obtener o establecer alm:|pt,tw| desc: clave del modelo (url)
        /// </summary>
        [DataNames("CTMDL_CLV")]
        [SugarColumn(ColumnName = "CTMDL_CLV", ColumnDescription = "clave del modelo (url)", IsNullable = true, Length = 255)]
        public string Clave {
            get {
                return this._clave;
            }
            set {
                this._clave = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
