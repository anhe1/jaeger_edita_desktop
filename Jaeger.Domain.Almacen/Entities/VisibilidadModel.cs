﻿/// develop: anhe1 081120191127
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    public class VisibilidadModel : BaseSingleModel {
        public VisibilidadModel() {
        }

        public VisibilidadModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
