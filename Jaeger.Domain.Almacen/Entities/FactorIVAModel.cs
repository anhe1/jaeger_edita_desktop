﻿/// develop: anhe1 081120191127
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    public class FactorIVAModel : BaseSingleModel {
        public FactorIVAModel() {
        }

        public FactorIVAModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
