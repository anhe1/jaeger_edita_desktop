﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase de los proveedores del modelo del producto o servicio
    /// </summary>
    [SugarTable("_ctlprv", "catalogo de proveedores del almacen de materia prima")]
    public class ModeloProveedorModel : BasePropertyChangeImplementation, IModeloProveedorModel {
        #region declaraciones
        private int index;
        private bool activo;
        private DateTime? fechaModifica;
        private int idmodelo;
        private int idproveedor;
        private decimal conversion;
        private decimal unitario;
        private string unidad;
        private string moneda;
        private string nota;
        private DateTime fechaNuevo;
        private string creo;
        private string modifico;
        #endregion

        public ModeloProveedorModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice principal de la tabla
        /// </summary>
        [DataNames("_ctlprv_id")]
        [SugarColumn(ColumnName = "_ctlprv_id", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_ctlprv_a")]
        [SugarColumn(ColumnName = "_ctlprv_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del modelo del producto
        /// </summary>
        [DataNames("_ctlprv_mdl_id")]
        [SugarColumn(ColumnName = "_ctlprv_mdl_id", ColumnDescription = "indice de relacion con el modelo del producto")]
        public int IdModelo {
            get {
                return this.idmodelo;
            }
            set {
                this.idmodelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del proveedor en el directorio
        /// </summary>
        [DataNames("_ctlprv_drctr_id")]
        [SugarColumn(ColumnName = "_ctlprv_drctr_id", ColumnDescription = "indice de relacion con el directorio de proveedores")]
        public int IdProveedor {
            get {
                return this.idproveedor;
            }
            set {
                this.idproveedor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlprv_cnvr")]
        [SugarColumn(ColumnName = "_ctlprv_cnvr", ColumnDescription = "factor de conversion a la unidad de almacen", Length = 14, DecimalDigits = 4, IsNullable = true, IsIgnore = true)]
        public decimal Conversion {
            get {
                return this.conversion;
            }
            set {
                this.conversion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlprv_nom")]
        [SugarColumn(IsIgnore = true, ColumnName = "_ctlprv_nom", ColumnDescription = "nombre del proveedor", Length = 255)]
        public string Nombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el valor unitario del producto ó modelo
        /// </summary>
        [DataNames("_ctlprv_untr")]
        [SugarColumn(ColumnName = "_ctlprv_untr", ColumnDescription = "alm:|mp,pt,tw| desc: precio unitario", Length = 11, DecimalDigits = 4)]
        public decimal Unitario {
            get {
                return this.unitario;
            }
            set {
                this.unitario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de surtimento 
        /// </summary>
        [DataNames("_ctlprv_undd")]
        [SugarColumn(ColumnName = "_ctlprv_undd", ColumnDescription = "alm:|mp,pt,tw| desc: unidad personalizada", Length = 20, IsNullable = true)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlprv_mnd")]
        [SugarColumn(ColumnName = "_ctlprv_mnd", ColumnDescription = "clave de moneda utilizada", Length = 20, IsNullable = true)]
        public string Moneda {
            get {
                return this.moneda;
            }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("_ctlprv_obs")]
        [SugarColumn(ColumnName = "_ctlprv_obs", ColumnDescription = "notas", Length = 32, IsNullable = true)]
        public string Nota {
            get {
                return this.nota;
            }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_ctlprv_fn")]
        [SugarColumn(ColumnName = "_ctlprv_fn", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("_ctlprv_usr_n")]
        [SugarColumn(ColumnName = "_ctlprv_usr_n", ColumnDescription = "clave del usuario que crea el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_ctlprv_fm")]
        [SugarColumn(ColumnName = "_ctlprv_fm", ColumnDescription = "ultima fecha de modificacion del regidstro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_ctlprv_usr_m")]
        [SugarColumn(ColumnName = "_ctlprv_usr_m", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifico {
            get {
                return this.modifico;
            }
            set {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }
    }
}
