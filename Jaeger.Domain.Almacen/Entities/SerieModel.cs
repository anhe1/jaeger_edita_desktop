﻿namespace Jaeger.Domain.Almacen.Entities {
    public class SerieModel : Base.Abstractions.BaseSingleModel {
        public SerieModel() {
        }

        public SerieModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
