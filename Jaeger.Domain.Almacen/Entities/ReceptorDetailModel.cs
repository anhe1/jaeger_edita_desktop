﻿using System.ComponentModel;
using SqlSugar;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("_drctr")]
    public class ReceptorDetailModel : ReceptorModel {
        private BindingList<ReceptorDomicilioModel> domicilios;

        public ReceptorDetailModel() : base() {
            this.domicilios = new BindingList<ReceptorDomicilioModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ReceptorDomicilioModel> Domicilios {
            get {
                return this.domicilios;
            }
            set {
                this.domicilios = value;
                this.OnPropertyChanged();
            }
        }
    }
}
