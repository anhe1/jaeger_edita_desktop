﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("_drccn")]
    public class ReceptorDomicilioModel : Base.Abstractions.DomicilioFiscal, IDomicilioFiscalModel {
        public ReceptorDomicilioModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer id principal de la tabla
        /// </summary>           
        [DataNames("_drccn_id")]
        [SugarColumn(ColumnName = "_drccn_id", ColumnDescription = "id principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int Id {
            get {
                return base.Id;
            }
            set {
                base.Id = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>           
        [DataNames("_drccn_a")]
        [SugarColumn(ColumnName = "_drccn_a", ColumnDescription = "registro activo", IsNullable = false, Length = 1)]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la relacion con el directorio
        /// </summary>           
        [DataNames("_drccn_drctr_id")]
        [SugarColumn(ColumnName = "_drccn_drctr_id", ColumnDescription = "indice del relacion con el directorio", IsNullable = false, Length = 11)]
        public new int SubId {
            get {
                return base.SubId;
            }
            set {
                base.SubId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo de pais
        /// </summary>           
        [DataNames("_drccn_cdgps")]
        [SugarColumn(ColumnName = "_drccn_cdgps", ColumnDescription = "codigo de pais", IsNullable = true, Length = 3)]
        public new string CodigoPais {
            get {
                return base.CodigoPais;
            }
            set {
                base.CodigoPais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        [DataNames("_drccn_usr_n")]
        [SugarColumn(ColumnName = "_drccn_usr_n", ColumnDescription = "usuario creo", IsNullable = false, Length = 10)]
        public new string Creo {
            get {
                return base.Creo;
            }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifico el registro
        /// </summary>           
        [DataNames("_drccn_usr_m")]
        [SugarColumn(ColumnName = "_drccn_usr_m", ColumnDescription = "usuario que modifico", IsNullable = true, Length = 10)]
        public new string Modifica {
            get {
                return base.Modifica;
            }
            set {
                base.Modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:codigo postal
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_cp")]
        [SugarColumn(ColumnName = "_drccn_cp", ColumnDescription = "codigo postal", IsNullable = true, Length = 10)]
        public new string CodigoPostal {
            get {
                return base.CodigoPostal;
            }
            set {
                base.CodigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_tp")]
        [SugarColumn(ColumnName = "_drccn_tp", ColumnDescription = "alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)", IsNullable = true, Length = 0)]
        public new string Tipo {
            get {
                return base.Tipo;
            }
            set {
                base.Tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:asentamiento humano
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_asnh")]
        [SugarColumn(ColumnName = "_drccn_asnh", ColumnDescription = "tipo de asentamiento humano", IsNullable = true, Length = 0)]
        public new string Asentamiento {
            get {
                return base.Asentamiento;
            }
            set {
                base.Asentamiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero exterior
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_extr")]
        [SugarColumn(ColumnName = "_drccn_extr", ColumnDescription = "numero exterior", IsNullable = true, Length = 0)]
        public new string NoExterior {
            get {
                return base.NoExterior;
            }
            set {
                base.NoExterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:numero interior
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_intr")]
        [SugarColumn(ColumnName = "_drccn_intr", ColumnDescription = "numero interior", IsNullable = true, Length = 0)]
        public new string NoInterior {
            get {
                return base.NoInterior;
            }
            set {
                base.NoInterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:calle
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_cll")]
        [SugarColumn(ColumnName = "_drccn_cll", ColumnDescription = "calle", IsNullable = true, Length = 0)]
        public new string Calle {
            get {
                return base.Calle;
            }
            set {
                base.Calle = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:colonia
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_cln")]
        [SugarColumn(ColumnName = "_drccn_cln", ColumnDescription = "colonia", IsNullable = true, Length = 0)]
        public new string Colonia {
            get {
                return base.Colonia;
            }
            set {
                base.Colonia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:delegacion / municipio
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_dlg")]
        [SugarColumn(ColumnName = "_drccn_dlg", ColumnDescription = "delegacion o municipio", IsNullable = true, Length = 0)]
        public new string Delegacion {
            get {
                return base.Delegacion;
                ;
            }
            set {
                base.Delegacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ciudad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_cdd")]
        [SugarColumn(ColumnName = "_drccn_cdd", ColumnDescription = "ciudad", IsNullable = true, Length = 0)]
        public new string Ciudad {
            get {
                return base.Ciudad;
            }
            set {
                base.Ciudad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:estado
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_std")]
        [SugarColumn(ColumnName = "_drccn_std", ColumnDescription = "estado", IsNullable = true, Length = 0)]
        public new string Estado {
            get {
                return base.Estado;
            }
            set {
                base.Estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:pais
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_ps")]
        [SugarColumn(ColumnName = "_drccn_ps", ColumnDescription = "Pais", IsNullable = true, Length = 0)]
        public new string Pais {
            get {
                return base.Pais;
            }
            set {
                base.Pais = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:localidad
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_lcldd")]
        [SugarColumn(ColumnName = "_drccn_lcldd", ColumnDescription = "localidad", IsNullable = true, Length = 0)]
        public new string Localidad {
            get {
                return base.Localidad;
            }
            set {
                base.Localidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:referencia
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_rfrnc")]
        [SugarColumn(ColumnName = "_drccn_rfrnc", ColumnDescription = "referencia", IsNullable = true, Length = 0)]
        public new string Referencia {
            get {
                return base.Referencia;
            }
            set {
                base.Referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:telefonos de contacto
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_tlfns")]
        [SugarColumn(ColumnName = "_drccn_tlfns", ColumnDescription = "telefonos de contactos", IsNullable = true, Length = 0)]
        public new string Telefono {
            get {
                return base.Telefono;
            }
            set {
                if (value != null)
                    base.Telefono = value.Replace("-", "").Trim();
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion de la ubicacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_dsp")]
        [SugarColumn(ColumnName = "_drccn_dsp", ColumnDescription = "descripcion de la ubicacion", IsNullable = true, Length = 0)]
        public new string Notas {
            get {
                return base.Notas;
            }
            set {
                base.Notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:horario y requisito de acceso
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_req")]
        [SugarColumn(ColumnName = "_drccn_req", ColumnDescription = "horario y requisito de acceso", IsNullable = true, Length = 0)]
        public new string Requerimiento {
            get {
                return base.Requerimiento;
            }
            set {
                base.Requerimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:current_timestamp()
        /// Nullable:False
        /// </summary>           
        [DataNames("_drccn_fn")]
        [SugarColumn(ColumnName = "_drccn_fn", ColumnDescription = "fecha de sistema", IsNullable = false)]
        public new DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:NULL
        /// Nullable:True
        /// </summary>           
        [DataNames("_drccn_fm")]
        [SugarColumn(ColumnName = "_drccn_fm", ColumnDescription = "ultima fecha de modificacion", IsNullable = true, Length = 0)]
        public new DateTime? FechaModifica {
            get {
                return base.FechaModifica;
            }
            set {
                base.FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de domicilio registrado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public new TipoDomicilioEnum TipoEnum {
            get {
                return base.TipoEnum;
            }
            set {
                base.TipoEnum = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new string Domicilio {
            get {
                return base.ToString();
            }
        }
    }
}
