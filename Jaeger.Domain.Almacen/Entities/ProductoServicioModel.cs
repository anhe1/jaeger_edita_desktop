﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase modelo de producto o servicio
    /// </summary>
    [SugarTable("CTPRD", "catalogo de productos y servicios")]
    public class ProductoServicioModel : Abstractions.Producto, IProductoServicioModel {
        #region declaraciones
        private bool _Activo;
        private int _IdAlmacen;
        private int _IdTipo;
        private string _Clave;
        private string _Creo;
        private DateTime _FechaNuevo;
        private int _Secuencia;
        #endregion

        public ProductoServicioModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        [DataNames("CTPRD_ID")]
        [SugarColumn(ColumnName = "CTPRD_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdProducto {
            get {
                return base.IdProducto;
            }
            set {
                base.IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("CTPRD_A")]
        [SugarColumn(ColumnName = "CTPRD_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this._Activo;
            }
            set {
                this._Activo = value;
                //this.OnPropertyChanged();
            }
        }
        
        [DataNames("CTPRD_ALM_ID")]
        [SugarColumn(ColumnName = "CTPRD_ALM_ID", ColumnDescription = "almacen al que pertenece (1-mp|2-pt|3-tw)", ColumnDataType = "SMALLINT", Length = 1)]
        public int IdAlmacen {
            get {
                return this._IdAlmacen;
            }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("CTPRD_TIPO")]
        [SugarColumn(ColumnName = "CTPRD_TIPO", ColumnDescription = "tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos)", ColumnDataType = "SMALLINT", DefaultValue = "1", Length = 1)]
        public int IdTipo {
            get {
                return this._IdTipo;
            }
            set {
                this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer indice de relacion de la categoria
        /// </summary>
        [DataNames("CTPRD_CTCLS_ID")]
        [SugarColumn(ColumnName = "CTPRD_CTCLS_ID", ColumnDescription = "indice de relacion de las categorias", DefaultValue = "0")]
        public new int IdCategoria {
            get {
                return base.IdCategoria;
            }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia u ordenamiento de objeto
        /// </summary>
        [DataNames("CTPRD_SEC")]
        [SugarColumn(ColumnName = "CTPRD_SEC", ColumnDescription = "secuencia u ordenamiento del objeto", IsNullable = false, IsIgnore = true)]
        public int Secuencia {
            get { return this._Secuencia; }
            set {
                this._Secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de producto para formar URL
        /// </summary>
        [DataNames("CTPRD_CLV")]
        [SugarColumn(ColumnName = "CTPRD_CLV", ColumnDescription = "clave de producto para formar URL", IsNullable = false)]
        public string Clave {
            get {
                return this._Clave;
            }
            set {
                this._Clave = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        [DataNames("CTPRD_NOM")]
        [SugarColumn(ColumnName = "CTPRD_NOM", ColumnDescription = "descripcion del producto", Length = 128)]
        public new string Nombre {
            get {
                return base.Nombre;
            }
            set {
                base.Nombre = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        [DataNames("CTPRD_USR_N")]
        [SugarColumn(ColumnName = "CTPRD_USR_N", ColumnDescription = "clave de usuario que crea el registro", Length = 20, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this._Creo;
            }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CTPRD_FN")]
        [SugarColumn(ColumnName = "CTPRD_FN", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string Codigo {
            get { return string.Format("{0}:{1}.{2}.{3}", this.IdAlmacen.ToString("0#"), this.IdTipo.ToString("0#"), this.IdCategoria.ToString("00#"), this.IdProducto.ToString("000#")); }
        }
    }
}
