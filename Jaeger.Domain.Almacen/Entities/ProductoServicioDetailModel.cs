﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase para representar un producto o servicio en detalle con sus modelos asociados
    /// </summary>
    [SugarTable("CTPRD", "catalogo de productos")]
    public class ProductoServicioDetailModel : ProductoServicioModel, IProductoServicioModel, IProductoServicioDetailModel {
        #region declaraciones
        private BindingList<ModeloDetailModel> _Modelos;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ProductoServicioDetailModel() : base() {
            this._Modelos = new BindingList<ModeloDetailModel>();
        }

        /// <summary>
        /// Desc:(1-producto, 2-servicio, 3-kit, 4-grupo de productos)
        /// </summary>           
        [SugarColumn(IsIgnore = true)]
        public ProdServTipoEnum Tipo {
            get {
                if (this.IdTipo == 1)
                    return ProdServTipoEnum.Producto;
                else if (this.IdTipo == 2)
                    return ProdServTipoEnum.Servicio;
                else
                    return ProdServTipoEnum.NoDefinido;
            }
            set {
                this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de almacen al que pertenece el producto
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum Almacen {
            get {
                if (this.IdAlmacen == (int)AlmacenEnum.MP)
                    return AlmacenEnum.MP;
                else
                    return AlmacenEnum.PT;
            }
            set {
                this.IdAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de modelos asociados al producto
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ModeloDetailModel> Modelos {
            get {
                return this._Modelos;
            }
            set {
                this._Modelos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
