﻿using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase modelo para vista de Clasificacion General (BPS)
    /// </summary>
    public class ClasificacionSingle : ClasificacionModel, IClasificacionModel, IClasificacionSingle {
        /// <summary>
        /// constructor
        /// </summary>
        public ClasificacionSingle() : base() { }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase
        /// </summary>
        [DataNames("Clase")]
        [SugarColumn(IsIgnore = true)]
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer nivel en la clasificacion
        /// </summary>
        [DataNames("Nivel")]
        [SugarColumn(IsIgnore = true)]
        public int Nivel { get; set; }

        /// <summary>
        /// obtener o establecer cantidad de hijos del nivel actual
        /// </summary>
        [DataNames("Childs")]
        [SugarColumn(IsIgnore = true)]
        public int Childs { get; set; }

        /// <summary>
        /// obtener descripcion de la categoria
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public new string Descriptor {
            get {
                return this.Descripcion;
            }
        }

        /// <summary>
        /// obtener codigo de la clasificacion {IdCategoria}{IdSubCategoria}{0}
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Codigo {
            get { return string.Format("{0}.{1}.{2}", this.IdCategoria.ToString("00#"), this.IdSubCategoria.ToString("00#"), 0); }
        }

        #region metodos publicos
        public IClasificacionModel GetClasificacion() {
            return this;
        }
        #endregion
    }
}
