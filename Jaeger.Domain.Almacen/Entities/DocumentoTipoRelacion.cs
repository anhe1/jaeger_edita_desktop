﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase para tipos de relacion entre documentos
    /// </summary>
    public class DocumentoTipoRelacion : BaseSingleModel {
        public DocumentoTipoRelacion() {

        }

        public DocumentoTipoRelacion(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
