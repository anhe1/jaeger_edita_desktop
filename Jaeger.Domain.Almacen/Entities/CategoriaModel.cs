﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    ///<summary>
    /// clase modelo para categorias en TIENDA WEB
    ///</summary>
    [SugarTable("_ctlclss", "catalogo de clasificaciones de bienes, productos y servicios (BPS)")]
    public class CategoriaModel : Abstractions.Categoria, ICategoriaModel {
        #region declaraciones
        private string _URL;
        #endregion

        public CategoriaModel() {
            base.Activo = true;
            base.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlclss_id", "CTCLS_ID", "IdCategoria")]
        [SugarColumn(ColumnName = "_ctlclss_id", ColumnDescription = "indice de la categoria", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdCategoria {
            get {
                return base.IdCategoria;
            }
            set {
                base.IdCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:activo (_ctlclss_a)
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlclss_a", "CTCLS_A", "Activo")]
        [SugarColumn(ColumnName = "_ctlclss_a", ColumnDescription = "registro activo", IsNullable = false)]
        public new bool Activo {
            get {
                return base.Activo;
            }
            set {
                base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estanlecer sub indice de la categoria
        /// </summary>
        [DataNames("_ctlclss_sbid", "CTCLS_SBID", "SubId")]
        [SugarColumn(ColumnName = "_ctlclss_sbid", ColumnDescription = "sub indice de la categoria")]
        public new int IdSubCategoria {
            get {
                return base.IdSubCategoria;
            }
            set {
                base.IdSubCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:index de documento (_ctlclss_alm_id)
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlclss_alm_id", "CTCLS_ALM_ID")]
        [SugarColumn(ColumnName = "_ctlclss_alm_id", ColumnDescription = "indice del departamento al que pertenece", IsNullable = false)]
        public new int IdAlmacen {
            get { return base.IdAlmacen; }
            set { base.IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTCLS_SEC", "Secuencia")]
        [SugarColumn(ColumnName = "_ctlclss_sec", ColumnDescription = "indice del departamento al que pertenece", IsNullable = false, IsIgnore = true)]
        public new int Secuencia {
            get { return base.Secuencia; }
            set {
                base.Secuencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:nombre de la clasificacion (_ctlclss_class1)
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlclss_class2", "CTCLS_CLASS2", "Descripcion")]
        [SugarColumn(ColumnName = "_ctlclss_class2", ColumnDescription = "nombre de la clasificacion", Length = 128, IsNullable = false)]
        public new string Descripcion {
            get {
                return base.Descripcion;
            }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:sub clasificacion (_ctlclss_class2)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlclss_class1", "CTCLS_CLASS1", "Clave")]
        [SugarColumn(ColumnName = "_ctlclss_class1", ColumnDescription = "clasificacíon", Length = 128, IsNullable = true)]
        public new string Clave {
            get {
                return base.Clave;
            }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Creo (_ctlclss_usr_n)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_ctlclss_usr_n", "CTCLS_USR_N")]
        [SugarColumn(ColumnName = "_ctlclss_usr_n", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsNullable = true)]
        public new string Creo {
            get {
                return base.Creo;
            }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Fec. sist. (_ctlclss_fn)
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlclss_fn", "CTCLS_FN")]
        [SugarColumn(ColumnName = "_ctlclss_fn", ColumnDescription = "fecha de creacion del registro", IsNullable = false)]
        public new DateTime FechaNuevo {
            get {
                return base.FechaNuevo;
            }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTCLS_URL")]
        [SugarColumn(ColumnName = "CTCLS_URL", ColumnDescription = "", IsNullable = true)]
        public string URL {
            get { return this._URL; }
            set { this._URL = value;
            this.OnPropertyChanged();
            }
        }
    }
}
