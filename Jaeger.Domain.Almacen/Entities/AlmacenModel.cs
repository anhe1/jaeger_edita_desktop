﻿using System;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase modelo para el catalogo de almacen
    /// </summary>
    public class AlmacenModel : Base.Abstractions.BasePropertyChangeImplementation, IAlmacenModel {
        #region declaraciones
        private int _IdAlmacen;
        private bool _Activo;
        private string _Clave;
        private string _Descripcion;
        private string _Creo;
        private DateTime _FechaNuevo;
        private AlmacenEnum _Tipo;
        #endregion

        public AlmacenModel() {
            this._Activo = true;
            this._FechaNuevo = DateTime.Now;
        }

        public AlmacenModel(int idAlmacen, string clave, string descripcion, AlmacenEnum tipo) : base() {
            this._IdAlmacen = idAlmacen;
            this._Clave = clave;
            this._Descripcion = descripcion;
            this._Tipo = tipo;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        public AlmacenEnum Tipo {
            get { return this._Tipo; }
            set {
                this._Tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        public bool Activo {
            get { return this._Activo; }
            set { this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del almacen
        /// </summary>
        public string Clave {
            get { return this._Clave; }
            set { this._Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer descripcion del almcen
        /// </summary>
        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        public string Creo {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        public string Descriptor {
            get { return string.Format("{0}: {1}", this.IdAlmacen.ToString("0#"), this.Descripcion); }
        }
        #endregion
    }
}
