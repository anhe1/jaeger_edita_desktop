﻿using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// modelo existencias
    /// </summary>
    public class ModeloExistenciaModel : Base.Abstractions.BasePropertyChangeImplementation, IModeloExistenciaModel {
        #region declaraciones
        private int _IdProducto;
        private int _IdModelo;
        private int _IdEspecificacion;
        private int _IdAlmacen;
        private decimal _Existencia;
        private decimal _Minimo;
        private decimal _Maximo;
        private decimal _ReOrden;
        private int _IdTipoAlmacen;
        #endregion

        public ModeloExistenciaModel() {
            this._IdEspecificacion = -1;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla de productos
        /// </summary>
        [DataNames("CTMDLX_CTPRD_ID")]
        [SugarColumn(ColumnName = "CTMDLX_CTPRD_ID")]
        public int IdProducto {
            get { return this._IdProducto; }
            set { this._IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla de modelos
        /// </summary>
        [DataNames("CTMDLX_CTMDL_ID")]
        [SugarColumn(ColumnName = "CTMDLX_CTMDL_ID")]
        public int IdModelo {
            get { return this._IdModelo; }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la especificacion o variante
        /// </summary>
        [DataNames("CTMDLX_CTESPC_ID")]
        [SugarColumn(ColumnName = "CTMDLX_CTESPC_ID")]
        public int IdEspecificacion {
            get { return this._IdEspecificacion; }
            set {
                this._IdEspecificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indice del catalogo de almacenes (CTALM_ID)
        /// </summary>
        [DataNames("CTMDLX_CTALM_ID")]
        [SugarColumn(ColumnName = "CTMDLX_CTALM_ID")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de almacen MP = 1, PT = 2
        /// </summary>
        [DataNames("CTMDLX_TPALM_ID")]
        [SugarColumn(ColumnName = "CTMDLX_TPALM_ID")]
        public int IdTipoAlmacen {
            get { return this._IdTipoAlmacen; }
            set {
                this._IdTipoAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad de existencia del modelo o producto
        /// </summary>
        [DataNames("CTMDLX_EXT")]
        [SugarColumn(ColumnName = "CTMDLX_EXT")]
        public decimal Existencia {
            get { return this._Existencia; }
            set {
                this._Existencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad minima
        /// </summary>
        [DataNames("CTMDLX_MIN")]
        [SugarColumn(ColumnName = "CTMDLX_MIN")]
        public decimal Minimo {
            get { return this._Minimo; }
            set {
                this._Minimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad maxima
        /// </summary>
        [DataNames("CTMDLX_MAX")]
        [SugarColumn(ColumnName = "CTMDLX_MAX")]
        public decimal Maximo {
            get { return this._Maximo; }
            set {
                this._Maximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad del punto de reorden
        /// </summary>
        [DataNames("CTMDLX_REORD")]
        [SugarColumn(ColumnName = "CTMDLX_REORD")]
        public decimal ReOrden {
            get { return this._ReOrden; }
            set {
                this._ReOrden = value;
                this.OnPropertyChanged();
            }
        }
    }
}
