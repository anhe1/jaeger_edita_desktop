﻿using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    public class ProductoModeloExistenciaModel : ModeloDetailModel, IProductoExistenciaModel, IProductoServicioModel, IModeloDetailModel, IModeloModel {
        #region declaraciones
        private string _Nombre;
        #endregion

        public ProductoModeloExistenciaModel() : base() { }

        #region informacion de producto
        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        [DataNames("CTPRD_NOM")]
        [SugarColumn(ColumnName = "CTPRD_NOM", ColumnDescription = "descripcion del producto", Length = 128)]
        public string Nombre {
            get {
                return this._Nombre;
            }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer nombre de la cartegoria
        /// </summary>
        [DataNames("CATEGORIA", "CLASE")]
        public string Categoria { get; set; }

        /// <summary>
        /// path utilizado principalmente en la vista web
        /// </summary>
        [DataNames("FCATEGORIA")]
        public string Path { get; set; }

        [DataNames("CTESPC_NOM")]
        public string Variante { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [DataNames("UNIDAD", "CTUND_NOM")]
        public new string Unidad { get; set; }

        [DataNames("FACTORUNIDAD")]
        public decimal FactorUnidad { get; set; }
    }
}
