﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("CTESPC")]
    public class EspecificacionModel : BasePropertyChangeImplementation, IEspecificacionModel {
        #region declaraciones
        private int _index;
        private bool _activo;
        private string _nombre;
        private string _nota;
        private DateTime _fechaNuevo;
        private DateTime? _fechaModifica;
        private string _creo;
        private string _modifica;
        #endregion

        public EspecificacionModel() {
            this.Activo = true;
            this.SetModified = false;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (CTESPC_ID)
        /// </summary>
        [DataNames("CTESPC_ID")]
        [SugarColumn(ColumnName = "CTESPC_ID", ColumnDescription = "", IsPrimaryKey = true, IsIdentity = true)]
        public int IdEspecificacion {
            get { return _index; }
            set {
                _index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CTESPC_A")]
        [SugarColumn(ColumnName = "CTESPC_A", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return _activo; }
            set {
                _activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion
        /// </summary>
        [DataNames("CTESPC_NOM")]
        [SugarColumn(ColumnName = "CTESPC_NOM", ColumnDescription = "nombre o descripcion", Length = 20)]
        public string Descripcion {
            get { return _nombre; }
            set {
                _nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("CTESPC_NOTA")]
        [SugarColumn(ColumnName = "CTESPC_NOTA", ColumnDescription = "observaciones", Length = 50)]
        public string Nota {
            get { return _nota; }
            set {
                _nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LSTSYS_FN)
        /// </summary>
        [DataNames("CTESPC_FN")]
        [SugarColumn(ColumnName = "CTESPC_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return _fechaNuevo; }
            set {
                _fechaNuevo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LSTSYS_USU_N)
        /// </summary>
        [DataNames("CTESPC_USR_N")]
        [SugarColumn(ColumnName = "CTESPC_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return _creo; }
            set {
                _creo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LSTSYS_FM)
        /// </summary>
        [DataNames("CTESPC_FM")]
        [SugarColumn(ColumnName = "CTESPC_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get { return _fechaModifica; }
            set {
                _fechaModifica = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer (LSTSYS_USU_M)
        /// </summary>
        [DataNames("CTESPC_USR_M")]
        [SugarColumn(ColumnName = "CTESPC_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return _modifica; }
            set {
                _modifica = value;
                OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public bool SetModified { get; set; }
    }
}
