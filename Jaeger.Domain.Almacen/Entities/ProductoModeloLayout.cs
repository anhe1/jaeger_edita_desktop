﻿using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    public class ProductoModeloLayout : Abstractions.Modelo, IProductoModeloLayout {

        public ProductoModeloLayout() : base() { }

        /// <summary>
        /// obtener o establecer el indice de la categoria del producto o modelo
        /// </summary>
        public new int IdCategoria {
            get { return base.IdCategoria; }
            set { base.IdCategoria = value; }
        }

        /// <summary>
        /// obtener o establecer el nombre de la categoria en modo texto
        /// </summary>
        public string Categoria { get; set; }

        /// <summary>
        /// obtener o establecer indice del producto
        /// </summary>
        public new int IdProducto {
            get { return base.IdProducto; }
            set { base.IdProducto = value; }
        }

        public string Producto { get; set; }
        
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer descripcion del modelo
        /// </summary>
        public string Modelo { get; set; }

        /// <summary>
        /// obtener o establecer marca
        /// </summary>
        public new string Marca {
            get { return base.Marca; }
            set {  base.Marca = value; }
        }

        /// <summary>
        /// obtener o establecer especificacion
        /// </summary>
        public new string Especificacion {
            get { return base.Especificacion; }
            set { base.Especificacion = value; }
        }

        /// <summary>
        /// modelos, publicacion para la tienda web
        /// </summary>
        public string Publicacion { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        public string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        public decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        public decimal TasaIVA { get; set; }
        
        public string Tienda { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        public int Secuencia { get; set; }

        public decimal Largo { get; set; }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        public decimal Ancho { get; set; }
        
        public decimal Alto { get; set; }
        
        public decimal Peso { get; set; }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        public string ClaveProdServ { get; set; }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        public string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        public string ObjetoImp { get; set; }
        
        public string Imagen { get; set; }
    }
}
