﻿/// develop: anhe1 081120191127
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    public class ProductoServicioTipoModel : BaseSingleModel {
        public ProductoServicioTipoModel() : base() { }

        public ProductoServicioTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
