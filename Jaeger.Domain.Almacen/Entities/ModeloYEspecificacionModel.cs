﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    [SugarTable("CTMDLE")]
    public class ModeloYEspecificacionModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _index;
        private bool _Activo;
        private int _IdProducto;
        private int _IdModelo;
        private int _IdEspecificacion;
        private bool _IsAutorizado;
        private int _IdAlmacen;
        private int _IdVisibilidad;
        private decimal _Existencia;
        private decimal _Minimo;
        private decimal _Maximo;
        private decimal _ReOrden;
        private string creo;
        private DateTime _FechaNuevo;
        private string modifica;
        private DateTime? fechaModifica;
        #endregion

        public ModeloYEspecificacionModel() {
            this._Activo = true;
            this._FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("CTMDLE_ID")]
        [SugarColumn(ColumnName = "CTMDLE_ID", ColumnDescription = "", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get { return this._index; }
            set {
                this._index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        [DataNames("CTMDLE_A")]
        [SugarColumn(ColumnName = "CTMDLE_A", ColumnDescription = "registro activo")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de almacen
        /// </summary>
        [DataNames("CTMDLE_ALM_ID")]
        [SugarColumn(ColumnName = "CTMDLE_ALM_ID", ColumnDescription = "indice del almacen")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_CTPRD_ID")]
        [SugarColumn(ColumnName = "CTMDLE_CTPRD_ID", ColumnDescription = "indice de relacion con el producto")]
        public int IdProducto {
            get { return this._IdProducto; }
            set {
                this._IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_CTMDL_ID")]
        [SugarColumn(ColumnName = "CTMDLE_CTMDL_ID", ColumnDescription = "indice de relacion con el modelo")]
        public int IdModelo {
            get { return this._IdModelo; }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_CTESPC_ID")]
        [SugarColumn(ColumnName = "CTMDLE_CTESPC_ID", ColumnDescription = "indice de relacion con la especificacion")]
        public int IdEspecificacion {
            get { return this._IdEspecificacion; }
            set {
                this._IdEspecificacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_ATRZD")]
        [SugarColumn(ColumnName = "CTMDLE_ATRZD", ColumnDescription = "autorizacion")]
        public bool IsAutorizado {
            get { return this._IsAutorizado; }
            set {
                this._IsAutorizado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_VIS")]
        [SugarColumn(ColumnName = "CTMDLE_VIS", ColumnDescription = "")]
        public int IdVisibilidad {
            get { return this._IdVisibilidad; }
            set {
                this._IdVisibilidad = value;
                this.OnPropertyChanged();
            }

        }

        #region eliminar
        [DataNames("CTMDLE_EXT", "CTMDLX_EXT")]
        [SugarColumn(ColumnName = "CTMDLE_EXT", ColumnDescription = "")]
        public decimal Existencia {
            get { return this._Existencia; }
            set {
                this._Existencia = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_MIN", "CTMDLX_MIN")]
        [SugarColumn(ColumnName = "CTMDLE_MIN", ColumnDescription = "")]
        public decimal Minimo {
            get { return this._Minimo; }
            set {
                this._Minimo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_MAX", "CTMDLX_MAX")]
        [SugarColumn(ColumnName = "CTMDLE_MAX", ColumnDescription = "")]
        public decimal Maximo {
            get { return this._Maximo; }
            set {
                this._Maximo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLE_REORD", "CTMDLX_REORD")]
        [SugarColumn(ColumnName = "CTMDLE_REORD", ColumnDescription = "")]
        public decimal ReOrden {
            get { return this._ReOrden; }
            set {
                this._ReOrden = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>           
        [DataNames("CTMDLE_USR_N")]
        [SugarColumn(ColumnName = "CTMDLE_USR_N", ColumnDescription = "")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CTMDLE_FN")]
        [SugarColumn(ColumnName = "CTMDLE_FN", ColumnDescription = "")]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima clave del usuario que modifica el registro
        /// </summary>           
        [DataNames("CTMDLE_USR_M")]
        [SugarColumn(ColumnName = "CTMDLE_USR_M", ColumnDescription = "")]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("CTMDLE_FM")]
        [SugarColumn(ColumnName = "CTMDLE_FM", ColumnDescription = "")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        public ModeloExistenciaModel GetExistenciaModel() {
            return new ModeloExistenciaModel() {
                Existencia = this.Existencia,
                IdProducto = this.IdProducto,
                IdEspecificacion = this.IdEspecificacion,
                IdModelo = this.IdModelo,
                Maximo = this.Maximo,
                IdAlmacen = this.IdAlmacen,
                Minimo = this.Minimo,
                ReOrden = this.ReOrden,
                IdTipoAlmacen = 0
            };
        }
    }
}
