﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// modelo de detalle de modelo, incluye lista de medios (Tienda WEB)
    /// </summary>
    public class ModeloXDetailModel : ModeloDetailModel, IModeloXDetailModel, IModeloDetailModel, IModeloModel, ICloneable, IDataErrorInfo {
        private BindingList<ModeloYEspecificacionModel> disponibles;
        private PublicacionModel _Publicacion;

        /// <summary>
        /// constructor
        /// </summary>
        public ModeloXDetailModel() : base() {
            this.disponibles = new BindingList<ModeloYEspecificacionModel>();
            this._Publicacion = new PublicacionModel();
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ModeloYEspecificacionModel> Disponibles {
            get { return this.disponibles; }
            set {
                this.disponibles = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public PublicacionModel Publicacion {
            get { return this._Publicacion; }
            set {
                this._Publicacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener indice de categoria de la publicacion siempre que no sea nula
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public int IdCategoriaP {
            get {
                if (this._Publicacion != null)
                    return this._Publicacion.IdCategoria;
                return -1;
            }
        }

        public new object Clone() {
            return this.MemberwiseClone();
        }

        #region metodos
        public new string Error {
            get {
                if (this.Unitario <= 0 | this.IdTipo == 0) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        public new string this[string columnName] {
            get {
                if (columnName == "Unitario") {
                    return "¡Valor unitario no válido!";
                } else if (columnName == "IdTipo") {
                    return "No se selecciono el tipo de Producto o Servicio";
                }
                return string.Empty;
            }
        }
        #endregion
    }
}
