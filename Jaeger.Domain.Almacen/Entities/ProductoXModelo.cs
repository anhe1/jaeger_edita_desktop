﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// vista
    /// </summary>
    public class ProductoXModelo : Abstractions.Producto {
        public ProductoXModelo() : base() {}

        /// <summary>
        /// obtener o establecer el indice de la tabla _ctlmdl_id
        /// </summary>
        [DataNames("IDMODELO")]
        public int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("ACTIVO")]
        public bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la categoria general
        /// </summary>
        [DataNames("IDCATEGORIA")]
        public new int IdCategoria { 
            get { return base.IdCategoria; }
            set { base.IdCategoria = value;}
        }

        /// <summary>
        /// obtener o establecer indice del producto
        /// </summary>
        [DataNames("IDPRODUCTO")]
        public new int IdProducto {
            get { return base.IdProducto; }
            set { base.IdProducto = value; }
        }

        /// <summary>
        /// obtener o establecer indice de la especificacion
        /// </summary>
        [DataNames("CTMDLE_CTESPC_ID")]
        public int IdEspecificacion { get; set; }

        /// <summary>
        /// obtener o establecer tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distinción de producto o servicio.
        /// </summary>
        [DataNames("TIPO")]
        public int IdTipoProducto { get; set; }

        /// <summary>
        /// obtener o establecer nombre de la cartegoria
        /// </summary>
        [DataNames("CATEGORIA")]
        public string Categoria { get; set; }

        /// <summary>
        /// path utilizado principalmente en la vista web
        /// </summary>
        [DataNames("FCATEGORIA")]
        public string Path { get; set; }

        [DataNames("CLASE")]
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del producto
        /// </summary>
        [DataNames("PRODUCTO")]
        public new string Nombre { get { return base.Nombre; } set { base.Nombre = value; } }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        [DataNames("MODELO")]
        public string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer marca del producto
        /// </summary>
        [DataNames("MARCA")]
        public string Marca { get; set; }

        /// <summary>
        /// obtener o establecer especificacion del modelo
        /// </summary>
        [DataNames("ESPECIFICACION")]
        public string Especificacion { get; set; }

        /// <summary>
        /// alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)
        /// </summary>
        [DataNames("VISIBILIDAD")]
        public int IdVisible { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        [DataNames("IDUNIDAD")]
        public int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        [DataNames("UNIDAD")]
        public string Unidad { get; set; }

        [DataNames("FACTORUNIDAD")]
        public decimal FactorUnidad { get; set; }

        [DataNames("AUTORIZADO")]
        public int IdAutorizado { get; set; }

        [DataNames("SKU")]
        public string NoIdentificacion { get; set; }

        public string Identificador {
            get { return this.NoIdentificacion + "-" + this.IdEspecificacion.ToString("00"); }
        }

        [DataNames("SEARCH")]
        public string Search { get; set; }

        public string Clave { get; set; }

        [DataNames("CTESPC_NOM")]
        public string Variante { get; set; }

        [DataNames("TASAIVA")]
        public decimal TasaIVA { get; set; }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        [DataNames("UNITARIO")]
        public decimal Unitario { get; set; }

        [DataNames("CTMDLX_EXT")]
        public decimal Existencia { get; set; }
    }
}
