﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase para representar un producto o servicio en detalle con sus modelos asociados (TIENDA WEB)
    /// </summary>
    public class ProductoServicioXDetailModel : ProductoServicioModel, IProductoServicioModel {
        #region declaraciones
        private BindingList<ModeloXDetailModel> _Modelos;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ProductoServicioXDetailModel() : base() {
            this._Modelos = new BindingList<ModeloXDetailModel>();
        }

        /// <summary>
        /// Desc:(1-producto, 2-servicio, 3-kit, 4-grupo de productos)
        /// </summary>           
        public ProdServTipoEnum Tipo {
            get {
                if (this.IdTipo == 1)
                    return ProdServTipoEnum.Producto;
                else if (this.IdTipo == 2)
                    return ProdServTipoEnum.Servicio;
                else
                    return ProdServTipoEnum.NoDefinido;
            }
            set {
                this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de almacen al que pertenece el producto
        /// </summary>
        public AlmacenEnum Almacen {
            get {
                if (this.IdAlmacen == 1)
                    return AlmacenEnum.MP;
                else
                    return AlmacenEnum.PT;
            }
            set {
                this.IdAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de modelos asociados al producto
        /// </summary>
        public BindingList<ModeloXDetailModel> Modelos {
            get {
                return this._Modelos;
            }
            set {
                this._Modelos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
