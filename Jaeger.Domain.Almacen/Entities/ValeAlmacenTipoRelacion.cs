﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    public class ValeAlmacenTipoRelacion : BaseSingleModel {
        public ValeAlmacenTipoRelacion() {
        }

        public ValeAlmacenTipoRelacion(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
