﻿using System;
using System.Net;
using System.IO;
using System.Drawing;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.Entities {
    ///<summary>
    ///Medios del catálogo de modelos
    ///</summary>
    [SugarTable("_ctlmdlmd")]
    public class MedioModel : BasePropertyChangeImplementation, IMedioModel {
        #region declaraciones
        private int id;
        private bool _Activo;
        private int subId;
        private string url;
        private Image imagen = null;
        #endregion

        public MedioModel() {
            this._Activo = true;
            this.imagen = null;
        }

        /// <summary>
        /// Desc:ID
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlmdlmd_id", "CTMDLM_ID")]
        [SugarColumn(ColumnName = "_ctlmdlmd_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.id;
            }
            set {
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLM_A")]
        [SugarColumn(IsIgnore = true)]
        public bool Activo {
            get { return this._Activo; }
            set { this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ID de modelo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlmdlmd_ctlmdl_id", "CTMDLM_CTMDL_ID")]
        [SugarColumn(ColumnName = "_ctlmdlmd_ctlmdl_id", ColumnDescription = "id de la tabla de modelos")]
        public int IdModelo {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:URL amazon
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_ctlmdlmd_url", "CTMDLM_URL")]
        [SugarColumn(ColumnName = "_ctlmdlmd_url", ColumnDescription = "url de amazon")]
        public string URL {
            get {
                return this.url;
            }
            set {
                this.url = value;
                this.OnPropertyChanged();
            }
        }
        
        [SugarColumn(IsIgnore = true)]
        public Image Imagen {
            get {
                if (this.imagen == null) {
                    try {
                        WebClient wc = new WebClient();
                        byte[] bytes = wc.DownloadData(this.URL);
                        MemoryStream ms = new MemoryStream(bytes);
                        this.imagen = Image.FromStream(ms);
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                        return new Bitmap(10, 10);
                    }
                }
                return this.imagen;
            }
        }
    }
}
