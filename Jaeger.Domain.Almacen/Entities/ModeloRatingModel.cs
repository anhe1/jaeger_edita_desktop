﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// comentarios de los clientes de la tienda web
    /// </summary>
    [SugarTable("CTMDLR")]
    public class ModeloRatingModel : BasePropertyChangeImplementation {
        #region public ModeloRatingModel() : this(
        private int _Index;
        private bool _Activo;
        private int _IdModelo;
        private int _Rating;
        private int _IdProveedor;
        private string _Nota;
        private string _URL;
        private DateTime _FechaNuevo;
        #endregion

        public ModeloRatingModel() {
            this._Activo = true;
            this._FechaNuevo = DateTime.Now;
        }

        [DataNames("CTMDLR_ID")]
        [SugarColumn(ColumnName = "CTMDLR_ID", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this._Index;
            }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_A")]
        [SugarColumn(ColumnName = "CTMDLR_A", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this._Activo;
            }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_RATING")]
        [SugarColumn(ColumnName = "CTMDLR_RATING")]
        public int Rating {
            get { return this._Rating; }
            set {
                this._Rating = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_CTMDL_ID")]
        [SugarColumn(ColumnName = "CTMDLR_CTMDL_ID", ColumnDescription = "indice principal de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdModelo {
            get {
                return this._IdModelo;
            }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_DRCTR_ID")]
        [SugarColumn(ColumnName = "CTMDLR_DRCTR_ID", ColumnDescription = "indice de relacion con el directorio de proveedores")]
        public int IdProveedor {
            get {
                return this._IdProveedor;
            }
            set {
                this._IdProveedor = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_NOTA")]
        [SugarColumn(ColumnName = "CTMDLR_NOTA", ColumnDescription = "notas", Length = 32, IsNullable = true)]
        public string Nota {
            get {
                return this._Nota;
            }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_URL")]
        [SugarColumn(ColumnName = "CTMDLR_URL", ColumnDescription = "url de amazon")]
        public string URL {
            get {
                return this._URL;
            }
            set {
                this._URL = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTMDLR_FN")]
        [SugarColumn(ColumnName = "CTMDLR_FN", ColumnDescription = "fecha de creacion del registro", ColumnDataType = "TIMESTAMP", DefaultValue = "CURRENT_TIMESTAMP")]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
