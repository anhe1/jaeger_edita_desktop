﻿using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase de vista para las clasificaciones de categorias de bienes, productos y servicios (BPS) del almacen de producto terminado, tambien se utiliza en la tienda web
    /// </summary>
    public class CategoriaSingle : CategoriaModel, ICategoriaModel, ICategoriaSingle {
        public CategoriaSingle() : base() { }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase
        /// </summary>
        [DataNames("Clase")]
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer nivel en la clasificacion
        /// </summary>
        [DataNames("Nivel")]
        public int Nivel { get; set; }

        /// <summary>
        /// obtener o establecer cantidad de hijos del nivel actual
        /// </summary>
        [DataNames("Childs")]
        public int Childs { get; set; }

        /// <summary>
        /// obtener la descripcion compuesta de la descripción
        /// </summary>
        public new string Descriptor {
            get {
                return this.Descripcion;
            }
        }

        public string Codigo {
            get { return string.Format("{0}.{1}.{2}", this.IdCategoria.ToString("00#"), this.IdSubCategoria.ToString("00#"), 0); }
        }
    }
}
