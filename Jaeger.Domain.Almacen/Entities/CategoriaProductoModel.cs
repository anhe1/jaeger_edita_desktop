﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.Entities {
    /// <summary>
    /// clase de vista para las clasificaciones de categorias de bienes, productos y servicios (BPS) del almacen de producto terminado, tambien se utiliza en la tienda web
    /// </summary>
    public class CategoriaProductoModel : ProductoServicioModel, Contracts.IProductoServicioModel, Contracts.ICategoriaProductoSingle, Contracts.ICategoriaModel {
        #region declaraciones
        private int _IdSubCategoria;
        private string _Descripcion;
        #endregion

        public CategoriaProductoModel() : base() { }

        /// <summary>
        /// obtener o estanlecer sub indice de la categoria
        /// </summary>
        [DataNames("SUBID")]
        public int IdSubCategoria {
            get {
                return this._IdSubCategoria;
            }
            set {
                this._IdSubCategoria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nivel en la clasificacion
        /// </summary>
        [DataNames("NIVEL")]
        public int Nivel { get; set; }

        /// <summary>
        /// obtener o establecer cantidad de hijos del nivel actual
        /// </summary>
        [DataNames("CHILDS")]
        public int Childs { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase
        /// </summary>
        [DataNames("CLASE")]
        public string Clase { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion, nombre de la categoria
        /// </summary>           
        [DataNames("DESCRIPCION")]
        public string Descripcion {
            get {
                return this._Descripcion;
            }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion, nombre de la categoria
        /// </summary>           
        public string Categoria { get { return this._Descripcion; } }

        [DataNames("URL")]
        public string URL { get; set; }

        /// <summary>
        /// obtener descriptor Categoria - Nombre del producto
        /// </summary>
        public string Descriptor {
            get { return string.Concat(this.Descripcion, " ", this.Nombre); }
        }
    }
}
