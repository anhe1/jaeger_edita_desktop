﻿namespace Jaeger.Domain.Almacen.Entities {
    public class MetodoEnvioModel : Base.Abstractions.BaseSingleModel {
        public MetodoEnvioModel() {

        }

        public MetodoEnvioModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
