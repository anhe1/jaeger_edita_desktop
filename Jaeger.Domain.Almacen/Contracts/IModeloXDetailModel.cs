﻿using Jaeger.Domain.Almacen.Entities;
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IModeloXDetailModel {
        BindingList<ModeloYEspecificacionModel> Disponibles { get; set; }

        string NoIdentificacion { get; set; }
    }
}
