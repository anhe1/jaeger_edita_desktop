﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones de Bienes, Productos y Servicios (BPS) del almacen de producto terminado
    /// </summary>
    public interface ISqlCategoriaRepository : IGenericRepository<CategoriaModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
