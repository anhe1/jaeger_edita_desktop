﻿using System;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IMovimientoModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (_ID)
        /// </summary>
        int IdMovimiento { get; set; }

        /// <summary>
        /// obtener o establecer registro activo (_A)
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del departamento (_CTALM_ID)
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer tipo de documento (_DOC_ID)
        /// </summary>
        int IdTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el efecto del movimiento (_CTEFC_ID)
        /// </summary>
        int IdTipoMovimiento { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de Almacen de Producto Terminado (_???_ID)
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de remisiones
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// obtener o establecer el numero de pedido del cliente hasta 21 caracteres
        /// </summary>
        int IdPedido { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de departamentos
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de modelos
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de precios
        /// </summary>
        int IdPrecio { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de especificaciones
        /// </summary>
        int IdEspecificacion { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de unidad
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer el factor de la unidad
        /// </summary>
        decimal UnidadFactor { get; set; }

        /// <summary>
        /// obtener o establecer el nombre de la unidad
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad entrante
        /// </summary>
        decimal CantidadE { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad saliente
        /// </summary>
        decimal CantidadS { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        string Producto { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        string Modelo { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la marca
        /// </summary>
        string Marca { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especificacion
        /// </summary>
        string Especificacion { get; set; }

        /// <summary>
        /// obtener o establecer costo unitario (MVAPT_UNTC)
        /// </summary>
        decimal CostoUnitario { get; set; }

        /// <summary>
        /// obtener o establecer costo de la unidad (MVAPT_UNDC)
        /// </summary>
        decimal CostoUnidad { get; set; }

        /// <summary>
        /// obtener o establecer valor unitario por pieza (MVAPT_UNTR)
        /// </summary>
        decimal ValorUnitario { get; set; }

        /// <summary>
        /// obtener o establecer valor unitario de la unidad (MVAPT_UNTR2)
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer sub total de la partida
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        decimal Importe { get; set; }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        decimal TasaIVA { get; set; }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        decimal TrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Identificador { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
