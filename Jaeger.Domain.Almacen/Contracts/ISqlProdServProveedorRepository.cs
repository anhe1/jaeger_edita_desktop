﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlProdServProveedorRepository : IGenericRepository<ModeloProveedorModel> {

        ModeloProveedorModel Save(ModeloProveedorModel proveedor);
    }
}
