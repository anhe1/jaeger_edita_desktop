﻿namespace Jaeger.Domain.Almacen.Contracts {
    public interface IEspecificacionModel {
        /// <summary>
        /// obtener o establecer indice de la tabla (CTESPC_ID)
        /// </summary>
        int IdEspecificacion { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer (LSTSYS_FN)
        /// </summary>
        System.DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer (LSTSYS_USU_N)
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer (LSTSYS_FM)
        /// </summary>
        System.DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer (LSTSYS_USU_M)
        /// </summary>
        string Modifica { get; set; }

        bool SetModified { get; set; }
    }
}
