﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// repositorio de medios
    /// </summary>
    public interface ISqlMediosRepository : IGenericRepository<MedioModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener lista de medios asociado al sub indic
        /// </summary>
        /// <param name="idModelo">indice</param>
        //IEnumerable<MedioModel> GetListById(int idModelo);

        /// <summary>
        /// almacenar una imagen
        /// </summary>
        MedioModel Save(MedioModel medio);
    }
}
