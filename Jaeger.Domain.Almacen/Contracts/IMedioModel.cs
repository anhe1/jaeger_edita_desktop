﻿namespace Jaeger.Domain.Almacen.Contracts {
    public interface IMedioModel {
        int Id { get; set; }

        int IdModelo { get; set; }

        string URL { get; set; }
    }
}