﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlContribuyenteRepository : IGenericRepository<ReceptorModel> {
        /// <summary>
        /// obtener listado de contribuyentes del directorio
        /// </summary>
        /// <param name="onlyActive">verdadero solo regristros activos</param>
        /// <param name="tipoRelacion">Tipo de relaciona con el directorio</param>
        IEnumerable<ReceptorDetailModel> GetList(bool onlyActive, TipoRelacionComericalEnum tipoRelacion = TipoRelacionComericalEnum.None);

        IEnumerable<ReceptorModel> GetList(TipoRelacionComericalEnum tipoRelacion, bool onlyActive);
    }
}
