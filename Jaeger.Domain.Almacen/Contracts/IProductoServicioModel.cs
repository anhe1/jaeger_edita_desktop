﻿namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// clase modelo de producto o servicio
    /// </summary>
    public interface IProductoServicioModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        bool Activo { get; set; }

        int IdAlmacen { get; set; }

        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion de la categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u ordenamiento de objeto
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer clave de producto para formar URL
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del producto
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        System.DateTime FechaNuevo { get; set; }

        string Codigo { get; }
    }
}