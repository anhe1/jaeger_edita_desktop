﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlEspecificacionRepository : IGenericRepository<EspecificacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        EspecificacionModel Save(EspecificacionModel model);
    }
}
