﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// interface de publicacion de productos de tienda web
    /// </summary>
    public interface ISqlPublicacionRepository : IGenericRepository<PublicacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1: class, new();

        bool Saveable(PublicacionModel model);
    }
}
