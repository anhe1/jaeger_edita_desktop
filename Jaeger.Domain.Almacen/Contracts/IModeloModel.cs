﻿using System;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IModeloModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla _ctlmdl_id
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        bool Activo { get; set; }

        int IdPrecio { get; set; }

        /// <summary>
        /// Desc:id categoría
        /// </summary>           
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obetener o establecer bandera de autorizacion del modelo
        /// </summary>
        int Autorizado { get; set; }

        /// <summary>
        /// obtener o establecer indice del almacen al que pertenece el modelo
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de modelo (producto o servicio)
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// alm:|tw| desc: para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)
        /// </summary>
        int IdVisible { get; set; }

        /// <summary>
        /// obtener o establecer si el modelo esta disponible
        /// </summary>
        bool IsAvailable { get; set; }

        /// <summary>
        /// obtener o establecer la unidad utilizada en largo y ancho
        /// </summary>
        int UnidadXY { get; set; }

        /// <summary>
        /// obtener o establecer la unidad utilizada en alto o calibre
        /// </summary>
        int UnidadZ { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IVA trasladado, puede ser TASA y valor FIJO
        /// </summary>
        string FactorTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        decimal? ValorTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS trasladado, puede ser TASA ó CUOTA
        /// </summary>
        string FactorTrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor % del impuesto IEPS, puede ser valor FIJO o RANGO, si el factor es TASA es fijo, si el factor es CUOTA puede ser rango
        /// </summary>
        decimal? ValorTrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA, puede ser un rango cuando el factor es TASA
        /// </summary>
        decimal? ValorRetencionIVA { get; set; }

        /// <summary>
        /// obtener o establecer el factor del impuesto IEPS que puede ser TASA ó CUOTA
        /// </summary>
        string FactorRetencionIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS, puede ser FIJO si al factor es TASA ó RANGO si el factor es CUOTA
        /// </summary>
        decimal? ValorRetencionIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido ISR, si el factor es TASA el valor puede ser un RANGO
        /// </summary>
        decimal? ValorRetecionISR { get; set; }

        decimal Largo { get; set; }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        decimal Ancho { get; set; }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        decimal Alto { get; set; }

        /// <summary>
        /// obtener o establecer el alto o calibre
        /// </summary>
        decimal Peso { get; set; }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        string ClaveProdServ { get; set; }

        /// <summary>
        /// Desc:Código de barras
        /// </summary>           
        string Codigo { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer el número del pedimento que ampara la importación del bien que se expresa en el siguiente formato: 
        /// últimos 2 dígitos del año de validación seguidos por dos espacios, 2 dígitos de la aduana de despacho seguidos por dos espacios, 
        /// 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo 
        /// que se trate de un pedimento consolidado iniciado en el año inmediato anterior o del pedimento original de una rectificación, 
        /// seguido de 6 dígitos de la numeración progresiva por aduana.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}" longitud: 21
        /// </summary>
        string NumRequerimiento { get; set; }

        /// <summary>
        /// Nodo opcional para asentar el número de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar 
        /// los datos de identificación del certificado de participación inmobiliaria no amortizable.
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado de participación 
        /// inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{2} [0-9]{2} [0-9]{4} [0-9]{7}", Longitud: 21
        /// </summary>
        string CtaPredial { get; set; }

        /// <summary>
        /// obtener o establecer el número de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operación del emisor, 
        /// amparado por el presente concepto. Opcionalmente se puede utilizar claves del estándar GTIN.
        /// </summary>
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,100}"
        string NoIdentificacion { get; set; }

        /// <summary>
        /// Desc:marca del producto
        /// </summary>           
        string Marca { get; set; }

        string Especificacion { get; set; }

        /// <summary>
        /// Desc:descripcion corta del producto
        /// </summary>           
        string Descripcion { get; set; }

        /// <summary>
        /// Desc:clave del usuario que crea el registro
        /// </summary>           
        string Creo { get; set; }

        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// Desc:clave del usuario que modifica
        /// </summary>           
        string Modifica { get; set; }
        DateTime? FechaModifica { get; set; }
    }
}