﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IProductoServicioDetailModel : IProductoServicioModel {
        ProdServTipoEnum Tipo { get; set; }
        AlmacenEnum Almacen { get; set; }
        BindingList<ModeloDetailModel> Modelos { get; set; }
    }
}