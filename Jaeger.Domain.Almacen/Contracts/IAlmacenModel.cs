﻿using System;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IAlmacenModel {
        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        AlmacenEnum Tipo { get; set; }

        /// <summary>
        /// obtener o establecer clave del almacen
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establcer descripcion del almacen
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        DateTime FechaNuevo { get; set; }

        string Descriptor { get; }
    }
}
