﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlModelosRepository : IGenericRepository<ModeloModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        IEnumerable<ModeloDetailModel> GetList(AlmacenEnum tipo, int index, bool onlyActive = true);
        ModeloDetailModel Save(ModeloDetailModel model);
        ModeloModel Save(ModeloModel item);
        bool Autoriza(ModeloModel model);
        //ModeloProveedorDetailModel Save(ModeloProveedorDetailModel model);
    }
}
