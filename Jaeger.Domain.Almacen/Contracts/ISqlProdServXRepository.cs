﻿using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// repositorio del catalogo de productos
    /// </summary>
    public interface ISqlProdServXRepository : IGenericRepository<ProductoServicioModel>, ISqlProdServRepository {
        /// <summary>
        /// desactivar producto o servicio
        /// </summary>
        int Delete(ProductoServicioModel item);

        /// <summary>
        /// almacenar objeto Producto-Servicio Details
        /// </summary>
        /// <param name="model">modelo Producto-Servicio-Details</param>
        /// <returns>mismo objeto con id actualizado</returns>
        ProductoServicioXDetailModel Save(ProductoServicioXDetailModel model);
    }
}