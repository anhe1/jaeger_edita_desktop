﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// repositorio del catalogo de productos
    /// </summary>
    public interface ISqlProdServRepository : IGenericRepository<ProductoServicioModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        ProductoServicioDetailModel Save(ProductoServicioDetailModel model);

        ProductoServicioModel Save(ProductoServicioModel model);
    }
}
