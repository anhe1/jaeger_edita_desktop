﻿using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IComprobanteAlmacen {
        int IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer si el objeto es de solo lectura
        /// </summary>
        bool IsReadOnly { get; set; }

        /// <summary>
        /// obtener o establecer el documento debe afectar los saldos
        /// </summary>
        bool IsSum { get; set; }

        AlmacenEnum Tipo { get; set; }

        TipoMovimientoEnum TipoMovimiento { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del tipo de operacion
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer el formato de impresion del documento
        /// </summary>
        string FormatoImpreso { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia de tipos de objetos utilizados como receptores del tipo de movimiento
        /// </summary>
        string Receptores { get; set; }

        /// <summary>
        /// obtener el descriptor del tipo de documento {Clave:Descripcion}
        /// </summary>
        string Descriptor { get; }
    }
}
