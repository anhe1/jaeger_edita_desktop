﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// Repositorio para Modelos-Especificaciones
    /// </summary>
    public interface ISqlModeloEspecificacionRepository : IGenericRepository<ModeloYEspecificacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        
        ModeloYEspecificacionModel Save(ModeloYEspecificacionModel model);
    }
}