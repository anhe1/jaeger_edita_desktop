﻿using System;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ICategoriaModel {
        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        int IdSubCategoria { get; set; }

        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        [Obsolete("No se utiliza en esta versión")]
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        int Secuencia { get; set; }

        /// <summary>
        /// obtener o establecer clave de la categoria
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador de la categoria
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion 
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener descripcion de la categoria
        /// </summary>
        string Descriptor { get; }
    }
}