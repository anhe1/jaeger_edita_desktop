﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlAlmacenRepository : IGenericRepository <AlmacenModel> {
        IEnumerable<IAlmacenModel> GetList(List<IConditional> conditionals);

        AlmacenModel Save(AlmacenModel model);
    }
}
