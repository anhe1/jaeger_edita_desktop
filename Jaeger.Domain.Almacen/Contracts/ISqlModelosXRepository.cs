﻿using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlModelosXRepository : ISqlModelosRepository {
        ModeloXDetailModel Save(ModeloXDetailModel model);
    }
}