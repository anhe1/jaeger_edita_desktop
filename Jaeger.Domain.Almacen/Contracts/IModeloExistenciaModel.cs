﻿namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// interface para el modelo existencias
    /// </summary>
    public interface IModeloExistenciaModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla de productos
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer el indice de la tabla de modelos
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer indice de la especificacion o variante
        /// </summary>
        int IdEspecificacion { get; set; }

        /// <summary>
        /// indice del catalogo de almacenes (CTALM_ID)
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer indice del tipo de almacen MP = 1, PT = 2
        /// </summary>
        int IdTipoAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer cantidad de existencia del modelo o producto
        /// </summary>
        decimal Existencia { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad minima
        /// </summary>
        decimal Minimo { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad maxima
        /// </summary>
        decimal Maximo { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad del punto de reorden
        /// </summary>
        decimal ReOrden { get; set; }
    }
}
