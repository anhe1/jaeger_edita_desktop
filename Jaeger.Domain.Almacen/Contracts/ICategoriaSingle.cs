﻿namespace Jaeger.Domain.Almacen.Contracts {
    public interface ICategoriaSingle {
        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        int IdSubCategoria { get; set; }

        /// <summary>
        /// obtener o establecer nivel en la clasificacion
        /// </summary>
        int Nivel { get; set; }

        /// <summary>
        /// obtener o establecer cantidad de hijos del nivel actual
        /// </summary>
        int Childs { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la clase
        /// </summary>
        string Clase { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener descripcion de la categoria
        /// </summary>
        string Descriptor { get; }

        /// <summary>
        /// obtener codigo de la clasificacion {IdCategoria}{IdSubCategoria}{0}
        /// </summary>
        string Codigo { get; }
    }
}