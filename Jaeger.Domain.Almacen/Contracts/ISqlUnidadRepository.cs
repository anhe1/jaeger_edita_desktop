﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface ISqlUnidadRepository : IGenericRepository<UnidadModel> {

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        UnidadModel Save(UnidadModel model);
    }
}
