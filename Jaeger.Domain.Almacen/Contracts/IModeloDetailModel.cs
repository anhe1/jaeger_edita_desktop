﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Contracts {
    public interface IModeloDetailModel : IModeloModel {
        /// <summary>
        /// obtener o establecer el almacen al que pertenece Materia Prima o Producto terminado
        /// </summary>
        AlmacenEnum Almacen { get; set; }
        /// <summary>
        /// obtener o establecer si es un producto o servicio
        /// </summary>
        ProdServTipoEnum Tipo { get; set; }
        string AutorizadoText { get; }

        new string NoIdentificacion { get; set; }
        BindingList<MedioModel> Medios { get; set; }
    }
}