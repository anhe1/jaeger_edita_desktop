﻿namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// Catagoria + Descripcion del producto o servicio
    /// </summary>
    public interface IClasificacionProductoSingle {
        /// <summary>
        /// obtener o establecer indice de la categoria
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer indice de la sub categoria
        /// </summary>
        int IdSubCategoria { get; set; }
        
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer descripcion de la categoria
        /// </summary>
        string Descripcion { get; set; }
        
        string Clase { get; set; }
        
        string Nombre { get; set; }

        string Descriptor { get; }
        
        string Codigo { get; }
    }
}