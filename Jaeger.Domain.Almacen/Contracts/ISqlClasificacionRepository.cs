﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones de Bienes, Productos y Servicios (BPS) del almacen de producto terminado
    /// </summary>
    public interface ISqlClasificacionRepository : IGenericRepository<ClasificacionModel> {
        /// <summary>
        /// Lista generica condicional
        /// </summary>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        int Salveable(ClasificacionModel model);
    }
}
