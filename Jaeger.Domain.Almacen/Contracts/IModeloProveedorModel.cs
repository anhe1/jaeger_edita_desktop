﻿namespace Jaeger.Domain.Almacen.Contracts {
    /// <summary>
    /// tabla de proveedores
    /// </summary>
    public interface IModeloProveedorModel {
        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del modelo
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de proveedores
        /// </summary>
        int IdProveedor { get; set; }

        /// <summary>
        /// obtener o establecer valor unitario
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de modificacion del registro
        /// </summary>
        System.DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer clave de usuario que modifica el registro
        /// </summary>
        string Modifico { get; set; }
    }
}
