﻿namespace Jaeger.Domain.Almacen.Contracts {
    public interface IProductoModeloLayout {
        /// <summary>
        /// obtener o establecer el indice de la categoria del producto o modelo
        /// </summary>
        int IdCategoria { get; set; }

        /// <summary>
        /// obtener o establecer el nombre de la categoria en modo texto
        /// </summary>
        string Categoria { get; set; }

        /// <summary>
        /// obtener o establecer indice del producto
        /// </summary>
        int IdProducto { get; set; }

        string Producto { get; set; }

        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer descripcion del modelo
        /// </summary>
        string Modelo { get; set; }

        /// <summary>
        /// obtener o establecer marca
        /// </summary>
        string Marca { get; set; }

        /// <summary>
        /// obtener o establecer especificacion
        /// </summary>
        string Especificacion { get; set; }

        /// <summary>
        /// modelos, publicacion para la tienda web
        /// </summary>
        string Publicacion { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,20}"
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer le valor unitario del modelo
        /// </summary>
        decimal Unitario { get; set; }

        /// <summary>
        /// obtener o establecer el valor del IVA trasladado %
        /// </summary>
        decimal TasaIVA { get; set; }

        string Tienda { get; set; }

        /// <summary>
        /// obtener o establecer la secuencia u orden del objeto a visualizar
        /// </summary>
        int Secuencia { get; set; }

        decimal Largo { get; set; }

        /// <summary>
        /// obtener o establecer el ancho
        /// </summary>
        decimal Ancho { get; set; }

        decimal Alto { get; set; }

        decimal Peso { get; set; }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        string ClaveProdServ { get; set; }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        string ClaveUnidad { get; set; }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        string ObjetoImp { get; set; }

        string Imagen { get; set; }
    }
}
