﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    public interface IEspecificacionQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// retgistros activos
        /// </summary>
        /// <param name="isActive">default true</param>
        IEspecificacionActiveQueryBuilder IsActive(bool isActive = true);
    }

    public interface IEspecificacionActiveQueryBuilder : IConditionalBuilder {

    }
}
