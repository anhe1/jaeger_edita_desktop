﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    /// <summary>
    /// consulta de modelos
    /// </summary>
    public interface IModelosQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// por tipo de del almacen
        /// </summary>
        IModelosQueryAlmacenBuilder ByIdAlmacen(AlmacenEnum idAlmacen);
        /// <summary>
        /// por tipo de del almacen
        /// </summary>
        IModelosQueryAlmacenBuilder ByIdAlmacen(int idAlmacen);
    }

    public interface IModelosQueryAlmacenBuilder {
        /// <summary>
        /// registro activo
        /// </summary>
        /// <param name="isActivo">default: true</param>
        IModelosQueryActivoBuilder Activo(bool isActivo = true);
    }

    public interface IModelosQueryActivoBuilder : IConditionalBuilder {
        /// <summary>
        /// busqueda por descripcion
        /// </summary>
        IModelosQueryAuthorizedBuilder Search(string descripcion);
        IModelosIdProductoQueryBuilder IdProducto(int[] ids);
        IModelosQueryAuthorizedBuilder Visibilidad();
    }

    public interface IModelosQueryAuthorizedBuilder : IConditionalBuilder {
        /// <summary>
        /// solo producto autorizados
        /// </summary>
        /// <param name="onlyAuthorized">default: true</param>
        IConditionalBuilder OnlyAuthorized(bool onlyAuthorized = true);
    }

    public interface IModelosIdProductoQueryBuilder : IConditionalBuilder {
        
    }
}
