﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public class UnidadQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IUnidadQueryBuilder, IUnidadAlmacenQueryBuilder, IUnidadActivoQueryBuilder {
        public UnidadQueryBuilder() : base() {
            
        }

        public IUnidadAlmacenQueryBuilder ByAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("ctund_alm_id", d0.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public IUnidadActivoQueryBuilder Activo(bool isActivo = true) {
            if (isActivo) {
                this._Conditionals.Add(new Conditional("ctund_a", "1"));
            }
            return this;
        }
    }
}
