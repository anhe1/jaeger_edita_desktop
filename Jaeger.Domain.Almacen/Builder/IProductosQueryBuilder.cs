﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public interface  IProductosQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IProductosAlmacenQueryBuilder ByAlmacen(AlmacenEnum idAlmacen);

        /// <summary>
        /// obtener producto por su indice
        /// </summary>
        IProductosAlmacenQueryBuilder ById(int index);
    }

    public interface IProductosAlmacenQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IProductosActivoQueryBuilder Activo(bool onlyActive = true);

        /// <summary>
        /// busqueda
        /// </summary>
        IProductosSearchQueryBuilder Search(string description);
    }

    public interface IProductosActivoQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IProductosActivoQueryBuilder Activo(bool onlyActive = true);
    }

    public interface IProductosSearchQueryBuilder : IConditionalBuilder {

    }
}
