﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.Builder {
    /// <summary>
    /// clase builder para consutla de existencias
    /// </summary>
    public class ExistenciasQueryBuilder : ModelosQueryBuilder, IConditionalBuilder, IConditionalBuild, IExistenciasQueryBuilder, IExistenciasFromAlmacenQueryBuilder {
        /// <summary>
        /// indice del almacen 
        /// </summary>
        /// <param name="idAlmacen">indice del almacen</param>
        public IExistenciasFromAlmacenQueryBuilder FromAlmacen(int idAlmacen) {
            this.Conditionals.Add(new Conditional("CTMDLX_CTALM_ID", idAlmacen.ToString()));
            return this;
        }
    }
}
