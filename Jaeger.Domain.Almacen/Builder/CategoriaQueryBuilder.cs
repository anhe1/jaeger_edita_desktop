﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.Builder {
    public class CategoriaQueryBuilder : ConditionalBuilder, ICategoriaQueryBuilder, IConditionalBuilder, IConditionalBuild, ICategoriaActivoQueryBuilder {
        public CategoriaQueryBuilder() : base() { }

        public virtual ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive)
                this._Conditionals.Add(new Conditional("CTCLS_A", "1"));
            return this;
        }
    }
}
