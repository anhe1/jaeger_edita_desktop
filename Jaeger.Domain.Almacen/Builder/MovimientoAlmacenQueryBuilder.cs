﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.Builder {
    public class MovimientoAlmacenQueryBuilder : ConditionalBuilder, IConditionalBuilder, IMovimientoAlmacenQueryBuilder, IMovimientoAlmacenRemisionQueryBuilder, IMovimientoAlmacenDocumentoQueryBuilder,
        IMovimientoAlmaceCanceladoQueryBuilder {

        /// <summary>
        /// solo registros activos
        /// </summary>
        public MovimientoAlmacenQueryBuilder() : base() {
            this._Conditionals.Add(new Conditional("MVAPT_A", "1"));
        }

        /// <summary>
        /// remision documento 26
        /// </summary>
        /// <param name="idRemision">indice del documento MVAPT_RMSN_ID</param>
        public IMovimientoAlmacenRemisionQueryBuilder Remision(int idRemision) {
            this._Conditionals.Add(new Conditional("MVAPT_DOC_ID", "26"));
            this._Conditionals.Add(new Conditional("MVAPT_RMSN_ID", idRemision.ToString()));
            return this;
        }

        /// <summary>
        /// documento de almacen
        /// </summary>
        /// <param name="idComprobante">id MVAPT_ALMPT_ID</param>
        public IMovimientoAlmacenRemisionQueryBuilder Documento(int idComprobante) {
            this._Conditionals.Add(new Conditional("MVAPT_ALMPT_ID", idComprobante.ToString()));
            return this;
        }

        public IMovimientoAlmaceCanceladoQueryBuilder IsCancel(bool isCancel = false) {
            if (isCancel) this._Conditionals.Add(new Conditional("@cancelado", ""));
            return this;
        }
    }
}
