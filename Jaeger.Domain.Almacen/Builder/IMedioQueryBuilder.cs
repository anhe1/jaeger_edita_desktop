﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    public interface IMedioQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// obtener por indice de la tabla
        /// </summary>
        IMedioIdQueryBuilder Id(int index);

        /// <summary>
        /// obtener por indice del modelo
        /// </summary>
        IMedioIdQueryBuilder IdModelo(int idModelo);

        /// <summary>
        /// obtener por indice del modelo
        /// </summary>
        IMedioIdQueryBuilder IdModelo(int[] idModelo);
    }

    public interface IMedioIdQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IMedioActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IMedioActiveQueryBuilder : IConditionalBuilder, IConditionalBuild {

    }
}
