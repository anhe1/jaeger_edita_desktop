﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public class MedioQueryBuilder : ConditionalBuilder, IMedioQueryBuilder, IMedioIdQueryBuilder, IMedioActiveQueryBuilder, IConditionalBuilder, IConditionalBuild {
        public MedioQueryBuilder() : base() { }

        public virtual IMedioIdQueryBuilder Id(int index) {
            this._Conditionals.Add(new Conditional("CTMDLM_ID", index.ToString()));
            return this;
        }

        public virtual IMedioIdQueryBuilder IdModelo(int idModelo) {
            this._Conditionals.Add(new Conditional("CTMDLM_CTMDL_ID", idModelo.ToString()));
            return this;
        }

        public virtual IMedioIdQueryBuilder IdModelo(int[] idModelo) {
            this._Conditionals.Add(new Conditional("CTMDLM_CTMDL_ID", string.Join(",", idModelo), ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public virtual IMedioActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) { this._Conditionals.Add(new Conditional("CTMDLM_A", "1")); }
            return this;
        }
    }
}
