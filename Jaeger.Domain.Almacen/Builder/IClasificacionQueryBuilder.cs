﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    /// <summary>
    /// interface para clasificacion o categorias de BPS
    /// </summary>
    public interface IClasificacionQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IClasificacionActivoQueryBuilder OnlyActive(bool onlyActive = true);
        [System.Obsolete("El indice del almacen no se utiliza")]
        IClasificacionTipoAlmacenQueryBuilder ByAlmacen(Base.ValueObjects.AlmacenEnum almacen);
        IClasificacionProductosQueryBuilder Productos();
    }

    [System.Obsolete("El indice del almacen no se utiliza")]
    public interface IClasificacionTipoAlmacenQueryBuilder : IConditionalBuilder {
        IClasificacionActivoQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IClasificacionActivoQueryBuilder : IConditionalBuilder {

    }

    public interface IClasificacionProductosQueryBuilder : IConditionalBuilder {
        IClasificacionNivelQueryBuilder Nivel(int nivel = 1);
    }

    public interface IClasificacionNivelQueryBuilder : IConditionalBuilder {
        IClasificacionActivoQueryBuilder OnlyActive(bool onlyActive = true);
    }
}
