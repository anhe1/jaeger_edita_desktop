﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public interface IUnidadQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IUnidadAlmacenQueryBuilder ByAlmacen(AlmacenEnum idAlmacen);
    }

    public interface IUnidadAlmacenQueryBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        IUnidadActivoQueryBuilder Activo(bool isActivo = true);
    }

    public interface IUnidadActivoQueryBuilder : IConditionalBuilder {

    }
}
