﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public class ProductosQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IProductosQueryBuilder, IProductosAlmacenQueryBuilder, IProductosActivoQueryBuilder,
        IProductosSearchQueryBuilder {
        public ProductosQueryBuilder() : base() {

        }

        /// <summary>
        /// obtener producto por su indice
        /// </summary>
        public virtual IProductosAlmacenQueryBuilder ById(int index) {
            this._Conditionals.Add(new Conditional("CTPRD_ID", index.ToString()));
            return this;
        }

        /// <summary>
        /// NA = 0, MP = 1, PT = 2
        /// </summary>
        public virtual IProductosAlmacenQueryBuilder ByAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("CTPRD_ALM_ID", d0.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public virtual IProductosActivoQueryBuilder Activo(bool activo = true) {
            if (activo) this._Conditionals.Add(new Conditional("CTPRD_A", "1"));
            return this;
        }

        public virtual IProductosSearchQueryBuilder Search(string description) {
            var valor = description.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ñ", "n");
            this._Conditionals.Add(new Conditional("@search", valor));
            return this;
        }
    }
}
