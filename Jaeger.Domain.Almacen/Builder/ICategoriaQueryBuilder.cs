﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    public interface ICategoriaQueryBuilder : IConditionalBuilder, IConditionalBuild {
        ICategoriaActivoQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface ICategoriaActivoQueryBuilder : IConditionalBuilder {

    }
}
