﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.Builder {
    public class EspecificacionQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IEspecificacionQueryBuilder, IEspecificacionActiveQueryBuilder {
        public EspecificacionQueryBuilder() : base() { }

        public IEspecificacionActiveQueryBuilder IsActive(bool isActive = true) {
            this._Conditionals.Add(new Conditional("CTESPC_A", "1"));
            return this;
        }
    }
}
