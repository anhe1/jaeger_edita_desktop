﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.Builder {
    /// <summary>
    /// interface para clasificacion o categorias de BPS
    /// </summary>
    public class ClasificacionQueryBuilder : ConditionalBuilder, IClasificacionQueryBuilder, IConditionalBuilder, IConditionalBuild, IClasificacionTipoAlmacenQueryBuilder, IClasificacionActivoQueryBuilder,
        IClasificacionNivelQueryBuilder, IClasificacionProductosQueryBuilder {
        public ClasificacionQueryBuilder() : base() {

        }

        [System.Obsolete("El indice del almacen no se utiliza")]
        public virtual IClasificacionTipoAlmacenQueryBuilder ByAlmacen(Base.ValueObjects.AlmacenEnum almacen) {
            var v1 = (int)almacen;
            this._Conditionals.Add(new Conditional("CTCAT_ALM_ID", v1.ToString()));
            return this;
        }

        public virtual IClasificacionActivoQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("CTCAT_A", "1"));
            return this;
        }

        public virtual IClasificacionProductosQueryBuilder Productos() {
            return this;
        }

        public virtual IClasificacionNivelQueryBuilder Nivel(int nivel = 1) {
            this._Conditionals.Add(new Conditional("_NIVEL", nivel.ToString()));
            return this;
        }
    }
}
