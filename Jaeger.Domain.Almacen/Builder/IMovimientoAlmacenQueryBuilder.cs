﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    public interface IMovimientoAlmacenQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// remision documento 26
        /// </summary>
        /// <param name="idRemision">indice del documento MVAPT_RMSN_ID</param>
        IMovimientoAlmacenRemisionQueryBuilder Remision(int idRemision);

        /// <summary>
        /// documento de almacen
        /// </summary>
        /// <param name="idComprobante">id MVAPT_ALMPT_ID</param>
        IMovimientoAlmacenRemisionQueryBuilder Documento(int idComprobante);
    }

    /// <summary>
    /// remision documento 26
    /// </summary>
    /// <param name="idRemision">indice del documento MVAPT_RMSN_ID</param>
    public interface IMovimientoAlmacenRemisionQueryBuilder {
        IMovimientoAlmaceCanceladoQueryBuilder IsCancel(bool isCancel = false);
    }

    /// <summary>
    /// documento de almacen
    /// </summary>
    /// <param name="idComprobante">id MVAPT_ALMPT_ID</param>
    public interface IMovimientoAlmacenDocumentoQueryBuilder { }

    public interface IMovimientoAlmaceCanceladoQueryBuilder : IConditionalBuilder { }
}
