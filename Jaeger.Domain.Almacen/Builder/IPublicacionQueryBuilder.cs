﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public interface IPublicacionQueryBuilder : IConditionalBuilder {
        IPublicacionActiveQueryBuilder IsActive(int[] idModelo);
    }

    public interface IPublicacionActiveQueryBuilder : IConditionalBuilder {
    }

    public class PublicacionQueryBuilder : ConditionalBuilder, IPublicacionQueryBuilder, IConditionalBuilder, IPublicacionActiveQueryBuilder {
        public PublicacionQueryBuilder() : base() {

        }

        public IPublicacionActiveQueryBuilder IsActive(int[] idModelo) {
            this._Conditionals.Add(new Conditional("CTMDLT_CTMDL_ID", string.Join(",", idModelo), ConditionalTypeEnum.In));
            return this;
        }
    }
}
