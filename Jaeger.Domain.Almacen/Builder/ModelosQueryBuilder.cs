﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.Builder {
    public class ModelosQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IModelosQueryBuilder, IModelosQueryAlmacenBuilder, IModelosQueryActivoBuilder, IModelosQueryAuthorizedBuilder,
        IModelosIdProductoQueryBuilder {
        public ModelosQueryBuilder() : base() { }

        /// <summary>
        /// por tipo de almacen NA = 0, MP = 1, PT = 2
        /// </summary>
        public virtual IModelosQueryAlmacenBuilder ByIdAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("CTMDL_ALM_ID", d0.ToString()));
            return this;
        }

        /// <summary>
        /// por tipo de almacen NA = 0, MP = 1, PT = 2
        /// </summary>
        public virtual IModelosQueryAlmacenBuilder ByIdAlmacen(int idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("CTMDL_ALM_ID", d0.ToString()));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        public virtual IModelosQueryActivoBuilder Activo(bool isActivo = true) {
            if (isActivo) {
                this._Conditionals.Add(new Conditional("CTMDL_A", "1"));
            }
            return this;
        }

        /// <summary>
        /// filtrar todos los modelos por lista de indice de producto
        /// </summary>
        public virtual IModelosIdProductoQueryBuilder IdProducto(int[] ids) {
            if (ids.Length > 0) {
                this._Conditionals.Add(new Conditional("CTMDL_CTPRD_ID", string.Join(",", ids), ConditionalTypeEnum.In));
            }
            return this;
        }

        /// <summary>
        /// busqueda por descripcion
        /// </summary>
        public virtual IModelosQueryAuthorizedBuilder Search(string descripcion) {
            var valor = descripcion.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").Replace("ñ", "n");
            this._Conditionals.Add(new Conditional("@search", valor));
            return this;
        }

        /// <summary>
        /// solo producto autorizados
        /// </summary>
        /// <param name="onlyAuthorized">default: true</param>
        public virtual IConditionalBuilder OnlyAuthorized(bool onlyAuthorized = true) {
            if (onlyAuthorized)
                this._Conditionals.Add(new Conditional("CTMDL_ATRZD", "1"));
            return this;
        }

        public virtual IModelosQueryAuthorizedBuilder Visibilidad() {
            this._Conditionals.Add(new Conditional("CTMDL_VIS", "2"));
            return this;
        }
    }
}
