﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.Builder {
    /// <summary>
    /// clase builder para consutla de existencias
    /// </summary>
    public interface IExistenciasQueryBuilder : IConditionalBuilder, IConditionalBuild, IModelosQueryBuilder {
        /// <summary>
        /// indice del almacen 
        /// </summary>
        /// <param name="idAlmacen">indice del almacen</param>
        IExistenciasFromAlmacenQueryBuilder FromAlmacen(int idAlmacen);
    }

    public interface IExistenciasFromAlmacenQueryBuilder : IModelosQueryBuilder {

    }
}
