﻿using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.MP.Builder {
    public interface IValeAlmacenBuilder {
        IValeAlmacenBuild Almacen (AlmacenEnum almacen);
    }

    public interface IValeAlmacenIdBuilder {
        IValeAlmacenStatusBuilder Movimiento(TipoComprobanteEnum tipo);

    }

    public interface IValeAlmacenStatusBuilder : IValeAlmacenBuild{
        IValeAlmacenReceptorBuilder WithStatus(ValeAlmacenStatusEnum status);
    }

    public interface IValeAlmacenEfectoBuilder : IValeAlmacenBuild {
        IValeAlmacenReceptorBuilder WithIdDirectorio(int idReceptor);
    }

    public interface IValeAlmacenReceptorBuilder {
        IValeAlmacenBuild SubTipo(MovimientoSubTipoEnum subTipo);
    }

    public interface IValeAlmacenBuild {
        IValeAlmacenDetailModel Build();
    }
}
