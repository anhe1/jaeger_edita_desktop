﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Almacen.MP.Contracts;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;

namespace Jaeger.Aplication.Almacen.MP.Builder {
    public class ConfigurationBuilder : IConfigurationBuilder, IConfigurationBuild {
        protected internal IConfiguration _Configuration;

        public ConfigurationBuilder() {
            this._Configuration = new Configuration();
        }

        public List<IParametroModel> Build(IConfiguration configuration) {
            throw new NotImplementedException();
        }

        public IConfiguration Build(List<IParametroModel> parametros) {
            throw new NotImplementedException();
        }
    }
}
