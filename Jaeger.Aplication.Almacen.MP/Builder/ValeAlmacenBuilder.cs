﻿using System;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Almacen.MP.Builder {
    public class ValeAlmacenBuilder : IValeAlmacenBuilder, IValeAlmacenBuild, IValeAlmacenIdBuilder, IValeAlmacenReceptorBuilder, IValeAlmacenEfectoBuilder, IValeAlmacenStatusBuilder {
        protected internal IValeAlmacenDetailModel _ValeAlmacen;
        public ValeAlmacenBuilder() {
            this._ValeAlmacen =
                new ValeAlmacenDetailModel {
                    Status = ValeAlmacenStatusEnum.Pendiente,
                    Creo = ConfigService.Piloto.Clave,
                    FechaNuevo = DateTime.Now,
                    FechaEmision = DateTime.Now,
                };
        }

        public IValeAlmacenDetailModel Build() {
            return this._ValeAlmacen;
        }

        public static ValeAlmacenConceptoDetailModel Build(ProductoServicioModeloModel model) {
            return new ValeAlmacenConceptoDetailModel {
                Activo = true,
                Descripcion = model.ProductoDescripcion,
                Especificacion = model.Especificacion,
                Marca = model.Marca,
                IdUnidad = model.IdUnidad,
                NoIdentificacion = model.NoIdentificacion,
                IdProducto = model.IdProducto,
                IdModelo = model.IdModelo,
                IdTipoComprobante = model.IdTipo,
                Unidad = model.Unidad,
                FechaNuevo = DateTime.Now,
            };
        }

        public IValeAlmacenBuild SubTipo(MovimientoSubTipoEnum subTipo) {
            //this._ValeAlmacen.s = (int)subTipo;
            return this;
        }

        public IValeAlmacenStatusBuilder Movimiento(TipoComprobanteEnum tipo) {
            this._ValeAlmacen.TipoComprobante = tipo;
            return this;
        }

        public IValeAlmacenReceptorBuilder WithIdDirectorio(int idReceptor) {
            this._ValeAlmacen.IdDirectorio = idReceptor;
            return this;
        }

        public IValeAlmacenReceptorBuilder WithStatus(ValeAlmacenStatusEnum status) {
            this._ValeAlmacen.IdStatus = (int)status;
            return this;
        }

        public IValeAlmacenBuild Almacen(AlmacenEnum almacen) {
            this._ValeAlmacen.IdTipoAlmacen = (int)almacen;
            return this;
        }
    }
}
