﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.DataAccess.Almacen.MP.Contracts;
using Jaeger.Domain.Empresa.Contracts;

namespace Jaeger.Aplication.Almacen.MP.Builder {
    public interface IConfigurationBuilder {
        IConfiguration Build(List<IParametroModel> parametros);
    }

    public interface IConfigurationBuild {
        List<IParametroModel> Build(IConfiguration configuration);
    }
}
