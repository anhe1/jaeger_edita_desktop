﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.Almacen.MP.Services {
    public class CategoriaService : Almacen.Services.ClasificacionService, Contracts.ICategoriaService {
        public CategoriaService() : base() { }
    }
}
