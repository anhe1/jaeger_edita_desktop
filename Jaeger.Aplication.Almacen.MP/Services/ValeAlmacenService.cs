﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.MP.Builder;
using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.MP.Builder;
using Jaeger.Domain.Almacen.MP.Contracts;
using Jaeger.Domain.Almacen.MP.Entities;
using Jaeger.Domain.Almacen.MP.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Aplication.Almacen.MP.Services {
    /// <summary>
    /// movimientos de almacen
    /// </summary>
    public class ValeAlmacenService : Almacen.Services.AlmacenService, Almacen.Contracts.IAlmacenService, IValeAlmacenService {
        #region declaraciones
        protected ISqValeAlmacenRepository almacenRepository;
        protected ISqlValeRelacionRepository relacionRepository;
        protected ISqlValeStatusRepository statusRepository;
        #endregion

        public ValeAlmacenService() : base(Domain.Base.ValueObjects.AlmacenEnum.MP) {
            this.almacenRepository = new DataAccess.Almacen.MP.Repositories.SqlSugarValeAlmacenRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.relacionRepository= new DataAccess.Almacen.MP.Repositories.SqlSugarValeRelacionRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.statusRepository = new DataAccess.Almacen.MP.Repositories.SqlSugarValeStatusRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.almacenRepository.GetList<T1>(conditionals).ToList();
        }

        public IValeAlmacenDetailModel GetComprobante(int index) {
            return this.almacenRepository.GetById(index);
        }

        public ValeAlmacenPrinter GetPrinter(int index) {
            var d1 = this.GetComprobante(index);
            var d0 = new ValeAlmacenPrinter(d1 as ValeAlmacenDetailModel);
            d0.Departamento = ValeAlmacenService.GetDepartamento(d1.IdDepartamento).Descriptor;
            if (d0.TipoComprobante == TipoComprobanteEnum.Devolucion) {
                d0.ClaveDevolucion = GetTipoDevolucion(d0.IdCveDevolucion).Descriptor;
            }
            return d0;
        }

        public IValeAlmacenDetailModel Save(IValeAlmacenDetailModel model) {
            model = this.almacenRepository.Save(model);

            if (model.Autorizacion != null) {
                for (int i = 0; i < model.Autorizacion.Count; i++) {
                    model.Autorizacion[i].IdComprobante = model.IdComprobante;
                    this.statusRepository.Save(model.Autorizacion[i]);
                }
            }

            if (model.Relaciones != null) {
                for (int i = 0; i < model.Relaciones.Count; i++) {
                    model.Relaciones[i].IdComprobante = model.IdComprobante;
                    this.relacionRepository.Save(model.Relaciones[i]);
                }
            }

            return model;
        }

        public IValeAlmacenDetailModel UpdatePDF(IValeAlmacenDetailModel model) {
            return model;
        }

        #region metodos estaticos
        /// <summary>
        /// obtener listado de status
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return ((ValeAlmacenStatusEnum[])Enum.GetValues(typeof(ValeAlmacenStatusEnum))).Select(c => new StatusModel((int)c, c.ToString())).ToList();
        }

        /// <summary>
        /// listado de tipos de relacion con otros vales
        /// </summary>
        public static List<DocumentoTipoRelacion> GetTipoRelacion() {
            return EnumerationExtension.GetEnumToClass<DocumentoTipoRelacion, ValeAlmacenTipoRelacionEnum>().ToList();
        }

        /// <summary>
        /// lista de tipos de devolucion
        /// </summary>
        public static List<TipoDevolucion> GetTipoDevolucion() {
            return EnumerationExtension.GetEnumToClass<TipoDevolucion, TipoDevolucionEnum>().ToList();
        }

        public static TipoDevolucion GetTipoDevolucion(int idDevolucion) {
            var d0 = GetTipoDevolucion();
            try {
                var d1 = d0.Where(it => it.Id == idDevolucion).FirstOrDefault();
                if (d1 != null) {
                    return d1;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new TipoDevolucion(0, "Desconocido");
        }

        /// <summary>
        /// lista de tipos de operacion
        /// </summary>
        /// <returns></returns>
        public static List<DocumentoAlmacenDetailModel> TipoOperacion() {
            return new List<DocumentoAlmacenDetailModel> {
                new DocumentoAlmacenDetailModel { IdDocumento = 1, Descripcion = "Vale de Entrada",
                    Tipo = Domain.Base.ValueObjects.AlmacenEnum.MP, TipoComprobante = TipoComprobanteEnum.Entrada, IsReadOnly = true, TipoMovimiento = TipoMovimientoEnum.Ingreso, IsSum = true, FormatoImpreso = "ValeAlmacenV21Reporte.rdlc",
                    ReceptorTipos = new BindingList<ItemSelectedModel> { new ItemSelectedModel { Id = 1, Name = "Cliente,Proveedor", Selected = true } }},
                new DocumentoAlmacenDetailModel { IdDocumento = 2, Descripcion = "Vale de Salida",
                    Tipo = Domain.Base.ValueObjects.AlmacenEnum.MP, TipoComprobante = TipoComprobanteEnum.Salida, IsReadOnly = true, TipoMovimiento = TipoMovimientoEnum.Egreso, IsSum = true, FormatoImpreso = "ValeAlmacenV21Reporte.rdlc",
                    ReceptorTipos = new BindingList<ItemSelectedModel> { new ItemSelectedModel { Id = 1, Name = "Cliente,Proveedor", Selected = true } }},
                new DocumentoAlmacenDetailModel { IdDocumento = 3, Descripcion = "Traslado de Producto",
                    Tipo = Domain.Base.ValueObjects.AlmacenEnum.MP, TipoComprobante = TipoComprobanteEnum.Traslado, IsReadOnly = true, TipoMovimiento = TipoMovimientoEnum.Traslado, IsSum = false, FormatoImpreso = "ValeTrasladoV21Reporte.rdlc",
                    ReceptorTipos = new BindingList<ItemSelectedModel> { new ItemSelectedModel { Id = 1, Name = "Cliente,Proveedor", Selected = true } }},
                new DocumentoAlmacenDetailModel { IdDocumento = 4, Descripcion = "Devolución de Producto",
                    Tipo = Domain.Base.ValueObjects.AlmacenEnum.MP, TipoComprobante = TipoComprobanteEnum.Devolucion, IsReadOnly = true, TipoMovimiento = TipoMovimientoEnum.Egreso, IsSum = true, FormatoImpreso = "DevolucionV21Reporte.rdlc",
                    ReceptorTipos = new BindingList<ItemSelectedModel>{ new ItemSelectedModel { Id = 1, Name = "Cliente,Proveedor", Selected = true } }}
            };
        }

        public static DocumentoAlmacenDetailModel TipoOperacion(int IdDocumento) {
            var d0 = TipoOperacion();
            try {
                var d1 = d0.Where(it => it.IdDocumento == IdDocumento).FirstOrDefault();
                if (d1 != null) {
                    return d1;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return new DocumentoAlmacenDetailModel();
        }

        public static IValeAlmacenBuilder Create() {
            return new ValeAlmacenBuilder();
        }

        public static IValeAlmacenQueryBuilder Query() {
            return new ValeAlmacenQueryBuilder();
        }
        #endregion
    }
}
