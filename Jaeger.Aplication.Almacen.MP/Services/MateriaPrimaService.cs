﻿using Jaeger.Aplication.Almacen.MP.Contracts;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Aplication.Almacen.MP.Services {
    /// <summary>
    /// catalogo de productos del almacen de materia prima
    /// </summary>
    public class MateriaPrimaService : Almacen.Services.CatalogoProductoService, IMateriaPrimaService, ICatalogoProductoService {
        /// <summary>
        /// constructor
        /// </summary>
        public MateriaPrimaService() : base(AlmacenEnum.MP) { }
    }
}
