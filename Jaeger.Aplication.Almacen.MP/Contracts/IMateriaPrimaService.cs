﻿using Jaeger.Aplication.Almacen.Contracts;

namespace Jaeger.Aplication.Almacen.MP.Contracts {
    /// <summary>
    /// catalogo de productos y servicios de materia prima
    /// </summary>
    public interface IMateriaPrimaService : ICatalogoProductoService {
        
    }
}
