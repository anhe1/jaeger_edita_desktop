﻿namespace Jaeger.Aplication.Almacen.MP.Contracts {
    /// <summary>
    /// catalogo de categorias del almacen
    /// </summary>
    public interface ICategoriaService : Almacen.Contracts.IClasificacionService {

    }
}
