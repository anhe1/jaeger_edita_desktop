﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Banco.Builder {
    public class GridViewCuentaBancariaBuilder : IGridViewCuentaBancariaBuilder, IGridViewCuentaBancariaBuild, IGridViewCuentaBancariaTempleteBuilder, IDisposable {
        private readonly List<GridViewDataColumn> _Columns;

        public GridViewCuentaBancariaBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }

        public IGridViewCuentaBancariaTempleteBuilder Templetes() {
            return this;
        }

        /// <summary>
        /// Vista normal
        /// </summary>
        public IGridViewCuentaBancariaBuild CuentasBancarias() {
            this.AddActivo().AddFormatoCondicionalActivo().AddBeneficiario().AddNumCuenta().AddClaveBanco().AddSucursal().AddAlias().AddCLABE().AddMoneda().AddCreo().AddFechaNuevo();
            return this;
        }

        public IGridViewCuentaBancariaBuild Columns() {
            return this;
        }

        public GridViewDataColumn[] Build() {
            return this._Columns.ToArray();
        }

        public IGridViewCuentaBancariaBuild AddActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                VisibleInColumnChooser = false,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddBeneficiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Beneficiario",
                HeaderText = "Beneficiario",
                Name = "Beneficiario",
                Width = 275
            });

            return this;
        }

        public IGridViewCuentaBancariaBuild AddNumCuenta() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumCuenta",
                HeaderText = "Núm. de Cuenta",
                Name = "NumCuenta",
                Width = 115
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddClaveBanco() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveBanco",
                HeaderText = "Clv. Banco",
                Name = "ClaveBanco",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 60
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddSucursal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Sucursal",
                HeaderText = "Sucursal",
                Name = "Sucursal",
            });
            return this;
        }
        public IGridViewCuentaBancariaBuild AddAlias() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Alias",
                HeaderText = "Alias",
                Name = "Alias",
                Width = 175
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddCLABE() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CLABE",
                HeaderText = "CLABE",
                Name = "CLABE",
                Width = 175
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddMoneda() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Moneda",
                HeaderText = "Moneda",
                Name = "Moneda",
            });
            return this;
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public IGridViewCuentaBancariaBuild AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public IGridViewCuentaBancariaBuild AddFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewCuentaBancariaBuild AddFormatoCondicionalActivo() {
            var expressionFormattingObject1 = new ExpressionFormattingObject {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellForeColor = Color.Red,
                Expression = "Activo = 0",
                Name = "RegistroActivo",
                RowBackColor = Color.Empty,
                RowForeColor = Color.Empty,
                TextAlignment = ContentAlignment.MiddleRight
            };
            this._Columns[this._Columns.Count-1].ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            return this;
        }

        public void Dispose() {
            GC.Collect();
        }
    }
}
