﻿using System;
using System.Drawing;
using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    /// <summary>
    /// Builder para el grid de documentos auxiliares del banco
    /// </summary>
    public class AuxiliarGridBuilder : GridViewBuilder, IGridViewBuilder, IAuxiliarGridBuilder, IAuxiliarColumnsGridBuilder, IAuxiliarTempletesGridBuilder, IDisposable {
        public AuxiliarGridBuilder() : base() { }

        #region templetes
        public IAuxiliarTempletesGridBuilder Templetes() {
            return this;
        }

        public IGridViewBuilder Master() {
            this._Columns.Clear();
            this.Id().Descripcion().Tipo().Button();
            return this;
        }

        public IGridViewBuilder Importar() {
            this._Columns.Clear();
            this.Id().CheckId().IdMovimiento().NoIdentificador1().Descripcion().FileName().Tipo();
            var c0 = this._Columns[0].ConditionalFormattingObjectList;
            c0.Add(this.WithoutIndex());
            return this;
        }
        #endregion

        #region columns
        public IAuxiliarColumnsGridBuilder Columns() {
            return this;
        }

        public IAuxiliarColumnsGridBuilder Button() {
            this._Columns.Add(new GridViewCommandColumn {
                FieldName = "Button",
                HeaderText = "Documento",
                Name = "Button",
                Width = 85
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 280
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdAuxiliar",
                HeaderText = "IdAuxiliar",
                Name = "IdAuxiliar",
                IsVisible = false,
                VisibleInColumnChooser = true
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder CheckId() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "IdAuxiliar",
                HeaderText = "IdAuxiliarC",
                Name = "IdAuxiliarC",
                IsVisible = true,
                VisibleInColumnChooser = true
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder IdMovimiento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdMovimiento",
                HeaderText = "IdMovimiento",
                Name = "IdMovimiento",
                IsVisible = false,
                VisibleInColumnChooser = true
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder Tipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                ReadOnly = true,
                Width = 90,
                DataType = typeof(string)
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder NoIdentificador1() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                ReadOnly = false,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                MaxLength = 11,
                DataType = typeof(string)
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder NoIdentificador() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                DataType = typeof(string),
                MaxLength = 11
            });
            return this;
        }

        public IAuxiliarColumnsGridBuilder FileName() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FileName",
                HeaderText = "Archivo",
                Name = "FileName",
                Width = 480,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        protected ExpressionFormattingObject WithoutIndex() {
            return new ExpressionFormattingObject("sin indice", "IdMovimiento<=0", true) {
                RowBackColor = Color.FromArgb(255, 209, 140),
                CellBackColor = Color.FromArgb(255, 209, 140)
            };
        }
    }
}
