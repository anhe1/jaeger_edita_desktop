﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewAuxiliarBuilder : IGridViewBuilder, IGridViewAuxiliarColumnsBuilder, IDisposable {
        IGridViewAuxiliarTempletesBuilder Templetes();
        IGridViewAuxiliarColumnsBuilder Columns();
    }

    public interface IGridViewAuxiliarColumnsBuilder {
        IGridViewAuxiliarColumnsBuilder AddId();
        IGridViewAuxiliarColumnsBuilder AddDescripcion();
        IGridViewAuxiliarColumnsBuilder AddTipo();
        IGridViewAuxiliarColumnsBuilder AddButton();
    }

    public interface IGridViewAuxiliarTempletesBuilder {
        IGridViewBuilder Master();
    }
}
