﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Banco.Builder {
    public class GridViewConceptosBuilder : IGridViewConceptosBuilder, IGridViewConceptosTempletesBuild, IGridViewConceptosBuild {
        private List<GridViewDataColumn> _Columns;

        public GridViewConceptosBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }


        public IGridViewConceptosTempletesBuild Templetes() {
            return this;
        }

        public IGridViewConceptosBuild Columns() {
            return this;
        }

        public IGridViewConceptosBuild Conceptos() {
            this.AddActivo().AddConcepto().AddTipo().AddIVA().AddCreo().AddFechaNuevo();
            return this;
        }

        public GridViewDataColumn[] Build() {
            return this._Columns.ToArray();
        }

        public IGridViewConceptosBuild AddActivo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                IsVisible = false,
                Name = "Activo",
                VisibleInColumnChooser = false,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewConceptosBuild AddConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Concepto",
                HeaderText = "Concepto",
                Name = "Concepto",
                Width = 275
            });
            return this;
        }

        public IGridViewConceptosBuild AddTipo() {
            var combo = new GridViewComboBoxColumn {
                FieldName = "IdTipoOperacion",
                HeaderText = "Tipo",
                Name = "IdTipoOperacion",
                Width = 110
            };
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "Id";
            combo.DataSource = Aplication.Banco.BancoConceptoService.GetTipoOperacion();
            this._Columns.Add(combo);
            return this;
        }


        public IGridViewConceptosBuild AddIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "IVA",
                HeaderText = "IVA",
                Name = "IVA",
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public IGridViewConceptosBuild AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public IGridViewConceptosBuild AddFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public void Dispose() {
            GC.Collect();
        }
    }
}
