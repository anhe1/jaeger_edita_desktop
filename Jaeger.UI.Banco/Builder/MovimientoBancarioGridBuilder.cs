﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public class MovimientoBancarioGridBuilder : GridViewBuilder, IGridViewBuilder, IMovimientoBancarioGridBuilder, IMovimientoBancarioTempletesGridBuilder, IMovimientoBancarioColumnsGridBuilder {
        public MovimientoBancarioGridBuilder() : base() { }

        public IMovimientoBancarioTempletesGridBuilder Templetes() {
            return this;
        }

        public IMovimientoBancarioTempletesGridBuilder Master() {
            return this;
        }
    }
}
