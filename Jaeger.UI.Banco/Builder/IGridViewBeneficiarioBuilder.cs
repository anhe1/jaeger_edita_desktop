﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewBeneficiarioBuilder : IDisposable {
        IGridViewBeneficiarioTempletesBuilder Templetes();
        IGridViewBeneficiarioBuild Columns();
    }

    public interface IGridViewBeneficiarioTempletesBuilder {
        IGridViewBeneficiarioBuild GetColumns();
    }

    public interface IGridViewBeneficiarioBuild {
        IGridViewBeneficiarioBuild AddClave();

        IGridViewBeneficiarioBuild AddRFC();

        IGridViewBeneficiarioBuild AddNombre();

        IGridViewBeneficiarioBuild AddTelefono();

        IGridViewBeneficiarioBuild AddCorreo();

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        IGridViewBeneficiarioBuild AddCreo();

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        IGridViewBeneficiarioBuild AddFechaNuevo();

        GridViewDataColumn[] Build();
    }
}
