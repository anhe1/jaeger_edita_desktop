﻿using System;
using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    /// <summary>
    /// Builder para el grid de documentos auxiliares del banco
    /// </summary>
    public class GridViewAuxiliarBuilder : GridViewBuilder, IGridViewAuxiliarBuilder, IGridViewAuxiliarColumnsBuilder, IGridViewAuxiliarTempletesBuilder, IDisposable {
        public GridViewAuxiliarBuilder() : base() {

        }

        public IGridViewAuxiliarColumnsBuilder AddButton() {
            this._Columns.Add(new GridViewCommandColumn {
                FieldName = "Button",
                HeaderText = "Documento",
                Name = "Button",
                Width = 85
            });
            return this;
        }

        public IGridViewAuxiliarColumnsBuilder AddDescripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 480
            });
            return this;
        }

        public IGridViewAuxiliarColumnsBuilder AddId() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdAuxiliar",
                HeaderText = "IdAuxiliar",
                Name = "IdAuxiliar",
                IsVisible = false,
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IGridViewAuxiliarColumnsBuilder AddTipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                ReadOnly = true,
                Width = 90
            });
            return this;
        }

        public IGridViewAuxiliarColumnsBuilder Columns() {
            return this;
        }

        public IGridViewBuilder Master() {
            this.AddId().AddDescripcion().AddTipo().AddButton();
            return this;
        }

        public IGridViewAuxiliarTempletesBuilder Templetes() {
            return this;
        }
    }
}
