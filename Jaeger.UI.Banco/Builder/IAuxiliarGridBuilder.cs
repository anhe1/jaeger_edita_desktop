﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    /// <summary>
    /// 
    /// </summary>
    public interface IAuxiliarGridBuilder : IGridViewBuilder, IAuxiliarColumnsGridBuilder, IDisposable {
        /// <summary>
        /// 
        /// </summary>
        IAuxiliarTempletesGridBuilder Templetes();
        /// <summary>
        /// 
        /// </summary>
        IAuxiliarColumnsGridBuilder Columns();
    }

    public interface IAuxiliarTempletesGridBuilder {
        IGridViewBuilder Master();
        /// <summary>
        /// Templete para grid de importaciones
        /// </summary>
        IGridViewBuilder Importar();
    }

    public interface IAuxiliarColumnsGridBuilder {
        /// <summary>
        /// indice del auxiliar
        /// </summary>
        IAuxiliarColumnsGridBuilder Id();

        /// <summary>
        /// columna check del indice del auxiliar
        /// </summary>
        IAuxiliarColumnsGridBuilder CheckId();

        /// <summary>
        /// indice del movimiento
        /// </summary>
        IAuxiliarColumnsGridBuilder IdMovimiento();

        /// <summary>
        /// descripcion del documento
        /// </summary>
        IAuxiliarColumnsGridBuilder Descripcion();

        /// <summary>
        /// columna de archivo
        /// </summary>
        IAuxiliarColumnsGridBuilder Tipo();

        /// <summary>
        /// boton de descarga
        /// </summary>
        IAuxiliarColumnsGridBuilder Button();

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>     
        IAuxiliarColumnsGridBuilder NoIdentificador();

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza (solo lectura)
        /// </summary>     
        IAuxiliarColumnsGridBuilder NoIdentificador1();

        /// <summary>
        /// 
        /// </summary>
        IAuxiliarColumnsGridBuilder FileName();
    }
}
