﻿using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public interface IContextMenuBancoBuilder : IContextMenuBuilder {
        IContextMenuBancoTempleteBuilder Templetes();
        new IContextMenuBancoItemsBuilder Items();
    }

    public interface IContextMenuBancoItemsBuilder : IContextMenuItemsBuilder {
        IContextMenuBancoItemsBuilder RegristroCobro();
        IContextMenuBancoItemsBuilder RegristroPago();
        IContextMenuBancoItemsBuilder PorCobrar();
    }

    public interface IContextMenuBancoTempleteBuilder : IContextMenuTempleteBuilder {
        
    }
}
