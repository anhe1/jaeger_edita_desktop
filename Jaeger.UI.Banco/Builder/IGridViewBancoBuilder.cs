﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewBancoBuilder : IGridViewBuilder, IGridViewBancoColumnsBuilder, IDisposable {
        IGridViewBancoTempletesBuilder Templetes();
        IGridViewBancoColumnsBuilder Columns();
    }

    public interface IGridViewBancoColumnsBuilder : IGridViewBuilder {
        #region movimientos bancarios
        IGridViewBancoColumnsBuilder AddIdentificador();

        IGridViewBancoColumnsBuilder AddStatus();

        IGridViewBancoColumnsBuilder AddBeneficiarioT();

        IGridViewBancoColumnsBuilder AddConcepto();

        IGridViewBancoColumnsBuilder AddClaveFormaPago();

        IGridViewBancoColumnsBuilder AddFechaDocto();

        IGridViewBancoColumnsBuilder AddNumDocto();

        IGridViewBancoColumnsBuilder AddClaveBancoT();

        IGridViewBancoColumnsBuilder AddSucursalT();

        IGridViewBancoColumnsBuilder AddCuentaCLABET();

        IGridViewBancoColumnsBuilder AddReferencia();

        IGridViewBancoColumnsBuilder AddNumAutorizacion();

        IGridViewBancoColumnsBuilder AddAbono();

        IGridViewBancoColumnsBuilder AddCargo();

        IGridViewBancoColumnsBuilder AddSaldo();

        IGridViewBancoColumnsBuilder AddFechaAplicacion();

        IGridViewBancoColumnsBuilder AddFechaEmision();

        IGridViewBancoColumnsBuilder AddFechaVence();

        IGridViewBancoColumnsBuilder AddCancela();

        IGridViewBancoColumnsBuilder AddPorComprobar();

        new IGridViewBancoColumnsBuilder AddCreo();

        IGridViewBancoColumnsBuilder AddNota();

        IGridViewBancoColumnsBuilder AddAfectaSaldoCuenta();
        #endregion

        #region comprobantes
        IGridViewBancoColumnsBuilder AddTipoComprobanteText();

        IGridViewBancoColumnsBuilder AddTipoDocumentoText();

        IGridViewBancoColumnsBuilder AddFolio();

        IGridViewBancoColumnsBuilder AddSerie();

        IGridViewBancoColumnsBuilder AddEmisorRFC();

        IGridViewBancoColumnsBuilder AddEmisorNombre();

        IGridViewBancoColumnsBuilder AddReceptorRFC();

        IGridViewBancoColumnsBuilder AddReceptorNombre();

        IGridViewBancoColumnsBuilder AddIdDocumento();

        IGridViewBancoColumnsBuilder AddTotal();

        IGridViewBancoColumnsBuilder AddAcumulado();
        #endregion
    }

    public interface IGridViewBancoTempletesBuilder {
        IGridViewBuilder Cobranza();
        IGridViewBuilder Pagos();
        IGridViewBuilder Movimientos();
        IGridViewBuilder Comprobantes();
    }
}
