﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Banco.Builder {
    public class GridViewBeneficiarioBuilder : IGridViewBeneficiarioBuilder, IGridViewBeneficiarioTempletesBuilder, IGridViewBeneficiarioBuild {
        private List<GridViewDataColumn> _Columns;

        public GridViewBeneficiarioBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }

        public GridViewDataColumn[] Build() {
            return this._Columns.ToArray();
        }

        public IGridViewBeneficiarioTempletesBuilder Templetes() {
            return this;
        }

        public IGridViewBeneficiarioBuild Columns() {
            return this;
        }

        public IGridViewBeneficiarioBuild GetColumns() {
            this.AddClave().AddRFC().AddNombre().AddTelefono().AddCorreo().AddCreo().AddFechaNuevo();
            return this;
        }

        public IGridViewBeneficiarioBuild AddClave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewBeneficiarioBuild AddRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                Width = 80,
                ReadOnly = true,
            });
            return this;
        }

        public IGridViewBeneficiarioBuild AddNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "Nombre",
                Width = 280,
                ReadOnly = true,
            });
            return this;
        }

        public IGridViewBeneficiarioBuild AddTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 110,
                ReadOnly = true,
            });
            return this;
        }

        public IGridViewBeneficiarioBuild AddCorreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo",
                Name = "Correo",
                Width = 185,
                ReadOnly = true,
            });
            return this;
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public IGridViewBeneficiarioBuild AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public IGridViewBeneficiarioBuild AddFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public void Dispose() {
            GC.Collect();
        }
    }
}
