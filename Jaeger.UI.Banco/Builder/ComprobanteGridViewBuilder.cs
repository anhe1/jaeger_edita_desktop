﻿using Jaeger.UI.Common.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public class ComprobanteGridViewBuilder : GridViewBuilder, IGridViewBuilder, IComprobanteGridViewBuilder, IComprobanteTempleteGridViewBuilder, IComprobanteColumnsGridViewBuilder {
        public ComprobanteGridViewBuilder() : base() { }

        public IComprobanteTempleteGridViewBuilder Templetes() {
            return this;
        }

        public IComprobanteTempleteGridViewBuilder Emitidos() {
            this._Columns.Clear();
            this.Id().TipoComprobante().Folio().Serie().Status().ReceptorRFC().ReceptorNombre().FechaEmision().Total().Acumulado();
            return this;
        }

        public IComprobanteTempleteGridViewBuilder Recibidos() {
            this._Columns.Clear();
            this.Id().TipoComprobante().Folio().Serie().Status().EmisorRFC().EmisorNombre().FechaEmision().Total().Acumulado();
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder TipoComprobante() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobante",
                HeaderText = "Tipo",
                Name = "TipoComprobante"
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                Width = 75
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                Width = 65
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Status() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Status",
                HeaderText = "Status",
                Name = "Status",
                Width = 85
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder EmisorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "Emisor RFC",
                Name = "EmisorRFC",
                Width = 90
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder EmisorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorNombre",
                HeaderText = "Emisor",
                Name = "EmisorNombre",
                Width = 200
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder ReceptorRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "Receptor RFC",
                Name = "ReceptorRFC",
                Width = 90
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder ReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Receptor",
                Name = "Receptor",
                Width = 200
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                Name = "IdDocumento",
                Width = 190
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder FechaEmision() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaEmision",
                FormatString = this.FormatStringDate,
                HeaderText = "Fecha Emisión",
                Name = "FechaEmision",
                Width = 85,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Total() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:n}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IComprobanteColumnsGridViewBuilder Acumulado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Acumulado",
                FormatString = "{0:n}",
                HeaderText = "Acumulado",
                Name = "Acumulado",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }
    }
}
