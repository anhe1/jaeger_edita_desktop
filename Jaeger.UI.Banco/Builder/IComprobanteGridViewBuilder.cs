﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public interface IComprobanteGridViewBuilder : IGridViewBuilder {
        IComprobanteTempleteGridViewBuilder Templetes();
    }

    public interface IComprobanteTempleteGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IComprobanteTempleteGridViewBuilder Emitidos();
        IComprobanteTempleteGridViewBuilder Recibidos();
    }

    public interface IComprobanteColumnsGridViewBuilder : IGridViewColumnsBuild {
        IComprobanteColumnsGridViewBuilder Id();
        IComprobanteColumnsGridViewBuilder TipoComprobante();
        IComprobanteColumnsGridViewBuilder Folio();
        IComprobanteColumnsGridViewBuilder Serie();
        IComprobanteColumnsGridViewBuilder Status();
        IComprobanteColumnsGridViewBuilder EmisorRFC();
        IComprobanteColumnsGridViewBuilder EmisorNombre();
        IComprobanteColumnsGridViewBuilder ReceptorRFC();
        IComprobanteColumnsGridViewBuilder ReceptorNombre();
        IComprobanteColumnsGridViewBuilder IdDocumento();
        IComprobanteColumnsGridViewBuilder FechaEmision();
        IComprobanteColumnsGridViewBuilder Total();
        IComprobanteColumnsGridViewBuilder Acumulado();
    }
}
