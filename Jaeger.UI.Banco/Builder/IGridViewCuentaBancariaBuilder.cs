﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewCuentaBancariaBuilder : IDisposable {
        IGridViewCuentaBancariaTempleteBuilder Templetes();
        IGridViewCuentaBancariaBuild Columns();
    }

    public interface IGridViewCuentaBancariaTempleteBuilder {
        IGridViewCuentaBancariaBuild CuentasBancarias();
    }

    public interface IGridViewCuentaBancariaBuild {
        IGridViewCuentaBancariaBuild CuentasBancarias();

        IGridViewCuentaBancariaBuild AddActivo();

        IGridViewCuentaBancariaBuild AddBeneficiario();

        IGridViewCuentaBancariaBuild AddNumCuenta();

        IGridViewCuentaBancariaBuild AddClaveBanco();

        IGridViewCuentaBancariaBuild AddSucursal();

        IGridViewCuentaBancariaBuild AddAlias();

        IGridViewCuentaBancariaBuild AddCLABE();

        IGridViewCuentaBancariaBuild AddMoneda();

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        IGridViewCuentaBancariaBuild AddCreo();

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        IGridViewCuentaBancariaBuild AddFechaNuevo();

        IGridViewCuentaBancariaBuild AddFormatoCondicionalActivo();

        GridViewDataColumn[] Build();
    }
}
