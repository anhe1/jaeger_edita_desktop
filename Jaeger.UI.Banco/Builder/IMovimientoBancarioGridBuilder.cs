﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public interface IMovimientoBancarioGridBuilder : IGridViewBuilder {
        IMovimientoBancarioTempletesGridBuilder Templetes();
    }

    public interface IMovimientoBancarioTempletesGridBuilder : IGridViewBuilder {
        IMovimientoBancarioTempletesGridBuilder Master();
    }

    public interface IMovimientoBancarioColumnsGridBuilder : IGridViewBuilder {

    }
}
