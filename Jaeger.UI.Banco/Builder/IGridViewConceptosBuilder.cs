﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewConceptosBuilder : IDisposable {
        IGridViewConceptosTempletesBuild Templetes();
        IGridViewConceptosBuild Columns();
    }

    public interface IGridViewConceptosTempletesBuild {
        IGridViewConceptosBuild Conceptos();
    }

    public interface IGridViewConceptosBuild {
        IGridViewConceptosBuild AddActivo();
        IGridViewConceptosBuild AddConcepto();
        IGridViewConceptosBuild AddIVA();
        IGridViewConceptosBuild AddFechaNuevo();
        IGridViewConceptosBuild AddTipo();
        IGridViewConceptosBuild AddCreo();
        GridViewDataColumn[] Build();
    }
}
