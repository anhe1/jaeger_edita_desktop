﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Banco.Builder {
    public class GridViewFormaPagoBuilder : IGridViewFormaPagoBuilder, IGridViewFormaPagoTempletes, IGridViewFormaPagoBuild {
        private readonly List<GridViewDataColumn> _Columns;

        public GridViewFormaPagoBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }

        public GridViewDataColumn[] Build() {
            return this._Columns.ToArray();
        }

        public IGridViewFormaPagoTempletes Templetes() {
            return this;
        }

        public IGridViewFormaPagoBuild FormaPago() {
            this.AddDescripcion().AddClave().AddClaveSAT().AddAfectaSaldo(). AddCreo().AddFechaNuevo();
            return this;
        }

        public IGridViewFormaPagoBuild Columns() {
            return this;
        }

        public IGridViewFormaPagoBuild AddDescripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 240,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewFormaPagoBuild AddClave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewFormaPagoBuild AddClaveSAT() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveSAT",
                HeaderText = "Clave SAT",
                Name = "ClaveSAT",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 65,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewFormaPagoBuild AddAfectaSaldo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "AfectarSaldo",
                HeaderText = "Afectar Saldo",
                Name = "AfectarSaldo",
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        public IGridViewFormaPagoBuild AddCreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        public IGridViewFormaPagoBuild AddFechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public void Dispose() {
            GC.Collect();
        }
    }
}
