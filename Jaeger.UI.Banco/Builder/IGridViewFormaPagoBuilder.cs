﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Builder {
    public interface IGridViewFormaPagoBuilder : IDisposable {
        IGridViewFormaPagoTempletes Templetes();
        IGridViewFormaPagoBuild Columns();
    }

    public interface IGridViewFormaPagoTempletes {
        IGridViewFormaPagoBuild FormaPago();
    }

    public interface IGridViewFormaPagoBuild {
        IGridViewFormaPagoBuild AddDescripcion();

        IGridViewFormaPagoBuild AddClave();

        IGridViewFormaPagoBuild AddClaveSAT();

        IGridViewFormaPagoBuild AddAfectaSaldo();

        /// <summary>
        /// obtener columna del usuario creador
        /// </summary>
        IGridViewFormaPagoBuild AddCreo();

        /// <summary>
        /// obtener fecha de nuevo registro
        /// </summary>
        IGridViewFormaPagoBuild AddFechaNuevo();

        GridViewDataColumn[] Build();
    }
}
