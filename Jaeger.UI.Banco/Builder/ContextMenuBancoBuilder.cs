﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public class ContextMenuBancoBuilder : ContextMenuBuilder, IContextMenuBuilder, IContextMenuBancoBuilder, IContextMenuBancoItemsBuilder, IContextMenuBancoTempleteBuilder { 
        public ContextMenuBancoBuilder() : base() {
            
        }

        public IContextMenuBancoItemsBuilder PorCobrar() {
            this.Agregar(new RadMenuItem { Text = "Por Cobrar", Name = "tadm_gcli_remxcobrar" });
            return this;
        }

        public IContextMenuBancoItemsBuilder RegristroCobro() {
            this.Agregar(new RadMenuItem { Text = "Registrar Cobro", Name = "ReciboCobro" });
            return this;
        }

        public IContextMenuBancoItemsBuilder RegristroPago() {
            this.Agregar(new RadMenuItem { Text = "Registrar Pago", Name = "ReciboPago" });
            return this;
        }

        public IContextMenuBancoTempleteBuilder Templetes() {
            throw new NotImplementedException();
        }

        IContextMenuBancoItemsBuilder IContextMenuBancoBuilder.Items() {
            throw new NotImplementedException();
        }
    }

    public class Prueba {
        public Prueba() {
            IContextMenuBancoBuilder d0 = new ContextMenuBancoBuilder();
            d0.Items().RegristroCobro().AddEditar();
        }
    }
}
