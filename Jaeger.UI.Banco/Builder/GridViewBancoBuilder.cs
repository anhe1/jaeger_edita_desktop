﻿using System.Collections.Generic;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Banco.Builder {
    public class GridViewBancoBuilder : GridViewBuilder, IGridViewBuilder, IGridViewBancoBuilder, IGridViewBancoColumnsBuilder, IGridViewBancoTempletesBuilder {

        public GridViewBancoBuilder() {
            this._Columns = new List<GridViewDataColumn>();
        }

        #region movimientos bancarios
        public IGridViewBancoColumnsBuilder AddIdentificador() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddStatus() {
            _Columns.Add(new GridViewComboBoxColumn {
                FieldName = "Status",
                HeaderText = "Estado",
                Name = "Status",
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddBeneficiarioT() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "BeneficiarioT",
                HeaderText = "Persona",
                Name = "BeneficiarioT",
                ReadOnly = true,
                Width = 275
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddConcepto() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Concepto",
                HeaderText = "Concepto",
                Name = "Concepto",
                ReadOnly = true,
                Width = 240
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddClaveFormaPago() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveFormaPago",
                HeaderText = "Clv. Pago",
                Name = "ClaveFormaPago",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddFechaDocto() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaDocto",
                FormatString = "{0:ddd dd MMM yyyy}",
                HeaderText = "Fec. Doc./Pago",
                Name = "FechaDocto",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 95
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddNumDocto() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumDocto",
                HeaderText = "No. Docto.",
                Name = "NumDocto",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddClaveBancoT() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveBancoT",
                HeaderText = "Clv. Banco",
                Name = "ClaveBancoT",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddSucursalT() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SucursalT",
                HeaderText = "Sucursal",
                IsVisible = false,
                Name = "SucursalT",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddCuentaCLABET() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CuentaCLABET",
                HeaderText = "CLABE",
                IsVisible = false,
                Name = "CuentaCLABET",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 120
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddReferencia() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 105
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddNumAutorizacion() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumAutorizacion",
                HeaderText = "Autorización",
                Name = "NumAutorizacion",
                Width = 105
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddAbono() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Abono",
                FormatString = "{0:n2}",
                HeaderText = "Abono",
                Name = "Abono",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 95
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddCargo() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cargo",
                FormatString = "{0:n2}",
                HeaderText = "Cargo",
                Name = "Cargo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 95
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddSaldo() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Saldo",
                FormatString = "{0:n2}",
                HeaderText = "Saldo",
                Name = "Saldo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddFechaAplicacion() {
            _Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaAplicacion",
                FormatString = "{0:dd MMM yyyy}",
                HeaderText = "Fec. Aplicación",
                Name = "FechaAplicacion",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddFechaEmision() {
            _Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEmision",
                FormatString = "{0:dd MMM yyyy}",
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddFechaVence() {
            _Columns.Add(new GridViewDateTimeColumn {
                FormatString = "{0:dd MMM yyyy}",
                HeaderText = "Fec. Vence",
                IsVisible = false,
                Name = "column11",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddCancela() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Canceló",
                Name = "Cancela",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddPorComprobar() {
            _Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "PorComprobar",
                HeaderText = "Por Comprobar",
                IsVisible = false,
                Name = "PorComprobar",
                ReadOnly = true
            });
            return this;
        }

        public new IGridViewBancoColumnsBuilder AddCreo() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                ReadOnly = true,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddNota() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                IsVisible = false,
                Name = "Nota",
                Width = 200
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddAfectaSaldoCuenta() {
            _Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "AfectaSaldoCuenta",
                HeaderText = "Afecta Saldo",
                IsVisible = false,
                Name = "AfectaSaldoCuenta",
                ReadOnly = true,
                VisibleInColumnChooser = false
            });
            return this;
        }
        #endregion

        #region comprobantes
        public IGridViewBancoColumnsBuilder AddTipoComprobanteText() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoComprobanteText",
                HeaderText = "Tipo",
                Name = "TipoComprobanteText"
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddTipoDocumentoText() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "TipoDocumentoText",
                HeaderText = "Documento",
                Name = "TipoDocumentoText"
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddFolio() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio"
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddSerie() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie"
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddEmisorRFC() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorRFC",
                HeaderText = "RFC (Emisor)",
                Name = "EmisorRFC",
                Width = 90
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddEmisorNombre() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "EmisorNombre",
                HeaderText = "Emisor",
                Name = "EmisorNombre",
                Width = 200
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddReceptorRFC() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorRFC",
                HeaderText = "RFC (Receptor)",
                Name = "ReceptorRFC",
                Width = 90
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddReceptorNombre() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Receptor",
                Name = "ReceptorNombre",
                Width = 200
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddIdDocumento() {
            _Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento (UUID)",
                Name = "IdDocumento",
                Width = 155
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddTotal() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:N2}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewBancoColumnsBuilder AddAcumulado() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Acumulado",
                FormatString = "{0:N2}",
                HeaderText = "Acumulado",
                Name = "Acumulado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public IGridViewBuilder Movimientos() {
            AddIdentificador().AddStatus().AddBeneficiarioT().AddConcepto().AddClaveFormaPago().AddFechaDocto().AddNumDocto().AddClaveBancoT().AddSucursalT().AddCuentaCLABET().AddReferencia()
            .AddNumAutorizacion().AddAbono().AddCargo().AddSaldo().AddFechaAplicacion().AddFechaEmision().AddFechaVence().AddCancela().AddPorComprobar().AddCreo().AddNota().AddAfectaSaldoCuenta();
            return this;
        }

        public IGridViewBuilder Comprobantes() {
            AddIdentificador().AddTipoComprobanteText().AddTipoDocumentoText().AddFechaEmision().AddFolio().AddSerie().AddFechaEmision().AddEmisorRFC().AddEmisorNombre().AddReceptorRFC().AddReceptorNombre()
                .AddIdDocumento().AddTotal().AddAcumulado().AddAbono().AddCargo();
            return this;
        }
        #endregion

        public IGridViewBuilder Cobranza() {
            AddIdentificador().AddStatus().AddFechaEmision().AddBeneficiarioT().AddConcepto().AddNumAutorizacion().AddIdDocumento().AddFechaAplicacion().AddCargo().AddAbono().AddCreo();
            return this;
        }

        public IGridViewBuilder Pagos() {
            AddIdentificador().AddStatus().AddFechaEmision().AddBeneficiarioT().AddConcepto().AddNumAutorizacion().AddFechaAplicacion().AddIdDocumento().AddCargo().AddAbono().AddCreo();
            return this;
        }

        public IGridViewBancoTempletesBuilder Templetes() {
            return this;
        }

        public IGridViewBancoColumnsBuilder AddColumns() {
            return this;
        }

        public IGridViewBancoColumnsBuilder Columns() {
            return this;
        }

        #region metodos estaticos
        /// <summary>
        /// convertir a fila de grid
        /// </summary>
        public static GridViewRowInfo ConverTo(GridViewRowInfo newRow, IMovimientoComprobanteMergeModelView item) {
            newRow.Cells["Identificador"].Value = item.Identificador;
            newRow.Cells["Status"].Value = item.Status;
            newRow.Cells["FechaEmision"].Value = item.FechaEmision;
            newRow.Cells["BeneficiarioT"].Value = item.BeneficiarioT;
            newRow.Cells["Concepto"].Value = item.Concepto;
            newRow.Cells["IdDocumento"].Value = item.IdDocumento;
            newRow.Cells["NumAutorizacion"].Value = item.NumAutorizacion;
            newRow.Cells["FechaAplicacion"].Value = item.FechaAplicacion;
            newRow.Cells["Cargo"].Value = item.Cargo1;
            newRow.Cells["Abono"].Value = item.Abono1;
            newRow.Cells["Creo"].Value = item.Creo;
            return newRow;
        }
        #endregion
    }
}
