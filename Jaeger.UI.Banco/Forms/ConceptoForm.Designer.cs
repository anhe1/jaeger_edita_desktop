﻿namespace Jaeger.UI.Banco.Forms
{
    partial class ConceptoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConceptoForm));
            this.lblDescripcion = new Telerik.WinControls.UI.RadLabel();
            this.lblIVA = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.grpDescripcion = new Telerik.WinControls.UI.RadGroupBox();
            this.btnBeneficiario = new Telerik.WinControls.UI.RadButton();
            this.Beneficiario = new Telerik.WinControls.UI.RadDropDownList();
            this.chkUnBeneficiario = new Telerik.WinControls.UI.RadCheckBox();
            this.lblFormatoImpreso = new Telerik.WinControls.UI.RadLabel();
            this.lblBeneficiarios = new Telerik.WinControls.UI.RadLabel();
            this.FormatoImpresion = new Telerik.WinControls.UI.RadDropDownList();
            this.lblTipoOperacion = new Telerik.WinControls.UI.RadLabel();
            this.lblEfecto = new Telerik.WinControls.UI.RadLabel();
            this.Beneficiarios = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.TipoOperacion = new Telerik.WinControls.UI.RadDropDownList();
            this.Efecto = new Telerik.WinControls.UI.RadDropDownList();
            this.lblConcepto = new Telerik.WinControls.UI.RadLabel();
            this.Concepto = new Telerik.WinControls.UI.RadTextBox();
            this.Activo = new Telerik.WinControls.UI.RadCheckBox();
            this.maskIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.grpComprobante = new Telerik.WinControls.UI.RadGroupBox();
            this.RequierirComprobante = new Telerik.WinControls.UI.RadCheckBox();
            this.lblAplicarComo = new Telerik.WinControls.UI.RadLabel();
            this.AplicaComo = new Telerik.WinControls.UI.RadDropDownList();
            this.AfectarSaldo = new Telerik.WinControls.UI.RadCheckBox();
            this.lblTipoComprobante = new Telerik.WinControls.UI.RadLabel();
            this.Relaciones = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.StatusComprobante = new Telerik.WinControls.UI.RadDropDownList();
            this.TipoComprobante = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.lblSubTipo = new Telerik.WinControls.UI.RadLabel();
            this.SubTipoComprobante = new Telerik.WinControls.UI.RadDropDownList();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.lblRelacionadoA = new Telerik.WinControls.UI.RadLabel();
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.errorConcepto = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lblDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDescripcion)).BeginInit();
            this.grpDescripcion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormatoImpreso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormatoImpresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEfecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpComprobante)).BeginInit();
            this.grpComprobante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequierirComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAplicarComo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AplicaComo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AfectarSaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTipoComprobante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelacionadoA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Location = new System.Drawing.Point(12, 18);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(67, 18);
            this.lblDescripcion.TabIndex = 1;
            this.lblDescripcion.Text = "Descripción:";
            // 
            // lblIVA
            // 
            this.lblIVA.Location = new System.Drawing.Point(489, 11);
            this.lblIVA.Name = "lblIVA";
            this.lblIVA.Size = new System.Drawing.Size(98, 18);
            this.lblIVA.TabIndex = 2;
            this.lblIVA.Text = "Porcentaje de IVA:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(87, 17);
            this.Descripcion.MaxLength = 30;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.NullText = "Descripción";
            this.Descripcion.Size = new System.Drawing.Size(295, 20);
            this.Descripcion.TabIndex = 3;
            // 
            // grpDescripcion
            // 
            this.grpDescripcion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDescripcion.Controls.Add(this.btnBeneficiario);
            this.grpDescripcion.Controls.Add(this.Beneficiario);
            this.grpDescripcion.Controls.Add(this.chkUnBeneficiario);
            this.grpDescripcion.Controls.Add(this.lblFormatoImpreso);
            this.grpDescripcion.Controls.Add(this.lblBeneficiarios);
            this.grpDescripcion.Controls.Add(this.FormatoImpresion);
            this.grpDescripcion.Controls.Add(this.lblTipoOperacion);
            this.grpDescripcion.Controls.Add(this.lblEfecto);
            this.grpDescripcion.Controls.Add(this.Beneficiarios);
            this.grpDescripcion.Controls.Add(this.TipoOperacion);
            this.grpDescripcion.Controls.Add(this.Efecto);
            this.grpDescripcion.Controls.Add(this.lblConcepto);
            this.grpDescripcion.Controls.Add(this.Concepto);
            this.grpDescripcion.Controls.Add(this.Activo);
            this.grpDescripcion.Controls.Add(this.Descripcion);
            this.grpDescripcion.Controls.Add(this.lblDescripcion);
            this.grpDescripcion.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpDescripcion.HeaderText = "";
            this.grpDescripcion.Location = new System.Drawing.Point(0, 30);
            this.grpDescripcion.Name = "grpDescripcion";
            this.grpDescripcion.Size = new System.Drawing.Size(455, 175);
            this.grpDescripcion.TabIndex = 4;
            // 
            // btnBeneficiario
            // 
            this.btnBeneficiario.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.btnBeneficiario.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnBeneficiario.Location = new System.Drawing.Point(416, 120);
            this.btnBeneficiario.Name = "btnBeneficiario";
            this.btnBeneficiario.Size = new System.Drawing.Size(20, 20);
            this.btnBeneficiario.TabIndex = 373;
            // 
            // Beneficiario
            // 
            this.Beneficiario.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Beneficiario.Location = new System.Drawing.Point(140, 120);
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.NullText = "Efecto";
            this.Beneficiario.Size = new System.Drawing.Size(270, 20);
            this.Beneficiario.TabIndex = 17;
            // 
            // chkUnBeneficiario
            // 
            this.chkUnBeneficiario.Location = new System.Drawing.Point(12, 120);
            this.chkUnBeneficiario.Name = "chkUnBeneficiario";
            this.chkUnBeneficiario.Size = new System.Drawing.Size(122, 18);
            this.chkUnBeneficiario.TabIndex = 16;
            this.chkUnBeneficiario.Text = "Un solo beneficiario:";
            // 
            // lblFormatoImpreso
            // 
            this.lblFormatoImpreso.Location = new System.Drawing.Point(12, 144);
            this.lblFormatoImpreso.Name = "lblFormatoImpreso";
            this.lblFormatoImpreso.Size = new System.Drawing.Size(119, 18);
            this.lblFormatoImpreso.TabIndex = 14;
            this.lblFormatoImpreso.Text = "Formato de Impresión:";
            // 
            // lblBeneficiarios
            // 
            this.lblBeneficiarios.Location = new System.Drawing.Point(12, 95);
            this.lblBeneficiarios.Name = "lblBeneficiarios";
            this.lblBeneficiarios.Size = new System.Drawing.Size(72, 18);
            this.lblBeneficiarios.TabIndex = 13;
            this.lblBeneficiarios.Text = "Beneficiarios:";
            // 
            // FormatoImpresion
            // 
            this.FormatoImpresion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.FormatoImpresion.Location = new System.Drawing.Point(140, 144);
            this.FormatoImpresion.Name = "FormatoImpresion";
            this.FormatoImpresion.NullText = "Efecto";
            this.FormatoImpresion.Size = new System.Drawing.Size(296, 20);
            this.FormatoImpresion.TabIndex = 15;
            // 
            // lblTipoOperacion
            // 
            this.lblTipoOperacion.Location = new System.Drawing.Point(185, 69);
            this.lblTipoOperacion.Name = "lblTipoOperacion";
            this.lblTipoOperacion.Size = new System.Drawing.Size(97, 18);
            this.lblTipoOperacion.TabIndex = 14;
            this.lblTipoOperacion.Text = "Tipo de operación";
            // 
            // lblEfecto
            // 
            this.lblEfecto.Location = new System.Drawing.Point(12, 69);
            this.lblEfecto.Name = "lblEfecto";
            this.lblEfecto.Size = new System.Drawing.Size(40, 18);
            this.lblEfecto.TabIndex = 15;
            this.lblEfecto.Text = "Efecto:";
            // 
            // Beneficiarios
            // 
            this.Beneficiarios.Location = new System.Drawing.Point(91, 94);
            this.Beneficiarios.Name = "Beneficiarios";
            this.Beneficiarios.NullText = "Objetos del directorio";
            this.Beneficiarios.ShowCheckAllItems = true;
            this.Beneficiarios.Size = new System.Drawing.Size(345, 20);
            this.Beneficiarios.TabIndex = 12;
            // 
            // TipoOperacion
            // 
            this.TipoOperacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoOperacion.Location = new System.Drawing.Point(288, 68);
            this.TipoOperacion.Name = "TipoOperacion";
            this.TipoOperacion.NullText = "Tipo de operación";
            this.TipoOperacion.Size = new System.Drawing.Size(148, 20);
            this.TipoOperacion.TabIndex = 10;
            // 
            // Efecto
            // 
            this.Efecto.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Efecto.Location = new System.Drawing.Point(56, 68);
            this.Efecto.Name = "Efecto";
            this.Efecto.NullText = "Efecto";
            this.Efecto.Size = new System.Drawing.Size(118, 20);
            this.Efecto.TabIndex = 11;
            // 
            // lblConcepto
            // 
            this.lblConcepto.Location = new System.Drawing.Point(12, 43);
            this.lblConcepto.Name = "lblConcepto";
            this.lblConcepto.Size = new System.Drawing.Size(57, 18);
            this.lblConcepto.TabIndex = 8;
            this.lblConcepto.Text = "Concepto:";
            // 
            // Concepto
            // 
            this.Concepto.Location = new System.Drawing.Point(77, 42);
            this.Concepto.MaxLength = 40;
            this.Concepto.Name = "Concepto";
            this.Concepto.NullText = "Concepto";
            this.Concepto.Size = new System.Drawing.Size(362, 20);
            this.Concepto.TabIndex = 7;
            // 
            // Activo
            // 
            this.Activo.Location = new System.Drawing.Point(388, 18);
            this.Activo.Name = "Activo";
            this.Activo.Size = new System.Drawing.Size(51, 18);
            this.Activo.TabIndex = 5;
            this.Activo.Text = "Activo";
            // 
            // maskIVA
            // 
            this.maskIVA.Location = new System.Drawing.Point(781, 481);
            this.maskIVA.Mask = "0.00";
            this.maskIVA.MaskType = Telerik.WinControls.UI.MaskType.Standard;
            this.maskIVA.Name = "maskIVA";
            this.maskIVA.Size = new System.Drawing.Size(69, 20);
            this.maskIVA.TabIndex = 6;
            this.maskIVA.TabStop = false;
            this.maskIVA.Text = "_.__";
            // 
            // grpComprobante
            // 
            this.grpComprobante.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpComprobante.Controls.Add(this.RequierirComprobante);
            this.grpComprobante.Controls.Add(this.lblAplicarComo);
            this.grpComprobante.Controls.Add(this.AplicaComo);
            this.grpComprobante.Controls.Add(this.lblIVA);
            this.grpComprobante.Controls.Add(this.AfectarSaldo);
            this.grpComprobante.Controls.Add(this.lblTipoComprobante);
            this.grpComprobante.Controls.Add(this.Relaciones);
            this.grpComprobante.Controls.Add(this.StatusComprobante);
            this.grpComprobante.Controls.Add(this.TipoComprobante);
            this.grpComprobante.Controls.Add(this.lblSubTipo);
            this.grpComprobante.Controls.Add(this.SubTipoComprobante);
            this.grpComprobante.Controls.Add(this.lblStatus);
            this.grpComprobante.Controls.Add(this.lblRelacionadoA);
            this.grpComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpComprobante.HeaderText = "";
            this.grpComprobante.Location = new System.Drawing.Point(0, 205);
            this.grpComprobante.Name = "grpComprobante";
            this.grpComprobante.Size = new System.Drawing.Size(455, 143);
            this.grpComprobante.TabIndex = 7;
            // 
            // RequierirComprobante
            // 
            this.RequierirComprobante.Location = new System.Drawing.Point(12, 11);
            this.RequierirComprobante.Name = "RequierirComprobante";
            this.RequierirComprobante.Size = new System.Drawing.Size(135, 18);
            this.RequierirComprobante.TabIndex = 9;
            this.RequierirComprobante.Text = "Requiere comprobante";
            // 
            // lblAplicarComo
            // 
            this.lblAplicarComo.Location = new System.Drawing.Point(230, 114);
            this.lblAplicarComo.Name = "lblAplicarComo";
            this.lblAplicarComo.Size = new System.Drawing.Size(74, 18);
            this.lblAplicarComo.TabIndex = 15;
            this.lblAplicarComo.Text = "Aplicar como:";
            // 
            // AplicaComo
            // 
            this.AplicaComo.Location = new System.Drawing.Point(310, 113);
            this.AplicaComo.Name = "AplicaComo";
            this.AplicaComo.NullText = "Aplicar como";
            this.AplicaComo.Size = new System.Drawing.Size(126, 20);
            this.AplicaComo.TabIndex = 16;
            // 
            // AfectarSaldo
            // 
            this.AfectarSaldo.Location = new System.Drawing.Point(12, 114);
            this.AfectarSaldo.Name = "AfectarSaldo";
            this.AfectarSaldo.Size = new System.Drawing.Size(171, 18);
            this.AfectarSaldo.TabIndex = 1;
            this.AfectarSaldo.Text = "Afecta saldo del comprobante";
            // 
            // lblTipoComprobante
            // 
            this.lblTipoComprobante.Location = new System.Drawing.Point(12, 35);
            this.lblTipoComprobante.Name = "lblTipoComprobante";
            this.lblTipoComprobante.Size = new System.Drawing.Size(103, 18);
            this.lblTipoComprobante.TabIndex = 6;
            this.lblTipoComprobante.Text = "Tipo Comprobante:";
            // 
            // Relaciones
            // 
            this.Relaciones.CheckedMember = "Selected";
            this.Relaciones.DisplayMember = "Name";
            this.Relaciones.Location = new System.Drawing.Point(121, 87);
            this.Relaciones.Name = "Relaciones";
            this.Relaciones.NullText = "Relación de comprobante";
            this.Relaciones.ShowCheckAllItems = true;
            this.Relaciones.Size = new System.Drawing.Size(315, 20);
            this.Relaciones.TabIndex = 13;
            this.Relaciones.ValueMember = "Id";
            // 
            // StatusComprobante
            // 
            this.StatusComprobante.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.StatusComprobante.Location = new System.Drawing.Point(310, 61);
            this.StatusComprobante.Name = "StatusComprobante";
            this.StatusComprobante.NullText = "Status de comprobante";
            this.StatusComprobante.Size = new System.Drawing.Size(126, 20);
            this.StatusComprobante.TabIndex = 4;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.Location = new System.Drawing.Point(121, 35);
            this.TipoComprobante.Name = "TipoComprobante";
            this.TipoComprobante.NullText = "Tipo de comprobante permitido";
            this.TipoComprobante.Size = new System.Drawing.Size(315, 20);
            this.TipoComprobante.TabIndex = 14;
            // 
            // lblSubTipo
            // 
            this.lblSubTipo.Location = new System.Drawing.Point(12, 62);
            this.lblSubTipo.Name = "lblSubTipo";
            this.lblSubTipo.Size = new System.Drawing.Size(53, 18);
            this.lblSubTipo.TabIndex = 5;
            this.lblSubTipo.Text = "Sub Tipo:";
            // 
            // SubTipoComprobante
            // 
            this.SubTipoComprobante.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.SubTipoComprobante.Location = new System.Drawing.Point(121, 61);
            this.SubTipoComprobante.Name = "SubTipoComprobante";
            this.SubTipoComprobante.NullText = "Sub Tipo";
            this.SubTipoComprobante.Size = new System.Drawing.Size(126, 20);
            this.SubTipoComprobante.TabIndex = 12;
            this.SubTipoComprobante.DataBindingComplete += new Telerik.WinControls.UI.ListBindingCompleteEventHandler(this.SubTipoComprobante_DataBindingComplete);
            this.SubTipoComprobante.ItemDataBound += new Telerik.WinControls.UI.ListItemDataBoundEventHandler(this.SubTipoComprobante_ItemDataBound);
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(265, 62);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 18);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Status:";
            // 
            // lblRelacionadoA
            // 
            this.lblRelacionadoA.Location = new System.Drawing.Point(12, 88);
            this.lblRelacionadoA.Name = "lblRelacionadoA";
            this.lblRelacionadoA.Size = new System.Drawing.Size(79, 18);
            this.lblRelacionadoA.TabIndex = 5;
            this.lblRelacionadoA.Text = "Relacionado a:";
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "";
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ReadOnly = false;
            this.TConcepto.ShowActualizar = false;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowCerrar = true;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = false;
            this.TConcepto.ShowGuardar = true;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = false;
            this.TConcepto.ShowRemover = false;
            this.TConcepto.Size = new System.Drawing.Size(455, 30);
            this.TConcepto.TabIndex = 13;
            // 
            // errorConcepto
            // 
            this.errorConcepto.ContainerControl = this;
            // 
            // ConceptoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 353);
            this.Controls.Add(this.grpComprobante);
            this.Controls.Add(this.grpDescripcion);
            this.Controls.Add(this.TConcepto);
            this.Controls.Add(this.maskIVA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConceptoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Concepto";
            this.Load += new System.EventHandler(this.ConceptoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpDescripcion)).EndInit();
            this.grpDescripcion.ResumeLayout(false);
            this.grpDescripcion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormatoImpreso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormatoImpresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEfecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maskIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpComprobante)).EndInit();
            this.grpComprobante.ResumeLayout(false);
            this.grpComprobante.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequierirComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAplicarComo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AplicaComo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AfectarSaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubTipoComprobante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelacionadoA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblDescripcion;
        private Telerik.WinControls.UI.RadLabel lblIVA;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadGroupBox grpDescripcion;
        private Telerik.WinControls.UI.RadCheckBox Activo;
        private Telerik.WinControls.UI.RadMaskedEditBox maskIVA;
        private Telerik.WinControls.UI.RadLabel lblConcepto;
        private Telerik.WinControls.UI.RadTextBox Concepto;
        private Telerik.WinControls.UI.RadGroupBox grpComprobante;
        private Telerik.WinControls.UI.RadLabel lblSubTipo;
        private Telerik.WinControls.UI.RadCheckBox AfectarSaldo;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        private Telerik.WinControls.UI.RadDropDownList StatusComprobante;
        private Telerik.WinControls.UI.RadLabel lblBeneficiarios;
        private Telerik.WinControls.UI.RadLabel lblTipoOperacion;
        private Telerik.WinControls.UI.RadLabel lblEfecto;
        private Telerik.WinControls.UI.RadCheckedDropDownList Beneficiarios;
        private Telerik.WinControls.UI.RadDropDownList TipoOperacion;
        private Telerik.WinControls.UI.RadDropDownList Efecto;
        private Telerik.WinControls.UI.RadCheckBox RequierirComprobante;
        private Common.Forms.ToolBarStandarControl TConcepto;
        private Telerik.WinControls.UI.RadCheckedDropDownList Relaciones;
        private Telerik.WinControls.UI.RadLabel lblTipoComprobante;
        private Telerik.WinControls.UI.RadCheckedDropDownList TipoComprobante;
        private Telerik.WinControls.UI.RadLabel lblRelacionadoA;
        private Telerik.WinControls.UI.RadLabel lblFormatoImpreso;
        private Telerik.WinControls.UI.RadDropDownList FormatoImpresion;
        private Telerik.WinControls.UI.RadDropDownList SubTipoComprobante;
        private Telerik.WinControls.UI.RadLabel lblAplicarComo;
        private Telerik.WinControls.UI.RadDropDownList AplicaComo;
        private Telerik.WinControls.UI.RadDropDownList Beneficiario;
        private Telerik.WinControls.UI.RadCheckBox chkUnBeneficiario;
        private Telerik.WinControls.UI.RadButton btnBeneficiario;
        private System.Windows.Forms.ErrorProvider errorConcepto;
    }
}
