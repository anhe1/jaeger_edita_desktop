﻿
namespace Jaeger.UI.Banco.Forms {
    partial class ConfigurationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.textBoxDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.NoCheque = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblNumeroCheque = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblBanco = new Telerik.WinControls.UI.RadLabel();
            this.ClaveBanco = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radMultiColumnComboBox1 = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radMultiColumnComboBox2 = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2.EditorControl.MasterTemplate)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "";
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ReadOnly = false;
            this.TConcepto.ShowActualizar = false;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowCerrar = true;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = false;
            this.TConcepto.ShowGuardar = true;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = false;
            this.TConcepto.ShowRemover = false;
            this.TConcepto.Size = new System.Drawing.Size(431, 30);
            this.TConcepto.TabIndex = 14;
            // 
            // textBoxDescripcion
            // 
            this.textBoxDescripcion.Location = new System.Drawing.Point(124, 20);
            this.textBoxDescripcion.MaxLength = 32;
            this.textBoxDescripcion.Name = "textBoxDescripcion";
            this.textBoxDescripcion.Size = new System.Drawing.Size(249, 20);
            this.textBoxDescripcion.TabIndex = 16;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(40, 18);
            this.radLabel3.TabIndex = 15;
            this.radLabel3.Text = "Folder:";
            // 
            // NoCheque
            // 
            this.NoCheque.Location = new System.Drawing.Point(124, 46);
            this.NoCheque.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.NoCheque.Name = "NoCheque";
            this.NoCheque.Size = new System.Drawing.Size(92, 20);
            this.NoCheque.TabIndex = 18;
            this.NoCheque.TabStop = false;
            this.NoCheque.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNumeroCheque
            // 
            this.lblNumeroCheque.Location = new System.Drawing.Point(12, 47);
            this.lblNumeroCheque.Name = "lblNumeroCheque";
            this.lblNumeroCheque.Size = new System.Drawing.Size(59, 18);
            this.lblNumeroCheque.TabIndex = 17;
            this.lblNumeroCheque.Text = "Decimales:";
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Location = new System.Drawing.Point(124, 72);
            this.radSpinEditor1.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.radSpinEditor1.Name = "radSpinEditor1";
            this.radSpinEditor1.Size = new System.Drawing.Size(92, 20);
            this.radSpinEditor1.TabIndex = 20;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 73);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(60, 18);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "Redondeo:";
            // 
            // lblBanco
            // 
            this.lblBanco.Location = new System.Drawing.Point(12, 99);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.Size = new System.Drawing.Size(90, 18);
            this.lblBanco.TabIndex = 22;
            this.lblBanco.Text = "Recibo de Cobro";
            // 
            // ClaveBanco
            // 
            this.ClaveBanco.AutoSizeDropDownToBestFit = true;
            this.ClaveBanco.DisplayMember = "Descripcion";
            this.ClaveBanco.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClaveBanco.NestedRadGridView
            // 
            this.ClaveBanco.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClaveBanco.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaveBanco.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClaveBanco.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClaveBanco.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 200;
            this.ClaveBanco.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.ClaveBanco.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.ClaveBanco.EditorControl.Name = "NestedRadGridView";
            this.ClaveBanco.EditorControl.ReadOnly = true;
            this.ClaveBanco.EditorControl.ShowGroupPanel = false;
            this.ClaveBanco.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClaveBanco.EditorControl.TabIndex = 0;
            this.ClaveBanco.Location = new System.Drawing.Point(124, 98);
            this.ClaveBanco.Name = "ClaveBanco";
            this.ClaveBanco.NullText = "Selecciona";
            this.ClaveBanco.Size = new System.Drawing.Size(263, 20);
            this.ClaveBanco.TabIndex = 21;
            this.ClaveBanco.TabStop = false;
            this.ClaveBanco.ValueMember = "Clave";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 125);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(84, 18);
            this.radLabel2.TabIndex = 24;
            this.radLabel2.Text = "Recibo de Pago";
            // 
            // radMultiColumnComboBox1
            // 
            this.radMultiColumnComboBox1.AutoSizeDropDownToBestFit = true;
            this.radMultiColumnComboBox1.DisplayMember = "Descripcion";
            this.radMultiColumnComboBox1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radMultiColumnComboBox1.NestedRadGridView
            // 
            this.radMultiColumnComboBox1.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.radMultiColumnComboBox1.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMultiColumnComboBox1.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBox1.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.FieldName = "Descripcion";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descripcion";
            gridViewTextBoxColumn4.Width = 200;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radMultiColumnComboBox1.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBox1.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBox1.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBox1.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.radMultiColumnComboBox1.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBox1.Location = new System.Drawing.Point(124, 124);
            this.radMultiColumnComboBox1.Name = "radMultiColumnComboBox1";
            this.radMultiColumnComboBox1.NullText = "Selecciona";
            this.radMultiColumnComboBox1.Size = new System.Drawing.Size(263, 20);
            this.radMultiColumnComboBox1.TabIndex = 23;
            this.radMultiColumnComboBox1.TabStop = false;
            this.radMultiColumnComboBox1.ValueMember = "Clave";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 151);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(108, 18);
            this.radLabel4.TabIndex = 26;
            this.radLabel4.Text = "Recibo de Comisión:";
            // 
            // radMultiColumnComboBox2
            // 
            this.radMultiColumnComboBox2.AutoSizeDropDownToBestFit = true;
            this.radMultiColumnComboBox2.DisplayMember = "Descripcion";
            this.radMultiColumnComboBox2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radMultiColumnComboBox2.NestedRadGridView
            // 
            this.radMultiColumnComboBox2.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.radMultiColumnComboBox2.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMultiColumnComboBox2.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBox2.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.FieldName = "Descripcion";
            gridViewTextBoxColumn6.HeaderText = "Descripción";
            gridViewTextBoxColumn6.Name = "Descripcion";
            gridViewTextBoxColumn6.Width = 200;
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.radMultiColumnComboBox2.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radMultiColumnComboBox2.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBox2.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBox2.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBox2.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.radMultiColumnComboBox2.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBox2.Location = new System.Drawing.Point(124, 150);
            this.radMultiColumnComboBox2.Name = "radMultiColumnComboBox2";
            this.radMultiColumnComboBox2.NullText = "Selecciona";
            this.radMultiColumnComboBox2.Size = new System.Drawing.Size(263, 20);
            this.radMultiColumnComboBox2.TabIndex = 25;
            this.radMultiColumnComboBox2.TabStop = false;
            this.radMultiColumnComboBox2.ValueMember = "Clave";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radLabel3);
            this.groupBox1.Controls.Add(this.radLabel4);
            this.groupBox1.Controls.Add(this.textBoxDescripcion);
            this.groupBox1.Controls.Add(this.radMultiColumnComboBox2);
            this.groupBox1.Controls.Add(this.lblNumeroCheque);
            this.groupBox1.Controls.Add(this.radLabel2);
            this.groupBox1.Controls.Add(this.NoCheque);
            this.groupBox1.Controls.Add(this.radMultiColumnComboBox1);
            this.groupBox1.Controls.Add(this.radLabel1);
            this.groupBox1.Controls.Add(this.lblBanco);
            this.groupBox1.Controls.Add(this.radSpinEditor1);
            this.groupBox1.Controls.Add(this.ClaveBanco);
            this.groupBox1.Location = new System.Drawing.Point(12, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(405, 190);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 239);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TConcepto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigurationForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TConcepto;
        private Telerik.WinControls.UI.RadTextBox textBoxDescripcion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor NoCheque;
        private Telerik.WinControls.UI.RadLabel lblNumeroCheque;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel lblBanco;
        private Telerik.WinControls.UI.RadMultiColumnComboBox ClaveBanco;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBox1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBox2;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}