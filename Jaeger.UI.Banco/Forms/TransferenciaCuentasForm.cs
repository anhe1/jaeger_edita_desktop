﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// transferencias entre cuentas
    /// </summary>
    public partial class TransferenciaCuentasForm : RadForm {
        private BindingList<IBancoCuentaDetailModel> cuentas;

        public TransferenciaCuentasForm(Aplication.Banco.IBancoService service) {
            InitializeComponent();
            this.TMovimiento.Service = service;
        }

        public TransferenciaCuentasForm(IMovimientoBancarioDetailModel model, Aplication.Banco.IBancoService service) {
            InitializeComponent();
            this.TMovimiento.Service = service;
            this.TMovimiento.Movimiento = model;
            if (this.TMovimiento.Movimiento != null) {
                this.TMovimiento.Movimiento.CuentaPropia = MovimientoBancarioDetailModel.Json(model.InfoAuxiliar).CuentaPropia;
            }
        }

        private void TransferenciaCuentasForm_Load(object sender, EventArgs e) {
            this.Preparar.RunWorkerAsync();
            this.FechaDocumento.Value = DateTime.Now;
            this.FechaDocumento.SetToNullValue();
            this.FechaAplicacion.Value = DateTime.Now;
            this.FechaAplicacion.SetToNullValue();

            this.TMovimiento.Nuevo.Click += this.Nuevo_Click;
            this.TMovimiento.Guardar.Click += this.Guardar_Click;
            this.TMovimiento.Actualizar.Click += this.Actualizar_Click;
            this.TMovimiento.Imprimir.Click += Imprimir_Click;
            this.TMovimiento.Cerrar.Click += this.Cerrar_Click;
        }

        private void Nuevo_Click(object sender, EventArgs e) {
            this.TMovimiento.Movimiento = new MovimientoBancarioDetailModel();
        }

        private void Guardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new Common.Forms.Waiting2Form(this.Guardar)) {
                    espera.ShowDialog(this);
                }
            } else {
                MessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Error, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento.Id == 0) {
                this.TMovimiento.Movimiento.TipoOperacion = BancoTipoOperacionEnum.TransferenciaEntreCuentas;
                this.TMovimiento.Movimiento.Tipo = MovimientoBancarioEfectoEnum.Egreso;
                this.TMovimiento.Movimiento.Status = MovimientoBancarioStatusEnum.Transito;
                this.TMovimiento.Movimiento.Concepto = "Transferencia entre cuentas";
            }
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(this.TMovimiento.Movimiento));
                imprimir.Show();
            }
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CboCuentaOrigen_SelectedIndexChanged(object sender, EventArgs e) {
            var seleccionado = ((GridViewDataRowInfo)this.comboBoxCuentaOrigen.SelectedItem).DataBoundItem as IBancoCuentaDetailModel;
            if (seleccionado != null) {
                this.TMovimiento.Movimiento.NumeroCuentaP = seleccionado.NumCuenta;
                this.TMovimiento.Movimiento.BancoP = seleccionado.Banco;
                this.TMovimiento.Movimiento.CuentaCLABEP = seleccionado.CLABE;
                this.TMovimiento.Movimiento.AliasP = seleccionado.Alias;
                this.TMovimiento.Movimiento.SucursalP = seleccionado.Sucursal;
                this.TMovimiento.Movimiento.BeneficiarioP = seleccionado.Beneficiario;
                this.TMovimiento.Movimiento.BeneficiarioRFCP = seleccionado.RFCBenficiario;
                this.TMovimiento.Movimiento.ClaveBancoP = seleccionado.ClaveBanco;
            }
        }

        private void CboCuentaDestino_SelectedIndexChanged(object sender, EventArgs e) {
            var seleccionado = ((GridViewDataRowInfo)this.comboBoxBeneficiario.SelectedItem).DataBoundItem as IBancoCuentaDetailModel;
            if (seleccionado != null) {
                this.TMovimiento.Movimiento.NumeroCuentaT = seleccionado.NumCuenta;
                this.TMovimiento.Movimiento.BancoT = seleccionado.Banco;
                this.TMovimiento.Movimiento.CuentaCLABET = seleccionado.CLABE;
                //this.ToolBar.Movimiento.AliasT = seleccionado.Alias;
                this.TMovimiento.Movimiento.SucursalT = seleccionado.Sucursal;
                this.TMovimiento.Movimiento.BeneficiarioT = seleccionado.Beneficiario;
                this.TMovimiento.Movimiento.BeneficiarioRFCT = seleccionado.RFCBenficiario;
                this.TMovimiento.Movimiento.ClaveBancoT = seleccionado.ClaveBanco;
            }
        }

        private void ComboBox_DropDownOpening(object sender, CancelEventArgs args) {
            args.Cancel = this.TMovimiento.Movimiento.Id > 0;
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            var query = Aplication.Banco.BancoCuentaService.Query().OnlyActive().Build();
            var d = this.TMovimiento.Service.GetList<BancoCuentaDetailModel>(query).ToList<IBancoCuentaDetailModel>();
            //this.cuentas = this.TMovimiento.Service.GetList(false);
            this.cuentas = new BindingList<IBancoCuentaDetailModel>(d);
            this.Moneda.DataSource = this.TMovimiento.Service.GetMonedas();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.comboBoxCuentaOrigen.DataSource = new BindingList<IBancoCuentaDetailModel>(this.cuentas);
            this.comboBoxCuentaOrigen.SelectedIndex =- 1;

            this.comboBoxBeneficiario.DataSource = new BindingList<IBancoCuentaDetailModel>(this.cuentas);
            this.comboBoxBeneficiario.SelectedIndex = -1;

            this.FormaPago.DataSource = this.TMovimiento.Service.GetBancoFormaPago();
            this.FormaPago.EditorControl.AllowRowResize = false;
            this.FormaPago.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.FormaPago.EditorControl.ShowRowHeaderColumn = false;
            this.FormaPago.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.FormaPago.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.FormaPago.SelectedIndex = -1;
            this.FormaPago.Enabled = true;

            this.TMovimiento.Actualizar.PerformClick();

            this.comboBoxCuentaOrigen.SelectedIndexChanged += new EventHandler(this.CboCuentaOrigen_SelectedIndexChanged);
            this.comboBoxBeneficiario.SelectedIndexChanged += CboCuentaDestino_SelectedIndexChanged;
        }

        private bool Validar() {
            this.Advertencia.Clear();
            if (this.TMovimiento.Movimiento.IdCuentaP == this.TMovimiento.Movimiento.IdCuentaT) {
                this.Advertencia.SetError(this.comboBoxCuentaOrigen, Properties.Resources.msg_Cuentas_Iguales);
                return false;
            } else if (this.TMovimiento.Movimiento.IdCuentaP <= 0) {
                this.Advertencia.SetError(this.comboBoxCuentaOrigen, Properties.Resources.msg_Concepto_Vacio);
                return false;
            } else if (this.TMovimiento.Movimiento.IdCuentaT <= 0) {
                this.Advertencia.SetError(this.comboBoxBeneficiario, Properties.Resources.msg_Cuenta_Destino);
                return false;
            } else if (this.TMovimiento.Movimiento.Concepto == null) {
                this.Advertencia.SetError(this.Concepto, Properties.Resources.msg_Concepto_Vacio);
                return false;
            } else if (this.TMovimiento.Movimiento.ClaveFormaPago == null) {
                this.Advertencia.SetError(this.FormaPago, Properties.Resources.msg_Banco_Movimiento_Error);
                return false;
            } else if (this.TMovimiento.Movimiento.Cargo <= 0) {
                this.Advertencia.SetError(this.Cargo, Properties.Resources.msg_Monto_Error);
                return false;
            }

            return true;
        }

        private void Guardar() {
            this.TMovimiento.Movimiento = this.TMovimiento.Service.Save(this.TMovimiento.Movimiento);
        }

        private void Desabilitar() {
            this.comboBoxCuentaOrigen.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.comboBoxCuentaOrigen.DropDownOpening += new RadPopupOpeningEventHandler(this.ComboBox_DropDownOpening);

            this.comboBoxBeneficiario.DropDownOpening += new RadPopupOpeningEventHandler(this.ComboBox_DropDownOpening);
            this.comboBoxBeneficiario.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.comboBoxBeneficiario.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FormaPago.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.FormaPago.DropDownOpening += new RadPopupOpeningEventHandler(this.ComboBox_DropDownOpening);

            this.Moneda.MultiColumnComboBoxElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);
            this.Moneda.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Moneda.DropDownOpening += new RadPopupOpeningEventHandler(this.ComboBox_DropDownOpening);

            this.TipoCambio.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumOperacion.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumAuto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumDocto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Referencia.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Concepto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Nota.ReadOnly = this.TMovimiento.Movimiento.Id > 0;

            this.FechaDocumento.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.FechaDocumento.DateTimePickerElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.FechaAplicacion.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.FechaAplicacion.DateTimePickerElement.ArrowButton.Visibility = (this.TMovimiento.Movimiento.Id > 0 ? ElementVisibility.Collapsed : ElementVisibility.Visible);

            this.Cargo.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.TMovimiento.ShowGuardar = !(this.TMovimiento.Movimiento.Id > 0);
        }

        private void ToolBarListStatus_SelectedValueChanged(object sender, Telerik.WinControls.UI.Data.ValueChangedEventArgs e) {
            if (e.NewValue != null) {
                if (this.TMovimiento.Movimiento != null)
                this.TMovimiento.Movimiento.IdStatus = (int)e.NewValue;
                Console.WriteLine(e.NewValue.ToString());
            }
        }

        private void ToolBar_BindingCompleted(object sender, EventArgs e) {
            this.FormaPago.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumDocto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaEmision.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaDocumento.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaDocto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.FechaAplicacion.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaAplicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.comboBoxCuentaOrigen.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "IdCuentaP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumeroCuentaP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumeroCuentaP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.BancoP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BancoP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CuentaClabeP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "CuentaCLABEP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.comboBoxBeneficiario.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "IdCuentaT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumeroCuentaT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumeroCuentaT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.BancoT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BancoT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CuentaClabeT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "CuentaCLABET", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Concepto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Referencia.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumAuto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumAutorizacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.NumOperacion.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumOperacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Nota.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCambio.DataBindings.Add("Text", this.TMovimiento.Movimiento, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Moneda.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Cargo.DataBindings.Add("Value", this.TMovimiento.Movimiento, "Cargo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Desabilitar();
        }
    }
}
