﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Telerik.WinControls.Data;
using Telerik.WinControls.Enumerations;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// grid de movimientos bancarios
    /// </summary>
    public partial class MovimientosGridControl : UserControl {
        #region declaraciones
        private bool _Cancelar = false;
        private bool _Auditar = false;
        private bool _ActualizarRegistro = false;
        private bool _Status = false;
        public BindingList<IMovimientoBancarioDetailModel> Data;
        public RadMenuItem TCopiar = new RadMenuItem { Name = "Copiar", Text = "Copiar" };
        public RadMenuItem TClonar = new RadMenuItem { Name = "Clonar", Text = "Clonar" };
        public RadMenuItem TImprimir = new RadMenuItem { Name = "Imprimir", Text = "Imprimir" };
        public RadMenuItem TImprimirComprobante = new RadMenuItem { Name = "Comprobante", Text = "Comprobante" };
        public RadMenuItem TImprimirHorizontal = new RadMenuItem { Name = "Horizonal", Text = "Horizontal" };
        public RadMenuItem TImprimirVertical = new RadMenuItem { Name = "Vertical", Text = "Vertifcal" };
        public RadMenuItem TSeleccionMultiple = new RadMenuItem { Name = "SeleccionMultimple", Text = "Selección Múltiple" };
        public RadMenuItem TBancoLayout = new RadMenuItem { Name = "BancoLayout", Text = "Layout Bancario" };
        public RadMenuItem TBanamexLayoutC = new RadMenuItem { Name = "LayoutBanamexC", Text = "Banamex C" };
        public RadMenuItem TEmportar = new RadMenuItem { Text = "Exportar por templete" };
        public RadMenuItem TAuxiliar = new RadMenuItem { Text = "Importar Auxiliar", ToolTipText = "Importar auxiliares de forma masiva" };
        public RadMenuItem TCAuxiliar = new RadMenuItem { Text = "Agregar archivo auxiliar" };
        #endregion

        #region eventos del control
        public event EventHandler<MovimientoBancarioDetailModel> Actualizar;
        public event EventHandler<IMovimientoBancarioDetailModel> Auditado;
        public event EventHandler<EventArgs> Referencia;
        public event EventHandler<EventArgs> NumAutorizacion;

        protected void OnActualizar(object sender, MovimientoBancarioDetailModel e) {
            if (this.Actualizar != null)
                this.Actualizar(sender, e);
        }

        protected void OnReferencia(object sender, EventArgs e) {
            if (this.Referencia != null) {
                this.Referencia(sender, e);
            }
        }

        protected void OnNumAutorizacion(object sender, EventArgs e) {
            if (this.NumAutorizacion != null) {
                this.NumAutorizacion(sender, e);
            }
        }

        protected void OnAuditado(object sender, IMovimientoBancarioDetailModel e) {
            if (this.Auditado != null) {
                this.Auditado(sender, e);
            }
        }

        public void OnFiltros(object sender, EventArgs eventArgs) {
            this.GridData.ActivateFilter(((CommandBarToggleButton)sender).ToggleState);
        }

        public void OnAutoSuma(object sender, EventArgs eventArgs) {
            this.GridData.AutoSum(((CommandBarToggleButton)sender).ToggleState == ToggleState.Off);
        }
        #endregion

        public MovimientosGridControl() {
            InitializeComponent();
            this.MenuContextual = new RadContextMenu();
        }

        private void MovimientosGridControl_Load(object sender, EventArgs e) {
            this.GridData.Standard();
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);

            this.GridComprobantes.Standard();
            this.GridComprobantes.HierarchyDataProvider = new GridViewEventDataProvider(this.GridComprobantes);

            this.GridAuxiliar.Standard();
            this.GridAuxiliar.HierarchyDataProvider = new GridViewEventDataProvider(this.GridAuxiliar);

            this.GridTransferencia.Standard();
            this.GridTransferencia.HierarchyDataProvider = new GridViewEventDataProvider(this.GridTransferencia);

            this.GridData.CellBeginEdit += this.GridData_CellBeginEdit;
            this.GridData.CellEndEdit += this.GridData_CellEndEdit;
            this.GridData.CellValidating += this.GridData_CellValidating;
            this.GridData.ContextMenuOpening += this.GridData_ContextMenuOpening;
            this.GridData.CommandCellClick += this.RadGridAuxiliares_CommandCellClick;

            this.MenuContextual.Items.AddRange(new RadItem[] { this.TCopiar, this.TClonar, this.TImprimir, this.TSeleccionMultiple, this.TBancoLayout });
            this.TCopiar.Click += this.MenuContextual_Copiar_Click;
            this.TSeleccionMultiple.Click += this.MenuContextual_SeleccionMultiple_Click;
        }

        #region propiedades
        public bool Cancelar {
            get { return this._Cancelar; }
            set { this._Cancelar = value; }
        }

        public bool Auditar {
            get { return this._Auditar; }
            set { this._Auditar = value; }
        }

        public bool Status {
            get { return this._Status; }
            set {
                this._Status = value;
                this.GridData.AllowEditRow = value;
            }
        }

        public RadContextMenu MenuContextual { get; set; }

        public Aplication.Banco.IBancoService Service { get; set; }
        #endregion

        #region eventos del grid
        protected virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.GridData.CurrentRow.ViewInfo.ViewTemplate == this.GridData.MasterTemplate) {
                    e.ContextMenu = this.MenuContextual.DropDown;
                }
            }
        }

        protected virtual void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    var seleccionado = this.Current();
                    switch (seleccionado.Status) {
                        case MovimientoBancarioStatusEnum.Cancelado:
                            e.Cancel = true;
                            break;
                        case MovimientoBancarioStatusEnum.Transito:
                            break;
                        case MovimientoBancarioStatusEnum.Aplicado:
                            break;
                        case MovimientoBancarioStatusEnum.Auditado:
                            e.Cancel = true;
                            break;
                        default:
                            break;
                    }
                } else if (e.Column.Name == "Referencia") {
                    var seleccionado = this.Current();
                    if (seleccionado.Referencia != null) {
                        if (seleccionado.Referencia.Length > 0) {
                            e.Cancel = true;
                        }
                    }
                } else if (e.Column.Name == "NumAutorizacion") {
                    var seleccionado = this.Current();
                    if (seleccionado.NumAutorizacion != null) {
                        if (seleccionado.NumAutorizacion.Length > 0) {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        protected virtual void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (this._ActualizarRegistro == true) {
                        var seleccionado = e.Row.DataBoundItem as MovimientoBancarioDetailModel;
                        this.OnActualizar(sender, seleccionado);
                        this.OnReferencia(sender, e);
                        this._ActualizarRegistro = false;
                    } else {
                        //var seleccionado = e.Row.DataBoundItem as MovimientoBancarioDetailModel;
                        //this.OnAuditado(sender, seleccionado);
                    }
                } else if (e.Column.Name == "Referencia") {
                    var seleccionado = this.Current();
                    if (seleccionado.Referencia != null) {
                        this.OnReferencia(sender, e);
                    }
                } else if (e.Column.Name == "NumAutorizacion") {
                    var seleccionado = this.Current();
                    if (seleccionado.NumAutorizacion != null) {
                        this.OnNumAutorizacion(sender, e);
                    }
                }
            }
        }

        protected virtual void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Column == null) {
                return;
            }
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        } else {
                            if ((int)e.Value == (int)MovimientoBancarioStatusEnum.Cancelado) {
                                if (this.Cancelar == false) {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Cancelar_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    return;
                                }

                                if (this._Cancelar == false) {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                    RadMessageBox.Show(this, "No tiene permisos para cancelar el movimiento", "Información", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    return;
                                }

                                if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                    return;
                                } else {
                                    if ((int)e.OldValue != (int)MovimientoBancarioStatusEnum.Cancelado) {
                                        e.Cancel = false;
                                        this._ActualizarRegistro = true;
                                    }
                                }
                            } else if ((int)e.Value == (int)MovimientoBancarioStatusEnum.Aplicado) {
                                var seleccionado = e.Row.DataBoundItem as MovimientoBancarioDetailModel;
                                if (seleccionado.PorComprobar == false) {
                                    this._ActualizarRegistro = true;
                                    e.Cancel = false;
                                } else {
                                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_Status_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Error, MessageBoxDefaultButton.Button1);
                                    e.Cancel = true;
                                }
                                return;
                            } else if ((int)e.Value == (int)MovimientoBancarioStatusEnum.Transito) {
                                if ((int)e.OldValue == (int)MovimientoBancarioStatusEnum.Aplicado) {
                                    this.GridData.CancelEdit();
                                    e.Cancel = true;
                                    RadMessageBox.Show(this, "No es posible cambiar al estado anterior.", "Información", MessageBoxButtons.OK, RadMessageIcon.Error, MessageBoxDefaultButton.Button1);
                                }
                                return;
                            } else if ((int)e.Value == (int)MovimientoBancarioStatusEnum.Auditado) {
                                this._ActualizarRegistro = true;
                                e.Cancel = false;
                            }
                        }
                    }
                }
            }
        }

        protected virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridComprobantes.Caption) {
                var rowView = e.ParentRow.DataBoundItem as MovimientoBancarioDetailModel;
                if (rowView != null) {
                    if (rowView.Comprobantes != null) {
                        foreach (var item in rowView.Comprobantes) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Identificador"].Value = item.Identificador;
                            row.Cells["TipoComprobanteText"].Value = item.TipoComprobanteText;
                            row.Cells["TipoDocumentoText"].Value = item.TipoDocumentoText;
                            row.Cells["Folio"].Value = item.Folio;
                            row.Cells["Serie"].Value = item.Serie;
                            row.Cells["IdDocumento"].Value = item.IdDocumento;
                            row.Cells["FechaEmision"].Value = item.FechaEmision;
                            row.Cells["EmisorRFC"].Value = item.EmisorRFC;
                            row.Cells["EmisorNombre"].Value = item.EmisorNombre;
                            row.Cells["ReceptorRFC"].Value = item.ReceptorRFC;
                            row.Cells["ReceptorNombre"].Value = item.ReceptorNombre;
                            row.Cells["Total"].Value = item.Total;
                            row.Cells["Acumulado"].Value = item.Acumulado;
                            row.Cells["Cargo"].Value = item.Cargo;
                            row.Cells["Abono"].Value = item.Abono;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.GridAuxiliar.Caption) {
                var rowView = e.ParentRow.DataBoundItem as MovimientoBancarioDetailModel;
                if (rowView != null) {
                    if (rowView.Comprobantes != null) {
                        foreach (var item in rowView.Auxiliar) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            row.Cells["FileName"].Value = item.FileName;
                            row.Cells["Button"].Value = item.Button;
                            row.Cells["URL"].Value = item.URL;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.GridTransferencia.Caption) {

            }
        }

        /// <summary>
        /// para descargar los documentos anexos
        /// </summary>
        private void RadGridAuxiliares_CommandCellClick(object sender, GridViewCellEventArgs e) {
            if (e.Row.ViewTemplate.Caption == this.GridAuxiliar.Caption) {
                var seleccionado = e.Row.Cells["URL"].Value as string;
                if (seleccionado != null) {
                    if (ValidacionService.URL(seleccionado)) {
                        var saveFile = new SaveFileDialog {
                            FileName = e.Row.Cells["FileName"].Value as string
                        };
                        if (saveFile.ShowDialog(this) == DialogResult.OK) {
                            if (Util.Downloads.File(seleccionado, saveFile.FileName) == false) {
                                RadMessageBox.Show(this, Properties.Resources.msg_Banco_Auxiliar_Descargar_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                                return;
                            } else {
                                RadMessageBox.Show(this, string.Format(Properties.Resources.msg_Banco_Auxiliar_Descargar_Correcto, saveFile.FileName), "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region menu contextual
        /// <summary>
        /// copiar al porta papeles la celda seleccionada
        /// </summary>
        protected virtual void MenuContextual_Copiar_Click(object sender, EventArgs e) {
            Clipboard.SetDataObject(this.GridData.CurrentCell.Value.ToString());
        }

        /// <summary>
        /// tipo de seleccion
        /// </summary>
        protected virtual void MenuContextual_SeleccionMultiple_Click(object sender, EventArgs e) {
            this.TSeleccionMultiple.IsChecked = !this.TSeleccionMultiple.IsChecked;
            this.GridData.MultiSelect = this.TSeleccionMultiple.IsChecked;
        }

       
        #endregion

        /// <summary>
        /// objeto actual seleccionado 
        /// </summary>
        public virtual MovimientoBancarioDetailModel Current() {
            return (MovimientoBancarioDetailModel)this.GridData.ReturnRowSelected();
        }

        public virtual System.Collections.Generic.List<IMovimientoBancarioDetailModel> GetSelecteds() {
            return this.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as IMovimientoBancarioDetailModel).ToList();
        }

        public virtual MovimientosReportePrinter GetPrinters() {
            var contador = this.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as MovimientoBancarioDetailModel).Count();
            if (contador > 1) {
                var seleccionados = this.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as MovimientoBancarioDetailModel).ToList();
                var d0 = new MovimientosReportePrinter {
                    Movimientos = seleccionados
                };
                return d0;
            } else {
                var seleccionados = this.GridData.ChildRows.Select(it => it.DataBoundItem as MovimientoBancarioDetailModel).ToList();
                var d0 = new MovimientosReportePrinter {
                    Movimientos = seleccionados
                };
                return d0;
            }
        }
    }
}
