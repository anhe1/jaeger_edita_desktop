﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// Bancos: catalogo de conceptos de movimientos banarios
    /// </summary>
    public partial class ConceptoCatalogoForm : RadForm {
        protected internal Aplication.Banco.IBancoConceptoService _Service;
        protected internal BindingList<IMovimientoConceptoDetailModel> _DataSource;
        protected internal RadMenuItem crearTabla = new RadMenuItem { Text = "Crear tablas", Name = "CrearTablas" };

        public ConceptoCatalogoForm() {
            InitializeComponent();
            this.TConcepto.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public ConceptoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this.TConcepto.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ConceptoCatalogoForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;

            this.TConcepto.Nuevo.Click += this.Agregar_Click;
            this.TConcepto.Editar.Click += this.Editar_Click;
            this.TConcepto.Remover.Click += this.Remover_Click;
            this.TConcepto.Actualizar.Click += this.Actualizar_Click;
            this.TConcepto.Cerrar.Click += this.Cerrar_Click;

            crearTabla.Click += new EventHandler(this.Crear_Click);
            this.TConcepto.Herramientas.Items.Add(crearTabla);
        }

        public virtual void Agregar_Click(object sender, EventArgs e) {
            using (var nuevo = new ConceptoForm(null, this._Service)) {
                nuevo.ShowDialog(this);
            }
        }

        public virtual void Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TConcepto.GetCurrent<MovimientoConceptoDetailModel>();
            if (seleccionado != null) {
                if (seleccionado.ReadOnly) {
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_TipoOperacion_NoEditable, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
                using (var editar = new ConceptoForm(seleccionado, this._Service)) {
                    editar.ShowDialog(this);
                }
            }
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TConcepto.GetCurrent<MovimientoConceptoDetailModel>();
            if (seleccionado != null) {
                if (seleccionado.ReadOnly) {
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_TipoOperacion_NoEditable, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                if (RadMessageBox.Show(this, "¿Esta seguro de desactivar el concepto seleccionado?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    using (var espera = new Waiting2Form(this.Desactivar)) {
                        espera.Text = "Actualizando ...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TConcepto.GridData.DataSource = this._DataSource;
        }

        public virtual void Crear_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_CrearTablas, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new Waiting2Form(this.Crear)) {
                    espera.Text = Properties.Resources.msg_Sistema_Creando_Tablas;
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Crear() {
            this._Service.CreateTables();
        }

        public virtual void Consultar() {
            this._DataSource = new BindingList<IMovimientoConceptoDetailModel>(
                this._Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().OnlyActive().Build()).ToList<IMovimientoConceptoDetailModel>()
                );
        }

        public virtual void Desactivar() {
            var seleccionado = this.TConcepto.GetCurrent<MovimientoConceptoDetailModel>();
            if (seleccionado != null) {
                seleccionado.Activo = !seleccionado.Activo;
                seleccionado = this._Service.Save(seleccionado);
            }
        }
    }
}
