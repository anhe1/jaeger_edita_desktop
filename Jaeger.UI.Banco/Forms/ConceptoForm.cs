﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Banco.Forms {
    public partial class ConceptoForm : RadForm {
        protected IBancoConceptoService service;
        private System.Collections.Generic.List<BancoTipoComprobanteModel> lista1;
        private MovimientoConceptoDetailModel currentConcepto;

        public ConceptoForm(IBancoConceptoService service) {
            InitializeComponent();
            this.service = service;
        }

        public ConceptoForm(MovimientoConceptoDetailModel model, IBancoConceptoService service) {
            InitializeComponent();
            this.currentConcepto = model;
            this.service = service;
        }

        private void ConceptoForm_Load(object sender, EventArgs e) {
            this.Efecto.DisplayMember = "Descripcion";
            this.Efecto.ValueMember = "Id";
            this.Efecto.DataSource = BancoConceptoService.GetMovimientoTipo();

            this.TipoOperacion.DisplayMember = "Descripcion";
            this.TipoOperacion.ValueMember = "Id";
            this.TipoOperacion.DataSource = BancoConceptoService.GetTipoOperacion();

            // relaciones comerciales
            this.Beneficiarios.CheckedMember = "Selected";
            this.Beneficiarios.DisplayMember = "Name";
            this.Beneficiarios.ValueMember = "Id";


            this.SubTipoComprobante.DisplayMember = "Descripcion";
            this.SubTipoComprobante.ValueMember = "Id";
            this.SubTipoComprobante.DataSource = ConfigService.CFDISubTipo();

            this.TipoComprobante.CheckedMember = "Selected";
            this.TipoComprobante.DisplayMember = "Name";
            this.TipoComprobante.ValueMember = "Id";

            this.Relaciones.CheckedMember = "Selected";
            this.Relaciones.DisplayMember = "Name";
            this.Relaciones.ValueMember = "Id";

            this.lista1 = ((MovimientoBancarioFormatoImpreso[])Enum.GetValues(typeof(MovimientoBancarioFormatoImpreso)))
                .Select(c => new BancoTipoComprobanteModel() {
                    Id = (int)c,
                    Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
                }).ToList();
            this.FormatoImpresion.DataSource = lista1;
            this.FormatoImpresion.DisplayMember = "Descripcion";
            this.FormatoImpresion.ValueMember = "Id";

            if (this.currentConcepto == null) {
                this.currentConcepto = new MovimientoConceptoDetailModel();
            }
            this.CreateBinding();

            this.TConcepto.Nuevo.Click += this.TConcepto_Nuevo_Click;
            this.TConcepto.Guardar.Click += this.TConcepto_Guardar_Click;
            this.TConcepto.Cerrar.Click += this.TConcepto_Cerrar_Click;

            
            this.SubTipoComprobante.SelectedValueChanged += this.SubTipoComprobante_SelectedValueChanged;
        }

        private void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            this.currentConcepto = null;
        }

        private void TConcepto_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new Common.Forms.Waiting2Form(this.Guardar)) {
                    espera.ShowDialog(this);
                    this.Close();
                }
            } else {
                RadMessageBox.Show(this, "Existen algunos errores de captura.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        private void TConcepto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            // titulo de la ventana
            this.Text = (this.currentConcepto.ReadOnly == true ? "Registro de solo lectura" : "Tipo de Operación:");
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this.currentConcepto, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Concepto.DataBindings.Clear();
            this.Concepto.DataBindings.Add("Text", this.currentConcepto, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Activo.DataBindings.Clear();
            this.Activo.DataBindings.Add("Checked", this.currentConcepto, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Efecto.DataBindings.Clear();
            this.Efecto.DataBindings.Add("SelectedValue", this.currentConcepto, "IdTipoEfecto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.RequierirComprobante.DataBindings.Clear();
            this.RequierirComprobante.DataBindings.Add("Checked", this.currentConcepto, "RequiereComprobantes", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Beneficiarios.DataBindings.Clear();
            this.Beneficiarios.DataSource = this.currentConcepto.ObjetosDirectorio;
            this.TipoOperacion.DataBindings.Clear();
            this.TipoOperacion.DataBindings.Add("SelectedValue", this.currentConcepto, "IdTipoOperacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.StatusComprobante.DataBindings.Clear();
            this.StatusComprobante.DataBindings.Add("SelectedValue", this.currentConcepto, "IdStatusComprobante", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AfectarSaldo.DataBindings.Clear();
            this.AfectarSaldo.DataBindings.Add("Checked", this.currentConcepto, "AfectaSaldoComprobantes", true, DataSourceUpdateMode.OnPropertyChanged);
            this.SubTipoComprobante.DataBindings.Clear();
            this.SubTipoComprobante.DataBindings.Add("SelectedValue", this.currentConcepto, "IdSubTipoComprobante", true, DataSourceUpdateMode.OnPropertyChanged);
            this.maskIVA.DataBindings.Clear();
            this.maskIVA.DataBindings.Add("Value", this.currentConcepto, "IVA", true, DataSourceUpdateMode.OnPropertyChanged);
            this.TipoComprobante.DataSource = this.currentConcepto.ObjetosComprobante;
            this.Relaciones.DataSource = this.currentConcepto.ObjetosRelacion;
            this.FormatoImpresion.DataBindings.Clear();
            this.FormatoImpresion.DataBindings.Add("SelectedValue", this.currentConcepto, "IdFormatoImpreso", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descripcion.ReadOnly = this.currentConcepto.ReadOnly;
            this.Concepto.ReadOnly = this.currentConcepto.ReadOnly;
            this.Activo.ReadOnly = this.currentConcepto.ReadOnly;
            this.maskIVA.ReadOnly = this.currentConcepto.ReadOnly;
            this.Efecto.ReadOnly = this.currentConcepto.ReadOnly;
            this.RequierirComprobante.ReadOnly = this.currentConcepto.ReadOnly;
            this.Beneficiarios.ReadOnly = this.currentConcepto.ReadOnly;
            this.TipoOperacion.ReadOnly = this.currentConcepto.ReadOnly;
            this.StatusComprobante.ReadOnly = this.currentConcepto.ReadOnly;
            this.AfectarSaldo.ReadOnly = this.currentConcepto.ReadOnly;
            this.SubTipoComprobante.ReadOnly = this.currentConcepto.ReadOnly;
            this.TConcepto.Guardar.Enabled = !this.currentConcepto.ReadOnly;
        }

        private void Guardar() {
            this.currentConcepto = this.service.Save(this.currentConcepto);
        }

        private bool Validar() {
            this.errorConcepto.Clear();
            if (string.IsNullOrEmpty(this.currentConcepto.Descripcion)) {
                this.errorConcepto.SetError(this.Descripcion, "Es necesaria una descripción.");
                return false;
            }

            if (string.IsNullOrEmpty(this.currentConcepto.Concepto)) {
                this.errorConcepto.SetError(this.Concepto, "Es necesaria un concepto");
                return false;
            }

            if (this.currentConcepto.RequiereComprobantes == true && this.currentConcepto.ObjetosComprobante.Count == 0) {
                this.errorConcepto.SetError(this.TipoComprobante, "No seleccionado tipos de comprobantes.");
                return false;
            }

            return true;
        }

        private void SubTipoComprobante_SelectedValueChanged(object sender, EventArgs e) {
            this.ChangeData();
        }

        private void SubTipoComprobante_DataBindingComplete(object sender, ListBindingCompleteEventArgs e) {
            this.ChangeData();
        }

        private void SubTipoComprobante_ItemDataBound(object sender, ListItemDataBoundEventArgs args) {
            this.ChangeData();
        }

        private void ChangeData() {
            if (this.currentConcepto != null) {
                if (currentConcepto.IdSubTipoComprobante == 1) {
                    this.StatusComprobante.DataSource = ((CFDIStatusEmitidoEnum[])Enum.GetValues(typeof(CFDIStatusEmitidoEnum)))
                        .Select(c => new StatusModel((int)c, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description)).ToList();
                    this.StatusComprobante.DisplayMember = "Descripcion";
                    this.StatusComprobante.ValueMember = "Id";
                } else if (currentConcepto.IdSubTipoComprobante == 2) {
                    this.StatusComprobante.DataSource = ((CFDIStatusRecibidoEnum[])Enum.GetValues(typeof(CFDIStatusRecibidoEnum)))
                        .Select(c => new StatusModel((int)c, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description)).ToList();
                    this.StatusComprobante.DisplayMember = "Descripcion";
                    this.StatusComprobante.ValueMember = "Id";
                } else {
                    this.StatusComprobante.DataSource = null;
                }
            }
        }
    }
}
