﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Banco;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.Banco.Forms {
    public partial class BuscarComprobanteForm : RadForm {
        protected IBancoService service;
        protected Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        protected internal BindingList<MovimientoBancarioComprobanteDetailModel> datos;
        protected internal MovimientoConceptoDetailModel _CurrentConcepto;
        protected internal int _IdDirectorio;
        protected internal MovimientoBancarioTipoComprobanteEnum _TipoComprobante;

        public void OnAgregar(MovimientoBancarioComprobanteDetailModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<MovimientoBancarioComprobanteDetailModel> AgregarComprobante;

        public BuscarComprobanteForm() {
            InitializeComponent();
        }

        public BuscarComprobanteForm(MovimientoConceptoDetailModel conceptoDetailModel, int idDirectorio, MovimientoBancarioTipoComprobanteEnum tipoComprobante, IBancoService service) {
            InitializeComponent();
            this.service = service;
            this._CurrentConcepto = conceptoDetailModel;
            this._IdDirectorio = idDirectorio;
            this._TipoComprobante = tipoComprobante;
            this.GridSearch.ShowFilteringRow = true;
        }

        private void BuscarComprobanteForm_Load(object sender, EventArgs e) {
            this.GridSearch.SubTipo = this._CurrentConcepto.SubTipoComprobante;
            this.TBuscar.Agregar.Click += this.TAgregar_Click;
            this.TBuscar.Buscar.Click += this.TBuscar_Click;
            this.TBuscar.Cerrar.Click += this.TCerrar_Click;
            this.TBuscar.Descripcion.TextBoxElement.TextAlign = HorizontalAlignment.Center;
            this.TBuscar.Descripcion.TextBoxElement.KeyPress += new KeyPressEventHandler(TelerikTextBoxExtension.TxbFolio_KeyPress);
            this.TBuscar.Descripcion.TextBoxElement.KeyDown += TextBoxElement_KeyDown;
            this.GridSearch.CellDoubleClick += this.Grid_CellDoubleClick;
        }

        private void TextBoxElement_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) this.TBuscar_Click(sender, e);
        }

        public virtual void TAgregar_Click(object sender, EventArgs e) {
            if (this.GridSearch.CurrentRow != null) {
                var seleccionado = this.GridSearch.CurrentRow.DataBoundItem as MovimientoBancarioComprobanteDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.Estado.ToLower().Contains("cancelado")) {
                        RadMessageBox.Show(this, Properties.Resources.msg_comprobante_cancelado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                    this.OnAgregar(seleccionado);
                }
            }
        }

        public virtual void TBuscar_Click(object sender, EventArgs e) {
            if (this.bwSearch.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.bwSearch.RunWorkerAsync();
            }
        }

        public virtual void TCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Grid_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.GridSearch.CurrentRow != null) {
                this.TBuscar.Agregar.PerformClick();
                this.Close();
            }
        }

        public virtual void WorkerSearch_DoWork(object sender, DoWorkEventArgs e) {
            if (this._TipoComprobante == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                var d0 = this.service.GetList<MovimientoBancarioComprobanteDetailModel>(
                    new Domain.Banco.Builder.ComprobanteSearchQueryBuilder().ForComprobante().With(this._CurrentConcepto).IdDirectorio(this._IdDirectorio).Build()
                    ).ToList();
                this.datos = new BindingList<MovimientoBancarioComprobanteDetailModel>(d0);
            } else if(this._TipoComprobante == MovimientoBancarioTipoComprobanteEnum.Remision) {
                var d0 = this.service.GetList<MovimientoBancarioComprobanteDetailModel>(
                    new Domain.Banco.Builder.ComprobanteSearchQueryBuilder().ForRemision().With(this._CurrentConcepto).IdDirectorio(this._IdDirectorio).Build()
                    ).ToList();
                this.datos = new BindingList<MovimientoBancarioComprobanteDetailModel>(d0);
            }
            e.Result = true;
        }

        private void WorkerSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridSearch.DataSource = this.datos;
            this.radWaitingBar1.StopWaiting();
        }
    }
}
