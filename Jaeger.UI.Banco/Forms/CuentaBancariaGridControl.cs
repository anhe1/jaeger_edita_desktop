﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.UI.Banco.Builder;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Banco.Forms {
    public class CuentaBancariaGridControl : GridStandarControl {
        public IBancoCuentaService _Service;
        public BindingList<IBancoCuentaDetailModel> _DataSource;

        public event EventHandler<IBancoCuentaDetailModel> Edicion;
        protected virtual void OnRaiseCustomEvent(IBancoCuentaDetailModel e) {
            EventHandler<IBancoCuentaDetailModel> raiseEvent = Edicion;
            if (raiseEvent != null) {
                raiseEvent(this, e);
            }
        }

        public event EventHandler<IBancoCuentaDetailModel> Eliminar;
        protected virtual void OnEliminar(IBancoCuentaDetailModel e) {
            EventHandler<IBancoCuentaDetailModel> raiseEvent = Eliminar;
            if (raiseEvent != null) {
                raiseEvent(this, e);
            }
        }

        public CuentaBancariaGridControl() : base() {
            this.Editar.Click += Editar_Click;
            this.Remover.Click += Remover_Click;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IGridViewCuentaBancariaBuilder d0 = new GridViewCuentaBancariaBuilder()) {
                this.GridData.Columns.AddRange(d0.Templetes().CuentasBancarias().Build());
            }
        }

        private void Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.GetCurrent<BancoCuentaDetailModel>();
            if (seleccionado != null) {
                if (seleccionado.ReadOnly == true) {
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_CuentaBancaria_NoEditable, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
                if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_CuentaBancariaEliminar, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    using (var espera = new Waiting2Form(this.Desactivar)) {
                        espera.ShowDialog(this);
                        if (seleccionado.Activo == false)
                            this.GridData.Rows.Remove(this.GridData.CurrentRow);
                    }
                }
            }
        }

        private void Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.GetCurrent<BancoCuentaDetailModel>();
            if (seleccionado != null) {
                this.OnRaiseCustomEvent(seleccionado);
            }
        }

        public virtual void Desactivar() {
            var seleccionado = this.GetCurrent<BancoCuentaDetailModel>() as IBancoCuentaDetailModel;
            seleccionado.Activo = !seleccionado.Activo;
            seleccionado = this._Service.Save(seleccionado);
        }
    }
}
