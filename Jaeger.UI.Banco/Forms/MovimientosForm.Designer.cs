﻿
namespace Jaeger.UI.Banco.Forms {
    partial class MovimientosForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TMovimiento = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.Movimientos = new Jaeger.UI.Banco.Forms.MovimientosGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TMovimiento
            // 
            this.TMovimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMovimiento.Location = new System.Drawing.Point(0, 0);
            this.TMovimiento.Name = "TMovimiento";
            this.TMovimiento.ShowActualizar = true;
            this.TMovimiento.ShowAutosuma = true;
            this.TMovimiento.ShowCancelar = true;
            this.TMovimiento.ShowCerrar = true;
            this.TMovimiento.ShowEditar = true;
            this.TMovimiento.ShowEjercicio = true;
            this.TMovimiento.ShowExportarExcel = true;
            this.TMovimiento.ShowFiltro = true;
            this.TMovimiento.ShowHerramientas = true;
            this.TMovimiento.ShowImprimir = true;
            this.TMovimiento.ShowNuevo = true;
            this.TMovimiento.ShowPeriodo = true;
            this.TMovimiento.Size = new System.Drawing.Size(1159, 30);
            this.TMovimiento.TabIndex = 1;
            // 
            // Movimientos
            // 
            this.Movimientos.Auditar = false;
            this.Movimientos.Cancelar = false;
            this.Movimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Movimientos.Location = new System.Drawing.Point(0, 30);
            this.Movimientos.Name = "Movimientos";
            this.Movimientos.Size = new System.Drawing.Size(1159, 523);
            this.Movimientos.Status = false;
            this.Movimientos.TabIndex = 2;
            // 
            // MovimientosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 553);
            this.Controls.Add(this.Movimientos);
            this.Controls.Add(this.TMovimiento);
            this.Name = "MovimientosForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Movimientos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MovimientosForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Common.Forms.ToolBarCommonControl TMovimiento;
        public MovimientosGridControl Movimientos;
    }
}