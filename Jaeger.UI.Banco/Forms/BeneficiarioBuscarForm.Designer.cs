﻿namespace Jaeger.UI.Banco.Forms {
    partial class BeneficiarioBuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BeneficiarioBuscarForm));
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarBuscarControl();
            this.dataGridDirectorio = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDirectorio.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "Nombre:";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowAgregar = false;
            this.ToolBar.ShowBuscar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowExistencia = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.Size = new System.Drawing.Size(579, 32);
            this.ToolBar.TabIndex = 1;
            this.ToolBar.ButtonBuscar_Click += new System.EventHandler<System.EventArgs>(this.Actualizar_Click);
            this.ToolBar.ButtonCerrar_Click += new System.EventHandler<System.EventArgs>(this.Cerrar_Click);
            // 
            // dataGridDirectorio
            // 
            this.dataGridDirectorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridDirectorio.Location = new System.Drawing.Point(0, 32);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "IdDrctr";
            gridViewTextBoxColumn1.HeaderText = "IdDrctr";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdDrctr";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn2.Width = 65;
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.FieldName = "RFC";
            gridViewTextBoxColumn4.HeaderText = "RFC";
            gridViewTextBoxColumn4.Name = "RFC";
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Correo";
            gridViewTextBoxColumn5.HeaderText = "Correo";
            gridViewTextBoxColumn5.Name = "Correo";
            gridViewTextBoxColumn5.Width = 150;
            this.dataGridDirectorio.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.dataGridDirectorio.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridDirectorio.Name = "dataGridDirectorio";
            this.dataGridDirectorio.Size = new System.Drawing.Size(579, 354);
            this.dataGridDirectorio.TabIndex = 2;
            this.dataGridDirectorio.DoubleClick += new System.EventHandler(this.dataGridDirectorio_DoubleClick);
            // 
            // ClienteBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 386);
            this.Controls.Add(this.dataGridDirectorio);
            this.Controls.Add(this.ToolBar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClienteBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar Beneficiario";
            this.Load += new System.EventHandler(this.ClienteBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDirectorio.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarBuscarControl ToolBar;
        private Telerik.WinControls.UI.RadGridView dataGridDirectorio;
    }
}