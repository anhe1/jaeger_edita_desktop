﻿namespace Jaeger.UI.Banco.Forms
{
    partial class CuentaBancariaCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentaBancariaCatalogoForm));
            this.TCuentaB = new Jaeger.UI.Banco.Forms.CuentaBancariaGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCuentaB
            // 
            this.TCuentaB.Caption = "";
            this.TCuentaB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCuentaB.Location = new System.Drawing.Point(0, 0);
            this.TCuentaB.Name = "TCuentaB";
            this.TCuentaB.ShowActualizar = true;
            this.TCuentaB.ShowAutorizar = false;
            this.TCuentaB.ShowAutosuma = false;
            this.TCuentaB.ShowCerrar = true;
            this.TCuentaB.ShowEditar = true;
            this.TCuentaB.ShowExportarExcel = false;
            this.TCuentaB.ShowFiltro = true;
            this.TCuentaB.ShowHerramientas = false;
            this.TCuentaB.ShowImagen = false;
            this.TCuentaB.ShowImprimir = false;
            this.TCuentaB.ShowNuevo = true;
            this.TCuentaB.ShowRemover = true;
            this.TCuentaB.ShowSeleccionMultiple = true;
            this.TCuentaB.Size = new System.Drawing.Size(1116, 505);
            this.TCuentaB.TabIndex = 1;
            // 
            // CuentaBancariaCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 505);
            this.Controls.Add(this.TCuentaB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CuentaBancariaCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bancos: Cuentas Bancarias";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CuentaBancariaCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal CuentaBancariaGridControl TCuentaB;
    }
}
