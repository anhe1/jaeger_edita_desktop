﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.UI.Banco.Forms {
    public partial class FormaPagoForm : RadForm {
        protected IBancoFormaPagoService service;
        private IBancoFormaPagoDetailModel currentFormaPago;
        protected IFormaPagoCatalogo catalogoFormaPago = new FormaPagoCatalogo();

        public FormaPagoForm(IBancoFormaPagoService service) {
            InitializeComponent();
            this.service = service;
        }

        public FormaPagoForm(BancoFormaPagoDetailModel model, IBancoFormaPagoService service) {
            InitializeComponent();
            this.service = service;
            this.currentFormaPago = model;
        }

        private void FormaPagoForm_Load(object sender, EventArgs e) {
            if (this.currentFormaPago == null) {
                this.currentFormaPago = new BancoFormaPagoDetailModel();
            }
            this.catalogoFormaPago.Load();
            this.comboBoxClaveSAT.DataSource = this.catalogoFormaPago.Items;
            this.CreateBinding();
        }

        private void ToolBarButtonNuevo_Click(object sender, EventArgs e) {
            this.currentFormaPago = new BancoFormaPagoDetailModel();
            this.CreateBinding();
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using(var _espera = new Common.Forms.Waiting1Form(this.Guardar)) {
                    _espera.Text = "Guardando ...";
                    _espera.ShowDialog(this);
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.comboBoxClaveSAT.SelectedIndex = -1;
            this.comboBoxClaveSAT.SelectedValue = null;
            this.textBoxClave.DataBindings.Clear();
            this.textBoxClave.DataBindings.Add("Text", this.currentFormaPago, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.comboBoxClaveSAT.DataBindings.Clear();
            this.comboBoxClaveSAT.DataBindings.Add("SelectedValue", this.currentFormaPago, "ClaveSAT", true, DataSourceUpdateMode.OnPropertyChanged);
            this.textBoxDescripcion.DataBindings.Clear();
            this.textBoxDescripcion.DataBindings.Add("Text", this.currentFormaPago, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.checkBoxActivo.DataBindings.Clear();
            this.checkBoxActivo.DataBindings.Add("Checked", this.currentFormaPago, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.AfectarSaldo.DataBindings.Clear();
            this.AfectarSaldo.DataBindings.Add("Checked", this.currentFormaPago, "AfectarSaldo", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Guardar() {
            try {
                this.currentFormaPago = this.service.Save(this.currentFormaPago);
            } catch (Domain.Base.Entities.JaegerException ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private bool Validar() {
            this.errorFormaPago.Clear();
            if (string.IsNullOrEmpty(this.currentFormaPago.Descripcion)) {
                this.errorFormaPago.SetError(this.textBoxDescripcion, "Es necesaria una descripción para la forma de pago");
                return false;
            }

            if (string.IsNullOrEmpty(this.currentFormaPago.Clave)) {
                this.errorFormaPago.SetError(this.textBoxClave, "Es necesario definir una clave personalizada.");
                return false;
            }

            if (string.IsNullOrEmpty(this.currentFormaPago.ClaveSAT)) {
                this.errorFormaPago.SetError(this.comboBoxClaveSAT, "Es necesario definir una clave SAT.");
                return false;
            }
            return true;
        }
    }
}
