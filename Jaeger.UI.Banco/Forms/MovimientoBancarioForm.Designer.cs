﻿using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Banco.Forms {
    partial class MovimientoBancarioForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn1 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewCalculatorColumn gridViewCalculatorColumn2 = new Telerik.WinControls.UI.GridViewCalculatorColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition7 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor2 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition8 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition9 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovimientoBancarioForm));
            this.lblNumCuentaP = new Telerik.WinControls.UI.RadLabel();
            this.NumeroCuentaP = new Telerik.WinControls.UI.RadTextBox();
            this.lblBancoP = new Telerik.WinControls.UI.RadLabel();
            this.lblClabeP = new Telerik.WinControls.UI.RadLabel();
            this.BancoP = new Telerik.WinControls.UI.RadTextBox();
            this.CuentaClabeP = new Telerik.WinControls.UI.RadTextBox();
            this.CuentaBancariaP = new Jaeger.UI.Common.Forms.MultiComboBoxControl();
            this.lblCuenta = new Telerik.WinControls.UI.RadLabel();
            this.ComprobantesPages = new Telerik.WinControls.UI.RadPageView();
            this.PageComprobantes = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridComprobantes = new Telerik.WinControls.UI.RadGridView();
            this.TComprobante = new Jaeger.UI.Banco.Forms.TbMovimientoComprobanteControl();
            this.PageAuxliar = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridAuxiliares = new Telerik.WinControls.UI.RadGridView();
            this.TAuxiliar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PanelCuentaP = new Telerik.WinControls.UI.RadPanel();
            this.ConceptoMovimiento = new Jaeger.UI.Common.Forms.MultiComboBoxControl();
            this.lblFechaRegistro = new Telerik.WinControls.UI.RadLabel();
            this.lblTipoOperacion = new Telerik.WinControls.UI.RadLabel();
            this.FechaRegistro = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblBeneficiarioT = new Telerik.WinControls.UI.RadLabel();
            this.lblRFCT = new Telerik.WinControls.UI.RadLabel();
            this.Beneficiario = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NumDocto = new Telerik.WinControls.UI.RadTextBox();
            this.BeneficiarioRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblSucursalP = new Telerik.WinControls.UI.RadLabel();
            this.Referencia = new Telerik.WinControls.UI.RadTextBox();
            this.lblFechaAplicacion = new Telerik.WinControls.UI.RadLabel();
            this.lblReferencia = new Telerik.WinControls.UI.RadLabel();
            this.lblNumCuentaT = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaDocumento = new Telerik.WinControls.UI.RadLabel();
            this.lblConcepto = new Telerik.WinControls.UI.RadLabel();
            this.lblBancoT = new Telerik.WinControls.UI.RadLabel();
            this.lblNumAutorizacion = new Telerik.WinControls.UI.RadLabel();
            this.NumOperacion = new Telerik.WinControls.UI.RadTextBox();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.SucursalT = new Telerik.WinControls.UI.RadTextBox();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.NumAuto = new Telerik.WinControls.UI.RadTextBox();
            this.Concepto = new Telerik.WinControls.UI.RadDropDownList();
            this.lblNumOperacion = new Telerik.WinControls.UI.RadLabel();
            this.BancoT = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Observaciones = new Telerik.WinControls.UI.RadTextBox();
            this.lblFormaPago = new Telerik.WinControls.UI.RadLabel();
            this.labelImporte = new Telerik.WinControls.UI.RadLabel();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.lblClabeT = new Telerik.WinControls.UI.RadLabel();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NumeroCuentaT = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ClabeT = new Telerik.WinControls.UI.RadTextBox();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.TipoCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.ParaAbono = new Telerik.WinControls.UI.RadCheckBox();
            this.PorComprobar = new Telerik.WinControls.UI.RadCheckBox();
            this.PanelCuentaT = new Telerik.WinControls.UI.RadPanel();
            this.Importe = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.SearchBeneficiario = new Telerik.WinControls.UI.RadButton();
            this.RefreshBeneficiarios = new Telerik.WinControls.UI.RadButton();
            this.FechaAplicacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaDocumento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.TMovimiento = new Jaeger.UI.Banco.Forms.TbMovimientoBancarioControl();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuentaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClabeP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobantesPages)).BeginInit();
            this.ComprobantesPages.SuspendLayout();
            this.PageComprobantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes.MasterTemplate)).BeginInit();
            this.PageAuxliar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliares)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliares.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelCuentaP)).BeginInit();
            this.PanelCuentaP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiarioT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFCT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDocto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSucursalP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuentaT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConcepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumAutorizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SucursalT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClabeT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClabeT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParaAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorComprobar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelCuentaT)).BeginInit();
            this.PanelCuentaT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshBeneficiarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumCuentaP
            // 
            this.lblNumCuentaP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumCuentaP.Location = new System.Drawing.Point(339, 8);
            this.lblNumCuentaP.Name = "lblNumCuentaP";
            this.lblNumCuentaP.Size = new System.Drawing.Size(74, 18);
            this.lblNumCuentaP.TabIndex = 321;
            this.lblNumCuentaP.Text = "Núm. Cuenta:";
            // 
            // NumeroCuentaP
            // 
            this.NumeroCuentaP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumeroCuentaP.Location = new System.Drawing.Point(414, 7);
            this.NumeroCuentaP.Name = "NumeroCuentaP";
            this.NumeroCuentaP.ReadOnly = true;
            this.NumeroCuentaP.Size = new System.Drawing.Size(113, 20);
            this.NumeroCuentaP.TabIndex = 326;
            // 
            // lblBancoP
            // 
            this.lblBancoP.Location = new System.Drawing.Point(7, 33);
            this.lblBancoP.Name = "lblBancoP";
            this.lblBancoP.Size = new System.Drawing.Size(39, 18);
            this.lblBancoP.TabIndex = 322;
            this.lblBancoP.Text = "Banco:";
            // 
            // lblClabeP
            // 
            this.lblClabeP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblClabeP.Location = new System.Drawing.Point(339, 33);
            this.lblClabeP.Name = "lblClabeP";
            this.lblClabeP.Size = new System.Drawing.Size(37, 18);
            this.lblClabeP.TabIndex = 323;
            this.lblClabeP.Text = "Clabe:";
            // 
            // BancoP
            // 
            this.BancoP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BancoP.Location = new System.Drawing.Point(57, 32);
            this.BancoP.Name = "BancoP";
            this.BancoP.NullText = "Banco";
            this.BancoP.ReadOnly = true;
            this.BancoP.Size = new System.Drawing.Size(279, 20);
            this.BancoP.TabIndex = 324;
            // 
            // CuentaClabeP
            // 
            this.CuentaClabeP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CuentaClabeP.Location = new System.Drawing.Point(387, 32);
            this.CuentaClabeP.Name = "CuentaClabeP";
            this.CuentaClabeP.NullText = "Clabe";
            this.CuentaClabeP.ReadOnly = true;
            this.CuentaClabeP.Size = new System.Drawing.Size(140, 20);
            this.CuentaClabeP.TabIndex = 325;
            this.CuentaClabeP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CuentaBancariaP
            // 
            this.CuentaBancariaP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CuentaBancariaP.AutoSizeDropDownToBestFit = true;
            this.CuentaBancariaP.DisplayMember = "Alias";
            this.CuentaBancariaP.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // CuentaBancariaP.NestedRadGridView
            // 
            this.CuentaBancariaP.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CuentaBancariaP.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CuentaBancariaP.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CuentaBancariaP.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CuentaBancariaP.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CuentaBancariaP.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CuentaBancariaP.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CuentaBancariaP.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdCuenta";
            gridViewTextBoxColumn1.HeaderText = "IdCuenta";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdCuenta";
            gridViewTextBoxColumn2.FieldName = "NumCuenta";
            gridViewTextBoxColumn2.HeaderText = "Núm. de Cuenta";
            gridViewTextBoxColumn2.Name = "NumCuenta";
            gridViewTextBoxColumn3.FieldName = "Beneficiario";
            gridViewTextBoxColumn3.HeaderText = "Beneficiario";
            gridViewTextBoxColumn3.Name = "Beneficiario";
            gridViewTextBoxColumn3.Width = 120;
            gridViewTextBoxColumn4.FieldName = "Alias";
            gridViewTextBoxColumn4.HeaderText = "Alias";
            gridViewTextBoxColumn4.Name = "Alias";
            gridViewTextBoxColumn4.Width = 95;
            this.CuentaBancariaP.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.CuentaBancariaP.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CuentaBancariaP.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CuentaBancariaP.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CuentaBancariaP.EditorControl.Name = "NestedRadGridView";
            this.CuentaBancariaP.EditorControl.ReadOnly = true;
            this.CuentaBancariaP.EditorControl.ShowGroupPanel = false;
            this.CuentaBancariaP.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CuentaBancariaP.EditorControl.TabIndex = 0;
            this.CuentaBancariaP.Location = new System.Drawing.Point(57, 7);
            this.CuentaBancariaP.Name = "CuentaBancariaP";
            this.CuentaBancariaP.NullText = "Selecciona";
            this.CuentaBancariaP.ReadOnly = false;
            this.CuentaBancariaP.Size = new System.Drawing.Size(279, 20);
            this.CuentaBancariaP.TabIndex = 21;
            this.CuentaBancariaP.TabStop = false;
            this.CuentaBancariaP.ValueMember = "IdCuenta";
            // 
            // lblCuenta
            // 
            this.lblCuenta.Location = new System.Drawing.Point(7, 8);
            this.lblCuenta.Name = "lblCuenta";
            this.lblCuenta.Size = new System.Drawing.Size(44, 18);
            this.lblCuenta.TabIndex = 22;
            this.lblCuenta.Text = "Cuenta:";
            // 
            // ComprobantesPages
            // 
            this.ComprobantesPages.Controls.Add(this.PageComprobantes);
            this.ComprobantesPages.Controls.Add(this.PageAuxliar);
            this.ComprobantesPages.DefaultPage = this.PageComprobantes;
            this.ComprobantesPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComprobantesPages.Location = new System.Drawing.Point(0, 286);
            this.ComprobantesPages.Name = "ComprobantesPages";
            this.ComprobantesPages.SelectedPage = this.PageComprobantes;
            this.ComprobantesPages.Size = new System.Drawing.Size(865, 257);
            this.ComprobantesPages.TabIndex = 32;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobantesPages.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.ComprobantesPages.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Bottom;
            // 
            // PageComprobantes
            // 
            this.PageComprobantes.Controls.Add(this.GridComprobantes);
            this.PageComprobantes.Controls.Add(this.TComprobante);
            this.PageComprobantes.ItemSize = new System.Drawing.SizeF(90F, 28F);
            this.PageComprobantes.Location = new System.Drawing.Point(10, 10);
            this.PageComprobantes.Name = "PageComprobantes";
            this.PageComprobantes.Size = new System.Drawing.Size(844, 209);
            this.PageComprobantes.Text = "Comprobantes";
            // 
            // GridComprobantes
            // 
            this.GridComprobantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridComprobantes.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridComprobantes.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.GridComprobantes.MasterTemplate.AllowAddNewRow = false;
            this.GridComprobantes.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn5.FieldName = "Id";
            gridViewTextBoxColumn5.HeaderText = "Index";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Id";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.ReadOnly = true;
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn6.FieldName = "TipoDocumentoText";
            gridViewTextBoxColumn6.HeaderText = "Tipo";
            gridViewTextBoxColumn6.Name = "TipoDocumentoText";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.VisibleInColumnChooser = false;
            gridViewTextBoxColumn6.Width = 65;
            gridViewMultiComboBoxColumn1.FieldName = "Folio";
            gridViewMultiComboBoxColumn1.HeaderText = "Folio";
            gridViewMultiComboBoxColumn1.Name = "Folio";
            gridViewMultiComboBoxColumn1.ReadOnly = true;
            gridViewMultiComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn7.FieldName = "Serie";
            gridViewTextBoxColumn7.HeaderText = "Serie";
            gridViewTextBoxColumn7.Name = "Serie";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn8.FieldName = "EmisorRFC";
            gridViewTextBoxColumn8.HeaderText = "RFC";
            gridViewTextBoxColumn8.Name = "EmisorRFC";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn9.FieldName = "EmisorNombre";
            gridViewTextBoxColumn9.HeaderText = "Emisor";
            gridViewTextBoxColumn9.Name = "EmisorNombre";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 190;
            gridViewTextBoxColumn10.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn10.HeaderText = "RFC";
            gridViewTextBoxColumn10.Name = "ReceptorRFC";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn11.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn11.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn11.Name = "ReceptorNombre";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 190;
            gridViewTextBoxColumn12.FieldName = "IdDocumento";
            gridViewTextBoxColumn12.HeaderText = "UUID";
            gridViewTextBoxColumn12.Name = "IdDocumento";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.Width = 155;
            gridViewTextBoxColumn13.FieldName = "SubTotal";
            gridViewTextBoxColumn13.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn13.HeaderText = "SubTotal";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "SubTotal";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.DataType = typeof(double);
            gridViewTextBoxColumn14.FieldName = "TrasladoIva";
            gridViewTextBoxColumn14.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn14.HeaderText = "Iva";
            gridViewTextBoxColumn14.IsVisible = false;
            gridViewTextBoxColumn14.Name = "Iva";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 80;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "(Acumulado+Cargo+Abono) > Total";
            expressionFormattingObject1.Name = "Totales";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewTextBoxColumn15.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn15.FieldName = "Total";
            gridViewTextBoxColumn15.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn15.HeaderText = "Total";
            gridViewTextBoxColumn15.Name = "Total";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn15.Width = 85;
            gridViewTextBoxColumn16.FieldName = "PorCobrar";
            gridViewTextBoxColumn16.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn16.HeaderText = "Por Cobrar";
            gridViewTextBoxColumn16.Name = "PorCobrar";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.DataType = typeof(decimal);
            gridViewTextBoxColumn17.FieldName = "Acumulado";
            gridViewTextBoxColumn17.FormatString = "{0:#,###0.00}";
            gridViewTextBoxColumn17.HeaderText = "Acumulado";
            gridViewTextBoxColumn17.Name = "Acumulado";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn17.Width = 85;
            gridViewCalculatorColumn1.FieldName = "Cargo";
            gridViewCalculatorColumn1.FormatString = "{0:#,###0.00}";
            gridViewCalculatorColumn1.HeaderText = "Cargo";
            gridViewCalculatorColumn1.Name = "Cargo";
            gridViewCalculatorColumn1.Width = 85;
            gridViewCalculatorColumn2.FieldName = "Abono";
            gridViewCalculatorColumn2.FormatString = "{0:#,###0.00}";
            gridViewCalculatorColumn2.HeaderText = "Abono";
            gridViewCalculatorColumn2.Name = "Abono";
            gridViewCalculatorColumn2.Width = 85;
            gridViewTextBoxColumn18.FieldName = "Estado";
            gridViewTextBoxColumn18.HeaderText = "Estado";
            gridViewTextBoxColumn18.Name = "Estado";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.Width = 80;
            gridViewTextBoxColumn19.FieldName = "TipoComprobanteText";
            gridViewTextBoxColumn19.HeaderText = "Tipo Comprobante";
            gridViewTextBoxColumn19.Name = "TipoComprobanteText";
            gridViewTextBoxColumn19.ReadOnly = true;
            this.GridComprobantes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn6,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewCalculatorColumn1,
            gridViewCalculatorColumn2,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.GridComprobantes.MasterTemplate.EnableAlternatingRowColor = true;
            this.GridComprobantes.MasterTemplate.EnableGrouping = false;
            this.GridComprobantes.MasterTemplate.ShowFilteringRow = false;
            this.GridComprobantes.MasterTemplate.ShowRowHeaderColumn = false;
            this.GridComprobantes.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.GridComprobantes.Name = "GridComprobantes";
            this.GridComprobantes.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.EnterMovesToNextCell;
            this.GridComprobantes.ShowGroupPanel = false;
            this.GridComprobantes.Size = new System.Drawing.Size(844, 179);
            this.GridComprobantes.TabIndex = 1;
            this.GridComprobantes.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.Comprobantes_ContextMenuOpening);
            // 
            // TComprobante
            // 
            this.TComprobante.Dock = System.Windows.Forms.DockStyle.Top;
            this.TComprobante.Location = new System.Drawing.Point(0, 0);
            this.TComprobante.Name = "TComprobante";
            this.TComprobante.ReadOnly = false;
            this.TComprobante.Size = new System.Drawing.Size(844, 30);
            this.TComprobante.TabIndex = 2;
            // 
            // PageAuxliar
            // 
            this.PageAuxliar.Controls.Add(this.GridAuxiliares);
            this.PageAuxliar.Controls.Add(this.TAuxiliar);
            this.PageAuxliar.ItemSize = new System.Drawing.SizeF(53F, 28F);
            this.PageAuxliar.Location = new System.Drawing.Point(10, 10);
            this.PageAuxliar.Name = "PageAuxliar";
            this.PageAuxliar.Size = new System.Drawing.Size(844, 211);
            this.PageAuxliar.Text = "Auxiliar";
            // 
            // GridAuxiliares
            // 
            this.GridAuxiliares.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridAuxiliares.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridAuxiliares.MasterTemplate.AllowAddNewRow = false;
            this.GridAuxiliares.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn20.FieldName = "Descripcion";
            gridViewTextBoxColumn20.HeaderText = "Descripción";
            gridViewTextBoxColumn20.Name = "Descripcion";
            gridViewTextBoxColumn20.Width = 480;
            gridViewTextBoxColumn21.FieldName = "Tipo";
            gridViewTextBoxColumn21.HeaderText = "Tipo";
            gridViewTextBoxColumn21.Name = "Tipo";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.Width = 90;
            gridViewCommandColumn1.FieldName = "Button";
            gridViewCommandColumn1.HeaderText = "Documento";
            gridViewCommandColumn1.Name = "Button";
            gridViewCommandColumn1.Width = 85;
            this.GridAuxiliares.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewCommandColumn1});
            this.GridAuxiliares.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.GridAuxiliares.Name = "GridAuxiliares";
            this.GridAuxiliares.ShowGroupPanel = false;
            this.GridAuxiliares.Size = new System.Drawing.Size(844, 181);
            this.GridAuxiliares.TabIndex = 2;
            this.GridAuxiliares.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.RadGridAuxiliares_CellBeginEdit);
            this.GridAuxiliares.CommandCellClick += new Telerik.WinControls.UI.CommandCellClickEventHandler(this.RadGridAuxiliares_CommandCellClick);
            // 
            // TAuxiliar
            // 
            this.TAuxiliar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TAuxiliar.Etiqueta = "";
            this.TAuxiliar.Location = new System.Drawing.Point(0, 0);
            this.TAuxiliar.Name = "TAuxiliar";
            this.TAuxiliar.ReadOnly = false;
            this.TAuxiliar.ShowActualizar = false;
            this.TAuxiliar.ShowAutorizar = false;
            this.TAuxiliar.ShowCerrar = false;
            this.TAuxiliar.ShowEditar = false;
            this.TAuxiliar.ShowExportarExcel = false;
            this.TAuxiliar.ShowFiltro = false;
            this.TAuxiliar.ShowGuardar = false;
            this.TAuxiliar.ShowHerramientas = false;
            this.TAuxiliar.ShowImagen = false;
            this.TAuxiliar.ShowImprimir = false;
            this.TAuxiliar.ShowNuevo = true;
            this.TAuxiliar.ShowRemover = true;
            this.TAuxiliar.Size = new System.Drawing.Size(844, 30);
            this.TAuxiliar.TabIndex = 3;
            // 
            // PanelCuentaP
            // 
            this.PanelCuentaP.Controls.Add(this.ConceptoMovimiento);
            this.PanelCuentaP.Controls.Add(this.lblFechaRegistro);
            this.PanelCuentaP.Controls.Add(this.lblTipoOperacion);
            this.PanelCuentaP.Controls.Add(this.FechaRegistro);
            this.PanelCuentaP.Controls.Add(this.lblNumCuentaP);
            this.PanelCuentaP.Controls.Add(this.CuentaBancariaP);
            this.PanelCuentaP.Controls.Add(this.NumeroCuentaP);
            this.PanelCuentaP.Controls.Add(this.lblCuenta);
            this.PanelCuentaP.Controls.Add(this.lblBancoP);
            this.PanelCuentaP.Controls.Add(this.CuentaClabeP);
            this.PanelCuentaP.Controls.Add(this.lblClabeP);
            this.PanelCuentaP.Controls.Add(this.BancoP);
            this.PanelCuentaP.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCuentaP.Location = new System.Drawing.Point(0, 30);
            this.PanelCuentaP.Name = "PanelCuentaP";
            this.PanelCuentaP.Size = new System.Drawing.Size(865, 63);
            this.PanelCuentaP.TabIndex = 33;
            // 
            // ConceptoMovimiento
            // 
            this.ConceptoMovimiento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ConceptoMovimiento.DisplayMember = "Descripcion";
            // 
            // ConceptoMovimiento.NestedRadGridView
            // 
            this.ConceptoMovimiento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ConceptoMovimiento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConceptoMovimiento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ConceptoMovimiento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn22.DataType = typeof(int);
            gridViewTextBoxColumn22.FieldName = "IdConcepto";
            gridViewTextBoxColumn22.HeaderText = "Id";
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "Id";
            gridViewTextBoxColumn23.FieldName = "Descripcion";
            gridViewTextBoxColumn23.HeaderText = "Descripción";
            gridViewTextBoxColumn23.Name = "Descripcion";
            this.ConceptoMovimiento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23});
            this.ConceptoMovimiento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.ConceptoMovimiento.EditorControl.Name = "NestedRadGridView";
            this.ConceptoMovimiento.EditorControl.ReadOnly = true;
            this.ConceptoMovimiento.EditorControl.ShowGroupPanel = false;
            this.ConceptoMovimiento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ConceptoMovimiento.EditorControl.TabIndex = 0;
            this.ConceptoMovimiento.Location = new System.Drawing.Point(550, 32);
            this.ConceptoMovimiento.Name = "ConceptoMovimiento";
            this.ConceptoMovimiento.NullText = "Documento";
            this.ConceptoMovimiento.ReadOnly = false;
            this.ConceptoMovimiento.Size = new System.Drawing.Size(201, 20);
            this.ConceptoMovimiento.TabIndex = 331;
            this.ConceptoMovimiento.TabStop = false;
            this.ConceptoMovimiento.ValueMember = "IdConcepto";
            this.ConceptoMovimiento.DropDownClosed += new Telerik.WinControls.UI.RadPopupClosedEventHandler(this.ConceptoMovimiento_DropDownClosed);
            this.ConceptoMovimiento.TextChanged += new System.EventHandler(this.ConceptoMovimiento_TextChanged);
            // 
            // lblFechaRegistro
            // 
            this.lblFechaRegistro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaRegistro.Location = new System.Drawing.Point(759, 8);
            this.lblFechaRegistro.Name = "lblFechaRegistro";
            this.lblFechaRegistro.Size = new System.Drawing.Size(53, 18);
            this.lblFechaRegistro.TabIndex = 330;
            this.lblFechaRegistro.Text = "Fec. Reg.:";
            // 
            // lblTipoOperacion
            // 
            this.lblTipoOperacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoOperacion.Location = new System.Drawing.Point(550, 8);
            this.lblTipoOperacion.Name = "lblTipoOperacion";
            this.lblTipoOperacion.Size = new System.Drawing.Size(110, 18);
            this.lblTipoOperacion.TabIndex = 329;
            this.lblTipoOperacion.Text = "Tipo de Movimiento:";
            // 
            // FechaRegistro
            // 
            this.FechaRegistro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaRegistro.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaRegistro.Location = new System.Drawing.Point(759, 32);
            this.FechaRegistro.Name = "FechaRegistro";
            this.FechaRegistro.Size = new System.Drawing.Size(90, 20);
            this.FechaRegistro.TabIndex = 327;
            this.FechaRegistro.TabStop = false;
            this.FechaRegistro.Text = "15/12/2016";
            this.FechaRegistro.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // lblBeneficiarioT
            // 
            this.lblBeneficiarioT.Location = new System.Drawing.Point(4, 33);
            this.lblBeneficiarioT.Name = "lblBeneficiarioT";
            this.lblBeneficiarioT.Size = new System.Drawing.Size(67, 18);
            this.lblBeneficiarioT.TabIndex = 347;
            this.lblBeneficiarioT.Text = "Beneficiario:";
            // 
            // lblRFCT
            // 
            this.lblRFCT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRFCT.Location = new System.Drawing.Point(700, 33);
            this.lblRFCT.Name = "lblRFCT";
            this.lblRFCT.Size = new System.Drawing.Size(28, 18);
            this.lblRFCT.TabIndex = 348;
            this.lblRFCT.Text = "RFC:";
            // 
            // Beneficiario
            // 
            this.Beneficiario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Beneficiario.DisplayMember = "Nombre";
            // 
            // Beneficiario.NestedRadGridView
            // 
            this.Beneficiario.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Beneficiario.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Beneficiario.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Beneficiario.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Beneficiario.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Beneficiario.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Beneficiario.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Beneficiario.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn24.FieldName = "Id";
            gridViewTextBoxColumn24.HeaderText = "Id";
            gridViewTextBoxColumn24.IsVisible = false;
            gridViewTextBoxColumn24.Name = "Id";
            gridViewTextBoxColumn25.FieldName = "Nombre";
            gridViewTextBoxColumn25.HeaderText = "Identidad Fiscal";
            gridViewTextBoxColumn25.Name = "Nombre";
            gridViewTextBoxColumn25.Width = 200;
            gridViewTextBoxColumn26.FieldName = "RFC";
            gridViewTextBoxColumn26.HeaderText = "RFC";
            gridViewTextBoxColumn26.Name = "RFC";
            gridViewTextBoxColumn26.Width = 85;
            gridViewTextBoxColumn27.FieldName = "Clave";
            gridViewTextBoxColumn27.HeaderText = "Clave";
            gridViewTextBoxColumn27.Name = "Clave";
            this.Beneficiario.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27});
            this.Beneficiario.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Beneficiario.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Beneficiario.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Beneficiario.EditorControl.Name = "NestedRadGridView";
            this.Beneficiario.EditorControl.ReadOnly = true;
            this.Beneficiario.EditorControl.ShowGroupPanel = false;
            this.Beneficiario.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Beneficiario.EditorControl.TabIndex = 0;
            this.Beneficiario.Location = new System.Drawing.Point(84, 32);
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.NullText = "Beneficiario";
            this.Beneficiario.Size = new System.Drawing.Size(536, 20);
            this.Beneficiario.TabIndex = 350;
            this.Beneficiario.TabStop = false;
            this.Beneficiario.ValueMember = "Id";
            // 
            // NumDocto
            // 
            this.NumDocto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumDocto.Location = new System.Drawing.Point(380, 6);
            this.NumDocto.Name = "NumDocto";
            this.NumDocto.NullText = "Folio";
            this.NumDocto.Size = new System.Drawing.Size(104, 20);
            this.NumDocto.TabIndex = 296;
            this.NumDocto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BeneficiarioRFC
            // 
            this.BeneficiarioRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BeneficiarioRFC.Location = new System.Drawing.Point(734, 32);
            this.BeneficiarioRFC.Name = "BeneficiarioRFC";
            this.BeneficiarioRFC.NullText = "RFC";
            this.BeneficiarioRFC.ReadOnly = true;
            this.BeneficiarioRFC.Size = new System.Drawing.Size(115, 20);
            this.BeneficiarioRFC.TabIndex = 349;
            this.BeneficiarioRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSucursalP
            // 
            this.lblSucursalP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSucursalP.Location = new System.Drawing.Point(518, 59);
            this.lblSucursalP.Name = "lblSucursalP";
            this.lblSucursalP.Size = new System.Drawing.Size(50, 18);
            this.lblSucursalP.TabIndex = 352;
            this.lblSucursalP.Text = "Sucursal:";
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(71, 110);
            this.Referencia.MaxLength = 24;
            this.Referencia.Name = "Referencia";
            this.Referencia.NullText = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(169, 20);
            this.Referencia.TabIndex = 336;
            // 
            // lblFechaAplicacion
            // 
            this.lblFechaAplicacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaAplicacion.Location = new System.Drawing.Point(673, 7);
            this.lblFechaAplicacion.Name = "lblFechaAplicacion";
            this.lblFechaAplicacion.Size = new System.Drawing.Size(82, 18);
            this.lblFechaAplicacion.TabIndex = 294;
            this.lblFechaAplicacion.Text = "Fec. Aplicación:";
            // 
            // lblReferencia
            // 
            this.lblReferencia.Location = new System.Drawing.Point(4, 111);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(61, 18);
            this.lblReferencia.TabIndex = 332;
            this.lblReferencia.Text = "Referencia:";
            // 
            // lblNumCuentaT
            // 
            this.lblNumCuentaT.Location = new System.Drawing.Point(4, 59);
            this.lblNumCuentaT.Name = "lblNumCuentaT";
            this.lblNumCuentaT.Size = new System.Drawing.Size(74, 18);
            this.lblNumCuentaT.TabIndex = 354;
            this.lblNumCuentaT.Text = "Núm. Cuenta:";
            // 
            // lblFechaDocumento
            // 
            this.lblFechaDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaDocumento.Location = new System.Drawing.Point(509, 7);
            this.lblFechaDocumento.Name = "lblFechaDocumento";
            this.lblFechaDocumento.Size = new System.Drawing.Size(64, 18);
            this.lblFechaDocumento.TabIndex = 293;
            this.lblFechaDocumento.Text = "Fec. Docto.:";
            // 
            // lblConcepto
            // 
            this.lblConcepto.Location = new System.Drawing.Point(4, 85);
            this.lblConcepto.Name = "lblConcepto";
            this.lblConcepto.Size = new System.Drawing.Size(57, 18);
            this.lblConcepto.TabIndex = 334;
            this.lblConcepto.Text = "Concepto:";
            // 
            // lblBancoT
            // 
            this.lblBancoT.Location = new System.Drawing.Point(253, 59);
            this.lblBancoT.Name = "lblBancoT";
            this.lblBancoT.Size = new System.Drawing.Size(39, 18);
            this.lblBancoT.TabIndex = 351;
            this.lblBancoT.Text = "Banco:";
            // 
            // lblNumAutorizacion
            // 
            this.lblNumAutorizacion.Location = new System.Drawing.Point(248, 111);
            this.lblNumAutorizacion.Name = "lblNumAutorizacion";
            this.lblNumAutorizacion.Size = new System.Drawing.Size(102, 18);
            this.lblNumAutorizacion.TabIndex = 333;
            this.lblNumAutorizacion.Text = "Núm. Autorización:";
            // 
            // NumOperacion
            // 
            this.NumOperacion.Location = new System.Drawing.Point(571, 110);
            this.NumOperacion.MaxLength = 20;
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.NullText = "Núm. Operación";
            this.NumOperacion.Size = new System.Drawing.Size(95, 20);
            this.NumOperacion.TabIndex = 350;
            // 
            // lblNota
            // 
            this.lblNota.Location = new System.Drawing.Point(4, 137);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(81, 18);
            this.lblNota.TabIndex = 335;
            this.lblNota.Text = "Observaciones:";
            // 
            // SucursalT
            // 
            this.SucursalT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SucursalT.Location = new System.Drawing.Point(571, 58);
            this.SucursalT.Name = "SucursalT";
            this.SucursalT.NullText = "Sucursal";
            this.SucursalT.ReadOnly = true;
            this.SucursalT.Size = new System.Drawing.Size(96, 20);
            this.SucursalT.TabIndex = 355;
            this.SucursalT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFolio
            // 
            this.lblFolio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFolio.Location = new System.Drawing.Point(341, 7);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 292;
            this.lblFolio.Text = "Folio:";
            // 
            // NumAuto
            // 
            this.NumAuto.Location = new System.Drawing.Point(356, 110);
            this.NumAuto.MaxLength = 24;
            this.NumAuto.Name = "NumAuto";
            this.NumAuto.NullText = "Núm. Autorización";
            this.NumAuto.Size = new System.Drawing.Size(112, 20);
            this.NumAuto.TabIndex = 337;
            // 
            // Concepto
            // 
            this.Concepto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Concepto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Concepto.AutoSize = false;
            this.Concepto.DropDownAnimationEnabled = false;
            this.Concepto.Location = new System.Drawing.Point(67, 84);
            this.Concepto.Name = "Concepto";
            this.Concepto.NullText = "Concepto";
            this.Concepto.ShowImageInEditorArea = false;
            this.Concepto.Size = new System.Drawing.Size(600, 20);
            this.Concepto.TabIndex = 338;
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.Concepto.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // lblNumOperacion
            // 
            this.lblNumOperacion.Location = new System.Drawing.Point(474, 111);
            this.lblNumOperacion.Name = "lblNumOperacion";
            this.lblNumOperacion.Size = new System.Drawing.Size(91, 18);
            this.lblNumOperacion.TabIndex = 349;
            this.lblNumOperacion.Text = "Núm. Operación:";
            // 
            // BancoT
            // 
            this.BancoT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BancoT.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // BancoT.NestedRadGridView
            // 
            this.BancoT.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.BancoT.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BancoT.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BancoT.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.BancoT.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.BancoT.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.BancoT.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn28.FieldName = "Clave";
            gridViewTextBoxColumn28.HeaderText = "Clave";
            gridViewTextBoxColumn28.Name = "Clave";
            gridViewTextBoxColumn29.FieldName = "Descripcion";
            gridViewTextBoxColumn29.HeaderText = "Nombre Corto";
            gridViewTextBoxColumn29.Name = "Descripcion";
            gridViewTextBoxColumn29.Width = 140;
            gridViewTextBoxColumn30.FieldName = "RazonSocial";
            gridViewTextBoxColumn30.HeaderText = "Institución";
            gridViewTextBoxColumn30.Name = "RazonSocial";
            gridViewTextBoxColumn30.Width = 220;
            this.BancoT.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30});
            this.BancoT.EditorControl.MasterTemplate.EnableGrouping = false;
            this.BancoT.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.BancoT.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.BancoT.EditorControl.Name = "NestedRadGridView";
            this.BancoT.EditorControl.ReadOnly = true;
            this.BancoT.EditorControl.ShowGroupPanel = false;
            this.BancoT.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.BancoT.EditorControl.TabIndex = 0;
            this.BancoT.Location = new System.Drawing.Point(298, 58);
            this.BancoT.Name = "BancoT";
            this.BancoT.NullText = "Catálogo de Bancos";
            this.BancoT.Size = new System.Drawing.Size(214, 20);
            this.BancoT.TabIndex = 357;
            this.BancoT.TabStop = false;
            // 
            // Observaciones
            // 
            this.Observaciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Observaciones.Location = new System.Drawing.Point(91, 136);
            this.Observaciones.MaxLength = 64;
            this.Observaciones.Name = "Observaciones";
            this.Observaciones.NullText = "Observaciones";
            this.Observaciones.Size = new System.Drawing.Size(576, 20);
            this.Observaciones.TabIndex = 339;
            // 
            // lblFormaPago
            // 
            this.lblFormaPago.Location = new System.Drawing.Point(4, 7);
            this.lblFormaPago.Name = "lblFormaPago";
            this.lblFormaPago.Size = new System.Drawing.Size(85, 18);
            this.lblFormaPago.TabIndex = 291;
            this.lblFormaPago.Text = "Forma de pago:";
            // 
            // labelImporte
            // 
            this.labelImporte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelImporte.Location = new System.Drawing.Point(684, 162);
            this.labelImporte.Name = "labelImporte";
            this.labelImporte.Size = new System.Drawing.Size(48, 18);
            this.labelImporte.TabIndex = 340;
            this.labelImporte.Text = "Importe:";
            // 
            // FormaPago
            // 
            this.FormaPago.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DisplayMember = "Descripcion";
            this.FormaPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowEditRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn31.FieldName = "Clave";
            gridViewTextBoxColumn31.HeaderText = "Clave";
            gridViewTextBoxColumn31.Name = "Clave";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn32.FieldName = "Descripcion";
            gridViewTextBoxColumn32.HeaderText = "Método";
            gridViewTextBoxColumn32.Name = "Descripcion";
            gridViewTextBoxColumn32.Width = 150;
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition7;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Location = new System.Drawing.Point(95, 6);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(241, 20);
            this.FormaPago.TabIndex = 295;
            this.FormaPago.TabStop = false;
            this.FormaPago.ValueMember = "Clave";
            this.FormaPago.SelectedIndexChanged += new System.EventHandler(this.FormaPago_SelectedIndexChanged);
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTipoCambio.Location = new System.Drawing.Point(684, 110);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(74, 18);
            this.lblTipoCambio.TabIndex = 343;
            this.lblTipoCambio.Text = "T. de Cambio:";
            // 
            // lblClabeT
            // 
            this.lblClabeT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblClabeT.Location = new System.Drawing.Point(695, 59);
            this.lblClabeT.Name = "lblClabeT";
            this.lblClabeT.Size = new System.Drawing.Size(37, 18);
            this.lblClabeT.TabIndex = 353;
            this.lblClabeT.Text = "Clabe:";
            // 
            // Moneda
            // 
            this.Moneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Moneda.AutoFilter = true;
            this.Moneda.AutoSizeDropDownToBestFit = true;
            this.Moneda.DisplayMember = "Clave";
            this.Moneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Moneda.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn33.FieldName = "Clave";
            gridViewTextBoxColumn33.HeaderText = "Clave";
            gridViewTextBoxColumn33.Name = "Clave";
            gridViewTextBoxColumn34.FieldName = "Descripcion";
            gridViewTextBoxColumn34.HeaderText = "Descripción";
            gridViewTextBoxColumn34.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34});
            this.Moneda.EditorControl.MasterTemplate.EnableFiltering = true;
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            filterDescriptor1.PropertyName = "Clave";
            filterDescriptor2.PropertyName = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1,
            filterDescriptor2});
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition8;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(740, 135);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(109, 20);
            this.Moneda.TabIndex = 345;
            this.Moneda.TabStop = false;
            this.Moneda.ValueMember = "Clave";
            // 
            // NumeroCuentaT
            // 
            this.NumeroCuentaT.AutoSizeDropDownHeight = true;
            this.NumeroCuentaT.AutoSizeDropDownToBestFit = true;
            this.NumeroCuentaT.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // NumeroCuentaT.NestedRadGridView
            // 
            this.NumeroCuentaT.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.NumeroCuentaT.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumeroCuentaT.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.NumeroCuentaT.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.NumeroCuentaT.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.NumeroCuentaT.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.NumeroCuentaT.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn35.FieldName = "NumeroDeCuenta";
            gridViewTextBoxColumn35.HeaderText = "Numero de Cuenta";
            gridViewTextBoxColumn35.Name = "NumeroDeCuenta";
            gridViewTextBoxColumn36.FieldName = "InsitucionBancaria";
            gridViewTextBoxColumn36.HeaderText = "Banco";
            gridViewTextBoxColumn36.Name = "Banco";
            gridViewTextBoxColumn37.FieldName = "Clabe";
            gridViewTextBoxColumn37.HeaderText = "CLABE";
            gridViewTextBoxColumn37.Name = "Clabe";
            gridViewTextBoxColumn37.Width = 90;
            gridViewTextBoxColumn38.FieldName = "Alias";
            gridViewTextBoxColumn38.HeaderText = "Alias";
            gridViewTextBoxColumn38.Name = "Alias";
            this.NumeroCuentaT.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38});
            this.NumeroCuentaT.EditorControl.MasterTemplate.EnableGrouping = false;
            this.NumeroCuentaT.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.NumeroCuentaT.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition9;
            this.NumeroCuentaT.EditorControl.Name = "NestedRadGridView";
            this.NumeroCuentaT.EditorControl.ReadOnly = true;
            this.NumeroCuentaT.EditorControl.ShowGroupPanel = false;
            this.NumeroCuentaT.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.NumeroCuentaT.EditorControl.TabIndex = 0;
            this.NumeroCuentaT.Location = new System.Drawing.Point(84, 58);
            this.NumeroCuentaT.Name = "NumeroCuentaT";
            this.NumeroCuentaT.NullText = "Selecciona";
            this.NumeroCuentaT.Size = new System.Drawing.Size(163, 20);
            this.NumeroCuentaT.TabIndex = 358;
            this.NumeroCuentaT.TabStop = false;
            // 
            // ClabeT
            // 
            this.ClabeT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClabeT.Location = new System.Drawing.Point(734, 58);
            this.ClabeT.Name = "ClabeT";
            this.ClabeT.NullText = "Clabe";
            this.ClabeT.ReadOnly = true;
            this.ClabeT.Size = new System.Drawing.Size(115, 20);
            this.ClabeT.TabIndex = 356;
            this.ClabeT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblMoneda
            // 
            this.lblMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneda.Location = new System.Drawing.Point(684, 136);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 346;
            this.lblMoneda.Text = "Moneda:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCambio.Location = new System.Drawing.Point(764, 109);
            this.TipoCambio.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.NullText = "Importe";
            this.TipoCambio.Size = new System.Drawing.Size(85, 20);
            this.TipoCambio.TabIndex = 347;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.Text = "0";
            this.TipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ParaAbono
            // 
            this.ParaAbono.Location = new System.Drawing.Point(222, 155);
            this.ParaAbono.Name = "ParaAbono";
            this.ParaAbono.Size = new System.Drawing.Size(131, 33);
            this.ParaAbono.TabIndex = 369;
            this.ParaAbono.Text = "Para abono a cuenta \r\ndel beneficiario";
            this.ParaAbono.Visible = false;
            // 
            // PorComprobar
            // 
            this.PorComprobar.Location = new System.Drawing.Point(98, 162);
            this.PorComprobar.Name = "PorComprobar";
            this.PorComprobar.Size = new System.Drawing.Size(82, 18);
            this.PorComprobar.TabIndex = 370;
            this.PorComprobar.Text = "Por justificar";
            // 
            // PanelCuentaT
            // 
            this.PanelCuentaT.Controls.Add(this.Importe);
            this.PanelCuentaT.Controls.Add(this.SearchBeneficiario);
            this.PanelCuentaT.Controls.Add(this.RefreshBeneficiarios);
            this.PanelCuentaT.Controls.Add(this.PorComprobar);
            this.PanelCuentaT.Controls.Add(this.ParaAbono);
            this.PanelCuentaT.Controls.Add(this.TipoCambio);
            this.PanelCuentaT.Controls.Add(this.lblMoneda);
            this.PanelCuentaT.Controls.Add(this.ClabeT);
            this.PanelCuentaT.Controls.Add(this.NumeroCuentaT);
            this.PanelCuentaT.Controls.Add(this.Moneda);
            this.PanelCuentaT.Controls.Add(this.lblClabeT);
            this.PanelCuentaT.Controls.Add(this.lblTipoCambio);
            this.PanelCuentaT.Controls.Add(this.FormaPago);
            this.PanelCuentaT.Controls.Add(this.labelImporte);
            this.PanelCuentaT.Controls.Add(this.FechaAplicacion);
            this.PanelCuentaT.Controls.Add(this.lblFormaPago);
            this.PanelCuentaT.Controls.Add(this.Observaciones);
            this.PanelCuentaT.Controls.Add(this.BancoT);
            this.PanelCuentaT.Controls.Add(this.lblNumOperacion);
            this.PanelCuentaT.Controls.Add(this.Concepto);
            this.PanelCuentaT.Controls.Add(this.NumAuto);
            this.PanelCuentaT.Controls.Add(this.lblFolio);
            this.PanelCuentaT.Controls.Add(this.SucursalT);
            this.PanelCuentaT.Controls.Add(this.lblNota);
            this.PanelCuentaT.Controls.Add(this.NumOperacion);
            this.PanelCuentaT.Controls.Add(this.lblNumAutorizacion);
            this.PanelCuentaT.Controls.Add(this.lblBancoT);
            this.PanelCuentaT.Controls.Add(this.lblConcepto);
            this.PanelCuentaT.Controls.Add(this.lblFechaDocumento);
            this.PanelCuentaT.Controls.Add(this.lblNumCuentaT);
            this.PanelCuentaT.Controls.Add(this.lblReferencia);
            this.PanelCuentaT.Controls.Add(this.FechaDocumento);
            this.PanelCuentaT.Controls.Add(this.lblFechaAplicacion);
            this.PanelCuentaT.Controls.Add(this.Referencia);
            this.PanelCuentaT.Controls.Add(this.lblSucursalP);
            this.PanelCuentaT.Controls.Add(this.BeneficiarioRFC);
            this.PanelCuentaT.Controls.Add(this.NumDocto);
            this.PanelCuentaT.Controls.Add(this.Beneficiario);
            this.PanelCuentaT.Controls.Add(this.lblRFCT);
            this.PanelCuentaT.Controls.Add(this.lblBeneficiarioT);
            this.PanelCuentaT.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCuentaT.Location = new System.Drawing.Point(0, 93);
            this.PanelCuentaT.Name = "PanelCuentaT";
            this.PanelCuentaT.Size = new System.Drawing.Size(865, 193);
            this.PanelCuentaT.TabIndex = 34;
            // 
            // Importe
            // 
            this.Importe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Importe.Location = new System.Drawing.Point(740, 161);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(109, 20);
            this.Importe.TabIndex = 373;
            this.Importe.TabStop = false;
            this.Importe.Value = "0";
            // 
            // SearchBeneficiario
            // 
            this.SearchBeneficiario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchBeneficiario.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.SearchBeneficiario.Image = global::Jaeger.UI.Banco.Properties.Resources.search_16px;
            this.SearchBeneficiario.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.SearchBeneficiario.Location = new System.Drawing.Point(626, 32);
            this.SearchBeneficiario.Name = "SearchBeneficiario";
            this.SearchBeneficiario.Size = new System.Drawing.Size(20, 20);
            this.SearchBeneficiario.TabIndex = 372;
            this.SearchBeneficiario.Click += new System.EventHandler(this.Beneficiario_Buscar_Click);
            // 
            // RefreshBeneficiarios
            // 
            this.RefreshBeneficiarios.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshBeneficiarios.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RefreshBeneficiarios.Image = global::Jaeger.UI.Banco.Properties.Resources.refresh_16px;
            this.RefreshBeneficiarios.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RefreshBeneficiarios.Location = new System.Drawing.Point(647, 32);
            this.RefreshBeneficiarios.Name = "RefreshBeneficiarios";
            this.RefreshBeneficiarios.Size = new System.Drawing.Size(20, 20);
            this.RefreshBeneficiarios.TabIndex = 371;
            // 
            // FechaAplicacion
            // 
            this.FechaAplicacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaAplicacion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaAplicacion.Location = new System.Drawing.Point(759, 6);
            this.FechaAplicacion.Name = "FechaAplicacion";
            this.FechaAplicacion.Size = new System.Drawing.Size(90, 20);
            this.FechaAplicacion.TabIndex = 298;
            this.FechaAplicacion.TabStop = false;
            this.FechaAplicacion.Text = "15/12/2016";
            this.FechaAplicacion.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // FechaDocumento
            // 
            this.FechaDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDocumento.Location = new System.Drawing.Point(575, 6);
            this.FechaDocumento.Name = "FechaDocumento";
            this.FechaDocumento.Size = new System.Drawing.Size(92, 20);
            this.FechaDocumento.TabIndex = 297;
            this.FechaDocumento.TabStop = false;
            this.FechaDocumento.Text = "15/12/2016";
            this.FechaDocumento.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // TMovimiento
            // 
            this.TMovimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMovimiento.Location = new System.Drawing.Point(0, 0);
            this.TMovimiento.Movimiento = null;
            this.TMovimiento.Name = "TMovimiento";
            this.TMovimiento.ShowActualizar = true;
            this.TMovimiento.ShowCancelar = false;
            this.TMovimiento.ShowCerrar = true;
            this.TMovimiento.ShowDuplicar = false;
            this.TMovimiento.ShowGuardar = true;
            this.TMovimiento.ShowHerramientas = false;
            this.TMovimiento.ShowImprimir = false;
            this.TMovimiento.ShowNuevo = true;
            this.TMovimiento.Size = new System.Drawing.Size(865, 30);
            this.TMovimiento.TabIndex = 35;
            this.TMovimiento.BindingCompleted += new System.EventHandler<System.EventArgs>(this.BindingCompleted);
            // 
            // MovimientoBancarioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 543);
            this.Controls.Add(this.ComprobantesPages);
            this.Controls.Add(this.PanelCuentaT);
            this.Controls.Add(this.PanelCuentaP);
            this.Controls.Add(this.TMovimiento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MovimientoBancarioForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Movimiento Bancario";
            this.Load += new System.EventHandler(this.MovimientoBancarioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuentaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClabeP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancariaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComprobantesPages)).EndInit();
            this.ComprobantesPages.ResumeLayout(false);
            this.PageComprobantes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).EndInit();
            this.PageAuxliar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliares.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliares)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelCuentaP)).EndInit();
            this.PanelCuentaP.ResumeLayout(false);
            this.PanelCuentaP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiarioT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFCT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDocto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSucursalP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaAplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuentaT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConcepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumAutorizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SucursalT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClabeT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClabeT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParaAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorComprobar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelCuentaT)).EndInit();
            this.PanelCuentaT.ResumeLayout(false);
            this.PanelCuentaT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshBeneficiarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal Telerik.WinControls.UI.RadLabel lblNumCuentaP;
        private Telerik.WinControls.UI.RadTextBox NumeroCuentaP;
        internal Telerik.WinControls.UI.RadLabel lblBancoP;
        internal Telerik.WinControls.UI.RadLabel lblClabeP;
        internal Telerik.WinControls.UI.RadTextBox BancoP;
        internal Telerik.WinControls.UI.RadTextBox CuentaClabeP;
        private MultiComboBoxControl CuentaBancariaP;
        private Telerik.WinControls.UI.RadLabel lblCuenta;
        private Telerik.WinControls.UI.RadPageView ComprobantesPages;
        private Telerik.WinControls.UI.RadPageViewPage PageComprobantes;
        private Telerik.WinControls.UI.RadPageViewPage PageAuxliar;
        private Telerik.WinControls.UI.RadPanel PanelCuentaP;
        internal Telerik.WinControls.UI.RadLabel lblFechaRegistro;
        internal Telerik.WinControls.UI.RadLabel lblTipoOperacion;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaRegistro;
        internal Telerik.WinControls.UI.RadLabel lblBeneficiarioT;
        internal Telerik.WinControls.UI.RadLabel lblRFCT;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Beneficiario;
        internal Telerik.WinControls.UI.RadTextBox NumDocto;
        internal Telerik.WinControls.UI.RadTextBox BeneficiarioRFC;
        internal Telerik.WinControls.UI.RadLabel lblSucursalP;
        internal Telerik.WinControls.UI.RadTextBox Referencia;
        internal Telerik.WinControls.UI.RadLabel lblFechaAplicacion;
        internal Telerik.WinControls.UI.RadLabel lblReferencia;
        internal Telerik.WinControls.UI.RadLabel lblNumCuentaT;
        internal Telerik.WinControls.UI.RadLabel lblFechaDocumento;
        internal Telerik.WinControls.UI.RadLabel lblConcepto;
        internal Telerik.WinControls.UI.RadLabel lblBancoT;
        internal Telerik.WinControls.UI.RadLabel lblNumAutorizacion;
        internal Telerik.WinControls.UI.RadTextBox NumOperacion;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadTextBox SucursalT;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadTextBox NumAuto;
        internal Telerik.WinControls.UI.RadDropDownList Concepto;
        internal Telerik.WinControls.UI.RadLabel lblNumOperacion;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox BancoT;
        internal Telerik.WinControls.UI.RadTextBox Observaciones;
        internal Telerik.WinControls.UI.RadLabel lblFormaPago;
        internal Telerik.WinControls.UI.RadLabel labelImporte;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel lblClabeT;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        private Telerik.WinControls.UI.RadMultiColumnComboBox NumeroCuentaT;
        internal Telerik.WinControls.UI.RadTextBox ClabeT;
        internal Telerik.WinControls.UI.RadLabel lblMoneda;
        internal Telerik.WinControls.UI.RadMaskedEditBox TipoCambio;
        internal Telerik.WinControls.UI.RadCheckBox ParaAbono;
        internal Telerik.WinControls.UI.RadCheckBox PorComprobar;
        private Telerik.WinControls.UI.RadPanel PanelCuentaT;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaAplicacion;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDocumento;
        private Telerik.WinControls.UI.RadButton RefreshBeneficiarios;
        private Telerik.WinControls.UI.RadButton SearchBeneficiario;
        private System.ComponentModel.BackgroundWorker Preparar;
        private Common.Forms.ToolBarStandarControl TAuxiliar;
        public TbMovimientoBancarioControl TMovimiento;
        private Telerik.WinControls.UI.RadCalculatorDropDown Importe;
        protected internal Telerik.WinControls.UI.RadGridView GridComprobantes;
        protected internal Telerik.WinControls.UI.RadGridView GridAuxiliares;
        protected internal System.Windows.Forms.ErrorProvider Advertencia;
        protected internal TbMovimientoComprobanteControl TComprobante;
        protected internal MultiComboBoxControl ConceptoMovimiento;
    }
}
