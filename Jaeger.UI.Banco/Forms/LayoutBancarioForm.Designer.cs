﻿namespace Jaeger.UI.Banco.Forms
{
    partial class LayoutBancarioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayoutBancarioForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.NumIdentificacion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.FechaPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Secuencial = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.NombreEmpresa = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.Naturaleza = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.Instrucciones = new Telerik.WinControls.UI.RadTextBox();
            this.Modificable = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.TipoOperacion = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.Moneda = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.Importe = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.TipoCuenta = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.NumSucursal = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.NumCuenta = new Telerik.WinControls.UI.RadTextBox();
            this.gridRegistro = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.LayoutCombo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.StandarRegistros = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.Crear = new Telerik.WinControls.UI.RadButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumIdentificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Secuencial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Instrucciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Modificable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistro.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(7, 7);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(103, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Núm. identificación";
            // 
            // NumIdentificacion
            // 
            this.NumIdentificacion.Location = new System.Drawing.Point(7, 31);
            this.NumIdentificacion.MaxLength = 12;
            this.NumIdentificacion.Name = "NumIdentificacion";
            this.NumIdentificacion.NullText = "000000000000";
            this.NumIdentificacion.ShowClearButton = true;
            this.NumIdentificacion.ShowNullText = true;
            this.NumIdentificacion.Size = new System.Drawing.Size(108, 20);
            this.NumIdentificacion.TabIndex = 1;
            this.NumIdentificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(121, 7);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(80, 18);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "Fecha de pago";
            // 
            // FechaPago
            // 
            this.FechaPago.Location = new System.Drawing.Point(121, 31);
            this.FechaPago.Name = "FechaPago";
            this.FechaPago.Size = new System.Drawing.Size(194, 20);
            this.FechaPago.TabIndex = 2;
            this.FechaPago.TabStop = false;
            this.FechaPago.Text = "domingo, 11 de abril de 2021";
            this.FechaPago.Value = new System.DateTime(2021, 4, 11, 22, 48, 21, 211);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(321, 7);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(59, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Secuencial";
            // 
            // Secuencial
            // 
            this.Secuencial.Location = new System.Drawing.Point(321, 31);
            this.Secuencial.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.Secuencial.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Secuencial.Name = "Secuencial";
            this.Secuencial.NullableValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Secuencial.Size = new System.Drawing.Size(76, 20);
            this.Secuencial.TabIndex = 1;
            this.Secuencial.TabStop = false;
            this.Secuencial.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.Secuencial.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(7, 57);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(121, 18);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Nombre de la empresa";
            // 
            // NombreEmpresa
            // 
            this.NombreEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.NombreEmpresa.Location = new System.Drawing.Point(7, 81);
            this.NombreEmpresa.MaxLength = 36;
            this.NombreEmpresa.Name = "NombreEmpresa";
            this.NombreEmpresa.NullText = "Nombre del la empresa";
            this.NombreEmpresa.ShowClearButton = true;
            this.NombreEmpresa.ShowNullText = true;
            this.NombreEmpresa.Size = new System.Drawing.Size(269, 20);
            this.NombreEmpresa.TabIndex = 1;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(282, 57);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(122, 18);
            this.radLabel5.TabIndex = 0;
            this.radLabel5.Text = "Descripción del archivo";
            // 
            // Descripcion
            // 
            this.Descripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Descripcion.Location = new System.Drawing.Point(282, 81);
            this.Descripcion.MaxLength = 20;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.NullText = "Descripción del archivo";
            this.Descripcion.ShowClearButton = true;
            this.Descripcion.ShowNullText = true;
            this.Descripcion.Size = new System.Drawing.Size(210, 20);
            this.Descripcion.TabIndex = 1;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(499, 57);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(118, 18);
            this.radLabel6.TabIndex = 0;
            this.radLabel6.Text = "Naturaleza del archivo";
            // 
            // Naturaleza
            // 
            this.Naturaleza.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Naturaleza.DisplayMember = "Id";
            this.Naturaleza.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Naturaleza.NestedRadGridView
            // 
            this.Naturaleza.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Naturaleza.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Naturaleza.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Naturaleza.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Naturaleza.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Naturaleza.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Naturaleza.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Index";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Index";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descriptor";
            gridViewTextBoxColumn3.Width = 200;
            this.Naturaleza.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Naturaleza.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Naturaleza.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Naturaleza.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Naturaleza.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Naturaleza.EditorControl.Name = "NestedRadGridView";
            this.Naturaleza.EditorControl.ReadOnly = true;
            this.Naturaleza.EditorControl.ShowGroupPanel = false;
            this.Naturaleza.EditorControl.ShowRowErrors = false;
            this.Naturaleza.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Naturaleza.EditorControl.TabIndex = 0;
            this.Naturaleza.Location = new System.Drawing.Point(499, 81);
            this.Naturaleza.Name = "Naturaleza";
            this.Naturaleza.NullText = "Naturaleza del archivo";
            this.Naturaleza.Size = new System.Drawing.Size(182, 20);
            this.Naturaleza.TabIndex = 3;
            this.Naturaleza.TabStop = false;
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel7.Location = new System.Drawing.Point(687, 57);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(188, 18);
            this.radLabel7.TabIndex = 0;
            this.radLabel7.Text = "Instrucciones para órdenes de pago ";
            // 
            // Instrucciones
            // 
            this.Instrucciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Instrucciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Instrucciones.Location = new System.Drawing.Point(687, 81);
            this.Instrucciones.MaxLength = 40;
            this.Instrucciones.Name = "Instrucciones";
            this.Instrucciones.NullText = "Instrucciones para órdenes de pago ";
            this.Instrucciones.ShowClearButton = true;
            this.Instrucciones.ShowNullText = true;
            this.Instrucciones.Size = new System.Drawing.Size(219, 20);
            this.Instrucciones.TabIndex = 1;
            // 
            // Modificable
            // 
            this.Modificable.Location = new System.Drawing.Point(7, 157);
            this.Modificable.Name = "Modificable";
            this.Modificable.Size = new System.Drawing.Size(269, 18);
            this.Modificable.TabIndex = 4;
            this.Modificable.Text = "Si es modificable (permite edición en el software) ";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(7, 107);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(97, 18);
            this.radLabel8.TabIndex = 0;
            this.radLabel8.Text = "Tipo de operación";
            // 
            // TipoOperacion
            // 
            this.TipoOperacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoOperacion.Location = new System.Drawing.Point(7, 131);
            this.TipoOperacion.Name = "TipoOperacion";
            this.TipoOperacion.NullText = "Tipo de Operación";
            this.TipoOperacion.Size = new System.Drawing.Size(125, 20);
            this.TipoOperacion.TabIndex = 5;
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(141, 107);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(78, 18);
            this.radLabel9.TabIndex = 0;
            this.radLabel9.Text = "Clave Moneda";
            // 
            // Moneda
            // 
            this.Moneda.Location = new System.Drawing.Point(141, 131);
            this.Moneda.MaxLength = 3;
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "001";
            this.Moneda.ShowClearButton = true;
            this.Moneda.ShowNullText = true;
            this.Moneda.Size = new System.Drawing.Size(78, 20);
            this.Moneda.TabIndex = 1;
            this.Moneda.Text = "001";
            this.Moneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(225, 107);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(137, 18);
            this.radLabel10.TabIndex = 0;
            this.radLabel10.Text = "Importe a abonar o cargar";
            // 
            // Importe
            // 
            this.Importe.Location = new System.Drawing.Point(225, 131);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(135, 20);
            this.Importe.TabIndex = 6;
            this.Importe.TabStop = false;
            this.Importe.Value = "0";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(366, 108);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(81, 18);
            this.radLabel11.TabIndex = 0;
            this.radLabel11.Text = "Tipo de cuenta";
            // 
            // TipoCuenta
            // 
            this.TipoCuenta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TipoCuenta.Location = new System.Drawing.Point(366, 131);
            this.TipoCuenta.Name = "TipoCuenta";
            this.TipoCuenta.Size = new System.Drawing.Size(221, 20);
            this.TipoCuenta.TabIndex = 5;
            // 
            // radLabel12
            // 
            this.radLabel12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel12.Location = new System.Drawing.Point(593, 107);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(106, 18);
            this.radLabel12.TabIndex = 0;
            this.radLabel12.Text = "Número de sucursal";
            // 
            // NumSucursal
            // 
            this.NumSucursal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumSucursal.Location = new System.Drawing.Point(593, 130);
            this.NumSucursal.MaxLength = 4;
            this.NumSucursal.Name = "NumSucursal";
            this.NumSucursal.NullText = "0000";
            this.NumSucursal.ShowClearButton = true;
            this.NumSucursal.ShowNullText = true;
            this.NumSucursal.Size = new System.Drawing.Size(108, 20);
            this.NumSucursal.TabIndex = 1;
            this.NumSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel13
            // 
            this.radLabel13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel13.Location = new System.Drawing.Point(707, 106);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(100, 18);
            this.radLabel13.TabIndex = 0;
            this.radLabel13.Text = "Número de cuenta";
            // 
            // NumCuenta
            // 
            this.NumCuenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumCuenta.Location = new System.Drawing.Point(707, 130);
            this.NumCuenta.MaxLength = 20;
            this.NumCuenta.Name = "NumCuenta";
            this.NumCuenta.NullText = "00000000000000000000";
            this.NumCuenta.ShowClearButton = true;
            this.NumCuenta.ShowNullText = true;
            this.NumCuenta.Size = new System.Drawing.Size(199, 20);
            this.NumCuenta.TabIndex = 1;
            this.NumCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gridRegistro
            // 
            this.gridRegistro.Dock = System.Windows.Forms.DockStyle.Top;
            this.gridRegistro.Location = new System.Drawing.Point(0, 247);
            // 
            // 
            // 
            this.gridRegistro.MasterTemplate.AllowAddNewRow = false;
            this.gridRegistro.MasterTemplate.AllowDeleteRow = false;
            gridViewTextBoxColumn4.FieldName = "Nombre";
            gridViewTextBoxColumn4.HeaderText = "Beneficiario";
            gridViewTextBoxColumn4.MaxLength = 55;
            gridViewTextBoxColumn4.Name = "Nombre";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 200;
            gridViewComboBoxColumn1.FieldName = "TipoCuenta";
            gridViewComboBoxColumn1.HeaderText = "Tipo de cuenta";
            gridViewComboBoxColumn1.Name = "TipoCuenta";
            gridViewComboBoxColumn1.Width = 85;
            gridViewTextBoxColumn5.FieldName = "ClaveBanco";
            gridViewTextBoxColumn5.HeaderText = "Clave de Banco";
            gridViewTextBoxColumn5.MaxLength = 4;
            gridViewTextBoxColumn5.Name = "ClaveBanco";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn6.FieldName = "NumeroCuenta";
            gridViewTextBoxColumn6.HeaderText = "Número de cuenta";
            gridViewTextBoxColumn6.MaxLength = 40;
            gridViewTextBoxColumn6.Name = "NúmeroCuenta";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 100;
            gridViewComboBoxColumn2.DataType = typeof(int);
            gridViewComboBoxColumn2.FieldName = "TipoOperacion";
            gridViewComboBoxColumn2.HeaderText = "Tipo de operación";
            gridViewComboBoxColumn2.Name = "TipoOperacion";
            gridViewComboBoxColumn2.ReadOnly = true;
            gridViewComboBoxColumn2.Width = 85;
            gridViewComboBoxColumn2.WrapText = true;
            gridViewTextBoxColumn7.DataType = typeof(double);
            gridViewTextBoxColumn7.FieldName = "Importe";
            gridViewTextBoxColumn7.FormatString = "{0:N2}";
            gridViewTextBoxColumn7.HeaderText = "Importe";
            gridViewTextBoxColumn7.Name = "Importe";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn7.Width = 85;
            gridViewTextBoxColumn8.FieldName = "ReferenciaAlfanumerica";
            gridViewTextBoxColumn8.HeaderText = "Referencia Alfanumérica";
            gridViewTextBoxColumn8.MaxLength = 40;
            gridViewTextBoxColumn8.Name = "ReferenciaAlfanumerica";
            gridViewTextBoxColumn8.Width = 100;
            gridViewTextBoxColumn8.WrapText = true;
            gridViewTextBoxColumn9.FieldName = "Instrucciones";
            gridViewTextBoxColumn9.HeaderText = "Instrucciones";
            gridViewTextBoxColumn9.MaxLength = 40;
            gridViewTextBoxColumn9.Name = "Instrucciones";
            gridViewTextBoxColumn9.Width = 100;
            gridViewTextBoxColumn10.FieldName = "ReferenciaNumerica";
            gridViewTextBoxColumn10.HeaderText = "Referencia Numérica";
            gridViewTextBoxColumn10.MaxLength = 7;
            gridViewTextBoxColumn10.Name = "ReferenciaNumerica";
            gridViewTextBoxColumn10.Width = 85;
            gridViewComboBoxColumn3.FieldName = "Plazo";
            gridViewComboBoxColumn3.HeaderText = "Plazo";
            gridViewComboBoxColumn3.Name = "Plazo";
            gridViewTextBoxColumn11.FieldName = "ClaveMoneda";
            gridViewTextBoxColumn11.HeaderText = "Clave de la moneda";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "ClaveMoneda";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.WrapText = true;
            this.gridRegistro.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewComboBoxColumn3,
            gridViewTextBoxColumn11});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem1.AggregateExpression = null;
            gridViewSummaryItem1.FormatString = "{0:N2}";
            gridViewSummaryItem1.Name = "Importe";
            this.gridRegistro.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridRegistro.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridRegistro.Name = "gridRegistro";
            this.gridRegistro.ShowGroupPanel = false;
            this.gridRegistro.Size = new System.Drawing.Size(916, 199);
            this.gridRegistro.TabIndex = 7;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.LayoutCombo);
            this.radPanel1.Controls.Add(this.radLabel15);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Controls.Add(this.radLabel12);
            this.radPanel1.Controls.Add(this.Importe);
            this.radPanel1.Controls.Add(this.radLabel13);
            this.radPanel1.Controls.Add(this.TipoCuenta);
            this.radPanel1.Controls.Add(this.radLabel9);
            this.radPanel1.Controls.Add(this.TipoOperacion);
            this.radPanel1.Controls.Add(this.radLabel11);
            this.radPanel1.Controls.Add(this.Modificable);
            this.radPanel1.Controls.Add(this.radLabel10);
            this.radPanel1.Controls.Add(this.Naturaleza);
            this.radPanel1.Controls.Add(this.radLabel3);
            this.radPanel1.Controls.Add(this.FechaPago);
            this.radPanel1.Controls.Add(this.radLabel4);
            this.radPanel1.Controls.Add(this.Descripcion);
            this.radPanel1.Controls.Add(this.radLabel7);
            this.radPanel1.Controls.Add(this.Instrucciones);
            this.radPanel1.Controls.Add(this.radLabel5);
            this.radPanel1.Controls.Add(this.NombreEmpresa);
            this.radPanel1.Controls.Add(this.radLabel6);
            this.radPanel1.Controls.Add(this.Secuencial);
            this.radPanel1.Controls.Add(this.radLabel8);
            this.radPanel1.Controls.Add(this.Moneda);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.NumCuenta);
            this.radPanel1.Controls.Add(this.NumIdentificacion);
            this.radPanel1.Controls.Add(this.NumSucursal);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 35);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(916, 182);
            this.radPanel1.TabIndex = 8;
            // 
            // LayoutCombo
            // 
            this.LayoutCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "Banbajio";
            radListDataItem2.Text = "Bancomer";
            this.LayoutCombo.Items.Add(radListDataItem1);
            this.LayoutCombo.Items.Add(radListDataItem2);
            this.LayoutCombo.Location = new System.Drawing.Point(658, 31);
            this.LayoutCombo.Name = "LayoutCombo";
            this.LayoutCombo.NullText = "Selecciona";
            this.LayoutCombo.Size = new System.Drawing.Size(248, 20);
            this.LayoutCombo.TabIndex = 8;
            // 
            // radLabel15
            // 
            this.radLabel15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel15.Location = new System.Drawing.Point(658, 7);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(114, 18);
            this.radLabel15.TabIndex = 7;
            this.radLabel15.Text = "Selecciona un Layout:";
            // 
            // StandarRegistros
            // 
            this.StandarRegistros.Dock = System.Windows.Forms.DockStyle.Top;
            this.StandarRegistros.Etiqueta = "";
            this.StandarRegistros.Location = new System.Drawing.Point(0, 217);
            this.StandarRegistros.Name = "StandarRegistros";
            this.StandarRegistros.ReadOnly = false;
            this.StandarRegistros.ShowActualizar = false;
            this.StandarRegistros.ShowAutorizar = false;
            this.StandarRegistros.ShowCerrar = false;
            this.StandarRegistros.ShowEditar = false;
            this.StandarRegistros.ShowExportarExcel = false;
            this.StandarRegistros.ShowFiltro = true;
            this.StandarRegistros.ShowGuardar = false;
            this.StandarRegistros.ShowHerramientas = false;
            this.StandarRegistros.ShowImagen = false;
            this.StandarRegistros.ShowImprimir = false;
            this.StandarRegistros.ShowNuevo = true;
            this.StandarRegistros.ShowRemover = true;
            this.StandarRegistros.Size = new System.Drawing.Size(916, 30);
            this.StandarRegistros.TabIndex = 10;
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.Location = new System.Drawing.Point(817, 455);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(75, 23);
            this.Cerrar.TabIndex = 11;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // Crear
            // 
            this.Crear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Crear.Location = new System.Drawing.Point(736, 455);
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(75, 23);
            this.Crear.TabIndex = 11;
            this.Crear.Text = "Crear";
            this.Crear.Click += new System.EventHandler(this.Crear_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(916, 35);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel14
            // 
            this.radLabel14.BackColor = System.Drawing.Color.White;
            this.radLabel14.Location = new System.Drawing.Point(7, 7);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(338, 18);
            this.radLabel14.TabIndex = 0;
            this.radLabel14.Text = "LAYOUT “C” FORMATOS PARA IMPORTACIÓN (TEF / TEF INTELAR)";
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // LayoutBancarioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 484);
            this.Controls.Add(this.radLabel14);
            this.Controls.Add(this.Crear);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.gridRegistro);
            this.Controls.Add(this.StandarRegistros);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LayoutBancarioForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Layout";
            this.Load += new System.EventHandler(this.LayoutBancarioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumIdentificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Secuencial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Naturaleza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Instrucciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Modificable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistro.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox NumIdentificacion;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker FechaPago;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor Secuencial;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox NombreEmpresa;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Naturaleza;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox Instrucciones;
        private Telerik.WinControls.UI.RadCheckBox Modificable;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList TipoOperacion;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox Moneda;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadCalculatorDropDown Importe;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadDropDownList TipoCuenta;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTextBox NumSucursal;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox NumCuenta;
        private Telerik.WinControls.UI.RadGridView gridRegistro;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Common.Forms.ToolBarStandarControl StandarRegistros;
        private Telerik.WinControls.UI.RadButton Crear;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadDropDownList LayoutCombo;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private System.Windows.Forms.ErrorProvider Advertencia;
    }
}
