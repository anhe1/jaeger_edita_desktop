﻿namespace Jaeger.UI.Banco.Forms
{
    partial class MovimientoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Movimientos = new Jaeger.UI.Banco.Forms.MovimientosGridControl();
            this.TMovimiento = new Jaeger.UI.Banco.Forms.TbMovimientoControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Movimientos
            // 
            this.Movimientos.Auditar = false;
            this.Movimientos.Cancelar = false;
            this.Movimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Movimientos.Location = new System.Drawing.Point(0, 30);
            this.Movimientos.Name = "Movimientos";
            this.Movimientos.Size = new System.Drawing.Size(1384, 631);
            this.Movimientos.TabIndex = 3;
            // 
            // TMovimiento
            // 
            this.TMovimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMovimiento.Location = new System.Drawing.Point(0, 0);
            this.TMovimiento.Name = "TMovimiento";
            this.TMovimiento.ShowActualizar = true;
            this.TMovimiento.ShowCancelar = true;
            this.TMovimiento.ShowCerrar = true;
            this.TMovimiento.ShowEditar = true;
            this.TMovimiento.ShowExportar = true;
            this.TMovimiento.ShowFiltro = true;
            this.TMovimiento.ShowHerramientas = true;
            this.TMovimiento.ShowImprimir = true;
            this.TMovimiento.ShowNuevo = true;
            this.TMovimiento.ShowReporteComprobante = true;
            this.TMovimiento.Size = new System.Drawing.Size(1384, 30);
            this.TMovimiento.TabIndex = 2;
            // 
            // MovimientoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 661);
            this.Controls.Add(this.Movimientos);
            this.Controls.Add(this.TMovimiento);
            this.Name = "MovimientoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Banco: Movimientos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MovimientoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal TbMovimientoControl TMovimiento;
        protected internal MovimientosGridControl Movimientos;
    }
}
