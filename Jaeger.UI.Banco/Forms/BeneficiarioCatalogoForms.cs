﻿using System;
using System.ComponentModel;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// Banco: catalogo de beneficiarios
    /// </summary>
    public partial class BeneficiarioCatalogoForms : Telerik.WinControls.UI.RadForm {
        #region declaraciones
        protected internal Aplication.Banco.IBancoService service;
        protected internal BindingList<BeneficiarioDetailModel> beneficiarios;
        #endregion

        public BeneficiarioCatalogoForms() {
            InitializeComponent();
            this.TBeneficiario.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public BeneficiarioCatalogoForms(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this.TBeneficiario.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void BeneficiarioCatalogoForms_Load(object sender, EventArgs e) {
            // reemplazar las imagenes standar
            using (Builder.IGridViewBeneficiarioBuilder d0 = new Builder.GridViewBeneficiarioBuilder()){
                this.TBeneficiario.GridData.Columns.AddRange(d0.Templetes().GetColumns().Build()); 
            }

            this.TBeneficiario.Nuevo.Image = Properties.Resources.add_user_male_16px;
            this.TBeneficiario.Nuevo.Click += this.Agregar_Click;
            this.TBeneficiario.Editar.Click += this.Editar_Click;
            this.TBeneficiario.Remover.Click += this.Remover_Click;
            this.TBeneficiario.Actualizar.Click += this.Actualizar_Click;
            this.TBeneficiario.Filtro.Click += this.Filtro_Click;
            this.TBeneficiario.Cerrar.Click += this.Cerrar_Click;
        }

        #region barra de herramientas
        public virtual void Agregar_Click(object sender, EventArgs e) {
            using (var nuevo = new BeneficiarioForm(null, this.service)) {
                nuevo.Text = "Nuevo Beneficiario";
                nuevo.ShowDialog(this);
            }
        }

        public virtual void Editar_Click(object sender, EventArgs e) {
            if (this.TBeneficiario.GridData.CurrentRow != null) {
                var seleccionado = this.TBeneficiario.GridData.CurrentRow.DataBoundItem as BeneficiarioDetailModel;
                if (seleccionado != null) {
                    using (var editar = new BeneficiarioForm(seleccionado, this.service)) {
                        editar.Text = "Editar Beneficiario";
                        editar.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            if (this.TBeneficiario.GridData.CurrentRow != null) {
                RadMessageBox.Show(this, "¡No implementado!", "Error", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Consultando información ...";
                espera.ShowDialog(this);
            }
            this.TBeneficiario.GridData.DataSource = this.beneficiarios;
        }

        public virtual void Filtro_Click(object sender, EventArgs e) {
            this.TBeneficiario.GridData.ActivateFilter(((Telerik.WinControls.UI.CommandBarToggleButton)sender).ToggleState);
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        public virtual void Consultar() {
            //this.beneficiarios = this.service.GetBeneficiarios(new List<TipoRelacionComericalEnum> { TipoRelacionComericalEnum.Cliente, TipoRelacionComericalEnum.Proveedor, TipoRelacionComericalEnum.Empleado });
        }
    }
}
