﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Aplication.Empresa.Contracts;

namespace Jaeger.UI.Banco.Forms {
    public partial class MovimientoCatalogoForm : RadForm {
        #region declaraciones
        private Domain.Base.ValueObjects.UIAction _permisos;
        protected internal IConfigurationService _Empresa;
        #endregion

        public MovimientoCatalogoForm() {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public MovimientoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void MovimientoCatalogoForm_Load(object sender, EventArgs e) {
            this.TMovimiento.Cancelar.Enabled = false;
            this.TMovimiento.Nuevo.Enabled = this._permisos.Agregar;
            this.TMovimiento.Cancelar.Enabled = this._permisos.Cancelar;
            this.TMovimiento.Editar.Enabled = this._permisos.Editar;
            this.Movimientos.Status = this._permisos.Status;
            this.Movimientos.Actualizar += Movimientos_Actualizar;
            this.TMovimiento.Editar.Click += this.TMovimiento_Editar_Click;
            this.TMovimiento.Cancelar.Click += this.TMovimiento_Cancelar_Click;
            this.TMovimiento.Actualizar.Click += this.TMovimiento_Actualizar_Click;
            this.TMovimiento.Filtro.Click += this.Movimientos.OnFiltros;
            this.TMovimiento.Cerrar.Click += this.TMovimiento_Cerrar_Click;
            this.TMovimiento.ReporteComprobante.Click += this.TMovimiento_IComprobante_Click;
            this.TMovimiento.ReporteLVertical.Click += this.TMovimiento_ListaVertical_Click;
            this.TMovimiento.ReporteEstadoCuenta.Click += this.TMovimiento_IEstadoCuenta_Click;
            this.TMovimiento.Exportar.Click += this.TMovimiento_Exportar_Click;
            
            this.Movimientos.MenuContextual.Items["Imprimir"].Click += this.TMovimiento_IComprobante_Click;
            this.Movimientos.MenuContextual.Items["Clonar"].Click += this.MenuContextualClonar_Click;
            this.Movimientos.TBanamexLayoutC.Click += this.LayoutBanamexC_Click;
            //this.OnLoad();
            this.TMovimiento.CreateService();
        }

        #region barra de herramientas
        public virtual void OnLoad() {
            this.TMovimiento.Service = new Aplication.Banco.BancoService();
        }

        public virtual void TMovimiento_Editar_Click(object sender, EventArgs e) {
            var seleccionado = (MovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            if (seleccionado != null) {
                if (seleccionado.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                    using (var editar = new TransferenciaCuentasForm(seleccionado, this.TMovimiento.Service)) {
                        editar.ShowDialog(this);
                    }
                } else {
                    using (var edita = new MovimientoBancarioForm(seleccionado, this.TMovimiento.Service)) {
                        edita.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void TMovimiento_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = (MovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            if (seleccionado != null) {
                if (seleccionado.Status == MovimientoBancarioStatusEnum.Cancelado) { return; }
                if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                    seleccionado.Status = MovimientoBancarioStatusEnum.Cancelado;
                    using (var espera = new Common.Forms.Waiting1Form(this.Aplicar)) {
                        espera.Text = "Aplicando cambios ...";
                        espera.ShowDialog(this);
                    }
                    if (this.TMovimiento.Tag != null) {
                        if ((bool)this.TMovimiento.Tag == false) {
                            MessageBox.Show("Error");
                            this.TMovimiento.Tag = null;
                        }
                    }
                }
            }
        }

        public virtual void TMovimiento_Actualizar_Click(object sender, EventArgs e) {
            if (this.TMovimiento.CuentaCorriente != null) {
                using (var espera = new Common.Forms.Waiting2Form(this.Consultar)) {
                    espera.Text = "Consultando ...";
                    espera.ShowDialog(this);
                }
                var combo = this.Movimientos.GridData.Columns["Status"] as GridViewComboBoxColumn;
                combo.DisplayMember = "Descripcion";
                combo.ValueMember = "Id";
                combo.DataSource = Aplication.Banco.BancoService.GetStatus();
                this.Movimientos.GridData.DataSource = this.TMovimiento.CuentaCorriente.Movimientos;
            } else {
                RadMessageBox.Show(this, Properties.Resources.msg_Banco_Cuenta_Seleccionar, "Información", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void TMovimiento_Exportar_Click(object sender, EventArgs e) {
            using (var exportar = new Common.Forms.TelerikGridExportForm(this.Movimientos.GridData)) {
                exportar.ShowDialog(this);
            }
        }

        public virtual void TMovimiento_IComprobante_Click(object sender, EventArgs e) {
            var seleccionado = (IMovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            if (seleccionado != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(seleccionado));
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_ListaVertical_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.GetPrinters();
            if (seleccionado != null) {
                var imprimir = new ReporteForm(seleccionado);
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_IEstadoCuenta_Click(object sender, EventArgs e) {
            var seleccionado = this.TMovimiento.CuentaCorriente;
            if (seleccionado != null) {
                var imprimir = new ReporteForm(new EstadoCuentaPrinter(seleccionado));
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region menu contextual
        private void MenuContextualClonar_Click(object sender, EventArgs e) {
            var seleccionado = (MovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            if (seleccionado != null) {
                seleccionado.Clonar();
                if (seleccionado.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                    using (var editar = new TransferenciaCuentasForm(seleccionado, this.TMovimiento.Service)) {
                        editar.ShowDialog(this);
                    }
                } else {
                    var j = MovimientoBancarioDetailModel.Json(seleccionado.InfoAuxiliar);
                    seleccionado.CuentaPropia = j.CuentaPropia;
                    using (var edita = new MovimientoBancarioForm(seleccionado, this.TMovimiento.Service)) {
                        edita.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void LayoutBanamexC_Click(object sender, EventArgs e) {
            var seleccionados = this.Movimientos.GetSelecteds();
            if (seleccionados != null) {
                if(seleccionados.Where(it=> it.Tipo == MovimientoBancarioEfectoEnum.Ingreso).Count() > 0) {
                    RadMessageBox.Show(this, "Uno o más movimientos seleccionados son de tipo ingreso y este layout no puede ser aplicado", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    return;
                }
                using (var layout = new LayoutBancarioForm(seleccionados, this.TMovimiento.Service)) {
                    layout.ShowDialog(this);
                }
                return;
            }
        }
        #endregion

        private void Movimientos_Actualizar(object sender, MovimientoBancarioDetailModel e) {
            using (var espera = new Common.Forms.Waiting1Form(this.Aplicar)) {
                espera.Text = "Aplicando cambios ...";
                espera.ShowDialog(this);
            }
            if (this.TMovimiento.Tag != null) {
                if ((bool)this.TMovimiento.Tag == false) {
                    MessageBox.Show("Error");
                    this.TMovimiento.Tag = null;
                }
            }
        }

        #region metodos privados
        public virtual void Consultar() {
            if (this.TMovimiento.CuentaCorriente != null) {
                var s = new BancoCuentaSaldoDetailModel();
                if (this.TMovimiento.CuentaCorriente.Cuenta.IdCuenta == 0)
                    return;
                if (this.TMovimiento.GetPeriodo() == 0) {
                    s.SaldoInicial = 0;
                } else {
                    s = this.TMovimiento.Service.GetSaldo(this.TMovimiento.CuentaCorriente.Cuenta.IdCuenta, new DateTime(this.TMovimiento.GetEjercicio(), this.TMovimiento.GetPeriodo(), 1));
                    if (s != null) {
                        s.SaldoInicial = s.SaldoInicial;
                    } else {
                        s = new BancoCuentaSaldoDetailModel() { SaldoInicial = 0 };
                    }
                }
                
                var q0 = Aplication.Banco.BancoService.Query().IdCuenta(this.TMovimiento.CuentaCorriente.Cuenta.IdCuenta).Year(this.TMovimiento.GetEjercicio()).Month(this.TMovimiento.GetPeriodo()).Build();
                var d0 = this.TMovimiento.Service.GetList<MovimientoBancarioDetailModel>(q0).ToList<IMovimientoBancarioDetailModel>();
                if (d0 != null) {
                    this.TMovimiento.CuentaCorriente.Movimientos = new System.ComponentModel.BindingList<IMovimientoBancarioDetailModel>(d0);
                    this.TMovimiento.CuentaCorriente.Saldo = s;
                    this.Movimientos.GridData.Columns["Saldo"].WrapText = true;
                    this.Movimientos.GridData.Columns["Saldo"].HeaderText = "Saldo = " + this.TMovimiento.CuentaCorriente.Saldo.SaldoInicial.ToString("C");
                }
                this.TMovimiento.CuentaCorriente.Procesar();
            }
        }

        public virtual void Aplicar() {
            var seleccionado = (MovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            this.TMovimiento.Tag = this.TMovimiento.Service.Aplicar(seleccionado);
        }
        #endregion
    }
}
