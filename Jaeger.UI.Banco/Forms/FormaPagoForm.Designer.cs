﻿namespace Jaeger.UI.Banco.Forms
{
    partial class FormaPagoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaPagoForm));
            this.checkBoxActivo = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxClave = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxClaveSAT = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxDescripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.AfectarSaldo = new Telerik.WinControls.UI.RadCheckBox();
            this.toolBarStandarControl1 = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.errorFormaPago = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AfectarSaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorFormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxActivo
            // 
            this.checkBoxActivo.Location = new System.Drawing.Point(349, 52);
            this.checkBoxActivo.Name = "checkBoxActivo";
            this.checkBoxActivo.Size = new System.Drawing.Size(51, 18);
            this.checkBoxActivo.TabIndex = 1;
            this.checkBoxActivo.Text = "Activo";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(10, 26);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(35, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Clave:";
            // 
            // textBoxClave
            // 
            this.textBoxClave.Location = new System.Drawing.Point(51, 25);
            this.textBoxClave.MaxLength = 3;
            this.textBoxClave.Name = "textBoxClave";
            this.textBoxClave.Size = new System.Drawing.Size(82, 20);
            this.textBoxClave.TabIndex = 3;
            this.textBoxClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(139, 26);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(58, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Clave SAT:";
            // 
            // comboBoxClaveSAT
            // 
            this.comboBoxClaveSAT.AutoSizeDropDownToBestFit = true;
            this.comboBoxClaveSAT.DisplayMember = "Descripcion";
            this.comboBoxClaveSAT.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // comboBoxClaveSAT.NestedRadGridView
            // 
            this.comboBoxClaveSAT.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxClaveSAT.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxClaveSAT.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxClaveSAT.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxClaveSAT.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.comboBoxClaveSAT.EditorControl.Name = "NestedRadGridView";
            this.comboBoxClaveSAT.EditorControl.ReadOnly = true;
            this.comboBoxClaveSAT.EditorControl.ShowGroupPanel = false;
            this.comboBoxClaveSAT.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxClaveSAT.EditorControl.TabIndex = 0;
            this.comboBoxClaveSAT.Location = new System.Drawing.Point(203, 25);
            this.comboBoxClaveSAT.Name = "comboBoxClaveSAT";
            this.comboBoxClaveSAT.Size = new System.Drawing.Size(197, 20);
            this.comboBoxClaveSAT.TabIndex = 4;
            this.comboBoxClaveSAT.TabStop = false;
            this.comboBoxClaveSAT.ValueMember = "Clave";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(10, 52);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(67, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Descripción:";
            // 
            // textBoxDescripcion
            // 
            this.textBoxDescripcion.Location = new System.Drawing.Point(83, 51);
            this.textBoxDescripcion.MaxLength = 32;
            this.textBoxDescripcion.Name = "textBoxDescripcion";
            this.textBoxDescripcion.Size = new System.Drawing.Size(249, 20);
            this.textBoxDescripcion.TabIndex = 3;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.AfectarSaldo);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.comboBoxClaveSAT);
            this.radGroupBox1.Controls.Add(this.checkBoxActivo);
            this.radGroupBox1.Controls.Add(this.textBoxDescripcion);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.textBoxClave);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "Forma de Pago:";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 30);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(414, 108);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "Forma de Pago:";
            // 
            // AfectarSaldo
            // 
            this.AfectarSaldo.Location = new System.Drawing.Point(83, 77);
            this.AfectarSaldo.Name = "AfectarSaldo";
            this.AfectarSaldo.Size = new System.Drawing.Size(149, 18);
            this.AfectarSaldo.TabIndex = 5;
            this.AfectarSaldo.Text = "Afectar saldo de la cuenta";
            // 
            // toolBarStandarControl1
            // 
            this.toolBarStandarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarStandarControl1.Etiqueta = "";
            this.toolBarStandarControl1.Location = new System.Drawing.Point(0, 0);
            this.toolBarStandarControl1.Name = "toolBarStandarControl1";
            this.toolBarStandarControl1.ShowActualizar = false;
            this.toolBarStandarControl1.ShowAutorizar = false;
            this.toolBarStandarControl1.ShowCerrar = true;
            this.toolBarStandarControl1.ShowEditar = false;
            this.toolBarStandarControl1.ShowExportarExcel = false;
            this.toolBarStandarControl1.ShowFiltro = false;
            this.toolBarStandarControl1.ShowGuardar = true;
            this.toolBarStandarControl1.ShowHerramientas = false;
            this.toolBarStandarControl1.ShowImagen = false;
            this.toolBarStandarControl1.ShowImprimir = false;
            this.toolBarStandarControl1.ShowNuevo = true;
            this.toolBarStandarControl1.ShowRemover = false;
            this.toolBarStandarControl1.Size = new System.Drawing.Size(414, 30);
            this.toolBarStandarControl1.TabIndex = 6;
            this.toolBarStandarControl1.Nuevo.Click += this.ToolBarButtonNuevo_Click;
            this.toolBarStandarControl1.Guardar.Click += this.ToolBarButtonGuardar_Click;
            this.toolBarStandarControl1.Cerrar.Click += this.ToolBarButtonCerrar_Click;
            // 
            // errorFormaPago
            // 
            this.errorFormaPago.ContainerControl = this;
            // 
            // FormaPagoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 138);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.toolBarStandarControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormaPagoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Forma de Pago";
            this.Load += new System.EventHandler(this.FormaPagoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxClaveSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AfectarSaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorFormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadCheckBox checkBoxActivo;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox textBoxClave;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxClaveSAT;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox textBoxDescripcion;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Common.Forms.ToolBarStandarControl toolBarStandarControl1;
        private Telerik.WinControls.UI.RadCheckBox AfectarSaldo;
        private System.Windows.Forms.ErrorProvider errorFormaPago;
    }
}
