﻿using System;
using Jaeger.UI.Banco.Builder;

namespace Jaeger.UI.Banco.Forms {
    public class ConceptoCatalogoGridControl : Common.Forms.GridStandarControl {
        public ConceptoCatalogoGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IGridViewConceptosBuilder d0 = new GridViewConceptosBuilder()) {
                this.GridData.Columns.AddRange(d0.Templetes().Conceptos().Build());
            }
        }
    }
}
