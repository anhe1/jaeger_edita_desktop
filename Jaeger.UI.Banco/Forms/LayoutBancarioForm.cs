﻿using System;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Layout.Banco;
using Telerik.WinControls;

namespace Jaeger.UI.Banco.Forms {
    public partial class LayoutBancarioForm : RadForm {
        #region declaraciones
        protected internal IBancoService Service;
        protected internal List<IMovimientoBancarioDetailModel> Movimientos;
        protected internal ILayout Layout1 = new Layout.Banco.Banamex.LayoutC();
        #endregion

        public LayoutBancarioForm(List<IMovimientoBancarioDetailModel> movimientos, IBancoService service) {
            InitializeComponent();
            this.Movimientos = movimientos;
            this.Service = service;
        }

        private void LayoutBancarioForm_Load(object sender, EventArgs e) {
            this.LayoutCombo.Enabled = false;
            this.FechaPago.Value = DateTime.Now;
            this.FechaPago.MinDate = DateTime.Now;
            this.FechaPago.DateTimePickerElement.ToolTipText = "Fecha en que se realizará el cargo a su cuenta.";
            this.NumIdentificacion.TextBoxElement.ToolTipText = "Número de indentificación del cliente.";
            this.NumIdentificacion.ReadOnly = true;
            this.Secuencial.SpinElement.ToolTipText = "Número consecutivo de archivo, pueden ser 0001 hasta 0099. No pueden repetirse secuenciales para la misma de fecha.";
            this.NombreEmpresa.TextBoxElement.ToolTipText = "Razón social de la empresa.";
            this.NombreEmpresa.ReadOnly = true;
            this.Naturaleza.MultiColumnComboBoxElement.ToolTipText = "Tipos de archivos";
            this.Instrucciones.TextBoxElement.ToolTipText = "Campo opcional, sólo se utiliza para órdenes de pago";
            this.Descripcion.TextBoxElement.ToolTipText = "Descripción del tipo de pago que esta realizando. Esta descripciónse mostrará en su estado decuenta de Banca Electrónica e impreso.";
            this.TipoOperacion.DropDownListElement.ToolTipText = "";
            this.TipoOperacion.ReadOnly = true;
            this.Moneda.TextBoxElement.ToolTipText = "";
            this.Importe.CalculatorElement.ToolTipText = "";
            this.Importe.ReadOnly = true;
            this.TipoCuenta.DropDownListElement.ToolTipText = "";
            this.NumSucursal.TextBoxElement.ToolTipText = "";
            this.NumCuenta.ReadOnly = true;
            // naturaleza del archivo
            this.Naturaleza.DisplayMember = "Descriptor";
            this.Naturaleza.ValueMember = "Index";
            this.Naturaleza.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetTipoArchivo();
            // tipo de operacion
            this.TipoOperacion.DisplayMember = "Descriptor";
            this.TipoOperacion.ValueMember = "Index";
            this.TipoOperacion.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetTipoOperacion();
            // tipo de cuentas
            this.TipoCuenta.DisplayMember = "Descriptor";
            this.TipoCuenta.ValueMember = "Index";
            this.TipoCuenta.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetTipoCuentas();
            // tipo de operacion
            var tipoOperacioncolumn = this.gridRegistro.Columns["TipoOperacion"] as GridViewComboBoxColumn;
            tipoOperacioncolumn.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetTipoOperacion();
            tipoOperacioncolumn.ValueMember = "Index";
            tipoOperacioncolumn.DisplayMember = "Descriptor";

            var tipoCuenta = this.gridRegistro.Columns["TipoCuenta"] as GridViewComboBoxColumn;
            tipoCuenta.DisplayMember = "Descriptor";
            tipoCuenta.ValueMember = "Index";
            tipoCuenta.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetTipoCuentas();

            var plazo = this.gridRegistro.Columns["Plazo"] as GridViewComboBoxColumn;
            plazo.DisplayMember = "Descriptor";
            plazo.ValueMember = "Index";
            plazo.DataSource = Jaeger.Layout.Banco.Banamex.Catalogo.GetPlazos();

            this.Traslator();
            this.gridRegistro.DataSource = this.Layout1.Registros;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Crear_Click(object sender, EventArgs e) {
            this.Advertencia.Clear();
            if (this.Naturaleza.SelectedValue == null) {
                RadMessageBox.Show(this, "Selecciona una opción para la naturaleza del archivo", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
                this.Advertencia.SetError(this.Naturaleza, "Selecciona una opción válida");
                return ;
            }

            var saveFile = new System.Windows.Forms.SaveFileDialog { AddExtension = true, DefaultExt = ".txt", Title = "Nombre del archivo", FileName = this.Descripcion.Text };
            if (saveFile.ShowDialog(this) == System.Windows.Forms.DialogResult.OK) {
                this.Layout1.CuentaOrigen.Nombre = this.NombreEmpresa.Text;
                this.Layout1.Fecha = this.FechaPago.Value;
                this.Layout1.Descripcion = this.Descripcion.Text;
                (this.Layout1 as Jaeger.Layout.Banco.Banamex.LayoutC).CuentaOrigen.TipoCuenta = (int)this.TipoCuenta.SelectedValue;
                (this.Layout1 as Jaeger.Layout.Banco.Banamex.LayoutC).TipoOperacion = (int)this.TipoOperacion.SelectedValue;
                (this.Layout1 as Jaeger.Layout.Banco.Banamex.LayoutC).NaturalezaArchivo = int.Parse(this.Naturaleza.SelectedValue.ToString());
                (this.Layout1 as Jaeger.Layout.Banco.Banamex.LayoutC).Instrucciones = this.Instrucciones.Text;
                this.Layout1.NumeroCliente = this.NumIdentificacion.Text;
                this.Layout1.Secuencia = this.Secuencial.Text;
                this.Layout1.IsEditable = this.Modificable.Checked;
                this.Layout1.CuentaOrigen.Sucursal = this.NumSucursal.Text;
                this.Layout1.CuentaOrigen.NumeroCuenta = this.NumCuenta.Text;
                this.Layout1.Create(saveFile.FileName);
            }
        }

        protected internal void Traslator() {
            var cuentaOrigen = this.Movimientos.FirstOrDefault();
            var origen = this.Service.GetCuenta(cuentaOrigen.IdCuentaP);
            this.NombreEmpresa.Text = origen.Beneficiario;
            this.NumSucursal.Text = origen.Sucursal;
            this.NumCuenta.Text = origen.NumCuenta;
            this.Descripcion.Text = "Pago a proveedor";
            this.Naturaleza.SelectedIndex = 2;
            this.NumIdentificacion.Text = origen.NumCliente;
            this.Secuencial.Text = "1";

            if (this.Movimientos.Count == 1) {
                this.Descripcion.Text = "PAGO A PROVEEDOR " + cuentaOrigen.Identificador;
            }

            decimal total = 0;
            foreach (var movimiento in this.Movimientos) {
                this.Layout1.Registros.Add(new Jaeger.Layout.Banco.Banamex.RegistroCargoOAbono {
                    Nombre = movimiento.BeneficiarioT,
                    RFC = movimiento.BeneficiarioRFCT,
                    Importe = double.Parse(movimiento.Cargo.ToString()),
                    ClaveMoneda = 1,
                    NumeroCuenta = movimiento.NumeroCuentaT,
                    ClaveBanco = movimiento.ClaveBancoT,
                    Referencia = movimiento.Referencia,
                    ReferenciaAlfanumerica = movimiento.Referencia,
                    TipoOperacion = 0,
                    Plazo = "00",
                    TipoCuenta = 1, 
                    Descripcion = movimiento.Identificador,
                });
                total += movimiento.Cargo;
            }
            this.Importe.Value = total;
        }
    }
}
