﻿using System;
using System.Windows.Forms;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Aplication.Empresa.Contracts;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// formulario de movimientos bancarios
    /// </summary>
    public partial class MovimientosForm : RadForm {
        #region declaraciones
        protected MovimientoBancarioEfectoEnum efecto;
        protected internal IBancoService Service;
        protected internal IConfigurationService _Empresa;
        protected internal Domain.Base.ValueObjects.UIAction _Permisos;
        protected internal IConfiguration _Configuration;
        #endregion

        public MovimientosForm() {
            InitializeComponent();
            this.efecto = MovimientoBancarioEfectoEnum.Ingreso;
        }

        public MovimientosForm(MovimientoBancarioEfectoEnum efectoEnum) {
            InitializeComponent();
            this.efecto = efectoEnum;
        }

        private void MovimientosForm_Load(object sender, EventArgs e) {
            this.TMovimiento.Herramientas.Items.Add(this.Movimientos.TEmportar);
            this.TMovimiento.Herramientas.Items.Add(this.Movimientos.TAuxiliar);

            this.TMovimiento.Imprimir.Items.Add(this.Movimientos.TImprimirComprobante);
            this.TMovimiento.Imprimir.Items.Add(this.Movimientos.TImprimirHorizontal);
            this.TMovimiento.Imprimir.Items.Add(this.Movimientos.TImprimirVertical);

            this.Movimientos.MenuContextual.Items.Add(this.Movimientos.TCAuxiliar);

            this.Movimientos.Auditado += Movimientos_Auditado;
            this.Movimientos.Actualizar += Movimientos_Actualizar;
            this.TMovimiento.Filtro.Click += this.Movimientos.OnFiltros;
            this.TMovimiento.Editar.Click += this.TMovimiento_Editar_Click;
            this.TMovimiento.Cancelar.Click += this.TMovimiento_Cancelar_Click;
            this.TMovimiento.AutoSuma.Click += this.Movimientos.OnAutoSuma;
            this.TMovimiento.Actualizar.Click += this.TMovimiento_Actualizar_Click;
            this.TMovimiento.Cerrar.Click += this.TMovimiento_Cerrar_Click;
            this.TMovimiento.ExportarExcel.Click += this.TMovimiento_Exportar_Click;
            this.Movimientos.TAuxiliar.Click += this.TMovimiento_Auxiliar_Click;
            this.Movimientos.TCAuxiliar.Enabled = true;
            this.Movimientos.TCAuxiliar.Click += this.TContext_Auxiliar_Click;
            this.Movimientos.TImprimirVertical.Click += this.TMovimiento_ListaVertical_Click;
            this.Movimientos.MenuContextual.Items["Imprimir"].Click += this.TMovimiento_IComprobante_Click;
            this.Movimientos.TImprimirComprobante.Click += this.TMovimiento_IComprobante_Click;
        }

        #region barra de herramientas
        public virtual void TMovimiento_IComprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(seleccionado));
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_ListaVertical_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.GetPrinters();
            if (seleccionado != null) {
                var imprimir = new ReporteForm(seleccionado);
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_ListaHorizontal_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.GetPrinters();
            if (seleccionado != null) {
                var imprimir = new ReporteForm(seleccionado);
                imprimir.Show();
            }
        }

        public virtual void TMovimiento_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                if (seleccionado.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                    using (var editar = new TransferenciaCuentasForm(seleccionado, this.Service)) {
                        editar.ShowDialog(this);
                    }
                } else {
                    using (var edita = new MovimientoBancarioForm(seleccionado, this.Service)) {
                        edita.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void TMovimiento_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                if (seleccionado.Status == MovimientoBancarioStatusEnum.Cancelado) { return; }
                if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                    seleccionado.Status = MovimientoBancarioStatusEnum.Cancelado;
                    using (var espera = new Waiting1Form(this.Aplicar)) {
                        espera.Text = "Aplicando cambios ...";
                        espera.ShowDialog(this);
                    }
                    if (this.TMovimiento.Tag != null) {
                        if ((bool)this.TMovimiento.Tag == false) {
                            MessageBox.Show("Error");
                            this.TMovimiento.Tag = null;
                        }
                    }
                }
            }
        }

        public virtual void TMovimiento_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            var combo = this.Movimientos.GridData.Columns["Status"] as GridViewComboBoxColumn;
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "Id";
            combo.DataSource = BancoService.GetStatus();
            this.Movimientos.GridData.DataSource = this.Movimientos.Data;
        }

        public virtual void TMovimiento_Auxiliar_Click(object sender, EventArgs e) {
            var aux = new MovimientoUploadAuxForm(this.Service);
            aux.ShowDialog(this);
        }

        public virtual void TMovimiento_Exportar_Click(object sender, EventArgs e) {
            using (var exportar = new TelerikGridExportForm(this.Movimientos.GridData)) {
                exportar.ShowDialog(this);
            }
        }

        public virtual void TMovimiento_ExportarT_Click(object sender, EventArgs e) {
            var templete = new SaveFileDialog() { Filter = "*.xlsx|*.XLSX", Title = "Selecciona un templete válido para esta acción." };
            if (templete.ShowDialog() == DialogResult.OK) {
                var save = new SaveFileDialog() { Filter = "*.xlsx|*.XLSX", Title = "Guardar archivo como:" };
                if (save.ShowDialog() == DialogResult.OK) {
                    BancoService.ExportarExcel(save.FileName, templete.FileName, this.Movimientos.Data);
                }
            }
        }

        public virtual void TMovimiento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void TContext_Auxiliar_Click(object sender, EventArgs e) {

        }
        #endregion

        #region metodos privados
        /// <summary>
        /// evento al actualizar un registro
        /// </summary>
        private void Movimientos_Actualizar(object sender, IMovimientoBancarioDetailModel e) {
            using (var espera = new Waiting1Form(this.Aplicar)) {
                espera.Text = "Aplicando cambios ...";
                espera.ShowDialog(this);
            }
            if (this.TMovimiento.Tag != null) {
                if ((bool)this.TMovimiento.Tag == false) {
                    MessageBox.Show("Error");
                    this.TMovimiento.Tag = null;
                }
            }
        }

        public virtual void Movimientos_Auditado(object sender, IMovimientoBancarioDetailModel e) {
            //MessageBox.Show(this, "Evento Auditado");
            //throw new NotImplementedException();
        }

        public virtual void Consultar() {
            var query = BancoService.Query().Year(this.TMovimiento.GetEjercicio()).Month(this.TMovimiento.GetMes()).WithEfecto(this.efecto).Build();
            var d1 = this.Service.GetList<MovimientoBancarioDetailModel>(query).ToList<IMovimientoBancarioDetailModel>();
            this.Movimientos.Data = new System.ComponentModel.BindingList<IMovimientoBancarioDetailModel>(d1);
        }

        public virtual void Aplicar() {
            var seleccionado = this.Movimientos.Current();
            this.TMovimiento.Tag = this.Service.Aplicar(seleccionado);
        }
        #endregion
    }
}
