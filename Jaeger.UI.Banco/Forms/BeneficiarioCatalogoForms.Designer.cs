﻿namespace Jaeger.UI.Banco.Forms
{
    partial class BeneficiarioCatalogoForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BeneficiarioCatalogoForms));
            this.TBeneficiario = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TBeneficiario
            // 
            this.TBeneficiario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TBeneficiario.Location = new System.Drawing.Point(0, 0);
            this.TBeneficiario.Name = "TBeneficiario";
            this.TBeneficiario.Size = new System.Drawing.Size(902, 544);
            this.TBeneficiario.TabIndex = 0;
            // 
            // BeneficiarioCatalogoForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 544);
            this.Controls.Add(this.TBeneficiario);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BeneficiarioCatalogoForms";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bancos: Beneficiarios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BeneficiarioCatalogoForms_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.GridStandarControl TBeneficiario;
    }
}
