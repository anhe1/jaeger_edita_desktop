﻿
namespace Jaeger.UI.Banco.Forms {
    partial class MovimientoUploadAuxForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovimientoUploadAuxForm));
            this.GAuxiliar = new Telerik.WinControls.UI.RadGridView();
            this.TAuxiliar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TStatus = new Telerik.WinControls.UI.RadStatusStrip();
            this.Status = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.GAuxiliar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAuxiliar.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GAuxiliar
            // 
            this.GAuxiliar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GAuxiliar.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GAuxiliar.MasterTemplate.AllowAddNewRow = false;
            this.GAuxiliar.MasterTemplate.AllowRowResize = false;
            this.GAuxiliar.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GAuxiliar.Name = "GAuxiliar";
            this.GAuxiliar.ShowGroupPanel = false;
            this.GAuxiliar.Size = new System.Drawing.Size(684, 315);
            this.GAuxiliar.TabIndex = 3;
            // 
            // TAuxiliar
            // 
            this.TAuxiliar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TAuxiliar.Etiqueta = "";
            this.TAuxiliar.Location = new System.Drawing.Point(0, 0);
            this.TAuxiliar.Name = "TAuxiliar";
            this.TAuxiliar.ReadOnly = false;
            this.TAuxiliar.ShowActualizar = true;
            this.TAuxiliar.ShowAutorizar = false;
            this.TAuxiliar.ShowCerrar = true;
            this.TAuxiliar.ShowEditar = false;
            this.TAuxiliar.ShowExportarExcel = false;
            this.TAuxiliar.ShowFiltro = false;
            this.TAuxiliar.ShowGuardar = true;
            this.TAuxiliar.ShowHerramientas = false;
            this.TAuxiliar.ShowImagen = false;
            this.TAuxiliar.ShowImprimir = false;
            this.TAuxiliar.ShowNuevo = true;
            this.TAuxiliar.ShowRemover = true;
            this.TAuxiliar.Size = new System.Drawing.Size(684, 30);
            this.TAuxiliar.TabIndex = 0;
            // 
            // TStatus
            // 
            this.TStatus.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Status});
            this.TStatus.Location = new System.Drawing.Point(0, 345);
            this.TStatus.Name = "TStatus";
            this.TStatus.Size = new System.Drawing.Size(684, 26);
            this.TStatus.SizingGrip = false;
            this.TStatus.TabIndex = 2;
            // 
            // Status
            // 
            this.Status.Name = "Status";
            this.TStatus.SetSpring(this.Status, false);
            this.Status.Text = "Se muestra solo los archivos con referencias a movimientos registrados.";
            this.Status.TextWrap = true;
            // 
            // MovimientoUploadAuxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 371);
            this.Controls.Add(this.GAuxiliar);
            this.Controls.Add(this.TStatus);
            this.Controls.Add(this.TAuxiliar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MovimientoUploadAuxForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Importar";
            this.Load += new System.EventHandler(this.MovimientoUploadAuxForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GAuxiliar.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GAuxiliar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TAuxiliar;
        protected internal Telerik.WinControls.UI.RadGridView GAuxiliar;
        private Telerik.WinControls.UI.RadStatusStrip TStatus;
        private Telerik.WinControls.UI.RadLabelElement Status;
    }
}