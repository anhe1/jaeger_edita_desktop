﻿using System;
using System.Linq;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Banco.Forms {
    public partial class MovimientoBancarioForm : RadForm {
        #region declaraciones
        protected internal BindingList<IBancoCuentaDetailModel> _CuentasBancarias; 
        protected internal BindingList<BeneficiarioDetailModel> _Beneficiarios = new BindingList<BeneficiarioDetailModel>();
        protected internal BindingList<MovimientoConceptoDetailModel> _TiposMovimiento;
        protected internal MovimientoConceptoDetailModel _CurrentTipoMovimiento;
        protected internal IConfiguration _Configuration = new Configuration();
        #endregion

        public MovimientoBancarioForm() {
            InitializeComponent();
        }

        public MovimientoBancarioForm(Aplication.Banco.IBancoService service) {
            InitializeComponent();
            this.TMovimiento.Service = service;
            this.TMovimiento.Movimiento = null;
        }

        public MovimientoBancarioForm(IMovimientoBancarioDetailModel movimiento, Aplication.Banco.IBancoService service) {
            InitializeComponent();
            this.TMovimiento.Service = service;
            this.TMovimiento.Movimiento = movimiento;
        }

        private void MovimientoBancarioForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.TComprobante.Nuevo.Visibility = ElementVisibility.Collapsed;
            this.Preparar.RunWorkerAsync();
            this.FechaDocumento.Value = DateTime.Now;
            this.FechaDocumento.SetToNullValue();
            this.FechaAplicacion.Value = DateTime.Now;
            this.FechaAplicacion.SetToNullValue();

            // tooltip de los controles
            this.NumDocto.TextBoxElement.ToolTipText = "Número de cheque o documento.";
            this.Referencia.TextBoxElement.ToolTipText = "Proporcionar detalles sobre el propósito de la transacción utilizado principalmente en layout bancario.";
            this.NumAuto.TextBoxElement.ToolTipText = "Número de autorización proporcionado por el Banco";
            this.NumOperacion.TextBoxElement.ToolTipText = "Número de autorización proporcionado por el Banco";
            this.Observaciones.TextBoxElement.ToolTipText = "Notas personalizadas";
            this.FechaDocumento.DateTimePickerElement.ToolTipText = "Fecha del documento";
            this.FechaAplicacion.DateTimePickerElement.ToolTipText = "Fecha de aplicación del movimiento";

            this.ConceptoMovimiento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowRowResize = false;
            this.ConceptoMovimiento.EditorControl.TableElement.Size = new Size(300, 200);
            this.ConceptoMovimiento.EditorControl.AllowRowResize = false;
            this.ConceptoMovimiento.EditorControl.AllowSearchRow = true;

            this.ConceptoMovimiento.AutoSizeDropDownToBestFit = true;
            this.ConceptoMovimiento.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.ConceptoMovimiento.MultiColumnComboBoxElement.DropDownAnimationEnabled = true;

            this.TMovimiento.Guardar.Click += this.TMovimiento_Guardar_Click;
            this.TMovimiento.Imprimir.Click += this.TMovimiento_Imprimir_Click;
            this.TMovimiento.Cerrar.Click += this.TMovimiento_Cerrar_Click;

            this.TComprobante.Buscar.Click += this.TComprobante_Buscar_Click;
            this.TComprobante.Agregar.Click += this.TComprobante_Agregar_Click;
            this.TComprobante.Remover.Click += this.TComprobante_Remover_Click;

            this.TAuxiliar.Nuevo.Click += this.TAuxiliar_Nuevo_Click;
            this.TAuxiliar.Remover.Click += this.TAuxiliar_Remover_Click;
        }

        #region barra de herramientas
        protected virtual void TMovimiento_Nuevo_Click(object sender, EventArgs e) {
            this.TMovimiento.Movimiento = new MovimientoBancarioDetailModel();
            this.TMovimiento.Actualizar.PerformClick();
        }

        protected virtual void TMovimiento_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == true) {
                using (var espera = new Common.Forms.Waiting1Form(this.GuardarP)) {
                    espera.Text = "Guardando ...";
                    espera.ShowDialog(this);
                    BindingCompleted(sender, e);
                }
            } else {
                RadMessageBox.Show(this, Properties.Resources.msg_Banco_Movimiento_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
            }
        }

        protected virtual void TMovimiento_Guardar_Auxiliar_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.Waiting2Form(this.GuardarAuxiliar)) {
                espera.Text = "Guardando auxiliar";
                espera.ShowDialog(this);
            }
        }

        protected virtual void TMovimiento_Imprimir_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(this.TMovimiento.Movimiento));
                imprimir.Show();
            }
        }

        protected virtual void Beneficiario_Buscar_Click(object sender, EventArgs e) {
            var _buscar = new BeneficiarioBuscarForm(this._CurrentTipoMovimiento.DirectorioSelected, this.TMovimiento.Service);
            _buscar.Selected += Buscar_Selected;
            _buscar.ShowDialog(this);
        }

        protected virtual void Buscar_Selected(object sender, BeneficiarioDetailModel e) {
            if (e != null) {
                this.Beneficiario.Text = e.Nombre;
            }
        }

        protected virtual void TMovimiento_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de comprobante
        protected virtual void TComprobante_Buscar_Click(object sender, EventArgs e) {
            if (this.ConceptoMovimiento.SelectedItem != null) {
                this._CurrentTipoMovimiento = ((GridViewDataRowInfo)this.ConceptoMovimiento.SelectedItem).DataBoundItem as MovimientoConceptoDetailModel;
                var buscar = new BuscarComprobanteForm(this._CurrentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio, this.TComprobante.GetTipoComprobante(), this.TMovimiento.Service);
                buscar.AgregarComprobante += TComprobante_Agregar_Click;
                buscar.ShowDialog(this);
            }
        }

        protected virtual void TComprobante_Agregar_Click(object sender, MovimientoBancarioComprobanteDetailModel e) {
            if (e != null) {
                if (this.TMovimiento.Movimiento.Agregar(e) == true)
                    RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Duplicado, "Atención", MessageBoxButtons.OK);
            }
        }

        protected virtual void TComprobante_Agregar_Click(object sender, EventArgs e) {
            var selectedValue = this.TComprobante.TDocumento.SelectedItem as GridViewRowInfo;
            if (selectedValue != null) {
                var seleccionado = selectedValue.DataBoundItem as MovimientoBancarioComprobanteDetailModel;
                if (seleccionado != null)
                    if (this.TMovimiento.Movimiento.Agregar(seleccionado) == true)
                        RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Duplicado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        protected virtual void TComprobante_Remover_Click(object sender, EventArgs e) {
            if (this.GridComprobantes.CurrentRow == null) { return; }
            if (RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                if (this.GridComprobantes.CurrentRow != null) {
                    var seleccionado = this.GridComprobantes.CurrentRow.DataBoundItem as MovimientoBancarioComprobanteDetailModel;
                    if (seleccionado.Id <= 0) {
                        this.GridComprobantes.Rows.Remove(this.GridComprobantes.CurrentRow);
                    } else {
                        this.GridComprobantes.CurrentRow.Cells["Activo"].Value = false;
                    }
                    this.GridComprobantes.Refresh();
                }
            }
        }
        #endregion

        #region barra de herramientas auxiliar
        protected virtual void TAuxiliar_Nuevo_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.*|*.*|*.xml|*.XML|*.xls|*.XLS|*.xlsx|*.XLSX|*.pdf|*.PDF" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var info = new System.IO.FileInfo(openFile.FileName);
                if (info.Length > 5000000) {
                    MessageBox.Show("Nop muy grande!");
                } else {
                    // crear registro para el axuliar
                    var auxiliar = new MovimientoBancarioAuxiliarDetailModel {
                        Descripcion = System.IO.Path.GetFileName(openFile.FileName),
                        FileName = System.IO.Path.GetFileName(openFile.FileName),
                        Base64 = Util.FileService.ReadFileB64(openFile.FileName),
                        Tag = openFile.FileName,
                        IdMovimiento = this.TMovimiento.Movimiento.Id
                    };
                    this.TMovimiento.Movimiento.Auxiliar.Add(auxiliar);
                }
            }
        }

        protected virtual void TAuxiliar_Remover_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                var seleccionado = this.GridAuxiliares.ReturnRowSelected() as MovimientoBancarioAuxiliarDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdAuxiliar == 0) {
                        this.GridAuxiliares.Rows.Remove(this.GridAuxiliares.CurrentRow);
                    } else {
                        RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Remover_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }
        #endregion

        #region acciones del grid auxiliares
        private void RadGridAuxiliares_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            var seleccionado = this.GridAuxiliares.ReturnRowSelected() as MovimientoBancarioAuxiliarDetailModel;
            if (seleccionado != null) {
                if (seleccionado.IdAuxiliar == 0) {
                    e.Cancel = false;
                    return;
                }
            }
            e.Cancel = true;
        }

        private void RadGridAuxiliares_CommandCellClick(object sender, GridViewCellEventArgs e) {
            var seleccionado = this.GridAuxiliares.ReturnRowSelected() as MovimientoBancarioAuxiliarDetailModel;
            if (seleccionado != null) {
                if (ValidacionService.URL(seleccionado.URL)) {
                    var saveFile = new SaveFileDialog {
                        FileName = seleccionado.FileName
                    };
                    if (saveFile.ShowDialog(this) == DialogResult.OK) {
                        if (Util.Downloads.File(seleccionado.URL, saveFile.FileName) == false) {
                            RadMessageBox.Show(this, Properties.Resources.msg_Banco_Auxiliar_Descargar_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                            return;
                        } else {
                            RadMessageBox.Show(this, string.Format(Properties.Resources.msg_Banco_Auxiliar_Descargar_Correcto, saveFile.FileName), "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                        }
                    }
                }
            }
        }
        #endregion

        #region eventos del control
        protected virtual void FormaPago_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.FormaPago.SelectedItem == null) { return; }
            var _seleccionado = ((GridViewDataRowInfo)this.FormaPago.SelectedItem).DataBoundItem as BancoFormaPagoModel;
            if (_seleccionado != null) {
                if (this._CurrentTipoMovimiento != null) {
                    this.TMovimiento.Movimiento.FormaPagoDescripcion = _seleccionado.Descripcion;
                    this.TMovimiento.Movimiento.AfectaSaldoCuenta = _seleccionado.AfectarSaldo;
                }
            }
        }

        protected virtual void ConceptoMovimiento_TextChanged(object sender, EventArgs e) {
            if (this.ConceptoMovimiento.SelectedItem == null)
                return;
            this._CurrentTipoMovimiento = ((GridViewDataRowInfo)this.ConceptoMovimiento.SelectedItem).DataBoundItem as MovimientoConceptoDetailModel;
            
            if (this._CurrentTipoMovimiento != null) {
                if (_CurrentTipoMovimiento.Efecto == MovimientoBancarioEfectoEnum.Ingreso) {
                    this.GridComprobantes.Columns["Cargo"].IsVisible = true;
                    this.GridComprobantes.Columns["Abono"].IsVisible = false;
                    this.GridComprobantes.Columns["EmisorNombre"].IsVisible = false;
                    this.GridComprobantes.Columns["ReceptorNombre"].IsVisible = true;
                } else if (_CurrentTipoMovimiento.Efecto == MovimientoBancarioEfectoEnum.Egreso) {
                    this.GridComprobantes.Columns["Cargo"].IsVisible = false;
                    this.GridComprobantes.Columns["Abono"].IsVisible = true;
                    this.GridComprobantes.Columns["EmisorNombre"].IsVisible = true;
                    this.GridComprobantes.Columns["ReceptorNombre"].IsVisible = false;
                }
            }
        }

        protected virtual void ConceptoMovimiento_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.ConceptoMovimiento.SelectedItem == null)
                return;
            this._CurrentTipoMovimiento = ((GridViewDataRowInfo)this.ConceptoMovimiento.SelectedItem).DataBoundItem as MovimientoConceptoDetailModel;
            
            if (this._CurrentTipoMovimiento != null) {
             
                this.TMovimiento.Movimiento.AfectaSaldoComprobantes = this._CurrentTipoMovimiento.AfectaSaldoComprobantes;
                this.TMovimiento.Movimiento.Tipo = this._CurrentTipoMovimiento.Efecto;
                this.TMovimiento.Movimiento.IdConcepto = this._CurrentTipoMovimiento.IdConcepto;
                this.TMovimiento.Movimiento.IdTipoOperacion = this._CurrentTipoMovimiento.IdTipoOperacion;
                this.TMovimiento.Movimiento.Concepto = this._CurrentTipoMovimiento.Concepto;
                this.TMovimiento.Movimiento.FormatoImpreso = (MovimientoBancarioFormatoImpreso)this._CurrentTipoMovimiento.IdFormatoImpreso;
                this.TComprobante.Documento.DataSource = this._CurrentTipoMovimiento.ObjetosComprobante.Where(it => it.Selected);
                this.TComprobante.Documento.DisplayMember = "Name";
                this.TComprobante.Documento.ValueMember = "Id";
                this.GridComprobantes.Enabled = this._CurrentTipoMovimiento.RequiereComprobantes;
                if (this.TMovimiento.Movimiento.Id == 0) {
                    this.TMovimiento.Movimiento.Importe = 0;
                    if (this.TMovimiento.Movimiento.Comprobantes.Count > 0) {
                        var abono = this.TMovimiento.Movimiento.GetTotalAbono();
                        var cargo = this.TMovimiento.Movimiento.GetTotalCargo();
                        if (abono > 0) {
                            this.TMovimiento.Movimiento.Importe = abono;
                        } else {
                            this.TMovimiento.Movimiento.Importe = cargo;
                        }
                    }
                }
            }
        }

        protected virtual void ConceptoMovimiento_DropDownClosed(object sender, RadPopupClosedEventArgs args) {
            if (this._CurrentTipoMovimiento != null) {
                using (var espera = new Common.Forms.Waiting1Form(this.GetBeneficiarios)) {
                    espera.Text = "Obteniendo beneficiarios ...";
                    espera.ShowDialog(this);
                }
                this.Beneficiario.DataSource = this._Beneficiarios;
                this.Beneficiario.DisplayMember = "Nombre";
                this.Beneficiario.ValueMember = "Id";
                this.Beneficiario.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
                this.Beneficiario.AutoFilter = true;
                this.Beneficiario.EditorControl.AllowRowResize = false;
                this.Beneficiario.EditorControl.AllowSearchRow = true;
                this.Beneficiario.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
                this.Beneficiario.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.Beneficiario.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
                this.Beneficiario.Enabled = this._Beneficiarios.Count > 0;
                if (this._Beneficiarios.Count <= 0)
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_Beneniciarios_Sin, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        protected virtual void CuentaOrigen_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.CuentaBancariaP.SelectedItem == null)
                return;
            var seleccionado = ((GridViewDataRowInfo)this.CuentaBancariaP.SelectedItem).DataBoundItem as BancoCuentaDetailModel;
            if (seleccionado != null) {
                this.TMovimiento.Movimiento.IdCuentaP = seleccionado.IdCuenta;
                this.TMovimiento.Movimiento.NumeroCuentaP = seleccionado.NumCuenta;
                this.TMovimiento.Movimiento.BancoP = seleccionado.Banco;
                this.TMovimiento.Movimiento.CuentaCLABEP = seleccionado.CLABE;
                this.TMovimiento.Movimiento.AliasP = seleccionado.Alias;
                this.TMovimiento.Movimiento.SucursalP = seleccionado.Sucursal;
                this.TMovimiento.Movimiento.BeneficiarioP = seleccionado.Beneficiario;
                this.TMovimiento.Movimiento.BeneficiarioRFCP = seleccionado.RFCBenficiario;
                this.TMovimiento.Movimiento.ClaveBancoP = seleccionado.ClaveBanco;
            }
        }

        protected virtual void Beneficiario_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.Beneficiario.SelectedItem == null)
                return;
            var seleccionado = ((GridViewDataRowInfo)this.Beneficiario.SelectedItem).DataBoundItem as BeneficiarioDetailModel;
            if (seleccionado != null) {
                this.TMovimiento.Movimiento.IdDirectorio = seleccionado.IdDirectorio;
                this.TMovimiento.Movimiento.BeneficiarioT = seleccionado.Nombre;
                this.TMovimiento.Movimiento.BeneficiarioRFCT = seleccionado.RFC;
                this.TMovimiento.Movimiento.NumeroCuentaT = "";
                this.TMovimiento.Movimiento.BancoT = "";
                this.TMovimiento.Movimiento.CuentaCLABET = "";
                this.TMovimiento.Movimiento.SucursalT = "";
                this.TMovimiento.Movimiento.ClaveBancoT = "";
                this.NumeroCuentaT.SelectedIndexChanged -= NumeroCuentaT_SelectedIndexChanged;
                this.NumeroCuentaT.DataSource = seleccionado.CuentasBancarias;
                this.NumeroCuentaT.DisplayMember = "NumeroDeCuenta";
                this.NumeroCuentaT.SelectedIndex = -1;
                this.NumeroCuentaT.SelectedIndexChanged += NumeroCuentaT_SelectedIndexChanged;
            }
        }

        protected virtual void NumeroCuentaT_SelectedIndexChanged(object sender, EventArgs e) {
            var temporal = this.NumeroCuentaT.SelectedItem as GridViewDataRowInfo;
            if (temporal != null) {
                var seleccionado = ((GridViewDataRowInfo)this.NumeroCuentaT.SelectedItem).DataBoundItem as Domain.Contribuyentes.Entities.CuentaBancariaModel;
                if (seleccionado != null) {
                    this.TMovimiento.Movimiento.IdCuentaT = seleccionado.IdCuenta;
                    this.TMovimiento.Movimiento.NumeroCuentaT = seleccionado.NumeroDeCuenta;
                    this.TMovimiento.Movimiento.BancoT = seleccionado.Banco;
                    this.TMovimiento.Movimiento.CuentaCLABET = seleccionado.Clabe;
                    this.TMovimiento.Movimiento.SucursalT = seleccionado.Sucursal;
                    this.TMovimiento.Movimiento.ClaveBancoT = seleccionado.Clave;
                } else {
                    this.TMovimiento.Movimiento.NumeroCuentaT = "";
                    this.TMovimiento.Movimiento.BancoT = "";
                    this.TMovimiento.Movimiento.CuentaCLABET = "";
                    this.TMovimiento.Movimiento.SucursalT = "";
                    this.TMovimiento.Movimiento.ClaveBancoT = "";
                }
            }
        }

        protected virtual void BindingCompleted(object sender, EventArgs e) {
            this.ConceptoMovimiento.DataBindings.Clear();
            this.ConceptoMovimiento.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "IdConcepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaRegistro.DataBindings.Clear();
            this.FechaRegistro.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaEmision", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FormaPago.DataBindings.Clear();
            this.FormaPago.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "ClaveFormaPago", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumDocto.DataBindings.Clear();
            this.NumDocto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumDocto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaDocumento.DataBindings.Clear();
            this.FechaDocumento.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaDocto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaAplicacion.DataBindings.Clear();
            this.FechaAplicacion.DataBindings.Add("Value", this.TMovimiento.Movimiento, "FechaAplicacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CuentaBancariaP.DataBindings.Clear();
            this.CuentaBancariaP.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "IdCuentaP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumeroCuentaP.DataBindings.Clear();
            this.NumeroCuentaP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumeroCuentaP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoP.DataBindings.Clear();
            this.BancoP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BancoP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CuentaClabeP.DataBindings.Clear();
            this.CuentaClabeP.DataBindings.Add("Text", this.TMovimiento.Movimiento, "CuentaCLABEP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Beneficiario.DataBindings.Clear();
            this.Beneficiario.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BeneficiarioT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BeneficiarioRFC.DataBindings.Clear();
            this.BeneficiarioRFC.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BeneficiarioRFCT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumeroCuentaT.DataBindings.Clear();
            this.NumeroCuentaT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumeroCuentaT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoT.DataBindings.Clear();
            this.BancoT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "BancoT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ClabeT.DataBindings.Clear();
            this.ClabeT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "CuentaCLABET", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SucursalT.DataBindings.Clear();
            this.SucursalT.DataBindings.Add("Text", this.TMovimiento.Movimiento, "SucursalT", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Concepto.DataBindings.Clear();
            this.Concepto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Concepto", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Referencia.DataBindings.Clear();
            this.Referencia.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumAuto.DataBindings.Clear();
            this.NumAuto.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumAutorizacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumOperacion.DataBindings.Clear();
            this.NumOperacion.DataBindings.Add("Text", this.TMovimiento.Movimiento, "NumOperacion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Observaciones.DataBindings.Clear();
            this.Observaciones.DataBindings.Add("Text", this.TMovimiento.Movimiento, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PorComprobar.DataBindings.Clear();
            this.PorComprobar.DataBindings.Add("Checked", this.TMovimiento.Movimiento, "PorComprobar", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TipoCambio.DataBindings.Clear();
            this.TipoCambio.DataBindings.Add("Text", this.TMovimiento.Movimiento, "TipoCambio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("SelectedValue", this.TMovimiento.Movimiento, "ClaveMoneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Importe.DataBindings.Clear();
            this.Importe.DataBindings.Add("Value", this.TMovimiento.Movimiento, "Importe", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Importe.ReadOnly = this.TMovimiento.Movimiento.IsEditable;

            this.GridComprobantes.DataSource = this.TMovimiento.Movimiento.Comprobantes;
            this.GridAuxiliares.DataSource = this.TMovimiento.Movimiento.Auxiliar;

            if (TMovimiento.Movimiento.Tipo == MovimientoBancarioEfectoEnum.Ingreso) {
                this.GridComprobantes.Columns["Abono"].IsVisible = false;
            } else if (TMovimiento.Movimiento.Tipo == MovimientoBancarioEfectoEnum.Egreso) {
                this.GridComprobantes.Columns["Cargo"].IsVisible = false;
            }

            Desabilitar();
        }
        #endregion

        #region metodos privados
        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.GetCuentasBancarias();
            this.GetTiposMovimiento();
            this.Moneda.DataSource = this.TMovimiento.Service.GetMonedas();
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CuentaBancariaP.DataSource = new BindingList<IBancoCuentaDetailModel>(this._CuentasBancarias);
            this.CuentaBancariaP.SelectedIndex = -1;

            this.FormaPago.DataSource = this.TMovimiento.Service.GetBancoFormaPago();
            this.FormaPago.SelectedIndex = -1;

            this.ConceptoMovimiento.DataSource = this._TiposMovimiento;
            this.ConceptoMovimiento.SelectedIndex = -1;
            this.ConceptoMovimiento.DataSource = this._TiposMovimiento;
            this.TMovimiento.Actualizar.PerformClick();
            this.ConceptoMovimiento.SelectedIndexChanged += new EventHandler(this.ConceptoMovimiento_SelectedIndexChanged);
            this.CuentaBancariaP.SelectedIndexChanged += new EventHandler(this.CuentaOrigen_SelectedIndexChanged);
            this.Beneficiario.SelectedIndexChanged += Beneficiario_SelectedIndexChanged;
            this.NumeroCuentaT.SelectedIndexChanged += NumeroCuentaT_SelectedIndexChanged;
        }

        protected virtual void GetCuentasBancarias() {
            this._CuentasBancarias = new BindingList<IBancoCuentaDetailModel>(
                this.TMovimiento.Service.GetList<BancoCuentaDetailModel>(
                    Aplication.Banco.BancoCuentaService.Query().OnlyActive().Build()).ToList<IBancoCuentaDetailModel>());
        }

        protected virtual void GetTiposMovimiento() {
            this._TiposMovimiento = new BindingList<MovimientoConceptoDetailModel>(
                this.TMovimiento.Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().OnlyActive().Build()
                ).ToList());
        }

        protected virtual void GetBeneficiarios() {
            Console.WriteLine(this._CurrentTipoMovimiento.ObjetosDirectorio);
            if (this._CurrentTipoMovimiento.DirectorioSelected.Count > 0) {
                this._Beneficiarios = this.TMovimiento.Service.GetBeneficiarios(this._CurrentTipoMovimiento.DirectorioSelected);
            } else {
                this._Beneficiarios = new BindingList<BeneficiarioDetailModel>();
            }
        }

        private void GuardarP() {
            this.TMovimiento.Movimiento = this.TMovimiento.Service.Save(this.TMovimiento.Movimiento);
        }

        private void GuardarAuxiliar() {
            var d0 = this.TMovimiento.Service.Save(this.TMovimiento.Movimiento.Auxiliar.ToList(), this.TMovimiento.Movimiento.Identificador);
            this.TMovimiento.Movimiento.Auxiliar = new BindingList<MovimientoBancarioAuxiliarDetailModel>(d0);
        }

        private bool Validar() {
            this.Advertencia.Clear();
            if (this.TMovimiento.Movimiento.IdConcepto <= 0) {
                this.Advertencia.SetError(this.ConceptoMovimiento, Properties.Resources.msg_Banco_Concepto_NoValido);
                return false;
            }

            if (this.TMovimiento.Movimiento.IdDirectorio <= 0 && this.Beneficiario.Enabled == true) {
                this.Advertencia.SetError(this.Beneficiario, "Selecciona un beneficiario válido!");
                return false;
            }

            if (string.IsNullOrEmpty(this.TMovimiento.Movimiento.ClaveFormaPago)) {
                this.Advertencia.SetError(this.FormaPago, Properties.Resources.msg_Banco_Movimiento_FormaPago_NoValida);
                return false;
            }

            if (string.IsNullOrEmpty(this.TMovimiento.Movimiento.Concepto)) {
                this.Advertencia.SetError(this.Concepto, "Es necesario un concepto para la operación.");
                return false;
            }

            if (this.TMovimiento.Movimiento.FechaDocto == null) {
                RadMessageBox.Show(this, "Indica una fecha para el documento.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                this.Advertencia.SetError(this.FechaDocumento, "Indica una fecha para el documento.");
                return false;
            }

            if (this.TMovimiento.Movimiento.Comprobantes.Count <= 0) {
                if (this.TMovimiento.Movimiento.Id == 0) {
                    if (RadMessageBox.Show(this, "No se indicaron comprobantes relacionados a este movimiento. ¿Es un moviento por comprobar?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                        this.TMovimiento.Movimiento.PorComprobar = true;
                    } else {
                        this.Advertencia.SetError(this.Concepto, "No se indicaron comprobantes relacionados a este movimiento.");
                        return false;
                    }
                }
            }

            if (this.TMovimiento.Movimiento.Id > 0) {
                if (this.TMovimiento.Movimiento.PorComprobar) {
                    if (this.TMovimiento.Movimiento.Comprobado == false) {
                        RadMessageBox.Show(this, "Los importes para el movimiento por comprobar no coinciden.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                        return false;
                    } else {
                        this.TMovimiento.Movimiento.PorComprobar = false;
                    }
                }
            }

            //if (this.Comprobar() == false) {
            //    RadMessageBox.Show(this, "Los importes en los comprobantes no coinciden.", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
            //    return false;
            //}

            return true;
        }

        private bool Comprobar() {
            // comprobar los importes no sean mayores al importe total del comprobante
            foreach (var item in this.TMovimiento.Movimiento.Comprobantes) {
                if (Math.Round((item.Acumulado + item.Abono + item.Cargo), this._Configuration.Decimales) != Math.Round(item.Total, this._Configuration.Decimales)) {
                    return false;
                }
            }
            return true;
        }

        private void Desabilitar() {
            this.ConceptoMovimiento.ReadOnly = !(this.TMovimiento.Movimiento.Id == 0);

            this.FechaRegistro.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.FechaDocumento.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.FormaPago.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.FechaAplicacion.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.CuentaBancariaP.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.Beneficiario.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.NumeroCuentaT.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.BancoT.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.Moneda.Enabled = this.TMovimiento.Movimiento.Id == 0;

            this.TipoCambio.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumOperacion.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumAuto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.NumDocto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Referencia.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Concepto.ReadOnly = this.TMovimiento.Movimiento.Id > 0;
            this.Observaciones.ReadOnly = this.TMovimiento.Movimiento.Id > 0;

            this.SearchBeneficiario.Enabled = !(this.TMovimiento.Movimiento.Id > 0);
            this.RefreshBeneficiarios.Enabled = !(this.TMovimiento.Movimiento.Id > 0);

            this.PorComprobar.Enabled = this.TMovimiento.Movimiento.Id == 0;
            this.TMovimiento.ShowImprimir = this.TMovimiento.Movimiento.Id > 0;
            this.GridComprobantes.AllowEditRow = !(this.TMovimiento.Movimiento.Id == 0 == this.TMovimiento.Movimiento.PorComprobar == true);

            this.TMovimiento.Enabled = true;
            this.PanelCuentaP.Enabled = true;
            this.PanelCuentaT.Enabled = true;
            this.ComprobantesPages.Enabled = true;
            this.TComprobante.ReadOnly = (this.TMovimiento.Movimiento.Id == 0 == this.TMovimiento.Movimiento.PorComprobar == true);

            if ((this.TMovimiento.Movimiento.Id == 0 == this.TMovimiento.Movimiento.PorComprobar == true)) {
                this.TMovimiento.Guardar.Click -= this.TMovimiento_Guardar_Click;
                this.TMovimiento.Guardar.Click += this.TMovimiento_Guardar_Auxiliar_Click;
            }

            if ((this.TMovimiento.Movimiento.Id > 0 == this.TMovimiento.Movimiento.PorComprobar == true)) {
                this.TComprobante.Documento.DataSource = this._CurrentTipoMovimiento.ObjetosComprobante.Where(it => it.Selected);
                this.TComprobante.Documento.DisplayMember = "Name";
                this.TComprobante.Documento.ValueMember = "Id";
            }
        }
        #endregion

        #region acciones del grid
        private void Comprobantes_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            e.Cancel = true;
        }
        #endregion
    }
}
