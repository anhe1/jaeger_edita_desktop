﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.UI.Banco.Forms {
    public partial class TbMovimientoBancarioControl : UserControl {
        public IBancoService Service;
        public event EventHandler<EventArgs> BindingCompleted;

        public TbMovimientoBancarioControl() {
            InitializeComponent();
        }

        private void TbMovimientoBancarioControl_Load(object sender, EventArgs e) {
            this.ToolBar.OverflowButton.Visibility = ElementVisibility.Hidden;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            this.Status.DataSource = BancoService.GetStatus();
            this.Nuevo.Click += new System.EventHandler(this.Nuevo_Click);
            this.Actualizar.Click += new System.EventHandler(this.Actualizar_Click);
        }

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }

        #region propiedades del control
        [Description("Mostar el botón Nuevo"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Nuevo.Visibility = ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Cancelar"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cancelar.Visibility = ElementVisibility.Visible;
                else
                    this.Cancelar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Cancelar"), Category("Botones")]
        public bool ShowDuplicar {
            get {
                return this.Duplicar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Duplicar.Visibility = ElementVisibility.Visible;
                else
                    this.Duplicar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Guardar"), Category("Botones")]
        public bool ShowGuardar {
            get {
                return this.Guardar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Guardar.Visibility = ElementVisibility.Visible;
                else
                    this.Guardar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Actualizar"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Imprimir"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Herramientas"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Cerrar"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
            }
        }
        #endregion

        public IMovimientoBancarioDetailModel Movimiento {
            get; set;
        }

        #region barra de herramientas
        private void Nuevo_Click(object sender, EventArgs e) {
            this.Movimiento = null;
            this.Actualizar.PerformClick();
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            if (this.Movimiento == null) {
                this.Movimiento = new MovimientoBancarioDetailModel();
            }
            this.CreateBinding();
        }

        #endregion

        private void CreateBinding() {
            this.Identificador.DataBindings.Clear();
            this.Identificador.DataBindings.Add("Text", this.Movimiento, "Identificador", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Status.SetEditable(this.Movimiento.Status == MovimientoBancarioStatusEnum.Cancelado);
            this.Status.DataBindings.Clear();
            this.Status.DataBindings.Add("SelectedValue", this.Movimiento, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);
            this.OnBindingClompleted(new EventArgs());
        }

        private void LayoutBanamexC_Click(object sender, EventArgs e) {
            using (var espera = new UI.Common.Forms.Waiting1Form(this.CrearLayout)) {
                espera.Text = "Creando ...";
                espera.ShowDialog(this);
            }

            if (this.Tag != null) {
                Clipboard.SetDataObject((string)this.Tag);
                MessageBox.Show((string)this.Tag);
            }
            this.Tag = null;
        }

        private void CrearLayout() {
            //this.Tag = this.Service.LayoutBanamex(this.Movimiento, "Prueba", true);
        }
    }
}
