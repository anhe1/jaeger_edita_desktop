﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.UI.Banco.Builder;

namespace Jaeger.UI.Banco.Forms {
    public class FormaPagoGridControl : Common.Forms.GridStandarControl {
        protected internal BindingList<IBancoFormaPagoDetailModel> _DataSource;

        public FormaPagoGridControl() : base() {
            this.ShowFiltro = true;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IGridViewFormaPagoBuilder d0 = new GridViewFormaPagoBuilder()) {
                this.GridData.Columns.AddRange(d0.Templetes().FormaPago().Build());
            }
        }
    }
}
