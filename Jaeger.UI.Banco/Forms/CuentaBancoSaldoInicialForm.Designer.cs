﻿namespace Jaeger.UI.Banco.Forms {
    partial class CuentaBancoSaldoInicialForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentaBancoSaldoInicialForm));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.RadLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.TxbEmisorNumeroCta = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.TxbEmisorBanco = new Telerik.WinControls.UI.RadTextBox();
            this.TxbEmisorSucursal = new Telerik.WinControls.UI.RadTextBox();
            this.TxbEmisorClabe = new Telerik.WinControls.UI.RadTextBox();
            this.CboEmisorCuenta = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.TxbEmisorRfc = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.FechaDocumento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TxbAbono = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.LabelAbono = new Telerik.WinControls.UI.RadLabel();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorNumeroCta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorClabe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorRfc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.RadLabel11);
            this.radPanel1.Controls.Add(this.RadLabel13);
            this.radPanel1.Controls.Add(this.TxbEmisorNumeroCta);
            this.radPanel1.Controls.Add(this.RadLabel14);
            this.radPanel1.Controls.Add(this.RadLabel16);
            this.radPanel1.Controls.Add(this.RadLabel15);
            this.radPanel1.Controls.Add(this.TxbEmisorBanco);
            this.radPanel1.Controls.Add(this.TxbEmisorSucursal);
            this.radPanel1.Controls.Add(this.TxbEmisorClabe);
            this.radPanel1.Controls.Add(this.CboEmisorCuenta);
            this.radPanel1.Controls.Add(this.radLabel22);
            this.radPanel1.Controls.Add(this.TxbEmisorRfc);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 30);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(521, 104);
            this.radPanel1.TabIndex = 1;
            // 
            // RadLabel11
            // 
            this.RadLabel11.Location = new System.Drawing.Point(17, 17);
            this.RadLabel11.Name = "RadLabel11";
            this.RadLabel11.Size = new System.Drawing.Size(69, 18);
            this.RadLabel11.TabIndex = 371;
            this.RadLabel11.Text = "Cta. Destino:";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(12, 42);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(74, 18);
            this.RadLabel13.TabIndex = 372;
            this.RadLabel13.Text = "Núm. Cuenta:";
            // 
            // TxbEmisorNumeroCta
            // 
            this.TxbEmisorNumeroCta.Location = new System.Drawing.Point(92, 42);
            this.TxbEmisorNumeroCta.MaxLength = 20;
            this.TxbEmisorNumeroCta.Name = "TxbEmisorNumeroCta";
            this.TxbEmisorNumeroCta.NullText = "Núm. Cuenta";
            this.TxbEmisorNumeroCta.Size = new System.Drawing.Size(100, 20);
            this.TxbEmisorNumeroCta.TabIndex = 382;
            // 
            // RadLabel14
            // 
            this.RadLabel14.Location = new System.Drawing.Point(198, 42);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(39, 18);
            this.RadLabel14.TabIndex = 373;
            this.RadLabel14.Text = "Banco:";
            // 
            // RadLabel16
            // 
            this.RadLabel16.Location = new System.Drawing.Point(187, 68);
            this.RadLabel16.Name = "RadLabel16";
            this.RadLabel16.Size = new System.Drawing.Size(50, 18);
            this.RadLabel16.TabIndex = 374;
            this.RadLabel16.Text = "Sucursal:";
            // 
            // RadLabel15
            // 
            this.RadLabel15.Location = new System.Drawing.Point(331, 69);
            this.RadLabel15.Name = "RadLabel15";
            this.RadLabel15.Size = new System.Drawing.Size(37, 18);
            this.RadLabel15.TabIndex = 375;
            this.RadLabel15.Text = "Clabe:";
            // 
            // TxbEmisorBanco
            // 
            this.TxbEmisorBanco.Location = new System.Drawing.Point(243, 42);
            this.TxbEmisorBanco.Name = "TxbEmisorBanco";
            this.TxbEmisorBanco.NullText = "Banco";
            this.TxbEmisorBanco.ReadOnly = true;
            this.TxbEmisorBanco.Size = new System.Drawing.Size(266, 20);
            this.TxbEmisorBanco.TabIndex = 376;
            // 
            // TxbEmisorSucursal
            // 
            this.TxbEmisorSucursal.Location = new System.Drawing.Point(243, 68);
            this.TxbEmisorSucursal.MaxLength = 20;
            this.TxbEmisorSucursal.Name = "TxbEmisorSucursal";
            this.TxbEmisorSucursal.NullText = "Sucursal";
            this.TxbEmisorSucursal.ReadOnly = true;
            this.TxbEmisorSucursal.Size = new System.Drawing.Size(82, 20);
            this.TxbEmisorSucursal.TabIndex = 377;
            this.TxbEmisorSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxbEmisorClabe
            // 
            this.TxbEmisorClabe.Location = new System.Drawing.Point(371, 68);
            this.TxbEmisorClabe.MaxLength = 22;
            this.TxbEmisorClabe.Name = "TxbEmisorClabe";
            this.TxbEmisorClabe.NullText = "Clabe";
            this.TxbEmisorClabe.ReadOnly = true;
            this.TxbEmisorClabe.Size = new System.Drawing.Size(138, 20);
            this.TxbEmisorClabe.TabIndex = 378;
            this.TxbEmisorClabe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CboEmisorCuenta
            // 
            // 
            // CboEmisorCuenta.NestedRadGridView
            // 
            this.CboEmisorCuenta.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CboEmisorCuenta.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboEmisorCuenta.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CboEmisorCuenta.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Banco";
            gridViewTextBoxColumn1.HeaderText = "Banco";
            gridViewTextBoxColumn1.Name = "Banco";
            gridViewTextBoxColumn2.FieldName = "Beneficiario";
            gridViewTextBoxColumn2.HeaderText = "Beneficiario";
            gridViewTextBoxColumn2.Name = "Beneficiario";
            gridViewTextBoxColumn3.FieldName = "Alias";
            gridViewTextBoxColumn3.HeaderText = "Alias";
            gridViewTextBoxColumn3.Name = "Alias";
            gridViewTextBoxColumn4.FieldName = "NumeroDeCuenta";
            gridViewTextBoxColumn4.HeaderText = "Cuenta";
            gridViewTextBoxColumn4.Name = "NumeroDeCuenta";
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.Name = "Clave";
            this.CboEmisorCuenta.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.CboEmisorCuenta.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CboEmisorCuenta.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CboEmisorCuenta.EditorControl.Name = "NestedRadGridView";
            this.CboEmisorCuenta.EditorControl.ReadOnly = true;
            this.CboEmisorCuenta.EditorControl.ShowGroupPanel = false;
            this.CboEmisorCuenta.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CboEmisorCuenta.EditorControl.TabIndex = 0;
            this.CboEmisorCuenta.Location = new System.Drawing.Point(92, 17);
            this.CboEmisorCuenta.Name = "CboEmisorCuenta";
            this.CboEmisorCuenta.NullText = "Selecciona";
            this.CboEmisorCuenta.Size = new System.Drawing.Size(417, 20);
            this.CboEmisorCuenta.TabIndex = 379;
            this.CboEmisorCuenta.TabStop = false;
            // 
            // radLabel22
            // 
            this.radLabel22.Location = new System.Drawing.Point(58, 67);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(28, 18);
            this.radLabel22.TabIndex = 380;
            this.radLabel22.Text = "RFC:";
            // 
            // TxbEmisorRfc
            // 
            this.TxbEmisorRfc.Location = new System.Drawing.Point(92, 68);
            this.TxbEmisorRfc.MaxLength = 14;
            this.TxbEmisorRfc.Name = "TxbEmisorRfc";
            this.TxbEmisorRfc.NullText = "RFC";
            this.TxbEmisorRfc.ReadOnly = true;
            this.TxbEmisorRfc.Size = new System.Drawing.Size(89, 20);
            this.TxbEmisorRfc.TabIndex = 381;
            this.TxbEmisorRfc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(289, 143);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(119, 18);
            this.RadLabel8.TabIndex = 366;
            this.RadLabel8.Text = "Fecha del Saldo Inicial:";
            // 
            // FechaDocumento
            // 
            this.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDocumento.Location = new System.Drawing.Point(414, 139);
            this.FechaDocumento.Name = "FechaDocumento";
            this.FechaDocumento.Size = new System.Drawing.Size(95, 20);
            this.FechaDocumento.TabIndex = 368;
            this.FechaDocumento.TabStop = false;
            this.FechaDocumento.Text = "15/12/2016";
            this.FechaDocumento.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // TxbAbono
            // 
            this.TxbAbono.Location = new System.Drawing.Point(414, 166);
            this.TxbAbono.Mask = "c";
            this.TxbAbono.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TxbAbono.Name = "TxbAbono";
            this.TxbAbono.NullText = "Importe";
            this.TxbAbono.Size = new System.Drawing.Size(95, 20);
            this.TxbAbono.TabIndex = 371;
            this.TxbAbono.TabStop = false;
            this.TxbAbono.Text = "$0.00";
            this.TxbAbono.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LabelAbono
            // 
            this.LabelAbono.Location = new System.Drawing.Point(340, 167);
            this.LabelAbono.Name = "LabelAbono";
            this.LabelAbono.Size = new System.Drawing.Size(68, 18);
            this.LabelAbono.TabIndex = 370;
            this.LabelAbono.Text = "Saldo inicial:";
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(521, 30);
            this.ToolBar.TabIndex = 372;
            this.ToolBar.Guardar.Click += this.Guardar_Click;
            this.ToolBar.Actualizar.Click += this.Actualizar_Click;
            this.ToolBar.Cerrar.Click += this.Cerrar_Click;
            // 
            // CuentaBancoSaldoInicialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 194);
            this.Controls.Add(this.TxbAbono);
            this.Controls.Add(this.LabelAbono);
            this.Controls.Add(this.RadLabel8);
            this.Controls.Add(this.FechaDocumento);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CuentaBancoSaldoInicialForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Saldo Inicial";
            this.Load += new System.EventHandler(this.ViewCuentaBancoSaldoInicial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorNumeroCta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorClabe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CboEmisorCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbEmisorRfc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxbAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel radPanel1;
        internal Telerik.WinControls.UI.RadLabel RadLabel11;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        private Telerik.WinControls.UI.RadTextBox TxbEmisorNumeroCta;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadLabel RadLabel16;
        internal Telerik.WinControls.UI.RadLabel RadLabel15;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorBanco;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorSucursal;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorClabe;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CboEmisorCuenta;
        internal Telerik.WinControls.UI.RadLabel radLabel22;
        internal Telerik.WinControls.UI.RadTextBox TxbEmisorRfc;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDocumento;
        internal Telerik.WinControls.UI.RadMaskedEditBox TxbAbono;
        internal Telerik.WinControls.UI.RadLabel LabelAbono;
        private Common.Forms.ToolBarStandarControl ToolBar;
    }
}
