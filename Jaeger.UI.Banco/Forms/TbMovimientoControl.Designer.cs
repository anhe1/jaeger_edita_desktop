﻿namespace Jaeger.UI.Banco.Forms {
    partial class TbMovimientoControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.CuentasBancaria = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblCuenta = new Telerik.WinControls.UI.CommandBarLabel();
            this.Cuenta = new Telerik.WinControls.UI.CommandBarHostItem();
            this.lblNumCta = new Telerik.WinControls.UI.CommandBarLabel();
            this.lblNumero = new Telerik.WinControls.UI.CommandBarLabel();
            this.Info = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.lblPeriodo = new Telerik.WinControls.UI.CommandBarLabel();
            this.Periodo = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.lblEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.TEjercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.SaldoInicial = new Telerik.WinControls.UI.RadMenuItem();
            this.TransferenciaCuentas = new Telerik.WinControls.UI.RadMenuItem();
            this.Movimiento = new Telerik.WinControls.UI.RadMenuItem();
            this.Editar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Separator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.ReporteComprobante = new Telerik.WinControls.UI.RadMenuItem();
            this.ReporteDiario = new Telerik.WinControls.UI.RadMenuItem();
            this.ReporteEstadoCuenta = new Telerik.WinControls.UI.RadMenuItem();
            this.ReporteTransferencias = new Telerik.WinControls.UI.RadMenuItem();
            this.ReporteCheques = new Telerik.WinControls.UI.RadMenuItem();
            this.AutoSuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Crear = new Telerik.WinControls.UI.RadMenuItem();
            this.Exportar = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            this.ReporteLHorizontal = new Telerik.WinControls.UI.RadMenuItem();
            this.ReporteLVertical = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.radCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(673, 3);
            this.Ejercicio.Maximum = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Minimum = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.NullableValue = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            this.Ejercicio.Size = new System.Drawing.Size(65, 20);
            this.Ejercicio.TabIndex = 24;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.Ejercicio.Value = new decimal(new int[] {
            2013,
            0,
            0,
            0});
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Controls.Add(this.Ejercicio);
            this.radCommandBar1.Controls.Add(this.CuentasBancaria);
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1381, 30);
            this.radCommandBar1.TabIndex = 22;
            // 
            // CuentasBancaria
            // 
            this.CuentasBancaria.AutoSizeDropDownToBestFit = true;
            this.CuentasBancaria.DisplayMember = "Alias";
            this.CuentasBancaria.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // CuentasBancaria.NestedRadGridView
            // 
            this.CuentasBancaria.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CuentasBancaria.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CuentasBancaria.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CuentasBancaria.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CuentasBancaria.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CuentasBancaria.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CuentasBancaria.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "NumCuenta";
            gridViewTextBoxColumn1.HeaderText = "Núm. de Cuenta";
            gridViewTextBoxColumn1.Name = "NumCuenta";
            gridViewTextBoxColumn2.FieldName = "Beneficiario";
            gridViewTextBoxColumn2.HeaderText = "Beneficiario";
            gridViewTextBoxColumn2.Name = "Beneficiario";
            gridViewTextBoxColumn2.Width = 120;
            gridViewTextBoxColumn3.FieldName = "Alias";
            gridViewTextBoxColumn3.HeaderText = "Alias";
            gridViewTextBoxColumn3.Name = "Alias";
            gridViewTextBoxColumn3.Width = 95;
            this.CuentasBancaria.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.CuentasBancaria.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CuentasBancaria.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CuentasBancaria.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.CuentasBancaria.EditorControl.Name = "NestedRadGridView";
            this.CuentasBancaria.EditorControl.ReadOnly = true;
            this.CuentasBancaria.EditorControl.ShowGroupPanel = false;
            this.CuentasBancaria.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CuentasBancaria.EditorControl.TabIndex = 0;
            this.CuentasBancaria.Location = new System.Drawing.Point(59, 3);
            this.CuentasBancaria.Name = "CuentasBancaria";
            this.CuentasBancaria.NullText = "Selecciona";
            this.CuentasBancaria.Size = new System.Drawing.Size(199, 20);
            this.CuentasBancaria.TabIndex = 23;
            this.CuentasBancaria.TabStop = false;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.commandBarRowElement1.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblCuenta,
            this.Cuenta,
            this.lblNumCta,
            this.lblNumero,
            this.Info,
            this.Separator1,
            this.lblPeriodo,
            this.Periodo,
            this.lblEjercicio,
            this.TEjercicio,
            this.Separator2,
            this.Nuevo,
            this.Editar,
            this.Cancelar,
            this.Separator3,
            this.Actualizar,
            this.Filtro,
            this.Imprimir,
            this.AutoSuma,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // lblCuenta
            // 
            this.lblCuenta.DisplayName = "Etiqueta: Cuenta";
            this.lblCuenta.Name = "lblCuenta";
            this.lblCuenta.Text = "Cuenta:";
            this.lblCuenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cuenta
            // 
            this.Cuenta.DisplayName = "Cuenta Bancaria";
            this.Cuenta.MinSize = new System.Drawing.Size(210, 20);
            this.Cuenta.Name = "Cuenta";
            this.Cuenta.Text = "Cuenta Bancaria";
            this.Cuenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // lblNumCta
            // 
            this.lblNumCta.DisplayName = "Etiqueta: Núm. Cuenta";
            this.lblNumCta.Name = "lblNumCta";
            this.lblNumCta.Text = "Núm. Cta.:";
            this.lblNumCta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // lblNumero
            // 
            this.lblNumero.DisplayName = "Cuenta";
            this.lblNumero.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumero.MinSize = new System.Drawing.Size(100, 26);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Text = "000000000000000000";
            this.lblNumero.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Info
            // 
            this.Info.DisplayName = "Información";
            this.Info.Image = global::Jaeger.UI.Banco.Properties.Resources.card_exchange_16px;
            this.Info.Name = "Info";
            this.Info.Text = "Información";
            this.Info.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Info.Click += new System.EventHandler(this.TInfo_Click);
            // 
            // Separator1
            // 
            this.Separator1.DisplayName = "Separador";
            this.Separator1.Name = "Separator1";
            this.Separator1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Separator1.VisibleInOverflowMenu = false;
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.DisplayName = "Etiqueta: Periodo";
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.Text = "Periodo:";
            this.lblPeriodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Periodo
            // 
            this.Periodo.DefaultItemsCountInDropDown = 12;
            this.Periodo.DisplayName = "commandBarDropDownList1";
            this.Periodo.DrawText = false;
            this.Periodo.DropDownAnimationEnabled = true;
            this.Periodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Periodo.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.Periodo.MaxDropDownItems = 0;
            this.Periodo.MinSize = new System.Drawing.Size(95, 22);
            this.Periodo.Name = "Periodo";
            this.Periodo.Padding = new System.Windows.Forms.Padding(0);
            this.Periodo.Text = "";
            this.Periodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // lblEjercicio
            // 
            this.lblEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.lblEjercicio.Name = "lblEjercicio";
            this.lblEjercicio.Text = "Ejercicio:";
            this.lblEjercicio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TEjercicio
            // 
            this.TEjercicio.DisplayName = "commandBarHostItem1";
            this.TEjercicio.MinSize = new System.Drawing.Size(70, 15);
            this.TEjercicio.Name = "TEjercicio";
            this.TEjercicio.Text = "";
            this.TEjercicio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "Separador";
            this.Separator2.Name = "Separator2";
            this.Separator2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Banco.Properties.Resources.new_file_16px;
            this.Nuevo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.SaldoInicial,
            this.TransferenciaCuentas,
            this.Movimiento});
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // SaldoInicial
            // 
            this.SaldoInicial.Name = "SaldoInicial";
            this.SaldoInicial.Text = "Saldo Inicial";
            this.SaldoInicial.Click += new System.EventHandler(this.Nuevo_SaldoInicial_Click);
            // 
            // TransferenciaCuentas
            // 
            this.TransferenciaCuentas.Name = "TransferenciaCuentas";
            this.TransferenciaCuentas.Text = "Transferencia entre cuentas";
            this.TransferenciaCuentas.Click += new System.EventHandler(this.Nuevo_Transferencia_Click);
            // 
            // Movimiento
            // 
            this.Movimiento.Name = "Movimiento";
            this.Movimiento.Text = "Movimiento";
            this.Movimiento.Click += new System.EventHandler(this.Nuevo_Movimiento_Click);
            // 
            // Editar
            // 
            this.Editar.DisplayName = "Ver";
            this.Editar.DrawText = true;
            this.Editar.Image = global::Jaeger.UI.Banco.Properties.Resources.edit_16px;
            this.Editar.Name = "Editar";
            this.Editar.Text = "Editar";
            this.Editar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Image = global::Jaeger.UI.Banco.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Separator3
            // 
            this.Separator3.DisplayName = "Separador";
            this.Separator3.Name = "Separator3";
            this.Separator3.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Banco.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.Banco.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Banco.Properties.Resources.print_16px;
            this.Imprimir.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ReporteComprobante,
            this.ReporteLHorizontal,
            this.ReporteLVertical,
            this.ReporteDiario,
            this.ReporteEstadoCuenta,
            this.ReporteTransferencias,
            this.ReporteCheques});
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ReporteComprobante
            // 
            this.ReporteComprobante.Name = "ReporteComprobante";
            this.ReporteComprobante.Text = "Comprobante";
            // 
            // ReporteDiario
            // 
            this.ReporteDiario.Name = "ReporteDiario";
            this.ReporteDiario.Text = "Reporte Diario";
            // 
            // ReporteEstadoCuenta
            // 
            this.ReporteEstadoCuenta.Name = "ReporteEstadoCuenta";
            this.ReporteEstadoCuenta.Text = "Estado de Cuenta";
            // 
            // ReporteTransferencias
            // 
            this.ReporteTransferencias.Name = "ReporteTransferencias";
            this.ReporteTransferencias.Text = "Reporte Transferencia entre cuentas";
            // 
            // ReporteCheques
            // 
            this.ReporteCheques.Name = "ReporteCheques";
            this.ReporteCheques.Text = "Reporte Cheques emitidos";
            // 
            // AutoSuma
            // 
            this.AutoSuma.DisplayName = "commandBarToggleButton1";
            this.AutoSuma.DrawText = true;
            this.AutoSuma.Image = global::Jaeger.UI.Banco.Properties.Resources.sigma_16px;
            this.AutoSuma.Name = "AutoSuma";
            this.AutoSuma.Text = "AutoSuma";
            this.AutoSuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Banco.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Crear,
            this.Exportar});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Crear
            // 
            this.Crear.Name = "Crear";
            this.Crear.Text = "Crear";
            // 
            // Exportar
            // 
            this.Exportar.Image = global::Jaeger.UI.Banco.Properties.Resources.xls_16px;
            this.Exportar.Name = "Exportar";
            this.Exportar.Text = "Exportar";
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Banco.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // ReporteLHorizontal
            // 
            this.ReporteLHorizontal.Name = "ReporteLHorizontal";
            this.ReporteLHorizontal.Text = "Listado Horizontal";
            // 
            // ReporteLVertical
            // 
            this.ReporteLVertical.Name = "ReporteLVertical";
            this.ReporteLVertical.Text = "Listado Vertical";
            // 
            // TbMovimientoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radCommandBar1);
            this.Name = "TbMovimientoControl";
            this.Size = new System.Drawing.Size(1381, 30);
            this.Load += new System.EventHandler(this.TbMovimientoControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCommandBar1.ResumeLayout(false);
            this.radCommandBar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentasBancaria)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        internal Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        public Telerik.WinControls.UI.CommandBarLabel lblCuenta;
        public Telerik.WinControls.UI.CommandBarLabel lblNumCta;
        public Telerik.WinControls.UI.CommandBarLabel lblNumero;
        public Telerik.WinControls.UI.CommandBarSeparator Separator1;
        private Telerik.WinControls.UI.CommandBarLabel lblPeriodo;
        private Telerik.WinControls.UI.CommandBarLabel lblEjercicio;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        private Telerik.WinControls.UI.CommandBarSeparator Separator3;
        internal Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox CuentasBancaria;
        private System.ComponentModel.BackgroundWorker Preparar;
        internal Telerik.WinControls.UI.RadMenuItem TransferenciaCuentas;
        internal Telerik.WinControls.UI.RadMenuItem Movimiento;
        internal Telerik.WinControls.UI.RadMenuItem Crear;
        internal Telerik.WinControls.UI.RadMenuItem ReporteComprobante;
        internal Telerik.WinControls.UI.RadMenuItem ReporteDiario;
        internal Telerik.WinControls.UI.RadMenuItem ReporteEstadoCuenta;
        internal Telerik.WinControls.UI.RadMenuItem ReporteTransferencias;
        internal Telerik.WinControls.UI.RadMenuItem ReporteCheques;
        internal Telerik.WinControls.UI.RadMenuItem SaldoInicial;
        internal Telerik.WinControls.UI.RadMenuItem Exportar;
        protected internal Telerik.WinControls.UI.CommandBarHostItem Cuenta;
        protected internal Telerik.WinControls.UI.CommandBarButton Info;
        protected internal Telerik.WinControls.UI.CommandBarDropDownList Periodo;
        protected internal Telerik.WinControls.UI.CommandBarHostItem TEjercicio;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Nuevo;
        protected internal Telerik.WinControls.UI.CommandBarButton Editar;
        protected internal Telerik.WinControls.UI.CommandBarButton Cancelar;
        protected internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Imprimir;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarToggleButton AutoSuma;
        internal Telerik.WinControls.UI.RadMenuItem ReporteLHorizontal;
        internal Telerik.WinControls.UI.RadMenuItem ReporteLVertical;
    }
}
