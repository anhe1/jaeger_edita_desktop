﻿namespace Jaeger.UI.Banco.Forms {
    partial class TbMovimientoBancarioControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblEstado = new Telerik.WinControls.UI.CommandBarLabel();
            this.Status = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Identificador = new Telerik.WinControls.UI.CommandBarLabel();
            this.Separator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Duplicar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cancelar = new Telerik.WinControls.UI.CommandBarButton();
            this.Guardar = new Telerik.WinControls.UI.CommandBarButton();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Layout1 = new Telerik.WinControls.UI.RadMenuItem();
            this.LayoutBanamexC = new Telerik.WinControls.UI.RadMenuItem();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(899, 30);
            this.radCommandBar1.TabIndex = 1;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.commandBarRowElement1.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            this.ToolBar.Enabled = true;
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblEstado,
            this.Status,
            this.Separator2,
            this.Identificador,
            this.Separator3,
            this.Nuevo,
            this.Duplicar,
            this.Cancelar,
            this.Guardar,
            this.Actualizar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // lblEstado
            // 
            this.lblEstado.DisplayName = "Estado";
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Text = "Estado:";
            // 
            // Status
            // 
            this.Status.AutoCompleteDisplayMember = "Descripcion";
            this.Status.AutoCompleteValueMember = "Id";
            this.Status.DisplayMember = "Descripcion";
            this.Status.DisplayName = "Descripcion";
            this.Status.DropDownAnimationEnabled = true;
            this.Status.MaxDropDownItems = 0;
            this.Status.Name = "Status";
            this.Status.Text = "";
            this.Status.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Status.ValueMember = "Id";
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Identificador
            // 
            this.Identificador.DisplayName = "commandBarLabel1";
            this.Identificador.MaxSize = new System.Drawing.Size(59, 26);
            this.Identificador.MinSize = new System.Drawing.Size(59, 26);
            this.Identificador.Name = "Identificador";
            this.Identificador.Text = "T00000000";
            // 
            // Separator3
            // 
            this.Separator3.DisplayName = "commandBarSeparator3";
            this.Separator3.Name = "Separator3";
            this.Separator3.VisibleInOverflowMenu = false;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "Nuevo";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Banco.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Duplicar
            // 
            this.Duplicar.DisplayName = "Duplicar";
            this.Duplicar.DrawText = true;
            this.Duplicar.Image = global::Jaeger.UI.Banco.Properties.Resources.copy_16px;
            this.Duplicar.Name = "Duplicar";
            this.Duplicar.Text = "Duplicar";
            this.Duplicar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cancelar
            // 
            this.Cancelar.DisplayName = "Cancelar";
            this.Cancelar.DrawText = true;
            this.Cancelar.Image = global::Jaeger.UI.Banco.Properties.Resources.cancel_16px;
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Guardar
            // 
            this.Guardar.DisplayName = "Guardar";
            this.Guardar.DrawText = true;
            this.Guardar.Image = global::Jaeger.UI.Banco.Properties.Resources.save_16px;
            this.Guardar.Name = "Guardar";
            this.Guardar.Text = "Guardar";
            this.Guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.Banco.Properties.Resources.refresh_16px;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Image = global::Jaeger.UI.Banco.Properties.Resources.print_16px;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "Herramientas";
            this.Herramientas.DrawText = true;
            this.Herramientas.Image = global::Jaeger.UI.Banco.Properties.Resources.toolbox_16px;
            this.Herramientas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Layout1});
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Layout1
            // 
            this.Layout1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.LayoutBanamexC});
            this.Layout1.Name = "Layout1";
            this.Layout1.Text = "Layout";
            // 
            // LayoutBanamexC
            // 
            this.LayoutBanamexC.Name = "LayoutBanamexC";
            this.LayoutBanamexC.Text = "Banamex";
            this.LayoutBanamexC.Click += new System.EventHandler(this.LayoutBanamexC_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Banco.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbMovimientoBancarioControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radCommandBar1);
            this.Name = "TbMovimientoBancarioControl";
            this.Size = new System.Drawing.Size(899, 30);
            this.Load += new System.EventHandler(this.TbMovimientoBancarioControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel lblEstado;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        private Telerik.WinControls.UI.CommandBarLabel Identificador;
        private Telerik.WinControls.UI.CommandBarSeparator Separator3;
        public Telerik.WinControls.UI.CommandBarButton Nuevo;
        public Telerik.WinControls.UI.CommandBarButton Duplicar;
        public Telerik.WinControls.UI.CommandBarButton Imprimir;
        public Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public Telerik.WinControls.UI.CommandBarButton Cancelar;
        internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        private System.ComponentModel.BackgroundWorker Preparar;
        internal Telerik.WinControls.UI.CommandBarDropDownList Status;
        public Telerik.WinControls.UI.CommandBarButton Guardar;
        private Telerik.WinControls.UI.RadMenuItem Layout1;
        private Telerik.WinControls.UI.RadMenuItem LayoutBanamexC;
    }
}
