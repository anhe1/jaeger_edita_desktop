﻿
namespace Jaeger.UI.Banco.Forms {
    partial class MovimientosGridControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn3 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCommandColumn gridViewCommandColumn1 = new Telerik.WinControls.UI.GridViewCommandColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.GridComprobantes = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridAuxiliar = new Telerik.WinControls.UI.GridViewTemplate();
            this.GridTransferencia = new Telerik.WinControls.UI.GridViewTemplate();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTransferencia)).BeginInit();
            this.SuspendLayout();
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            gridViewTextBoxColumn1.FieldName = "Identificador";
            gridViewTextBoxColumn1.HeaderText = "Identificador";
            gridViewTextBoxColumn1.Name = "Identificador";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 75;
            gridViewComboBoxColumn1.FieldName = "Status";
            gridViewComboBoxColumn1.HeaderText = "Estado";
            gridViewComboBoxColumn1.Name = "Status";
            gridViewComboBoxColumn1.Width = 85;
            gridViewTextBoxColumn2.FieldName = "BeneficiarioT";
            gridViewTextBoxColumn2.HeaderText = "Persona";
            gridViewTextBoxColumn2.Name = "BeneficiarioT";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 275;
            gridViewTextBoxColumn3.FieldName = "Concepto";
            gridViewTextBoxColumn3.HeaderText = "Concepto";
            gridViewTextBoxColumn3.Name = "Concepto";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 240;
            gridViewTextBoxColumn4.FieldName = "ClaveFormaPago";
            gridViewTextBoxColumn4.HeaderText = "Clv. Pago";
            gridViewTextBoxColumn4.Name = "ClaveFormaPago";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn5.FieldName = "FechaDocto";
            gridViewTextBoxColumn5.FormatString = "{0:ddd dd MMM yyyy}";
            gridViewTextBoxColumn5.HeaderText = "Fec. Doc./Pago";
            gridViewTextBoxColumn5.Name = "FechaDocto";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "NumDocto";
            gridViewTextBoxColumn6.HeaderText = "No. Docto.";
            gridViewTextBoxColumn6.Name = "NumDocto";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 85;
            gridViewTextBoxColumn7.FieldName = "ClaveBancoT";
            gridViewTextBoxColumn7.HeaderText = "Clv. Banco";
            gridViewTextBoxColumn7.Name = "ClaveBancoT";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.FieldName = "SucursalT";
            gridViewTextBoxColumn8.HeaderText = "Sucursal";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "SucursalT";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.FieldName = "CuentaCLABET";
            gridViewTextBoxColumn9.HeaderText = "CLABE";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "CuentaCLABET";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 120;
            gridViewTextBoxColumn10.FieldName = "Referencia";
            gridViewTextBoxColumn10.HeaderText = "Referencia";
            gridViewTextBoxColumn10.Name = "Referencia";
            gridViewTextBoxColumn10.Width = 105;
            gridViewTextBoxColumn11.FieldName = "NumAutorizacion";
            gridViewTextBoxColumn11.HeaderText = "Autorización";
            gridViewTextBoxColumn11.Name = "NumAutorizacion";
            gridViewTextBoxColumn11.Width = 105;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Abono";
            gridViewTextBoxColumn12.FormatString = "{0:n2}";
            gridViewTextBoxColumn12.HeaderText = "Depósito";
            gridViewTextBoxColumn12.Name = "Abono";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 95;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "Cargo";
            gridViewTextBoxColumn13.FormatString = "{0:n2}";
            gridViewTextBoxColumn13.HeaderText = "Retiro";
            gridViewTextBoxColumn13.Name = "Cargo";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 95;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Red;
            expressionFormattingObject1.Expression = "Saldo <= 0";
            expressionFormattingObject1.Name = "NewCondition";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewTextBoxColumn14.DataType = typeof(decimal);
            gridViewTextBoxColumn14.FieldName = "Saldo";
            gridViewTextBoxColumn14.FormatString = "{0:n2}";
            gridViewTextBoxColumn14.HeaderText = "Saldo";
            gridViewTextBoxColumn14.Name = "Saldo";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn14.Width = 85;
            gridViewDateTimeColumn1.FieldName = "FechaAplicacion";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yyyy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Aplicación";
            gridViewDateTimeColumn1.Name = "FechaAplicacion";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn1.Width = 85;
            gridViewDateTimeColumn2.FieldName = "FechaEmision";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yyyy}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Emisión";
            gridViewDateTimeColumn2.Name = "FechaEmision";
            gridViewDateTimeColumn2.ReadOnly = true;
            gridViewDateTimeColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewDateTimeColumn2.Width = 85;
            gridViewDateTimeColumn3.FormatString = "{0:dd MMM yyyy}";
            gridViewDateTimeColumn3.HeaderText = "Fec. Vence";
            gridViewDateTimeColumn3.IsVisible = false;
            gridViewDateTimeColumn3.Name = "column11";
            gridViewDateTimeColumn3.ReadOnly = true;
            gridViewDateTimeColumn3.Width = 85;
            gridViewTextBoxColumn15.FieldName = "Cancela";
            gridViewTextBoxColumn15.HeaderText = "Canceló";
            gridViewTextBoxColumn15.Name = "Cancela";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 85;
            conditionalFormattingObject1.ApplyToRow = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "PorComprobar";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "true";
            conditionalFormattingObject1.TValue2 = "true";
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "PorComprobar";
            gridViewCheckBoxColumn1.HeaderText = "Por Comprobar";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "PorComprobar";
            gridViewCheckBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn16.FieldName = "Creo";
            gridViewTextBoxColumn16.HeaderText = "Creó";
            gridViewTextBoxColumn16.Name = "Creo";
            gridViewTextBoxColumn16.ReadOnly = true;
            gridViewTextBoxColumn16.Width = 85;
            gridViewTextBoxColumn17.FieldName = "Nota";
            gridViewTextBoxColumn17.HeaderText = "Nota";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Nota";
            gridViewTextBoxColumn17.Width = 200;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Silver;
            conditionalFormattingObject2.Name = "Afecta Saldo";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.TValue1 = "FALSE";
            conditionalFormattingObject2.TValue2 = "0";
            gridViewCheckBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewCheckBoxColumn2.FieldName = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn2.HeaderText = "Afecta Saldo";
            gridViewCheckBoxColumn2.IsVisible = false;
            gridViewCheckBoxColumn2.Name = "AfectaSaldoCuenta";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewCheckBoxColumn2.VisibleInColumnChooser = false;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewDateTimeColumn1,
            gridViewDateTimeColumn2,
            gridViewDateTimeColumn3,
            gridViewTextBoxColumn15,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewCheckBoxColumn2});
            this.GridData.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.GridComprobantes,
            this.GridTransferencia,
            this.GridAuxiliar});
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(1098, 465);
            this.GridData.TabIndex = 2;
            // 
            // GridComprobantes
            // 
            this.GridComprobantes.Caption = "Comprobantes";
            gridViewTextBoxColumn18.FieldName = "Identificador";
            gridViewTextBoxColumn18.HeaderText = "Identificador";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.MaxLength = 11;
            gridViewTextBoxColumn18.Name = "Identificador";
            gridViewTextBoxColumn19.FieldName = "TipoComprobanteText";
            gridViewTextBoxColumn19.HeaderText = "Tipo";
            gridViewTextBoxColumn19.Name = "TipoComprobanteText";
            gridViewTextBoxColumn20.FieldName = "TipoDocumentoText";
            gridViewTextBoxColumn20.HeaderText = "Documento";
            gridViewTextBoxColumn20.Name = "TipoDocumentoText";
            gridViewTextBoxColumn21.FieldName = "Folio";
            gridViewTextBoxColumn21.HeaderText = "Folio";
            gridViewTextBoxColumn21.Name = "Folio";
            gridViewTextBoxColumn22.FieldName = "Serie";
            gridViewTextBoxColumn22.HeaderText = "Serie";
            gridViewTextBoxColumn22.Name = "Serie";
            gridViewTextBoxColumn23.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn23.FieldName = "FechaEmision";
            gridViewTextBoxColumn23.FormatString = "{0:dd-MMM-yy}";
            gridViewTextBoxColumn23.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn23.Name = "FechaEmision";
            gridViewTextBoxColumn23.Width = 75;
            gridViewTextBoxColumn24.FieldName = "EmisorRFC";
            gridViewTextBoxColumn24.HeaderText = "RFC (Emisor)";
            gridViewTextBoxColumn24.Name = "EmisorRFC";
            gridViewTextBoxColumn24.Width = 90;
            gridViewTextBoxColumn25.FieldName = "EmisorNombre";
            gridViewTextBoxColumn25.HeaderText = "Emisor";
            gridViewTextBoxColumn25.Name = "EmisorNombre";
            gridViewTextBoxColumn25.Width = 200;
            gridViewTextBoxColumn26.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn26.HeaderText = "RFC (Receptor)";
            gridViewTextBoxColumn26.Name = "ReceptorRFC";
            gridViewTextBoxColumn26.Width = 90;
            gridViewTextBoxColumn27.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn27.HeaderText = "Receptor";
            gridViewTextBoxColumn27.Name = "ReceptorNombre";
            gridViewTextBoxColumn27.Width = 200;
            gridViewTextBoxColumn28.FieldName = "IdDocumento";
            gridViewTextBoxColumn28.HeaderText = "IdDocumento (UUID)";
            gridViewTextBoxColumn28.Name = "IdDocumento";
            gridViewTextBoxColumn28.Width = 155;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.FieldName = "Total";
            gridViewTextBoxColumn29.FormatString = "{0:N2}";
            gridViewTextBoxColumn29.HeaderText = "Total";
            gridViewTextBoxColumn29.Name = "Total";
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 85;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.FieldName = "Acumulado";
            gridViewTextBoxColumn30.FormatString = "{0:N2}";
            gridViewTextBoxColumn30.HeaderText = "Acumulado";
            gridViewTextBoxColumn30.Name = "Acumulado";
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 85;
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.FieldName = "Abono";
            gridViewTextBoxColumn31.FormatString = "{0:N2}";
            gridViewTextBoxColumn31.HeaderText = "Abono";
            gridViewTextBoxColumn31.Name = "Abono";
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 85;
            gridViewTextBoxColumn32.DataType = typeof(decimal);
            gridViewTextBoxColumn32.FieldName = "Cargo";
            gridViewTextBoxColumn32.FormatString = "{0:N2}";
            gridViewTextBoxColumn32.HeaderText = "Cargo";
            gridViewTextBoxColumn32.Name = "Cargo";
            gridViewTextBoxColumn32.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn32.Width = 85;
            this.GridComprobantes.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.GridComprobantes.ViewDefinition = tableViewDefinition1;
            // 
            // GridAuxiliar
            // 
            this.GridAuxiliar.Caption = "Documentos";
            gridViewTextBoxColumn34.FieldName = "Descripcion";
            gridViewTextBoxColumn34.HeaderText = "Descripción";
            gridViewTextBoxColumn34.Name = "Descripcion";
            gridViewTextBoxColumn34.Width = 200;
            gridViewTextBoxColumn35.FieldName = "FileName";
            gridViewTextBoxColumn35.HeaderText = "Archivo";
            gridViewTextBoxColumn35.Name = "FileName";
            gridViewTextBoxColumn35.Width = 150;
            gridViewCommandColumn1.FieldName = "Button";
            gridViewCommandColumn1.HeaderText = "Descarga";
            gridViewCommandColumn1.Name = "Button";
            gridViewCommandColumn1.Width = 85;
            gridViewTextBoxColumn36.FieldName = "URL";
            gridViewTextBoxColumn36.HeaderText = "URL";
            gridViewTextBoxColumn36.IsVisible = false;
            gridViewTextBoxColumn36.Name = "URL";
            gridViewTextBoxColumn36.VisibleInColumnChooser = false;
            this.GridAuxiliar.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewCommandColumn1,
            gridViewTextBoxColumn36});
            this.GridAuxiliar.ViewDefinition = tableViewDefinition3;
            // 
            // GridTransferencia
            // 
            this.GridTransferencia.Caption = "Transferencia";
            gridViewTextBoxColumn33.FieldName = "Identificador";
            gridViewTextBoxColumn33.HeaderText = "Identificador";
            gridViewTextBoxColumn33.Name = "Identificador";
            gridViewTextBoxColumn33.ReadOnly = true;
            gridViewTextBoxColumn33.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn33.Width = 75;
            this.GridTransferencia.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33});
            this.GridTransferencia.ViewDefinition = tableViewDefinition2;
            // 
            // MovimientosGridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GridData);
            this.Name = "MovimientosGridControl";
            this.Size = new System.Drawing.Size(1098, 465);
            this.Load += new System.EventHandler(this.MovimientosGridControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridComprobantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAuxiliar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridTransferencia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public Telerik.WinControls.UI.RadGridView GridData;
        protected internal Telerik.WinControls.UI.GridViewTemplate GridComprobantes;
        protected internal Telerik.WinControls.UI.GridViewTemplate GridAuxiliar;
        protected internal Telerik.WinControls.UI.GridViewTemplate GridTransferencia;
    }
}
