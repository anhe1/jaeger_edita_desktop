﻿namespace Jaeger.UI.Banco.Forms {
    partial class TbMovimientoComprobanteControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Bar = new Telerik.WinControls.UI.RadCommandBar();
            this.TDocumento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblDocumento = new Telerik.WinControls.UI.CommandBarLabel();
            this.Documento = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.Separator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.HostItem1 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Nuevo = new Telerik.WinControls.UI.CommandBarButton();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Remover = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).BeginInit();
            this.Bar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Bar
            // 
            this.Bar.Controls.Add(this.TDocumento);
            this.Bar.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bar.Location = new System.Drawing.Point(0, 0);
            this.Bar.Name = "Bar";
            this.Bar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.Bar.Size = new System.Drawing.Size(707, 55);
            this.Bar.TabIndex = 0;
            // 
            // TDocumento
            // 
            this.TDocumento.AutoSizeDropDownToBestFit = true;
            this.TDocumento.DisplayMember = "IdDocumento";
            // 
            // TDocumento.NestedRadGridView
            // 
            this.TDocumento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.TDocumento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TDocumento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TDocumento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.TDocumento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.TDocumento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.TDocumento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.TDocumento.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "TipoComprobanteText";
            gridViewTextBoxColumn1.HeaderText = "Tipo";
            gridViewTextBoxColumn1.Name = "TipoComprobanteText";
            gridViewTextBoxColumn2.FieldName = "Folio";
            gridViewTextBoxColumn2.HeaderText = "Folio";
            gridViewTextBoxColumn2.Name = "Folio";
            gridViewTextBoxColumn3.FieldName = "Serie";
            gridViewTextBoxColumn3.HeaderText = "Serie";
            gridViewTextBoxColumn3.Name = "Serie";
            gridViewTextBoxColumn4.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn4.FieldName = "FechaEmision";
            gridViewTextBoxColumn4.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn4.Name = "FechaEmision";
            gridViewTextBoxColumn5.FieldName = "EmisorRFC";
            gridViewTextBoxColumn5.HeaderText = "EmisorRFC";
            gridViewTextBoxColumn5.Name = "EmisorRFC";
            gridViewTextBoxColumn6.FieldName = "Emisor";
            gridViewTextBoxColumn6.HeaderText = "Emisor";
            gridViewTextBoxColumn6.Name = "Emisor";
            gridViewTextBoxColumn7.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn7.HeaderText = "ReceptorRFC";
            gridViewTextBoxColumn7.Name = "ReceptorRFC";
            gridViewTextBoxColumn8.FieldName = "Receptor";
            gridViewTextBoxColumn8.HeaderText = "Receptor";
            gridViewTextBoxColumn8.Name = "Receptor";
            gridViewTextBoxColumn9.FieldName = "IdDocumento";
            gridViewTextBoxColumn9.HeaderText = "IdDocumento";
            gridViewTextBoxColumn9.Name = "IdDocumento";
            gridViewTextBoxColumn10.FieldName = "Total";
            gridViewTextBoxColumn10.FormatString = "{0:n2}";
            gridViewTextBoxColumn10.HeaderText = "Total";
            gridViewTextBoxColumn10.Name = "Total";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.FieldName = "Acumulado";
            gridViewTextBoxColumn11.FormatString = "{0:n2}";
            gridViewTextBoxColumn11.HeaderText = "Acumulado";
            gridViewTextBoxColumn11.Name = "Acumulado";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.FieldName = "Estado";
            gridViewTextBoxColumn12.HeaderText = "Estado";
            gridViewTextBoxColumn12.Name = "Estado";
            this.TDocumento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.TDocumento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.TDocumento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.TDocumento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.TDocumento.EditorControl.Name = "NestedRadGridView";
            this.TDocumento.EditorControl.ReadOnly = true;
            this.TDocumento.EditorControl.ShowGroupPanel = false;
            this.TDocumento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.TDocumento.EditorControl.TabIndex = 0;
            this.TDocumento.Location = new System.Drawing.Point(168, 5);
            this.TDocumento.Name = "TDocumento";
            this.TDocumento.Size = new System.Drawing.Size(242, 20);
            this.TDocumento.TabIndex = 2;
            this.TDocumento.TabStop = false;
            // 
            // BarRowElement
            // 
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Comprobante";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblDocumento,
            this.Documento,
            this.Separator1,
            this.HostItem1,
            this.Separator2,
            this.Agregar,
            this.Nuevo,
            this.Buscar,
            this.Remover});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // lblDocumento
            // 
            this.lblDocumento.DisplayName = "commandBarLabel1";
            this.lblDocumento.Name = "lblDocumento";
            this.lblDocumento.Text = "Documento: ";
            // 
            // Documento
            // 
            this.Documento.DropDownAnimationEnabled = true;
            this.Documento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Documento.MaxDropDownItems = 0;
            this.Documento.MaxSize = new System.Drawing.Size(100, 22);
            this.Documento.MinSize = new System.Drawing.Size(95, 22);
            this.Documento.Name = "Documento";
            this.Documento.Text = "";
            // 
            // Separator1
            // 
            this.Separator1.DisplayName = "commandBarSeparator1";
            this.Separator1.Name = "Separator1";
            this.Separator1.VisibleInOverflowMenu = false;
            // 
            // HostItem1
            // 
            this.HostItem1.DisplayName = "Comprobantes";
            this.HostItem1.MinSize = new System.Drawing.Size(350, 15);
            this.HostItem1.Name = "HostItem1";
            this.HostItem1.Text = "Comprobantes";
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Agregar
            // 
            this.Agregar.DisplayName = "commandBarButton1";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Banco.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Nuevo
            // 
            this.Nuevo.DisplayName = "commandBarButton2";
            this.Nuevo.DrawText = true;
            this.Nuevo.Image = global::Jaeger.UI.Banco.Properties.Resources.new_file_16px;
            this.Nuevo.Name = "Nuevo";
            this.Nuevo.Text = "Nuevo";
            this.Nuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Buscar
            // 
            this.Buscar.DisplayName = "commandBarButton3";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Banco.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Remover
            // 
            this.Remover.DisplayName = "commandBarButton4";
            this.Remover.DrawText = true;
            this.Remover.Image = global::Jaeger.UI.Banco.Properties.Resources.delete_16px;
            this.Remover.Name = "Remover";
            this.Remover.Text = "Remover";
            this.Remover.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbMovimientoComprobanteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Bar);
            this.Name = "TbMovimientoComprobanteControl";
            this.Size = new System.Drawing.Size(707, 30);
            this.Load += new System.EventHandler(this.TbMovimientoComprobanteControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).EndInit();
            this.Bar.ResumeLayout(false);
            this.Bar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TDocumento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar Bar;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.CommandBarLabel lblDocumento;
        private Telerik.WinControls.UI.CommandBarSeparator Separator1;
        private Telerik.WinControls.UI.CommandBarHostItem HostItem1;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        internal Telerik.WinControls.UI.CommandBarButton Agregar;
        internal Telerik.WinControls.UI.CommandBarButton Nuevo;
        internal Telerik.WinControls.UI.CommandBarButton Buscar;
        internal Telerik.WinControls.UI.CommandBarButton Remover;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox TDocumento;
        public Telerik.WinControls.UI.CommandBarDropDownList Documento;
    }
}
