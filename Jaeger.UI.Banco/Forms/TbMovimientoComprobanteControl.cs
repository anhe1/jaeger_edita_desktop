﻿using System;
using System.Windows.Forms;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.UI.Common.Services;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Forms {
    public partial class TbMovimientoComprobanteControl : UserControl {
        private bool _ReadOnly = false;

        public TbMovimientoComprobanteControl() {
            InitializeComponent();
        }

        private void TbMovimientoComprobanteControl_Load(object sender, EventArgs e) {
            this.HostItem1.HostedItem = this.TDocumento.MultiColumnComboBoxElement;
            this.ToolBar.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.ToolBar.Grip.Enabled = false;
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;
            // se suspende porque los tipos de documentos se cargan con el tipo de movimiento
            //this.Documento.DataSource = Aplication.Banco.BancoService.GetTipoComprobante();
            this.Documento.ValueMember = "Id";
            this.Documento.DisplayMember = "Descriptor";
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        public virtual MovimientoBancarioTipoComprobanteEnum GetTipoComprobante() {
            var selectedValue = this.Documento.SelectedItem as RadListDataItem;
            if (selectedValue != null) {
                var seleccionado = selectedValue.DataBoundItem as Domain.Base.Entities.ItemSelectedModel;
                if (seleccionado != null) {
                    return (MovimientoBancarioTipoComprobanteEnum)Enum.Parse(typeof(MovimientoBancarioTipoComprobanteEnum), seleccionado.Name.Replace('ó','o').ToString());
                } else {
                    Console.WriteLine("No se selecciono un objeto valido");
                }
            }
            return MovimientoBancarioTipoComprobanteEnum.NA;
        }

        public virtual void CreateReadOnly() {
            if (this._ReadOnly) {
                this.Documento.Enabled = false;
                this.Agregar.Enabled = false;
                this.Remover.Enabled = false;
                this.Nuevo.Enabled = false;
                this.Buscar.Enabled = false;
                this.TDocumento.SetEditable(false);
            } else {
                this.Documento.Enabled = true;
                this.Agregar.Enabled = true;
                this.Remover.Enabled = true;
                this.Nuevo.Enabled = true;
                this.Buscar.Enabled = true;
                this.TDocumento.SetEditable(true);
            }
        }
    }
}
