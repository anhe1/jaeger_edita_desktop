﻿using System;
using System.ComponentModel;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.UI.Banco.Forms {
    public partial class BeneficiarioBuscarForm : RadForm {
        protected IBancoService service;
        public BindingList<BeneficiarioDetailModel> datos;
        private BindingList<ItemSelectedModel> relationType;

        public event EventHandler<BeneficiarioDetailModel> Selected;
        public void OnSelected(BeneficiarioDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public BeneficiarioBuscarForm(BindingList<ItemSelectedModel> relationType, IBancoService service) {
            InitializeComponent();
            this.service = service;
            this.relationType = relationType;
        }

        private void ClienteBuscarForm_Load(object sender, EventArgs e) {
            this.dataGridDirectorio.Standard();
            this.ToolBar.Descripcion.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.ToolBar.Descripcion.Text.Length > 4) {
                Actualizar_Click(sender, e);
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            //var _busqueda = "%" + this.ToolBar.Descripcion.Text.Replace("á", "%").Replace("é", "%").Replace("í", "%").Replace("ó", "%").Replace("ú", "%").Replace("ñ", "%").Replace("e", "%").Replace("i", "%").Replace("o", "%").Replace("u", "%").Replace(" ", "%") + "%";
            //this.datos = new BindingList<BeneficiarioDetailModel>(this.service.GetBeneficiarios(this.relationType, _busqueda));
            this.dataGridDirectorio.DataSource = this.datos;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void dataGridDirectorio_DoubleClick(object sender, EventArgs e) {
            if (this.dataGridDirectorio.CurrentRow != null) {
                var seleccionado = this.dataGridDirectorio.CurrentRow.DataBoundItem as BeneficiarioDetailModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                    this.ToolBar.Cerrar.PerformClick();
                }
            }
        }

        public virtual void Consultar() {
            var _busqueda = "%" + this.ToolBar.Descripcion.Text.Replace("á", "%").Replace("é", "%").Replace("í", "%").Replace("ó", "%").Replace("ú", "%").Replace("ñ", "%").Replace("e", "%").Replace("i", "%").Replace("o", "%").Replace("u", "%").Replace(" ", "%") + "%";
            this.datos = new BindingList<BeneficiarioDetailModel>(this.service.GetBeneficiarios(this.relationType, _busqueda));
        }
    }
}
