﻿namespace Jaeger.UI.Banco.Forms
{
    partial class BancoReporteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BancoReporteForm));
            this.ConceptoMovimiento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.CuentaBancaria = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.textBoxNumeroCuentaP = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.textBoxBancoP = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.FechaDocumento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Crear = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumeroCuentaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxBancoP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ConceptoMovimiento
            // 
            this.ConceptoMovimiento.AutoSizeDropDownToBestFit = true;
            this.ConceptoMovimiento.DisplayMember = "Descripcion";
            this.ConceptoMovimiento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ConceptoMovimiento.NestedRadGridView
            // 
            this.ConceptoMovimiento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ConceptoMovimiento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConceptoMovimiento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ConceptoMovimiento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowDeleteRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowEditRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowRowHeaderContextMenu = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdConcepto";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            this.ConceptoMovimiento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.ConceptoMovimiento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.ConceptoMovimiento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.ConceptoMovimiento.EditorControl.Name = "NestedRadGridView";
            this.ConceptoMovimiento.EditorControl.ReadOnly = true;
            this.ConceptoMovimiento.EditorControl.ShowGroupPanel = false;
            this.ConceptoMovimiento.EditorControl.ShowRowErrors = false;
            this.ConceptoMovimiento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ConceptoMovimiento.EditorControl.TabIndex = 0;
            this.ConceptoMovimiento.Location = new System.Drawing.Point(128, 63);
            this.ConceptoMovimiento.MinimumSize = new System.Drawing.Size(201, 20);
            this.ConceptoMovimiento.Name = "ConceptoMovimiento";
            this.ConceptoMovimiento.NullText = "Documento";
            // 
            // 
            // 
            this.ConceptoMovimiento.RootElement.MinSize = new System.Drawing.Size(201, 20);
            this.ConceptoMovimiento.Size = new System.Drawing.Size(277, 20);
            this.ConceptoMovimiento.TabIndex = 1;
            this.ConceptoMovimiento.TabStop = false;
            this.ConceptoMovimiento.ValueMember = "IdConcepto";
            // 
            // radLabel20
            // 
            this.radLabel20.Location = new System.Drawing.Point(12, 63);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(110, 18);
            this.radLabel20.TabIndex = 329;
            this.radLabel20.Text = "Tipo de Movimiento:";
            // 
            // RadLabel13
            // 
            this.RadLabel13.Location = new System.Drawing.Point(331, 38);
            this.RadLabel13.Name = "RadLabel13";
            this.RadLabel13.Size = new System.Drawing.Size(74, 18);
            this.RadLabel13.TabIndex = 321;
            this.RadLabel13.Text = "Núm. Cuenta:";
            // 
            // CuentaBancaria
            // 
            this.CuentaBancaria.AutoSizeDropDownToBestFit = true;
            this.CuentaBancaria.DisplayMember = "Alias";
            this.CuentaBancaria.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // CuentaBancaria.NestedRadGridView
            // 
            this.CuentaBancaria.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.CuentaBancaria.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CuentaBancaria.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CuentaBancaria.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.CuentaBancaria.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CuentaBancaria.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CuentaBancaria.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CuentaBancaria.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdCuenta";
            gridViewTextBoxColumn3.HeaderText = "IdCuenta";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "IdCuenta";
            gridViewTextBoxColumn4.FieldName = "NumCuenta";
            gridViewTextBoxColumn4.HeaderText = "Núm. de Cuenta";
            gridViewTextBoxColumn4.Name = "NumCuenta";
            gridViewTextBoxColumn5.FieldName = "Beneficiario";
            gridViewTextBoxColumn5.HeaderText = "Beneficiario";
            gridViewTextBoxColumn5.Name = "Beneficiario";
            gridViewTextBoxColumn5.Width = 120;
            gridViewTextBoxColumn6.FieldName = "Alias";
            gridViewTextBoxColumn6.HeaderText = "Alias";
            gridViewTextBoxColumn6.Name = "Alias";
            gridViewTextBoxColumn6.Width = 95;
            this.CuentaBancaria.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.CuentaBancaria.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CuentaBancaria.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CuentaBancaria.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.CuentaBancaria.EditorControl.Name = "NestedRadGridView";
            this.CuentaBancaria.EditorControl.ReadOnly = true;
            this.CuentaBancaria.EditorControl.ShowGroupPanel = false;
            this.CuentaBancaria.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CuentaBancaria.EditorControl.TabIndex = 0;
            this.CuentaBancaria.Location = new System.Drawing.Point(62, 11);
            this.CuentaBancaria.Name = "CuentaBancaria";
            this.CuentaBancaria.NullText = "Selecciona";
            this.CuentaBancaria.Size = new System.Drawing.Size(343, 20);
            this.CuentaBancaria.TabIndex = 21;
            this.CuentaBancaria.TabStop = false;
            this.CuentaBancaria.ValueMember = "IdCuenta";
            // 
            // textBoxNumeroCuentaP
            // 
            this.textBoxNumeroCuentaP.Location = new System.Drawing.Point(406, 37);
            this.textBoxNumeroCuentaP.Name = "textBoxNumeroCuentaP";
            this.textBoxNumeroCuentaP.ReadOnly = true;
            this.textBoxNumeroCuentaP.Size = new System.Drawing.Size(101, 20);
            this.textBoxNumeroCuentaP.TabIndex = 326;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(44, 18);
            this.radLabel1.TabIndex = 22;
            this.radLabel1.Text = "Cuenta:";
            // 
            // RadLabel14
            // 
            this.RadLabel14.Location = new System.Drawing.Point(12, 38);
            this.RadLabel14.Name = "RadLabel14";
            this.RadLabel14.Size = new System.Drawing.Size(39, 18);
            this.RadLabel14.TabIndex = 322;
            this.RadLabel14.Text = "Banco:";
            // 
            // textBoxBancoP
            // 
            this.textBoxBancoP.Location = new System.Drawing.Point(62, 37);
            this.textBoxBancoP.Name = "textBoxBancoP";
            this.textBoxBancoP.NullText = "Banco";
            this.textBoxBancoP.ReadOnly = true;
            this.textBoxBancoP.Size = new System.Drawing.Size(260, 20);
            this.textBoxBancoP.TabIndex = 324;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(416, 12);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(91, 18);
            this.radCheckBox1.TabIndex = 330;
            this.radCheckBox1.Text = "radCheckBox1";
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Location = new System.Drawing.Point(416, 63);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(91, 18);
            this.radCheckBox2.TabIndex = 330;
            this.radCheckBox2.Text = "radCheckBox1";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(12, 90);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(64, 18);
            this.RadLabel8.TabIndex = 331;
            this.RadLabel8.Text = "Fec. Docto.:";
            // 
            // FechaDocumento
            // 
            this.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDocumento.Location = new System.Drawing.Point(78, 89);
            this.FechaDocumento.Name = "FechaDocumento";
            this.FechaDocumento.Size = new System.Drawing.Size(92, 20);
            this.FechaDocumento.TabIndex = 332;
            this.FechaDocumento.TabStop = false;
            this.FechaDocumento.Text = "15/12/2016";
            this.FechaDocumento.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 116);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(64, 18);
            this.radLabel2.TabIndex = 333;
            this.radLabel2.Text = "Fec. Docto.:";
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radDateTimePicker1.Location = new System.Drawing.Point(78, 115);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(92, 20);
            this.radDateTimePicker1.TabIndex = 334;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "15/12/2016";
            this.radDateTimePicker1.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // Crear
            // 
            this.Crear.Location = new System.Drawing.Point(353, 121);
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(110, 24);
            this.Crear.TabIndex = 335;
            this.Crear.Text = "radButton1";
            this.Crear.Click += new System.EventHandler(this.Crear_Click);
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(353, 151);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(110, 24);
            this.radButton2.TabIndex = 335;
            this.radButton2.Text = "radButton1";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // BancoReporteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 214);
            this.Controls.Add(this.radButton2);
            this.Controls.Add(this.Crear);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radDateTimePicker1);
            this.Controls.Add(this.RadLabel8);
            this.Controls.Add(this.FechaDocumento);
            this.Controls.Add(this.radCheckBox2);
            this.Controls.Add(this.radCheckBox1);
            this.Controls.Add(this.ConceptoMovimiento);
            this.Controls.Add(this.RadLabel13);
            this.Controls.Add(this.radLabel20);
            this.Controls.Add(this.CuentaBancaria);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.textBoxNumeroCuentaP);
            this.Controls.Add(this.textBoxBancoP);
            this.Controls.Add(this.RadLabel14);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BancoReporteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reporte";
            this.Load += new System.EventHandler(this.BancoReporteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoMovimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumeroCuentaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxBancoP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Crear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMultiColumnComboBox ConceptoMovimiento;
        internal Telerik.WinControls.UI.RadLabel radLabel20;
        internal Telerik.WinControls.UI.RadLabel RadLabel13;
        private Telerik.WinControls.UI.RadMultiColumnComboBox CuentaBancaria;
        private Telerik.WinControls.UI.RadTextBox textBoxNumeroCuentaP;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadLabel RadLabel14;
        internal Telerik.WinControls.UI.RadTextBox textBoxBancoP;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDocumento;
        internal Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadButton Crear;
        private Telerik.WinControls.UI.RadButton radButton2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}
