﻿namespace Jaeger.UI.Banco.Forms
{
    partial class FormaPagoCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormaPagoCatalogoForm));
            this.TFormaP = new Jaeger.UI.Banco.Forms.FormaPagoGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TFormaP
            // 
            this.TFormaP.Caption = "";
            this.TFormaP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TFormaP.Location = new System.Drawing.Point(0, 0);
            this.TFormaP.Name = "TFormaP";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TFormaP.Permisos = uiAction1;
            this.TFormaP.ReadOnly = false;
            this.TFormaP.ShowActualizar = true;
            this.TFormaP.ShowAutorizar = false;
            this.TFormaP.ShowAutosuma = false;
            this.TFormaP.ShowCerrar = true;
            this.TFormaP.ShowEditar = true;
            this.TFormaP.ShowExportarExcel = false;
            this.TFormaP.ShowFiltro = true;
            this.TFormaP.ShowHerramientas = false;
            this.TFormaP.ShowImagen = false;
            this.TFormaP.ShowImprimir = false;
            this.TFormaP.ShowItem = false;
            this.TFormaP.ShowNuevo = true;
            this.TFormaP.ShowRemover = true;
            this.TFormaP.ShowSeleccionMultiple = true;
            this.TFormaP.Size = new System.Drawing.Size(744, 474);
            this.TFormaP.TabIndex = 0;
            // 
            // FormaPagoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 474);
            this.Controls.Add(this.TFormaP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormaPagoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Banco: Formas de Pago";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormaPagoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FormaPagoGridControl TFormaP;
    }
}
