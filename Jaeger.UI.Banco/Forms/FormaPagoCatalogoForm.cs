﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.UI.Banco.Forms {
    public partial class FormaPagoCatalogoForm : RadForm {
        protected internal IBancoFormaPagoService Service;
        protected internal BindingList<IBancoFormaPagoDetailModel> _DataSource;
        
        public FormaPagoCatalogoForm() {
            InitializeComponent();
            this.TFormaP.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public FormaPagoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this.TFormaP.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void FormaPagoCatalogoForm_Load(object sender, EventArgs e) {
            this.TFormaP.Nuevo.Click += this.Agregar_Click;
            this.TFormaP.Editar.Click += this.Editar_Click;
            this.TFormaP.Remover.Click += this.Remover_Click;
            this.TFormaP.Actualizar.Click += this.Actualizar_Click;
            this.TFormaP.Cerrar.Click += this.Cerrar_Click;
            //this.OnLoad();
        }

        public virtual void OnLoad() {
            this.Service = new BancoFormaPagoService();
        }

        public virtual void Agregar_Click(object sender, EventArgs e) {
            var _nuevo = new FormaPagoForm(this.Service);
            _nuevo.ShowDialog(this);
        }

        public virtual void Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TFormaP.GetCurrent<BancoFormaPagoDetailModel>();
            if (seleccionado != null) {
                using (var espera = new FormaPagoForm(seleccionado, this.Service)) {
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TFormaP.GetCurrent<BancoFormaPagoDetailModel>();
            if (seleccionado != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_FormaPago_Eliminar, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    using (var espera = new Waiting2Form(this.Desactivar)) {
                        espera.Text = "Actualizando ...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TFormaP.GridData.DataSource = this._DataSource;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void Consultar() {
            this._DataSource = new BindingList<IBancoFormaPagoDetailModel>(this.Service.GetList<BancoFormaPagoDetailModel>(BancoFormaPagoService.Query().OnlyActive(true).Build()).ToList<IBancoFormaPagoDetailModel>());
        }

        public virtual void Desactivar() {
            var seleccionado = this.TFormaP.GetCurrent<BancoFormaPagoDetailModel>() as IBancoFormaPagoDetailModel;
            if (seleccionado != null) {
                seleccionado.Activo = !seleccionado.Activo;
                seleccionado = this.Service.Save(seleccionado);
            }
        }
    }
}
