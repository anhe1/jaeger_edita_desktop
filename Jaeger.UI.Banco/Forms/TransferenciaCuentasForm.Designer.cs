﻿namespace Jaeger.UI.Banco.Forms
{
    partial class TransferenciaCuentasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor2 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferenciaCuentasForm));
            this.comboBoxCuentaOrigen = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblCuenta1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxBeneficiario = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Group1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.FormaPago = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.NumDocto = new Telerik.WinControls.UI.RadTextBox();
            this.FechaDocumento = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaAplicacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbl_Origen_NumCuenta = new Telerik.WinControls.UI.RadLabel();
            this.NumeroCuentaP = new Telerik.WinControls.UI.RadTextBox();
            this.lblOrigen_Banco = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.BancoP = new Telerik.WinControls.UI.RadTextBox();
            this.CuentaClabeP = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.BancoT = new Telerik.WinControls.UI.RadTextBox();
            this.NumeroCuentaT = new Telerik.WinControls.UI.RadTextBox();
            this.CuentaClabeT = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.TipoCambio = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblTipoCambio = new Telerik.WinControls.UI.RadLabel();
            this.LabelAbono = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.Concepto = new Telerik.WinControls.UI.RadDropDownList();
            this.NumAuto = new Telerik.WinControls.UI.RadTextBox();
            this.Referencia = new Telerik.WinControls.UI.RadTextBox();
            this.RadLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.RadLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.lblNumOperacion = new Telerik.WinControls.UI.RadLabel();
            this.NumOperacion = new Telerik.WinControls.UI.RadTextBox();
            this.Cargo = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            this.TMovimiento = new Jaeger.UI.Banco.Forms.TbMovimientoBancarioControl();
            this.Preparar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuenta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1)).BeginInit();
            this.Group1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDocto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_Origen_NumCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrigen_Banco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAuto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cargo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxCuentaOrigen
            // 
            this.comboBoxCuentaOrigen.AutoSizeDropDownToBestFit = true;
            this.comboBoxCuentaOrigen.DisplayMember = "Alias";
            this.comboBoxCuentaOrigen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // comboBoxCuentaOrigen.NestedRadGridView
            // 
            this.comboBoxCuentaOrigen.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxCuentaOrigen.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCuentaOrigen.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxCuentaOrigen.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdCuenta";
            gridViewTextBoxColumn1.HeaderText = "IdCuenta";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdCuenta";
            gridViewTextBoxColumn2.FieldName = "NumCuenta";
            gridViewTextBoxColumn2.HeaderText = "Núm. de Cuenta";
            gridViewTextBoxColumn2.Name = "NumCuenta";
            gridViewTextBoxColumn3.FieldName = "Beneficiario";
            gridViewTextBoxColumn3.HeaderText = "Beneficiario";
            gridViewTextBoxColumn3.Name = "Beneficiario";
            gridViewTextBoxColumn3.Width = 120;
            gridViewTextBoxColumn4.FieldName = "Alias";
            gridViewTextBoxColumn4.HeaderText = "Alias";
            gridViewTextBoxColumn4.Name = "Alias";
            gridViewTextBoxColumn4.Width = 95;
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxCuentaOrigen.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.comboBoxCuentaOrigen.EditorControl.Name = "NestedRadGridView";
            this.comboBoxCuentaOrigen.EditorControl.ReadOnly = true;
            this.comboBoxCuentaOrigen.EditorControl.ShowGroupPanel = false;
            this.comboBoxCuentaOrigen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxCuentaOrigen.EditorControl.TabIndex = 0;
            this.comboBoxCuentaOrigen.Location = new System.Drawing.Point(84, 22);
            this.comboBoxCuentaOrigen.Name = "comboBoxCuentaOrigen";
            this.comboBoxCuentaOrigen.NullText = "Selecciona";
            this.comboBoxCuentaOrigen.Size = new System.Drawing.Size(428, 20);
            this.comboBoxCuentaOrigen.TabIndex = 21;
            this.comboBoxCuentaOrigen.TabStop = false;
            this.comboBoxCuentaOrigen.ValueMember = "IdCuenta";
            // 
            // lblCuenta1
            // 
            this.lblCuenta1.Location = new System.Drawing.Point(13, 23);
            this.lblCuenta1.Name = "lblCuenta1";
            this.lblCuenta1.Size = new System.Drawing.Size(44, 18);
            this.lblCuenta1.TabIndex = 22;
            this.lblCuenta1.Text = "Cuenta:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(13, 23);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(44, 18);
            this.radLabel2.TabIndex = 23;
            this.radLabel2.Text = "Cuenta:";
            // 
            // comboBoxBeneficiario
            // 
            this.comboBoxBeneficiario.AutoSizeDropDownToBestFit = true;
            this.comboBoxBeneficiario.DisplayMember = "Beneficiario";
            this.comboBoxBeneficiario.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // comboBoxBeneficiario.NestedRadGridView
            // 
            this.comboBoxBeneficiario.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxBeneficiario.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxBeneficiario.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxBeneficiario.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn5.DataType = typeof(int);
            gridViewTextBoxColumn5.FieldName = "IdCuenta";
            gridViewTextBoxColumn5.HeaderText = "IdCuenta";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "IdCuenta";
            gridViewTextBoxColumn6.FieldName = "NumCuenta";
            gridViewTextBoxColumn6.HeaderText = "Núm. de Cuenta";
            gridViewTextBoxColumn6.Name = "NumCuenta";
            gridViewTextBoxColumn7.FieldName = "Beneficiario";
            gridViewTextBoxColumn7.HeaderText = "Beneficiario";
            gridViewTextBoxColumn7.Name = "Beneficiario";
            gridViewTextBoxColumn7.Width = 120;
            gridViewTextBoxColumn8.FieldName = "Alias";
            gridViewTextBoxColumn8.HeaderText = "Alias";
            gridViewTextBoxColumn8.Name = "Alias";
            gridViewTextBoxColumn8.Width = 95;
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.EnableGrouping = false;
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.comboBoxBeneficiario.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.comboBoxBeneficiario.EditorControl.Name = "NestedRadGridView";
            this.comboBoxBeneficiario.EditorControl.ReadOnly = true;
            this.comboBoxBeneficiario.EditorControl.ShowGroupPanel = false;
            this.comboBoxBeneficiario.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.comboBoxBeneficiario.EditorControl.TabIndex = 0;
            this.comboBoxBeneficiario.Location = new System.Drawing.Point(84, 21);
            this.comboBoxBeneficiario.Name = "comboBoxBeneficiario";
            this.comboBoxBeneficiario.NullText = "Selecciona";
            this.comboBoxBeneficiario.Size = new System.Drawing.Size(428, 20);
            this.comboBoxBeneficiario.TabIndex = 24;
            this.comboBoxBeneficiario.TabStop = false;
            this.comboBoxBeneficiario.ValueMember = "IdCuenta";
            // 
            // Group1
            // 
            this.Group1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Group1.Controls.Add(this.radLabel1);
            this.Group1.Controls.Add(this.FechaEmision);
            this.Group1.Controls.Add(this.radLabel3);
            this.Group1.Controls.Add(this.RadLabel7);
            this.Group1.Controls.Add(this.RadLabel8);
            this.Group1.Controls.Add(this.RadLabel9);
            this.Group1.Controls.Add(this.FormaPago);
            this.Group1.Controls.Add(this.NumDocto);
            this.Group1.Controls.Add(this.FechaDocumento);
            this.Group1.Controls.Add(this.FechaAplicacion);
            this.Group1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Group1.HeaderText = "Forma de Pago";
            this.Group1.Location = new System.Drawing.Point(0, 30);
            this.Group1.Name = "Group1";
            this.Group1.Size = new System.Drawing.Size(676, 74);
            this.Group1.TabIndex = 25;
            this.Group1.Text = "Forma de Pago";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(297, 16);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(78, 18);
            this.radLabel1.TabIndex = 299;
            this.radLabel1.Text = "Fecha de Reg.:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaEmision.Location = new System.Drawing.Point(379, 15);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(85, 20);
            this.FechaEmision.TabIndex = 300;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "15/12/2016";
            this.FechaEmision.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(13, 42);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(85, 18);
            this.radLabel3.TabIndex = 291;
            this.radLabel3.Text = "Forma de pago:";
            // 
            // RadLabel7
            // 
            this.RadLabel7.Location = new System.Drawing.Point(326, 42);
            this.RadLabel7.Name = "RadLabel7";
            this.RadLabel7.Size = new System.Drawing.Size(33, 18);
            this.RadLabel7.TabIndex = 292;
            this.RadLabel7.Text = "Folio:";
            // 
            // RadLabel8
            // 
            this.RadLabel8.Location = new System.Drawing.Point(478, 17);
            this.RadLabel8.Name = "RadLabel8";
            this.RadLabel8.Size = new System.Drawing.Size(37, 18);
            this.RadLabel8.TabIndex = 293;
            this.RadLabel8.Text = "Fecha:";
            // 
            // RadLabel9
            // 
            this.RadLabel9.Location = new System.Drawing.Point(572, 16);
            this.RadLabel9.Name = "RadLabel9";
            this.RadLabel9.Size = new System.Drawing.Size(82, 18);
            this.RadLabel9.TabIndex = 294;
            this.RadLabel9.Text = "Fec. Aplicación:";
            // 
            // FormaPago
            // 
            this.FormaPago.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.FormaPago.AutoSizeDropDownToBestFit = true;
            this.FormaPago.DisplayMember = "Descripcion";
            this.FormaPago.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // FormaPago.NestedRadGridView
            // 
            this.FormaPago.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.FormaPago.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormaPago.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormaPago.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.FormaPago.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.FormaPago.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn9.FieldName = "Clave";
            gridViewTextBoxColumn9.HeaderText = "Clave";
            gridViewTextBoxColumn9.Name = "Clave";
            gridViewTextBoxColumn10.FieldName = "Descripcion";
            gridViewTextBoxColumn10.HeaderText = "Método";
            gridViewTextBoxColumn10.Name = "Descripcion";
            gridViewTextBoxColumn10.Width = 150;
            this.FormaPago.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.FormaPago.EditorControl.MasterTemplate.EnableGrouping = false;
            this.FormaPago.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.FormaPago.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.FormaPago.EditorControl.Name = "NestedRadGridView";
            this.FormaPago.EditorControl.ReadOnly = true;
            this.FormaPago.EditorControl.ShowGroupPanel = false;
            this.FormaPago.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.FormaPago.EditorControl.TabIndex = 0;
            this.FormaPago.Enabled = false;
            this.FormaPago.Location = new System.Drawing.Point(104, 41);
            this.FormaPago.Name = "FormaPago";
            this.FormaPago.NullText = "Forma de pago";
            this.FormaPago.Size = new System.Drawing.Size(216, 20);
            this.FormaPago.TabIndex = 295;
            this.FormaPago.TabStop = false;
            this.FormaPago.ValueMember = "Clave";
            // 
            // NumDocto
            // 
            this.NumDocto.Location = new System.Drawing.Point(365, 41);
            this.NumDocto.Name = "NumDocto";
            this.NumDocto.NullText = "Folio";
            this.NumDocto.Size = new System.Drawing.Size(99, 20);
            this.NumDocto.TabIndex = 296;
            this.NumDocto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FechaDocumento
            // 
            this.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaDocumento.Location = new System.Drawing.Point(478, 41);
            this.FechaDocumento.Name = "FechaDocumento";
            this.FechaDocumento.Size = new System.Drawing.Size(85, 20);
            this.FechaDocumento.TabIndex = 297;
            this.FechaDocumento.TabStop = false;
            this.FechaDocumento.Text = "15/12/2016";
            this.FechaDocumento.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // FechaAplicacion
            // 
            this.FechaAplicacion.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaAplicacion.Location = new System.Drawing.Point(569, 40);
            this.FechaAplicacion.Name = "FechaAplicacion";
            this.FechaAplicacion.Size = new System.Drawing.Size(95, 20);
            this.FechaAplicacion.TabIndex = 298;
            this.FechaAplicacion.TabStop = false;
            this.FechaAplicacion.Text = "15/12/2016";
            this.FechaAplicacion.Value = new System.DateTime(2016, 12, 15, 20, 4, 51, 874);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.lbl_Origen_NumCuenta);
            this.radGroupBox2.Controls.Add(this.NumeroCuentaP);
            this.radGroupBox2.Controls.Add(this.lblOrigen_Banco);
            this.radGroupBox2.Controls.Add(this.RadLabel15);
            this.radGroupBox2.Controls.Add(this.BancoP);
            this.radGroupBox2.Controls.Add(this.CuentaClabeP);
            this.radGroupBox2.Controls.Add(this.comboBoxCuentaOrigen);
            this.radGroupBox2.Controls.Add(this.lblCuenta1);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox2.HeaderText = "Datos de cuenta origen";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 104);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(676, 81);
            this.radGroupBox2.TabIndex = 26;
            this.radGroupBox2.Text = "Datos de cuenta origen";
            // 
            // lbl_Origen_NumCuenta
            // 
            this.lbl_Origen_NumCuenta.Location = new System.Drawing.Point(13, 49);
            this.lbl_Origen_NumCuenta.Name = "lbl_Origen_NumCuenta";
            this.lbl_Origen_NumCuenta.Size = new System.Drawing.Size(74, 18);
            this.lbl_Origen_NumCuenta.TabIndex = 321;
            this.lbl_Origen_NumCuenta.Text = "Núm. Cuenta:";
            // 
            // NumeroCuentaP
            // 
            this.NumeroCuentaP.Location = new System.Drawing.Point(93, 48);
            this.NumeroCuentaP.Name = "NumeroCuentaP";
            this.NumeroCuentaP.ReadOnly = true;
            this.NumeroCuentaP.Size = new System.Drawing.Size(100, 20);
            this.NumeroCuentaP.TabIndex = 326;
            // 
            // lblOrigen_Banco
            // 
            this.lblOrigen_Banco.Location = new System.Drawing.Point(206, 49);
            this.lblOrigen_Banco.Name = "lblOrigen_Banco";
            this.lblOrigen_Banco.Size = new System.Drawing.Size(39, 18);
            this.lblOrigen_Banco.TabIndex = 322;
            this.lblOrigen_Banco.Text = "Banco:";
            // 
            // RadLabel15
            // 
            this.RadLabel15.Location = new System.Drawing.Point(478, 49);
            this.RadLabel15.Name = "RadLabel15";
            this.RadLabel15.Size = new System.Drawing.Size(37, 18);
            this.RadLabel15.TabIndex = 323;
            this.RadLabel15.Text = "Clabe:";
            // 
            // BancoP
            // 
            this.BancoP.Location = new System.Drawing.Point(251, 48);
            this.BancoP.Name = "BancoP";
            this.BancoP.NullText = "Banco";
            this.BancoP.ReadOnly = true;
            this.BancoP.Size = new System.Drawing.Size(215, 20);
            this.BancoP.TabIndex = 324;
            // 
            // CuentaClabeP
            // 
            this.CuentaClabeP.Location = new System.Drawing.Point(521, 48);
            this.CuentaClabeP.Name = "CuentaClabeP";
            this.CuentaClabeP.NullText = "Clabe";
            this.CuentaClabeP.ReadOnly = true;
            this.CuentaClabeP.Size = new System.Drawing.Size(141, 20);
            this.CuentaClabeP.TabIndex = 325;
            this.CuentaClabeP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.BancoT);
            this.radGroupBox3.Controls.Add(this.NumeroCuentaT);
            this.radGroupBox3.Controls.Add(this.CuentaClabeT);
            this.radGroupBox3.Controls.Add(this.radLabel4);
            this.radGroupBox3.Controls.Add(this.RadLabel6);
            this.radGroupBox3.Controls.Add(this.RadLabel5);
            this.radGroupBox3.Controls.Add(this.comboBoxBeneficiario);
            this.radGroupBox3.Controls.Add(this.radLabel2);
            this.radGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox3.HeaderText = "Datos de la cuenta destino";
            this.radGroupBox3.Location = new System.Drawing.Point(0, 185);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(676, 82);
            this.radGroupBox3.TabIndex = 27;
            this.radGroupBox3.Text = "Datos de la cuenta destino";
            // 
            // BancoT
            // 
            this.BancoT.Location = new System.Drawing.Point(251, 47);
            this.BancoT.Name = "BancoT";
            this.BancoT.NullText = "Banco";
            this.BancoT.ReadOnly = true;
            this.BancoT.Size = new System.Drawing.Size(215, 20);
            this.BancoT.TabIndex = 344;
            // 
            // NumeroCuentaT
            // 
            this.NumeroCuentaT.Location = new System.Drawing.Point(93, 47);
            this.NumeroCuentaT.Name = "NumeroCuentaT";
            this.NumeroCuentaT.ReadOnly = true;
            this.NumeroCuentaT.Size = new System.Drawing.Size(100, 20);
            this.NumeroCuentaT.TabIndex = 342;
            // 
            // CuentaClabeT
            // 
            this.CuentaClabeT.Location = new System.Drawing.Point(521, 48);
            this.CuentaClabeT.Name = "CuentaClabeT";
            this.CuentaClabeT.NullText = "Clabe";
            this.CuentaClabeT.ReadOnly = true;
            this.CuentaClabeT.Size = new System.Drawing.Size(141, 20);
            this.CuentaClabeT.TabIndex = 331;
            this.CuentaClabeT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(206, 47);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(39, 18);
            this.radLabel4.TabIndex = 328;
            this.radLabel4.Text = "Banco:";
            // 
            // RadLabel6
            // 
            this.RadLabel6.Location = new System.Drawing.Point(13, 48);
            this.RadLabel6.Name = "RadLabel6";
            this.RadLabel6.Size = new System.Drawing.Size(74, 18);
            this.RadLabel6.TabIndex = 330;
            this.RadLabel6.Text = "Núm. Cuenta:";
            // 
            // RadLabel5
            // 
            this.RadLabel5.Location = new System.Drawing.Point(478, 47);
            this.RadLabel5.Name = "RadLabel5";
            this.RadLabel5.Size = new System.Drawing.Size(37, 18);
            this.RadLabel5.TabIndex = 329;
            this.RadLabel5.Text = "Clabe:";
            // 
            // TipoCambio
            // 
            this.TipoCambio.Location = new System.Drawing.Point(253, 102);
            this.TipoCambio.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.TipoCambio.Name = "TipoCambio";
            this.TipoCambio.NullText = "Importe";
            this.TipoCambio.Size = new System.Drawing.Size(93, 20);
            this.TipoCambio.TabIndex = 347;
            this.TipoCambio.TabStop = false;
            this.TipoCambio.Text = "0";
            this.TipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel23
            // 
            this.radLabel23.Location = new System.Drawing.Point(354, 103);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(50, 18);
            this.radLabel23.TabIndex = 346;
            this.radLabel23.Text = "Moneda:";
            // 
            // Moneda
            // 
            this.Moneda.AutoFilter = true;
            this.Moneda.AutoSizeDropDownToBestFit = true;
            this.Moneda.DisplayMember = "Clave";
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn11.FieldName = "Clave";
            gridViewTextBoxColumn11.HeaderText = "Clave";
            gridViewTextBoxColumn11.Name = "Clave";
            gridViewTextBoxColumn12.FieldName = "Descripcion";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.Moneda.EditorControl.MasterTemplate.EnableFiltering = true;
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            filterDescriptor1.PropertyName = "Clave";
            filterDescriptor2.PropertyName = "Descripcion";
            this.Moneda.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1,
            filterDescriptor2});
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(410, 102);
            this.Moneda.Name = "Moneda";
            this.Moneda.NullText = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(93, 20);
            this.Moneda.TabIndex = 345;
            this.Moneda.TabStop = false;
            this.Moneda.ValueMember = "Clave";
            // 
            // lblTipoCambio
            // 
            this.lblTipoCambio.Location = new System.Drawing.Point(159, 103);
            this.lblTipoCambio.Name = "lblTipoCambio";
            this.lblTipoCambio.Size = new System.Drawing.Size(88, 18);
            this.lblTipoCambio.TabIndex = 343;
            this.lblTipoCambio.Text = "Tipo de Cambio:";
            // 
            // LabelAbono
            // 
            this.LabelAbono.Location = new System.Drawing.Point(515, 103);
            this.LabelAbono.Name = "LabelAbono";
            this.LabelAbono.Size = new System.Drawing.Size(48, 18);
            this.LabelAbono.TabIndex = 340;
            this.LabelAbono.Text = "Importe:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(93, 73);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(569, 20);
            this.Nota.TabIndex = 339;
            // 
            // Concepto
            // 
            this.Concepto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Concepto.AutoSize = false;
            this.Concepto.DropDownAnimationEnabled = false;
            this.Concepto.Location = new System.Drawing.Point(93, 21);
            this.Concepto.Name = "Concepto";
            this.Concepto.NullText = "Concepto";
            this.Concepto.ShowImageInEditorArea = false;
            this.Concepto.Size = new System.Drawing.Size(569, 20);
            this.Concepto.TabIndex = 338;
            // 
            // NumAuto
            // 
            this.NumAuto.Location = new System.Drawing.Point(352, 47);
            this.NumAuto.Name = "NumAuto";
            this.NumAuto.NullText = "Núm. Autorización";
            this.NumAuto.Size = new System.Drawing.Size(112, 20);
            this.NumAuto.TabIndex = 337;
            // 
            // Referencia
            // 
            this.Referencia.Location = new System.Drawing.Point(93, 47);
            this.Referencia.Name = "Referencia";
            this.Referencia.NullText = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(145, 20);
            this.Referencia.TabIndex = 336;
            // 
            // RadLabel21
            // 
            this.RadLabel21.Location = new System.Drawing.Point(8, 74);
            this.RadLabel21.Name = "RadLabel21";
            this.RadLabel21.Size = new System.Drawing.Size(81, 18);
            this.RadLabel21.TabIndex = 335;
            this.RadLabel21.Text = "Observaciones:";
            // 
            // RadLabel18
            // 
            this.RadLabel18.Location = new System.Drawing.Point(8, 22);
            this.RadLabel18.Name = "RadLabel18";
            this.RadLabel18.Size = new System.Drawing.Size(57, 18);
            this.RadLabel18.TabIndex = 334;
            this.RadLabel18.Text = "Concepto:";
            // 
            // RadLabel19
            // 
            this.RadLabel19.Location = new System.Drawing.Point(244, 48);
            this.RadLabel19.Name = "RadLabel19";
            this.RadLabel19.Size = new System.Drawing.Size(102, 18);
            this.RadLabel19.TabIndex = 333;
            this.RadLabel19.Text = "Núm. Autorización:";
            // 
            // RadLabel17
            // 
            this.RadLabel17.Location = new System.Drawing.Point(8, 48);
            this.RadLabel17.Name = "RadLabel17";
            this.RadLabel17.Size = new System.Drawing.Size(61, 18);
            this.RadLabel17.TabIndex = 332;
            this.RadLabel17.Text = "Referencia:";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Controls.Add(this.lblNumOperacion);
            this.radGroupBox4.Controls.Add(this.NumOperacion);
            this.radGroupBox4.Controls.Add(this.Cargo);
            this.radGroupBox4.Controls.Add(this.TipoCambio);
            this.radGroupBox4.Controls.Add(this.Referencia);
            this.radGroupBox4.Controls.Add(this.radLabel23);
            this.radGroupBox4.Controls.Add(this.RadLabel17);
            this.radGroupBox4.Controls.Add(this.Moneda);
            this.radGroupBox4.Controls.Add(this.RadLabel18);
            this.radGroupBox4.Controls.Add(this.RadLabel19);
            this.radGroupBox4.Controls.Add(this.RadLabel21);
            this.radGroupBox4.Controls.Add(this.lblTipoCambio);
            this.radGroupBox4.Controls.Add(this.NumAuto);
            this.radGroupBox4.Controls.Add(this.Concepto);
            this.radGroupBox4.Controls.Add(this.LabelAbono);
            this.radGroupBox4.Controls.Add(this.Nota);
            this.radGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox4.HeaderText = "Concepto y referencias";
            this.radGroupBox4.Location = new System.Drawing.Point(0, 267);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(676, 137);
            this.radGroupBox4.TabIndex = 28;
            this.radGroupBox4.Text = "Concepto y referencias";
            // 
            // lblNumOperacion
            // 
            this.lblNumOperacion.Location = new System.Drawing.Point(470, 48);
            this.lblNumOperacion.Name = "lblNumOperacion";
            this.lblNumOperacion.Size = new System.Drawing.Size(91, 18);
            this.lblNumOperacion.TabIndex = 349;
            this.lblNumOperacion.Text = "Núm. Operación:";
            // 
            // NumOperacion
            // 
            this.NumOperacion.Location = new System.Drawing.Point(567, 47);
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.NullText = "Núm. Operación";
            this.NumOperacion.Size = new System.Drawing.Size(97, 20);
            this.NumOperacion.TabIndex = 350;
            // 
            // Cargo
            // 
            this.Cargo.Location = new System.Drawing.Point(569, 102);
            this.Cargo.Name = "Cargo";
            this.Cargo.Size = new System.Drawing.Size(93, 20);
            this.Cargo.TabIndex = 348;
            this.Cargo.TabStop = false;
            this.Cargo.Value = "0";
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // TMovimiento
            // 
            this.TMovimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TMovimiento.Location = new System.Drawing.Point(0, 0);
            this.TMovimiento.Movimiento = null;
            this.TMovimiento.Name = "TMovimiento";
            this.TMovimiento.ShowActualizar = true;
            this.TMovimiento.ShowCancelar = false;
            this.TMovimiento.ShowCerrar = true;
            this.TMovimiento.ShowDuplicar = true;
            this.TMovimiento.ShowGuardar = true;
            this.TMovimiento.ShowHerramientas = false;
            this.TMovimiento.ShowImprimir = true;
            this.TMovimiento.ShowNuevo = true;
            this.TMovimiento.Size = new System.Drawing.Size(676, 30);
            this.TMovimiento.TabIndex = 29;
            this.TMovimiento.BindingCompleted += new System.EventHandler<System.EventArgs>(this.ToolBar_BindingCompleted);
            // 
            // Preparar
            // 
            this.Preparar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Preparar_DoWork);
            this.Preparar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Preparar_RunWorkerCompleted);
            // 
            // TransferenciaCuentasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 404);
            this.Controls.Add(this.radGroupBox4);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.Group1);
            this.Controls.Add(this.TMovimiento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TransferenciaCuentasForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Transferencia entre Cuentas";
            this.Load += new System.EventHandler(this.TransferenciaCuentasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCuentaOrigen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuenta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1)).EndInit();
            this.Group1.ResumeLayout(false);
            this.Group1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormaPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumDocto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaAplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_Origen_NumCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrigen_Banco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BancoT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumeroCuentaT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CuentaClabeT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoCambio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelAbono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Concepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAuto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumOperacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cargo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxCuentaOrigen;
        private Telerik.WinControls.UI.RadLabel lblCuenta1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadMultiColumnComboBox comboBoxBeneficiario;
        private Telerik.WinControls.UI.RadGroupBox Group1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        internal Telerik.WinControls.UI.RadLabel radLabel3;
        internal Telerik.WinControls.UI.RadLabel RadLabel7;
        internal Telerik.WinControls.UI.RadLabel RadLabel8;
        internal Telerik.WinControls.UI.RadLabel RadLabel9;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox FormaPago;
        internal Telerik.WinControls.UI.RadTextBox NumDocto;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaDocumento;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaAplicacion;
        internal Telerik.WinControls.UI.RadLabel lbl_Origen_NumCuenta;
        private Telerik.WinControls.UI.RadTextBox NumeroCuentaP;
        internal Telerik.WinControls.UI.RadLabel lblOrigen_Banco;
        internal Telerik.WinControls.UI.RadLabel RadLabel15;
        internal Telerik.WinControls.UI.RadTextBox BancoP;
        internal Telerik.WinControls.UI.RadTextBox CuentaClabeP;
        internal Telerik.WinControls.UI.RadTextBox BancoT;
        private Telerik.WinControls.UI.RadTextBox NumeroCuentaT;
        internal Telerik.WinControls.UI.RadTextBox CuentaClabeT;
        internal Telerik.WinControls.UI.RadLabel radLabel4;
        internal Telerik.WinControls.UI.RadLabel RadLabel6;
        internal Telerik.WinControls.UI.RadLabel RadLabel5;
        internal Telerik.WinControls.UI.RadMaskedEditBox TipoCambio;
        internal Telerik.WinControls.UI.RadLabel radLabel23;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        internal Telerik.WinControls.UI.RadLabel lblTipoCambio;
        internal Telerik.WinControls.UI.RadLabel LabelAbono;
        internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadDropDownList Concepto;
        internal Telerik.WinControls.UI.RadTextBox NumAuto;
        internal Telerik.WinControls.UI.RadTextBox Referencia;
        internal Telerik.WinControls.UI.RadLabel RadLabel21;
        internal Telerik.WinControls.UI.RadLabel RadLabel18;
        internal Telerik.WinControls.UI.RadLabel RadLabel19;
        internal Telerik.WinControls.UI.RadLabel RadLabel17;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadCalculatorDropDown Cargo;
        private System.Windows.Forms.ErrorProvider Advertencia;
        internal Telerik.WinControls.UI.RadLabel lblNumOperacion;
        internal Telerik.WinControls.UI.RadTextBox NumOperacion;
        private TbMovimientoBancarioControl TMovimiento;
        private System.ComponentModel.BackgroundWorker Preparar;
        internal Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
    }
}
