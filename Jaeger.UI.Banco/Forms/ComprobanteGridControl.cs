﻿using Telerik.WinControls.UI;
using Jaeger.UI.Banco.Builder;

namespace Jaeger.UI.Banco.Forms {
    public class ComprobanteGridControl : RadGridView {
        protected internal Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        protected internal IComprobanteGridViewBuilder _Templete;
        public ComprobanteGridControl() : base() {
            this._Templete = new ComprobanteGridViewBuilder();
            this.MasterTemplate.EnableGrouping = false;
            this.MasterTemplate.AutoGenerateColumns = false;
            this.MasterTemplate.AllowAddNewRow = false;
            this.MasterTemplate.AllowEditRow = false;
            this.MasterTemplate.AllowDeleteRow = false;
            this.MasterTemplate.EnableFiltering = true;
            this.MasterTemplate.ShowFilteringRow = false;
            this.MasterTemplate.EnableAlternatingRowColor = true;
            this.MasterTemplate.AllowRowResize = false;
            this.MasterTemplate.ShowRowHeaderColumn = false;
            this.subTipo = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
        }

        public Domain.Base.ValueObjects.CFDISubTipoEnum SubTipo {
            get { return this.subTipo; }
            set {
                this.subTipo = value;
                this.CreateView();
            }
        }

        public virtual void CreateView() {
            if (this.subTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido | this.subTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Nomina) {
                this.MasterTemplate.Columns.Clear();
                this.MasterTemplate.Columns.AddRange(this._Templete.Templetes().Emitidos().Build());
            } else {
                this.MasterTemplate.Columns.Clear();
                this.MasterTemplate.Columns.AddRange(this._Templete.Templetes().Recibidos().Build());
            }
        }
    }
}
