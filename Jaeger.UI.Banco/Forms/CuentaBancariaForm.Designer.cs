﻿namespace Jaeger.UI.Banco.Forms
{
    partial class CuentaBancariaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuentaBancariaForm));
            this.lblNumCuenta = new Telerik.WinControls.UI.RadLabel();
            this.NumCuenta = new Telerik.WinControls.UI.RadTextBox();
            this.lblCLABE = new Telerik.WinControls.UI.RadLabel();
            this.Clabe = new Telerik.WinControls.UI.RadTextBox();
            this.lblCuentaContable = new Telerik.WinControls.UI.RadLabel();
            this.CtaContable = new Telerik.WinControls.UI.RadTextBox();
            this.lblDiaCorte = new Telerik.WinControls.UI.RadLabel();
            this.lblBancoRFC = new Telerik.WinControls.UI.RadLabel();
            this.BancoRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblNumeroCliente = new Telerik.WinControls.UI.RadLabel();
            this.NumCliente = new Telerik.WinControls.UI.RadTextBox();
            this.Moneda = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ClaveBanco = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblBancoSucursal = new Telerik.WinControls.UI.RadLabel();
            this.Sucursal = new Telerik.WinControls.UI.RadTextBox();
            this.Telefono = new Telerik.WinControls.UI.RadTextBox();
            this.lblTelefono = new Telerik.WinControls.UI.RadLabel();
            this.Alias = new Telerik.WinControls.UI.RadTextBox();
            this.lblAlias = new Telerik.WinControls.UI.RadLabel();
            this.SaldoInicial = new Telerik.WinControls.UI.RadTextBox();
            this.lblSaldoInicial = new Telerik.WinControls.UI.RadLabel();
            this.lblMoneda = new Telerik.WinControls.UI.RadLabel();
            this.lblBanco = new Telerik.WinControls.UI.RadLabel();
            this.NoCheque = new Telerik.WinControls.UI.RadSpinEditor();
            this.DiaCorte = new Telerik.WinControls.UI.RadSpinEditor();
            this.lblNumeroCheque = new Telerik.WinControls.UI.RadLabel();
            this.BeneficiarioRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.Activo = new Telerik.WinControls.UI.RadCheckBox();
            this.Beneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.FechaApertura = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblBeneficiario = new Telerik.WinControls.UI.RadLabel();
            this.lblFechaApertura = new Telerik.WinControls.UI.RadLabel();
            this.BancoExtranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.Funcionario = new Telerik.WinControls.UI.RadTextBox();
            this.lblFuncionario = new Telerik.WinControls.UI.RadLabel();
            this.errorCuentaBancaria = new System.Windows.Forms.ErrorProvider(this.components);
            this.TCuenta = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.grpCuenta = new Telerik.WinControls.UI.RadGroupBox();
            this.grpBancos = new Telerik.WinControls.UI.RadGroupBox();
            this.CheckBanco = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCLABE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clabe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuentaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaContable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiaCorte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoSucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sucursal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Alias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaldoInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaldoInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiaCorte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCheque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaApertura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaApertura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoExtranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Funcionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuncionario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorCuentaBancaria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpCuenta)).BeginInit();
            this.grpCuenta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpBancos)).BeginInit();
            this.grpBancos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumCuenta
            // 
            this.lblNumCuenta.Location = new System.Drawing.Point(12, 75);
            this.lblNumCuenta.Name = "lblNumCuenta";
            this.lblNumCuenta.Size = new System.Drawing.Size(74, 18);
            this.lblNumCuenta.TabIndex = 1;
            this.lblNumCuenta.Text = "Núm. Cuenta:";
            // 
            // NumCuenta
            // 
            this.NumCuenta.Location = new System.Drawing.Point(110, 74);
            this.NumCuenta.MaxLength = 20;
            this.NumCuenta.Name = "NumCuenta";
            this.NumCuenta.Size = new System.Drawing.Size(149, 20);
            this.NumCuenta.TabIndex = 3;
            // 
            // lblCLABE
            // 
            this.lblCLABE.Location = new System.Drawing.Point(12, 101);
            this.lblCLABE.Name = "lblCLABE";
            this.lblCLABE.Size = new System.Drawing.Size(41, 18);
            this.lblCLABE.TabIndex = 1;
            this.lblCLABE.Text = "CLABE:";
            // 
            // Clabe
            // 
            this.Clabe.Location = new System.Drawing.Point(110, 100);
            this.Clabe.MaxLength = 18;
            this.Clabe.Name = "Clabe";
            this.Clabe.Size = new System.Drawing.Size(149, 20);
            this.Clabe.TabIndex = 5;
            this.Clabe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbFolio_KeyPress);
            // 
            // lblCuentaContable
            // 
            this.lblCuentaContable.Location = new System.Drawing.Point(12, 127);
            this.lblCuentaContable.Name = "lblCuentaContable";
            this.lblCuentaContable.Size = new System.Drawing.Size(92, 18);
            this.lblCuentaContable.TabIndex = 1;
            this.lblCuentaContable.Text = "Cuenta Contable:";
            // 
            // CtaContable
            // 
            this.CtaContable.Location = new System.Drawing.Point(110, 126);
            this.CtaContable.MaxLength = 40;
            this.CtaContable.Name = "CtaContable";
            this.CtaContable.Size = new System.Drawing.Size(149, 20);
            this.CtaContable.TabIndex = 7;
            // 
            // lblDiaCorte
            // 
            this.lblDiaCorte.Location = new System.Drawing.Point(270, 126);
            this.lblDiaCorte.Name = "lblDiaCorte";
            this.lblDiaCorte.Size = new System.Drawing.Size(71, 18);
            this.lblDiaCorte.TabIndex = 1;
            this.lblDiaCorte.Text = "Día de Corte:";
            // 
            // lblBancoRFC
            // 
            this.lblBancoRFC.Location = new System.Drawing.Point(12, 47);
            this.lblBancoRFC.Name = "lblBancoRFC";
            this.lblBancoRFC.Size = new System.Drawing.Size(28, 18);
            this.lblBancoRFC.TabIndex = 1;
            this.lblBancoRFC.Text = "RFC:";
            // 
            // BancoRFC
            // 
            this.BancoRFC.Location = new System.Drawing.Point(57, 46);
            this.BancoRFC.MaxLength = 15;
            this.BancoRFC.Name = "BancoRFC";
            this.BancoRFC.NullText = "RFC";
            this.BancoRFC.Size = new System.Drawing.Size(98, 20);
            this.BancoRFC.TabIndex = 2;
            // 
            // lblNumeroCliente
            // 
            this.lblNumeroCliente.Location = new System.Drawing.Point(12, 153);
            this.lblNumeroCliente.Name = "lblNumeroCliente";
            this.lblNumeroCliente.Size = new System.Drawing.Size(73, 18);
            this.lblNumeroCliente.TabIndex = 1;
            this.lblNumeroCliente.Text = "Núm. Cliente:";
            // 
            // NumCliente
            // 
            this.NumCliente.Location = new System.Drawing.Point(110, 152);
            this.NumCliente.MaxLength = 14;
            this.NumCliente.Name = "NumCliente";
            this.NumCliente.Size = new System.Drawing.Size(149, 20);
            this.NumCliente.TabIndex = 9;
            this.NumCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbFolio_KeyPress);
            // 
            // Moneda
            // 
            this.Moneda.AutoSizeDropDownHeight = true;
            this.Moneda.AutoSizeDropDownToBestFit = true;
            this.Moneda.DisplayMember = "Clave";
            // 
            // Moneda.NestedRadGridView
            // 
            this.Moneda.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Moneda.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Moneda.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Moneda.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Moneda.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Moneda.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Moneda.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Moneda.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Moneda.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Moneda.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Moneda.EditorControl.Name = "NestedRadGridView";
            this.Moneda.EditorControl.ReadOnly = true;
            this.Moneda.EditorControl.ShowGroupPanel = false;
            this.Moneda.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Moneda.EditorControl.TabIndex = 0;
            this.Moneda.Location = new System.Drawing.Point(360, 151);
            this.Moneda.Name = "Moneda";
            this.Moneda.Size = new System.Drawing.Size(92, 20);
            this.Moneda.TabIndex = 10;
            this.Moneda.TabStop = false;
            this.Moneda.ValueMember = "Clave";
            // 
            // ClaveBanco
            // 
            this.ClaveBanco.AutoSizeDropDownToBestFit = true;
            this.ClaveBanco.DisplayMember = "Descripcion";
            this.ClaveBanco.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClaveBanco.NestedRadGridView
            // 
            this.ClaveBanco.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClaveBanco.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaveBanco.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClaveBanco.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClaveBanco.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClaveBanco.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 200;
            this.ClaveBanco.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.ClaveBanco.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClaveBanco.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.ClaveBanco.EditorControl.Name = "NestedRadGridView";
            this.ClaveBanco.EditorControl.ReadOnly = true;
            this.ClaveBanco.EditorControl.ShowGroupPanel = false;
            this.ClaveBanco.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClaveBanco.EditorControl.TabIndex = 0;
            this.ClaveBanco.Location = new System.Drawing.Point(57, 20);
            this.ClaveBanco.Name = "ClaveBanco";
            this.ClaveBanco.NullText = "Selecciona";
            this.ClaveBanco.Size = new System.Drawing.Size(263, 20);
            this.ClaveBanco.TabIndex = 3;
            this.ClaveBanco.TabStop = false;
            this.ClaveBanco.ValueMember = "Clave";
            // 
            // lblBancoSucursal
            // 
            this.lblBancoSucursal.Location = new System.Drawing.Point(177, 47);
            this.lblBancoSucursal.Name = "lblBancoSucursal";
            this.lblBancoSucursal.Size = new System.Drawing.Size(50, 18);
            this.lblBancoSucursal.TabIndex = 1;
            this.lblBancoSucursal.Text = "Sucursal:";
            // 
            // Sucursal
            // 
            this.Sucursal.Location = new System.Drawing.Point(233, 46);
            this.Sucursal.MaxLength = 20;
            this.Sucursal.Name = "Sucursal";
            this.Sucursal.Size = new System.Drawing.Size(87, 20);
            this.Sucursal.TabIndex = 2;
            // 
            // Telefono
            // 
            this.Telefono.Location = new System.Drawing.Point(70, 71);
            this.Telefono.MaxLength = 20;
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(85, 20);
            this.Telefono.TabIndex = 6;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Location = new System.Drawing.Point(12, 72);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 18);
            this.lblTelefono.TabIndex = 5;
            this.lblTelefono.Text = "Teléfono:";
            // 
            // Alias
            // 
            this.Alias.Location = new System.Drawing.Point(110, 48);
            this.Alias.MaxLength = 20;
            this.Alias.Name = "Alias";
            this.Alias.Size = new System.Drawing.Size(149, 20);
            this.Alias.TabIndex = 1;
            // 
            // lblAlias
            // 
            this.lblAlias.Location = new System.Drawing.Point(12, 49);
            this.lblAlias.Name = "lblAlias";
            this.lblAlias.Size = new System.Drawing.Size(32, 18);
            this.lblAlias.TabIndex = 7;
            this.lblAlias.Text = "Alias:";
            // 
            // SaldoInicial
            // 
            this.SaldoInicial.Location = new System.Drawing.Point(696, 21);
            this.SaldoInicial.Name = "SaldoInicial";
            this.SaldoInicial.Size = new System.Drawing.Size(100, 20);
            this.SaldoInicial.TabIndex = 10;
            // 
            // lblSaldoInicial
            // 
            this.lblSaldoInicial.Location = new System.Drawing.Point(616, 22);
            this.lblSaldoInicial.Name = "lblSaldoInicial";
            this.lblSaldoInicial.Size = new System.Drawing.Size(65, 18);
            this.lblSaldoInicial.TabIndex = 9;
            this.lblSaldoInicial.Text = "Saldo inicia:";
            // 
            // lblMoneda
            // 
            this.lblMoneda.Location = new System.Drawing.Point(270, 152);
            this.lblMoneda.Name = "lblMoneda";
            this.lblMoneda.Size = new System.Drawing.Size(50, 18);
            this.lblMoneda.TabIndex = 9;
            this.lblMoneda.Text = "Moneda:";
            // 
            // lblBanco
            // 
            this.lblBanco.Location = new System.Drawing.Point(12, 21);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.Size = new System.Drawing.Size(39, 18);
            this.lblBanco.TabIndex = 9;
            this.lblBanco.Text = "Banco:";
            // 
            // NoCheque
            // 
            this.NoCheque.Location = new System.Drawing.Point(360, 99);
            this.NoCheque.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.NoCheque.Name = "NoCheque";
            this.NoCheque.Size = new System.Drawing.Size(92, 20);
            this.NoCheque.TabIndex = 6;
            this.NoCheque.TabStop = false;
            this.NoCheque.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DiaCorte
            // 
            this.DiaCorte.Location = new System.Drawing.Point(360, 125);
            this.DiaCorte.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.DiaCorte.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DiaCorte.Name = "DiaCorte";
            this.DiaCorte.NullableValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DiaCorte.Size = new System.Drawing.Size(92, 20);
            this.DiaCorte.TabIndex = 8;
            this.DiaCorte.TabStop = false;
            this.DiaCorte.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.DiaCorte.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNumeroCheque
            // 
            this.lblNumeroCheque.Location = new System.Drawing.Point(270, 100);
            this.lblNumeroCheque.Name = "lblNumeroCheque";
            this.lblNumeroCheque.Size = new System.Drawing.Size(77, 18);
            this.lblNumeroCheque.TabIndex = 1;
            this.lblNumeroCheque.Text = "Núm. Cheque:";
            // 
            // BeneficiarioRFC
            // 
            this.BeneficiarioRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.BeneficiarioRFC.Location = new System.Drawing.Point(340, 48);
            this.BeneficiarioRFC.MaxLength = 14;
            this.BeneficiarioRFC.Name = "BeneficiarioRFC";
            this.BeneficiarioRFC.NullText = "RFC";
            this.BeneficiarioRFC.ShowClearButton = true;
            this.BeneficiarioRFC.Size = new System.Drawing.Size(112, 20);
            this.BeneficiarioRFC.TabIndex = 2;
            this.BeneficiarioRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(270, 49);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 9;
            this.lblRFC.Text = "RFC:";
            // 
            // Activo
            // 
            this.Activo.Location = new System.Drawing.Point(360, 179);
            this.Activo.Name = "Activo";
            this.Activo.Size = new System.Drawing.Size(51, 18);
            this.Activo.TabIndex = 11;
            this.Activo.Text = "Activo";
            // 
            // Beneficiario
            // 
            this.Beneficiario.Location = new System.Drawing.Point(110, 22);
            this.Beneficiario.MaxLength = 256;
            this.Beneficiario.Name = "Beneficiario";
            this.Beneficiario.Size = new System.Drawing.Size(342, 20);
            this.Beneficiario.TabIndex = 0;
            // 
            // FechaApertura
            // 
            this.FechaApertura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaApertura.Location = new System.Drawing.Point(360, 73);
            this.FechaApertura.Name = "FechaApertura";
            this.FechaApertura.Size = new System.Drawing.Size(92, 20);
            this.FechaApertura.TabIndex = 4;
            this.FechaApertura.TabStop = false;
            this.FechaApertura.Text = "09/12/2020";
            this.FechaApertura.Value = new System.DateTime(2020, 12, 9, 15, 3, 16, 921);
            // 
            // lblBeneficiario
            // 
            this.lblBeneficiario.Location = new System.Drawing.Point(12, 23);
            this.lblBeneficiario.Name = "lblBeneficiario";
            this.lblBeneficiario.Size = new System.Drawing.Size(67, 18);
            this.lblBeneficiario.TabIndex = 9;
            this.lblBeneficiario.Text = "Beneficiario:";
            // 
            // lblFechaApertura
            // 
            this.lblFechaApertura.Location = new System.Drawing.Point(270, 74);
            this.lblFechaApertura.Name = "lblFechaApertura";
            this.lblFechaApertura.Size = new System.Drawing.Size(84, 18);
            this.lblFechaApertura.TabIndex = 1;
            this.lblFechaApertura.Text = "Fecha Apertura:";
            // 
            // BancoExtranjero
            // 
            this.BancoExtranjero.Location = new System.Drawing.Point(345, 47);
            this.BancoExtranjero.Name = "BancoExtranjero";
            this.BancoExtranjero.Size = new System.Drawing.Size(104, 18);
            this.BancoExtranjero.TabIndex = 15;
            this.BancoExtranjero.Text = "Banco Extranjero";
            // 
            // Funcionario
            // 
            this.Funcionario.Location = new System.Drawing.Point(234, 71);
            this.Funcionario.MaxLength = 20;
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.Size = new System.Drawing.Size(218, 20);
            this.Funcionario.TabIndex = 10;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.Location = new System.Drawing.Point(161, 72);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(67, 18);
            this.lblFuncionario.TabIndex = 9;
            this.lblFuncionario.Text = "Funcionario:";
            // 
            // errorCuentaBancaria
            // 
            this.errorCuentaBancaria.ContainerControl = this;
            // 
            // TCuenta
            // 
            this.TCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCuenta.Etiqueta = "";
            this.TCuenta.Location = new System.Drawing.Point(0, 0);
            this.TCuenta.Name = "TCuenta";
            this.TCuenta.ReadOnly = false;
            this.TCuenta.ShowActualizar = true;
            this.TCuenta.ShowAutorizar = false;
            this.TCuenta.ShowCerrar = true;
            this.TCuenta.ShowEditar = false;
            this.TCuenta.ShowExportarExcel = false;
            this.TCuenta.ShowFiltro = false;
            this.TCuenta.ShowGuardar = true;
            this.TCuenta.ShowHerramientas = false;
            this.TCuenta.ShowImagen = false;
            this.TCuenta.ShowImprimir = false;
            this.TCuenta.ShowNuevo = true;
            this.TCuenta.ShowRemover = false;
            this.TCuenta.Size = new System.Drawing.Size(470, 30);
            this.TCuenta.TabIndex = 11;
            // 
            // grpCuenta
            // 
            this.grpCuenta.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpCuenta.Controls.Add(this.lblBeneficiario);
            this.grpCuenta.Controls.Add(this.lblMoneda);
            this.grpCuenta.Controls.Add(this.BeneficiarioRFC);
            this.grpCuenta.Controls.Add(this.NoCheque);
            this.grpCuenta.Controls.Add(this.lblFechaApertura);
            this.grpCuenta.Controls.Add(this.lblNumCuenta);
            this.grpCuenta.Controls.Add(this.lblNumeroCheque);
            this.grpCuenta.Controls.Add(this.lblRFC);
            this.grpCuenta.Controls.Add(this.NumCuenta);
            this.grpCuenta.Controls.Add(this.SaldoInicial);
            this.grpCuenta.Controls.Add(this.lblDiaCorte);
            this.grpCuenta.Controls.Add(this.lblAlias);
            this.grpCuenta.Controls.Add(this.lblSaldoInicial);
            this.grpCuenta.Controls.Add(this.Clabe);
            this.grpCuenta.Controls.Add(this.Activo);
            this.grpCuenta.Controls.Add(this.DiaCorte);
            this.grpCuenta.Controls.Add(this.lblCLABE);
            this.grpCuenta.Controls.Add(this.lblNumeroCliente);
            this.grpCuenta.Controls.Add(this.Moneda);
            this.grpCuenta.Controls.Add(this.NumCliente);
            this.grpCuenta.Controls.Add(this.CtaContable);
            this.grpCuenta.Controls.Add(this.Alias);
            this.grpCuenta.Controls.Add(this.lblCuentaContable);
            this.grpCuenta.Controls.Add(this.FechaApertura);
            this.grpCuenta.Controls.Add(this.Beneficiario);
            this.grpCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpCuenta.HeaderText = "Datos de la cuenta";
            this.grpCuenta.Location = new System.Drawing.Point(0, 30);
            this.grpCuenta.Name = "grpCuenta";
            this.grpCuenta.Size = new System.Drawing.Size(470, 205);
            this.grpCuenta.TabIndex = 16;
            this.grpCuenta.Text = "Datos de la cuenta";
            // 
            // grpBancos
            // 
            this.grpBancos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpBancos.Controls.Add(this.CheckBanco);
            this.grpBancos.Controls.Add(this.lblBanco);
            this.grpBancos.Controls.Add(this.lblTelefono);
            this.grpBancos.Controls.Add(this.BancoExtranjero);
            this.grpBancos.Controls.Add(this.Telefono);
            this.grpBancos.Controls.Add(this.ClaveBanco);
            this.grpBancos.Controls.Add(this.lblBancoSucursal);
            this.grpBancos.Controls.Add(this.Sucursal);
            this.grpBancos.Controls.Add(this.Funcionario);
            this.grpBancos.Controls.Add(this.lblBancoRFC);
            this.grpBancos.Controls.Add(this.BancoRFC);
            this.grpBancos.Controls.Add(this.lblFuncionario);
            this.grpBancos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpBancos.HeaderText = "Datos del Banco";
            this.grpBancos.Location = new System.Drawing.Point(0, 235);
            this.grpBancos.Name = "grpBancos";
            this.grpBancos.Size = new System.Drawing.Size(470, 101);
            this.grpBancos.TabIndex = 17;
            this.grpBancos.Text = "Datos del Banco";
            // 
            // CheckBanco
            // 
            this.CheckBanco.Location = new System.Drawing.Point(345, 23);
            this.CheckBanco.Name = "CheckBanco";
            this.CheckBanco.Size = new System.Drawing.Size(51, 18);
            this.CheckBanco.TabIndex = 16;
            this.CheckBanco.Text = "Incluir";
            // 
            // CuentaBancariaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 336);
            this.Controls.Add(this.grpBancos);
            this.Controls.Add(this.grpCuenta);
            this.Controls.Add(this.TCuenta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CuentaBancariaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cuenta Bancaria";
            this.Load += new System.EventHandler(this.CuentaBancariaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblNumCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCLABE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clabe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCuentaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CtaContable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiaCorte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Moneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBancoSucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sucursal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Telefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Alias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaldoInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaldoInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMoneda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiaCorte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumeroCheque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeneficiarioRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Activo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Beneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaApertura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaApertura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BancoExtranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Funcionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuncionario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorCuentaBancaria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpCuenta)).EndInit();
            this.grpCuenta.ResumeLayout(false);
            this.grpCuenta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpBancos)).EndInit();
            this.grpBancos.ResumeLayout(false);
            this.grpBancos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblNumCuenta;
        private Telerik.WinControls.UI.RadTextBox NumCuenta;
        private Telerik.WinControls.UI.RadTextBox Clabe;
        private Telerik.WinControls.UI.RadLabel lblCLABE;
        private Telerik.WinControls.UI.RadLabel lblCuentaContable;
        private Telerik.WinControls.UI.RadTextBox CtaContable;
        private Telerik.WinControls.UI.RadLabel lblDiaCorte;
        private Telerik.WinControls.UI.RadLabel lblBancoRFC;
        private Telerik.WinControls.UI.RadTextBox BancoRFC;
        private Telerik.WinControls.UI.RadLabel lblNumeroCliente;
        private Telerik.WinControls.UI.RadTextBox NumCliente;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Moneda;
        private Telerik.WinControls.UI.RadMultiColumnComboBox ClaveBanco;
        private Telerik.WinControls.UI.RadLabel lblBancoSucursal;
        private Telerik.WinControls.UI.RadTextBox Sucursal;
        private Telerik.WinControls.UI.RadTextBox Telefono;
        private Telerik.WinControls.UI.RadLabel lblTelefono;
        private Telerik.WinControls.UI.RadTextBox Alias;
        private Telerik.WinControls.UI.RadLabel lblAlias;
        private Telerik.WinControls.UI.RadTextBox SaldoInicial;
        private Telerik.WinControls.UI.RadLabel lblSaldoInicial;
        private Telerik.WinControls.UI.RadLabel lblMoneda;
        private Telerik.WinControls.UI.RadLabel lblBanco;
        private Telerik.WinControls.UI.RadSpinEditor NoCheque;
        private Telerik.WinControls.UI.RadSpinEditor DiaCorte;
        private Telerik.WinControls.UI.RadLabel lblNumeroCheque;
        private Telerik.WinControls.UI.RadLabel lblFuncionario;
        private Telerik.WinControls.UI.RadTextBox Funcionario;
        private Telerik.WinControls.UI.RadCheckBox Activo;
        private Telerik.WinControls.UI.RadTextBox Beneficiario;
        private Telerik.WinControls.UI.RadDateTimePicker FechaApertura;
        private Telerik.WinControls.UI.RadLabel lblBeneficiario;
        private Telerik.WinControls.UI.RadLabel lblFechaApertura;
        private Telerik.WinControls.UI.RadCheckBox BancoExtranjero;
        private System.Windows.Forms.ErrorProvider errorCuentaBancaria;
        private Telerik.WinControls.UI.RadTextBox BeneficiarioRFC;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        private Common.Forms.ToolBarStandarControl TCuenta;
        private Telerik.WinControls.UI.RadGroupBox grpBancos;
        private Telerik.WinControls.UI.RadGroupBox grpCuenta;
        private Telerik.WinControls.UI.RadCheckBox CheckBanco;
    }
}
