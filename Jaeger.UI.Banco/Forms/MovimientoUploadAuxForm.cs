﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Banco.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.UI.Banco.Forms {
    public partial class MovimientoUploadAuxForm : RadForm {
        #region declaraciones
        protected internal List<MovimientoBancarioAuxiliarDetailModel> _DataSource;
        protected internal List<string> _Ignorar = new List<string>();
        protected internal IBancoService _Service;
        protected internal string _Pattern = @"[EID][0-9]{8}";
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="service">service</param>
        public MovimientoUploadAuxForm(IBancoService service) {
            InitializeComponent();
            this._Service = service;
        }

        private void MovimientoUploadAuxForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            using (IAuxiliarGridBuilder view = new AuxiliarGridBuilder()) {
                this.GAuxiliar.Standard();
                this.GAuxiliar.AllowEditRow = true;
                this.GAuxiliar.Columns.AddRange(view.Templetes().Importar().Build());
            }
            this.TAuxiliar.Remover.Enabled = false;
            this.TAuxiliar.Nuevo.Text = "Agregar";
            this.TAuxiliar.Actualizar.Text = "Buscar";
            this.TAuxiliar.Nuevo.Click += TAuxiliar_Agregar_Click;
            this.TAuxiliar.Guardar.Click += TAuxiliar_Guardar_Click;
            this.TAuxiliar.Remover.Click += TAuxiliar_Remover_Click;
            this.TAuxiliar.Actualizar.Click += TAuxliar_Actualizar_Click;
            this.TAuxiliar.Cerrar.Click += TAuxiliar_Cerrar_Click;
            this.GAuxiliar.RowsChanged += GridAuxiliares_RowsChanged;
            this.GAuxiliar.DataSource = this._DataSource;
        }

        #region barra de herramientas
        private void TAuxiliar_Agregar_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.*|*.*|*.xml|*.XML|*.xls|*.XLS|*.xlsx|*.XLSX|*.pdf|*.PDF", CheckFileExists = true };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var info = new System.IO.FileInfo(openFile.FileName);
                if (info.Length > 5000000) {
                    MessageBox.Show("Nop muy grande!");
                } else {
                    var d1 = this.Agregar(openFile.FileName);
                    if (d1 != null) {
                        this.Agregar(d1);
                        this.GAuxiliar.DataSource = this._DataSource;
                    }
                }
            }
        }

        private void TAuxiliar_Guardar_Click(object sender, EventArgs e) {
            var d0 = this._DataSource.Where(it => it.IdMovimiento <= 0).Count();
            if (d0 > 0) {
                RadMessageBox.Show(this, "Existen archivos que no tienen coincidencia con ningún movimiento, estos serán ignorados.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
            using (var espera = new UI.Common.Forms.Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TAuxiliar_Remover_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                var seleccionado = this.GAuxiliar.ReturnRowSelected() as MovimientoBancarioAuxiliarDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdAuxiliar == 0) {
                        this.GAuxiliar.Rows.Remove(this.GAuxiliar.CurrentRow);
                    } else {
                        RadMessageBox.Show(Properties.Resources.msg_Banco_Comprobante_Remover_Error, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
        }

        private void TAuxliar_Actualizar_Click(object sender, EventArgs e) {
            var folder = new FolderBrowserDialog() { Description = "Selecciona una carpeta" };
            if (folder.ShowDialog(this) != DialogResult.OK)
                return;

            if (System.IO.Directory.Exists(folder.SelectedPath) == false) {
                MessageBox.Show(this, Properties.Resources.msg_RutaNoValida, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            this._DataSource = new List<MovimientoBancarioAuxiliarDetailModel>();
            this._Ignorar = new List<string>();
            var files = System.IO.Directory.GetFiles(folder.SelectedPath);

            foreach (var openFile in files) {
                if (Regex.Match(openFile, _Pattern, RegexOptions.Singleline).Success) {
                    Console.WriteLine(openFile);
                    var info = new System.IO.FileInfo(openFile);
                    if (info.Length > 5000000) {
                        MessageBox.Show("Nop muy grande!");
                        this.Status.Text = openFile + "(Archivo muy grande.)";
                    } else {
                        this.Agregar(this.Agregar(openFile));
                    }
                } else {
                    this._Ignorar.Add(openFile);
                }
            }
            using (var espera = new Common.Forms.Waiting2Form(this.Aguanta)) {
                espera.Text = "Buscando ...";
                espera.ShowDialog(this);
            }
            this.GAuxiliar.DataSource = this._DataSource;
            if (this._Ignorar.Count > 0) {
                MessageBox.Show(this, "Los siguientes archivos fueron ignorados: \r\n" + string.Join("\r\n", this._Ignorar), "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TAuxiliar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        private void GridAuxiliares_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            this.Status.Text = this.GAuxiliar.Rows.Count().ToString() + " Archivo(s) encontrados.";
            this.TAuxiliar.Remover.Enabled = this.GAuxiliar.Rows.Count() > 0;
        }
        #endregion

        #region metodos privados
        private void Aguanta() {
            var c0 = this._Service.GetList<MovimientoBancarioModel>(BancoService.Query().Identificador(this._DataSource.Select(it => it.Identificador).ToList()).Build());
            this._DataSource = this._DataSource.Select(it => { it.IdMovimiento = c0.Where(b => b.Identificador == it.Identificador).Select(b => b.Id).FirstOrDefault(); return it; }).ToList();
        }

        private void Guardar() {
            for (int i = 0; i < this._DataSource.Count; i++) {
                if (this._DataSource[i].IdMovimiento > 0) {
                    this._DataSource[i] = this._Service.Save(this._DataSource[i]) as MovimientoBancarioAuxiliarDetailModel;
                }
                this.Status.Text = this._DataSource[i].Descripcion;
            }
        }

        private MovimientoBancarioAuxiliarDetailModel Agregar(string openFile) {
            if (System.IO.File.Exists(openFile)) {
                // crear registro para el axuliar
                var auxiliar = new MovimientoBancarioAuxiliarDetailModel {
                    Descripcion = System.IO.Path.GetFileName(openFile),
                    FileName = System.IO.Path.GetFileName(openFile),
                    Base64 = Util.FileService.ReadFileB64(openFile),
                    Tag = openFile,
                    Identificador = Regex.Matches(openFile, _Pattern, RegexOptions.Singleline)[0].Value
                };
                return auxiliar;
            }
            return null;
        }

        private void Agregar(MovimientoBancarioAuxiliarDetailModel auxiliar) {
            if (this._DataSource == null) {
                this._DataSource = new List<MovimientoBancarioAuxiliarDetailModel>();
            }

            try {
                var exists = this._DataSource.Where(it => it.Identificador == auxiliar.Identificador).FirstOrDefault();
                if (exists == null) {
                    this._DataSource.Add(auxiliar);
                }
            } catch (Exception ex) {
                this.Status.Text = ex.Message;
            }
        }
        #endregion
    }
}
