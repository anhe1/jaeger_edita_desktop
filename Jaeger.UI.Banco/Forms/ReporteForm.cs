﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.QRCode.Helpers;
using Jaeger.Util;

namespace Jaeger.UI.Banco.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Banco");
        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(MovimientoBancarioPrinter)) {
                this.CrearTransferencia();
            } else if (this.CurrentObject.GetType() == typeof(EstadoCuentaPrinter)) {
                this.CrearEstadoCuenta();
            } else if (this.CurrentObject.GetType() == typeof(MovimientosReportePrinter)) {
                this.CrearReporte1();
            }
        }

        private void CrearTransferencia() {
            var current = (MovimientoBancarioPrinter)this.CurrentObject;
            //var d1 = MovimientoBancarioDetailModel.Json(current.InfoAuxiliar);
            //current.CuentaPropia = d1.CuentaPropia;
            if (current.TipoOperacion == BancoTipoOperacionEnum.TransferenciaEntreCuentas) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancosTransferenciaCuentasReporte30.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancosMovimientoReporte31.rdlc");
                var d1 = new List<MovimientoBancarioComprobanteDetailModel>();
                foreach (var item in current.Comprobantes) {
                    d1.Add(item as MovimientoBancarioComprobanteDetailModel);
                }
                this.SetDataSource("MovimientoBancarioComprobantes", DbConvert.ConvertToDataTable<MovimientoBancarioComprobanteDetailModel>(d1));
                current.Titulo = GetEnumDescription(current.FormatoImpreso).ToUpper();
            }
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d = DbConvert.ConvertToDataTable<MovimientoBancarioPrinter>(new List<MovimientoBancarioPrinter>() { current });
            this.SetDataSource("MovimientoBancario", d);
            this.Finalizar();
        }

        private void CrearEstadoCuenta() {
            var current = (EstadoCuentaPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancoEstadoCuentav10Reporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d0 = new List<MovimientoBancarioDetailModel>();
            foreach (var item in current.Movimientos) {
                d0.Add(item as MovimientoBancarioDetailModel);
            }
            var d = DbConvert.ConvertToDataTable<MovimientoBancarioDetailModel>(d0);
            var d1 = DbConvert.ConvertToDataTable(new List<BancoCuentaDetailModel> { current.Cuenta });
            this.SetDataSource("Cuenta", d1);
            this.SetDataSource("Saldo", DbConvert.ConvertToDataTable(new List<BancoCuentaSaldoModel> { current.Saldo }));
            this.SetDataSource("Movimientos", d);
            this.Finalizar();
        }

        private void CrearReporte1() {
            var current = (MovimientosReportePrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Banco.Reportes.BancoEstadoCuentav11Reporte.rdlc");
            
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d0 = new List<MovimientoBancarioDetailModel>();
            foreach (var item in current.Movimientos) {
                d0.Add(item as MovimientoBancarioDetailModel);
            }
            var d = DbConvert.ConvertToDataTable<MovimientoBancarioDetailModel>(d0);
            
            
            
            this.SetDataSource("Movimientos", d);
            this.Finalizar();
        }

        static string GetEnumDescription(Enum value) {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any()) {
                return attributes.First().Description;
            }

            return value.ToString();
        }
    }
}
