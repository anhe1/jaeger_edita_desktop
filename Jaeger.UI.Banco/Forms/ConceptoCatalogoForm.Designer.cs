﻿namespace Jaeger.UI.Banco.Forms
{
    partial class ConceptoCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConceptoCatalogoForm));
            this.TConcepto = new Jaeger.UI.Banco.Forms.ConceptoCatalogoGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TConcepto
            // 
            this.TConcepto.Caption = "";
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TConcepto.Location = new System.Drawing.Point(0, 0);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ShowActualizar = true;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowAutosuma = false;
            this.TConcepto.ShowCerrar = true;
            this.TConcepto.ShowEditar = true;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = true;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = true;
            this.TConcepto.ShowRemover = true;
            this.TConcepto.ShowSeleccionMultiple = true;
            this.TConcepto.Size = new System.Drawing.Size(937, 588);
            this.TConcepto.TabIndex = 1;
            // 
            // ConceptoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 588);
            this.Controls.Add(this.TConcepto);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConceptoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bancos: Catálogo de Conceptos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ConceptoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ConceptoCatalogoGridControl TConcepto;
    }
}
