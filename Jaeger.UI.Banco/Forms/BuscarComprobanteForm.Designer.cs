﻿namespace Jaeger.UI.Banco.Forms {
    partial class BuscarComprobanteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscarComprobanteForm));
            this.bwSearch = new System.ComponentModel.BackgroundWorker();
            this.TBuscar = new Jaeger.UI.Common.Forms.TbStandarSearchControl();
            this.GridSearch = new Jaeger.UI.Banco.Forms.ComprobanteGridControl();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch.MasterTemplate)).BeginInit();
            this.GridSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // bwSearch
            // 
            this.bwSearch.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerSearch_DoWork);
            this.bwSearch.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerSearch_RunWorkerCompleted);
            // 
            // TBuscar
            // 
            this.TBuscar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBuscar.Etiqueta = "";
            this.TBuscar.Location = new System.Drawing.Point(0, 0);
            this.TBuscar.Name = "TBuscar";
            this.TBuscar.ShowAgregar = true;
            this.TBuscar.ShowBuscar = true;
            this.TBuscar.ShowCerrar = true;
            this.TBuscar.ShowExistencia = false;
            this.TBuscar.ShowFiltro = false;
            this.TBuscar.ShowTextBox = false;
            this.TBuscar.Size = new System.Drawing.Size(902, 30);
            this.TBuscar.TabIndex = 3;
            // 
            // GridSearch
            // 
            this.GridSearch.Controls.Add(this.radWaitingBar1);
            this.GridSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridSearch.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridSearch.MasterTemplate.AllowAddNewRow = false;
            this.GridSearch.MasterTemplate.AllowDeleteRow = false;
            this.GridSearch.MasterTemplate.AllowEditRow = false;
            this.GridSearch.MasterTemplate.AllowRowResize = false;
            this.GridSearch.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "Status";
            gridViewTextBoxColumn5.HeaderText = "Status";
            gridViewTextBoxColumn5.Name = "Status";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn6.HeaderText = "Receptor RFC";
            gridViewTextBoxColumn6.Name = "ReceptorRFC";
            gridViewTextBoxColumn6.Width = 90;
            gridViewTextBoxColumn7.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn7.HeaderText = "Receptor";
            gridViewTextBoxColumn7.Name = "Receptor";
            gridViewTextBoxColumn7.Width = 200;
            gridViewTextBoxColumn8.FieldName = "FechaEmision";
            gridViewTextBoxColumn8.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn8.HeaderText = "Fecha Emisión";
            gridViewTextBoxColumn8.Name = "FechaEmision";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "Total";
            gridViewTextBoxColumn9.FormatString = "{0:n}";
            gridViewTextBoxColumn9.HeaderText = "Total";
            gridViewTextBoxColumn9.Name = "Total";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "Acumulado";
            gridViewTextBoxColumn10.FormatString = "{0:n}";
            gridViewTextBoxColumn10.HeaderText = "Acumulado";
            gridViewTextBoxColumn10.Name = "Acumulado";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            this.GridSearch.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.GridSearch.MasterTemplate.EnableAlternatingRowColor = true;
            this.GridSearch.MasterTemplate.EnableFiltering = true;
            this.GridSearch.MasterTemplate.EnableGrouping = false;
            this.GridSearch.MasterTemplate.ShowRowHeaderColumn = false;
            this.GridSearch.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridSearch.Name = "GridSearch";
            this.GridSearch.Size = new System.Drawing.Size(902, 226);
            this.GridSearch.SubTipo = Jaeger.Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido;
            this.GridSearch.TabIndex = 4;
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.GridSearch;
            this.radWaitingBar1.Location = new System.Drawing.Point(266, 86);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(130, 24);
            this.radWaitingBar1.TabIndex = 1;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingSpeed = 80;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // BuscarComprobanteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 256);
            this.Controls.Add(this.GridSearch);
            this.Controls.Add(this.TBuscar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BuscarComprobanteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar";
            this.Load += new System.EventHandler(this.BuscarComprobanteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
            this.GridSearch.ResumeLayout(false);
            this.GridSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker bwSearch;
        protected internal Common.Forms.TbStandarSearchControl TBuscar;
        public ComprobanteGridControl GridSearch;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
    }
}
