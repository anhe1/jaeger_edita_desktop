﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Banco.Forms {
    public partial class TbMovimientoControl : UserControl {
        public IBancoService Service;
        public EstadoCuentaModel CuentaCorriente;

        public TbMovimientoControl() {
            InitializeComponent();
        }

        private void TbMovimientoControl_Load(object sender, EventArgs e) {
            this.CuentaCorriente = new EstadoCuentaModel();
            this.ToolBar.OverflowButton.AddRemoveButtonsMenuItem.Enabled = false;
            this.ToolBar.OverflowButton.CustomizeButtonMenuItem.Enabled = false;

            this.Cuenta.HostedItem = this.CuentasBancaria.MultiColumnComboBoxElement;
            this.CuentasBancaria.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.CuentasBancaria.EditorControl.MasterTemplate.AllowRowResize = false; 
            
            this.TEjercicio.HostedItem = this.Ejercicio.SpinElement;
            this.Periodo.DisplayMember = "Descripcion";
            this.Periodo.ValueMember = "Id";
            this.Periodo.DataSource = ConfigService.GetMeses();
            this.Periodo.SelectedValue = DateTime.Now.Month;

            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Value = DateTime.Now.Year;
        }

        #region propiedades del control
        [Description("Mostar el botón Nuevo"), Category("Botones")]
        public bool ShowNuevo {
            get {
                return this.Nuevo.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Nuevo.Visibility = ElementVisibility.Visible;
                else
                    this.Nuevo.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Editar"), Category("Botones")]
        public bool ShowEditar {
            get {
                return this.Editar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Editar.Visibility = ElementVisibility.Visible;
                else
                    this.Editar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Cancelar"), Category("Botones")]
        public bool ShowCancelar {
            get {
                return this.Cancelar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cancelar.Visibility = ElementVisibility.Visible;
                else
                    this.Cancelar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Actualizar"), Category("Botones")]
        public bool ShowActualizar {
            get {
                return this.Actualizar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Actualizar.Visibility = ElementVisibility.Visible;
                else
                    this.Actualizar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Imprimir"), Category("Botones")]
        public bool ShowImprimir {
            get {
                return this.Imprimir.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Imprimir.Visibility = ElementVisibility.Visible;
                else
                    this.Imprimir.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Filtro"), Category("Botones")]
        public bool ShowFiltro {
            get {
                return this.Filtro.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Filtro.Visibility = ElementVisibility.Visible;
                else
                    this.Filtro.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Herramientas"), Category("Botones")]
        public bool ShowHerramientas {
            get {
                return this.Herramientas.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Herramientas.Visibility = ElementVisibility.Visible;
                else
                    this.Herramientas.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostar el botón Cerrar"), Category("Botones")]
        public bool ShowCerrar {
            get {
                return this.Cerrar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Cerrar.Visibility = ElementVisibility.Visible;
                else
                    this.Cerrar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Imprimir Comprobante"), Category("Botones")]
        public bool ShowReporteComprobante {
            get {
                return this.ReporteComprobante.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.ReporteComprobante.Visibility = ElementVisibility.Visible;
                else
                    this.ReporteComprobante.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar opción para exportar en modo excel"), Category("Botones")]
        public bool ShowExportar {
            get {
                return this.Exportar.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.Exportar.Visibility = ElementVisibility.Visible;
                else
                    this.Exportar.Visibility = ElementVisibility.Collapsed;
            }
        }

        [Description("Mostrar autosuma"), Category("Botones")]
        public bool ShowAutoSum {
            get {
                return this.AutoSuma.Visibility == ElementVisibility.Visible;
            }
            set {
                if (value == true)
                    this.AutoSuma.Visibility = ElementVisibility.Visible;
                else
                    this.AutoSuma.Visibility = ElementVisibility.Collapsed;
            }
        }
        #endregion

        public int GetPeriodo() {
            if (this.Periodo.SelectedValue != null) {
                return (int)this.Periodo.SelectedValue;
            } else {
                this.Periodo.PerformClick();
                return ConfigService.GetMeses(this.Periodo.Text);
            }
        }

        public int GetEjercicio() {
            return int.Parse(this.Ejercicio.Value.ToString());
        }

        public void CreateService() {
            this.Preparar.RunWorkerAsync();
        }

        private void CuentaBancaria_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.CuentasBancaria.SelectedItem as GridViewRowInfo;
            if (temporal != null) {
                var cuenta = temporal.DataBoundItem as BancoCuentaDetailModel;
                if (cuenta != null) {
                    this.CuentaCorriente.Cuenta = cuenta;
                    this.lblNumero.DataBindings.Clear();
                    this.lblNumero.DataBindings.Add("Text", this.CuentaCorriente.Cuenta, "NumCuenta", true, DataSourceUpdateMode.OnPropertyChanged);
                } else {
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_Cuentas_ErrorRecuperar, "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        public virtual void TInfo_Click(object sender, EventArgs e) {
            var info = this.CuentasBancaria.SelectedItem as GridViewRowInfo;
            var seleccionado = info.DataBoundItem as BancoCuentaDetailModel;
            if (seleccionado != null) {
                using (var editar = new CuentaBancariaForm(seleccionado, this.Service)) {
                    editar.ShowDialog(this);
                }
            }
        }

        public virtual void Nuevo_Transferencia_Click(object sender, EventArgs e) {
            using (var nuevo = new TransferenciaCuentasForm(this.Service)) {
                nuevo.ShowDialog(this);
            }
        }

        public virtual void Nuevo_Movimiento_Click(object sender, EventArgs e) {
            using (var nuevo = new MovimientoBancarioForm(this.Service)) {
                nuevo.ShowDialog(this);
            }
        }

        public virtual void Nuevo_SaldoInicial_Click(object sender, EventArgs e) {
            using (var nuevo = new CuentaBancoSaldoInicialForm(this.CuentaCorriente.Cuenta, new DateTime(this.GetEjercicio(), this.GetPeriodo(), 1), this.Service)) {
                nuevo.ShowDialog(this);
            }
        }

        /// <summary>
        /// Crear tablas de sisterma
        /// </summary>
        public virtual void Crear_Tablas_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_CrearTablas, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new Common.Forms.Waiting1Form(this.Service.CreateTables)) {
                    espera.ShowDialog(this);
                }
            }
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            var q0 = BancoCuentaService.Query().OnlyActive().Build();
            //var d = this.Service.GetList(false);
            var d = this.Service.GetList<BancoCuentaDetailModel>(q0).ToList();
            if (d != null) {
                if (d.Count > 0) {
                    this.CuentasBancaria.SelectedIndexChanged += this.CuentaBancaria_SelectedValueChanged;
                    this.CuentasBancaria.DataSource = d;
                }
            } else {
                this.CuentasBancaria.Enabled = false;
            }
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CuentasBancaria.Enabled = true;
        }
    }
}
