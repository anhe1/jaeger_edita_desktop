﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Aplication.Banco;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Banco.Forms {
    public partial class CuentaBancoSaldoInicialForm : RadForm {
        private IBancoCuentaService service;
        private BancoCuentaDetailModel corriente;
        private BancoCuentaSaldoDetailModel saldo;

        public CuentaBancoSaldoInicialForm(BancoCuentaDetailModel objeto, DateTime fecha, IBancoCuentaService service) {
            InitializeComponent();
            this.service = service;
            this.corriente = objeto;
            this.saldo = new BancoCuentaSaldoDetailModel {
                FechaInicial = fecha,
                IdCuenta = objeto.IdCuenta
            };
        }

        private void ViewCuentaBancoSaldoInicial_Load(object sender, EventArgs e) {
            this.service = new BancoService();
            var query = BancoCuentaService.Query().OnlyActive().Build();
            var d = this.service.GetList<BancoCuentaDetailModel>(query).ToList();
            this.CboEmisorCuenta.AutoSizeDropDownToBestFit = true;
            this.CboEmisorCuenta.DisplayMember = "Beneficiario";
            this.CboEmisorCuenta.DataSource = d;// this.service.GetList(true);
            this.CboEmisorCuenta.SelectedValueChanged += CboCtaDestino_SelectedValueChanged;
            this.CboEmisorCuenta.SelectedIndex = -1;
            this.ToolBar.Actualizar.PerformClick();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            BancoCuentaSaldoDetailModel test = this.service.GetSaldo(this.corriente.IdCuenta, this.saldo.FechaInicial);
            if (test != null) {
                this.saldo = test;
            }
            this.CreateBinding();
        }

        public virtual void Guardar_Click(object sender, EventArgs e) {
            this.saldo.Creo = ConfigService.Piloto.Clave;
            using (var espera = new Common.Forms.Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.Close();
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CboCtaDestino_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.CboEmisorCuenta.SelectedItem as GridViewRowInfo;
            if (temporal != null) {
                var o = temporal.DataBoundItem as BancoCuentaDetailModel;
                if (o != null) {
                    this.corriente = o;
                    this.corriente.Alias = o.Alias;
                    this.corriente.Banco = o.Banco;
                    this.corriente.CLABE = o.CLABE;
                    this.saldo.IdCuenta = o.IdCuenta;
                }
            }
        }

        public virtual void CreateBinding() {
            this.CboEmisorCuenta.DataBindings.Clear();
            this.CboEmisorCuenta.DataBindings.Add("Text", this.corriente, "Alias", true, DataSourceUpdateMode.OnPropertyChanged);
            this.CboEmisorCuenta.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;

            this.TxbEmisorSucursal.DataBindings.Clear();
            this.TxbEmisorSucursal.DataBindings.Add("Text", this.corriente, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorClabe.DataBindings.Clear();
            this.TxbEmisorClabe.DataBindings.Add("Text", this.corriente, "Clabe", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorNumeroCta.DataBindings.Clear();
            this.TxbEmisorNumeroCta.DataBindings.Add("Text", this.corriente, "NumCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorBanco.DataBindings.Clear();
            this.TxbEmisorBanco.DataBindings.Add("Text", this.corriente, "Banco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbEmisorRfc.DataBindings.Clear();
            this.TxbEmisorRfc.DataBindings.Add("Text", this.corriente, "Rfc", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaDocumento.DataBindings.Clear();
            this.FechaDocumento.SetToNullValue();
            this.FechaDocumento.DataBindings.Add("Value", this.saldo, "FechaInicial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TxbAbono.DataBindings.Clear();
            this.TxbAbono.DataBindings.Add("Text", this.saldo, "SaldoInicial", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        public virtual void Guardar() {
            this.saldo = this.service.Save(this.saldo);
        }
    }
}
