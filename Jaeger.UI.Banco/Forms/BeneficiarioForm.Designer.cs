﻿namespace Jaeger.UI.Banco.Forms
{
    partial class BeneficiarioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BeneficiarioForm));
            this.Informacion = new Telerik.WinControls.UI.RadPanel();
            this.lblRefComercial = new Telerik.WinControls.UI.RadLabel();
            this.chkExtranjero = new Telerik.WinControls.UI.RadCheckBox();
            this.cboRelacionComercial = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.txtClave = new Telerik.WinControls.UI.RadTextBox();
            this.lblClave = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.txtRFC = new Telerik.WinControls.UI.RadTextBox();
            this.lblCURP = new Telerik.WinControls.UI.RadLabel();
            this.txtCURP = new Telerik.WinControls.UI.RadTextBox();
            this.lblRegistroFiscal = new Telerik.WinControls.UI.RadLabel();
            this.lblRazonSocial = new Telerik.WinControls.UI.RadLabel();
            this.txtBeneficiario = new Telerik.WinControls.UI.RadTextBox();
            this.lblCorreo = new Telerik.WinControls.UI.RadLabel();
            this.txtTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.lblTelefono = new Telerik.WinControls.UI.RadLabel();
            this.txtCorreo = new Telerik.WinControls.UI.RadTextBox();
            this.txtNumRegIdTrib = new Telerik.WinControls.UI.RadTextBox();
            this.gCtaBanco = new Telerik.WinControls.UI.RadGridView();
            this.TCuenta = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TBeneficiario = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.Informacion)).BeginInit();
            this.Informacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblRefComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExtranjero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRelacionComercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroFiscal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeneficiario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumRegIdTrib)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCtaBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCtaBanco.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Informacion
            // 
            this.Informacion.Controls.Add(this.lblRefComercial);
            this.Informacion.Controls.Add(this.chkExtranjero);
            this.Informacion.Controls.Add(this.cboRelacionComercial);
            this.Informacion.Controls.Add(this.txtClave);
            this.Informacion.Controls.Add(this.lblClave);
            this.Informacion.Controls.Add(this.lblRFC);
            this.Informacion.Controls.Add(this.txtRFC);
            this.Informacion.Controls.Add(this.lblCURP);
            this.Informacion.Controls.Add(this.txtCURP);
            this.Informacion.Controls.Add(this.lblRegistroFiscal);
            this.Informacion.Controls.Add(this.lblRazonSocial);
            this.Informacion.Controls.Add(this.txtBeneficiario);
            this.Informacion.Controls.Add(this.lblCorreo);
            this.Informacion.Controls.Add(this.txtTelefono);
            this.Informacion.Controls.Add(this.lblTelefono);
            this.Informacion.Controls.Add(this.txtCorreo);
            this.Informacion.Controls.Add(this.txtNumRegIdTrib);
            this.Informacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.Informacion.Location = new System.Drawing.Point(0, 30);
            this.Informacion.Name = "Informacion";
            this.Informacion.Size = new System.Drawing.Size(621, 114);
            this.Informacion.TabIndex = 72;
            // 
            // lblRefComercial
            // 
            this.lblRefComercial.Location = new System.Drawing.Point(395, 82);
            this.lblRefComercial.Name = "lblRefComercial";
            this.lblRefComercial.Size = new System.Drawing.Size(79, 18);
            this.lblRefComercial.TabIndex = 85;
            this.lblRefComercial.Text = "Rel. Comercial:";
            // 
            // chkExtranjero
            // 
            this.chkExtranjero.Location = new System.Drawing.Point(533, 29);
            this.chkExtranjero.Name = "chkExtranjero";
            this.chkExtranjero.Size = new System.Drawing.Size(70, 18);
            this.chkExtranjero.TabIndex = 84;
            this.chkExtranjero.Text = "Extranjero";
            // 
            // cboRelacionComercial
            // 
            this.cboRelacionComercial.Location = new System.Drawing.Point(480, 81);
            this.cboRelacionComercial.Name = "cboRelacionComercial";
            this.cboRelacionComercial.NullText = "Relación Comercial";
            this.cboRelacionComercial.Size = new System.Drawing.Size(129, 20);
            this.cboRelacionComercial.TabIndex = 79;
            // 
            // txtClave
            // 
            this.txtClave.Location = new System.Drawing.Point(14, 28);
            this.txtClave.Name = "txtClave";
            this.txtClave.NullText = "Clave";
            this.txtClave.Size = new System.Drawing.Size(83, 20);
            this.txtClave.TabIndex = 70;
            this.txtClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblClave
            // 
            this.lblClave.Location = new System.Drawing.Point(14, 6);
            this.lblClave.Name = "lblClave";
            this.lblClave.Size = new System.Drawing.Size(35, 18);
            this.lblClave.TabIndex = 69;
            this.lblClave.Text = "Clave:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(103, 5);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(131, 18);
            this.lblRFC.TabIndex = 71;
            this.lblRFC.Text = "Reg. Fed. Contribuyentes";
            // 
            // txtRFC
            // 
            this.txtRFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRFC.Location = new System.Drawing.Point(103, 28);
            this.txtRFC.MaxLength = 14;
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.NullText = "RFC";
            this.txtRFC.Size = new System.Drawing.Size(131, 20);
            this.txtRFC.TabIndex = 72;
            this.txtRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCURP
            // 
            this.lblCURP.Location = new System.Drawing.Point(248, 5);
            this.lblCURP.Name = "lblCURP";
            this.lblCURP.Size = new System.Drawing.Size(132, 18);
            this.lblCURP.TabIndex = 73;
            this.lblCURP.Text = "Clv. Única Reg. Población";
            // 
            // txtCURP
            // 
            this.txtCURP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCURP.Location = new System.Drawing.Point(248, 28);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.NullText = "CURP";
            this.txtCURP.Size = new System.Drawing.Size(132, 20);
            this.txtCURP.TabIndex = 74;
            this.txtCURP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRegistroFiscal
            // 
            this.lblRegistroFiscal.Location = new System.Drawing.Point(386, 6);
            this.lblRegistroFiscal.Name = "lblRegistroFiscal";
            this.lblRegistroFiscal.Size = new System.Drawing.Size(106, 18);
            this.lblRegistroFiscal.TabIndex = 83;
            this.lblRegistroFiscal.Text = "Núm. Reg. Id. Fiscal:";
            // 
            // lblRazonSocial
            // 
            this.lblRazonSocial.Location = new System.Drawing.Point(13, 56);
            this.lblRazonSocial.Name = "lblRazonSocial";
            this.lblRazonSocial.Size = new System.Drawing.Size(86, 18);
            this.lblRazonSocial.TabIndex = 75;
            this.lblRazonSocial.Text = "Identidad Fiscal:";
            // 
            // txtBeneficiario
            // 
            this.txtBeneficiario.Location = new System.Drawing.Point(103, 54);
            this.txtBeneficiario.Name = "txtBeneficiario";
            this.txtBeneficiario.NullText = "Identidad Fiscal";
            this.txtBeneficiario.Size = new System.Drawing.Size(506, 20);
            this.txtBeneficiario.TabIndex = 76;
            // 
            // lblCorreo
            // 
            this.lblCorreo.Location = new System.Drawing.Point(191, 82);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(43, 18);
            this.lblCorreo.TabIndex = 82;
            this.lblCorreo.Text = "Correo:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(69, 81);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.NullText = "Teléfono";
            this.txtTelefono.Size = new System.Drawing.Size(107, 20);
            this.txtTelefono.TabIndex = 77;
            // 
            // lblTelefono
            // 
            this.lblTelefono.Location = new System.Drawing.Point(13, 82);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 18);
            this.lblTelefono.TabIndex = 81;
            this.lblTelefono.Text = "Teléfono:";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(240, 81);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.NullText = "Correo Electrónico";
            this.txtCorreo.Size = new System.Drawing.Size(140, 20);
            this.txtCorreo.TabIndex = 78;
            // 
            // txtNumRegIdTrib
            // 
            this.txtNumRegIdTrib.Location = new System.Drawing.Point(386, 28);
            this.txtNumRegIdTrib.Name = "txtNumRegIdTrib";
            this.txtNumRegIdTrib.Size = new System.Drawing.Size(141, 20);
            this.txtNumRegIdTrib.TabIndex = 80;
            // 
            // gCtaBanco
            // 
            this.gCtaBanco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gCtaBanco.Location = new System.Drawing.Point(0, 174);
            // 
            // 
            // 
            this.gCtaBanco.MasterTemplate.AllowAddNewRow = false;
            this.gCtaBanco.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewCheckBoxColumn1.FieldName = "IsActive";
            gridViewCheckBoxColumn1.HeaderText = "IsActive";
            gridViewCheckBoxColumn1.IsVisible = false;
            gridViewCheckBoxColumn1.Name = "IsActive";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.DataType = typeof(int);
            gridViewTextBoxColumn2.FieldName = "SubId";
            gridViewTextBoxColumn2.HeaderText = "SubId";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "SubId";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewCheckBoxColumn2.FieldName = "Verificado";
            gridViewCheckBoxColumn2.HeaderText = "Verificado";
            gridViewCheckBoxColumn2.Name = "Verificado";
            gridViewMultiComboBoxColumn1.FieldName = "Banco";
            gridViewMultiComboBoxColumn1.HeaderText = "Institución Bancaria";
            gridViewMultiComboBoxColumn1.Name = "Banco";
            gridViewMultiComboBoxColumn1.Width = 120;
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.FieldName = "InsitucionBancaria";
            gridViewTextBoxColumn4.HeaderText = "Nombre Corto";
            gridViewTextBoxColumn4.Name = "InsitucionBancaria";
            gridViewTextBoxColumn4.Width = 150;
            gridViewTextBoxColumn5.FieldName = "Sucursal";
            gridViewTextBoxColumn5.HeaderText = "Sucursal";
            gridViewTextBoxColumn5.Name = "Sucursal";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.FieldName = "NumeroDeCuenta";
            gridViewTextBoxColumn6.HeaderText = "Número de Cuenta";
            gridViewTextBoxColumn6.Name = "NumeroDeCuenta";
            gridViewTextBoxColumn6.Width = 120;
            gridViewTextBoxColumn7.FieldName = "Clabe";
            gridViewTextBoxColumn7.HeaderText = "Cuenta Clabe";
            gridViewTextBoxColumn7.Name = "Clabe";
            gridViewTextBoxColumn7.Width = 110;
            gridViewTextBoxColumn8.FieldName = "Nombre";
            gridViewTextBoxColumn8.HeaderText = "Nombre (s)";
            gridViewTextBoxColumn8.Name = "Beneficiario";
            gridViewTextBoxColumn8.Width = 150;
            gridViewTextBoxColumn9.FieldName = "PrimerApellido";
            gridViewTextBoxColumn9.HeaderText = "Apellido Paterno";
            gridViewTextBoxColumn9.Name = "PrimerApellido";
            gridViewTextBoxColumn9.Width = 110;
            gridViewTextBoxColumn10.FieldName = "SegundoApellido";
            gridViewTextBoxColumn10.HeaderText = "Apellido Materno";
            gridViewTextBoxColumn10.Name = "SegundoApellido";
            gridViewTextBoxColumn10.Width = 110;
            gridViewTextBoxColumn11.FieldName = "Rfc";
            gridViewTextBoxColumn11.HeaderText = "RFC";
            gridViewTextBoxColumn11.Name = "RFC";
            gridViewTextBoxColumn11.Width = 90;
            gridViewComboBoxColumn1.FieldName = "TipoCuenta";
            gridViewComboBoxColumn1.HeaderText = "Tipo de Cuenta";
            gridViewComboBoxColumn1.Name = "TipoCuenta";
            gridViewComboBoxColumn1.Width = 80;
            gridViewTextBoxColumn12.DataType = typeof(double);
            gridViewTextBoxColumn12.FieldName = "CargoMaximo";
            gridViewTextBoxColumn12.HeaderText = "Cargo Máximo";
            gridViewTextBoxColumn12.Name = "CargoMaximo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 80;
            gridViewTextBoxColumn13.FieldName = "Alias";
            gridViewTextBoxColumn13.HeaderText = "Alias de cuenta";
            gridViewTextBoxColumn13.Name = "Alias";
            gridViewTextBoxColumn13.Width = 75;
            gridViewTextBoxColumn13.WrapText = true;
            gridViewTextBoxColumn14.FieldName = "RefNumerica";
            gridViewTextBoxColumn14.HeaderText = "Ref. Númerica";
            gridViewTextBoxColumn14.MaxLength = 7;
            gridViewTextBoxColumn14.Name = "RefNumerica";
            gridViewTextBoxColumn15.FieldName = "RefAlfanumerica";
            gridViewTextBoxColumn15.HeaderText = "Ref. Alfanúmerica";
            gridViewTextBoxColumn15.MaxLength = 40;
            gridViewTextBoxColumn15.Name = "RefAlfanumerica";
            gridViewTextBoxColumn15.Width = 80;
            gridViewDateTimeColumn1.FieldName = "FecNuevo";
            gridViewDateTimeColumn1.FormatString = "{0:dd MMM yy}";
            gridViewDateTimeColumn1.HeaderText = "Fec. Sist.";
            gridViewDateTimeColumn1.IsVisible = false;
            gridViewDateTimeColumn1.Name = "DateNew";
            gridViewTextBoxColumn16.FieldName = "Creo";
            gridViewTextBoxColumn16.HeaderText = "Creó";
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "Creo";
            this.gCtaBanco.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewCheckBoxColumn2,
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn16});
            this.gCtaBanco.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gCtaBanco.Name = "gCtaBanco";
            this.gCtaBanco.ShowGroupPanel = false;
            this.gCtaBanco.Size = new System.Drawing.Size(621, 248);
            this.gCtaBanco.TabIndex = 3;
            this.gCtaBanco.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridCtaBanco_CellBeginEdit);
            this.gCtaBanco.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridCtaBanco_CellEndEdit);
            this.gCtaBanco.SelectionChanged += new System.EventHandler(this.GridCtaBanco_SelectionChanged);
            // 
            // TCuenta
            // 
            this.TCuenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCuenta.Etiqueta = "Cuentas Bancarias";
            this.TCuenta.Location = new System.Drawing.Point(0, 144);
            this.TCuenta.Name = "TCuenta";
            this.TCuenta.ReadOnly = false;
            this.TCuenta.ShowActualizar = false;
            this.TCuenta.ShowAutorizar = false;
            this.TCuenta.ShowCerrar = false;
            this.TCuenta.ShowEditar = false;
            this.TCuenta.ShowExportarExcel = false;
            this.TCuenta.ShowFiltro = false;
            this.TCuenta.ShowGuardar = false;
            this.TCuenta.ShowHerramientas = false;
            this.TCuenta.ShowImagen = false;
            this.TCuenta.ShowImprimir = false;
            this.TCuenta.ShowNuevo = true;
            this.TCuenta.ShowRemover = true;
            this.TCuenta.Size = new System.Drawing.Size(621, 30);
            this.TCuenta.TabIndex = 4;
            // 
            // TBeneficiario
            // 
            this.TBeneficiario.Dock = System.Windows.Forms.DockStyle.Top;
            this.TBeneficiario.Etiqueta = "";
            this.TBeneficiario.Location = new System.Drawing.Point(0, 0);
            this.TBeneficiario.Name = "TBeneficiario";
            this.TBeneficiario.ReadOnly = false;
            this.TBeneficiario.ShowActualizar = false;
            this.TBeneficiario.ShowAutorizar = false;
            this.TBeneficiario.ShowCerrar = true;
            this.TBeneficiario.ShowEditar = false;
            this.TBeneficiario.ShowExportarExcel = false;
            this.TBeneficiario.ShowFiltro = false;
            this.TBeneficiario.ShowGuardar = true;
            this.TBeneficiario.ShowHerramientas = false;
            this.TBeneficiario.ShowImagen = false;
            this.TBeneficiario.ShowImprimir = false;
            this.TBeneficiario.ShowNuevo = false;
            this.TBeneficiario.ShowRemover = false;
            this.TBeneficiario.Size = new System.Drawing.Size(621, 30);
            this.TBeneficiario.TabIndex = 74;
            // 
            // BeneficiarioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 422);
            this.Controls.Add(this.gCtaBanco);
            this.Controls.Add(this.TCuenta);
            this.Controls.Add(this.Informacion);
            this.Controls.Add(this.TBeneficiario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BeneficiarioForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Beneficiario";
            this.Load += new System.EventHandler(this.BeneficiarioBancarioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Informacion)).EndInit();
            this.Informacion.ResumeLayout(false);
            this.Informacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblRefComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExtranjero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRelacionComercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblClave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegistroFiscal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeneficiario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumRegIdTrib)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCtaBanco.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCtaBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel Informacion;
        internal Telerik.WinControls.UI.RadGridView gCtaBanco;
        private Telerik.WinControls.UI.RadCheckBox chkExtranjero;
        private Telerik.WinControls.UI.RadCheckedDropDownList cboRelacionComercial;
        private Telerik.WinControls.UI.RadTextBox txtClave;
        private Telerik.WinControls.UI.RadLabel lblClave;
        private Telerik.WinControls.UI.RadLabel lblRFC;
        private Telerik.WinControls.UI.RadTextBox txtRFC;
        private Telerik.WinControls.UI.RadLabel lblCURP;
        private Telerik.WinControls.UI.RadTextBox txtCURP;
        private Telerik.WinControls.UI.RadLabel lblRegistroFiscal;
        private Telerik.WinControls.UI.RadLabel lblRazonSocial;
        private Telerik.WinControls.UI.RadTextBox txtBeneficiario;
        private Telerik.WinControls.UI.RadLabel lblCorreo;
        private Telerik.WinControls.UI.RadTextBox txtTelefono;
        private Telerik.WinControls.UI.RadLabel lblTelefono;
        private Telerik.WinControls.UI.RadTextBox txtCorreo;
        private Telerik.WinControls.UI.RadTextBox txtNumRegIdTrib;
        private Telerik.WinControls.UI.RadLabel lblRefComercial;
        private Common.Forms.ToolBarStandarControl TBeneficiario;
        private Common.Forms.ToolBarStandarControl TCuenta;
    }
}
