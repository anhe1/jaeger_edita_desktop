﻿using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Banco.Forms {
    public partial class ComplementoPagoForm : RadForm {
        public ComplementoPagoForm() {
            InitializeComponent();
        }

        private void ComplementoPagoForm_Load(object sender, EventArgs e) {

        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Copiar_Click(object sender, EventArgs e) {
            System.Windows.Forms.Clipboard.SetText(this.Resultado.Text);
        }
    }
}
