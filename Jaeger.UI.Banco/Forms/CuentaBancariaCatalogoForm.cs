﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;

namespace Jaeger.UI.Banco.Forms {
    /// <summary>
    /// Banco: 
    /// </summary>
    public partial class CuentaBancariaCatalogoForm : RadForm {
        protected internal RadMenuItem CrearTablas = new RadMenuItem { Text = "Crear tablas", Name = "CrearTablas" };

        public CuentaBancariaCatalogoForm() {
            InitializeComponent();
        }

        public CuentaBancariaCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this.TCuentaB.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void CuentaBancariaCatalogoForm_Load(object sender, EventArgs e) {
            this.TCuentaB.Herramientas.Items.Add(this.CrearTablas);
            this.CrearTablas.Click += this.CrearTablas_Click;
            this.TCuentaB.Nuevo.Click += this.Agregar_Click;
            this.TCuentaB.Actualizar.Click += this.Actualizar_Click;
            this.TCuentaB.Cerrar.Click += this.Cerrar_Click;
            this.TCuentaB.Edicion += TCuentaB_Edicion;
            this.Tag = null;
        }

        #region barra de herramientas
        public virtual void Agregar_Click(object sender, EventArgs e) {
            using (var nuevo = new CuentaBancariaForm(null, this.TCuentaB._Service)) {
                nuevo.ShowDialog(this);
            }
        }

        private void TCuentaB_Edicion(object sender, IBancoCuentaDetailModel e) {
            if (e.ReadOnly == true) {
                RadMessageBox.Show(this, Properties.Resources.msg_Banco_CuentaBancaria_NoEditable, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
            using (var editar = new CuentaBancariaForm(e, this.TCuentaB._Service)) {
                editar.ShowDialog(this);
            }
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consulta)) {
                espera.Text = Properties.Resources.msg_Sistema_Consulta_Informacion;
                espera.ShowDialog(this);
            }
            this.TCuentaB.GridData.DataSource = this.TCuentaB._DataSource;
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void CrearTablas_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, Properties.Resources.msg_Banco_CrearTablas, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                using (var espera = new Waiting2Form(this.Crear)) {
                    espera.Text = Properties.Resources.msg_Sistema_Creando_Tablas;
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Consulta() {
            var query = this.TCuentaB._Service.GetList<BancoCuentaDetailModel>(Aplication.Banco.BancoCuentaService.Query().OnlyActive().Build()).ToList<IBancoCuentaDetailModel>();
            this.TCuentaB._DataSource = new BindingList<IBancoCuentaDetailModel>(query);
        }

        public virtual void Crear() {
            this.TCuentaB._Service.CreateTables();
        }
        #endregion
    }
}
