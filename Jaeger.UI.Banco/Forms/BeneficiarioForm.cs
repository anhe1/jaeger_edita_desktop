﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Banco;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.UI.Banco.Forms {
    public partial class BeneficiarioForm : RadForm {
        protected IBancoService servicio;
        private BeneficiarioDetailModel beneficiario;
        private RelacionComercialData rc = new RelacionComercialData();
        private BindingList<RelacionComercial> dataRelations = new BindingList<RelacionComercial>();
        private IBancosCatalogo catalogoBancos = new BancosCatalogo();

        public BeneficiarioForm(BeneficiarioDetailModel beneficiario, IBancoService servicio) {
            InitializeComponent();
            this.servicio = servicio;
            this.beneficiario = beneficiario;
        }

        private void BeneficiarioBancarioForm_Load(object sender, EventArgs e) {
            if (this.beneficiario == null)
                this.beneficiario = new BeneficiarioDetailModel();
            // relaciones comerciales
            this.cboRelacionComercial.CheckedMember = "Selected";
            this.cboRelacionComercial.DisplayMember = "Name";
            this.cboRelacionComercial.ValueMember = "Id";
            foreach (RelacionComercial item in rc.Items) {
                this.dataRelations.Add(item);
            }

            this.cboRelacionComercial.DataSource = dataRelations;
            this.catalogoBancos.Load();
            var cbancos = (GridViewMultiComboBoxColumn)this.gCtaBanco.MasterTemplate.Columns["Banco"];
            cbancos.DisplayMember = "RazonSocial";
            cbancos.ValueMember = "RazonSocial";
            cbancos.DataSource = this.catalogoBancos.Items;
            this.CreateBinding();

            this.TBeneficiario.Guardar.Click += this.TBeneficiario_Guardar_Click;
            this.TBeneficiario.Cerrar.Click += this.TBeneficiario_Cerrar_Click;

            this.TCuenta.Nuevo.Click += this.TCuenta_Agregar_Click;
            this.TCuenta.Remover.Click += this.TCuenta_Quitar_Click;
        }

        private void TBeneficiario_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
        }

        private void TBeneficiario_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region cuentas bancarias   

        private void TCuenta_Agregar_Click(object sender, EventArgs e) {
            this.gCtaBanco.Rows.AddNew();
        }

        private void TCuenta_Quitar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show("¿Esta seguro de remover la cuenta de banco seleccionada?", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                if (this.gCtaBanco.CurrentRow != null) {
                    if (DbConvert.ConvertInt32((this.gCtaBanco.CurrentRow.Cells["Id"].Value)) <= 0) {
                        this.gCtaBanco.Rows.Remove(this.gCtaBanco.CurrentRow);
                    } else {
                        this.gCtaBanco.CurrentRow.Cells["IsActive"].Value = false;
                    }
                    this.gCtaBanco.Refresh();
                }
            }
        }

        private void GridCtaBanco_SelectionChanged(object sender, EventArgs e) {
            if (this.gCtaBanco.CurrentRow != null) {
                var cuenta = this.gCtaBanco.CurrentRow.DataBoundItem as CuentaBancariaModel;
                if (cuenta != null) {
                    if (cuenta.IdCuenta == 0) {
                        this.TBeneficiario.Remover.Enabled = true;
                        this.gCtaBanco.AllowEditRow = true;
                    } else {
                        this.TBeneficiario.Remover.Enabled = true;
                        this.gCtaBanco.AllowEditRow = true;
                    }
                }
            }
        }

        private void GridCtaBanco_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (DbConvert.ConvertBool(e.Row.Cells["Verificado"].Value) == true) {
                e.Cancel = true;
                return;
            }
            if (this.gCtaBanco.CurrentColumn is GridViewMultiComboBoxColumn) {
                RadMultiColumnComboBoxElement editor = (RadMultiColumnComboBoxElement)this.gCtaBanco.ActiveEditor;
                if ((string)editor.Tag == "listo") {
                    return;
                }
                editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Clave", Name = "Clave", FieldName = "Clave" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Descripcion", Name = "Descripcion", FieldName = "Descripcion" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Razon Social", Name = "RazonSocial", FieldName = "RazonSocial" });
                editor.AutoSizeDropDownToBestFit = true;
                editor.Tag = "listo";
            }
        }

        private void GridCtaBanco_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.ActiveEditor is RadMultiColumnComboBoxElement) {
                RadMultiColumnComboBoxElement editor = e.ActiveEditor as RadMultiColumnComboBoxElement;
                if (!(editor == null)) {
                    var viewInfo = editor.SelectedItem as GridViewRowInfo;
                    if (!(viewInfo == null)) {
                        var item = viewInfo.DataBoundItem as ClaveBanco;
                        if (item != null) {
                            e.Row.Cells["Clave"].Value = item.Clave;
                            e.Row.Cells["InsitucionBancaria"].Value = item.Descripcion;
                            e.Row.Cells["Banco"].Value = item.RazonSocial;
                            e.Row.Cells["Beneficiario"].Value = this.beneficiario.Nombre;
                            e.Row.Cells["Rfc"].Value = this.beneficiario.RFC;
                        }
                    }
                }
            }
        }

        #endregion

        private void Guardar() {
            this.beneficiario.Relacion = rc.GetRelacion();
            //this.beneficiario = this.servicio.Save(this.beneficiario);
        }

        private void CreateBinding() {
            this.txtClave.DataBindings.Add("Text", this.beneficiario, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtRFC.DataBindings.Add("Text", this.beneficiario, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCURP.DataBindings.Add("Text", this.beneficiario, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtNumRegIdTrib.DataBindings.Add("Text", this.beneficiario, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtBeneficiario.DataBindings.Add("Text", this.beneficiario, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtTelefono.DataBindings.Add("Text", this.beneficiario, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);
            this.txtCorreo.DataBindings.Add("Text", this.beneficiario, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);
            this.chkExtranjero.DataBindings.Add("Checked", this.beneficiario, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);
            this.gCtaBanco.DataSource = this.beneficiario.CuentasBancarias;
            rc.SetRelacion(this.beneficiario.Relacion);
        }
    }
}
