﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Banco;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using System.Linq;

namespace Jaeger.UI.Banco.Forms {
    public partial class BancoReporteForm : RadForm {
        protected IBancoService Service;

        public EstadoCuentaModel CuentaCorriente {
            get;
            private set;
        }

        public BancoReporteForm() {
            InitializeComponent();
        }

        private void BancoReporteForm_Load(object sender, EventArgs e) {
            this.Service = new BancoService();
            this.CuentaCorriente = new EstadoCuentaModel();
            this.backgroundWorker1.RunWorkerAsync();
        }

        private void CuentaBancaria_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.CuentaBancaria.SelectedItem as GridViewRowInfo;
            if (temporal != null) {
                var cuenta = temporal.DataBoundItem as BancoCuentaDetailModel;
                if (cuenta != null) {
                    
                } else {
                    RadMessageBox.Show(this, Properties.Resources.msg_Banco_Cuentas_ErrorRecuperar, "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            //var d = this.Service.GetList(false);
            var query = BancoCuentaService.Query().OnlyActive().Build();
            var d = this.Service.GetList<BancoCuentaDetailModel>(query).ToList();
            if (d != null) {
                if (d.Count > 0) {
                    this.CuentaBancaria.SelectedIndexChanged += this.CuentaBancaria_SelectedValueChanged;
                    this.CuentaBancaria.DataSource = d;
                }
            } else {
                this.CuentaBancaria.Enabled = false;
            }
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CuentaBancaria.Enabled = true;
        }

        private void Crear_Click(object sender, EventArgs e) {
            //this.Service.GetList(0, 0, new DateTime(2021, 3, 1), new DateTime(2021, 3, 1), true);
        }
    }
}
