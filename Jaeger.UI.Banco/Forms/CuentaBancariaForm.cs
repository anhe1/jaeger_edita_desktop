﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Aplication.Banco;
using Jaeger.UI.Common.Services;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos.Entities;

namespace Jaeger.UI.Banco.Forms {
    public partial class CuentaBancariaForm : RadForm {
        #region declaraciones
        protected IBancoCuentaService service;
        protected IBancosCatalogo bancoCatalogo = new BancosCatalogo();
        private IBancoCuentaDetailModel cuentaBancaria;
        #endregion

        public CuentaBancariaForm(IBancoCuentaService service) {
            InitializeComponent();
            this.service = service;
        }

        public CuentaBancariaForm(IBancoCuentaDetailModel model, IBancoCuentaService service) {
            InitializeComponent();
            this.service = service;
            this.cuentaBancaria = model;
        }

        private void CuentaBancariaForm_Load(object sender, EventArgs e) {
            this.bancoCatalogo.Load();

            this.TCuenta.Nuevo.Click += this.TCuenta_Nuevo_Click;
            this.TCuenta.Guardar.Click += this.TCuenta_Guardar_Click;
            this.TCuenta.Actualizar.Click += this.TCuenta_Actualizar_Click;
            this.TCuenta.Cerrar.Click += this.TCuenta_Cerrar_Click;

            this.NumCuenta.KeyPress += Extensions.TextBoxOnlyNumbers_KeyPress;
            this.ClaveBanco.DataSource = this.bancoCatalogo.Items;
            this.ClaveBanco.SelectedIndex = -1;
            this.Moneda.DataSource = this.service.GetMonedas();
            this.TCuenta.Actualizar.PerformClick();
            this.ClaveBanco.SelectedIndexChanged += CboClaveBanco_SelectedIndexChanged;
        }

        private void CboClaveBanco_SelectedIndexChanged(object sender, EventArgs e) {
            var seleccionado = ((GridViewDataRowInfo)this.ClaveBanco.SelectedItem).DataBoundItem as ClaveBanco;
            if (seleccionado != null) {
                this.cuentaBancaria.Banco = seleccionado.Descripcion;
            }
        }

        private void TCuenta_Nuevo_Click(object sender, EventArgs e) {
            this.cuentaBancaria = null;
            this.TCuenta.Actualizar.PerformClick();
        }

        private void TCuenta_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                using (var espera = new Common.Forms.Waiting2Form(this.Guardar)) {
                    espera.ShowDialog(this);
                    this.Close();
                }
            }
        }

        private void TCuenta_Actualizar_Click(object sender, EventArgs e) {
            if (this.cuentaBancaria == null) {
                this.cuentaBancaria = new BancoCuentaDetailModel();
            } else {
                using (var espera = new UI.Common.Forms.Waiting2Form(this.Actualizar)) {
                    espera.ShowDialog(this);
                }
            }
            this.CreateBinding();
        }

        private void TCuenta_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void CreateBinding() {
            this.Beneficiario.DataBindings.Clear();
            this.Beneficiario.DataBindings.Add("Text", this.cuentaBancaria, "Beneficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BeneficiarioRFC.DataBindings.Clear();
            this.BeneficiarioRFC.DataBindings.Add("Text", this.cuentaBancaria, "RFCBenficiario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Activo.DataBindings.Clear();
            this.Activo.DataBindings.Add("Checked", this.cuentaBancaria, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCuenta.DataBindings.Clear();
            this.NumCuenta.DataBindings.Add("Text", this.cuentaBancaria, "NumCuenta", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCliente.DataBindings.Clear();
            this.NumCliente.DataBindings.Add("Text", this.cuentaBancaria, "NumCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Clabe.DataBindings.Clear();
            this.Clabe.DataBindings.Add("Text", this.cuentaBancaria, "CLABE", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CtaContable.DataBindings.Clear();
            this.CtaContable.DataBindings.Add("Text", this.cuentaBancaria, "CuentaContable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoRFC.DataBindings.Clear();
            this.BancoRFC.DataBindings.Add("Text", this.cuentaBancaria, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NumCliente.DataBindings.Clear();
            this.NumCliente.DataBindings.Add("Text", this.cuentaBancaria, "NumCliente", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Moneda.DataBindings.Clear();
            this.Moneda.DataBindings.Add("Text", this.cuentaBancaria, "Moneda", true, DataSourceUpdateMode.OnPropertyChanged);

            this.ClaveBanco.DataBindings.Clear();
            this.ClaveBanco.DataBindings.Add("SelectedValue", this.cuentaBancaria, "ClaveBanco", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Sucursal.DataBindings.Clear();
            this.Sucursal.DataBindings.Add("Text", this.cuentaBancaria, "Sucursal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Telefono.DataBindings.Clear();
            this.Telefono.DataBindings.Add("Text", this.cuentaBancaria, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Funcionario.DataBindings.Clear();
            this.Funcionario.DataBindings.Add("Text", this.cuentaBancaria, "Funcionario", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Alias.DataBindings.Clear();
            this.Alias.DataBindings.Add("Text", this.cuentaBancaria, "Alias", true, DataSourceUpdateMode.OnPropertyChanged);

            this.SaldoInicial.DataBindings.Clear();
            this.SaldoInicial.DataBindings.Add("Text", this.cuentaBancaria, "SaldoInicial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.NoCheque.DataBindings.Clear();
            this.NoCheque.DataBindings.Add("Text", this.cuentaBancaria, "NoCheque", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiaCorte.DataBindings.Clear();
            this.DiaCorte.DataBindings.Add("Text", this.cuentaBancaria, "DiaCorte", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaApertura.DataBindings.Clear();
            this.FechaApertura.DataBindings.Add("Value", this.cuentaBancaria, "FechaApertura", true, DataSourceUpdateMode.OnPropertyChanged);

            this.CheckBanco.DataBindings.Clear();
            this.CheckBanco.DataBindings.Add("Checked", this.cuentaBancaria, "IsIncluded", true, DataSourceUpdateMode.OnPropertyChanged);

            this.BancoExtranjero.DataBindings.Clear();
            this.BancoExtranjero.DataBindings.Add("Checked", this.cuentaBancaria, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Activo.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Beneficiario.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.BeneficiarioRFC.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.NumCuenta.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.NumCliente.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Clabe.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.CtaContable.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.BancoRFC.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.NumCliente.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Moneda.Enabled = !this.cuentaBancaria.ReadOnly;
            this.ClaveBanco.Enabled = !this.cuentaBancaria.ReadOnly;
            this.Sucursal.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Telefono.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Funcionario.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.Alias.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.SaldoInicial.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.NoCheque.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.DiaCorte.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.FechaApertura.ReadOnly = this.cuentaBancaria.ReadOnly;
            this.BancoExtranjero.ReadOnly = this.cuentaBancaria.ReadOnly;

            ClaveBanco.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
            BancoRFC.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
            Sucursal.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
            Telefono.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
            Funcionario.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
            BancoExtranjero.DataBindings.Add("Enabled", this.CheckBanco, "Checked", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Guardar() {
            this.cuentaBancaria = this.service.Save(this.cuentaBancaria);
        }

        private void Actualizar() {
            this.cuentaBancaria = this.service.GetCuenta(this.cuentaBancaria.IdCuenta);
        }

        /// <summary>
        /// para asegurarnos que digiten solo numeros
        /// </summary>
        private void TxbFolio_KeyPress(object sender, KeyPressEventArgs e) {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else {
                char keyChar = e.KeyChar;
                e.Handled = !(keyChar.ToString() == char.ConvertFromUtf32(8));
            }
        }

        private bool Validar() {
            this.errorCuentaBancaria.Clear();
            if (string.IsNullOrEmpty(this.cuentaBancaria.Beneficiario)) {
                this.errorCuentaBancaria.SetError(this.Beneficiario, "Es necesario un nombre para el beneficiario de la cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.Alias)) {
                this.errorCuentaBancaria.SetError(this.Alias, "Es necesario un alias para la cuenta.");
                return false;
            }

            if (this.cuentaBancaria.NumCuenta == null) {
                this.errorCuentaBancaria.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.NumCuenta.Trim())) {
                this.errorCuentaBancaria.SetError(this.NumCuenta, "Es necesario asociar un número de cuenta.");
                return false;
            }

            if (string.IsNullOrEmpty(this.cuentaBancaria.ClaveBanco) && this.CheckBanco.Checked) {
                this.errorCuentaBancaria.SetError(this.ClaveBanco, "Selecciona una clave de banco para la cuenta.");
                return false;
            }
            return true;
        }
    }
}
