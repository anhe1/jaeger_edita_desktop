﻿namespace Jaeger.Certifica.Entities {
    /// <summary>
    /// servicio del proveedor autorizado de certificacion
    /// </summary>
    public abstract class ProviderServiceConfiguration  {

        public ProviderServiceConfiguration() {
            this.Settings = new ProviderService();
            this.CerBase64 = string.Empty;
            this.KeyBase64 = string.Empty;
            this.PassKey = string.Empty;
        }

        /// <summary>
        /// certificado base 64
        /// </summary>
        public string CerBase64 { get; set; }

        /// <summary>
        /// llave key en base 64
        /// </summary>
        public string KeyBase64 { get; set; }

        /// <summary>
        /// password
        /// </summary>
        public string PassKey { get; set; }

        /// <summary>
        /// obtener o establecer configuracion
        /// </summary>
        public ProviderService Settings { get; set; }
    }
}
