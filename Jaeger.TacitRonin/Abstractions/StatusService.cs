﻿using System.ServiceModel;
using System;
using Jaeger.Certifica.com.SAT.QueryCFDI;
using System.Security.Cryptography.X509Certificates;

namespace Jaeger.Certifica.Entities {
    public abstract class StatusService : SATServices {
        private static BasicHttpBinding _myBinding;
        protected StatusService(string url) : base(url) {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
               delegate (object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };

            _myBinding = GetBinding();
        }

        internal abstract Acuse StatusRequest(string rfcEmisor, string rfcReceptor, decimal Total, string uuid);

        internal virtual Acuse RequestStatus(string rfcEmisor, string rfcReceptor, decimal total, string uuid) {
            var consulta = string.Format("?re={0}&rr={1}&tt={2}&id={3}", rfcEmisor.ToUpper().Replace("&", "&amp;"), rfcReceptor.ToUpper().Replace("&", "&amp;"), total.ToString("0.00"), uuid.ToUpper());
            
            Acuse acuse = null;

            for (int i = 0; i < 3; i++) {
                acuse = ConsultaCFDIService(consulta);
                if (acuse != null)
                    break;
            }
            return acuse;
        }
        public Acuse ConsultaCFDIService(string consulta) {
            try {
                using (var client = new ConsultaCFDIServiceClient(_myBinding, new EndpointAddress(this.Url))) {
                    return client.Consulta(consulta);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}
