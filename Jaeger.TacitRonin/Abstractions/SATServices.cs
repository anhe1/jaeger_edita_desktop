﻿using System.ServiceModel;
using System;

namespace Jaeger.Certifica.Entities {
    public abstract class SATServices {
        private string _url;
        public string Url {
            get { return _url; }
        }

        public SATServices(string url) {
            _url = url; ;
        }

        public BasicHttpBinding GetBinding() {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            var myBinding = new BasicHttpBinding {
                ReceiveTimeout = new TimeSpan(0, 1, 0),
                SendTimeout = new TimeSpan(0, 1, 0),
                OpenTimeout = new TimeSpan(0, 1, 0),
                CloseTimeout = new TimeSpan(0, 1, 0),
                MaxReceivedMessageSize = 2147483647,
                BypassProxyOnLocal = true,
                UseDefaultWebProxy = true
            };
            myBinding.Security.Mode = Url.StartsWith("https") ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None;
            return myBinding;
        }
    }
}
