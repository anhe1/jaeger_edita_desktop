﻿namespace Jaeger.Certifica.ValueObjects
{
    public enum ValidacionResultEnum {
        Error,
        EnEspera,
        Valido,
        NoValido
    }
}
