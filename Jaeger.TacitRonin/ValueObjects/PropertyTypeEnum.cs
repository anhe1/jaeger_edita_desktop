﻿namespace Jaeger.Certifica.ValueObjects
{
    public enum PropertyTypeEnum {
        None,
        Error,
        Warning,
        Attention,
        Information,
        Completed
    }
}
