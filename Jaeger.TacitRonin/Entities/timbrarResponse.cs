﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Certifica.Entities {
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", ConfigurationName = "com.SF.Timbre.Produccion.TimbradoPortType")]
    public interface TimbradoPortType {

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarPorNotaCredito", ReplyAction = "urn:cancelarPorNotaCreditoResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        cancelarPorNotaCreditoResponse cancelarPorNotaCredito(cancelarPorNotaCreditoRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarPorNotaCredito", ReplyAction = "urn:cancelarPorNotaCreditoResponse")]
        System.Threading.Tasks.Task<cancelarPorNotaCreditoResponse> cancelarPorNotaCreditoAsync(cancelarPorNotaCreditoRequest request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarViaCSDAlmacenado", ReplyAction = "urn:cancelarViaCSDAlmacenadoResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        cancelarViaCSDAlmacenadoResponse cancelarViaCSDAlmacenado(cancelarViaCSDAlmacenadoRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarViaCSDAlmacenado", ReplyAction = "urn:cancelarViaCSDAlmacenadoResponse")]
        System.Threading.Tasks.Task<cancelarViaCSDAlmacenadoResponse> cancelarViaCSDAlmacenadoAsync(cancelarViaCSDAlmacenadoRequest request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:enviarSolicitudCancelacion", ReplyAction = "urn:enviarSolicitudCancelacionResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        enviarSolicitudCancelacionResponse enviarSolicitudCancelacion(enviarSolicitudCancelacionRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:enviarSolicitudCancelacion", ReplyAction = "urn:enviarSolicitudCancelacionResponse")]
        System.Threading.Tasks.Task<enviarSolicitudCancelacionResponse> enviarSolicitudCancelacionAsync(enviarSolicitudCancelacionRequest request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelar", ReplyAction = "urn:cancelarResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        cancelarResponse cancelar(cancelarRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelar", ReplyAction = "urn:cancelarResponse")]
        System.Threading.Tasks.Task<cancelarResponse> cancelarAsync(cancelarRequest request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:timbrarBase64", ReplyAction = "urn:timbrarBase64Response")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        timbrarBase64Response timbrarBase64(timbrarBase64Request request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:timbrarBase64", ReplyAction = "urn:timbrarBase64Response")]
        System.Threading.Tasks.Task<timbrarBase64Response> timbrarBase64Async(timbrarBase64Request request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:timbrar", ReplyAction = "urn:timbrarResponse")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        timbrarResponse timbrar(timbrarRequest request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:timbrar", ReplyAction = "urn:timbrarResponse")]
        System.Threading.Tasks.Task<timbrarResponse> timbrarAsync(timbrarRequest request);

        // CODEGEN: El parámetro 'return' requiere información adicional de esquema que no se puede capturar con el modo de parámetros. El atributo específico es 'System.Xml.Serialization.XmlElementAttribute'.
        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarBase64", ReplyAction = "urn:cancelarBase64Response")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        cancelarBase64Response cancelarBase64(cancelarBase64Request request);

        [System.ServiceModel.OperationContractAttribute(Action = "urn:cancelarBase64", ReplyAction = "urn:cancelarBase64Response")]
        System.Threading.Tasks.Task<cancelarBase64Response> cancelarBase64Async(cancelarBase64Request request);
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com/xsd")]
    public partial class CFDIResultadoCertificacion : object, System.ComponentModel.INotifyPropertyChanged {

        private string cadenaOriginalField;

        private string certificadoSATField;

        private byte[] cfdiTimbradoField;

        private System.Nullable<System.DateTime> fechaTimbradoField;

        private bool fechaTimbradoFieldSpecified;

        private string mensajeField;

        private byte[] qrCodeField;

        private string selloSATField;

        private int statusField;

        private bool statusFieldSpecified;

        private string uuidField;

        private string versionTFDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 0)]
        public string cadenaOriginal {
            get {
                return this.cadenaOriginalField;
            }
            set {
                this.cadenaOriginalField = value;
                this.RaisePropertyChanged("cadenaOriginal");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 1)]
        public string certificadoSAT {
            get {
                return this.certificadoSATField;
            }
            set {
                this.certificadoSATField = value;
                this.RaisePropertyChanged("certificadoSAT");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true, Order = 2)]
        public byte[] cfdiTimbrado {
            get {
                return this.cfdiTimbradoField;
            }
            set {
                this.cfdiTimbradoField = value;
                this.RaisePropertyChanged("cfdiTimbrado");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 3)]
        public System.Nullable<System.DateTime> fechaTimbrado {
            get {
                return this.fechaTimbradoField;
            }
            set {
                this.fechaTimbradoField = value;
                this.RaisePropertyChanged("fechaTimbrado");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool fechaTimbradoSpecified {
            get {
                return this.fechaTimbradoFieldSpecified;
            }
            set {
                this.fechaTimbradoFieldSpecified = value;
                this.RaisePropertyChanged("fechaTimbradoSpecified");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 4)]
        public string mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("mensaje");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true, Order = 5)]
        public byte[] qrCode {
            get {
                return this.qrCodeField;
            }
            set {
                this.qrCodeField = value;
                this.RaisePropertyChanged("qrCode");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 6)]
        public string selloSAT {
            get {
                return this.selloSATField;
            }
            set {
                this.selloSATField = value;
                this.RaisePropertyChanged("selloSAT");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public int status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("status");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool statusSpecified {
            get {
                return this.statusFieldSpecified;
            }
            set {
                this.statusFieldSpecified = value;
                this.RaisePropertyChanged("statusSpecified");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 8)]
        public string uuid {
            get {
                return this.uuidField;
            }
            set {
                this.uuidField = value;
                this.RaisePropertyChanged("uuid");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 9)]
        public string versionTFD {
            get {
                return this.versionTFDField;
            }
            set {
                this.versionTFDField = value;
                this.RaisePropertyChanged("versionTFD");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com/xsd")]
    public partial class CFDICertificacion : object, System.ComponentModel.INotifyPropertyChanged {

        private string mensajeField;

        private CFDIResultadoCertificacion[] resultadosField;

        private int statusField;

        private bool statusFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 0)]
        public string mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("mensaje");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultados", IsNullable = true, Order = 1)]
        public CFDIResultadoCertificacion[] resultados {
            get {
                return this.resultadosField;
            }
            set {
                this.resultadosField = value;
                this.RaisePropertyChanged("resultados");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public int status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("status");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool statusSpecified {
            get {
                return this.statusFieldSpecified;
            }
            set {
                this.statusFieldSpecified = value;
                this.RaisePropertyChanged("statusSpecified");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com/xsd")]
    public partial class CFDIResultadoCancelacion : object, System.ComponentModel.INotifyPropertyChanged {

        private string mensajeField;

        private int statusField;

        private bool statusFieldSpecified;

        private string statusUUIDField;

        private string uuidField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 0)]
        public string mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("mensaje");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public int status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("status");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool statusSpecified {
            get {
                return this.statusFieldSpecified;
            }
            set {
                this.statusFieldSpecified = value;
                this.RaisePropertyChanged("statusSpecified");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 2)]
        public string statusUUID {
            get {
                return this.statusUUIDField;
            }
            set {
                this.statusUUIDField = value;
                this.RaisePropertyChanged("statusUUID");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 3)]
        public string uuid {
            get {
                return this.uuidField;
            }
            set {
                this.uuidField = value;
                this.RaisePropertyChanged("uuid");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com/xsd")]
    public partial class CFDICancelacion : object, System.ComponentModel.INotifyPropertyChanged {

        private string mensajeField;

        private CFDIResultadoCancelacion[] resultadosField;

        private int statusField;

        private bool statusFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, Order = 0)]
        public string mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("mensaje");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("resultados", IsNullable = true, Order = 1)]
        public CFDIResultadoCancelacion[] resultados {
            get {
                return this.resultadosField;
            }
            set {
                this.resultadosField = value;
                this.RaisePropertyChanged("resultados");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public int status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
                this.RaisePropertyChanged("status");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool statusSpecified {
            get {
                return this.statusFieldSpecified;
            }
            set {
                this.statusFieldSpecified = value;
                this.RaisePropertyChanged("statusSpecified");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarPorNotaCredito", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarPorNotaCreditoRequest {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string uuid;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] derCertCSD;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 4)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] derKeyCSD;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 5)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string contrasenaCSD;

        public cancelarPorNotaCreditoRequest() {
        }

        public cancelarPorNotaCreditoRequest(string usuario, string password, string uuid, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            this.usuario = usuario;
            this.password = password;
            this.uuid = uuid;
            this.derCertCSD = derCertCSD;
            this.derKeyCSD = derKeyCSD;
            this.contrasenaCSD = contrasenaCSD;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarPorNotaCreditoResponse", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarPorNotaCreditoResponse {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDIResultadoCertificacion @return;

        public cancelarPorNotaCreditoResponse() {
        }

        public cancelarPorNotaCreditoResponse(CFDIResultadoCertificacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarViaCSDAlmacenado", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarViaCSDAlmacenadoRequest {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string rfcEmisor;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute("uuids", IsNullable = true)]
        public string[] uuids;

        public cancelarViaCSDAlmacenadoRequest() {
        }

        public cancelarViaCSDAlmacenadoRequest(string usuario, string password, string rfcEmisor, string[] uuids) {
            this.usuario = usuario;
            this.password = password;
            this.rfcEmisor = rfcEmisor;
            this.uuids = uuids;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarViaCSDAlmacenadoResponse", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarViaCSDAlmacenadoResponse {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICancelacion @return;

        public cancelarViaCSDAlmacenadoResponse() {
        }

        public cancelarViaCSDAlmacenadoResponse(CFDICancelacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "enviarSolicitudCancelacion", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class enviarSolicitudCancelacionRequest {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] solicitudCancelacion;

        public enviarSolicitudCancelacionRequest() {
        }

        public enviarSolicitudCancelacionRequest(string usuario, string password, byte[] solicitudCancelacion) {
            this.usuario = usuario;
            this.password = password;
            this.solicitudCancelacion = solicitudCancelacion;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "enviarSolicitudCancelacionResponse", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class enviarSolicitudCancelacionResponse {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICancelacion @return;

        public enviarSolicitudCancelacionResponse() {
        }

        public enviarSolicitudCancelacionResponse(CFDICancelacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelar", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarRequest {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute("uuids", IsNullable = true)]
        public string[] uuids;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] derCertCSD;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 4)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] derKeyCSD;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 5)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string contrasenaCSD;

        public cancelarRequest() {
        }

        public cancelarRequest(string usuario, string password, string[] uuids, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            this.usuario = usuario;
            this.password = password;
            this.uuids = uuids;
            this.derCertCSD = derCertCSD;
            this.derKeyCSD = derKeyCSD;
            this.contrasenaCSD = contrasenaCSD;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarResponse", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarResponse {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICancelacion @return;

        public cancelarResponse() {
        }

        public cancelarResponse(CFDICancelacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "timbrarBase64", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class timbrarBase64Request {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string cfdiBase64;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        public bool zip;

        public timbrarBase64Request() {
        }

        public timbrarBase64Request(string usuario, string password, string cfdiBase64, bool zip) {
            this.usuario = usuario;
            this.password = password;
            this.cfdiBase64 = cfdiBase64;
            this.zip = zip;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "timbrarBase64Response", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class timbrarBase64Response {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICertificacion @return;

        public timbrarBase64Response() {
        }

        public timbrarBase64Response(CFDICertificacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "timbrar", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class timbrarRequest {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] cfdi;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        public bool zip;

        public timbrarRequest() {
        }

        public timbrarRequest(string usuario, string password, byte[] cfdi, bool zip) {
            this.usuario = usuario;
            this.password = password;
            this.cfdi = cfdi;
            this.zip = zip;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "timbrarResponse", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class timbrarResponse {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICertificacion @return;

        public timbrarResponse() {
        }

        public timbrarResponse(CFDICertificacion @return) {
            this.@return = @return;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarBase64", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarBase64Request {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string usuario;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string password;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute("uuids", IsNullable = true)]
        public string[] uuids;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 3)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string derCertCSDBase64;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 4)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string derKeyCSDBase64;

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 5)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string contrasenaCSD;

        public cancelarBase64Request() {
        }

        public cancelarBase64Request(string usuario, string password, string[] uuids, string derCertCSDBase64, string derKeyCSDBase64, string contrasenaCSD) {
            this.usuario = usuario;
            this.password = password;
            this.uuids = uuids;
            this.derCertCSDBase64 = derCertCSDBase64;
            this.derKeyCSDBase64 = derKeyCSDBase64;
            this.contrasenaCSD = contrasenaCSD;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName = "cancelarBase64Response", WrapperNamespace = "http://timbrado.ws.cfdi.solucionfactible.com", IsWrapped = true)]
    public partial class cancelarBase64Response {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://timbrado.ws.cfdi.solucionfactible.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CFDICancelacion @return;

        public cancelarBase64Response() {
        }

        public cancelarBase64Response(CFDICancelacion @return) {
            this.@return = @return;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface TimbradoPortTypeChannel : TimbradoPortType, System.ServiceModel.IClientChannel {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TimbradoPortTypeClient : System.ServiceModel.ClientBase<TimbradoPortType>, TimbradoPortType {

        public TimbradoPortTypeClient() {
        }

        public TimbradoPortTypeClient(string endpointConfigurationName) :
                base(endpointConfigurationName) {
        }

        public TimbradoPortTypeClient(string endpointConfigurationName, string remoteAddress) :
                base(endpointConfigurationName, remoteAddress) {
        }

        public TimbradoPortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
                base(endpointConfigurationName, remoteAddress) {
        }

        public TimbradoPortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress) {
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        cancelarPorNotaCreditoResponse TimbradoPortType.cancelarPorNotaCredito(cancelarPorNotaCreditoRequest request) {
            return base.Channel.cancelarPorNotaCredito(request);
        }

        public CFDIResultadoCertificacion cancelarPorNotaCredito(string usuario, string password, string uuid, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            cancelarPorNotaCreditoRequest inValue = new cancelarPorNotaCreditoRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuid = uuid;
            inValue.derCertCSD = derCertCSD;
            inValue.derKeyCSD = derKeyCSD;
            inValue.contrasenaCSD = contrasenaCSD;
            cancelarPorNotaCreditoResponse retVal = ((TimbradoPortType)(this)).cancelarPorNotaCredito(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<cancelarPorNotaCreditoResponse> TimbradoPortType.cancelarPorNotaCreditoAsync(cancelarPorNotaCreditoRequest request) {
            return base.Channel.cancelarPorNotaCreditoAsync(request);
        }

        public System.Threading.Tasks.Task<cancelarPorNotaCreditoResponse> cancelarPorNotaCreditoAsync(string usuario, string password, string uuid, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            cancelarPorNotaCreditoRequest inValue = new cancelarPorNotaCreditoRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuid = uuid;
            inValue.derCertCSD = derCertCSD;
            inValue.derKeyCSD = derKeyCSD;
            inValue.contrasenaCSD = contrasenaCSD;
            return ((TimbradoPortType)(this)).cancelarPorNotaCreditoAsync(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        cancelarViaCSDAlmacenadoResponse TimbradoPortType.cancelarViaCSDAlmacenado(cancelarViaCSDAlmacenadoRequest request) {
            return base.Channel.cancelarViaCSDAlmacenado(request);
        }

        public CFDICancelacion cancelarViaCSDAlmacenado(string usuario, string password, string rfcEmisor, string[] uuids) {
            cancelarViaCSDAlmacenadoRequest inValue = new cancelarViaCSDAlmacenadoRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.rfcEmisor = rfcEmisor;
            inValue.uuids = uuids;
            cancelarViaCSDAlmacenadoResponse retVal = ((TimbradoPortType)(this)).cancelarViaCSDAlmacenado(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<cancelarViaCSDAlmacenadoResponse> TimbradoPortType.cancelarViaCSDAlmacenadoAsync(cancelarViaCSDAlmacenadoRequest request) {
            return base.Channel.cancelarViaCSDAlmacenadoAsync(request);
        }

        public System.Threading.Tasks.Task<cancelarViaCSDAlmacenadoResponse> cancelarViaCSDAlmacenadoAsync(string usuario, string password, string rfcEmisor, string[] uuids) {
            cancelarViaCSDAlmacenadoRequest inValue = new cancelarViaCSDAlmacenadoRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.rfcEmisor = rfcEmisor;
            inValue.uuids = uuids;
            return ((TimbradoPortType)(this)).cancelarViaCSDAlmacenadoAsync(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        enviarSolicitudCancelacionResponse TimbradoPortType.enviarSolicitudCancelacion(enviarSolicitudCancelacionRequest request) {
            return base.Channel.enviarSolicitudCancelacion(request);
        }

        public CFDICancelacion enviarSolicitudCancelacion(string usuario, string password, byte[] solicitudCancelacion) {
            enviarSolicitudCancelacionRequest inValue = new enviarSolicitudCancelacionRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.solicitudCancelacion = solicitudCancelacion;
            enviarSolicitudCancelacionResponse retVal = ((TimbradoPortType)(this)).enviarSolicitudCancelacion(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<enviarSolicitudCancelacionResponse> TimbradoPortType.enviarSolicitudCancelacionAsync(enviarSolicitudCancelacionRequest request) {
            return base.Channel.enviarSolicitudCancelacionAsync(request);
        }

        public System.Threading.Tasks.Task<enviarSolicitudCancelacionResponse> enviarSolicitudCancelacionAsync(string usuario, string password, byte[] solicitudCancelacion) {
            enviarSolicitudCancelacionRequest inValue = new enviarSolicitudCancelacionRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.solicitudCancelacion = solicitudCancelacion;
            return ((TimbradoPortType)(this)).enviarSolicitudCancelacionAsync(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        cancelarResponse TimbradoPortType.cancelar(cancelarRequest request) {
            return base.Channel.cancelar(request);
        }

        public CFDICancelacion cancelar(string usuario, string password, string[] uuids, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            cancelarRequest inValue = new cancelarRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuids = uuids;
            inValue.derCertCSD = derCertCSD;
            inValue.derKeyCSD = derKeyCSD;
            inValue.contrasenaCSD = contrasenaCSD;
            cancelarResponse retVal = ((TimbradoPortType)(this)).cancelar(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<cancelarResponse> TimbradoPortType.cancelarAsync(cancelarRequest request) {
            return base.Channel.cancelarAsync(request);
        }

        public System.Threading.Tasks.Task<cancelarResponse> cancelarAsync(string usuario, string password, string[] uuids, byte[] derCertCSD, byte[] derKeyCSD, string contrasenaCSD) {
            cancelarRequest inValue = new cancelarRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuids = uuids;
            inValue.derCertCSD = derCertCSD;
            inValue.derKeyCSD = derKeyCSD;
            inValue.contrasenaCSD = contrasenaCSD;
            return ((TimbradoPortType)(this)).cancelarAsync(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        timbrarBase64Response TimbradoPortType.timbrarBase64(timbrarBase64Request request) {
            return base.Channel.timbrarBase64(request);
        }

        public CFDICertificacion timbrarBase64(string usuario, string password, string cfdiBase64, bool zip) {
            timbrarBase64Request inValue = new timbrarBase64Request();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.cfdiBase64 = cfdiBase64;
            inValue.zip = zip;
            timbrarBase64Response retVal = ((TimbradoPortType)(this)).timbrarBase64(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<timbrarBase64Response> TimbradoPortType.timbrarBase64Async(timbrarBase64Request request) {
            return base.Channel.timbrarBase64Async(request);
        }

        public System.Threading.Tasks.Task<timbrarBase64Response> timbrarBase64Async(string usuario, string password, string cfdiBase64, bool zip) {
            timbrarBase64Request inValue = new timbrarBase64Request();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.cfdiBase64 = cfdiBase64;
            inValue.zip = zip;
            return ((TimbradoPortType)(this)).timbrarBase64Async(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        timbrarResponse TimbradoPortType.timbrar(timbrarRequest request) {
            return base.Channel.timbrar(request);
        }

        public CFDICertificacion timbrar(string usuario, string password, byte[] cfdi, bool zip) {
            timbrarRequest inValue = new timbrarRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.cfdi = cfdi;
            inValue.zip = zip;
            timbrarResponse retVal = ((TimbradoPortType)(this)).timbrar(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<timbrarResponse> TimbradoPortType.timbrarAsync(timbrarRequest request) {
            return base.Channel.timbrarAsync(request);
        }

        public System.Threading.Tasks.Task<timbrarResponse> timbrarAsync(string usuario, string password, byte[] cfdi, bool zip) {
            timbrarRequest inValue = new timbrarRequest();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.cfdi = cfdi;
            inValue.zip = zip;
            return ((TimbradoPortType)(this)).timbrarAsync(inValue);
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        cancelarBase64Response TimbradoPortType.cancelarBase64(cancelarBase64Request request) {
            return base.Channel.cancelarBase64(request);
        }

        public CFDICancelacion cancelarBase64(string usuario, string password, string[] uuids, string derCertCSDBase64, string derKeyCSDBase64, string contrasenaCSD) {
            cancelarBase64Request inValue = new cancelarBase64Request();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuids = uuids;
            inValue.derCertCSDBase64 = derCertCSDBase64;
            inValue.derKeyCSDBase64 = derKeyCSDBase64;
            inValue.contrasenaCSD = contrasenaCSD;
            cancelarBase64Response retVal = ((TimbradoPortType)(this)).cancelarBase64(inValue);
            return retVal.@return;
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<cancelarBase64Response> TimbradoPortType.cancelarBase64Async(cancelarBase64Request request) {
            return base.Channel.cancelarBase64Async(request);
        }

        public System.Threading.Tasks.Task<cancelarBase64Response> cancelarBase64Async(string usuario, string password, string[] uuids, string derCertCSDBase64, string derKeyCSDBase64, string contrasenaCSD) {
            cancelarBase64Request inValue = new cancelarBase64Request();
            inValue.usuario = usuario;
            inValue.password = password;
            inValue.uuids = uuids;
            inValue.derCertCSDBase64 = derCertCSDBase64;
            inValue.derKeyCSDBase64 = derKeyCSDBase64;
            inValue.contrasenaCSD = contrasenaCSD;
            return ((TimbradoPortType)(this)).cancelarBase64Async(inValue);
        }
    }

}
