﻿// esta clase es para contener la información de la busqueda de un comprobante fiscal a traves del servicio de utilerias de Solución Factible
using System;

namespace Jaeger.Certifica.Entities {
    public class ResponseBuscar {
        private DateTime? fechaCancelacionField;
        private DateTime? fechaEmisionField;
        private DateTime? fechaTimbradoField;

        public ResponseBuscar() {
        }

        public string Folio { get; set; }

        public string Serie { get; set; }

        public string IdDocumento { get; set; }

        public string EmisorRFC { get; set; }

        public string EmisorNombre { get; set; }

        public string ReceptorRFC { get; set; }

        public string ReceptorNombre { get; set; }

        public bool Cancelado { get; set; }

        public DateTime? FechaCancelacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancelacionField > firstGoodDate)
                    return this.fechaCancelacionField;
                return null;
            }
            set {
                this.fechaCancelacionField = value;
            }
        }

        public DateTime? FechaEmision {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmisionField > firstGoodDate)
                    return this.fechaEmisionField;
                return null;
            }
            set {
                this.fechaEmisionField = value;
            }
        }

        public DateTime? FechaTimbrado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbradoField > firstGoodDate)
                    return this.fechaTimbradoField;
                return null;
            }
            set {
                this.fechaTimbradoField = value;
            }
        }

        public string SelloDigital { get; set; }

        public string SelloSAT { get; set; }

        public decimal Total { get; set; }
    }
}
