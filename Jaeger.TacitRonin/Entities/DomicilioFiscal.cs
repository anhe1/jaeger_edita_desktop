﻿namespace Jaeger.Certifica.Entities {
    public class DomicilioFiscal {
        public DomicilioFiscal() {
        }

        public string Calle {
            get; set;
        }

        public string Ciudad {
            get; set;
        }

        public string CodigoPais {
            get; set;
        }

        public string CodigoPostal {
            get; set;
        }

        public string Colonia {
            get; set;
        }

        public string Delegacion {
            get; set;
        }

        public string Estado {
            get; set;
        }

        public string Localidad {
            get; set;
        }

        public string NoExterior {
            get; set;
        }

        public string NoInterior {
            get; set;
        }

        public string Pais {
            get; set;
        }

        public string Referencia {
            get; set;
        }

        public override string ToString() {
            string direccion = string.Empty;

            if (!(string.IsNullOrEmpty(this.Calle)))
                direccion = string.Concat("Calle ", this.Calle);

            if (!(string.IsNullOrEmpty(this.NoExterior)))
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);

            if (!(string.IsNullOrEmpty(this.NoInterior)))
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);

            if (!(string.IsNullOrEmpty(this.Colonia)))
                direccion = string.Concat(direccion, " Col. ", this.Colonia);

            if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

            if (!(string.IsNullOrEmpty(this.Delegacion)))
                direccion = string.Concat(direccion, " ", this.Delegacion);

            if (!(string.IsNullOrEmpty(this.Estado)))
                direccion = string.Concat(direccion, ", ", this.Estado);

            if (!(string.IsNullOrEmpty(this.Referencia)))
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

            if (!(string.IsNullOrEmpty(this.Localidad)))
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

            return direccion;
        }
    }
}
