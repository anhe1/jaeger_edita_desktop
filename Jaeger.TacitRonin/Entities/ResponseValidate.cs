﻿using System;
using System.Collections.Generic;
using Jaeger.Certifica.ValueObjects;

namespace Jaeger.Certifica.Entities {
    public class ResponseValidate  {
        private List<PropertyValidate> validaciones;
        private List<PropertyValidateSingle> errores;

        /// <summary>
        /// contructor
        /// </summary>
        public ResponseValidate() {
            this.Fecha = DateTime.Now;
            this.FechaValidacion = DateTime.Now;
            this.validaciones = new List<PropertyValidate>();
            this.errores = new List<PropertyValidateSingle>();
        }

        public ValidacionResultEnum Valido {
            get; set;
        }

        public string ValidoText {
            get {
                return Enum.GetName(typeof(ValidacionResultEnum), this.Valido);
            }
            set {
                this.Valido = (ValidacionResultEnum)(Enum.Parse(typeof(ValidacionResultEnum), value));
            }
        }

        /// <summary>
        /// obtener o establecer el proveedor del servicio
        /// </summary>
        public string Provider {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la versión del estándar bajo el que se encuentra expresado el comprobante.
        /// </summary>
        public string Version {
            get; set;
        }

        public string Efecto {
            get; set;
        }

        /// <summary>
        /// obtener o establecer folio de control interno del contribuyente.
        /// </summary>
        public string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. 
        /// </summary>
        public string Serie {
            get; set;
        }

        public DateTime Fecha {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de validación
        /// </summary>
        public DateTime FechaValidacion {
            get; set;
        }

        public string IdDocumento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante
        /// </summary>
        public string Emisor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante
        /// </summary>
        public string EmisorRFC {
            get; set;
        }

        public string Receptor {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        public string ReceptorRFC {
            get; set;
        }

        public string TimeElapsed {
            get; set;
        }

        public decimal Total {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante
        /// </summary>
        public string ProofStatus {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el listado de validaciones del comprobante
        /// </summary>
        public List<PropertyValidate> Validations {
            get {
                return this.validaciones;
            }
            set {
                this.validaciones = value;
            }
        }

        public List<PropertyValidateSingle> Errors {
            get {
                return this.errores;
            }
            set {
                this.errores = value;
            }
        }
    }
}
