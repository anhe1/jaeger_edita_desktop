﻿using Jaeger.Certifica.ValueObjects;
using System.Collections.Generic;

namespace Jaeger.Certifica.Entities {
    public class PropertyValidate {
        private PropertyTypeEnum enumType;
        private string codigo;
        private string nombre;
        private string valor;
        private bool valido;
        private List<PropertyValidateSingle> propertyErrors;

        public PropertyValidate() {
            this.enumType = PropertyTypeEnum.Information;
            this.codigo = string.Empty;
            this.nombre = string.Empty;
            this.valor = string.Empty;
            this.valido = true;
            this.propertyErrors = new List<PropertyValidateSingle>();
        }

        public PropertyValidate(PropertyTypeEnum tipo, string codigo, string nombre, string valor, bool valido = true) {
            this.enumType = tipo;
            this.codigo = codigo;
            this.nombre = nombre;
            this.valor = valor;
            this.valido = valido;
            this.propertyErrors = new List<PropertyValidateSingle>();
        }

        public string Code {
            get {
                return this.codigo;
            }
            set {
                this.codigo = value;
            }
        }

        public List<PropertyValidateSingle> Errors {
            get {
                return this.propertyErrors;
            }
            set {
                this.propertyErrors = value;
            }
        }

        public string Name {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
            }
        }

        public PropertyTypeEnum Type {
            get {
                return this.enumType;
            }
            set {
                this.enumType = value;
            }
        }

        public bool Valid {
            get {
                return this.valido;
            }
            set {
                this.valido = value;
            }
        }

        public string Value {
            get {
                return this.valor;
            }
            set {
                this.valor = value;
            }
        }
    }
}
