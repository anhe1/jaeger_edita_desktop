﻿// develop: 061020160052
// purpose:
// * Mensajes de Respuesta
// Los mensajes de respuesta que arroja el servicio de consulta de CFDI´s incluyen la descripción del resultado de la operación que corresponden a la siguiente clasificación:
// Mensajes de Rechazo.
// N 601: La expresión impresa proporcionada no es válida.
// Este código de respuesta se presentará cuando la petición de validación no se haya respetado en el formato definido.
// N 602: Comprobante no encontrado.
// Este código de respuesta se presentará cuando el UUID del comprobante no se encuentre en la Base de Datos del SAT.
// Mensajes de Aceptación.
// S Comprobante obtenido satisfactoriamente.
// rev.: 201220171253: agregamos replace para escapar "&"
// rev.: 201801130147: agregamos try al generar url para la consulta, para regresar un error si no es posible generar la url

using System;

namespace Jaeger.Certifica.Entities {
    public class SatQueryResult {
        private long indexField;
        private string statusField;
        private string codigoField;
        private DateTime fechaField;
        private string keyField;

        public string Clave {
            get {
                return this.codigoField;
            }
            set {
                this.codigoField = value;
            }
        }

        public DateTime Fecha {
            get {
                return this.fechaField;
            }
            set {
                this.fechaField = value;
            }
        }

        public long Id {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }

        public string Key {
            get {
                return this.keyField;
            }
            set {
                this.keyField = value;
            }
        }

        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }

        public SatQueryResult() {
            this.indexField = 0;
            this.statusField = string.Empty;
            this.codigoField = string.Empty;
            this.fechaField = DateTime.Now;
            this.keyField = string.Empty;
            this.fechaField = DateTime.Now;
        }
    }
}
