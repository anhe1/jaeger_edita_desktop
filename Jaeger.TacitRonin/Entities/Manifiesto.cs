﻿namespace Jaeger.Certifica.Entities {
    public class Manifiesto {

        public Manifiesto() {
            this.Domicilio = new DomicilioFiscal();
        }

        public string RFC { get; set; }

        public string RazonSocial { get; set; }

        public string NombreComercial { get; set; }

        public string Correo { get; set; }

        public DomicilioFiscal Domicilio { get; set; }
    }
}
