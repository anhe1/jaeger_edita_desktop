﻿namespace Jaeger.Certifica.Entities {
    public class TimbresDisponibles {

        public string Mensaje { get; set; }

        public string Status { get; set; }

        public string Contratados { get; set; }

        public string Disponibles { get; set; }

        public string Usados { get; set; }
    }
}
