﻿using Jaeger.Certifica.ValueObjects;

namespace Jaeger.Certifica.Entities {
    public class PropertyValidateSingle {
        private PropertyTypeEnum objType;
        private string strCode;
        private string strName;
        private string strValue;

        /// <summary>
        /// Constructor
        /// </summary>
        public PropertyValidateSingle() {
            this.objType = PropertyTypeEnum.None;
            this.strCode = string.Empty;
            this.strName = string.Empty;
            this.strValue = string.Empty;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pType">Tipo</param>
        /// <param name="pCode">Codigo de Error</param>
        /// <param name="pName">Nombre de la propiedad</param>
        /// <param name="pValue">Valor de la propiedad</param>
        public PropertyValidateSingle(PropertyTypeEnum pType, string pCode, string pName, string pValue) {
            this.objType = pType;
            this.strCode = pCode;
            this.strName = pName;
            this.strValue = pValue;
        }

        public string Code {
            get {
                return this.strCode;
            }
            set {
                this.strCode = value;
            }
        }

        public string Name {
            get {
                return this.strName;
            }
            set {
                this.strName = value;
            }
        }

        public PropertyTypeEnum Type {
            get {
                return this.objType;
            }
            set {
                this.objType = value;
            }
        }

        public string Value {
            get {
                return this.strValue;
            }
            set {
                this.strValue = value;
            }
        }
    }
}
