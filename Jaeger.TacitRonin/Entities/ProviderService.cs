﻿using System.ComponentModel;
using Jaeger.Certifica.Contracts;

namespace Jaeger.Certifica.Entities {
    /// <summary>
    /// clase de configuracion para proveedor autorizado de certificacion
    /// </summary>
    public class ProviderService : IProviderService {
        private ServiceProviderEnum objProvider;
        private string user;
        private string password;
        private string rfc;
        private bool testing;
        private bool strict;

        public enum ServiceProviderEnum {
            Ninguno,
            Factorum,
            FiscoClic,
            SolucionFactible,
            Interno
        }

        /// <summary>
        /// constructor
        /// </summary>
        public ProviderService() {
            this.objProvider = ServiceProviderEnum.Ninguno;
            this.user = string.Empty;
            this.password = string.Empty;
            this.rfc = string.Empty;
            this.testing = true;
            this.strict = false;
        }

        [Category("Proveedor Autorizado Configuración")]
        [Description("")]
        [DisplayName("Contraseña")]
        [PasswordPropertyText(true)]
        public string Password {
            get {
                return this.password;
            }
            set {
                this.password = value;
            }
        }

        [Category("Proveedor Autorizado Configuración")]
        [Description("")]
        [DisplayName("Proveedor")]
        [TypeConverter(typeof(ServiceProviderEnum))]
        public ServiceProviderEnum Provider {
            get {
                return this.objProvider;
            }
            set {
                this.objProvider = value;
            }
        }

        [Category("Proveedor Autorizado Configuración")]
        [Description("")]
        [DisplayName("RFC")]
        public string RFC {
            get {
                return this.rfc;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.rfc = value.ToUpper();
                }
            }
        }

        [Category("Proveedor Autorizado Configuración")]
        [Description("")]
        [DisplayName("Estricto")]
        public bool Strinct {
            get {
                return this.strict;
            }
            set {
                this.strict = value;
            }
        }

        [Category("Proveedor Autorizado Configuración")]
        [Description("Establece el modo de timbrado, para pruebas solo debe deseleccionar la opción.")]
        [DisplayName("Modo productivo")]
        public bool Production {
            get {
                return this.testing;
            }
            set {
                this.testing = value;
            }
        }

        [Description("")]
        [DisplayName("Usuario")]
        public string User {
            get {
                return this.user;
            }
            set {
                this.user = value;
            }
        }
    }
}