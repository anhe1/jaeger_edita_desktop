﻿using Jaeger.Certifica.Entities;
using Jaeger.SAT.CFDI.Cancel;

namespace Jaeger.Certifica.Contracts {
    /// <summary>
    /// contrato del proveedor de certificacion
    /// </summary>
    public interface IProveedorCertificacion {

        #region propiedades

        /// <summary>
        /// obtener o establecer el numero de codigo de error
        /// </summary>
        int Codigo { get; set; }

        /// <summary>
        /// obtener o establecer el mensaje de error
        /// </summary>
        string Mensaje { get; set; }

        /// <summary>
        /// obtener o establecr el certificado del emisor en base64
        /// </summary>
        string CerBase64 { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        string KeyBase64 { get; set; }

        /// <summary>
        /// obtener o establecer la contraseña
        /// </summary>
        string PassKey { get; set; }

        /// <summary>
        /// configuracion del servicio
        /// </summary>
        ProviderService Settings { get; set; }

        #endregion

        #region metodos

        SAT.CFDI.V40.Comprobante Timbrar(string input);

        SAT.CRIP.V10.Retenciones TimbrarRetencion(string cfd);

        CancelaCFDResponse Cancelar(string uuid, string clave);

        ResponseValidate Validar(string inputXml);

        #endregion
    }
}
