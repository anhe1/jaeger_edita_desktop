﻿namespace Jaeger.Certifica.Contracts {
    public interface IProviderService {
        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre de usuario del servicio
        /// </summary>
        string User { get; set; }

        /// <summary>
        /// obtener o establecer password
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// obtener o establecer el modo productivo del servicio
        /// </summary>
        bool Production { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del servicio
        /// </summary>
        Entities.ProviderService.ServiceProviderEnum Provider { get; set; }

        /// <summary>
        /// obtener o establecer el modo estricto
        /// </summary>
        bool Strinct { get; set; }
    }
}
