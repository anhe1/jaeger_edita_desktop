﻿using System;
using Jaeger.Certifica.Helpers;
using Jaeger.Certifica.Contracts;
using Jaeger.Certifica.Entities;
using Jaeger.Certifica.ValueObjects;
using Jaeger.Certifica.com.FC.Service;
using System.Xml;
using Jaeger.CFDI.Cancel;
using Jaeger.CFDI.V33;

namespace Jaeger.Certifica.Services {
    public class FiscoClic : ProviderServiceConfiguration, IProveedorCertificacion {

        // credenciales para pruebas
        private readonly string usuarioTesting = "AAA111111ZZZ";
        private readonly string passwordTesting = "TeStInGfIsCoClIc2012Ws";

        public int Codigo {
            get; set;
        }

        public string Mensaje {
            get; set;
        }

        public new ProviderService Settings {
            get; set;
        }

        public FiscoClic(ProviderService conf) {
            this.Settings = conf;
        }

        #region timbrar comprobante

        public Comprobante Timbrar(string input) {
            if (this.Settings.Production == false) {
                return this.Timbrado_Testing(input);
            } else {
                return this.Timprado_Produccion(input);
            }
        }

        public Comprobante Timbrado_Testing(string inputXml) {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();

            try {
                // este servicio unicamente nos regresa el timbre fiscal
                stringResponse = interfaceCliente.timbraCFDIXMLTest(inputXml, this.usuarioTesting, this.passwordTesting);

                if (string.IsNullOrEmpty(stringResponse) == false) {
                    var timbre = CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital.Deserialize(stringResponse);

                    FileExtensions.WriteFileText(RouteManager.JaegerPath(PathsEnum.Temporal, string.Concat(timbre.UUID, ".xml")), stringResponse);
                    Comprobante comprobante = Comprobante.LoadXml(inputXml);
                    comprobante.Complemento = new ComprobanteComplemento();
                    comprobante.Complemento.TimbreFiscalDigital = timbre;
                    if (comprobante != null) {
                        comprobante.Save(RouteManager.JaegerPath(PathsEnum.Temporal, string.Concat(timbre.UUID, ".xml")));
                        this.Codigo = 0;
                        this.Mensaje = null;
                        return comprobante;
                    }
                }
            } catch (Exception ex) {
                this.Codigo = 0;
                this.Mensaje = ex.Message;
            }
            return null;
        }

        public Comprobante Timprado_Produccion(string inputXml) {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            try {
                // este servicio unicamente nos regresa el timbre fiscal
                this.Settings.User = "IPR981125PN9";
                this.Settings.Password = "aIm21dInGSMib59gaMmy";
                stringResponse = interfaceCliente.timbraCFDIXML(inputXml, this.Settings.User, this.Settings.Password);

                if (string.IsNullOrEmpty(stringResponse) == false) {
                    var timbre = CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital.Deserialize(stringResponse);
                    FileExtensions.WriteFileText(RouteManager.JaegerPath(PathsEnum.Comprobantes, string.Concat(timbre.UUID, ".xml")), stringResponse);
                    Comprobante comprobante = Comprobante.LoadXml(inputXml);
                    comprobante.Complemento = new ComprobanteComplemento();
                    comprobante.Complemento.TimbreFiscalDigital = timbre;
                    if (comprobante != null) {
                        comprobante.Save(RouteManager.JaegerPath(PathsEnum.Comprobantes, string.Concat(timbre.UUID, ".xml")));
                        this.Codigo = 0;
                        this.Mensaje = null;
                        return comprobante;
                    }
                }
            } catch (Exception ex) {
                this.Codigo = 0;
                this.Mensaje = ex.Message;
            }
            return null;
        }

        #endregion

        #region CFD Retenciones 1.0
        public CFDRetencion.V10.Retenciones TimbrarRetencion(string cfd) {
            return null;
        }
        #endregion

        #region cancelar comprobante

        public CancelaCFDResponse Cancelar(string uuid, string clave) {
            if (this.Settings.Production == false) {
                return this.Cancelar_Testing(uuid);
            } else {
                return this.Cancelar_Produccion(uuid);
            }
        }

        private CancelaCFDResponse Cancelar_Testing(string uuid) {
            string stringResponse = string.Empty;
            bool boolResponse = false;

            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            try {
                boolResponse = interfaceCliente.cancelaCFDITest(uuid, this.Settings.RFC, this.usuarioTesting, this.passwordTesting);
                if (boolResponse == true) {
                    stringResponse = interfaceCliente.cancelaCFDIAcuseTest(uuid, this.Settings.RFC, this.usuarioTesting, this.passwordTesting);
                    Console.WriteLine(stringResponse);
                }
            } catch (Exception) {

            }
            return null;
        }

        private CancelaCFDResponse Cancelar_Produccion(string uuid) {
            string stringResponse = string.Empty;
            cfdiServiceInterfaceClient interfaceCliente = new cfdiServiceInterfaceClient();
            interfaceCliente = null;
            Console.WriteLine(uuid + stringResponse + interfaceCliente.ToString());
            return null;
        }

        #endregion

        #region validacion

        public ResponseValidate Validar(string inputXml) {
            cfdiServiceInterfaceClient portClient = new cfdiServiceInterfaceClient();
            ResponseValidate validateResponse = new ResponseValidate();
            validateResponse.Provider = "";
            validateResponse.FechaValidacion = DateTime.Now;
            string stringResponse = string.Empty;
            DateTime inicio = DateTime.Now;

            if (this.Settings.Production == false) {
                try {
                    stringResponse = portClient.validaComprobanteTest(inputXml, this.usuarioTesting, this.passwordTesting, true);
                } catch (Exception) {

                }
            } else {
                try {
                    stringResponse = portClient.validaComprobante(inputXml, this.Settings.User, this.Settings.Password, true);
                } catch (Exception) {

                }
            }

            XmlDocument xmlResponse = new XmlDocument();
            try {
                xmlResponse.LoadXml(stringResponse);
                foreach (XmlNode item in xmlResponse["ResultadoValidacion"]) {
                    if (item.Name == "nodo") {
                        if (item.Attributes["tipo"].Value == "bandera") {
                            validateResponse.Validations.Add(new PropertyValidate { Code = "-1", Name = item.Attributes["nombre"].Value, Value = (item.Attributes["valor"].Value == "1" ? "Válido" : "No válido") });
                        } else if (item.Attributes["tipo"].Value == "datos") {
                            validateResponse.Validations.Add(new PropertyValidate { Code = "-1", Name = item.Attributes["nombre"].Value, Value = item.Attributes["valor"].Value });
                        }
                    } else if (item.Name == "banderaCorrecto") {
                        validateResponse.Validations.Add(new PropertyValidate { Code = "-1", Name = "Comprobante", Value = (item.InnerText.ToString() != "" ? true : false).ToString() });
                    } else if (item.Name == "validacionesRestantes") {

                    } else if (item.Name == "RFCEmisor") {

                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            DateTime fin = DateTime.Now;
            string totalMin = string.Empty;
            TimeSpan total = new TimeSpan(fin.Ticks - inicio.Ticks);
            totalMin = string.Concat(total.Hours.ToString("00"), ":", total.Minutes.ToString("00"), ":", total.Seconds.ToString("00"), ".", total.Milliseconds.ToString());
            validateResponse.TimeElapsed = totalMin;
            return validateResponse;
        }

        public CFDI.V40.Comprobante Timbrar40(string input) {
            throw new NotImplementedException();
        }
        #endregion
    }
}
