﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

namespace Jaeger.Certifica.Helpers {
    public class FileExtensions {
        public FileExtensions() {
        }

        public static bool DownloadFile(string address, string fileName) {
            bool downloadFile;
            try {
                WebClient webClient = new WebClient();

                Stream oFileStream = new FileStream(fileName, FileMode.Create);
                oFileStream.Write(webClient.DownloadData(address), 0, checked(webClient.DownloadData(address).Length));
                oFileStream.Flush();
                oFileStream.Close();

                downloadFile = true;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                downloadFile = false;
            }
            return downloadFile;
        }

        public static byte[] DownloadFile(string address) {
            byte[] downloadFile;
            try {
                downloadFile = (new WebClient()).DownloadData(address);
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                downloadFile = null;
            }
            return downloadFile;
        }

        public static string ReadFileB64(FileInfo archivo) {
            string base64String;
            try {
                if (!archivo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo.Name));
                }
                FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray, 0, length);
                fileStream.Close();
                base64String = Convert.ToBase64String(numArray);
            } catch (Exception exception) {
                throw exception;
            }
            return base64String;
        }

        public static string ReadFileB64(FileInfo archivo, bool insertLineBreaks) {
            string str;
            try {
                if (!archivo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo.Name));
                }
                FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray, 0, length);
                fileStream.Close();
                str = (!insertLineBreaks ? Convert.ToBase64String(numArray) : Convert.ToBase64String(numArray, Base64FormattingOptions.InsertLineBreaks).Trim());
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static string ReadFileB64(string archivo) {
            string base64String;
            FileInfo fileInfo = new FileInfo(archivo);
            try {
                if (!fileInfo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo));
                }
                FileStream fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray, 0, length);
                fileStream.Close();
                base64String = Convert.ToBase64String(numArray);
            } catch (Exception exception) {
                throw exception;
            }
            return base64String;
        }

        public static string ReadFileB64(string archivo, bool insertLineBreaks) {
            string str;
            FileInfo fileInfo = new FileInfo(archivo);
            try {
                if (!fileInfo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo));
                }
                FileStream fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray, 0, length);
                fileStream.Close();
                str = (!insertLineBreaks ? Convert.ToBase64String(numArray) : Convert.ToBase64String(numArray, Base64FormattingOptions.InsertLineBreaks).Trim());
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static byte[] ReadFileByte(string archivo) {
            byte[] numArray;
            FileInfo fileInfo = new FileInfo(archivo);
            try {
                if (!fileInfo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo));
                }
                FileStream fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray1 = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray1, 0, length);
                fileStream.Close();
                numArray = numArray1;
            } catch (Exception exception) {
                throw exception;
            }
            return numArray;
        }

        public static byte[] ReadFileByte(FileInfo archivo) {
            byte[] numArray;
            try {
                if (!archivo.Exists) {
                    throw new Exception(string.Concat("No existe el archivo: ", archivo.Name));
                }
                FileStream fileStream = new FileStream(archivo.FullName, FileMode.Open, FileAccess.Read);
                int length = checked((int)fileStream.Length);
                byte[] numArray1 = new byte[checked(checked(length - 1) + 1)];
                length = fileStream.Read(numArray1, 0, length);
                fileStream.Close();
                numArray = numArray1;
            } catch (Exception exception) {
                throw exception;
            }
            return numArray;
        }

        public static string ReadFileText(string archivo) {
            string str;
            string empty = string.Empty;
            if (!File.Exists(archivo)) {
                throw new Exception(string.Concat("No existe el archivo: ", archivo));
            }
            try {
                StreamReader streamReader = File.OpenText(archivo);
                empty = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
                str = empty;
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static string ReadFileText(FileInfo archivo) {
            string str;
            string empty = string.Empty;
            if (!archivo.Exists) {
                throw new Exception(string.Concat("No existe el archivo: ", archivo.Name));
            }
            try {
                StreamReader streamReader = archivo.OpenText();
                empty = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
                str = empty;
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static string ReadFileText(string archivo, int iLimite) {
            string str;
            char[] chrArray = new char[checked(checked(iLimite - 1) + 1)];
            string empty = string.Empty;
            if (!File.Exists(archivo)) {
                throw new Exception(string.Concat("No existe el archivo: ", archivo));
            }
            try {
                StreamReader streamReader = File.OpenText(archivo);
                streamReader.ReadBlock(chrArray, 0, iLimite);
                char[] chrArray1 = chrArray;
                for (int num = 0; num < checked(chrArray1.Length); num = checked(num + 1)) {
                    empty = string.Concat(empty, chrArray1[num]);
                }
                streamReader.Close();
                streamReader.Dispose();
                streamReader = null;
                str = empty;
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static object SystemOpenFile(string filename) {
            object systemOpenFile;
            try {
                ProcessStartInfo processStartInfo = new ProcessStartInfo() {
                    FileName = filename,
                    UseShellExecute = true
                };
                Process.Start(processStartInfo);
                systemOpenFile = true;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                systemOpenFile = false;
            }
            return systemOpenFile;
        }

        public static void WriteFileByte(byte[] data, string archivo) {
            try {
                using (FileStream fileStream = File.Create(archivo)) {
                    fileStream.Write(data, 0, checked(data.Length));
                    fileStream.Close();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public static void WriteFileByte(byte[] data, FileInfo archivo) {
            try {
                using (FileStream fileStream = archivo.OpenWrite()) {
                    fileStream.Write(data, 0, checked(data.Length));
                    fileStream.Close();
                    fileStream.Dispose();
                }
            } catch (Exception exception) {
                throw exception;
            }
        }

        public static void WriteFileText(string archivo, string texto) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, false)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(string archivo, string texto, bool append) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, append)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(string archivo, string texto, bool append, Encoding encoding) {
            using (StreamWriter streamWriter = new StreamWriter(archivo, append, encoding)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        public static void WriteFileText(Queue queTuplas, string nombre, bool agregarDatos) {
            IEnumerator enumerator = null;
            try {
                using (StreamWriter streamWriter = new StreamWriter(nombre, agregarDatos)) {
                    try {
                        enumerator = queTuplas.GetEnumerator();
                        while (enumerator.MoveNext()) {
                            string queTupla = enumerator.Current.ToString();
                            streamWriter.Write(string.Concat(queTupla, "\r\n"));
                        }
                    } finally {
                        if (enumerator is IDisposable) {
                            (enumerator as IDisposable).Dispose();
                        }
                    }
                    streamWriter.Dispose();
                }
            } catch (Exception exception) {
                throw exception;
            }
        }

        public static byte[] FromBase64(string base64Encoded) {
            byte[] numArray;
            if ((base64Encoded == null ? true : base64Encoded.Length == 0)) {
                numArray = null;
            } else {
                try {
                    numArray = Convert.FromBase64String(base64Encoded);
                } catch (FormatException fex) {
                    throw new FormatException(string.Concat("The provided string does not appear to be Base64 encoded:", Environment.NewLine, base64Encoded, Environment.NewLine), fex);
                }
            }
            return numArray;
        }
    }
}