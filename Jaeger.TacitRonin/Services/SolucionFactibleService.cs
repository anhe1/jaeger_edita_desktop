﻿// develop: 310120180127
// purpose: clase para certificacion y cancelacion con el proveedor SolucionFactible
// Cancelación:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//    601 - Error de autenticación,el nombre de usuario o contraseña son incorrectos.
//    602 - La cuenta de usuario se encuentra bloqueada.
//    603 - La contraseña de la cuenta ha expirado.
//    604 - Se ha superado el número máximo permitido de intentos fallidos de autenticación.
//    605 - El usuario se encuentra inactivo
//    611 - No se han proporcionado UUIDs a cancelar.
//    620 - Permiso denegado.
//    1701 - La llave privada y la llave pública del CSD no corresponden.
//    1702 - La llave privada de la contraseña es incorrecta.
//    1703 - La llave privada no cumple con la estructura esperada.
//    1704 - La llave Privada no es una llave RSA.
//    1710 - La estructura del certificado no cumple con la estructura X509 esperada.
//    1711 - El certificado no esá vigente todavía.
//    1712 - El certificado ha expirado.
//    1713 - La llave pública contenida en el certificado no es una llave RSA.
//    1803 - El dato no es un UUID válido.
//
// Códigos de status de cancelación de CFDI:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//
// Códigos de respuesta del SAT para la cancelación de CFDI:
//    201 - El folio se ha cancelado con éxito.
//    202 - El CFDI ya había sido cancelado previamente.
//    203 - UUID no corresponde al emisor.
//    204 - El CFDI no aplica para cancelación.
//    205 - El UUID no existe o no ha sido procesado por el SAT.
//    402 - El Contribuyente no se encuentra el la LCO o la validez de obligaciones se reporta como negativa.
// Nota: Sólo se puede tener certeza de que un CFDI fue cancelado cuando el statusUUID es 201 (Se ha cancelado el CFDI) o 202 (El CFDI ya había sido cancelado previamente).

using System;
using System.Text;
using System.Xml;
using Jaeger.Certifica.Entities;
using Jaeger.Certifica.Contracts;
using Jaeger.SAT.CFDI.Cancel;
using System.ServiceModel;
using Jaeger.Certifica.Helpers;
using Jaeger.Certifica.ValueObjects;

namespace Jaeger.Certifica.Services {
    public class SolucionFactibleService : ProviderServiceConfiguration, IProveedorCertificacion {
        #region declaraciones
        private int _status;
        private string _mensaje;
        private readonly string url_Timbrado_Testing = "http://testing.solucionfactible.com/ws/services/Timbrado.TimbradoHttpsSoap11Endpoint/";
        private readonly string url_Timbrado_Produccion = "https://solucionfactible.com/ws/services/Timbrado.TimbradoHttpsSoap11Endpoint/";
        private string url_Timbrado_Service = string.Empty;
        private readonly string url_Cancelar_Testing = "https://testing.solucionfactible.com/ws/services/Cancelacion.CancelacionHttpsSoap11Endpoint/";
        private readonly string url_Cancelar_Produccion = "https://solucionfactible.com/ws/services/Cancelacion.CancelacionHttpsSoap11Endpoint/";
        private string url_Cancelar_Service = string.Empty;
        private readonly string usuarioTesting = "testing@solucionfactible.com";
        private readonly string passwordTesting = "timbrado.SF.16672";
        #endregion

        public SolucionFactibleService(ProviderService conf) {
            this.Settings = conf;
            this.Initialize();
        }

        public int Codigo {
        get { return _status; }
            set { _status = value; }
        }

        public string Mensaje {
            get { return _mensaje; }
            set { _mensaje = value; }
        }

        /// <summary>
        /// configuracion del sevicio
        /// </summary>
        protected virtual BasicHttpBinding GetBinding(string url) {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            var _responseBinding = new BasicHttpBinding {
                CloseTimeout = new TimeSpan(0, 1, 0),
                OpenTimeout = new TimeSpan(0, 1, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                SendTimeout = new TimeSpan(0, 1, 0),
                AllowCookies = false,
                BypassProxyOnLocal = false,
                HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                MaxBufferSize = 65536,
                MaxBufferPoolSize = 524288,
                MaxReceivedMessageSize = 65536,
                MessageEncoding = WSMessageEncoding.Text,
                TextEncoding = Encoding.UTF8,
                TransferMode = TransferMode.Buffered,
                UseDefaultWebProxy = true
            };
            _responseBinding.Security.Mode = url.StartsWith("https") ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None;
            return _responseBinding;
        }

        #region CFDI
        public SAT.CFDI.V40.Comprobante Timbrar(string input) {
            var _reponse = this.Timbrar40_Testing(input);
            if (_reponse != "") {
                var _cfdi = SAT.CFDI.V40.Comprobante.LoadXml(_reponse);
                if (_cfdi != null) {
                    return _cfdi;
                }
            }
            return null;
        }
        #endregion

        #region CFD Retenciones 
        public SAT.CRIP.V10.Retenciones TimbrarRetencion(string cfd) {
            var _reponse = this.Timbrar40_Testing(cfd);
            if (_reponse != "") {
                var _cfdi = SAT.CRIP.V10.Retenciones.LoadXml(_reponse);
                if (_cfdi != null) {
                    _cfdi.OriginalXmlString = _reponse;
                    return _cfdi;
                }
            }
            return null;
        }

        public SAT.CRIP.V20.Retenciones TimbrarRetencionV20(string cfd) {
            var _reponse = this.Timbrar40_Testing(cfd);
            if (_reponse != "") {
                var _cfdi = SAT.CRIP.V20.Retenciones.LoadXml(_reponse);
                if (_cfdi != null) {
                    _cfdi.OriginalXmlString = _reponse;
                    return _cfdi;
                }
            }
            return null;
        }
        #endregion

        public CancelaCFDResponse Cancelar(string uuid, string clave) {
            byte[] byteCer = Convert.FromBase64String(this.CerBase64);
            byte[] byteKey = Convert.FromBase64String(this.KeyBase64);
            return this.Cancelar(this.PassKey, byteCer, byteKey, new string[] { uuid + "|" + clave + "|"});
            throw new NotImplementedException();
        }

        public ResponseValidate Validar(string inputXml) {
            throw new NotImplementedException();
        }

        private void Initialize() {
            if (this.Settings.Production == false) {
                this.url_Timbrado_Service = this.url_Timbrado_Testing;
                this.url_Cancelar_Service = this.url_Cancelar_Testing;
                this.Settings.User = this.usuarioTesting;
                this.Settings.Password = this.passwordTesting;
            } else {
                this.url_Timbrado_Service = this.url_Timbrado_Produccion;
                this.url_Cancelar_Service = this.url_Cancelar_Produccion;
            }
        }

        private string Timbrar40_Testing(string cfdi) {
            /// Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            com.SF.Timbre.Testing.TimbradoPortType portCliente = new com.SF.Timbre.Testing.TimbradoPortTypeClient(this.GetBinding(this.url_Timbrado_Service), new EndpointAddress(this.url_Timbrado_Service));

            try {
                byte[] _bytes = Encoding.UTF8.GetBytes(cfdi);
                var _timbrarRequest = new com.SF.Timbre.Testing.timbrarRequest {
                    cfdi = _bytes,
                    usuario = this.Settings.User,
                    password = this.Settings.Password
                };

                var _response = portCliente.timbrar(_timbrarRequest);

                if (_response.@return.status == 200) {
                    var resultado = _response.@return;
                    if (resultado.resultados != null) {
                        if (resultado.resultados[0].cfdiTimbrado != null) {
                            var _xmlResult = new XmlDocument();
                            _xmlResult.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            _xmlResult.Save(RouteManager.JaegerPath(PathsEnum.Comprobantes, string.Concat(resultado.resultados[0].uuid, ".xml")));
                            this.Codigo = 0;
                            this.Mensaje = _response.@return.mensaje;
                            return Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado);
                        } else {
                            if (resultado.resultados[0].mensaje != null) {
                                this.Mensaje = resultado.resultados[0].mensaje;
                            } else {
                                this.Mensaje = _response.@return.mensaje;
                            }
                            this.Codigo = _response.@return.status;
                            LogErrorService.LogWrite("----------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
                                this.Mensaje + "\r\n" +
                                cfdi + "\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        }
                    }
                } else {
                    this.Codigo = _response.@return.status;
                    this.Mensaje = _response.@return.mensaje;
                    LogErrorService.LogWrite(_response.@return.mensaje);
                }
            } catch (Exception ex) {
                this.Codigo = -1;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
            }
            return string.Empty;
        }

        private string Timbrar40_Produccion(string cfdi) {
            /// Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            com.SF.Timbre.Produccion.TimbradoPortType portCliente = new com.SF.Timbre.Produccion.TimbradoPortTypeClient(this.GetBinding(this.url_Timbrado_Service), new EndpointAddress(this.url_Timbrado_Service));

            try {
                byte[] _bytes = Encoding.UTF8.GetBytes(cfdi);
                var _timbrarRequest = new com.SF.Timbre.Produccion.timbrarRequest {
                    cfdi = _bytes,
                    usuario = this.Settings.User,
                    password = this.Settings.Password
                };

                var _response = portCliente.timbrar(_timbrarRequest);

                if (_response.@return.status == 200) {
                    var resultado = _response.@return;
                    if (resultado.resultados != null) {
                        if (resultado.resultados[0].cfdiTimbrado != null) {
                            var _xmlResult = new XmlDocument();
                            _xmlResult.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            _xmlResult.Save(RouteManager.JaegerPath(PathsEnum.Comprobantes, string.Concat(resultado.resultados[0].uuid, ".xml")));
                            this.Codigo = 0;
                            this.Mensaje = _response.@return.mensaje;
                            return Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado);
                        } else {
                            if (resultado.resultados[0].mensaje != null) {
                                this.Mensaje = resultado.resultados[0].mensaje;
                            } else {
                                this.Mensaje = _response.@return.mensaje;
                            }
                            this.Codigo = _response.@return.status;
                            LogErrorService.LogWrite(this.Mensaje);
                        }
                    }
                } else {
                    this.Codigo = _response.@return.status;
                    this.Mensaje = _response.@return.mensaje;
                    LogErrorService.LogWrite(_response.@return.mensaje);
                }
            } catch (Exception ex) {
                this.Codigo = -1;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
                LogErrorService.LogWrite(ex.Message);
            }
            return string.Empty;
        }

        private CancelaCFDResponse Cancelar(string contrasenaCSD, byte[] derCertCSD, byte[] derKeyCSD, string[] uuids) {
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            //El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado
            System.Net.ServicePointManager.Expect100Continue = false;
            var portCliente = new com.SF.Cancelacion.Produccion.CancelacionPortTypeClient(this.GetBinding(this.url_Cancelar_Service), new EndpointAddress(this.url_Cancelar_Service));
            try {
                com.SF.Cancelacion.Produccion.KeyValue[] objProperties = new com.SF.Cancelacion.Produccion.KeyValue[0];
                com.SF.Cancelacion.Produccion.StatusCancelacionResponse objResponse = new com.SF.Cancelacion.Produccion.StatusCancelacionResponse();
                Envelope objEnvelope = new Envelope();
                string stringXml;
                CancelaCFDResponse objAcuse = new CancelaCFDResponse();
                

                objResponse = portCliente.cancelar(this.Settings.User,
                                                      this.Settings.Password,
                                                      this.Settings.RFC,
                                                      uuids,
                                                      derCertCSD,
                                                      derKeyCSD,
                                                      contrasenaCSD,
                                                      objProperties);

                if (objResponse.status == 200) {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    FileExtensions.WriteFileByte(objResponse.acuseSat, RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Acuse-", "uuid", ".xml")));
                    objEnvelope = HelperXmlSerializer.XmlDeserializarStringXml<Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                    return objAcuse;
                } else if (objResponse.status == 201) {
                    this.Codigo = 201;
                    this.Mensaje = objResponse.mensaje;
                } else if (objResponse.status == 211) {
                    // La cancelación está en proceso
                    this.Codigo = 211;
                    this.Mensaje = objResponse.mensaje;
                    FileExtensions.WriteFileText(objResponse.ToString(), RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Estado-211-", "uuid", ".xml")));
                    return null;
                } else if (objResponse.status == 601) {
                    this.Codigo = 601;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                } else {
                    if (objResponse != null) {
                        this.Codigo = objResponse.status;
                        this.Mensaje = objResponse.mensaje;
                        FileExtensions.WriteFileText(objResponse.ToString(), RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Acuse-error-", "uuid", ".xml")));
                    }
                    return null;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            this.Codigo = 201;
            this.Mensaje = "Este procedimiento no se encuentra en modo productivo";
            return null;
        }
    }
}
