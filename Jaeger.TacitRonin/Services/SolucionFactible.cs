﻿// develop: 310120180127
// purpose: clase para certificacion y cancelacion con el proveedor SolucionFactible
// Cancelación:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//    601 - Error de autenticación,el nombre de usuario o contraseña son incorrectos.
//    602 - La cuenta de usuario se encuentra bloqueada.
//    603 - La contraseña de la cuenta ha expirado.
//    604 - Se ha superado el número máximo permitido de intentos fallidos de autenticación.
//    605 - El usuario se encuentra inactivo
//    611 - No se han proporcionado UUIDs a cancelar.
//    620 - Permiso denegado.
//    1701 - La llave privada y la llave pública del CSD no corresponden.
//    1702 - La llave privada de la contraseña es incorrecta.
//    1703 - La llave privada no cumple con la estructura esperada.
//    1704 - La llave Privada no es una llave RSA.
//    1710 - La estructura del certificado no cumple con la estructura X509 esperada.
//    1711 - El certificado no esá vigente todavía.
//    1712 - El certificado ha expirado.
//    1713 - La llave pública contenida en el certificado no es una llave RSA.
//    1803 - El dato no es un UUID válido.
//
// Códigos de status de cancelación de CFDI:
//    200 - El proceso de cancelación se ha completado correctamente.
//    500 - Han ocurrido errores que no han permitido completar el proceso de cancelación.
//
// Códigos de respuesta del SAT para la cancelación de CFDI:
//    201 - El folio se ha cancelado con éxito.
//    202 - El CFDI ya había sido cancelado previamente.
//    203 - UUID no corresponde al emisor.
//    204 - El CFDI no aplica para cancelación.
//    205 - El UUID no existe o no ha sido procesado por el SAT.
//    402 - El Contribuyente no se encuentra el la LCO o la validez de obligaciones se reporta como negativa.
// Nota: Sólo se puede tener certeza de que un CFDI fue cancelado cuando el statusUUID es 201 (Se ha cancelado el CFDI) o 202 (El CFDI ya había sido cancelado previamente).

using System;
using System.Text;
using System.Xml;
using Jaeger.Certifica.Entities;
using Jaeger.Certifica.Contracts;
using Jaeger.Certifica.Helpers;
using Jaeger.Certifica.ValueObjects;
using Jaeger.CFDI.Cancel;

namespace Jaeger.Certifica.Services {
    /// <summary>
    /// proveedor autorizado de certificacion de comprobantes fiscales por internet (CFDI)
    /// </summary>
    public class SolucionFactible : ProviderServiceConfiguration, IProveedorCertificacion {

        /// credenciales para pruebas
        private readonly string usuarioTesting = "testing@solucionfactible.com";
        private readonly string passwordTesting = "timbrado.SF.16672";

        public SolucionFactible(ProviderService conf) {
            this.Settings = conf;
        }

        public int Codigo { get; set; }

        public string Mensaje { get; set; }

        public new IProviderService Settings { get; set; }

        public CancelaCFDResponse Cancelar(string uuid, string clave) {
            if (this.Settings.Production == false)
                return this.CancelarTesting(uuid, clave);
            else
                return this.CancelarProduccion(uuid, clave);
        }


        #region v40
        public CFDI.V40.Comprobante Timbrar(string inputXml) {
            if (this.Settings.Production == false)
                return this.TimbradoTesting40(inputXml);
            else {
                return this.TimbradoProduccion40(inputXml);
            }
        }

        private CFDI.V40.Comprobante TimbradoTesting40(string cfdv33b64) {
            string testEndpoint = "TimbradoEndpoint_TESTING";
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            com.SF.Timbre.Testing.TimbradoPortType portCliente = null;
            portCliente = new com.SF.Timbre.Testing.TimbradoPortTypeClient(testEndpoint);

            try {
                byte[] bytes = Encoding.UTF8.GetBytes(cfdv33b64);
                com.SF.Timbre.Testing.timbrarRequest request = new com.SF.Timbre.Testing.timbrarRequest {
                    cfdi = bytes,
                    usuario = this.usuarioTesting,
                    password = this.passwordTesting
                };
                // enviamos datos para timbrar
                com.SF.Timbre.Testing.timbrarResponse response = portCliente.timbrar(request);

                if (response.@return.status == 200) {
                    com.SF.Timbre.Testing.CFDICertificacion resultado = response.@return;
                    if (resultado.resultados != null) {
                        if (resultado.resultados[0].cfdiTimbrado != null) {
                            XmlDocument xmlResultante = new XmlDocument();
                            xmlResultante.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            xmlResultante.Save(RouteManager.JaegerPath(PathsEnum.Temporal, string.Concat(resultado.resultados[0].uuid, "_temporal.xml")));
                            var resultComprobante = new CFDI.V40.Comprobante();
                            resultComprobante = CFDI.V40.Comprobante.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            return resultComprobante;
                        } else {
                            this.Codigo = response.@return.status;
                            this.Mensaje = response.@return.resultados[0].mensaje;
                        }
                    } else {
                        this.Codigo = response.@return.status;
                        this.Mensaje = response.@return.mensaje;
                    }
                }
            } catch (Exception ex) {
                this.Codigo = -1;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        private CFDI.V40.Comprobante TimbradoProduccion40(string cfdv33b64) {
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            string prodEndpoint = "TimbradoEndpoint_PRODUCCION";
            com.SF.Timbre.Produccion.TimbradoPortType portCliente = null;
            portCliente = new com.SF.Timbre.Produccion.TimbradoPortTypeClient(prodEndpoint);

            try {
                byte[] bytes = Encoding.UTF8.GetBytes(cfdv33b64);
                com.SF.Timbre.Produccion.timbrarRequest request = new com.SF.Timbre.Produccion.timbrarRequest();
                request.cfdi = bytes;
                request.usuario = this.Settings.User;
                request.password = this.Settings.Password;
                // se envian datos para timbrar
                com.SF.Timbre.Produccion.timbrarResponse response = portCliente.timbrar(request);

                if (response.@return.status == 200) {
                    com.SF.Timbre.Produccion.CFDICertificacion resultado = response.@return;
                    if (resultado.resultados != null) {
                        if (resultado.resultados[0].cfdiTimbrado != null) {
                            XmlDocument xmlResultante = new XmlDocument();
                            xmlResultante.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));

                            xmlResultante.Save(RouteManager.JaegerPath(PathsEnum.Comprobantes, string.Concat(resultado.resultados[0].uuid, ".xml")));
                            var resultComprobante = CFDI.V40.Comprobante.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            return resultComprobante;
                        } else {
                            this.Codigo = response.@return.status;
                            this.Mensaje = response.@return.resultados[0].mensaje;
                        }
                    } else {
                        this.Codigo = response.@return.status;
                        this.Mensaje = response.@return.mensaje;
                    }
                }
            } catch (Exception ex) {
                this.Codigo = 0;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #endregion

        #region CFD Retenciones 1.0
        public CFDRetencion.V10.Retenciones TimbrarRetencion(string cfd) {
            return null;
        }
        #endregion

        public CancelaCFDResponse CancelarProduccion(string uuid, string clave) {
            // Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            // El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            // en este ejemplo es: com.sf.ws.Timbrado
            com.SF.Cancelacion.Produccion.CancelacionPortTypeClient objCancelacion = null;
            CancelaCFDResponse objAcuse = new CancelaCFDResponse();
            objCancelacion = new com.SF.Cancelacion.Produccion.CancelacionPortTypeClient("Cancelacion_Produccion");

            try {
                com.SF.Cancelacion.Produccion.KeyValue[] objProperties = new com.SF.Cancelacion.Produccion.KeyValue[0];
                com.SF.Cancelacion.Produccion.StatusCancelacionResponse objResponse = new com.SF.Cancelacion.Produccion.StatusCancelacionResponse();
                Envelope objEnvelope = new Envelope();
                string stringXml;
                byte[] byteCer = Convert.FromBase64String(this.CerBase64);
                byte[] byteKey = Convert.FromBase64String(this.KeyBase64);

                objResponse = objCancelacion.cancelar(this.Settings.User,
                                                      this.Settings.Password,
                                                      this.Settings.RFC,
                                                      new string[] { uuid },
                                                      byteCer,
                                                      byteKey,
                                                      this.PassKey,
                                                      objProperties);
                if (objResponse.status == 200) {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    FileExtensions.WriteFileByte(objResponse.acuseSat, RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Acuse-", uuid, ".xml")));
                    objEnvelope = HelperXmlSerializer.XmlDeserializarStringXml<Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                    return objAcuse;
                }
                else if (objResponse.status == 201) {
                    this.Codigo = 201;
                    this.Mensaje = objResponse.mensaje;
                }
                else if (objResponse.status == 211) {
                    // La cancelación está en proceso
                    this.Codigo = 211;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                }
                else if (objResponse.status == 601) {
                    this.Codigo = 601;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                }
                else {
                    if (objResponse != null) {
                        this.Codigo = objResponse.status;
                        this.Mensaje = objResponse.mensaje;
                        FileExtensions.WriteFileText(objResponse.ToString(), RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Acuse-error-", uuid, ".xml")));
                    }
                    return null;
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            this.Codigo = 201;
            this.Mensaje = "Este procedimiento no se encuentra en modo productivo";
            return null;
        }

        public CancelaCFDResponse CancelarTesting(string uuid, string clave) {
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            //El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado
            com.SF.Cancelacion.Testing.CancelacionPortTypeClient objCancelacion = null;
            CancelaCFDResponse objAcuse = new CancelaCFDResponse();
            objCancelacion = new com.SF.Cancelacion.Testing.CancelacionPortTypeClient("Cancelacion_Testing");

            try {
                com.SF.Cancelacion.Testing.KeyValue[] objProperties = new com.SF.Cancelacion.Testing.KeyValue[0];
                com.SF.Cancelacion.Testing.StatusCancelacionResponse objResponse = new com.SF.Cancelacion.Testing.StatusCancelacionResponse();

                Envelope objEnvelope = new Envelope();
                string stringXml;
                byte[] byteCer = UTF8Encoding.UTF8.GetBytes(this.CerBase64);
                byte[] byteKey = UTF8Encoding.UTF8.GetBytes(this.KeyBase64);

                objResponse = objCancelacion.cancelar(this.usuarioTesting,
                                                      this.passwordTesting,
                                                      this.Settings.RFC,
                                                      new string[] { uuid + "|" + clave + "|" },
                                                      byteCer,
                                                      byteKey,
                                                      this.PassKey,
                                                      objProperties);
                if (objResponse.status == 200) {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    FileExtensions.WriteFileByte(objResponse.acuseSat, RouteManager.JaegerPath(PathsEnum.Temporal, string.Concat("Acuse-", uuid, "_testing.Xml")));
                    objEnvelope = HelperXmlSerializer.XmlDeserializarStringXml<Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                }
                else if (objResponse.status == 201) {
                    this.Codigo = 201;
                    this.Mensaje = objResponse.mensaje;
                }
                else if (objResponse.status == 601) {
                    this.Codigo = 601;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                }
                else {
                    this.Codigo = objResponse.status;
                    this.Mensaje = objResponse.mensaje;
                    FileExtensions.WriteFileText(RouteManager.JaegerPath(PathsEnum.Accuse, string.Concat("Acuse-error-", uuid, ".xml")), objResponse.mensaje);
                }
            }
            catch (Exception ex) {
                this.Codigo = -1;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return objAcuse;
        }

        public ResponseValidate Validar(string inputXml) {
            throw new NotImplementedException();
        }

        #region utilidades

        /// <summary>
        /// Devuelve la cantidad de folios (timbres) contratados, usados y disponibles para el usuario.
        /// </summary>
        /// <returns></returns>
        public TimbresDisponibles TimbresDisponible() {
            if (this.Settings.Production == false) {
                return this.TimbresDisponibles_Testing();
            }
            else {
                return this.TimbresDisponibles_Produccion();
            }
        }

        public TimbresDisponibles TimbresDisponibles_Testing() {
            com.SF.Utilerias.Testing.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Testing.UtileriasPortTypeClient("UtileriasHttpSoap11Endpoint_Testing");
            com.SF.Utilerias.Testing.getTimbresRequest request = new com.SF.Utilerias.Testing.getTimbresRequest();
            request.usuario = this.usuarioTesting;
            request.password = this.passwordTesting;
            request.rfcEmisor = this.Settings.RFC;

            try {
                com.SF.Utilerias.Testing.getTimbresResponse response = portClient.getTimbres(request);
                if (response != null) {
                    if (response.@return.status == 200) {
                        TimbresDisponibles a = new TimbresDisponibles();
                        a.Contratados = response.@return.contratados.ToString();
                        a.Disponibles = response.@return.disponibles.ToString();
                        a.Mensaje = response.@return.mensaje.ToString();
                        a.Status = response.@return.status.ToString();
                        a.Usados = response.@return.usados.ToString();
                        return a;
                    }
                }
            }
            catch (Exception ex) {
                this.Mensaje = ex.Message;
            }
            return null;
        }

        public TimbresDisponibles TimbresDisponibles_Produccion() {
            com.SF.Utilerias.Produccion.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Produccion.UtileriasPortTypeClient("UtileriasHttpsSoap11Endpoint1_Produccion");
            com.SF.Utilerias.Produccion.getTimbresRequest request = new com.SF.Utilerias.Produccion.getTimbresRequest();
            request.usuario = this.Settings.User;
            request.password = this.Settings.Password;
            request.rfcEmisor = this.Settings.RFC;

            try {
                com.SF.Utilerias.Produccion.getTimbresResponse response = portClient.getTimbres(request);
                if (response != null) {
                    if (response.@return.status == 200) {
                        TimbresDisponibles a = new TimbresDisponibles();
                        a.Contratados = response.@return.contratados.ToString();
                        a.Disponibles = response.@return.disponibles.ToString();
                        a.Mensaje = response.@return.mensaje.ToString();
                        a.Status = response.@return.status.ToString();
                        a.Usados = response.@return.usados.ToString();
                        return a;
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public ResponseBuscar Buscar_Produccion(string uuid) {
            com.SF.Utilerias.Produccion.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Produccion.UtileriasPortTypeClient("UtileriasHttpsSoap11Endpoint1_Produccion");
            com.SF.Utilerias.Produccion.ParametrosBuscar parametros = new com.SF.Utilerias.Produccion.ParametrosBuscar();
            parametros.uuid = uuid;
            com.SF.Utilerias.Produccion.buscarRequest request = new com.SF.Utilerias.Produccion.buscarRequest();
            request.usuario = this.Settings.User;
            request.password = this.Settings.Password;
            request.parametros = parametros;

            try {
                com.SF.Utilerias.Produccion.buscarResponse response = portClient.buscar(request);
                ResponseBuscar respuesta = new ResponseBuscar();
                if (response != null) {
                    if (response.@return != null) {
                        //respuesta.Cancelado = response.@return.cfdis[0].cancelado;
                        respuesta.EmisorNombre = response.@return.cfdis[0].emisorNombre;
                        respuesta.EmisorRFC = response.@return.cfdis[0].emisorRFC;
                        respuesta.FechaCancelacion = response.@return.cfdis[0].fechaCancelacion;
                        respuesta.FechaEmision = response.@return.cfdis[0].fechaEmision;
                        respuesta.FechaTimbrado = response.@return.cfdis[0].fechaTimbrado;
                        respuesta.Folio = response.@return.cfdis[0].folio;
                        respuesta.ReceptorNombre = response.@return.cfdis[0].receptorNombre;
                        respuesta.ReceptorRFC = response.@return.cfdis[0].receptorRFC;
                        respuesta.SelloDigital = response.@return.cfdis[0].selloDigital;
                        respuesta.SelloSAT = response.@return.cfdis[0].selloSAT;
                        respuesta.Serie = response.@return.cfdis[0].serie;
                        //respuesta.Total = new decimal(response.@return.cfdis[0].total);
                        respuesta.IdDocumento = response.@return.cfdis[0].uuid;
                        return respuesta;
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        public string GenerarManifiesto(Manifiesto manifiesto) {
            string result = "";
            com.SF.Utilerias.Testing.Emisor emisor = new com.SF.Utilerias.Testing.Emisor();

            emisor.rfc = manifiesto.RFC;
            emisor.razonSocial = manifiesto.RazonSocial;
            emisor.nombreComercial = manifiesto.NombreComercial;
            emisor.email = manifiesto.Correo;
            emisor.domicilioFiscal = new com.SF.Utilerias.Testing.DireccionFiscal();
            emisor.domicilioFiscal.calle = manifiesto.Domicilio.Calle;
            emisor.domicilioFiscal.ciudad = manifiesto.Domicilio.Ciudad;
            emisor.domicilioFiscal.codigoPostal = manifiesto.Domicilio.CodigoPostal;
            emisor.domicilioFiscal.colonia = manifiesto.Domicilio.Colonia;
            emisor.domicilioFiscal.estado = manifiesto.Domicilio.Estado;
            emisor.domicilioFiscal.noExt = manifiesto.Domicilio.NoExterior;
            emisor.domicilioFiscal.noInt = manifiesto.Domicilio.NoInterior;
            emisor.domicilioFiscal.pais = manifiesto.Domicilio.Pais;

            com.SF.Utilerias.Testing.UtileriasPortType portClient = null;
            portClient = new com.SF.Utilerias.Testing.UtileriasPortTypeClient("UtileriasHttpSoap11Endpoint_Testing");
            com.SF.Utilerias.Testing.generarManifiestoRequest request = new com.SF.Utilerias.Testing.generarManifiestoRequest();
            request.usuario = this.usuarioTesting;
            request.password = this.passwordTesting;
            request.emisor = emisor;

            try {
                com.SF.Utilerias.Testing.generarManifiestoResponse response = portClient.generarManifiesto(request);
                if (response != null) {
                    if (response.@return.status == 200) {
                        result = response.@return.mensaje;
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return result;
        }
        #endregion
    }
}
