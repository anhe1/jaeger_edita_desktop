﻿using System;
using Jaeger.Certifica.Helpers;
using Jaeger.Certifica.Contracts;
using Jaeger.Certifica.Entities;
using Jaeger.Certifica.ValueObjects;
using Jaeger.CFDI.V33;
using Jaeger.CFDI.Cancel;

namespace Jaeger.Certifica.Services {
    public class Factorum : ProviderServiceConfiguration, IProveedorCertificacion {
        // credenciales para pruebas
        private readonly string usuarioTesting = "prueba1@factorum.com.mx";
        private readonly string passwordTesting = "prueba2011";
        private readonly string rfcTesting = "LAN7008173R5";

        public int Codigo {
            get; set;
        }

        public string Mensaje {
            get; set;
        }

        public new ProviderService Settings {
            get; set;
        }

        public Factorum(ProviderService conf) {
            this.Settings = conf;
        }
        #region timbrar comprobante

        public Comprobante Timbrar(string input) {
            if (this.Settings.Production == false) {
                return this.Timbrado_Testing(input);
            } else {
                return this.Timprado_Produccion(input);
            }
        }

        private Comprobante Timbrado_Testing(string input) {
            com.FAC.Timbre.ReturnFactorumWS response = new com.FAC.Timbre.ReturnFactorumWS();
            com.FAC.Timbre.FactorumCFDiServiceSoapClient soapClient = new com.FAC.Timbre.FactorumCFDiServiceSoapClient();
            try {
                response = soapClient.FactorumGenYaSelladoTest(this.usuarioTesting, this.rfcTesting, this.passwordTesting, input);
                if (response != null) {
                    // por cualquier cosa guardamos primero el archivo de manera temporal
                    System.IO.File.WriteAllBytes(RouteManager.JaegerPath(PathsEnum.Temporal, string.Concat(response.UUID, ".xml")), response.ReturnFileXML);
                    Comprobante comprobante = Comprobante.LoadBytes(response.ReturnFileXML);
                    if (comprobante != null) {
                        this.Codigo = 0;
                        this.Mensaje = null;
                        return comprobante;
                    }
                }
            } catch (Exception ex) {
                this.Codigo = 1;
                this.Mensaje = ex.Message;
            }
            return null;
        }

        private Comprobante Timprado_Produccion(string input) {
            Console.WriteLine(input);
            return null;
        }

        #endregion

        #region CFD Retenciones 1.0
        public CFDRetencion.V10.Retenciones TimbrarRetencion(string cfd) {
            return null;
        }
        #endregion

        #region cancelar comprobante

        public CancelaCFDResponse Cancelar(string uuid, string clave) {
            if (this.Settings.Production == false) {
                return this.Cancelar_Testing(uuid, clave);
            } else {
                return this.Cancelar_Produccion(uuid);
            }
        }

        private CancelaCFDResponse Cancelar_Testing(string uuid, string clave) {
            // TODO: Implement this method
            Console.WriteLine(uuid);
            throw new NotImplementedException();
        }

        private CancelaCFDResponse Cancelar_Produccion(string uuid) {
            Console.WriteLine(uuid);
            throw new NotImplementedException();
        }

        #endregion

        #region validacion

        public ResponseValidate Validar(string inputXml) {
            com.FAC.Valida.WSValidaXMLSoapClient soapClient = new com.FAC.Valida.WSValidaXMLSoapClient();
            try {
                com.FAC.Valida.ReturnService response = soapClient.ValidaXMLparaWebService(this.usuarioTesting, this.rfcTesting, this.passwordTesting, System.Text.Encoding.UTF8.GetBytes(inputXml), true);
                if (response != null) {
                    ResponseValidate validateResponse = new ResponseValidate();
                    if (response.Codigo == "200") {
                        validateResponse.Valido = ValueObjects.ValidacionResultEnum.Valido;
                        foreach (string item in response.Resultado) {
                            validateResponse.Validations.Add(new PropertyValidate { Type = ValueObjects.PropertyTypeEnum.Information, Name = "1111", Value = item });
                        }
                        return validateResponse;
                    }
                }
            } catch (Exception) {

                throw;
            }
            return null;
        }

        public CFDI.V40.Comprobante Timbrar40(string input) {
            throw new NotImplementedException();
        }
        #endregion

    }
}
