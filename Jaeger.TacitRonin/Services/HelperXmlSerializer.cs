﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;

namespace Jaeger.Certifica.Helpers {
    public class HelperXmlSerializer {
        public static T XmlDeserializar<T>(FileInfo archivo) {
            T t;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextReader xmlTextReader = new XmlTextReader(archivo.FullName);
                T t1 = (T)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                t = t1;
            } catch (Exception exception) {
                throw exception;
            }
            return t;
        }

        public static T XmlDeserializar<T>(string archivo) {
            T t;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextReader xmlTextReader = new XmlTextReader(archivo);
                T t1 = (T)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                t = t1;
            } catch (Exception exception) {
                throw exception;
            }
            return t;
        }

        public static T XmlDeserializar<T>(Stream archivo) {
            T t;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextReader xmlTextReader = new XmlTextReader(archivo);
                T t1 = (T)xmlSerializer.Deserialize(archivo);
                xmlTextReader.Close();
                t = t1;
            } catch (Exception exception) {
                throw exception;
            }
            return t;
        }

        public static T XmlDeserializarStringXml<T>(string sXml) {
            T t;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                byte[] bytes = Encoding.UTF8.GetBytes(sXml);
                XmlTextReader xmlTextReader = new XmlTextReader(new MemoryStream(bytes));
                T t1 = (T)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                t = t1;
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("HelperXmlSerializer: XmlDeserializarStringXml: ", ex.Message));
                t = default(T);
            }
            return t;
        }

        public static T XmlDeserializarBytesXml<T>(byte[] bytes) {
            T t;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextReader xmlTextReader = new XmlTextReader(new MemoryStream(bytes));
                T t1 = (T)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                t = t1;
            } catch (Exception exception) {
                throw exception;
            }
            return t;
        }

        public static bool XmlSerializar<T>(T clase, string archivo) {
            bool flag;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(archivo, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase);
                xmlTextWriter.Close();
                flag = true;
            } catch (Exception exception) {
                throw exception;
            }
            return flag;
        }

        public static bool XmlSerializar<T>(T clase, string archivo, XmlSerializerNamespaces nameSpaces) {
            bool flag;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(archivo, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase, nameSpaces);
                xmlTextWriter.Close();
                flag = true;
            } catch (Exception exception) {
                throw exception;
            }
            return flag;
        }

        public static MemoryStream XmlSerializar1<T>(T clase) {
            MemoryStream memoryStream;
            MemoryStream memoryStream1 = new MemoryStream();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream1, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase);
                xmlTextWriter.Close();
                memoryStream = memoryStream1;
            } catch (Exception exception) {
                throw exception;
            }
            return memoryStream;
        }

        public static MemoryStream XmlSerializar1<T>(T clase, XmlSerializerNamespaces nameSpaces) {
            MemoryStream memoryStream;
            MemoryStream memoryStream1 = new MemoryStream();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream1, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase, nameSpaces);
                xmlTextWriter.Close();
                memoryStream = memoryStream1;
            } catch (Exception exception) {
                throw exception;
            }
            return memoryStream;
        }

        public static string XmlSerializar11<T>(T clase) {
            string str;
            MemoryStream memoryStream = new MemoryStream();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase);
                memoryStream.Position = 0;
                string end = (new StreamReader(memoryStream)).ReadToEnd();
                xmlTextWriter.Close();
                str = end;
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static string XmlSerializar11<T>(T clase, XmlSerializerNamespaces nameSpaces) {
            string str;
            MemoryStream memoryStream = new MemoryStream();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xmlSerializer.Serialize(xmlTextWriter, clase, nameSpaces);
                memoryStream.Position = 0;
                string end = (new StreamReader(memoryStream)).ReadToEnd();
                xmlTextWriter.Close();
                str = end;
            } catch (Exception exception) {
                throw exception;
            }
            return str;
        }

        public static StringWriter XmlSerializar2<T>(T clase) {
            StringWriter stringWriter;
            StringWriter stringWriter1 = new StringWriter();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter1);
                xmlSerializer.Serialize(xmlTextWriter, clase);
                xmlTextWriter.Close();
                stringWriter = stringWriter1;
            } catch (Exception exception) {
                throw exception;
            }
            return stringWriter;
        }

        public static StringWriter XmlSerializar2<T>(T clase, XmlSerializerNamespaces nameSpaces) {
            StringWriter stringWriter;
            StringWriter stringWriter1 = new StringWriter();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter1);
                xmlSerializer.Serialize(xmlTextWriter, clase, nameSpaces);
                xmlTextWriter.Close();
                stringWriter = stringWriter1;
            } catch (Exception exception) {
                throw exception;
            }
            return stringWriter;
        }

        public static void FormatXmlNode(XmlNode node, int indent) {

            bool m_text_only;

            // Do nothing if this is a text node.
            if (node is System.Xml.XmlText)
                return;

            // See if this node contains only text.
            m_text_only = true;
            if (node.HasChildNodes) {
                foreach (var m_Child in node.ChildNodes) {
                    if (!(m_Child is System.Xml.XmlText)) {
                        m_text_only = false;
                        break;
                    }
                }
            }

            // Process child nodes.
            if (node.HasChildNodes) {
                // Add a carriage return before the children.
                if (!m_text_only)
                    node.InsertBefore(node.OwnerDocument.CreateTextNode("\n"), node.FirstChild);

                // Format the children.
                foreach (XmlNode m_Child in node.ChildNodes)
                    FormatXmlNode(m_Child, indent + 1);
            }

            // Format this element.
            if (indent > 0) {
                // Indent before this element.
                node.ParentNode.InsertBefore(node.OwnerDocument.CreateTextNode(new string('\t', indent)), node);

                // Indent after the last child node.
                if (!m_text_only)
                    node.AppendChild(node.OwnerDocument.CreateTextNode(new string('\t', indent)));

                // Add a carriage return after this node.
                if (node.NextSibling == null)
                    node.ParentNode.AppendChild(node.OwnerDocument.CreateTextNode("\n"));
                else
                    node.ParentNode.InsertBefore(node.OwnerDocument.CreateTextNode("\n"), node.NextSibling);
            }
        }
    }
}
