﻿// develop: 061020160052
// purpose:
// * Mensajes de Respuesta
// Los mensajes de respuesta que arroja el servicio de consulta de CFDI´s incluyen la descripción del resultado de la operación que corresponden a la siguiente clasificación:
// Mensajes de Rechazo.
// N 601: La expresión impresa proporcionada no es válida.
// Este código de respuesta se presentará cuando la petición de validación no se haya respetado en el formato definido.
// N 602: Comprobante no encontrado.
// Este código de respuesta se presentará cuando el UUID del comprobante no se encuentre en la Base de Datos del SAT.
// Mensajes de Aceptación.
// S Comprobante obtenido satisfactoriamente.
// rev.: 201220171253: agregamos replace para escapar "&"
// rev.: 201801130147: agregamos try al generar url para la consulta, para regresar un error si no es posible generar la url
/// Error al realizar la solicitud HTTP a https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc. Esto puede deberse a que el certificado del servidor no está configurado correctamente en 
/// HTTP.SYS en el caso HTTPS. La causa puede ser también una falta de coincidencia del enlace de seguridad entre el cliente y el servidor.
/// Para evitar el error se agrego la linea: System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
using System;
using System.Threading.Tasks;
using Jaeger.Certifica.com.SAT.QueryCFDI;
using Jaeger.Certifica.Entities;

namespace Jaeger.Certifica.Services {
    public static class SATQueryService {
        /// <summary>
        /// El Servicio de consulta de CFDI´s se diseñó para permitir la validación accediendo a un servicio publicado en la página del SAT desde Internet, el servicio pretende 
        /// proveer una alternativa de consulta que requiera verificar el estado de un comprobante en las Bases de Datos del SAT. 
        /// </summary>
        public static SatQueryResult Query(string emisorRFC, string receptorRFC, decimal total, string uuid) {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            ConsultaCFDIServiceClient serviceQuery = new ConsultaCFDIServiceClient();
            Acuse serviceResponse = new Acuse();
            SatQueryResult queryResult = new SatQueryResult();
            string datos;

            queryResult.Key = "E";
            queryResult.Status = "Error";

            try {
                datos = string.Format("?re={0} &rr={1} &tt={2} &id={3}", emisorRFC.Replace("&", "&amp;"), receptorRFC.Replace("&", "&amp;"), total.ToString("0.00"), uuid);
            }
            catch (Exception ex) {
                queryResult.Clave = ex.Message;
                Console.WriteLine(ex.Message);
                return queryResult;
            }

            try {
                serviceQuery.Open();
            }
            catch (Exception ex) {
                queryResult.Clave = ex.Message;
                return queryResult;
            }

            try {
                serviceResponse = serviceQuery.Consulta(datos);
            }
            catch (Exception ex) {
                queryResult.Clave = ex.Message;
                return queryResult;
            }

            queryResult.Status = serviceResponse.Estado;
            queryResult.Clave = serviceResponse.CodigoEstatus;
            queryResult.Key = serviceResponse.CodigoEstatus.ToString().Substring(0, 1);

            serviceQuery.Close();
            return queryResult;
        }

        public static async Task<SatQueryResult> QueryAsync(string emisorRFC, string receptorRFC, decimal total, string uuid) {
            SatQueryResult estado = null;
            await Task.Run(() => {
                estado = SATQueryService.Query(emisorRFC, receptorRFC, total, uuid);
            });
            return estado;
        }
    }
}
