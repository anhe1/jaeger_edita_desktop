﻿// develop: 061020160052
// purpose:
// * Mensajes de Respuesta
// Los mensajes de respuesta que arroja el servicio de consulta de CFDI´s incluyen la descripción del resultado de la operación que corresponden a la siguiente clasificación:
// Mensajes de Rechazo.
// N 601: La expresión impresa proporcionada no es válida.
// Este código de respuesta se presentará cuando la petición de validación no se haya respetado en el formato definido.
// N 602: Comprobante no encontrado.
// Este código de respuesta se presentará cuando el UUID del comprobante no se encuentre en la Base de Datos del SAT.
// Mensajes de Aceptación.
// S Comprobante obtenido satisfactoriamente.
// rev.: 201220171253: agregamos replace para escapar "&"
// rev.: 201801130147: agregamos try al generar url para la consulta, para regresar un error si no es posible generar la url
/// Error al realizar la solicitud HTTP a https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc. Esto puede deberse a que el certificado del servidor no está configurado correctamente en 
/// HTTP.SYS en el caso HTTPS. La causa puede ser también una falta de coincidencia del enlace de seguridad entre el cliente y el servidor.
/// Para evitar el error se agrego la linea: System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
using Jaeger.Certifica.com.SAT.QueryCFDI;
using Jaeger.Certifica.Entities;

namespace Jaeger.Certifica.Services {
    public class Status : StatusService {
        private readonly string urlSAT = "https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc";

        public Status() : base("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc") {
        }

        public Status(string url) : base(url) {
        }

        internal override Acuse StatusRequest(string rfcEmisor, string rfcReceptor, decimal total, string uuid) {
            return this.RequestStatus(rfcEmisor, rfcReceptor, total, uuid);
        }

        public Acuse GetStatusCFDI(string rfcEmisor, string rfcReceptor, decimal Total, string uuid) {
            return StatusRequest(rfcEmisor, rfcReceptor, Total, uuid);
        }

        public SatQueryResult GetStatus(string rfcEmisor, string rfcReceptor, decimal Total, string uuid) {
            var _response = new SatQueryResult();
            var _acuse = this.GetStatusCFDI(rfcEmisor, rfcReceptor, Total, uuid);
            if (_acuse != null) {
                _response.Status = _acuse.Estado;
                _response.Clave = _acuse.CodigoEstatus;
                _response.Key = _acuse.CodigoEstatus.ToString().Substring(0, 1);
                return _response;
            }
            return null;
        }
    }
}
