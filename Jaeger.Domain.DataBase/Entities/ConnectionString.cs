﻿namespace Jaeger.Domain.DataBase.Entities {
    internal sealed class ConnectionString {
        internal const string DefaultValueDataSource = "";
        internal const int DefaultValuePortNumber = 3306;
        internal const string DefaultValueUserId = "";
        internal const string DefaultValuePassword = "";
        internal const string DefaultValueRoleName = "";
        internal const string DefaultValueCatalog = "";
        internal const string DefaultValueCharacterSet = "UTF8";
        internal const int DefaultValueDialect = 3;
        internal const int DefaultValuePacketSize = 8192;
        internal const bool DefaultValuePooling = true;
    }
}
