﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.DataBase.Entities {
    /// <summary>
    /// clase que contiene la configuracion de la base de datos
    /// </summary>
    [JsonObject("conf")]
    public class DataBaseConfiguracion : BasePropertyChangeImplementation, IDataBaseConfiguracion, IBuilder {
        #region variables
        private string _DataBase;
        private string _DataSource;
        private int _PortNumber;
        private string _Charset;
        private bool _Pooling;
        private int _PageSize;
        private int _ModoSSL;
        private bool _ForcedWrite;
        private string _UserID;
        private string _Password;
        private uint _ConnectionTimeout;
        private uint _ConnectionLifeTime;
        private readonly JsonSerializerSettings _JsonConf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        #endregion

        public DataBaseConfiguracion() {
            this._PortNumber = 4530;
            this._ModoSSL = 0;
            this._ConnectionLifeTime = 0;
            this._ConnectionTimeout = 60;
        }

        public DataBaseConfiguracion(string json) {
            var temp = JsonConvert.DeserializeObject<DataBaseConfiguracion>(json, _JsonConf);
            MapperClassExtensions.MatchAndMap<DataBaseConfiguracion, DataBaseConfiguracion>(temp, this);
        }

        public DataBaseConfiguracion(DataBaseConfiguracion json) {
            MapperClassExtensions.MatchAndMap<DataBaseConfiguracion, DataBaseConfiguracion>(json, this);
        }

        /// <summary>
        /// obtener o establecer el nombre de la base de datos
        /// </summary>
        [DisplayName("Base de Datos"), Description("Nombre de la base de datos."), PasswordPropertyText(false)]
        [DefaultValue("Server")]
        [JsonProperty("database", Order = 0)]
        public string Database {
            get { return this._DataBase; }
            set {
                if (this._DataBase != value) {
                    this._DataBase = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del servidor
        /// </summary>
        [DisplayName("Servidor"), Description("Servidor o Host del servicio de base de datos."), PasswordPropertyText(true)]
        [DefaultValue(ConnectionString.DefaultValueDataSource)]
        [JsonProperty("host", Order = 1)]
        public string HostName {
            get { return this._DataSource; }
            set {
                if (this._DataSource != value) {
                    this._DataSource = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el numero del puerto del servidor
        /// </summary>
        [DisplayName("Puerto"), Description("Puerto de conexión, default 3306"), PasswordPropertyText(true)]
        [DefaultValue(ConnectionString.DefaultValuePortNumber)]
        [JsonProperty("port", Order = 2)]
        public int PortNumber {
            get { return this._PortNumber; }
            set {
                if (this._PortNumber != value) {
                    this._PortNumber = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DisplayName("Modo SSL"), Description("Modo SSL aplicable a Mysql (default 0)")]
        [JsonProperty("modoSSL", Order = 3)]
        public int ModoSSL {
            get { return this._ModoSSL; }
            set { this._ModoSSL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el juego de caracteres utilizado
        /// </summary>
        [DisplayName("Charset"), Description("juego de caracteres utilizado")]
        [DefaultValue(ConnectionString.DefaultValueCharacterSet)]
        [JsonProperty("charset", Order = 4)]
        public string Charset {
            get { return this._Charset; }
            set {
                if (this._Charset != value) {
                    this._Charset = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DefaultValue(ConnectionString.DefaultValuePacketSize)]
        [JsonProperty("pooling", Order = 5)]
        public bool Pooling {
            get { return this._Pooling; }
            set {
                if (this._Pooling != value) {
                    this._Pooling = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonProperty("pagesize", Order = 6)]
        public int PageSize {
            get { return this._PageSize; }
            set {
                if (this._PageSize != value) {
                    this._PageSize = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonProperty("forcewrite", Order = 7)]
        public bool ForcedWrite {
            get { return this._ForcedWrite; }
            set {
                if (this._ForcedWrite != value) {
                    this._ForcedWrite = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonProperty("connectionTimeout", Order = 7)]
        public uint ConnectionTimeout {
            get { return this._ConnectionTimeout; }
            set { this._ConnectionTimeout = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("connectionLifeTime")]
        public uint ConnectionLifeTime {
            get { return this._ConnectionLifeTime; }
            set { this._ConnectionLifeTime = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer el nombre de usuario
        /// </summary>
        [DisplayName("Nombre de usuario"), Description("Nombre de usuario"), PasswordPropertyText(true)]
        [DefaultValue(ConnectionString.DefaultValueUserId)]
        [JsonProperty("user", Order = 8)]
        public string UserID {
            get { return this._UserID; }
            set {
                if (this._UserID != value) {
                    this._UserID = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el password de la base de datos
        /// </summary>
        [DisplayName("Contraseña"), Description("Contraseña del servicio."), PasswordPropertyText(true)]
        [DefaultValue(ConnectionString.DefaultValuePassword)]
        [JsonProperty("pass", Order = 9)]
        public string Password {
            get { return this._Password; }
            set {
                if (this._Password != value) {
                    this._Password = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public IDataBaseConfiguracion WithString(string json) {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            var temp = JsonConvert.DeserializeObject<DataBaseConfiguracion>(json, conf);
            MapperClassExtensions.MatchAndMap<DataBaseConfiguracion, DataBaseConfiguracion>(temp, this);
            return this;
        }

        public string Json() {
            Formatting objFormat = 0;
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public override string ToString() {
            Formatting objFormat = 0;
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }
    }
}
