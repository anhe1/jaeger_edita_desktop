﻿namespace Jaeger.Domain.DataBase.Contracts {
    public interface IBuilder {
        IDataBaseConfiguracion WithString(string json);
    }
}
