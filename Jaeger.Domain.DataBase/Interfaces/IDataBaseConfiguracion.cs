﻿namespace Jaeger.Domain.DataBase.Contracts {
    /// <summary>
    /// clase que contiene la configuracion de la base de datos
    /// </summary>
    public interface IDataBaseConfiguracion {
        /// <summary>
        /// obtener o establecer el nombre de la base de datos
        /// </summary>
        string Database { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del servidor
        /// </summary>
        string HostName { get; set; }

        /// <summary>
        /// obtener o establecer el numero del puerto del servidor
        /// </summary>
        int PortNumber { get; set; }

        int ModoSSL { get; set; }

        /// <summary>
        /// obtener o establecer el juego de caracteres utilizado
        /// </summary>
        string Charset { get; set; }

        bool Pooling { get; set; }

        bool ForcedWrite { get; set; }

        int PageSize { get; set; }

        /// <summary>
        /// obtener o estalecer el nombre de usuario
        /// </summary>
        string UserID { get; set; }

        /// <summary>
        /// obtener o establecer el password de la base de datos
        /// </summary>
        string Password { get; set; }

        string Json();
    }
}
