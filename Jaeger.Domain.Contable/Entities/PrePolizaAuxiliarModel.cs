﻿using System;
using SqlSugar;
using Jaeger.Domain.Contable.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaAuxiliarModel : Base.Abstractions.BasePropertyChangeImplementation, IPrePolizaAuxiliarModel {
        #region declaraciones
        private string _TIPO_POLI;
        private string _NUM_POLIZ;
        private double _NUM_PART;
        private int _PERIODO;
        private int _EJERCICIO;
        private string _NUM_CTA;
        private DateTime? _FECHA_POL;
        private string _CONCEP_PO;
        private string _DEBE_HABER;
        private decimal _MONTOMOV;
        private int? _NUMDEPTO;
        private double? _TIPCAMBIO;
        private int? _CONTRAPAR;
        private int? _ORDEN;
        private int? _CCOSTOS;
        private int? _CGRUPOS;
        private int? _IDINFADIPAR;
        private int? _IDUUID;
        private object _TAG;
        private System.ComponentModel.BindingList<PrePolizaComprobanteModel> _Comprobantes;
        #endregion

        public PrePolizaAuxiliarModel() {
            this._Comprobantes = new System.ComponentModel.BindingList<PrePolizaComprobanteModel>();
        }

        /// <summary>
        /// obtener o establecer tipo (TIPO_POLI)
        /// Longitud = 2
        /// </summary>
        [DataNames("TIPO_POLI")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "tipo_poli", ColumnDescription = "Tipo", IsNullable = false, Length = 2)]
        public string Tipo {
            get {
                return this._TIPO_POLI;
            }
            set {
                this._TIPO_POLI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero (NUM_POLIZ)
        /// Longitud = 5
        /// </summary>
        [DataNames("NUM_POLIZ")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_poliz", ColumnDescription = "Numero", IsNullable = false, Length = 5)]
        public string NumPoliza {
            get {
                return this._NUM_POLIZ;
            }
            set {
                this._NUM_POLIZ = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de partida (NUM_PART)
        /// </summary>
        [DataNames("NUM_PART")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "num_part", ColumnDescription = "Numero de la partida", IsNullable = false)]
        public double NumPartida {
            get {
                return this._NUM_PART;
            }
            set {
                this._NUM_PART = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer PERIODO
        /// </summary>
        [DataNames("PERIODO")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "periodo", ColumnDescription = "Periodo", IsNullable = false)]
        public int Periodo {
            get {
                return this._PERIODO;
            }
            set {
                this._PERIODO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer EJERCICIO
        /// </summary>
        [DataNames("EJERCICIO")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
        public int Ejercicio {
            get {
                return this._EJERCICIO;
            }
            set {
                this._EJERCICIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de la cuenta (NUM_CTA)
        /// Longitud = 21
        /// </summary>
        [DataNames("NUM_CTA")]
        [SugarColumn(ColumnName = "num_cta", ColumnDescription = "Numero de la cuenta", Length = 21)]
        public string NumCuenta {
            get {
                return this._NUM_CTA;
            }
            set {
                this._NUM_CTA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Fecha de registro (FECHA_POL)
        /// </summary>
        [DataNames("FECHA_POL")]
        [SugarColumn(ColumnName = "fecha_pol", ColumnDescription = "Fecha de registro")]
        public DateTime? Fecha {
            get {
                return this._FECHA_POL;
            }
            set {
                this._FECHA_POL = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Concepto (CONCEP_PO)
        /// Longitud = 120
        /// </summary>
        [DataNames("CONCEP_PO")]
        [SugarColumn(ColumnName = "concep_po", ColumnDescription = "Concepto", Length = 120)]
        public string Concepto {
            get {
                return this._CONCEP_PO;
            }
            set {
                this._CONCEP_PO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Abono o cargo (DEBE_HABER)
        /// Longitud = 1
        /// </summary>
        [DataNames("DEBE_HABER")]
        [SugarColumn(ColumnName = "debe_haber", ColumnDescription = "Abono o cargo", Length = 1)]
        public string DEBE_HABER {
            get {
                return this._DEBE_HABER;
            }
            set {
                this._DEBE_HABER = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Monto movimiento (MONTOMOV)
        /// </summary>
        [DataNames("MONTOMOV")]
        [SugarColumn(ColumnName = "montomov", ColumnDescription = "Monto movimiento")]
        public decimal Monto {
            get {
                return this._MONTOMOV;
            }
            set {
                this._MONTOMOV = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero depto (NUMDEPTO)
        /// </summary>
        [DataNames("NUMDEPTO")]
        [SugarColumn(ColumnName = "numdepto", ColumnDescription = "Numero depto")]
        public int? NumDepto {
            get {
                return this._NUMDEPTO;
            }
            set {
                this._NUMDEPTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio (TIPCAMBIO)
        /// </summary>
        [DataNames("TIPCAMBIO")]
        [SugarColumn(ColumnName = "tipcambio", ColumnDescription = "Tipo cambio")]
        public double? TipoCambio {
            get {
                return this._TIPCAMBIO;
            }
            set {
                this._TIPCAMBIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Contrapartida (CONTRAPAR)
        /// </summary>
        [DataNames("CONTRAPAR")]
        [SugarColumn(ColumnName = "contrapar", ColumnDescription = "Contrapartida")]
        public int? CONTRAPAR {
            get {
                return this._CONTRAPAR;
            }
            set {
                this._CONTRAPAR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Orden de la partida (ORDEN)
        /// </summary>
        [DataNames("ORDEN")]
        [SugarColumn(ColumnName = "orden", ColumnDescription = "Orden de la partida")]
        public int? ORDEN {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dentro de costos CCOSTOS
        /// </summary>
        [DataNames("CCOSTOS")]
        [SugarColumn(ColumnName = "ccostos", ColumnDescription = "Centro de costos")]
        public int? CCOSTOS {
            get {
                return this._CCOSTOS;
            }
            set {
                this._CCOSTOS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CGRUPOS
        /// </summary>
        [DataNames("CGRUPOS")]
        [SugarColumn(ColumnName = "cgrupos", ColumnDescription = "Grupos")]
        public int? CGRUPOS {
            get {
                return this._CGRUPOS;
            }
            set {
                this._CGRUPOS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de registro de forma de pago (IDINFADIPAR)
        /// </summary>
        [DataNames("IDINFADIPAR")]
        [SugarColumn(ColumnName = "idinfadipar", ColumnDescription = "Numero de registro de forma de pago")]
        public int? IDINFADIPAR {
            get {
                return this._IDINFADIPAR;
            }
            set {
                this._IDINFADIPAR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Numero de registro de comprobantes (IDUUID)
        /// </summary>
        [DataNames("IDUUID")]
        [SugarColumn(ColumnName = "iduuid", ColumnDescription = "Numero de registro de comprobantes")]
        public int? IDUUID {
            get {
                return this._IDUUID;
            }
            set {
                this._IDUUID = value;
                this.OnPropertyChanged();
            }
        }

        public System.ComponentModel.BindingList<PrePolizaComprobanteModel> Comprobantes {
            get { return this._Comprobantes; }
            set { this._Comprobantes = value;
                this.OnPropertyChanged();
            }
        }
    }
}
