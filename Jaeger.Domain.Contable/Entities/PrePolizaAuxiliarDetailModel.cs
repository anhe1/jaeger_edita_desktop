﻿namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaAuxiliarDetailModel : PrePolizaAuxiliarModel {
        public decimal Debe {
            get { if (this.DEBE_HABER == "1")
                    return this.Monto;
                return 0;
            }
        }

        public decimal Haber {
            get { if (this.DEBE_HABER == "0")
                    return this.Monto;
                return 0;
            }
        }

        public string ToString() {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n", "", this.NumCuenta, this.NumDepto, this.Concepto, this.TipoCambio.ToString(), this.Debe, this.Haber, this.CCOSTOS, this.CGRUPOS, "");
        }
    }
}
