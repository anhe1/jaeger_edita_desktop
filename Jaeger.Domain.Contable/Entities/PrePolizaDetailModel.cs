﻿using System.ComponentModel;

namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaDetailModel : PrePolizaModel {
        private BindingList<PrePolizaAuxiliarDetailModel> _Auxiliar;

        public PrePolizaDetailModel() : base() {
            this._Auxiliar = new BindingList<PrePolizaAuxiliarDetailModel>();
            this.Fecha = System.DateTime.Now;
        }

        public new string Concepto {
            get { return base.Concepto; }
            set { base.Concepto = value;
                if (this.Auxiliar != null) {
                    for (int i = 0; i < this.Auxiliar.Count; i++) {
                        this.Auxiliar[i].Concepto = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        public BindingList<PrePolizaAuxiliarDetailModel> Auxiliar {
            get { return this._Auxiliar; }
            set { this._Auxiliar = value;
                this.OnPropertyChanged(); }
        }

        public string Exportar() {
            var _cabecera_Poliza = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n", this.Tipo.ToString(), this.NumPoliza, this.Concepto, this.Fecha.Value.ToString("dd/MM/yy"), "", "", "", "", "", "");
            var _cabecera_Auxiliar = string.Empty;
            foreach (var item in this.Auxiliar) {
                _cabecera_Auxiliar = string.Concat(_cabecera_Auxiliar, item.ToString());
                if (item.Comprobantes != null) {
                    if (item.Comprobantes.Count > 0) {
                        var _cabecera_comprobante = ",,INICIO_CFDI,,,,,,\r\n";
                        foreach (var _comprobante in item.Comprobantes) {
                            _cabecera_comprobante = string.Concat(_cabecera_comprobante, _comprobante.ToString());
                        }
                        _cabecera_comprobante = string.Concat(_cabecera_comprobante, ",,FIN_CFDI,,,,,,\r\n");
                        _cabecera_Auxiliar = string.Concat(_cabecera_Auxiliar, _cabecera_comprobante);
                    }
                }
            }
            return string.Concat(_cabecera_Poliza, _cabecera_Auxiliar);
        }
    }
}
