﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaComprobanteModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int _NUMREG;
        private string _UUIDTIMBRE;
        private decimal _MONTO;
        private string _SERIE;
        private string _FOLIO;
        private string _RFCEMISOR;
        private string _RFCRECEPTOR;
        private int? _ORDEN;
        private DateTime _FECHA;
        private int _TIPOCOMPROBANTE;
        private decimal _TIPOCAMBIO;
        private string _VERSIONCFDI;
        private string _MONEDA;
        #endregion

        public PrePolizaComprobanteModel() {

        }

        /// <summary>
        /// obtener o establecer Numero de registro (NUMREG)
        /// </summary>
        [DataNames("NUMREG")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "numreg", ColumnDescription = "Numero de registro", IsNullable = false)]
        public int NUMREG {
            get {
                return this._NUMREG;
            }
            set {
                this._NUMREG = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer UUID del comprobante (UUIDTIMBRE)
        /// Longitud: 36
        /// </summary>
        [DataNames("UUIDTIMBRE")]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "uuidtimbre", ColumnDescription = "UUID del comprobante", IsNullable = false, Length = 36)]
        public string IdDocumento {
            get {
                return this._UUIDTIMBRE;
            }
            set {
                this._UUIDTIMBRE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONTO
        /// </summary>
        [DataNames("MONTO")]
        [SugarColumn(ColumnName = "monto", ColumnDescription = "Monto")]
        public decimal Monto {
            get {
                return this._MONTO;
            }
            set {
                this._MONTO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer SERIE
        /// Longitud: 100
        /// </summary>
        [DataNames("SERIE")]
        [SugarColumn(ColumnName = "serie", ColumnDescription = "Serie", Length = 100)]
        public string Serie {
            get {
                return this._SERIE;
            }
            set {
                this._SERIE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FOLIO
        /// Longitud: 100
        /// </summary>
        [DataNames("FOLIO")]
        [SugarColumn(ColumnName = "folio", ColumnDescription = "Folio", Length = 100)]
        public string Folio {
            get {
                return this._FOLIO;
            }
            set {
                this._FOLIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC Emisor (RFCEMISOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCEMISOR")]
        [SugarColumn(ColumnName = "rfcemisor", ColumnDescription = "RFC emisor", Length = 30)]
        public string EmisorRFC {
            get {
                return this._RFCEMISOR;
            }
            set {
                this._RFCEMISOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC receptor (RFCRECEPTOR)
        /// Longitud: 30
        /// </summary>
        [DataNames("RFCRECEPTOR")]
        [SugarColumn(ColumnName = "rfcreceptor", ColumnDescription = "RFC receptor", Length = 30)]
        public string ReceptorRFC {
            get {
                return this._RFCRECEPTOR;
            }
            set {
                this._RFCRECEPTOR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer orden de la partida (ORDEN)
        /// </summary>
        [DataNames("ORDEN")]
        [SugarColumn(ColumnName = "orden", ColumnDescription = "Orden de la partida")]
        public int? Orden {
            get {
                return this._ORDEN;
            }
            set {
                this._ORDEN = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer FECHA
        /// </summary>
        [DataNames("FECHA")]
        [SugarColumn(ColumnName = "fecha", ColumnDescription = "Fecha")]
        public DateTime Fecha {
            get {
                return this._FECHA;
            }
            set {
                this._FECHA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Indica si es CFDi o Extranjero (TIPOCOMPROBANTE)
        /// </summary>
        [DataNames("TIPOCOMPROBANTE")]
        [SugarColumn(ColumnName = "tipocomprobante", ColumnDescription = "Indica si es CFDi o Extranjero")]
        public int TipoComprobante {
            get {
                return this._TIPOCOMPROBANTE;
            }
            set {
                this._TIPOCOMPROBANTE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio (TIPOCAMBIO)
        /// </summary>
        [DataNames("TIPOCAMBIO")]
        [SugarColumn(ColumnName = "tipocambio", ColumnDescription = "tipo de cambio")]
        public decimal TipoCambio {
            get {
                return this._TIPOCAMBIO;
            }
            set {
                this._TIPOCAMBIO = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer VERSIONCFDI
        /// Longitud: 6
        /// </summary>
        [DataNames("VERSIONCFDI")]
        [SugarColumn(ColumnName = "versioncfdi", ColumnDescription = "version del CFDI", Length = 6)]
        public string Version {
            get {
                return this._VERSIONCFDI;
            }
            set {
                this._VERSIONCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer MONEDA
        /// Longitud: 3
        /// </summary>
        [DataNames("MONEDA")]
        [SugarColumn(ColumnName = "moneda", ColumnDescription = "Tipo de moneda", Length = 3)]
        public string Moneda {
            get {
                return this._MONEDA;
            }
            set {
                this._MONEDA = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\r\n", "", "", this.Fecha.ToString("dd/MM/yy"), this.Serie, this.Folio, this.EmisorRFC, this.ReceptorRFC, this.Monto, this.IdDocumento, this.TipoComprobante);
        }
    }
}
