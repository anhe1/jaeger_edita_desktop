﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private string _TIPO_POLI;
        private string _NUM_POLIZ;
        private int _PERIODO;
        private int _EJERCICIO;
        private DateTime? _FECHA_POL;
        private string _CONCEP_PO;
        private int? _NUM_PART;
        private string _LOGAUDITA;
        private string _CONTABILIZ;
        private int? _NUMPARCUA;
        private int? _TIENEDOCUMENTOS;
        private int? _PROCCONTAB;
        private string _ORIGEN;
        private string _UUID;
        private int? _ESPOLIZAPRIVADA;
        private string _UUIDOP;
        #endregion

        public PrePolizaModel() {
            this.Tipo = "Dr";
        }

        /// <summary>
        /// obtener o establecer TIPO_POLI; Tipo de poliza.< TIPO_POL> (2 caracteres) debe comenzar en  la primera columna de la tercera fila. El valor por defecto sera Dr.
        /// </summary>
        [DataNames("TIPO_POLI")]
        [SugarColumn(ColumnName = "tipo_poli", ColumnDescription = "Tipo poliza", IsNullable = false, Length = 2)]
        public string Tipo {
            get {
                return this._TIPO_POLI;
            }
            set {
                this._TIPO_POLI = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer NUM_POLIZ, El numero de poliza. (5 caracteres) este valor se tomara en cuenta siempre y cuando el parametro Captura de polizas/Numero secuencial este activado
        /// </summary>
        [DataNames("NUM_POLIZ")]
        [SugarColumn(ColumnName = "num_poliz", ColumnDescription = "Numero de poliza", IsNullable = false, Length = 5)]
        public string NumPoliza {
            get {
                return this._NUM_POLIZ;
            }
            set {
                this._NUM_POLIZ = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer PERIODO
        /// </summary>
        [DataNames("PERIODO")]
        [SugarColumn(ColumnName = "periodo", ColumnDescription = "Periodo", IsNullable = false)]
        public int Periodo {
            get {
                return this._PERIODO;
            }
            set {
                this._PERIODO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer EJERCICIO
        /// </summary>
        [DataNames("EJERCICIO")]
        [SugarColumn(ColumnName = "ejercicio", ColumnDescription = "Ejercicio", IsNullable = false)]
        public int Ejercicio {
            get {
                return this._EJERCICIO;
            }
            set {
                this._EJERCICIO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer FECHA_POL, La fecha de contabilizacion va en la tercera columna y tercera fila
        /// </summary>
        [DataNames("FECHA_POL")]
        [SugarColumn(ColumnName = "fecha_pol", ColumnDescription = "Fecha poliza")]
        public DateTime? Fecha {
            get {
                return this._FECHA_POL;
            }
            set {
                this._FECHA_POL = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer CONCEP_PO, El concepto de la poliza. <CONCEPTO> (120 caracteres) debe de ir en la segunda columna y tercera fila a continuacion del tipo de poliza.
        /// </summary>
        [DataNames("CONCEP_PO")]
        [SugarColumn(ColumnName = "concep_po", ColumnDescription = "Concepto", Length = 120)]
        public string Concepto {
            get {
                return this._CONCEP_PO;
            }
            set {
                this._CONCEP_PO = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer NUM_PART
        /// </summary>
        [DataNames("NUM_PART")]
        [SugarColumn(ColumnName = "num_part", ColumnDescription = "Numero de partida")]
        public int? NumPartida {
            get {
                return this._NUM_PART;
            }
            set {
                this._NUM_PART = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer LOGAUDITA
        /// </summary>
        [DataNames("LOGAUDITA")]
        [SugarColumn(ColumnName = "logaudita", ColumnDescription = "Poliza auditada", Length = 1)]
        public string LOGAUDITA {
            get {
                return this._LOGAUDITA;
            }
            set {
                this._LOGAUDITA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer CONTABILIZ
        /// </summary>
        [DataNames("CONTABILIZ")]
        [SugarColumn(ColumnName = "contabiliz", ColumnDescription = "Esta contabilizada", Length = 1)]
        public string CONTABILIZ {
            get {
                return this._CONTABILIZ;
            }
            set {
                this._CONTABILIZ = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer NUMPARCUA
        /// </summary>
        [DataNames("NUMPARCUA")]
        [SugarColumn(ColumnName = "numparcua", ColumnDescription = "Numero de partida de la cuenta")]
        public int? NUMPARCUA {
            get {
                return this._NUMPARCUA;
            }
            set {
                this._NUMPARCUA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer TIENEDOCUMENTOS
        /// </summary>
        [DataNames("TIENEDOCUMENTOS")]
        [SugarColumn(ColumnName = "tienedocumentos", ColumnDescription = "Tiene documentos asosciados")]
        public int? TIENEDOCUMENTOS {
            get {
                return this._TIENEDOCUMENTOS;
            }
            set {
                this._TIENEDOCUMENTOS = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer PROCCONTAB
        /// </summary>
        [DataNames("PROCCONTAB")]
        [SugarColumn(ColumnName = "proccontab", ColumnDescription = "Numero de transaccion para la interfaz")]
        public int? PROCCONTAB {
            get {
                return this._PROCCONTAB;
            }
            set {
                this._PROCCONTAB = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer ORIGEN
        /// </summary>
        [DataNames("ORIGEN")]
        [SugarColumn(ColumnName = "origen", ColumnDescription = "Origen", Length = 15)]
        public string ORIGEN {
            get {
                return this._ORIGEN;
            }
            set {
                this._ORIGEN = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer UUID
        /// </summary>
        [DataNames("UUID")]
        [SugarColumn(ColumnName = "uuid", ColumnDescription = "UUID del comprobante", Length = 100)]
        public string UUID {
            get {
                return this._UUID;
            }
            set {
                this._UUID = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer ESPOLIZAPRIVADA
        /// </summary>
        [DataNames("ESPOLIZAPRIVADA")]
        [SugarColumn(ColumnName = "espolizaprivada", ColumnDescription = "Es poliza privada")]
        public int? ESPOLIZAPRIVADA {
            get {
                return this._ESPOLIZAPRIVADA;
            }
            set {
                this._ESPOLIZAPRIVADA = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer UUID de la operacion asociada (UUIDOP)
        /// </summary>
        [DataNames("UUIDOP")]
        [SugarColumn(ColumnName = "uuidop", ColumnDescription = "UUID de la operacion asociada", Length = 40)]
        public string UUIDOP {
            get {
                return this._UUIDOP;
            }
            set {
                this._UUIDOP = value;
                this.OnPropertyChanged();
            }
        }
    }
}
