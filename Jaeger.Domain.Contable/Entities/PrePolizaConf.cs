﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaConf : PrePolizaModel {
        private System.ComponentModel.BindingList<PrePolizaAuxiliarConf> _Auxiliar;

        public PrePolizaConf() {
            this.Auxiliar = new System.ComponentModel.BindingList<PrePolizaAuxiliarConf>();
        }

        public System.ComponentModel.BindingList<PrePolizaAuxiliarConf> Auxiliar {
            get { return this._Auxiliar; }
            set { this._Auxiliar = value;
                this.OnPropertyChanged();
            }
        }

        public string ToJson() {
            Formatting objFormat = 0;
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static PrePolizaConf FromJson(string json) {
            return JsonConvert.DeserializeObject<PrePolizaConf>(json);
        }
    }
}
