﻿namespace Jaeger.Domain.Contable.Entities {
    public class PrePolizaAuxiliarConf : PrePolizaAuxiliarModel {
        #region
        private string _Propiedad;
        #endregion

        public PrePolizaAuxiliarConf() {

        }

        public string Propiedad {
            get { return this._Propiedad; }
            set { this._Propiedad = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsDetail { get; set; }
        public bool IsCargo { get; set; }

        public bool AsociaCFDI { get; set; }
    }
}
