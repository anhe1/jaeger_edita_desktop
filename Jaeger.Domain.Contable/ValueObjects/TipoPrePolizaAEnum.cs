﻿using System.ComponentModel;

namespace Jaeger.Domain.Contable.ValueObjects {
    public enum TipoPrePolizaAEnum {
        [Description("Ingresos")]
        Ig = 1,
        [Description("Egresos")]
        Eg = 2,
        [Description("Diario")]
        Dr = 3
    }
}
