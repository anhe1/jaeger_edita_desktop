﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Group = new Telerik.WinControls.UI.RadGroupBox();
            this.Costo = new Telerik.WinControls.UI.RadSpinEditor();
            this.CostoLabel = new Telerik.WinControls.UI.RadLabel();
            this.Detector = new Telerik.WinControls.UI.RadTextBox();
            this.Search = new Telerik.WinControls.UI.RadButton();
            this.Cantidad = new Telerik.WinControls.UI.RadSpinEditor();
            this.CantidadLabel = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.lblDescripcion = new Telerik.WinControls.UI.RadLabel();
            this.DetectorLbl = new Telerik.WinControls.UI.RadLabel();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.lblDepartamento = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            this.TDocumento = new Jaeger.UI.Common.Forms.TbDocumentoControl();
            ((System.ComponentModel.ISupportInitialize)(this.Group)).BeginInit();
            this.Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Costo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Detector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CantidadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetectorLbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            this.SuspendLayout();
            // 
            // Group
            // 
            this.Group.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Group.Controls.Add(this.Costo);
            this.Group.Controls.Add(this.CostoLabel);
            this.Group.Controls.Add(this.Detector);
            this.Group.Controls.Add(this.Search);
            this.Group.Controls.Add(this.Cantidad);
            this.Group.Controls.Add(this.CantidadLabel);
            this.Group.Controls.Add(this.Descripcion);
            this.Group.Controls.Add(this.lblDescripcion);
            this.Group.Controls.Add(this.DetectorLbl);
            this.Group.Controls.Add(this.Departamento);
            this.Group.Controls.Add(this.Nota);
            this.Group.Controls.Add(this.lblDepartamento);
            this.Group.Controls.Add(this.FechaEmision);
            this.Group.Controls.Add(this.lblFechaEmision);
            this.Group.Controls.Add(this.lblNota);
            this.Group.Controls.Add(this.Folio);
            this.Group.Controls.Add(this.lblFolio);
            this.Group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Group.HeaderText = "";
            this.Group.Location = new System.Drawing.Point(0, 30);
            this.Group.Margin = new System.Windows.Forms.Padding(0);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(1175, 102);
            this.Group.TabIndex = 93;
            this.Group.TabStop = false;
            // 
            // Costo
            // 
            this.Costo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Costo.DecimalPlaces = 2;
            this.Costo.Location = new System.Drawing.Point(1085, 76);
            this.Costo.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.Costo.Name = "Costo";
            this.Costo.Size = new System.Drawing.Size(80, 20);
            this.Costo.TabIndex = 382;
            this.Costo.TabStop = false;
            this.Costo.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CostoLabel
            // 
            this.CostoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CostoLabel.Location = new System.Drawing.Point(1025, 77);
            this.CostoLabel.Name = "CostoLabel";
            this.CostoLabel.Size = new System.Drawing.Size(38, 18);
            this.CostoLabel.TabIndex = 383;
            this.CostoLabel.Text = "Costo:";
            // 
            // Detector
            // 
            this.Detector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Detector.Location = new System.Drawing.Point(931, 53);
            this.Detector.Name = "Detector";
            this.Detector.Size = new System.Drawing.Size(208, 20);
            this.Detector.TabIndex = 383;
            // 
            // Search
            // 
            this.Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Search.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Search.Image = global::Jaeger.UI.CCalidad.Properties.Resources.search_16;
            this.Search.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Search.Location = new System.Drawing.Point(1145, 54);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(20, 20);
            this.Search.TabIndex = 382;
            // 
            // Cantidad
            // 
            this.Cantidad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Cantidad.Location = new System.Drawing.Point(931, 77);
            this.Cantidad.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Size = new System.Drawing.Size(80, 20);
            this.Cantidad.TabIndex = 380;
            this.Cantidad.TabStop = false;
            this.Cantidad.TextAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CantidadLabel
            // 
            this.CantidadLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CantidadLabel.Location = new System.Drawing.Point(871, 78);
            this.CantidadLabel.Name = "CantidadLabel";
            this.CantidadLabel.Size = new System.Drawing.Size(54, 18);
            this.CantidadLabel.TabIndex = 381;
            this.CantidadLabel.Text = "Cantidad:";
            // 
            // Descripcion
            // 
            this.Descripcion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Descripcion.AutoSize = false;
            this.Descripcion.Location = new System.Drawing.Point(8, 28);
            this.Descripcion.MaxLength = 510;
            this.Descripcion.Multiline = true;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.NullText = "Descripción";
            this.Descripcion.Size = new System.Drawing.Size(854, 46);
            this.Descripcion.TabIndex = 379;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.Location = new System.Drawing.Point(9, 6);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(67, 18);
            this.lblDescripcion.TabIndex = 94;
            this.lblDescripcion.Text = "Descripción:";
            // 
            // DetectorLbl
            // 
            this.DetectorLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DetectorLbl.Location = new System.Drawing.Point(868, 54);
            this.DetectorLbl.Name = "DetectorLbl";
            this.DetectorLbl.Size = new System.Drawing.Size(52, 18);
            this.DetectorLbl.TabIndex = 89;
            this.DetectorLbl.Text = "Detector:";
            // 
            // Departamento
            // 
            this.Departamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Departamento.AutoSizeDropDownToBestFit = true;
            this.Departamento.DisplayMember = "Nombre";
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "IdDepartamento";
            gridViewTextBoxColumn13.HeaderText = "Clave";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "IdDepartamento";
            gridViewTextBoxColumn14.FieldName = "Nombre";
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Descripcion";
            gridViewTextBoxColumn15.FieldName = "Descriptor";
            gridViewTextBoxColumn15.HeaderText = "Descriptor";
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "Descriptor";
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(955, 29);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(210, 20);
            this.Departamento.TabIndex = 32;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "IdDepartamento";
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(99, 77);
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Observaciones";
            this.Nota.Size = new System.Drawing.Size(763, 20);
            this.Nota.TabIndex = 19;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDepartamento.Location = new System.Drawing.Point(868, 30);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(81, 18);
            this.lblDepartamento.TabIndex = 31;
            this.lblDepartamento.Text = "Departamento:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Location = new System.Drawing.Point(954, 4);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 7;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(867, 5);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(85, 18);
            this.lblFechaEmision.TabIndex = 6;
            this.lblFechaEmision.Text = "Fec. de Emisión:";
            // 
            // lblNota
            // 
            this.lblNota.Location = new System.Drawing.Point(9, 78);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(81, 18);
            this.lblNota.TabIndex = 18;
            this.lblNota.Text = "Observaciones:";
            this.lblNota.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // Folio
            // 
            this.Folio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Folio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.Location = new System.Drawing.Point(770, 4);
            this.Folio.Name = "Folio";
            this.Folio.NullText = "Folio";
            this.Folio.Size = new System.Drawing.Size(92, 20);
            this.Folio.TabIndex = 5;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFolio
            // 
            this.lblFolio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFolio.Location = new System.Drawing.Point(731, 5);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 4;
            this.lblFolio.Text = "Folio:";
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // TDocumento
            // 
            this.TDocumento.Dock = System.Windows.Forms.DockStyle.Top;
            this.TDocumento.Location = new System.Drawing.Point(0, 0);
            this.TDocumento.Name = "TDocumento";
            this.TDocumento.ShowActualizar = true;
            this.TDocumento.ShowComplemento = false;
            this.TDocumento.ShowEnviar = false;
            this.TDocumento.ShowGuardar = true;
            this.TDocumento.ShowIdDocumento = true;
            this.TDocumento.ShowImprimir = true;
            this.TDocumento.ShowPDF = true;
            this.TDocumento.Size = new System.Drawing.Size(1175, 30);
            this.TDocumento.TabIndex = 3;
            // 
            // NoConformidadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Group);
            this.Controls.Add(this.TDocumento);
            this.Name = "NoConformidadControl";
            this.Size = new System.Drawing.Size(1175, 132);
            this.Load += new System.EventHandler(this.NoConformidadControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Group)).EndInit();
            this.Group.ResumeLayout(false);
            this.Group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Costo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Detector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CantidadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetectorLbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal Common.Forms.TbDocumentoControl TDocumento;
        private Telerik.WinControls.UI.RadGroupBox Group;
        internal Telerik.WinControls.UI.RadLabel lblDescripcion;
        internal Telerik.WinControls.UI.RadLabel DetectorLbl;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        protected internal Telerik.WinControls.UI.RadTextBox Nota;
        internal Telerik.WinControls.UI.RadLabel lblDepartamento;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblNota;
        internal Telerik.WinControls.UI.RadTextBox Folio;
        internal Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadTextBox Descripcion;
        private System.Windows.Forms.ErrorProvider Advertencia;
        internal Telerik.WinControls.UI.RadSpinEditor Cantidad;
        internal Telerik.WinControls.UI.RadLabel CantidadLabel;
        public Telerik.WinControls.UI.RadButton Search;
        internal Telerik.WinControls.UI.RadTextBox Detector;
        internal Telerik.WinControls.UI.RadSpinEditor Costo;
        internal Telerik.WinControls.UI.RadLabel CostoLabel;
    }
}
