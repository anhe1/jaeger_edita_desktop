﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Aplication.CCalidad.Contracts;
using Telerik.WinControls;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class AccionCorrectivaForm : RadForm {
        protected internal AccionCorrectivaDetailModel _Current;
        protected internal INoConformidadesService _Service;

        public event EventHandler<AccionCorrectivaDetailModel> Selected;
        public void OnSelected(AccionCorrectivaDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public AccionCorrectivaForm(INoConformidadesService service, AccionCorrectivaDetailModel model) {
            InitializeComponent();
            if (model == null) {
                this._Current = new AccionCorrectivaDetailModel();
            } else {
                _Current = model;
            }
            this._Service = service;
        }

        private void AccionCorrectivaForm_Load(object sender, EventArgs e) {
            TipoAccion.DataSource = NoConformidadService.GetTipoAccion();
            TipoAccion.DisplayMember = "Descriptor";
            TipoAccion.ValueMember = "Id";
            TipoAccion.SelectedValue = this._Current.IdTipoAccion;

            this.Fecha.Value = this._Current.Fecha;
            this.Descripcion.Text = this._Current.Descripcion;
            this.Departamento.DataSource = this._Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());
            this.Departamento.ValueMember = "IdDepartamento";
            this.Departamento.SelectedValue = this._Current.IdDepartamento;
        }

        protected virtual void TButton_Aceptar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Existen errore en la captura, por favor revisar y volver a intentar", "Atención", System.Windows.Forms.MessageBoxButtons.OK);
                return;
            }
            this._Current.Descripcion = this.Descripcion.Text;
            this._Current.Fecha = this.Fecha.Value;
            this._Current.IdTipoAccion = (int)this.TipoAccion.SelectedValue;
            this._Current.IdDepartamento = (int)this.Departamento.SelectedValue;
            this._Current.Responsable = this.Responsable.Text;
            this.OnSelected(_Current);
            this.Close();
        }

        protected virtual void TButton_Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual bool Validar() {
            this.ProveedorError.Clear();
            if (string.IsNullOrEmpty(this.Descripcion.Text)) {
                this.ProveedorError.SetError(this.Descripcion, "Este campo es necesario.");
                return false;
            }

            if (string.IsNullOrEmpty(this.Responsable.Text)) {
                this.ProveedorError.SetError(this.Responsable, "Este campo es necesario.");
                return false;
            }
            return true;
        }

        private void Search_Click(object sender, EventArgs e) {
            var search = new EmpleadoBuscar(this._Service);
            search.Selected += Search_Selected;
            search.ShowDialog(this);
        }

        private void Search_Selected(object sender, EmpleadoModel e) {
            if (e != null) {
                this.Responsable.Text = e.Nombre;
                if (e.Id > 0) {
                    //this._Current.IdResponsable = e.Id;
                }
            }
        }
    }
}
