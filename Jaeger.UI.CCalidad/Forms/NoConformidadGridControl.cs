﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.CCalidad.Builder;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CCalidad.Forms {
    public class NoConformidadGridControl : UI.Common.Forms.GridCommonControl {
        #region
        public INoConformidadesService Service;
        public RadMenuItem ImprimirComprobante = new RadMenuItem { Text = "Comprobante" };
        public RadMenuItem Autorizar = new RadMenuItem { Text = "Autorizar" };
        protected internal List<DepartamentoModel> Departamentos;
        protected internal GridViewTemplate CausaRaizGridData = new GridViewTemplate() { Caption = "Análisis Causa - Raíz" };
        protected internal GridViewTemplate AccionesCorrectivasGridData = new GridViewTemplate() { Caption = "Acciones Correctivas" };
        protected internal GridViewTemplate AccionesPreventivasGridData = new GridViewTemplate() { Caption = "Acciones Preventivas" };
        protected internal GridViewTemplate CostoNoCalidadGridData = new GridViewTemplate() { Caption = "Costo de No Calidad" };
        protected internal GridViewTemplate OrdenRelacionadaGridData = new GridViewTemplate() { Caption = "Orden Relacionada" };
        protected internal bool ActualizarRegistro = false;
        #endregion

        public NoConformidadGridControl() : base() {
            this.Load += NoConformidadGridControl_Load;
        }

        private void NoConformidadGridControl_Load(object sender, EventArgs e) {
            this.InitializeComponent();
            this.Actualizar.Click += TNoConformidad_Actualizar_Click;
            this.ShowImprimir = true;
            this.Imprimir.Items.Add(this.ImprimirComprobante);
        }

        protected internal void InitializeComponent() {
            this.DataSource = null;
            this.Service = null;
            this.GridData.Columns.Clear();
            this.CausaRaizGridData.Columns.Clear();
            this.CausaRaizGridData.Standard();
            this.AccionesCorrectivasGridData.Columns.Clear();
            this.AccionesCorrectivasGridData.Standard();
            this.AccionesPreventivasGridData.Columns.Clear();
            this.AccionesPreventivasGridData.Standard();
            this.CostoNoCalidadGridData.Columns.Clear();
            this.CostoNoCalidadGridData.Standard();
            this.OrdenRelacionadaGridData.Columns.Clear();
            this.OrdenRelacionadaGridData.Standard();

            using (INoConformidadGridBuilder view = new NoConformidadGridBuilder()) {
                this.GridData.Columns.AddRange(view.Templetes().Master().Build());
                this.CausaRaizGridData.Columns.AddRange(view.Templetes().CausaRaiz().Build());
                this.AccionesCorrectivasGridData.Columns.AddRange(view.Templetes().AccionesCorrectivas().Build());
                this.AccionesPreventivasGridData.Columns.AddRange(view.Templetes().AccionesPreventivas().Build());
                this.OrdenRelacionadaGridData.Columns.AddRange(view.Templetes().OrdenProduccion().Build());
                this.CostoNoCalidadGridData.Columns.AddRange(view.Templetes().CostoNoCalidad().Build());
            }

            var status = this.GridData.Columns["IdStatus"] as GridViewComboBoxColumn;
            status.DataSource = NoConformidadService.GetStatus();

            this.GridData.ReadOnly = false;
            this.GridData.MasterTemplate.Templates.AddRange(this.CausaRaizGridData, this.AccionesCorrectivasGridData, this.AccionesPreventivasGridData, this.OrdenRelacionadaGridData, this.CostoNoCalidadGridData);
            this.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.CausaRaizGridData.HierarchyDataProvider = new GridViewEventDataProvider(this.CausaRaizGridData);
            this.AccionesCorrectivasGridData.HierarchyDataProvider = new GridViewEventDataProvider(this.AccionesCorrectivasGridData);
            this.AccionesPreventivasGridData.HierarchyDataProvider = new GridViewEventDataProvider(this.AccionesPreventivasGridData);
            this.CostoNoCalidadGridData.HierarchyDataProvider = new GridViewEventDataProvider(this.CostoNoCalidadGridData);
            this.OrdenRelacionadaGridData.HierarchyDataProvider = new GridViewEventDataProvider(this.OrdenRelacionadaGridData);
        }

        public BindingList<NoConformidadSingleModel> DataSource { get; set; }

        protected virtual void TNoConformidad_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = DataSource;
        }

        #region acciones del grid
        protected override void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            // si es la zona de filtro retornamos
            if (e.Row is GridViewFilteringRowInfo) { return; }
            if (e.Column.Name == "IdStatus") {
                if (this._permisos.Status) {
                    var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                    switch (seleccionado.Status) {
                        case Domain.CCalidad.ValueObjects.NoConformidadStatusEnum.Cancelado:
                            e.Cancel = true;
                            break;
                        case Domain.CCalidad.ValueObjects.NoConformidadStatusEnum.Auditado:
                            e.Cancel = true;
                            break;
                        default:
                            break;
                    }
                }
            } else {
                e.Cancel = true;
            }
        }

        protected override void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (e.Row is GridViewFilteringRowInfo | e.Column == null) { return; }
            if (e.Column.Name == "IdStatus") {
                if (e.ActiveEditor != null) {
                    if (e.OldValue == e.Value) {
                        e.Cancel = true;
                    } else {
                        if ((int)e.Value == (int)Domain.CCalidad.ValueObjects.NoConformidadStatusEnum.Cancelado) {
                            e.Cancel = true;
                            this.GridData.CancelEdit();
                            RadMessageBox.Show(this, Properties.Resources.msg_objeto_Cancelar_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Error);
                            return;
                        }
                        this.ActualizarRegistro = true;
                    }
                }
            }
        }

        protected override void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Column.Name == "IdStatus") {
                if (this.ActualizarRegistro == true) {
                    var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                    var response = new NoConformidadStatusModel {
                        IdNoConformidad = seleccionado.IdNoConformidad,
                        CvMotivo = "",
                        Usuario = ConfigService.Piloto.Clave,
                        FechaNuevo = DateTime.Now,
                        IdStatusA = seleccionado.IdStatus,
                        IdStatusB = (int)e.Value,
                        Nota = "",
                        IdNoConformidadR = 0
                    };
                    seleccionado.Tag = response;
                    using (var espera = new Waiting2Form(this.SetStatus)) {
                        espera.Text = "Aplicando, espere un momento ...";
                        espera.ShowDialog(this);
                    }
                    this.ActualizarRegistro = false;
                }
            }
        }

        protected virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.CausaRaizGridData.Caption) {
                using (var espera = new Waiting1Form(this.GetComprobante)) {
                    espera.ShowDialog(this);
                }
                var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.CausaRaiz != null) {
                        foreach (var item in seleccionado.CausaRaiz) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["IdNoConformidad"].Value = item.IdNoConformidad;
                            row.Cells["IdClasificacion5"].Value = item.IdClasificacion5;
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            row.Cells["IdEfecto"].Value = item.IdEfecto;
                            row.Cells["IdDepartamento"].Value = item.IdDepartamento;
                            row.Cells["Responsable"].Value = item.Responsable;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.AccionesCorrectivasGridData.Caption) {
                var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.AccionesCorrectivas != null) {
                        foreach (var item in seleccionado.AccionesCorrectivas) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["IdTipoAccion"].Value = item.IdTipoAccion;
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            row.Cells["IdDepartamento"].Value = item.IdDepartamento;
                            row.Cells["Responsable"].Value = item.Responsable;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.AccionesPreventivasGridData.Caption) {
                var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.AccionesPreventivas != null) {
                        foreach (var item in seleccionado.AccionesPreventivas) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            row.Cells["Fecha"].Value = item.Fecha;
                            row.Cells["Responsable"].Value = item.Responsable;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.OrdenRelacionadaGridData.Caption) {
                var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.OrdenProduccion != null) {
                        foreach (var item in seleccionado.OrdenProduccion) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["IdOrden"].Value = item.IdOrden;
                            row.Cells["Cliente"].Value = item.Cliente;
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            } else if (e.Template.Caption == this.CostoNoCalidadGridData.Caption) {
                var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
                if (seleccionado != null) {
                    if (seleccionado.CostosNoCalidad != null) {
                        foreach (var item in seleccionado.CostosNoCalidad) {
                            var row = e.Template.Rows.NewRow();
                            row.Cells["IdOrden"].Value = item.IdOrden;
                            row.Cells["Cliente"].Value = item.Cliente;
                            row.Cells["Descripcion"].Value = item.Descripcion;
                            row.Cells["Maquinaria"].Value = item.Maquinaria;
                            row.Cells["ManoObra"].Value = item.ManoObra;
                            row.Cells["MateriaPrima"].Value = item.MateriaPrima;
                            row.Cells["Consumibles"].Value = item.Consumibles;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos
        protected virtual void Consultar() {
            var query = NoConformidadService.Query().Year(this.GetEjercicio());
            // si el periodo es visible
            if (this.ShowPeriodo == true) {
                query.Month(this.GetPeriodo());
            } else {
                query.Status(Domain.CCalidad.ValueObjects.NoConformidadStatusEnum.Proceso);
            }

            this.DataSource = new BindingList<NoConformidadSingleModel>(this.Service.GetList<NoConformidadSingleModel>(query.Build()));
            this.Departamentos = this.Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());

            var departamento = this.GridData.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamento.DisplayMember = "Nombre";
            departamento.ValueMember = "IdDepartamento";
            departamento.DataSource = this.Departamentos;

            var departamento1 = this.CausaRaizGridData.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamento1.DisplayMember = "Nombre";
            departamento1.ValueMember = "IdDepartamento";
            departamento1.DataSource = new List<DepartamentoModel>(this.Departamentos);

            var clasificacion = this.CausaRaizGridData.Columns["IdClasificacion5"] as GridViewComboBoxColumn;
            clasificacion.DisplayMember = "Descriptor";
            clasificacion.ValueMember = "Id";
            clasificacion.DataSource = NoConformidadService.GetClasificacion();

            var efecto = this.CausaRaizGridData.Columns["IdEfecto"] as GridViewComboBoxColumn;
            efecto.DisplayMember = "Descriptor";
            efecto.ValueMember = "Id";
            efecto.DataSource = NoConformidadService.GetClasificacion();

            var departamento2 = this.AccionesCorrectivasGridData.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamento2.DisplayMember = "Nombre";
            departamento2.ValueMember = "IdDepartamento";
            departamento2.DataSource = new List<DepartamentoModel>(this.Departamentos);

            var idTipoAccion = this.AccionesCorrectivasGridData.Columns["IdTipoAccion"] as GridViewComboBoxColumn;
            idTipoAccion.DisplayMember = "Descriptor";
            idTipoAccion.ValueMember = "Id";
            idTipoAccion.DataSource = NoConformidadService.GetTipoAccion();
        }

        protected virtual void GetComprobante() {
            var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                var d = this.Service.GetById(seleccionado.IdNoConformidad);
                if (d != null) {
                    seleccionado.SetValues(d);
                }
            }
        }

        protected virtual void SetStatus() {
            var seleccionado = this.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                if (seleccionado.Tag != null) {
                    this.Service.Save(seleccionado);
                }
            }
        }
        #endregion
    }
}
