﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class CertificadoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pHeader = new System.Windows.Forms.PictureBox();
            this.lblTitulo = new Telerik.WinControls.UI.RadLabel();
            this.lblPedido = new Telerik.WinControls.UI.RadLabel();
            this.IdPedido = new Telerik.WinControls.UI.RadTextBox();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblGuia = new Telerik.WinControls.UI.RadLabel();
            this.Cliente = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Leyenda = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Desviacion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.Imprimir = new Telerik.WinControls.UI.RadButton();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.pHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitulo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGuia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Leyenda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Desviacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imprimir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pHeader
            // 
            this.pHeader.BackColor = System.Drawing.Color.White;
            this.pHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pHeader.Location = new System.Drawing.Point(0, 0);
            this.pHeader.Name = "pHeader";
            this.pHeader.Size = new System.Drawing.Size(569, 45);
            this.pHeader.TabIndex = 3;
            this.pHeader.TabStop = false;
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.White;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(12, 12);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(403, 28);
            this.lblTitulo.TabIndex = 99;
            this.lblTitulo.Text = "CERTIFICADO DE CALIDAD INTERNO\r\nLIBERACIÓN DE PRODUCTO TERMINADO PARA ENTREGA AL " +
    "CLIENTE";
            // 
            // lblPedido
            // 
            this.lblPedido.Location = new System.Drawing.Point(10, 60);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(53, 18);
            this.lblPedido.TabIndex = 456;
            this.lblPedido.Text = "# Pedido:";
            // 
            // IdPedido
            // 
            this.IdPedido.Location = new System.Drawing.Point(62, 59);
            this.IdPedido.MaxLength = 16;
            this.IdPedido.Name = "IdPedido";
            this.IdPedido.NullText = "# Pedido";
            this.IdPedido.ReadOnly = true;
            this.IdPedido.Size = new System.Drawing.Size(82, 20);
            this.IdPedido.TabIndex = 457;
            this.IdPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FechaEmision
            // 
            this.FechaEmision.CustomFormat = "dd/MMMM/yyyy";
            this.FechaEmision.Enabled = false;
            this.FechaEmision.Location = new System.Drawing.Point(241, 59);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.ReadOnly = true;
            this.FechaEmision.Size = new System.Drawing.Size(211, 20);
            this.FechaEmision.TabIndex = 453;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "miércoles, 27 de septiembre de 2017";
            this.FechaEmision.Value = new System.DateTime(2017, 9, 27, 19, 9, 13, 971);
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Location = new System.Drawing.Point(168, 60);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 452;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblGuia
            // 
            this.lblGuia.Location = new System.Drawing.Point(10, 85);
            this.lblGuia.Name = "lblGuia";
            this.lblGuia.Size = new System.Drawing.Size(43, 18);
            this.lblGuia.TabIndex = 458;
            this.lblGuia.Text = "Cliente:";
            // 
            // Cliente
            // 
            this.Cliente.Location = new System.Drawing.Point(62, 85);
            this.Cliente.MaxLength = 32;
            this.Cliente.Name = "Cliente";
            this.Cliente.NullText = "Descripción";
            this.Cliente.ReadOnly = true;
            this.Cliente.Size = new System.Drawing.Size(497, 20);
            this.Cliente.TabIndex = 459;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(10, 111);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 460;
            this.radLabel1.Text = "Descripción:";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(83, 111);
            this.Descripcion.MaxLength = 32;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.NullText = "Descripción";
            this.Descripcion.ReadOnly = true;
            this.Descripcion.Size = new System.Drawing.Size(476, 20);
            this.Descripcion.TabIndex = 461;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(10, 137);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(50, 18);
            this.radLabel2.TabIndex = 462;
            this.radLabel2.Text = "Leyenda:";
            // 
            // Leyenda
            // 
            this.Leyenda.AutoSize = false;
            this.Leyenda.Location = new System.Drawing.Point(10, 163);
            this.Leyenda.MaxLength = 32;
            this.Leyenda.Multiline = true;
            this.Leyenda.Name = "Leyenda";
            this.Leyenda.NullText = "Descripción";
            this.Leyenda.ReadOnly = true;
            this.Leyenda.Size = new System.Drawing.Size(549, 80);
            this.Leyenda.TabIndex = 463;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(9, 249);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(119, 18);
            this.radLabel3.TabIndex = 464;
            this.radLabel3.Text = "Permiso de Desviación";
            // 
            // Desviacion
            // 
            this.Desviacion.AutoSize = false;
            this.Desviacion.Location = new System.Drawing.Point(10, 272);
            this.Desviacion.MaxLength = 32;
            this.Desviacion.Multiline = true;
            this.Desviacion.Name = "Desviacion";
            this.Desviacion.NullText = "Descripción";
            this.Desviacion.ReadOnly = true;
            this.Desviacion.Size = new System.Drawing.Size(548, 80);
            this.Desviacion.TabIndex = 465;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(9, 358);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(81, 18);
            this.radLabel4.TabIndex = 466;
            this.radLabel4.Text = "Observaciones:";
            // 
            // Nota
            // 
            this.Nota.AutoSize = false;
            this.Nota.Location = new System.Drawing.Point(10, 379);
            this.Nota.MaxLength = 32;
            this.Nota.Multiline = true;
            this.Nota.Name = "Nota";
            this.Nota.NullText = "Descripción";
            this.Nota.Size = new System.Drawing.Size(548, 40);
            this.Nota.TabIndex = 467;
            // 
            // Imprimir
            // 
            this.Imprimir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Imprimir.Location = new System.Drawing.Point(331, 439);
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Size = new System.Drawing.Size(110, 24);
            this.Imprimir.TabIndex = 468;
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.Click += new System.EventHandler(this.Imprimir_Click);
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cerrar.Location = new System.Drawing.Point(447, 439);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(110, 24);
            this.Cerrar.TabIndex = 469;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // CertificadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cerrar;
            this.ClientSize = new System.Drawing.Size(569, 475);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.Imprimir);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.Nota);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.Desviacion);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.Leyenda);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.Descripcion);
            this.Controls.Add(this.lblGuia);
            this.Controls.Add(this.Cliente);
            this.Controls.Add(this.lblPedido);
            this.Controls.Add(this.IdPedido);
            this.Controls.Add(this.FechaEmision);
            this.Controls.Add(this.lblFechaEmision);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.pHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CertificadoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Certificado";
            this.Load += new System.EventHandler(this.OrdenProduccionCertificadoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitulo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGuia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Leyenda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Desviacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imprimir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pHeader;
        private Telerik.WinControls.UI.RadLabel lblTitulo;
        internal Telerik.WinControls.UI.RadLabel lblPedido;
        internal Telerik.WinControls.UI.RadTextBox IdPedido;
        internal Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblFechaEmision;
        internal Telerik.WinControls.UI.RadLabel lblGuia;
        internal Telerik.WinControls.UI.RadTextBox Cliente;
        internal Telerik.WinControls.UI.RadLabel radLabel1;
        internal Telerik.WinControls.UI.RadTextBox Descripcion;
        internal Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadTextBox Leyenda;
        internal Telerik.WinControls.UI.RadLabel radLabel3;
        internal Telerik.WinControls.UI.RadTextBox Desviacion;
        internal Telerik.WinControls.UI.RadLabel radLabel4;
        internal Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadButton Imprimir;
        private Telerik.WinControls.UI.RadButton Cerrar;
    }
}