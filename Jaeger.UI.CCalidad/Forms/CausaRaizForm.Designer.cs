﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class CausaRaizForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.InfoGroup = new Telerik.WinControls.UI.RadGroupBox();
            this.ResponsableCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.Search = new Telerik.WinControls.UI.RadButton();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.Responsable = new Telerik.WinControls.UI.RadTextBox();
            this.DescripcionLabel = new Telerik.WinControls.UI.RadLabel();
            this.ResponsableLabel = new Telerik.WinControls.UI.RadLabel();
            this.DepartamentoLabel = new Telerik.WinControls.UI.RadLabel();
            this.Efecto = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.EfectoLabel = new Telerik.WinControls.UI.RadLabel();
            this.Clasificacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ClasificacionLabel = new Telerik.WinControls.UI.RadLabel();
            this.Cancelar = new Telerik.WinControls.UI.RadButton();
            this.Aceptar = new Telerik.WinControls.UI.RadButton();
            this.HeaderLabel = new Telerik.WinControls.UI.RadLabel();
            this.ProveedorError = new System.Windows.Forms.ErrorProvider(this.components);
            this.Header = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.InfoGroup)).BeginInit();
            this.InfoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsableCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsableLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartamentoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfectoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProveedorError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // InfoGroup
            // 
            this.InfoGroup.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.InfoGroup.Controls.Add(this.ResponsableCheck);
            this.InfoGroup.Controls.Add(this.Search);
            this.InfoGroup.Controls.Add(this.Departamento);
            this.InfoGroup.Controls.Add(this.Descripcion);
            this.InfoGroup.Controls.Add(this.Responsable);
            this.InfoGroup.Controls.Add(this.DescripcionLabel);
            this.InfoGroup.Controls.Add(this.ResponsableLabel);
            this.InfoGroup.Controls.Add(this.DepartamentoLabel);
            this.InfoGroup.Controls.Add(this.Efecto);
            this.InfoGroup.Controls.Add(this.EfectoLabel);
            this.InfoGroup.Controls.Add(this.Clasificacion);
            this.InfoGroup.Controls.Add(this.ClasificacionLabel);
            this.InfoGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoGroup.HeaderText = "Descripción";
            this.InfoGroup.Location = new System.Drawing.Point(0, 50);
            this.InfoGroup.Name = "InfoGroup";
            this.InfoGroup.Size = new System.Drawing.Size(620, 175);
            this.InfoGroup.TabIndex = 1;
            this.InfoGroup.Text = "Descripción";
            // 
            // ResponsableCheck
            // 
            this.ResponsableCheck.Location = new System.Drawing.Point(432, 74);
            this.ResponsableCheck.Name = "ResponsableCheck";
            this.ResponsableCheck.Size = new System.Drawing.Size(94, 18);
            this.ResponsableCheck.TabIndex = 375;
            this.ResponsableCheck.Text = "Es responsable";
            // 
            // Search
            // 
            this.Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Search.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Search.Image = global::Jaeger.UI.CCalidad.Properties.Resources.search_16;
            this.Search.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Search.Location = new System.Drawing.Point(588, 46);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(20, 20);
            this.Search.TabIndex = 374;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // Departamento
            // 
            this.Departamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Departamento.AutoSizeDropDownToBestFit = true;
            this.Departamento.DisplayMember = "Nombre";
            this.Departamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn8.DataType = typeof(int);
            gridViewTextBoxColumn8.FieldName = "IdDepartamento";
            gridViewTextBoxColumn8.HeaderText = "Clave";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "IdDepartamento";
            gridViewTextBoxColumn9.FieldName = "Nombre";
            gridViewTextBoxColumn9.HeaderText = "Descripción";
            gridViewTextBoxColumn9.Name = "Descripcion";
            gridViewTextBoxColumn10.FieldName = "Descriptor";
            gridViewTextBoxColumn10.HeaderText = "Descriptor";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "Descriptor";
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(110, 45);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(229, 20);
            this.Departamento.TabIndex = 33;
            this.Departamento.TabStop = false;
            this.Departamento.ValueMember = "IdDepartamento";
            // 
            // Descripcion
            // 
            this.Descripcion.AutoSize = false;
            this.Descripcion.Location = new System.Drawing.Point(12, 98);
            this.Descripcion.MaxLength = 256;
            this.Descripcion.Multiline = true;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(596, 60);
            this.Descripcion.TabIndex = 13;
            // 
            // Responsable
            // 
            this.Responsable.Location = new System.Drawing.Point(432, 46);
            this.Responsable.Name = "Responsable";
            this.Responsable.NullText = "Selecciona";
            this.Responsable.Size = new System.Drawing.Size(150, 20);
            this.Responsable.TabIndex = 11;
            this.Responsable.TabStop = false;
            // 
            // DescripcionLabel
            // 
            this.DescripcionLabel.Location = new System.Drawing.Point(12, 74);
            this.DescripcionLabel.Name = "DescripcionLabel";
            this.DescripcionLabel.Size = new System.Drawing.Size(67, 18);
            this.DescripcionLabel.TabIndex = 12;
            this.DescripcionLabel.Text = "Descripción:";
            // 
            // ResponsableLabel
            // 
            this.ResponsableLabel.Location = new System.Drawing.Point(354, 47);
            this.ResponsableLabel.Name = "ResponsableLabel";
            this.ResponsableLabel.Size = new System.Drawing.Size(72, 18);
            this.ResponsableLabel.TabIndex = 10;
            this.ResponsableLabel.Text = "Responsable:";
            // 
            // DepartamentoLabel
            // 
            this.DepartamentoLabel.Location = new System.Drawing.Point(12, 47);
            this.DepartamentoLabel.Name = "DepartamentoLabel";
            this.DepartamentoLabel.Size = new System.Drawing.Size(81, 18);
            this.DepartamentoLabel.TabIndex = 8;
            this.DepartamentoLabel.Text = "Departamento:";
            // 
            // Efecto
            // 
            // 
            // Efecto.NestedRadGridView
            // 
            this.Efecto.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Efecto.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Efecto.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Efecto.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Efecto.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Efecto.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Efecto.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "Id";
            gridViewTextBoxColumn11.HeaderText = "IdEfecto";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "Id";
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            gridViewTextBoxColumn12.FieldName = "Descriptor";
            gridViewTextBoxColumn12.HeaderText = "Descripción";
            gridViewTextBoxColumn12.Name = "Descriptor";
            gridViewTextBoxColumn12.Width = 150;
            this.Efecto.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.Efecto.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Efecto.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Efecto.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.Efecto.EditorControl.Name = "NestedRadGridView";
            this.Efecto.EditorControl.ReadOnly = true;
            this.Efecto.EditorControl.ShowGroupPanel = false;
            this.Efecto.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Efecto.EditorControl.TabIndex = 0;
            this.Efecto.Location = new System.Drawing.Point(400, 19);
            this.Efecto.Name = "Efecto";
            this.Efecto.NullText = "Selecciona";
            this.Efecto.Size = new System.Drawing.Size(208, 20);
            this.Efecto.TabIndex = 7;
            this.Efecto.TabStop = false;
            // 
            // EfectoLabel
            // 
            this.EfectoLabel.Location = new System.Drawing.Point(354, 21);
            this.EfectoLabel.Name = "EfectoLabel";
            this.EfectoLabel.Size = new System.Drawing.Size(40, 18);
            this.EfectoLabel.TabIndex = 6;
            this.EfectoLabel.Text = "Efecto:";
            // 
            // Clasificacion
            // 
            // 
            // Clasificacion.NestedRadGridView
            // 
            this.Clasificacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Clasificacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clasificacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Clasificacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Clasificacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Clasificacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "Id";
            gridViewTextBoxColumn13.HeaderText = "Id";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "Id";
            gridViewTextBoxColumn14.FieldName = "Descriptor";
            gridViewTextBoxColumn14.HeaderText = "Descripción";
            gridViewTextBoxColumn14.Name = "Descriptor";
            gridViewTextBoxColumn14.Width = 150;
            this.Clasificacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.Clasificacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Clasificacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Clasificacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.Clasificacion.EditorControl.Name = "NestedRadGridView";
            this.Clasificacion.EditorControl.ReadOnly = true;
            this.Clasificacion.EditorControl.ShowGroupPanel = false;
            this.Clasificacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Clasificacion.EditorControl.TabIndex = 0;
            this.Clasificacion.Location = new System.Drawing.Point(110, 19);
            this.Clasificacion.Name = "Clasificacion";
            this.Clasificacion.NullText = "Selecciona";
            this.Clasificacion.Size = new System.Drawing.Size(229, 20);
            this.Clasificacion.TabIndex = 5;
            this.Clasificacion.TabStop = false;
            // 
            // ClasificacionLabel
            // 
            this.ClasificacionLabel.Location = new System.Drawing.Point(12, 21);
            this.ClasificacionLabel.Name = "ClasificacionLabel";
            this.ClasificacionLabel.Size = new System.Drawing.Size(92, 18);
            this.ClasificacionLabel.TabIndex = 4;
            this.ClasificacionLabel.Text = "Clasifiación 5 m\'s";
            // 
            // Cancelar
            // 
            this.Cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancelar.Location = new System.Drawing.Point(498, 240);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(110, 24);
            this.Cancelar.TabIndex = 2;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Click += new System.EventHandler(this.TButton_Cancelar_Click);
            // 
            // Aceptar
            // 
            this.Aceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Aceptar.Location = new System.Drawing.Point(382, 240);
            this.Aceptar.Name = "Aceptar";
            this.Aceptar.Size = new System.Drawing.Size(110, 24);
            this.Aceptar.TabIndex = 2;
            this.Aceptar.Text = "Aceptar";
            this.Aceptar.Click += new System.EventHandler(this.TButton_Aceptar_Click);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.BackColor = System.Drawing.Color.White;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.HeaderLabel.Location = new System.Drawing.Point(12, 12);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(125, 18);
            this.HeaderLabel.TabIndex = 3;
            this.HeaderLabel.Text = "Análisis Causa - Efecto";
            // 
            // ProveedorError
            // 
            this.ProveedorError.ContainerControl = this;
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(620, 50);
            this.Header.TabIndex = 0;
            this.Header.TabStop = false;
            // 
            // CausaRaizForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 276);
            this.Controls.Add(this.HeaderLabel);
            this.Controls.Add(this.Aceptar);
            this.Controls.Add(this.Cancelar);
            this.Controls.Add(this.InfoGroup);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CausaRaizForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Causa - Efecto";
            this.Load += new System.EventHandler(this.CausaRaizForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.InfoGroup)).EndInit();
            this.InfoGroup.ResumeLayout(false);
            this.InfoGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsableCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResponsableLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartamentoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Efecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EfectoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clasificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClasificacionLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProveedorError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Header;
        private Telerik.WinControls.UI.RadGroupBox InfoGroup;
        private Telerik.WinControls.UI.RadLabel ClasificacionLabel;
        private Telerik.WinControls.UI.RadButton Cancelar;
        private Telerik.WinControls.UI.RadButton Aceptar;
        private Telerik.WinControls.UI.RadLabel HeaderLabel;
        private Telerik.WinControls.UI.RadLabel DescripcionLabel;
        private Telerik.WinControls.UI.RadLabel ResponsableLabel;
        private Telerik.WinControls.UI.RadLabel DepartamentoLabel;
        private Telerik.WinControls.UI.RadLabel EfectoLabel;
        protected internal Telerik.WinControls.UI.RadTextBox Descripcion;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Efecto;
        protected internal Telerik.WinControls.UI.RadTextBox Responsable;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Clasificacion;
        private System.Windows.Forms.ErrorProvider ProveedorError;
        public Telerik.WinControls.UI.RadButton Search;
        public Telerik.WinControls.UI.RadCheckBox ResponsableCheck;
    }
}