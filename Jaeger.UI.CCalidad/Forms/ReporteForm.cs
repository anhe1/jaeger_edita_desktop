﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.QRCode.Helpers;
using Jaeger.Util;

namespace Jaeger.UI.CCalidad.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources localResource = new EmbeddedResources("Jaeger.Domain.CCalidad");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.NombreComercial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(NoConformidadPrinter)) {
                this.CrearNoConformidad();
            } else if (this.CurrentObject.GetType() == typeof(CertificadoCalidadModel)) {
                this.Certificado();
            }
        }

        private void CrearNoConformidad() {
            var current = (NoConformidadPrinter)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream(@"Jaeger.Domain.CCalidad.Reports.NoConformidadV01Reporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remisiones");
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(new List<NoConformidadPrinter> { current }));
            this.SetDataSource("CausaRaiz", DbConvert.ConvertToDataTable(current.CausaRaiz));
            this.SetDataSource("AccionCorrectiva", DbConvert.ConvertToDataTable(current.AccionesCorrectivas));
            this.SetDataSource("AccionPreventiva", DbConvert.ConvertToDataTable(current.AccionesPreventivas));
            this.SetDataSource("OrdenProduccion", DbConvert.ConvertToDataTable(current.OrdenProduccion));
            this.SetDataSource("CostoNoCalidad", DbConvert.ConvertToDataTable(current.CostosNoCalidad));
            this.Finalizar();
        }

        private void Certificado() {
            var current = (CertificadoCalidadModel)this.CurrentObject;
            this.LoadDefinition = this.localResource.GetStream("Jaeger.Domain.CCalidad.Reports.OrdenProduccionCertificadoReporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Certificado");
            var d = DbConvert.ConvertToDataTable<CertificadoCalidadModel>(new List<CertificadoCalidadModel>() { current });
            this.SetDataSource("Data", d);
            this.Finalizar();
        }
    }
}
