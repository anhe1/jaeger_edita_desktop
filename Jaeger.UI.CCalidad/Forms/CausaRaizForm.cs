﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Aplication.CCalidad.Contracts;
using System.Collections.Generic;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class CausaRaizForm : RadForm {
        protected internal CausaRaizDetailModel _Current;
        protected internal INoConformidadesService Service;

        public event EventHandler<CausaRaizDetailModel> Selected;
        public void OnSelected(CausaRaizDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public CausaRaizForm(INoConformidadesService service, CausaRaizDetailModel model) {
            InitializeComponent();
            if (model == null) {
                this._Current = new CausaRaizDetailModel();
            } else {
                _Current = model;
            }
            this.Service = service;
        }

        private void CausaRaizForm_Load(object sender, EventArgs e) {
            Clasificacion.DataSource = NoConformidadService.GetClasificacion();
            Clasificacion.DisplayMember = "Descriptor";
            Clasificacion.ValueMember = "Id";
            Clasificacion.SelectedValue = this._Current.IdClasificacion5;

            Efecto.DataSource = NoConformidadService.GetEfecto();
            Efecto.DisplayMember = "Descriptor";
            Efecto.ValueMember = "Id";
            Efecto.SelectedValue = this._Current.IdEfecto;

            this.Descripcion.Text = this._Current.Descripcion;
            this.Departamento.DataSource = this.Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());
            this.Departamento.SelectedValue = this._Current.IdDepartamento;

            this.Responsable.Text = this._Current.Responsable;
            this.Responsable.Tag = this._Current.IdResponsable;

            this.ResponsableCheck.Checked = this._Current.IsResponsable;
        }

        protected virtual void TButton_Aceptar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Existen errore en la captura, por favor revisar y volver a intentar", "Atención", System.Windows.Forms.MessageBoxButtons.OK);
                return;
            }
            this._Current.Descripcion = this.Descripcion.Text;
            this._Current.IdClasificacion5 = (int)this.Clasificacion.SelectedValue;
            this._Current.IdEfecto = (int)this.Efecto.SelectedValue;
            this._Current.IdDepartamento = (int)this.Departamento.SelectedValue;
            this._Current.Responsable = this.Responsable.Text;
            this._Current.IsResponsable = this.ResponsableCheck.Checked;
            if (this.Responsable.Tag != null) {
                this._Current.IdResponsable = (int)this.Responsable.Tag;
            } else {
                this._Current.IdResponsable = -1;
            }
            this.OnSelected(_Current);
            this.Close();
        }

        protected virtual void TButton_Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual bool Validar() {
            this.ProveedorError.Clear();
            if (string.IsNullOrEmpty(this.Descripcion.Text)) {
                this.ProveedorError.SetError(this.Descripcion, "Este campo es necesario.");
                return false;
            }
            if (string.IsNullOrEmpty(this.Responsable.Text)) {
                this.ProveedorError.SetError(this.Responsable, "Este campo es necesario.");
                return false;
            }
            return true;
        }

        private void Search_Click(object sender, EventArgs e) {
            var search = new EmpleadoBuscar(this.Service);
            search.Selected += Search_Selected;
            search.ShowDialog(this);
        }

        private void Search_Selected(object sender, EmpleadoModel e) {
            if (e != null) {
                this.Responsable.Text = e.Nombre;
                if (e.Id > 0) {
                    this._Current.IdResponsable = e.Id;
                }
            }
        }
    }
}
