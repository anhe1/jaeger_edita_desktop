﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class AccionPreventivaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Header = new System.Windows.Forms.PictureBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.Departamento = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Search = new Telerik.WinControls.UI.RadButton();
            this.Responsable = new Telerik.WinControls.UI.RadTextBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Cancelar = new Telerik.WinControls.UI.RadButton();
            this.Aceptar = new Telerik.WinControls.UI.RadButton();
            this.HeaderLabel = new Telerik.WinControls.UI.RadLabel();
            this.ProveedorError = new System.Windows.Forms.ErrorProvider(this.components);
            this.Fecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.Header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProveedorError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.White;
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(620, 50);
            this.Header.TabIndex = 0;
            this.Header.TabStop = false;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.Fecha);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.Departamento);
            this.radGroupBox1.Controls.Add(this.Search);
            this.radGroupBox1.Controls.Add(this.Responsable);
            this.radGroupBox1.Controls.Add(this.Descripcion);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Descripción";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 50);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(620, 178);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Descripción";
            // 
            // Departamento
            // 
            // 
            // Departamento.NestedRadGridView
            // 
            this.Departamento.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamento.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamento.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamento.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamento.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamento.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamento.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "IdDepartamento";
            gridViewTextBoxColumn3.HeaderText = "IdDepartamento";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "IdDepartamento";
            gridViewTextBoxColumn3.VisibleInColumnChooser = false;
            gridViewTextBoxColumn4.FieldName = "Descriptor";
            gridViewTextBoxColumn4.HeaderText = "Descripción";
            gridViewTextBoxColumn4.Name = "Descriptor";
            gridViewTextBoxColumn4.Width = 150;
            this.Departamento.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.Departamento.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamento.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamento.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Departamento.EditorControl.Name = "NestedRadGridView";
            this.Departamento.EditorControl.ReadOnly = true;
            this.Departamento.EditorControl.ShowGroupPanel = false;
            this.Departamento.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamento.EditorControl.TabIndex = 0;
            this.Departamento.Location = new System.Drawing.Point(99, 22);
            this.Departamento.Name = "Departamento";
            this.Departamento.NullText = "Selecciona";
            this.Departamento.Size = new System.Drawing.Size(249, 20);
            this.Departamento.TabIndex = 377;
            this.Departamento.TabStop = false;
            // 
            // Search
            // 
            this.Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Search.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Search.Image = global::Jaeger.UI.CCalidad.Properties.Resources.search_16;
            this.Search.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Search.Location = new System.Drawing.Point(588, 24);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(20, 20);
            this.Search.TabIndex = 376;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // Responsable
            // 
            this.Responsable.Location = new System.Drawing.Point(432, 24);
            this.Responsable.Name = "Responsable";
            this.Responsable.NullText = "Selecciona";
            this.Responsable.Size = new System.Drawing.Size(150, 20);
            this.Responsable.TabIndex = 375;
            this.Responsable.TabStop = false;
            // 
            // Descripcion
            // 
            this.Descripcion.AutoSize = false;
            this.Descripcion.Location = new System.Drawing.Point(12, 75);
            this.Descripcion.MaxLength = 256;
            this.Descripcion.Multiline = true;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(596, 90);
            this.Descripcion.TabIndex = 13;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(12, 51);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(67, 18);
            this.radLabel5.TabIndex = 12;
            this.radLabel5.Text = "Descripción:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(354, 24);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(72, 18);
            this.radLabel4.TabIndex = 10;
            this.radLabel4.Text = "Responsable:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 24);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(81, 18);
            this.radLabel3.TabIndex = 8;
            this.radLabel3.Text = "Departamento:";
            // 
            // Cancelar
            // 
            this.Cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancelar.Location = new System.Drawing.Point(498, 234);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(110, 24);
            this.Cancelar.TabIndex = 2;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Click += new System.EventHandler(this.TButton_Cancelar_Click);
            // 
            // Aceptar
            // 
            this.Aceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Aceptar.Location = new System.Drawing.Point(382, 234);
            this.Aceptar.Name = "Aceptar";
            this.Aceptar.Size = new System.Drawing.Size(110, 24);
            this.Aceptar.TabIndex = 2;
            this.Aceptar.Text = "Aceptar";
            this.Aceptar.Click += new System.EventHandler(this.TButton_Aceptar_Click);
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.BackColor = System.Drawing.Color.White;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.HeaderLabel.Location = new System.Drawing.Point(12, 12);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(118, 18);
            this.HeaderLabel.TabIndex = 3;
            this.HeaderLabel.Text = "Acciones Preventivas";
            // 
            // ProveedorError
            // 
            this.ProveedorError.ContainerControl = this;
            // 
            // Fecha
            // 
            this.Fecha.Location = new System.Drawing.Point(416, 50);
            this.Fecha.Name = "Fecha";
            this.Fecha.Size = new System.Drawing.Size(192, 20);
            this.Fecha.TabIndex = 379;
            this.Fecha.TabStop = false;
            this.Fecha.Text = "sábado, 10 de agosto de 2024";
            this.Fecha.Value = new System.DateTime(2024, 8, 10, 22, 23, 22, 521);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(315, 51);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(95, 18);
            this.radLabel6.TabIndex = 378;
            this.radLabel6.Text = "Fecha de revisión:";
            // 
            // AccionPreventivaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 270);
            this.Controls.Add(this.HeaderLabel);
            this.Controls.Add(this.Aceptar);
            this.Controls.Add(this.Cancelar);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.Header);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccionPreventivaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Acciones Preventivas";
            this.Load += new System.EventHandler(this.AccionPreventivaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Header)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Responsable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProveedorError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Header;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton Cancelar;
        private Telerik.WinControls.UI.RadButton Aceptar;
        private Telerik.WinControls.UI.RadLabel HeaderLabel;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        protected internal Telerik.WinControls.UI.RadTextBox Descripcion;
        private System.Windows.Forms.ErrorProvider ProveedorError;
        public Telerik.WinControls.UI.RadButton Search;
        protected internal Telerik.WinControls.UI.RadTextBox Responsable;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox Departamento;
        private Telerik.WinControls.UI.RadDateTimePicker Fecha;
        private Telerik.WinControls.UI.RadLabel radLabel6;
    }
}