﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.CCalidad.Services;
using System.Net.NetworkInformation;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class NoConformidadStatusForm : RadForm {
        protected internal INoConformidadesService _Service;
        protected internal NoConformidadDetailModel _Current;

        public event EventHandler<NoConformidadStatusModel> Selected;
        public void OnSelected(NoConformidadStatusModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public event EventHandler<EventArgs> New;
        public void OnNew(EventArgs e) {
            if (this.New != null)
                this.New(this, e);
        }

        public event EventHandler<NoConformidadDetailModel> Editable;
        public void OnEditable(NoConformidadDetailModel e) {
            if (this.Editable != null)
                this.Editable(this, e);
        }

        public NoConformidadStatusForm(INoConformidadesService service, NoConformidadDetailModel noConformidad) {
            InitializeComponent();
            this._Service = service;
            this._Current = noConformidad;
        }

        private void NoConformidadStatusForm_Load(object sender, EventArgs e) {
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.SetEditable(false);
            this.IdDocumento.Text = this._Current.IdNoConformidad.ToString();

            this.IdStatus.DataSource = NoConformidadesService.GetStatus();
            this.MotivoCancelacion.DataSource = NoConformidadesService.GetMotivoCancelacion();

            this.TCancelar.Autorizar.Text = "Autorizar";
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
        }

        #region barra de herramientas
        public virtual void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var status = this.IdStatus.SelectedItem.DataBoundItem as Domain.Base.Entities.StatusModel;
            var seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as MotivoCancelacionModel;
            if (seleccionado != null) {
                var response = this._Current.GetStatusModel();
                response.CvMotivo = seleccionado.Descriptor;
                response.Usuario = ConfigService.Piloto.Clave;
                response.IdStatusA = status.Id;
                response.Nota = this.Nota.Text;
                response.IdNoConformidadR = 0;
                this.OnSelected(response);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion
    }
}
