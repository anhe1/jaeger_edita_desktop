﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class EmpleadoBuscar : RadForm {
        protected internal INoConformidadService Service;
        protected internal List<EmpleadoModel> _DataSource;

        public event EventHandler<EmpleadoModel> Selected;
        public void OnSelected(EmpleadoModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public EmpleadoBuscar(INoConformidadService service) {
            InitializeComponent();
            this.Service = service;
        }

        protected virtual void EmpleadoBuscar_Load(object sender, EventArgs e) {
            this.Search.Enabled = true;
        }

        protected virtual void Search_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Searched)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = _DataSource;
        }

        protected virtual void Aceptar_Click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as EmpleadoModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                    this.Close();
                }
            }
        }

        protected virtual void Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Searched() {
            this._DataSource = this.Service.GetList<EmpleadoModel>(new List<Domain.Base.Builder.IConditional> { new Conditional("@search", this.SearchText.Text) });
        }

        private void SearchText_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) {
                this.Search.PerformClick();
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.GridData.CurrentRow != null) {
                this.Aceptar.PerformClick();
            }
        }
    }
}
