﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Aplication.CCalidad.Contracts;
using System.Collections.Generic;
using Telerik.WinControls;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class AccionPreventivaForm : RadForm {
        protected internal AccionPreventivaDetailModel _Current;
        protected internal INoConformidadesService _Service;

        public event EventHandler<AccionPreventivaDetailModel> Selected;
        public void OnSelected(AccionPreventivaDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public AccionPreventivaForm(INoConformidadesService service, AccionPreventivaDetailModel model) {
            InitializeComponent();
            if (model == null) {
                this._Current = new AccionPreventivaDetailModel();
            } else {
                _Current = model;
            }
            this._Service = service;
        }

        private void AccionPreventivaForm_Load(object sender, EventArgs e) {
            this.Descripcion.Text = this._Current.Descripcion;
            this.Departamento.DataSource = this._Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());
            this.Departamento.ValueMember = "IdDepartamento";
            //this.Departamento.SelectedValue = this._Current.IdDepartamento;
            this.Responsable.Text = this._Current.Responsable;
            if (this._Current.Fecha == null) {
                this.Fecha.Value = DateTime.Now;
            } else {
                this.Fecha.Value = this._Current.Fecha.Value;
            }
        }

        protected virtual void TButton_Aceptar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show(this, "Existen errore en la captura, por favor revisar y volver a intentar", "Atención", System.Windows.Forms.MessageBoxButtons.OK);
                return;
            }
            this._Current.Descripcion = this.Descripcion.Text;
            this._Current.Fecha = this.Fecha.Value;
            this._Current.Responsable = this.Responsable.Text;
            this.OnSelected(_Current);
            this.Close();
        }

        protected virtual void TButton_Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual bool Validar() {
            this.ProveedorError.Clear();
            if (string.IsNullOrEmpty(this.Descripcion.Text)) {
                this.ProveedorError.SetError(this.Descripcion, "Este campo es necesario.");
                return false;
            }
            return true;
        }

        private void Search_Click(object sender, EventArgs e) {
            var search = new EmpleadoBuscar(this._Service);
            search.Selected += Search_Selected;
            search.ShowDialog(this);
        }

        private void Search_Selected(object sender, EmpleadoModel e) {
            if (e != null) {
                this.Responsable.Text = e.Nombre;
                if (e.Id > 0) {
                    //this._Current.IdResponsable = e.Id;
                }
            }
        }
    }
}
