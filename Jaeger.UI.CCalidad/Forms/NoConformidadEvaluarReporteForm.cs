﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class NoConformidadEvaluarReporteForm : RadForm {
        #region declaraciones
        protected internal INoConformidadesService Service;
        protected internal List<NoConformidadDetailModel> DataSource;
        protected internal List<DepartamentoModel> Departamentos;
        #endregion

        public NoConformidadEvaluarReporteForm() {
            InitializeComponent();
        }

        private void NoConformidadEvaluarReporteForm_Load(object sender, EventArgs e) {
            this.TNoConformidad.Actualizar.Text = "Crear";
            this.TNoConformidad.Service = this.Service;
            this.TNoConformidad.ShowEjercicio = false;
            this.TNoConformidad.ShowPeriodo = false;
            this.TNoConformidad.ShowImprimir = false;
            
            this.TNoConformidad.GridData.GroupDescriptors.Add(new GridGroupByExpression("IdDepartamento as IdDepartamento format \"{0}: {1}\" Group By IdDepartamento"));

            this.TReporte.Actualizar.Click += TReporte_Actualizar_Click;
            this.TReporte.Cerrar.Click += TReporte_Cerrar_Click;
        }

        private void TReporte_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            var reporteData = new NoConformidadDataModel(this.DataSource, this.Departamentos);

            PorEjercicio.Series[0].DataPoints.Clear();
            PorEjercicio.Series[1].DataPoints.Clear();
            PorEjercicio.Series[2].DataPoints.Clear();
            NoCalidad.Series[0].DataPoints.Clear();

            if (this.PorDepartamento.Series.Count > 0) {
                this.PorDepartamento.Series.Clear();
            }

            var total = this.PorEjercicio.Series[0] as BarSeries;
            total.DataSource = reporteData.Total;
            total.ValueMember = "Count";
            total.CategoryMember = "Month";

            var canceladas = this.PorEjercicio.Series[1] as BarSeries;
            canceladas.DataSource = reporteData.Canceladas;
            canceladas.ValueMember = "Count";
            canceladas.CategoryMember = "Month";

            var enProceso = this.PorEjercicio.Series[2] as BarSeries;
            enProceso.DataSource = reporteData.EnProceso;
            enProceso.ValueMember = "Count";
            enProceso.CategoryMember = "Month";

            var autorizadas = this.PorEjercicio.Series[3] as BarSeries;
            autorizadas.DataSource = reporteData.Autorizadas;
            autorizadas.ValueMember = "Count";
            autorizadas.CategoryMember = "Month";

            var pendientes = this.PorEjercicio.Series[4] as BarSeries;
            pendientes.DataSource = reporteData.Pendientes;
            pendientes.ValueMember = "Count";
            pendientes.CategoryMember = "Month";

            var noCalidad = NoCalidad.Series[0] as BarSeries;
            noCalidad.LegendTitle = "Totales";
            noCalidad.BorderWidth = 2;
            noCalidad.DataSource = reporteData.Costos;
            noCalidad.ValueMember = "Total";
            noCalidad.CategoryMember = "Month";

            PieSeries pieSeries = new PieSeries();
            pieSeries.ValueMember = "Count";
            pieSeries.LegendTitleMember = "Name";
            pieSeries.DataSource = reporteData.Departamentos;
            pieSeries.ShowLabels = true;
            pieSeries.SyncLinesToLabelsColor = true;

            this.PorDepartamento.Series.Add(pieSeries);

            this.TNoConformidad.SetEjercicio(this.TReporte.GetEjercicio());
            this.TNoConformidad.Actualizar.PerformClick();
        }

        public void TReporte_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        protected virtual void Consultar() {
            var query = NoConformidadesService.Query().Year(this.TReporte.GetEjercicio()).Build();
            this.DataSource = this.Service.GetList<NoConformidadDetailModel>(query);
            this.Departamentos = this.Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());
        }
    }
}
