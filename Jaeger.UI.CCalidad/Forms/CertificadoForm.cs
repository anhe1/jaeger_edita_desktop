﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;

namespace Jaeger.UI.CCalidad.Forms {
    /// <summary>
    /// Certificado de Calidad
    /// </summary>
    public partial class CertificadoForm : RadForm {
        #region declaraciones
        protected internal ICertificadoCalidadModel _Certificado;
        protected internal OrdenProduccionModel _OrdenProduccion;
        #endregion

        public CertificadoForm(OrdenProduccionModel ordenProduccion) {
            InitializeComponent();
            this._OrdenProduccion = ordenProduccion;
        }

        private void OrdenProduccionCertificadoForm_Load(object sender, EventArgs e) {
            this._Certificado = new CertificadoCalidadModel();
            this.Orden();
            this.CrateBinding();
        }

        private void CrateBinding() {
            this.IdPedido.DataBindings.Clear();
            this.IdPedido.DataBindings.Add("Text", this._Certificado, "IdPedido", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.DataBindings.Add("Value", this._Certificado, "Fecha", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.Cliente.DataBindings.Clear();
            this.Cliente.DataBindings.Add("Text", this._Certificado, "Cliente", true, DataSourceUpdateMode.OnPropertyChanged);
            
            this.Descripcion.DataBindings.Clear();
            this.Descripcion.DataBindings.Add("Text", this._Certificado, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Leyenda.DataBindings.Clear();
            this.Leyenda.DataBindings.Add("Text", this._Certificado, "Leyenda1", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Desviacion.DataBindings.Clear();
            this.Desviacion.DataBindings.Add("Text", this._Certificado, "Leyenda2", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Nota.DataBindings.Clear();
            this.Nota.DataBindings.Add("Text", this._Certificado, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void Orden() {
            this._Certificado = new CertificadoCalidadModel {
                IdPedido = this._OrdenProduccion.IdOrden,
                Descripcion = this._OrdenProduccion.Descripcion,
                Cliente = this._OrdenProduccion.Cliente
            };
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Imprimir_Click(object sender, EventArgs e) {
            using (var imprimir = new ReporteForm(this._Certificado)) {
                imprimir.ShowDialog(this);
            }
        }
    }
}
