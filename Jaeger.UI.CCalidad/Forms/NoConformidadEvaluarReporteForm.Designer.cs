﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadEvaluarReporteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries5 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis2 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries6 = new Telerik.WinControls.UI.BarSeries();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TReporte = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.ContenedorPrincipal = new Telerik.WinControls.UI.RadSplitContainer();
            this.Panel2 = new Telerik.WinControls.UI.SplitPanel();
            this.ContenedorSub = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.PorEjercicio = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.NoCalidad = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.PorDepartamento = new Telerik.WinControls.UI.RadChartView();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.TNoConformidad = new Jaeger.UI.CCalidad.Forms.NoConformidadGridControl();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPrincipal)).BeginInit();
            this.ContenedorPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorSub)).BeginInit();
            this.ContenedorSub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorEjercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NoCalidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TReporte
            // 
            this.TReporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.TReporte.Location = new System.Drawing.Point(0, 0);
            this.TReporte.Name = "TReporte";
            this.TReporte.ShowActualizar = true;
            this.TReporte.ShowAutosuma = false;
            this.TReporte.ShowCancelar = false;
            this.TReporte.ShowCerrar = true;
            this.TReporte.ShowEditar = false;
            this.TReporte.ShowEjercicio = true;
            this.TReporte.ShowExportarExcel = false;
            this.TReporte.ShowFiltro = false;
            this.TReporte.ShowHerramientas = false;
            this.TReporte.ShowImprimir = false;
            this.TReporte.ShowItem = false;
            this.TReporte.ShowNuevo = false;
            this.TReporte.ShowPeriodo = false;
            this.TReporte.Size = new System.Drawing.Size(1233, 30);
            this.TReporte.TabIndex = 0;
            // 
            // ContenedorPrincipal
            // 
            this.ContenedorPrincipal.Controls.Add(this.Panel2);
            this.ContenedorPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContenedorPrincipal.Location = new System.Drawing.Point(0, 30);
            this.ContenedorPrincipal.Name = "ContenedorPrincipal";
            // 
            // 
            // 
            this.ContenedorPrincipal.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.ContenedorPrincipal.Size = new System.Drawing.Size(1233, 614);
            this.ContenedorPrincipal.TabIndex = 1;
            this.ContenedorPrincipal.TabStop = false;
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.ContenedorSub);
            this.Panel2.Location = new System.Drawing.Point(0, 0);
            this.Panel2.Name = "Panel2";
            // 
            // 
            // 
            this.Panel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Panel2.Size = new System.Drawing.Size(1233, 614);
            this.Panel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.1908056F, 0F);
            this.Panel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(234, 0);
            this.Panel2.TabIndex = 1;
            this.Panel2.TabStop = false;
            this.Panel2.Text = "splitPanel1";
            // 
            // ContenedorSub
            // 
            this.ContenedorSub.Controls.Add(this.splitPanel1);
            this.ContenedorSub.Controls.Add(this.splitPanel2);
            this.ContenedorSub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContenedorSub.Location = new System.Drawing.Point(0, 0);
            this.ContenedorSub.Name = "ContenedorSub";
            this.ContenedorSub.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.ContenedorSub.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.ContenedorSub.Size = new System.Drawing.Size(1233, 614);
            this.ContenedorSub.TabIndex = 0;
            this.ContenedorSub.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1233, 305);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel3);
            this.radSplitContainer1.Controls.Add(this.splitPanel4);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1233, 305);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.PorEjercicio);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel3.Size = new System.Drawing.Size(614, 305);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // PorEjercicio
            // 
            this.PorEjercicio.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            categoricalAxis1.LabelRotationAngle = 300D;
            categoricalAxis1.Title = "Meses";
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFormat = "{0:N0}";
            linearAxis1.LabelRotationAngle = 300D;
            linearAxis1.TickOrigin = null;
            linearAxis1.Title = "No Conformidad";
            this.PorEjercicio.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.PorEjercicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorEjercicio.LegendTitle = "Por Status";
            this.PorEjercicio.Location = new System.Drawing.Point(0, 0);
            this.PorEjercicio.Name = "PorEjercicio";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries1.LegendTitle = "Total";
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries2.LegendTitle = "Canceladas";
            barSeries2.VerticalAxis = linearAxis1;
            barSeries3.HorizontalAxis = categoricalAxis1;
            barSeries3.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries3.LegendTitle = "En Proceso";
            barSeries3.VerticalAxis = linearAxis1;
            barSeries4.HorizontalAxis = categoricalAxis1;
            barSeries4.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries4.LegendTitle = "Autorizadas";
            barSeries4.VerticalAxis = linearAxis1;
            barSeries5.HorizontalAxis = categoricalAxis1;
            barSeries5.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries5.LegendTitle = "Pendientes";
            barSeries5.VerticalAxis = linearAxis1;
            this.PorEjercicio.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2,
            barSeries3,
            barSeries4,
            barSeries5});
            this.PorEjercicio.ShowGrid = false;
            this.PorEjercicio.ShowLegend = true;
            this.PorEjercicio.ShowTitle = true;
            this.PorEjercicio.Size = new System.Drawing.Size(614, 305);
            this.PorEjercicio.TabIndex = 0;
            this.PorEjercicio.Title = "Por Ejercicio";
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.NoCalidad);
            this.splitPanel4.Location = new System.Drawing.Point(618, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(615, 305);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // NoCalidad
            // 
            this.NoCalidad.AreaDesign = cartesianArea2;
            categoricalAxis2.IsPrimary = true;
            categoricalAxis2.LabelRotationAngle = 300D;
            categoricalAxis2.Title = "Meses";
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.LabelRotationAngle = 300D;
            linearAxis2.TickOrigin = null;
            linearAxis2.Title = "Total";
            this.NoCalidad.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis2,
            linearAxis2});
            this.NoCalidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NoCalidad.LegendTitle = "No Conformidad";
            this.NoCalidad.Location = new System.Drawing.Point(0, 0);
            this.NoCalidad.Name = "NoCalidad";
            barSeries6.HorizontalAxis = categoricalAxis2;
            barSeries6.LabelMode = Telerik.WinControls.UI.BarLabelModes.Top;
            barSeries6.LegendTitle = "Costo";
            barSeries6.VerticalAxis = linearAxis2;
            this.NoCalidad.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries6});
            this.NoCalidad.ShowGrid = false;
            this.NoCalidad.ShowLegend = true;
            this.NoCalidad.ShowTitle = true;
            this.NoCalidad.Size = new System.Drawing.Size(615, 305);
            this.NoCalidad.TabIndex = 2;
            this.NoCalidad.Title = "No Calidad";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(0, 309);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1233, 305);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel5);
            this.radSplitContainer2.Controls.Add(this.splitPanel6);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(1233, 305);
            this.radSplitContainer2.TabIndex = 2;
            this.radSplitContainer2.TabStop = false;
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.PorDepartamento);
            this.splitPanel5.Location = new System.Drawing.Point(0, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(335, 305);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2274207F, 0F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(-279, 0);
            this.splitPanel5.TabIndex = 0;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // PorDepartamento
            // 
            this.PorDepartamento.AreaType = Telerik.WinControls.UI.ChartAreaType.Pie;
            this.PorDepartamento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PorDepartamento.Location = new System.Drawing.Point(0, 0);
            this.PorDepartamento.Name = "PorDepartamento";
            this.PorDepartamento.ShowGrid = false;
            this.PorDepartamento.ShowLegend = true;
            this.PorDepartamento.ShowTitle = true;
            this.PorDepartamento.Size = new System.Drawing.Size(335, 305);
            this.PorDepartamento.TabIndex = 1;
            this.PorDepartamento.Title = "Por Departamento";
            // 
            // splitPanel6
            // 
            this.splitPanel6.Controls.Add(this.TNoConformidad);
            this.splitPanel6.Location = new System.Drawing.Point(339, 0);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel6.Size = new System.Drawing.Size(894, 305);
            this.splitPanel6.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2274207F, 0F);
            this.splitPanel6.SizeInfo.SplitterCorrection = new System.Drawing.Size(279, 0);
            this.splitPanel6.TabIndex = 1;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // TNoConformidad
            // 
            this.TNoConformidad.DataSource = null;
            this.TNoConformidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNoConformidad.Location = new System.Drawing.Point(0, 0);
            this.TNoConformidad.Name = "TNoConformidad";
            this.TNoConformidad.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TNoConformidad.Permisos = uiAction1;
            this.TNoConformidad.ShowActualizar = false;
            this.TNoConformidad.ShowAgrupar = false;
            this.TNoConformidad.ShowAutosuma = false;
            this.TNoConformidad.ShowCancelar = false;
            this.TNoConformidad.ShowCerrar = false;
            this.TNoConformidad.ShowEditar = false;
            this.TNoConformidad.ShowEjercicio = false;
            this.TNoConformidad.ShowExportarExcel = false;
            this.TNoConformidad.ShowFiltro = true;
            this.TNoConformidad.ShowHerramientas = false;
            this.TNoConformidad.ShowImprimir = true;
            this.TNoConformidad.ShowItem = false;
            this.TNoConformidad.ShowNuevo = false;
            this.TNoConformidad.ShowPeriodo = false;
            this.TNoConformidad.ShowSeleccionMultiple = false;
            this.TNoConformidad.Size = new System.Drawing.Size(894, 305);
            this.TNoConformidad.TabIndex = 1;
            // 
            // NoConformidadEvaluarReporteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 644);
            this.Controls.Add(this.ContenedorPrincipal);
            this.Controls.Add(this.TReporte);
            this.Name = "NoConformidadEvaluarReporteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "No Conformidad: Reporte Anual";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NoConformidadEvaluarReporteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorPrincipal)).EndInit();
            this.ContenedorPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ContenedorSub)).EndInit();
            this.ContenedorSub.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorEjercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NoCalidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TReporte;
        private Telerik.WinControls.UI.RadSplitContainer ContenedorPrincipal;
        private Telerik.WinControls.UI.SplitPanel Panel2;
        private Telerik.WinControls.UI.RadChartView PorEjercicio;
        private Telerik.WinControls.UI.RadSplitContainer ContenedorSub;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadChartView NoCalidad;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadChartView PorDepartamento;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        public NoConformidadGridControl TNoConformidad;
    }
}