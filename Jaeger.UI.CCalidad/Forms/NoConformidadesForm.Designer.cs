﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TNoConformidad = new Jaeger.UI.CCalidad.Forms.NoConformidadGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TNoConformidad
            // 
            this.TNoConformidad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNoConformidad.Location = new System.Drawing.Point(0, 0);
            this.TNoConformidad.Name = "TNoConformidad";
            this.TNoConformidad.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TNoConformidad.Permisos = uiAction1;
            this.TNoConformidad.ShowActualizar = true;
            this.TNoConformidad.ShowAgrupar = false;
            this.TNoConformidad.ShowAutosuma = false;
            this.TNoConformidad.ShowCancelar = false;
            this.TNoConformidad.ShowCerrar = true;
            this.TNoConformidad.ShowEditar = true;
            this.TNoConformidad.ShowEjercicio = true;
            this.TNoConformidad.ShowExportarExcel = false;
            this.TNoConformidad.ShowFiltro = true;
            this.TNoConformidad.ShowHerramientas = false;
            this.TNoConformidad.ShowImprimir = false;
            this.TNoConformidad.ShowItem = false;
            this.TNoConformidad.ShowNuevo = true;
            this.TNoConformidad.ShowPeriodo = true;
            this.TNoConformidad.ShowSeleccionMultiple = true;
            this.TNoConformidad.Size = new System.Drawing.Size(1238, 620);
            this.TNoConformidad.TabIndex = 1;
            // 
            // NoConformidadesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 620);
            this.Controls.Add(this.TNoConformidad);
            this.Name = "NoConformidadesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "No Conformidades";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NoConformidadesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected internal NoConformidadGridControl TNoConformidad;
    }
}