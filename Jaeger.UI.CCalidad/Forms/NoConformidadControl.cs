﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class NoConformidadControl : UserControl {
        private bool _ReadOnly = false;

        public NoConformidadControl() {
            InitializeComponent();
        }

        private void NoConformidadControl_Load(object sender, EventArgs e) {
            this.FechaEmision.DateTimePickerElement.ArrowButton.Visibility = ElementVisibility.Hidden;
            this.FechaEmision.DateTimePickerElement.TextBoxElement.TextBoxItem.ReadOnly = true;

            this.Folio.ReadOnly = true;
        }

        public bool ReadOnly {
            get { return _ReadOnly; }
            set {
                _ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        private void CreateReadOnly() {
            // si es verdadero entonces bloquear todos los controles
            this.Descripcion.ReadOnly = this._ReadOnly;
            this.Detector.ReadOnly = this._ReadOnly;
            this.Cantidad.ReadOnly = this._ReadOnly;
            this.Nota.ReadOnly = this._ReadOnly;
            this.Search.Enabled = !this._ReadOnly;

            if (this._ReadOnly) {
                Departamento.DropDownOpening += EditorControl_DropDownOpening;
                Departamento.EditorControl.CurrentRowChanging += EditorControl_CurrentRowChanging;
                Departamento.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                Departamento.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;

                this.TDocumento.Status.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.TDocumento.Status.DropDownListElement.ArrowButton.Visibility = ElementVisibility.Hidden;

                this.TDocumento.Status.PopupOpening += Status_PopupOpening;
            }
        }

        private void Status_PopupOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }

        public static void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = true;
        }
        private static void EditorControl_CurrentRowChanging(object sender, CurrentRowChangingEventArgs e) {
            e.Cancel = true;
        }
    }
}
