﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class NoConformidadesForm : RadForm {

        public NoConformidadesForm() {
            InitializeComponent();
        }

        protected virtual void NoConformidadesForm_Load(object sender, EventArgs e) {
            this.TNoConformidad.Nuevo.Click += TNoConformidad_Nuevo_Click;
            this.TNoConformidad.Editar.Click += TNoConformidad_Editar_Click;
            this.TNoConformidad.Cancelar.Click += TNoConformidad_Cancelar_Click;
            this.TNoConformidad.ImprimirComprobante.Click += this.TNoConformidad_Imprimir_Click;
            this.TNoConformidad.ContextImprimir.Click += this.TNoConformidad_Imprimir_Click;
            this.TNoConformidad.Cerrar.Click += TNoConformidad_Cerrar_Click;
        }

        protected virtual void TNoConformidad_Cancelar_Click(object sender, EventArgs e) {
            var seleccionado = this.TNoConformidad.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                if (seleccionado.IdStatus == 0) {
                    MessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else {
                    if (MessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        var cancela = new NoConformidadStatusForm(this.TNoConformidad.Service, seleccionado);
                        cancela.Selected += this.Cancela_Selected;
                        cancela.ShowDialog(this);

                    }
                }
            }
        }

        protected virtual void Cancela_Selected(object sender, NoConformidadStatusModel e) {
            if (e != null) {
                this.Tag = e;
                using (var espera = new Waiting1Form(this.Cancelar)) {
                    espera.Text = "Cancelando ...";
                    espera.ShowDialog(this);
                }
            }
        }

        protected virtual void TNoConformidad_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new NoConformidadForm(this.TNoConformidad.Service, null) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        protected virtual void TNoConformidad_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TNoConformidad.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                var nuevo = new NoConformidadForm(this.TNoConformidad.Service, seleccionado) { MdiParent = this.ParentForm };
                nuevo.Show();
            }
        }


        private void TNoConformidad_Imprimir_Click(object sender, EventArgs e) {
            var seleccionado = this.TNoConformidad.GetCurrent<NoConformidadSingleModel>();
            if (seleccionado != null) {
                using (var printer = new ReporteForm(this.TNoConformidad.Service.GetPrinter(seleccionado.IdNoConformidad))) {
                    printer.ShowDialog(this);
                }
            }
        }

        protected virtual void TNoConformidad_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        #region
        
        protected virtual void Cancelar() {

        }
        #endregion
    }
}
