﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction2 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction3 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction5 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction6 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction4 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TCausaRaiz = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.Contenedor = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.PageAnalisis = new Telerik.WinControls.UI.RadPageView();
            this.PageCausaRaiz = new Telerik.WinControls.UI.RadPageViewPage();
            this.PageAccionesCorrectivas = new Telerik.WinControls.UI.RadPageViewPage();
            this.TCorrectivo = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.PageAccionesPreventivas = new Telerik.WinControls.UI.RadPageViewPage();
            this.TPreventivo = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.PageOrdenes = new Telerik.WinControls.UI.RadPageView();
            this.PageOrden = new Telerik.WinControls.UI.RadPageViewPage();
            this.TOrden = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.PageCosto = new Telerik.WinControls.UI.RadPageViewPage();
            this.TCosto = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.TNoConformidad = new Jaeger.UI.CCalidad.Forms.NoConformidadControl();
            this.TImagen = new Jaeger.UI.Common.Forms.GridStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).BeginInit();
            this.Contenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageAnalisis)).BeginInit();
            this.PageAnalisis.SuspendLayout();
            this.PageCausaRaiz.SuspendLayout();
            this.PageAccionesCorrectivas.SuspendLayout();
            this.PageAccionesPreventivas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PageOrdenes)).BeginInit();
            this.PageOrdenes.SuspendLayout();
            this.PageOrden.SuspendLayout();
            this.PageCosto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TCausaRaiz
            // 
            this.TCausaRaiz.Caption = "Análisis Causa - Raíz";
            this.TCausaRaiz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCausaRaiz.Location = new System.Drawing.Point(0, 0);
            this.TCausaRaiz.Name = "TCausaRaiz";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TCausaRaiz.Permisos = uiAction1;
            this.TCausaRaiz.ReadOnly = false;
            this.TCausaRaiz.ShowActualizar = false;
            this.TCausaRaiz.ShowAutorizar = false;
            this.TCausaRaiz.ShowAutosuma = false;
            this.TCausaRaiz.ShowCerrar = false;
            this.TCausaRaiz.ShowEditar = true;
            this.TCausaRaiz.ShowExportarExcel = false;
            this.TCausaRaiz.ShowFiltro = true;
            this.TCausaRaiz.ShowHerramientas = false;
            this.TCausaRaiz.ShowImagen = false;
            this.TCausaRaiz.ShowImprimir = false;
            this.TCausaRaiz.ShowNuevo = true;
            this.TCausaRaiz.ShowRemover = true;
            this.TCausaRaiz.ShowSeleccionMultiple = false;
            this.TCausaRaiz.Size = new System.Drawing.Size(876, 175);
            this.TCausaRaiz.TabIndex = 2;
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.splitPanel1);
            this.Contenedor.Controls.Add(this.splitPanel2);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 132);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.Contenedor.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.Contenedor.Size = new System.Drawing.Size(1230, 423);
            this.Contenedor.TabIndex = 0;
            this.Contenedor.TabStop = false;
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer3);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(1230, 219);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.02229297F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 7);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // PageAnalisis
            // 
            this.PageAnalisis.Controls.Add(this.PageCausaRaiz);
            this.PageAnalisis.Controls.Add(this.PageAccionesCorrectivas);
            this.PageAnalisis.Controls.Add(this.PageAccionesPreventivas);
            this.PageAnalisis.DefaultPage = this.PageCausaRaiz;
            this.PageAnalisis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageAnalisis.Location = new System.Drawing.Point(0, 0);
            this.PageAnalisis.Name = "PageAnalisis";
            this.PageAnalisis.SelectedPage = this.PageCausaRaiz;
            this.PageAnalisis.Size = new System.Drawing.Size(897, 219);
            this.PageAnalisis.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageAnalisis.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageCausaRaiz
            // 
            this.PageCausaRaiz.Controls.Add(this.TCausaRaiz);
            this.PageCausaRaiz.ItemSize = new System.Drawing.SizeF(114F, 24F);
            this.PageCausaRaiz.Location = new System.Drawing.Point(10, 33);
            this.PageCausaRaiz.Name = "PageCausaRaiz";
            this.PageCausaRaiz.Size = new System.Drawing.Size(876, 175);
            this.PageCausaRaiz.Text = "Análisis Causa - Raíz";
            // 
            // PageAccionesCorrectivas
            // 
            this.PageAccionesCorrectivas.Controls.Add(this.TCorrectivo);
            this.PageAccionesCorrectivas.ItemSize = new System.Drawing.SizeF(115F, 24F);
            this.PageAccionesCorrectivas.Location = new System.Drawing.Point(10, 37);
            this.PageAccionesCorrectivas.Name = "PageAccionesCorrectivas";
            this.PageAccionesCorrectivas.Size = new System.Drawing.Size(1209, 171);
            this.PageAccionesCorrectivas.Text = "Acciones Correctivas";
            // 
            // TCorrectivo
            // 
            this.TCorrectivo.Caption = "Análisis Causa - Raíz";
            this.TCorrectivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCorrectivo.Location = new System.Drawing.Point(0, 0);
            this.TCorrectivo.Name = "TCorrectivo";
            uiAction2.Agregar = false;
            uiAction2.Autorizar = false;
            uiAction2.Cancelar = false;
            uiAction2.Editar = false;
            uiAction2.Exportar = false;
            uiAction2.Importar = false;
            uiAction2.Imprimir = false;
            uiAction2.Remover = false;
            uiAction2.Reporte = false;
            uiAction2.Status = false;
            this.TCorrectivo.Permisos = uiAction2;
            this.TCorrectivo.ReadOnly = false;
            this.TCorrectivo.ShowActualizar = false;
            this.TCorrectivo.ShowAutorizar = false;
            this.TCorrectivo.ShowAutosuma = false;
            this.TCorrectivo.ShowCerrar = false;
            this.TCorrectivo.ShowEditar = true;
            this.TCorrectivo.ShowExportarExcel = false;
            this.TCorrectivo.ShowFiltro = true;
            this.TCorrectivo.ShowHerramientas = false;
            this.TCorrectivo.ShowImagen = false;
            this.TCorrectivo.ShowImprimir = false;
            this.TCorrectivo.ShowNuevo = true;
            this.TCorrectivo.ShowRemover = true;
            this.TCorrectivo.ShowSeleccionMultiple = false;
            this.TCorrectivo.Size = new System.Drawing.Size(1209, 171);
            this.TCorrectivo.TabIndex = 3;
            // 
            // PageAccionesPreventivas
            // 
            this.PageAccionesPreventivas.Controls.Add(this.TPreventivo);
            this.PageAccionesPreventivas.ItemSize = new System.Drawing.SizeF(116F, 24F);
            this.PageAccionesPreventivas.Location = new System.Drawing.Point(10, 37);
            this.PageAccionesPreventivas.Name = "PageAccionesPreventivas";
            this.PageAccionesPreventivas.Size = new System.Drawing.Size(1209, 171);
            this.PageAccionesPreventivas.Text = "Acciones Preventivas";
            // 
            // TPreventivo
            // 
            this.TPreventivo.Caption = "";
            this.TPreventivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TPreventivo.Location = new System.Drawing.Point(0, 0);
            this.TPreventivo.Name = "TPreventivo";
            uiAction3.Agregar = false;
            uiAction3.Autorizar = false;
            uiAction3.Cancelar = false;
            uiAction3.Editar = false;
            uiAction3.Exportar = false;
            uiAction3.Importar = false;
            uiAction3.Imprimir = false;
            uiAction3.Remover = false;
            uiAction3.Reporte = false;
            uiAction3.Status = false;
            this.TPreventivo.Permisos = uiAction3;
            this.TPreventivo.ReadOnly = false;
            this.TPreventivo.ShowActualizar = false;
            this.TPreventivo.ShowAutorizar = false;
            this.TPreventivo.ShowAutosuma = false;
            this.TPreventivo.ShowCerrar = false;
            this.TPreventivo.ShowEditar = true;
            this.TPreventivo.ShowExportarExcel = false;
            this.TPreventivo.ShowFiltro = true;
            this.TPreventivo.ShowHerramientas = false;
            this.TPreventivo.ShowImagen = false;
            this.TPreventivo.ShowImprimir = false;
            this.TPreventivo.ShowNuevo = true;
            this.TPreventivo.ShowRemover = true;
            this.TPreventivo.ShowSeleccionMultiple = false;
            this.TPreventivo.Size = new System.Drawing.Size(1209, 171);
            this.TPreventivo.TabIndex = 3;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.PageOrdenes);
            this.splitPanel2.Location = new System.Drawing.Point(0, 223);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(1230, 200);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.022293F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -7);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // PageOrdenes
            // 
            this.PageOrdenes.Controls.Add(this.PageOrden);
            this.PageOrdenes.Controls.Add(this.PageCosto);
            this.PageOrdenes.DefaultPage = this.PageOrden;
            this.PageOrdenes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageOrdenes.Location = new System.Drawing.Point(0, 0);
            this.PageOrdenes.Name = "PageOrdenes";
            this.PageOrdenes.SelectedPage = this.PageOrden;
            this.PageOrdenes.Size = new System.Drawing.Size(1230, 200);
            this.PageOrdenes.TabIndex = 1;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageOrdenes.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageOrden
            // 
            this.PageOrden.Controls.Add(this.TOrden);
            this.PageOrden.ItemSize = new System.Drawing.SizeF(182F, 24F);
            this.PageOrden.Location = new System.Drawing.Point(10, 33);
            this.PageOrden.Name = "PageOrden";
            this.PageOrden.Size = new System.Drawing.Size(1209, 156);
            this.PageOrden.Text = "Orden de Producción Relacioanda";
            // 
            // TOrden
            // 
            this.TOrden.Caption = "";
            this.TOrden.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TOrden.Location = new System.Drawing.Point(0, 0);
            this.TOrden.Name = "TOrden";
            uiAction5.Agregar = false;
            uiAction5.Autorizar = false;
            uiAction5.Cancelar = false;
            uiAction5.Editar = false;
            uiAction5.Exportar = false;
            uiAction5.Importar = false;
            uiAction5.Imprimir = false;
            uiAction5.Remover = false;
            uiAction5.Reporte = false;
            uiAction5.Status = false;
            this.TOrden.Permisos = uiAction5;
            this.TOrden.ReadOnly = false;
            this.TOrden.ShowActualizar = false;
            this.TOrden.ShowAutorizar = false;
            this.TOrden.ShowAutosuma = false;
            this.TOrden.ShowCerrar = false;
            this.TOrden.ShowEditar = false;
            this.TOrden.ShowExportarExcel = false;
            this.TOrden.ShowFiltro = true;
            this.TOrden.ShowHerramientas = false;
            this.TOrden.ShowImagen = false;
            this.TOrden.ShowImprimir = false;
            this.TOrden.ShowNuevo = true;
            this.TOrden.ShowRemover = true;
            this.TOrden.ShowSeleccionMultiple = false;
            this.TOrden.Size = new System.Drawing.Size(1209, 156);
            this.TOrden.TabIndex = 2;
            // 
            // PageCosto
            // 
            this.PageCosto.Controls.Add(this.TCosto);
            this.PageCosto.ItemSize = new System.Drawing.SizeF(120F, 24F);
            this.PageCosto.Location = new System.Drawing.Point(10, 37);
            this.PageCosto.Name = "PageCosto";
            this.PageCosto.Size = new System.Drawing.Size(1209, 152);
            this.PageCosto.Text = "Costos de No Calidad";
            // 
            // TCosto
            // 
            this.TCosto.Caption = "";
            this.TCosto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCosto.Location = new System.Drawing.Point(0, 0);
            this.TCosto.Name = "TCosto";
            uiAction6.Agregar = false;
            uiAction6.Autorizar = false;
            uiAction6.Cancelar = false;
            uiAction6.Editar = false;
            uiAction6.Exportar = false;
            uiAction6.Importar = false;
            uiAction6.Imprimir = false;
            uiAction6.Remover = false;
            uiAction6.Reporte = false;
            uiAction6.Status = false;
            this.TCosto.Permisos = uiAction6;
            this.TCosto.ReadOnly = false;
            this.TCosto.ShowActualizar = false;
            this.TCosto.ShowAutorizar = false;
            this.TCosto.ShowAutosuma = false;
            this.TCosto.ShowCerrar = false;
            this.TCosto.ShowEditar = false;
            this.TCosto.ShowExportarExcel = false;
            this.TCosto.ShowFiltro = true;
            this.TCosto.ShowHerramientas = false;
            this.TCosto.ShowImagen = false;
            this.TCosto.ShowImprimir = false;
            this.TCosto.ShowNuevo = true;
            this.TCosto.ShowRemover = true;
            this.TCosto.ShowSeleccionMultiple = false;
            this.TCosto.Size = new System.Drawing.Size(1209, 152);
            this.TCosto.TabIndex = 3;
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel4);
            this.radSplitContainer3.Controls.Add(this.splitPanel5);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer3.Size = new System.Drawing.Size(1230, 219);
            this.radSplitContainer3.TabIndex = 1;
            this.radSplitContainer3.TabStop = false;
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.PageAnalisis);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel4.Size = new System.Drawing.Size(897, 219);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2316476F, 0F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(258, 0);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.TImagen);
            this.splitPanel5.Location = new System.Drawing.Point(901, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(329, 219);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2316476F, 0F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(-258, 0);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // TNoConformidad
            // 
            this.TNoConformidad.Dock = System.Windows.Forms.DockStyle.Top;
            this.TNoConformidad.Location = new System.Drawing.Point(0, 0);
            this.TNoConformidad.Name = "TNoConformidad";
            this.TNoConformidad.ReadOnly = false;
            this.TNoConformidad.Size = new System.Drawing.Size(1230, 132);
            this.TNoConformidad.TabIndex = 1;
            // 
            // TImagen
            // 
            this.TImagen.Caption = "";
            this.TImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TImagen.Location = new System.Drawing.Point(0, 0);
            this.TImagen.Name = "TImagen";
            uiAction4.Agregar = false;
            uiAction4.Autorizar = false;
            uiAction4.Cancelar = false;
            uiAction4.Editar = false;
            uiAction4.Exportar = false;
            uiAction4.Importar = false;
            uiAction4.Imprimir = false;
            uiAction4.Remover = false;
            uiAction4.Reporte = false;
            uiAction4.Status = false;
            this.TImagen.Permisos = uiAction4;
            this.TImagen.ReadOnly = false;
            this.TImagen.ShowActualizar = false;
            this.TImagen.ShowAutorizar = false;
            this.TImagen.ShowAutosuma = false;
            this.TImagen.ShowCerrar = false;
            this.TImagen.ShowEditar = false;
            this.TImagen.ShowExportarExcel = false;
            this.TImagen.ShowFiltro = false;
            this.TImagen.ShowHerramientas = false;
            this.TImagen.ShowImagen = false;
            this.TImagen.ShowImprimir = false;
            this.TImagen.ShowNuevo = true;
            this.TImagen.ShowRemover = true;
            this.TImagen.ShowSeleccionMultiple = false;
            this.TImagen.Size = new System.Drawing.Size(329, 219);
            this.TImagen.TabIndex = 3;
            // 
            // NoConformidadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1230, 555);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.TNoConformidad);
            this.Name = "NoConformidadForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "No Conformidad";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NoConformidadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Contenedor)).EndInit();
            this.Contenedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PageAnalisis)).EndInit();
            this.PageAnalisis.ResumeLayout(false);
            this.PageCausaRaiz.ResumeLayout(false);
            this.PageAccionesCorrectivas.ResumeLayout(false);
            this.PageAccionesPreventivas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PageOrdenes)).EndInit();
            this.PageOrdenes.ResumeLayout(false);
            this.PageOrden.ResumeLayout(false);
            this.PageCosto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NoConformidadControl TNoConformidad;
        private Common.Forms.GridStandarControl TCausaRaiz;
        private Telerik.WinControls.UI.RadSplitContainer Contenedor;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadPageView PageAnalisis;
        private Telerik.WinControls.UI.RadPageViewPage PageCausaRaiz;
        private Telerik.WinControls.UI.RadPageViewPage PageAccionesCorrectivas;
        private Telerik.WinControls.UI.RadPageView PageOrdenes;
        private Telerik.WinControls.UI.RadPageViewPage PageOrden;
        private Telerik.WinControls.UI.RadPageViewPage PageCosto;
        private Common.Forms.GridStandarControl TCorrectivo;
        private Common.Forms.GridStandarControl TCosto;
        private Telerik.WinControls.UI.RadPageViewPage PageAccionesPreventivas;
        private Common.Forms.GridStandarControl TPreventivo;
        protected internal Common.Forms.GridStandarControl TOrden;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Common.Forms.GridStandarControl TImagen;
    }
}