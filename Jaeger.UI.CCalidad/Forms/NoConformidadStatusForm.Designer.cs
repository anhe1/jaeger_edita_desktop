﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadStatusForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.MotivoCancelacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.Group = new Telerik.WinControls.UI.RadGroupBox();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblFecha = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.IdStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.lblRemision = new Telerik.WinControls.UI.RadLabel();
            this.TCancelar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group)).BeginInit();
            this.Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRemision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MotivoCancelacion
            // 
            this.MotivoCancelacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MotivoCancelacion.AutoSizeDropDownHeight = true;
            this.MotivoCancelacion.AutoSizeDropDownToBestFit = true;
            this.MotivoCancelacion.DisplayMember = "Descriptor";
            this.MotivoCancelacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MotivoCancelacion.NestedRadGridView
            // 
            this.MotivoCancelacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MotivoCancelacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotivoCancelacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MotivoCancelacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn3.FieldName = "Descriptor";
            gridViewTextBoxColumn3.HeaderText = "Descriptor";
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "Descriptor";
            this.MotivoCancelacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.MotivoCancelacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.MotivoCancelacion.EditorControl.Name = "NestedRadGridView";
            this.MotivoCancelacion.EditorControl.ReadOnly = true;
            this.MotivoCancelacion.EditorControl.ShowGroupPanel = false;
            this.MotivoCancelacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MotivoCancelacion.EditorControl.TabIndex = 0;
            this.MotivoCancelacion.Location = new System.Drawing.Point(146, 52);
            this.MotivoCancelacion.Name = "MotivoCancelacion";
            this.MotivoCancelacion.NullText = "Selecciona";
            this.MotivoCancelacion.Size = new System.Drawing.Size(280, 20);
            this.MotivoCancelacion.TabIndex = 11;
            this.MotivoCancelacion.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(13, 53);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(127, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "Motivo de modificación:";
            // 
            // Group
            // 
            this.Group.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Group.Controls.Add(this.FechaEntrega);
            this.Group.Controls.Add(this.lblFecha);
            this.Group.Controls.Add(this.Nota);
            this.Group.Controls.Add(this.lblNota);
            this.Group.Controls.Add(this.IdStatus);
            this.Group.Controls.Add(this.lblStatus);
            this.Group.Controls.Add(this.IdDocumento);
            this.Group.Controls.Add(this.lblRemision);
            this.Group.Controls.Add(this.radLabel6);
            this.Group.Controls.Add(this.MotivoCancelacion);
            this.Group.Dock = System.Windows.Forms.DockStyle.Top;
            this.Group.HeaderText = "Para cada remisión que desea cancelar debe capturar el motivo de cancelación";
            this.Group.Location = new System.Drawing.Point(0, 29);
            this.Group.Name = "Group";
            this.Group.Size = new System.Drawing.Size(442, 109);
            this.Group.TabIndex = 1;
            this.Group.TabStop = false;
            this.Group.Text = "Para cada remisión que desea cancelar debe capturar el motivo de cancelación";
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEntrega.Location = new System.Drawing.Point(227, 26);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(199, 20);
            this.FechaEntrega.TabIndex = 3;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "martes, 27 de julio de 2021";
            this.FechaEntrega.Value = new System.DateTime(2021, 7, 27, 22, 12, 11, 710);
            // 
            // lblFecha
            // 
            this.lblFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFecha.Location = new System.Drawing.Point(188, 27);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(37, 18);
            this.lblFecha.TabIndex = 2;
            this.lblFecha.Text = "Fecha:";
            // 
            // Nota
            // 
            this.Nota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nota.Location = new System.Drawing.Point(100, 77);
            this.Nota.MaxLength = 100;
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(326, 20);
            this.Nota.TabIndex = 13;
            // 
            // lblNota
            // 
            this.lblNota.Location = new System.Drawing.Point(13, 78);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(81, 18);
            this.lblNota.TabIndex = 12;
            this.lblNota.Text = "Observaciones:";
            // 
            // IdStatus
            // 
            this.IdStatus.DisplayMember = "Descriptor";
            this.IdStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdStatus.Location = new System.Drawing.Point(57, 26);
            this.IdStatus.Name = "IdStatus";
            this.IdStatus.NullText = "Selecciona";
            this.IdStatus.Size = new System.Drawing.Size(120, 20);
            this.IdStatus.TabIndex = 1;
            this.IdStatus.ValueMember = "Id";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(12, 27);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 18);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Status:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdDocumento.Location = new System.Drawing.Point(243, 26);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "# Núm.";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Size = new System.Drawing.Size(72, 20);
            this.IdDocumento.TabIndex = 5;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRemision
            // 
            this.lblRemision.Location = new System.Drawing.Point(183, 28);
            this.lblRemision.Name = "lblRemision";
            this.lblRemision.Size = new System.Drawing.Size(54, 18);
            this.lblRemision.TabIndex = 4;
            this.lblRemision.Text = "Remisión:";
            // 
            // TCancelar
            // 
            this.TCancelar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TCancelar.Etiqueta = "";
            this.TCancelar.Location = new System.Drawing.Point(0, 0);
            this.TCancelar.Name = "TCancelar";
            this.TCancelar.ReadOnly = false;
            this.TCancelar.ShowActualizar = false;
            this.TCancelar.ShowAutorizar = true;
            this.TCancelar.ShowCerrar = true;
            this.TCancelar.ShowEditar = false;
            this.TCancelar.ShowExportarExcel = false;
            this.TCancelar.ShowFiltro = false;
            this.TCancelar.ShowGuardar = false;
            this.TCancelar.ShowHerramientas = false;
            this.TCancelar.ShowImagen = false;
            this.TCancelar.ShowImprimir = false;
            this.TCancelar.ShowNuevo = false;
            this.TCancelar.ShowRemover = false;
            this.TCancelar.Size = new System.Drawing.Size(442, 29);
            this.TCancelar.TabIndex = 0;
            // 
            // NoConformidadStatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 140);
            this.Controls.Add(this.Group);
            this.Controls.Add(this.TCancelar);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(650, 400);
            this.MinimizeBox = false;
            this.Name = "NoConformidadStatusForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(650, 400);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cambio de Status";
            this.Load += new System.EventHandler(this.NoConformidadStatusForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group)).EndInit();
            this.Group.ResumeLayout(false);
            this.Group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRemision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TCancelar;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox Group;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        public Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel lblNota;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        private Telerik.WinControls.UI.RadLabel lblFecha;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox MotivoCancelacion;
        protected internal Telerik.WinControls.UI.RadDropDownList IdStatus;
        internal Telerik.WinControls.UI.RadTextBox IdDocumento;
        private Telerik.WinControls.UI.RadLabel lblRemision;
    }
}