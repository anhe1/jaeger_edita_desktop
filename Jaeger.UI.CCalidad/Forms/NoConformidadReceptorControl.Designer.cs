﻿namespace Jaeger.UI.CCalidad.Forms {
    partial class NoConformidadReceptorControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Actualizar = new Telerik.WinControls.UI.RadButton();
            this.Agregar = new Telerik.WinControls.UI.RadButton();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.PanelNombre = new Telerik.WinControls.UI.RadPanel();
            this.PanelRFC = new Telerik.WinControls.UI.RadPanel();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.IdDirectorio = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelNombre)).BeginInit();
            this.PanelNombre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRFC)).BeginInit();
            this.PanelRFC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            this.SuspendLayout();
            // 
            // Actualizar
            // 
            this.Actualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Actualizar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Actualizar.Enabled = false;
            this.Actualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Actualizar.Location = new System.Drawing.Point(652, 0);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(20, 20);
            this.Actualizar.TabIndex = 377;
            this.Actualizar.Text = "radButton1";
            // 
            // Agregar
            // 
            this.Agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Agregar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Agregar.Enabled = false;
            this.Agregar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Agregar.Location = new System.Drawing.Point(696, 0);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(20, 20);
            this.Agregar.TabIndex = 378;
            this.Agregar.Text = "radButton1";
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(35, 0);
            this.RFC.MaxLength = 14;
            this.RFC.Name = "RFC";
            this.RFC.NullText = "Registro Federal";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(100, 20);
            this.RFC.TabIndex = 15;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Nombre
            // 
            this.Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nombre.AutoSizeDropDownHeight = true;
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.DisplayMember = "Nombre";
            // 
            // Nombre.NestedRadGridView
            // 
            this.Nombre.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nombre.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nombre.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nombre.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nombre.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nombre.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdDirectorio";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdDirectorio";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "RFC";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "RFC";
            gridViewTextBoxColumn3.Width = 110;
            gridViewTextBoxColumn4.FieldName = "Nombre";
            gridViewTextBoxColumn4.HeaderText = "Denominación o Razón Social";
            gridViewTextBoxColumn4.Name = "Nombre";
            gridViewTextBoxColumn4.Width = 250;
            gridViewTextBoxColumn5.FieldName = "NombreComercial";
            gridViewTextBoxColumn5.HeaderText = "Nombre Comercial";
            gridViewTextBoxColumn5.Name = "NombreComercial";
            gridViewTextBoxColumn5.Width = 150;
            this.Nombre.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.Nombre.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nombre.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Nombre.EditorControl.Name = "NestedRadGridView";
            this.Nombre.EditorControl.ReadOnly = true;
            this.Nombre.EditorControl.ShowGroupPanel = false;
            this.Nombre.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Nombre.EditorControl.TabIndex = 0;
            this.Nombre.Location = new System.Drawing.Point(59, 0);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(447, 20);
            this.Nombre.TabIndex = 11;
            this.Nombre.TabStop = false;
            this.Nombre.ValueMember = "Id";
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(2, 1);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(50, 18);
            this.lblNombre.TabIndex = 10;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(4, 1);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 14;
            this.lblRFC.Text = "RFC:";
            // 
            // PanelNombre
            // 
            this.PanelNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelNombre.Controls.Add(this.Nombre);
            this.PanelNombre.Controls.Add(this.lblNombre);
            this.PanelNombre.Location = new System.Drawing.Point(1, 0);
            this.PanelNombre.Name = "PanelNombre";
            // 
            // 
            // 
            this.PanelNombre.RootElement.Opacity = 0D;
            this.PanelNombre.Size = new System.Drawing.Size(509, 20);
            this.PanelNombre.TabIndex = 376;
            // 
            // PanelRFC
            // 
            this.PanelRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelRFC.Controls.Add(this.RFC);
            this.PanelRFC.Controls.Add(this.lblRFC);
            this.PanelRFC.Location = new System.Drawing.Point(516, 0);
            this.PanelRFC.Name = "PanelRFC";
            // 
            // 
            // 
            this.PanelRFC.RootElement.Opacity = 0D;
            this.PanelRFC.Size = new System.Drawing.Size(137, 20);
            this.PanelRFC.TabIndex = 384;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Buscar.Enabled = false;
            this.Buscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Buscar.Location = new System.Drawing.Point(674, 0);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(20, 20);
            this.Buscar.TabIndex = 382;
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(-88, 65);
            this.IdDirectorio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ShowBorder = false;
            this.IdDirectorio.ShowUpDownButtons = false;
            this.IdDirectorio.Size = new System.Drawing.Size(37, 20);
            this.IdDirectorio.TabIndex = 381;
            this.IdDirectorio.TabStop = false;
            // 
            // NoConformidadReceptorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Actualizar);
            this.Controls.Add(this.Agregar);
            this.Controls.Add(this.PanelNombre);
            this.Controls.Add(this.PanelRFC);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.IdDirectorio);
            this.Name = "NoConformidadReceptorControl";
            this.Size = new System.Drawing.Size(721, 20);
            this.Load += new System.EventHandler(this.NoConformidadReceptorControl_Load);
            this.Resize += new System.EventHandler(this.NoConformidadReceptorControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelNombre)).EndInit();
            this.PanelNombre.ResumeLayout(false);
            this.PanelNombre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRFC)).EndInit();
            this.PanelRFC.ResumeLayout(false);
            this.PanelRFC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadButton Actualizar;
        public Telerik.WinControls.UI.RadButton Agregar;
        public Telerik.WinControls.UI.RadTextBox RFC;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Nombre;
        public Telerik.WinControls.UI.RadLabel lblNombre;
        public Telerik.WinControls.UI.RadLabel lblRFC;
        protected internal Telerik.WinControls.UI.RadPanel PanelNombre;
        protected internal Telerik.WinControls.UI.RadPanel PanelRFC;
        public Telerik.WinControls.UI.RadButton Buscar;
        public Telerik.WinControls.UI.RadSpinEditor IdDirectorio;
    }
}
