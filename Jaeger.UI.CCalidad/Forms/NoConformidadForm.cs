﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.CCalidad.Builder;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Aplication.CCalidad.Services;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.UI.CCalidad.Forms {
    public partial class NoConformidadForm : RadForm {
        #region declaraciones
        protected internal INoConformidadesService _Service;
        protected internal NoConformidadDetailModel _NoConformidad;
        #endregion

        public NoConformidadForm() {
            InitializeComponent();
        }

        public NoConformidadForm(INoConformidadesService service, NoConformidadDetailModel model) {
            InitializeComponent();
            this._Service = service;
            this._NoConformidad = model;
        }

        private void NoConformidadForm_Load(object sender, EventArgs e) {
            using (INoConformidadGridBuilder view = new NoConformidadGridBuilder()) {
                this.TCausaRaiz.GridData.Columns.AddRange(view.Templetes().CausaRaiz().Build());
                this.TCorrectivo.GridData.Columns.AddRange(view.Templetes().AccionesCorrectivas().Build());
                this.TPreventivo.GridData.Columns.AddRange(view.Templetes().AccionesPreventivas().Build());
                this.TOrden.GridData.Columns.AddRange(view.Templetes().OrdenProduccion().Build());
                this.TCosto.GridData.Columns.AddRange(view.Templetes().CostoNoCalidad().Build());
            }

            this.TNoConformidad.Folio.ReadOnly = true;

            var clasificacion5m = this.TCausaRaiz.GridData.Columns["IdClasificacion5"] as GridViewComboBoxColumn;
            clasificacion5m.DisplayMember = "Descripcion";
            clasificacion5m.ValueMember = "Id";
            clasificacion5m.DataSource = NoConformidadService.GetClasificacion();

            var efecto = this.TCausaRaiz.GridData.Columns["IdEfecto"] as GridViewComboBoxColumn;
            efecto.DisplayMember = "Descripcion";
            efecto.ValueMember = "Id";
            efecto.DataSource = NoConformidadService.GetEfecto();

            var accion = this.TCorrectivo.GridData.Columns["IdTipoAccion"] as GridViewComboBoxColumn;
            accion.DisplayMember = "Descripcion";
            accion.ValueMember = "Id";
            accion.DataSource = NoConformidadService.GetTipoAccion();

            this.TNoConformidad.TDocumento.Status.DataSource = NoConformidadService.GetStatus();
            this.TNoConformidad.Departamento.DataSource = this._Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());

            this.TCausaRaiz.Nuevo.Click += TCausaRaiz_Nuevo_Click;
            this.TCausaRaiz.Editar.Click += TCausaRaiz_Editar_Click;
            this.TCausaRaiz.Remover.Click += TCausaRaiz_Remover_Click;

            this.TCorrectivo.Nuevo.Click += TCorrectivo_Nuevo_Click;
            this.TCorrectivo.Editar.Click += TCorrectivo_Edita_Click;
            this.TCorrectivo.Remover.Click += TCorrectivo_Remover_Click;

            this.TPreventivo.Nuevo.Click += TPreventivo_Nuevo_Click;
            this.TPreventivo.Editar.Click += TPreventivo_Editar_Click;
            this.TPreventivo.Remover.Click += TPreventivo_Remover_Click;

            var departamentos = this._Service.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>());
            var departamento = this.TCorrectivo.GridData.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamento.DisplayMember = "Descriptor";
            departamento.ValueMember = "IdDepartamento";
            departamento.DataSource = new List<DepartamentoModel>(departamentos);

            var departamento1 = this.TCausaRaiz.GridData.Columns["IdDepartamento"] as GridViewComboBoxColumn;
            departamento1.DisplayMember = "Descriptor";
            departamento1.ValueMember = "IdDepartamento";
            departamento1.DataSource = new List<DepartamentoModel>(departamentos);

            this.TOrden.Nuevo.Click += TOrdenRelacion_Nuevo_Click;
            this.TOrden.Remover.Click += TOrdenRelacion_Remover_Click;

            this.TCosto.Nuevo.Click += TCosto_Nuevo_Click;
            this.TCosto.Remover.Click += TCosto_Remover_Click;

            this.TNoConformidad.TDocumento.Actualizar.Click += TNoConformidad_Actualizar_Click;
            this.TNoConformidad.TDocumento.Imprimir.Click += TNoConformidad_Imprimir_Click;
            this.TNoConformidad.TDocumento.Imprimir.Enabled = true;
            this.TNoConformidad.TDocumento.Guardar.Click += TNoConformidad_Guardar_Click;
            this.TNoConformidad.TDocumento.Cerrar.Click += TNoConformidad_Cerrar_Click;

            this.TImagen.Nuevo.Click += this.TImagen_Nuevo_Click;
            this.TImagen.Remover.Click += this.TImagen_Remover_Click;
            this.TImagen.GridData.TableElement.RowHeight = 300;
            this.TImagen.GridData.Columns.Add(new GridViewImageColumn {
                FieldName = "Imagen",
                HeaderText = "Imagen",
                ImageLayout = ImageLayout.Zoom,
                Name = "column1",
                Width = 300
            });

            this.TCausaRaiz.Permisos = new Domain.Base.ValueObjects.UIAction() { Agregar = true, Remover = true, Editar = true };
            this.TCorrectivo.Permisos = new Domain.Base.ValueObjects.UIAction() { Agregar = true, Remover = true, Editar = true };
            this.TPreventivo.Permisos = new Domain.Base.ValueObjects.UIAction() { Agregar = true, Remover = true, Editar = true };
            this.TOrden.Permisos = new Domain.Base.ValueObjects.UIAction() { Agregar = true, Remover = true };
            this.TCosto.Permisos = new Domain.Base.ValueObjects.UIAction() { Agregar = true, Remover = true };

            this.TNoConformidad.TDocumento.Actualizar.PerformClick();
        }

        #region no conformidad
        private void TNoConformidad_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void TNoConformidad_Imprimir_Click(object sender, EventArgs e) {
            using (var printer = new ReporteForm(this._Service.GetPrinter(this._NoConformidad.IdNoConformidad))) {
                printer.ShowDialog(this);
            }
        }

        private void TNoConformidad_Guardar_Click(object sender, EventArgs e) {
            if (this.Validar() == false) {
                RadMessageBox.Show("Con errores");
                return;
            }
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void TNoConformidad_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region Causa Raiz
        protected virtual void TCausaRaiz_Nuevo_Click(object sender, EventArgs e) {
            using (var form = new CausaRaizForm(this._Service, null)) {
                form.Selected += TCausaRaisForm_Selected;
                form.ShowDialog(this);
            }
        }

        protected virtual void TCausaRaisForm_Selected(object sender, CausaRaizDetailModel e) {
            if (e != null) {
                if (e.Id == 0) {
                    this._NoConformidad.CausaRaiz.Add(e);
                }
            }
        }

        protected virtual void TCausaRaiz_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TCausaRaiz.GetCurrent<CausaRaizDetailModel>();
            if (seleccionado != null) {
                using (var form = new CausaRaizForm(this._Service, seleccionado)) {
                    form.Selected += TCausaRaisForm_Selected;
                    form.ShowDialog(this);
                }
            }
        }

        protected virtual void TCausaRaiz_Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TCausaRaiz.GetCurrent<CausaRaizDetailModel>();
            if (seleccionado != null) {

                if (seleccionado.Id > 0) {
                    if (RadMessageBox.Show("¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        seleccionado.Activo = false;
                        this.TCausaRaiz.GridData.CurrentRow.IsVisible = false;
                    }
                } else {
                    this.TCausaRaiz.GridData.Rows.Remove(this.TCausaRaiz.GridData.CurrentRow);
                }
            }
        }
        #endregion

        #region acciones correctivo
        protected virtual void TCorrectivo_Nuevo_Click(object sender, EventArgs e) {
            using (var form = new AccionCorrectivaForm(this._Service, null)) {
                form.Selected += TAccionCorrectivaForm_Selected;
                form.ShowDialog(this);
            }
        }

        protected virtual void TCorrectivo_Edita_Click(object sender, EventArgs eventArgs) {
            var seleccionado = this.TCorrectivo.GetCurrent<AccionCorrectivaDetailModel>();
            if (seleccionado != null) {
                using (var form = new AccionCorrectivaForm(this._Service, seleccionado)) {
                    form.Selected += TAccionCorrectivaForm_Selected;
                    form.ShowDialog(this);
                }
            }
        }

        protected virtual void TCorrectivo_Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TCorrectivo.GetCurrent<AccionCorrectivaDetailModel>();
            if (seleccionado != null) {
                if (seleccionado.Id > 0) {
                    if (RadMessageBox.Show("¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        seleccionado.Activo = false;
                        this.TCorrectivo.GridData.CurrentRow.IsVisible = false;
                    }
                } else {
                    this.TCorrectivo.GridData.Rows.Remove(this.TCorrectivo.GridData.CurrentRow);
                }
            }
        }

        protected virtual void TAccionCorrectivaForm_Selected(object sender, AccionCorrectivaDetailModel e) {
            if (e != null) {
                if (e.Id == 0) {
                    this._NoConformidad.AccionesCorrectivas.Add(e);
                }
            }
        }
        #endregion

        #region acciones preventivas
        protected virtual void TPreventivo_Nuevo_Click(object sender, EventArgs e) {
            using (var form = new AccionPreventivaForm(this._Service, null)) {
                form.Selected += TAccionPreventivaForm_Selected;
                form.ShowDialog(this);
            }
        }

        protected virtual void TPreventivo_Editar_Click(object sender, EventArgs eventArgs) {
            var seleccionado = this.TPreventivo.GetCurrent<AccionPreventivaDetailModel>();
            if (seleccionado != null) {
                using (var form = new AccionPreventivaForm(this._Service, seleccionado)) {
                    form.Selected += TAccionPreventivaForm_Selected;
                    form.ShowDialog(this);
                }
            }
        }

        protected virtual void TAccionPreventivaForm_Selected(object sender, AccionPreventivaDetailModel e) {
            if (e != null) {
                if (e.Id == 0) {
                    this._NoConformidad.AccionesPreventivas.Add(e);
                }
            }
        }

        protected virtual void TPreventivo_Remover_Click(object sender, EventArgs e) {
            var seleccionado = this.TPreventivo.GetCurrent<AccionPreventivaDetailModel>();
            if (seleccionado != null) {

                if (seleccionado.Id > 0) {
                    if (RadMessageBox.Show("¿Esta seguro de remover el elemento?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                        seleccionado.Activo = false;
                        this.TPreventivo.GridData.CurrentRow.IsVisible = false;
                    }
                } else {
                    this.TPreventivo.GridData.Rows.Remove(this.TPreventivo.GridData.CurrentRow);
                }
            }
        }
        #endregion

        #region orden de produccion
        protected virtual void TOrdenRelacion_Nuevo_Click(object sender, EventArgs e) {

        }

        protected virtual void TOrdenRelacion_Remover_Click(object sender, EventArgs e) {

        }

        protected virtual void TCosto_Nuevo_Click(object sender, EventArgs e) {

        }

        protected virtual void TCosto_Remover_Click(object sender, EventArgs e) {
            if (this.TCosto.GridData.CurrentRow != null) {
                var seleccionado = this.TCosto.GetCurrent<CostoNoCalidadDetailModel>();
                if (seleccionado != null) {
                    if (seleccionado.Id == 0) {
                        seleccionado.Activo = false;
                        this.TCosto.GridData.CurrentRow.IsVisible = false;
                    } else {
                        this.TCosto.GridData.Rows.Remove(this.TCosto.GridData.CurrentRow);
                    }
                }
            }
        }
        #endregion

        #region
        public virtual void TImagen_Nuevo_Click(object sender, EventArgs e) {
            if (this._NoConformidad.IdNoConformidad == 0) {
                RadMessageBox.Show(this, "Debes guardar el documento antes de agregar imagenes", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            var openFile = new OpenFileDialog { Filter = "*.jpg|*.JPG|*.jpeg|*.JPEG|*.png|*.PNG" };
            if (openFile.ShowDialog(this) == DialogResult.Cancel)
                return;

            if (System.IO.File.Exists(openFile.FileName) == false) {
                RadMessageBox.Show(this, Properties.Resources.msg_file_noExist, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            var contenido = Jaeger.Util.FileService.ReadFileB64(openFile.FileName);
            if (contenido != null) {
                var media = MediaService.Create();
                media.Tag = contenido;
                media.FileName = openFile.FileName;
                media.URL = openFile.FileName;
                this._NoConformidad.Media.Add(media);
            } else {
                RadMessageBox.Show(this, "Ocurrio un error", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
            }
        }

        public virtual void TImagen_Remover_Click(object sender, EventArgs e) {
            if (this.TImagen.GridData.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.msg_objeto_remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var seleccionado = this.TImagen.GridData.CurrentRow.DataBoundItem as MediaModel;
                    if (seleccionado.Id > 0) {
                        if (this._Service.Save(seleccionado) != null) {
                            try {
                                this.TImagen.GridData.Rows.Remove(this.TImagen.GridData.CurrentRow);
                            } catch (Exception ex) {
                                RadMessageBox.Show(this, "Error: " + ex.Message, "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                            }
                        }
                    } else {
                        this.TImagen.GridData.Rows.Remove(this.TImagen.GridData.CurrentRow);
                    }
                }
            }
        }
        #endregion

        #region
        protected virtual void CreateBinding() {
            this.TNoConformidad.TDocumento.Status.DataBindings.Clear();
            this.TNoConformidad.TDocumento.Status.DataBindings.Add("SelectedValue", this._NoConformidad, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Folio.DataBindings.Clear();
            this.TNoConformidad.Folio.DataBindings.Add("Text", this._NoConformidad, "IdNoConformidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.FechaEmision.DataBindings.Clear();
            this.TNoConformidad.FechaEmision.DataBindings.Add("Text", this._NoConformidad, "Fecha", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Detector.DataBindings.Clear();
            this.TNoConformidad.Detector.DataBindings.Add("Text", this._NoConformidad, "Detector", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Descripcion.DataBindings.Clear();
            this.TNoConformidad.Descripcion.DataBindings.Add("Text", this._NoConformidad, "Descripcion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Nota.DataBindings.Clear();
            this.TNoConformidad.Nota.DataBindings.Add("Text", this._NoConformidad, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Departamento.DataBindings.Clear();
            this.TNoConformidad.Departamento.DataBindings.Add("SelectedValue", this._NoConformidad, "IdDepartamento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Cantidad.DataBindings.Clear();
            this.TNoConformidad.Cantidad.DataBindings.Add("Value", this._NoConformidad, "Cantidad", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.Costo.DataBindings.Clear();
            this.TNoConformidad.Costo.DataBindings.Add("Value", this._NoConformidad, "Costo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TNoConformidad.TDocumento.Guardar.DataBindings.Clear();
            this.TNoConformidad.TDocumento.Guardar.DataBindings.Add("Enabled", this._NoConformidad, "IsEditable", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TCausaRaiz.GridData.DataSource = this._NoConformidad.CausaRaiz;
            this.TCorrectivo.GridData.DataSource = this._NoConformidad.AccionesCorrectivas;
            this.TPreventivo.GridData.DataSource = this._NoConformidad.AccionesPreventivas;
            this.TOrden.GridData.DataSource = this._NoConformidad.OrdenProduccion;
            this.TCosto.GridData.DataSource = this._NoConformidad.CostosNoCalidad;
            this.TImagen.GridData.DataSource = this._NoConformidad.Media;

            this.TCausaRaiz.ReadOnly = !this._NoConformidad.IsEditable;
            this.TCorrectivo.ReadOnly = !this._NoConformidad.IsEditable;
            this.TPreventivo.ReadOnly = !this._NoConformidad.IsEditable;
            this.TOrden.ReadOnly = !this._NoConformidad.IsEditable;
            this.TCosto.ReadOnly = !this._NoConformidad.IsEditable;
            this.TImagen.ReadOnly = !this._NoConformidad.IsEditable;

            this.TNoConformidad.ReadOnly = !this._NoConformidad.IsEditable;
        }

        protected virtual void Consultar() {
            if (this._NoConformidad == null) {
                this._NoConformidad = new NoConformidadDetailModel();
            } else {
                this._NoConformidad = this._Service.GetById(this._NoConformidad.IdNoConformidad);
            }
        }

        protected virtual void Guardar() {
            this._NoConformidad = this._Service.Save(this._NoConformidad);
        }

        protected virtual bool Validar() {
            return true;
        }
        #endregion
    }
}
