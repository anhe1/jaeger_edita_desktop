﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.CCalidad.Builder {
    internal class NoConformidadGridBuilder : GridViewBuilder, IGridViewBuilder, INoConformidadGridBuilder, INoConformidadTempletesGridBuilder, INoConformidadColumnsGridBuilder, INoConformidadCausaRaizTempletesGridBuilder,
        INoConformidadCausaRaizColumnsGridBuilder, INoConformidadAccionesCorrectivasTempletesGridBuilder, INoConformidadAccionesCorrectivasColumnsGridBuilder, INoConformidadAccionesPreventivasTempletesGridBuilder, INoConformidadAccionesPreventivasColumnsGridBuilder,
        INoConformidadOrdenColumnsGridBuilder , INoConformidadCostoColumnsGridBuilder {

        public NoConformidadGridBuilder() : base() { }

        #region templetes
        public INoConformidadTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public INoConformidadTempletesGridBuilder Master() {
            this.Numero().FechaEmision().CboStatus().IdDepartamento().Descripcion().Cantidad().Unidad().Importe().Detector().Creo().Modifica().FechaModifica();
            return this;
        }

        public INoConformidadCausaRaizTempletesGridBuilder CausaRaiz() {
            this.IdNoConformidad();
            this.IdClasificacion5();
            this.Descripcion();
            this.IdEfecto();
            this.IdDepartamento();
            this.Responsable();
            this.CheckResponsable();
            return this;
        }

        public INoConformidadAccionesCorrectivasTempletesGridBuilder AccionesCorrectivas() {
            this.IdTipoAccion();
            this.Descripcion();
            this.IdDepartamento();
            this.Responsable();
            return this;
        }

        public INoConformidadAccionesPreventivasTempletesGridBuilder AccionesPreventivas() {
            //this.Afectacion();
            this.Descripcion();
            this.Fecha();
            this.Responsable();
            return this;
        }

        public INoConformidadTempletesGridBuilder OrdenProduccion() {
            this.IdOrden();
            this.Cliente();
            this.Descripcion();
            return this;
        }

        public INoConformidadTempletesGridBuilder CostoNoCalidad() {
            this.IdOrden();
            this.Cliente();
            this.Descripcion();
            this.Maquinaria();
            this.ManoObra();
            this.MateriaPrima();
            this.Consumibles();
            return this;
        }
        #endregion

        #region No Conformidad
        public INoConformidadColumnsGridBuilder Numero() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdNoConformidad",
                FormatString = this.FormarStringFolio,
                Name = "IdNoConformidad",
                HeaderText = "# Núm.",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate,
                FieldName = "Fecha",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Emisión",
                Name = "Fecha",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85,
                ReadOnly = true,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Fecha() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate,
                FieldName = "Fecha",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fecha",
                Name = "Fecha",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder CboStatus() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 85,
                DisplayMember = "Descriptor",
                ValueMember = "Id",
            };
            this._Columns.Add(combo);
            return this;
        }

        public INoConformidadColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = this.FormatStringNumber,
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }

        /// <summary>
        /// importe total de costos de no calidad
        /// </summary>
        public INoConformidadColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Costo",
                FormatString = this.FormatStringMoney,
                HeaderText = "Costo",
                Name = "Costo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder IdUnidad() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdUnidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                FormatString = this.FormatStringMoney,
                HeaderText = "Unidad",
                Name = "Unidad",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 85,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Descripcion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Descripcion",
                HeaderText = "Descripción",
                Name = "Descripcion",
                Width = 450
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Responsable() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Responsable",
                HeaderText = "Responsable",
                Name = "Responsable",
                Width = 200
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Detector() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType= typeof(string),
                FieldName = "Detector",
                HeaderText = "Detector",
                Name = "Detector",
                Width = 200
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder IdDepartamento() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdDepartamento",
                HeaderText = "Departamento",
                Name = "IdDepartamento",
                Width = 200,
            };
            this._Columns.Add(combo);
            return this;
        }

        public INoConformidadColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard,
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder FechaModifica() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.ShortDate,
                FieldName = "FechaModifica",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. \r\nModifica",
                Name = "FechaModifica",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 85,
                ReadOnly = true,
            });
            return this;
        }

        public INoConformidadColumnsGridBuilder Modifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard,
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        #region Analisis Causa Raiz
        public INoConformidadCausaRaizColumnsGridBuilder IdNoConformidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdNoConformidad",
                FormatString = "{0:N0}",
                Name = "IdNoConformidad",
                HeaderText = "# Núm.",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 65,
                VisibleInColumnChooser = false,
                IsVisible = false,
            });
            return this;
        }

        public INoConformidadCausaRaizColumnsGridBuilder IdClasificacion5() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdClasificacion5",
                FormatString = "{0:N0}",
                Name = "IdClasificacion5",
                HeaderText = "Clasificacion",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 165
            });
            return this;
        }

        public INoConformidadCausaRaizColumnsGridBuilder IdEfecto() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdEfecto",
                FormatString = "{0:N0}",
                Name = "IdEfecto",
                HeaderText = "Efecto",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 120
            });
            return this;
        }

        public INoConformidadCausaRaizTempletesGridBuilder CheckResponsable() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                DataType = typeof(bool),
                FieldName = "IsResponsable",
                Name = "IsResponsable",
                HeaderText = "Resp. NC",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 50
            });
            return this;
        }
        #endregion

        #region Acciones Correctivas
        public INoConformidadAccionesCorrectivasColumnsGridBuilder IdTipoAccion() {
            var combo = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdTipoAccion",
                HeaderText = "Tipo de Acción",
                Name = "IdTipoAccion",
                Width = 95,
                DisplayMember = "Descriptor",
                TextAlignment = ContentAlignment.MiddleLeft,
                ValueMember = "Id",
            };
            this._Columns.Add(combo);
            return this;
        }
        #endregion

        #region Acciones Preventivas
        public INoConformidadAccionesPreventivasColumnsGridBuilder Afectacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Afectacion",
                HeaderText = "Afectación",
                Name = "Afectacion",
                Width = 200
            });
            return this;
        }
        #endregion

        #region Orden de Produccion
        public INoConformidadOrdenColumnsGridBuilder IdOrden() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdOrden",
                FormatString = "{0:N0}",
                Name = "IdOrden",
                HeaderText = "# Orden",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public INoConformidadOrdenColumnsGridBuilder Cliente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Cliente",
                HeaderText = "Cliente",
                Name = "Cliente",
                Width = 200
            });
            return this;
        }
        #endregion

        #region Costos de No Calidad
        public INoConformidadOrdenColumnsGridBuilder Maquinaria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Maquinaria",
                FormatString = this.FormatStringNumber,
                HeaderText = "Maquinaria",
                Name = "Maquinaria",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }

        public INoConformidadOrdenColumnsGridBuilder ManoObra() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ManoObra",
                FormatString = this.FormatStringNumber,
                HeaderText = "Mano de Obra",
                Name = "ManoObra",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }
        
        public INoConformidadOrdenColumnsGridBuilder MateriaPrima() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "MateriaPrima",
                FormatString = this.FormatStringNumber,
                HeaderText = "Materia Prima",
                Name = "MateriaPrima",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }
        
        public INoConformidadOrdenColumnsGridBuilder Consumibles() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Consumibles",
                FormatString = this.FormatStringNumber,
                HeaderText = "Consumibles",
                Name = "Consumibles",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85,
            });
            return this;
        }
        #endregion
    }
}
