﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.CCalidad.Builder {
    internal interface INoConformidadGridBuilder : IGridViewBuilder {
        INoConformidadTempletesGridBuilder Templetes();
    }

    internal interface INoConformidadTempletesGridBuilder : IGridViewBuilder {
        INoConformidadTempletesGridBuilder Master();
        INoConformidadCausaRaizTempletesGridBuilder CausaRaiz();
        INoConformidadAccionesCorrectivasTempletesGridBuilder AccionesCorrectivas();
        INoConformidadAccionesPreventivasTempletesGridBuilder AccionesPreventivas();
        INoConformidadTempletesGridBuilder OrdenProduccion();
        INoConformidadTempletesGridBuilder CostoNoCalidad();
    }

    internal interface INoConformidadColumnsGridBuilder : IGridViewBuilder {
        INoConformidadColumnsGridBuilder Numero();
        INoConformidadColumnsGridBuilder CboStatus();
        INoConformidadColumnsGridBuilder FechaEmision();
        INoConformidadColumnsGridBuilder Cantidad();
        INoConformidadColumnsGridBuilder IdUnidad();
        INoConformidadColumnsGridBuilder Unidad();
        INoConformidadColumnsGridBuilder Descripcion();
        INoConformidadColumnsGridBuilder Responsable();
        INoConformidadColumnsGridBuilder Detector();
        INoConformidadColumnsGridBuilder IdDepartamento();
        /// <summary>
        /// importe total de costos de no calidad
        /// </summary>
        INoConformidadColumnsGridBuilder Importe();
        INoConformidadColumnsGridBuilder Creo();
        INoConformidadColumnsGridBuilder FechaModifica();
        INoConformidadColumnsGridBuilder Modifica();
    }
    public interface INoConformidadCausaRaizTempletesGridBuilder : IGridViewBuilder {
        INoConformidadCausaRaizTempletesGridBuilder CausaRaiz();
        INoConformidadCausaRaizTempletesGridBuilder CheckResponsable();
    }

    public interface INoConformidadCausaRaizColumnsGridBuilder : IGridViewBuilder {
        INoConformidadCausaRaizColumnsGridBuilder IdNoConformidad();
        INoConformidadCausaRaizColumnsGridBuilder IdClasificacion5();
        INoConformidadCausaRaizColumnsGridBuilder IdEfecto();
    }

    public interface INoConformidadAccionesCorrectivasTempletesGridBuilder : IGridViewBuilder {
        INoConformidadAccionesCorrectivasTempletesGridBuilder AccionesCorrectivas();
    }

    public interface INoConformidadAccionesCorrectivasColumnsGridBuilder : IGridViewBuilder {
        INoConformidadAccionesCorrectivasColumnsGridBuilder IdTipoAccion();
    }

    public interface INoConformidadAccionesPreventivasTempletesGridBuilder : IGridViewBuilder {
        INoConformidadAccionesPreventivasTempletesGridBuilder AccionesPreventivas();
    }

    public interface INoConformidadAccionesPreventivasColumnsGridBuilder : IGridViewBuilder {
        INoConformidadAccionesPreventivasColumnsGridBuilder Afectacion();
    }

    public interface INoConformidadOrdenColumnsGridBuilder : IGridViewBuilder {
        INoConformidadOrdenColumnsGridBuilder IdOrden();
        INoConformidadOrdenColumnsGridBuilder Cliente();
    }

    public interface INoConformidadCostoColumnsGridBuilder : IGridViewBuilder {
        INoConformidadOrdenColumnsGridBuilder Maquinaria();
        INoConformidadOrdenColumnsGridBuilder ManoObra();
        INoConformidadOrdenColumnsGridBuilder MateriaPrima();
        INoConformidadOrdenColumnsGridBuilder Consumibles();
    }
}
