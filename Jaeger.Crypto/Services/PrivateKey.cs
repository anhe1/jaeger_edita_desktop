﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Crypto.Services {
    public class PrivateKey {
        #region declaraciones
        private string mensajeErrorField;
        private int codigoErrorField;
        private string tipoCertificadoField;
        private string moduloField;
        private byte[] bufferLlaveField;
        private RSACryptoServiceProvider llaveRsaField;
        #endregion

        #region propiedades
        public int CodigoDeError {
            get {
                return this.codigoErrorField;
            }
        }

        public RSACryptoServiceProvider LlavePrivadaRsa {
            get {
                return this.llaveRsaField;
            }
        }

        public string MensajeDeError {
            get {
                return this.mensajeErrorField;
            }
        }

        public string Modulus {
            get {
                return this.moduloField;
            }
        }

        public string TipoCertificado {
            get {
                return this.tipoCertificadoField;
            }
        }
        #endregion

        public PrivateKey() {
            this.Inicializar();
        }

        public PrivateKey(string pathFileKey, string password) {
            this.Inicializar();
            this.Cargar(pathFileKey, password);
        }

        public void Cargar(string pathFileKey, string password) {
            if (pathFileKey == "") {
                this.mensajeErrorField = "La ruta de la llave privada no puede ser vacía.";
                this.codigoErrorField = 121;
            } else if (password == "") {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            } else if (File.Exists(pathFileKey)) {
                try {
                    byte[] numArray = File.ReadAllBytes(pathFileKey);
                    if (numArray != null) {
                        this.Cargar(numArray, password);
                    } else {
                        this.mensajeErrorField = "Archivo de llave vacío.";
                        this.codigoErrorField = 121;
                        return;
                    }
                } catch (Exception exception) {
                    this.mensajeErrorField = string.Concat("Error al leer el archivo de llave. ", exception.Message);
                    this.codigoErrorField = 521;
                    return;
                }
            } else {
                this.mensajeErrorField = "Ruta de archivo de llave privada inválida o no existe.";
                this.codigoErrorField = 221;
            }
        }

        public void Cargar(byte[] keyblob, string password) {
            if (keyblob == null) {
                this.mensajeErrorField = "El arreglo de bytes de la llave privada no puede ser nulo.";
                this.codigoErrorField = 123;
            } else if (!(password == "")) {
                RSACryptoServiceProvider rSACryptoServiceProvider = null;
                rSACryptoServiceProvider = this.DecodeX509PublicKey(keyblob);
                if (rSACryptoServiceProvider == null) {
                    rSACryptoServiceProvider = this.DecodeRsaPrivateKey(keyblob);
                    if (rSACryptoServiceProvider == null) {
                        rSACryptoServiceProvider = this.DecodePrivateKeyInfo(keyblob);
                        if (rSACryptoServiceProvider == null) {
                            SecureString secureString = this.ToSecureString(password);
                            if (secureString != null) {
                                rSACryptoServiceProvider = this.DecodeEncryptedPrivateKeyInfo(keyblob, secureString);
                                if (rSACryptoServiceProvider != null) {
                                    this.llaveRsaField = rSACryptoServiceProvider;
                                    this.tipoCertificadoField = "Llave privada PKCS #8 encriptada";
                                } else if (this.mensajeErrorField == "") {
                                    this.mensajeErrorField = "Llave privada incorrecta o dañada.";
                                    this.codigoErrorField = 321;
                                }
                            } else {
                                this.mensajeErrorField = "La contraseña de la llave no puede ser vacía.";
                                this.codigoErrorField = 122;
                            }
                        } else {
                            this.llaveRsaField = rSACryptoServiceProvider;
                            this.tipoCertificadoField = "Llave privada PKCS #8 sin encriptación";
                        }
                    } else {
                        this.llaveRsaField = rSACryptoServiceProvider;
                        this.tipoCertificadoField = "Llave privada RSA";
                    }
                } else {
                    this.llaveRsaField = rSACryptoServiceProvider;
                    this.tipoCertificadoField = "Llave pública X509";
                }
            } else {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            }
        }

        public void CargarB64(string llavePrivadaB64, string contraseniaLlave) {
            if (llavePrivadaB64 == "") {
                this.mensajeErrorField = "La cadena de la llave privada en base 64 no puede ser vacía.";
                this.codigoErrorField = 124;
            } else if (!(contraseniaLlave == "")) {
                try {
                    byte[] numArray = Convert.FromBase64String(llavePrivadaB64);
                    if (numArray != null) {
                        this.Cargar(numArray, contraseniaLlave);
                    } else {
                        this.mensajeErrorField = "Archivo de llave vacío.";
                        this.codigoErrorField = 124;
                        return;
                    }
                } catch (Exception exception) {
                    this.mensajeErrorField = string.Concat("Error al leer el archivo de llave. ", exception.Message);
                    this.codigoErrorField = 521;
                    return;
                }
            } else {
                this.mensajeErrorField = "La contraseña de la llave privada no puede ser vacía.";
                this.codigoErrorField = 122;
            }
        }

        public void ExportarPem(string rutaArchivoPem) {
            if (rutaArchivoPem == "") {
                this.mensajeErrorField = "La ruta del archivo PEM no puede ser vacía.";
                this.codigoErrorField = 0;
            } else if (this.bufferLlaveField == null) {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 101;
            } else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPem))) {
                rutaArchivoPem = string.Concat(Path.GetDirectoryName(rutaArchivoPem), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPem));
                rutaArchivoPem = string.Concat(rutaArchivoPem, ".pem");
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN RSA PRIVATE KEY-----");
                    string base64String = Convert.ToBase64String(this.bufferLlaveField);
                    for (int i = 0; i < base64String.Length - 1; i += 64) {
                        if (i + 64 >= base64String.Length - 1) {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        } else {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END RSA PRIVATE KEY-----");
                    File.WriteAllText(rutaArchivoPem, stringBuilder.ToString(), new UTF8Encoding(false));
                    if (!File.Exists(rutaArchivoPem)) {
                        this.mensajeErrorField = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this.codigoErrorField = 505;
                    }
                } catch (Exception exception) {
                    this.mensajeErrorField = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this.codigoErrorField = 506;
                }
            } else {
                this.mensajeErrorField = "El directorio especificado no es válido o no existe.";
                this.codigoErrorField = 201;
            }
        }

        public string ExportarPemB64() {
            if (this.bufferLlaveField == null) {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 101;
            } else {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN RSA PRIVATE KEY-----");
                    string base64String = Convert.ToBase64String(this.bufferLlaveField);
                    for (int i = 0; i < base64String.Length - 1; i += 64) {
                        if (i + 64 >= base64String.Length - 1) {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        } else {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END RSA PRIVATE KEY-----");
                    this.mensajeErrorField = string.Empty;
                    this.codigoErrorField = 0;
                    return stringBuilder.ToString();

                } catch (Exception exception) {
                    this.mensajeErrorField = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this.codigoErrorField = 506;
                }
            }
            return string.Empty;
        }

        public string ExportarXml() {
            string xmlString;
            if (this.llaveRsaField != null) {
                xmlString = this.llaveRsaField.ToXmlString(true);
            } else {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 103;
                xmlString = null;
            }
            return xmlString;
        }

        public string SellarCadenaSha1(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA1");
        }

        public string SellarCadenaSha256(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA256");
        }

        public string SellarCadenaSha512(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA512");
        }

        #region metodos privados
        ~PrivateKey() {
            this.mensajeErrorField = null;
            this.codigoErrorField = 0;
            this.tipoCertificadoField = null;
            this.moduloField = null;
            this.bufferLlaveField = null;
            if (this.llaveRsaField != null) {
                this.llaveRsaField = null;
            }
            GC.Collect();
        }

        private int GetIntegerSize(BinaryReader binr) {
            int num;
            byte num1 = 0;
            byte num2 = 0;
            byte num3 = 0;
            int num4 = 0;
            num1 = binr.ReadByte();
            if (num1 == 2) {
                num1 = binr.ReadByte();
                if (num1 == 129) {
                    num4 = binr.ReadByte();
                } else if (num1 != 130) {
                    num4 = num1;
                } else {
                    num3 = binr.ReadByte();
                    num2 = binr.ReadByte();
                    byte[] numArray = new byte[] { num2, num3, 0, 0 };
                    num4 = BitConverter.ToInt32(numArray, 0);
                }
                while (binr.ReadByte() == 0) {
                    num4--;
                }
                binr.BaseStream.Seek((long)-1, SeekOrigin.Current);
                num = num4;
            } else {
                num = 0;
            }
            return num;
        }

        private void Inicializar() {
            this.mensajeErrorField = "";
            this.codigoErrorField = 0;
            this.tipoCertificadoField = "";
            this.moduloField = "";
            this.llaveRsaField = null;
        }

        private string SellarCadenaCodificacion(string cadenaOriginal, string codSha) {
            this.mensajeErrorField = "";
            this.codigoErrorField = 0;
            byte[] numArray;
            string base64String;
            if (this.llaveRsaField == null) {
                this.mensajeErrorField = "No ha cargado una llave privada.";
                this.codigoErrorField = 103;
                base64String = null;
            } else if (!(cadenaOriginal == "")) {
                RSACryptoServiceProvider rSACryptoServiceProvider = this.llaveRsaField;
                try {
                    byte[] bytes = (new UTF8Encoding()).GetBytes(cadenaOriginal);
                    try {
                        try {
                            numArray = rSACryptoServiceProvider.SignData(bytes, CryptoConfig.MapNameToOID(codSha));
                        } catch (CryptographicException cryptographicException) {
                            this.mensajeErrorField = string.Concat("Error interno al sellar la cadena original: ", cryptographicException.Message);
                            this.codigoErrorField = 522;
                            numArray = null;
                        }
                    } finally {
                        rSACryptoServiceProvider.PersistKeyInCsp = false;
                    }
                } finally {
                    if (rSACryptoServiceProvider != null) {
                        ((IDisposable)rSACryptoServiceProvider).Dispose();
                    }
                }
                base64String = Convert.ToBase64String(numArray);
            } else {
                this.mensajeErrorField = "La cadena original no puede ser vacía.";
                this.codigoErrorField = 125;
                base64String = null;
            }
            return base64String;
        }

        private bool CompareBytearrays(byte[] a, byte[] b) {
            bool flag;
            if (a.Length == b.Length) {
                int num = 0;
                byte[] numArray = a;
                int num1 = 0;
                while (num1 < numArray.Length) {
                    if (numArray[num1] == b[num]) {
                        num++;
                        num1++;
                    } else {
                        flag = false;
                        return flag;
                    }
                }
                flag = true;
            } else {
                flag = false;
            }
            return flag;
        }

        private RSACryptoServiceProvider DecodeEncryptedPrivateKeyInfo(byte[] encpkcs8, SecureString pass) {
            int num;
            int num1;
            RSACryptoServiceProvider rSACryptoServiceProvider;
            if (pass != null) {
                byte[] numArray = new byte[] { 6, 9, 42, 134, 72, 134, 247, 13, 1, 5, 13 };
                byte[] numArray1 = new byte[] { 6, 9, 42, 134, 72, 134, 247, 13, 1, 5, 12 };
                byte[] numArray2 = new byte[] { 6, 8, 42, 134, 72, 134, 247, 13, 3, 7 };
                byte[] numArray3 = new byte[10];

                bool flag = false;
                MemoryStream memoryStream = new MemoryStream(encpkcs8);

                BinaryReader binaryReader = new BinaryReader(memoryStream);
                byte num2 = 0;
                ushort num3 = 0;
                try {
                    try {
                        num3 = binaryReader.ReadUInt16();
                        if (num3 == 33072) {
                            binaryReader.ReadByte();
                        } else if (num3 != 33328) {
                            rSACryptoServiceProvider = null;
                            return rSACryptoServiceProvider;
                        } else {
                            binaryReader.ReadInt16();
                        }
                        num3 = binaryReader.ReadUInt16();
                        if (num3 == 33072) {
                            binaryReader.ReadByte();
                        } else if (num3 == 33328) {
                            binaryReader.ReadInt16();
                        }
                        if (this.CompareBytearrays(binaryReader.ReadBytes(11), numArray)) {
                            num3 = binaryReader.ReadUInt16();
                            if (num3 == 33072) {
                                binaryReader.ReadByte();
                            } else if (num3 == 33328) {
                                binaryReader.ReadInt16();
                            }
                            num3 = binaryReader.ReadUInt16();
                            if (num3 == 33072) {
                                binaryReader.ReadByte();
                            } else if (num3 == 33328) {
                                binaryReader.ReadInt16();
                            }
                            if (this.CompareBytearrays(binaryReader.ReadBytes(11), numArray1)) {
                                num3 = binaryReader.ReadUInt16();
                                if (num3 == 33072) {
                                    binaryReader.ReadByte();
                                } else if (num3 == 33328) {
                                    binaryReader.ReadInt16();
                                }
                                num2 = binaryReader.ReadByte();
                                if (num2 == 4) {
                                    byte[] numArray5 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                    num2 = binaryReader.ReadByte();
                                    if (num2 == 2) {
                                        int num4 = binaryReader.ReadByte();
                                        if (num4 == 1) {
                                            num1 = binaryReader.ReadByte();
                                        } else if (num4 != 2) {
                                            rSACryptoServiceProvider = null;
                                            return rSACryptoServiceProvider;
                                        } else {
                                            num1 = 256 * binaryReader.ReadByte() + binaryReader.ReadByte();
                                        }
                                        num3 = binaryReader.ReadUInt16();
                                        if (num3 == 33072) {
                                            binaryReader.ReadByte();
                                        } else if (num3 == 33328) {
                                            binaryReader.ReadInt16();
                                        }
                                        if (num3 == 4400) {
                                            numArray3 = binaryReader.ReadBytes(7);
                                        } else if (this.CompareBytearrays(binaryReader.ReadBytes(10), numArray2)) {
                                            flag = true;
                                        } else {
                                            this.mensajeErrorField = "No es una llave privada OIDdes-EDE3-CBC";
                                            rSACryptoServiceProvider = null;
                                            return rSACryptoServiceProvider;
                                        }
                                        num2 = binaryReader.ReadByte();
                                        if (num2 == 4) {
                                            byte[] numArray6 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                            num2 = binaryReader.ReadByte();
                                            if (num2 == 4) {
                                                num2 = binaryReader.ReadByte();
                                                if (num2 == 129) {
                                                    num = binaryReader.ReadByte();
                                                } else if (num2 != 130) {
                                                    num = num2;
                                                } else {
                                                    num = 256 * binaryReader.ReadByte() + binaryReader.ReadByte();
                                                }
                                                byte[] numArray7 = binaryReader.ReadBytes(num);
                                                byte[] numArray8 = this.DecryptPbdk2(numArray7, numArray5, numArray6, pass, num1, flag);
                                                if (numArray8 != null) {
                                                    rSACryptoServiceProvider = this.DecodePrivateKeyInfo(numArray8);
                                                } else {
                                                    this.mensajeErrorField = "Contraseña de llave privada incorrecta.";
                                                    this.codigoErrorField = 323;
                                                    rSACryptoServiceProvider = null;
                                                }
                                            } else {
                                                rSACryptoServiceProvider = null;
                                            }
                                        } else {
                                            rSACryptoServiceProvider = null;
                                        }
                                    } else {
                                        rSACryptoServiceProvider = null;
                                    }
                                } else {
                                    rSACryptoServiceProvider = null;
                                }
                            } else {
                                rSACryptoServiceProvider = null;
                            }
                        } else {
                            rSACryptoServiceProvider = null;
                        }
                    } catch (Exception exception) {
                        Console.WriteLine(exception.Message);
                        rSACryptoServiceProvider = null;
                    }
                } finally {
                    binaryReader.Close();
                }
            } else {
                rSACryptoServiceProvider = null;
            }
            return rSACryptoServiceProvider;
        }

        private RSACryptoServiceProvider DecodePrivateKeyInfo(byte[] pkcs8) {
            RSACryptoServiceProvider rSACryptoServiceProvider;
            byte[] numArray = new byte[] { 48, 13, 6, 9, 42, 134, 72, 134, 247, 13, 1, 1, 1, 5, 0 };

            MemoryStream memoryStream = new MemoryStream(pkcs8);
            int length = (int)memoryStream.Length;
            BinaryReader binaryReader = new BinaryReader(memoryStream);
            byte num = 0;
            ushort num1 = 0;
            try {
                try {
                    num1 = binaryReader.ReadUInt16();
                    if (num1 == 33072) {
                        binaryReader.ReadByte();
                    } else if (num1 != 33328) {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    } else {
                        binaryReader.ReadInt16();
                    }
                    num = binaryReader.ReadByte();
                    if (num == 2) {
                        num1 = binaryReader.ReadUInt16();
                        if (num1 != 1) {
                            rSACryptoServiceProvider = null;
                        } else if (this.CompareBytearrays(binaryReader.ReadBytes(15), numArray)) {
                            num = binaryReader.ReadByte();
                            if (num == 4) {
                                num = binaryReader.ReadByte();
                                if (num == 129) {
                                    binaryReader.ReadByte();
                                } else if (num == 130) {
                                    binaryReader.ReadUInt16();
                                }
                                byte[] numArray2 = binaryReader.ReadBytes((int)((long)length - memoryStream.Position));
                                rSACryptoServiceProvider = this.DecodeRsaPrivateKey(numArray2);
                            } else {
                                rSACryptoServiceProvider = null;
                            }
                        } else {
                            rSACryptoServiceProvider = null;
                        }
                    } else {
                        rSACryptoServiceProvider = null;
                    }
                } catch (Exception exception) {
                    Console.WriteLine(exception.Message);
                    rSACryptoServiceProvider = null;
                }
            } finally {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private RSACryptoServiceProvider DecodeRsaPrivateKey(byte[] privkey) {
            byte[] numArray;
            byte[] numArray1;
            byte[] numArray2;
            byte[] numArray3;
            byte[] numArray4;
            byte[] numArray5;
            int num;
            RSACryptoServiceProvider rSACryptoServiceProvider;
            this.bufferLlaveField = privkey;
            BinaryReader binaryReader = new BinaryReader(new MemoryStream(privkey));
            ushort num1 = 0;
            int integerSize = 0;
            try {
                try {
                    num1 = binaryReader.ReadUInt16();
                    if (num1 == 33072) {
                        binaryReader.ReadByte();
                    } else if (num1 != 33328) {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    } else {
                        binaryReader.ReadInt16();
                    }
                    num1 = binaryReader.ReadUInt16();
                    if (num1 != 258) {
                        rSACryptoServiceProvider = null;
                    } else if (binaryReader.ReadByte() == 0) {
                        integerSize = this.GetIntegerSize(binaryReader);
                        byte[] numArray6 = binaryReader.ReadBytes(integerSize);
                        integerSize = this.GetIntegerSize(binaryReader);
                        byte[] numArray7 = binaryReader.ReadBytes(integerSize);
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray1 = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray1 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray1, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray2 = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray2 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray2, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray3 = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray3 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray3, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray4 = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray4 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray4, num - integerSize);
                        }
                        integerSize = this.GetIntegerSize(binaryReader);
                        if (integerSize % 8 == 0) {
                            numArray5 = binaryReader.ReadBytes(integerSize);
                        } else {
                            num = (integerSize / 8 + 1) * 8;
                            numArray5 = new byte[num];
                            binaryReader.ReadBytes(integerSize).CopyTo(numArray5, num - integerSize);
                        }
                        RSACryptoServiceProvider rSACryptoServiceProvider1 = new RSACryptoServiceProvider();
                        RSAParameters rSAParameter = new RSAParameters();
                        this.moduloField = Encoding.UTF8.GetString(numArray6);
                        rSAParameter.Modulus = numArray6;
                        rSAParameter.Exponent = numArray7;
                        rSAParameter.D = numArray;
                        rSAParameter.P = numArray1;
                        rSAParameter.Q = numArray2;
                        rSAParameter.DP = numArray3;
                        rSAParameter.DQ = numArray4;
                        rSAParameter.InverseQ = numArray5;
                        rSACryptoServiceProvider1.ImportParameters(rSAParameter);
                        rSACryptoServiceProvider = rSACryptoServiceProvider1;
                    } else {
                        rSACryptoServiceProvider = null;
                    }
                } catch (Exception exception) {
                    this.mensajeErrorField = string.Concat("Error al desencriptar la llave. ", exception.Message);
                    this.codigoErrorField = 322;
                    rSACryptoServiceProvider = null;
                }
            } finally {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private RSACryptoServiceProvider DecodeX509PublicKey(byte[] x509key) {
            RSACryptoServiceProvider rSACryptoServiceProvider;
            byte[] numArray = new byte[] { 48, 13, 6, 9, 42, 134, 72, 134, 247, 13, 1, 1, 1, 5, 0 };

            BinaryReader binaryReader = new BinaryReader(new MemoryStream(x509key));
            ushort num = 0;
            try {
                try {
                    num = binaryReader.ReadUInt16();
                    if (num == 33072) {
                        binaryReader.ReadByte();
                    } else if (num != 33328) {
                        rSACryptoServiceProvider = null;
                        return rSACryptoServiceProvider;
                    } else {
                        binaryReader.ReadInt16();
                    }
                    if (this.CompareBytearrays(binaryReader.ReadBytes(15), numArray)) {
                        num = binaryReader.ReadUInt16();
                        if (num == 33027) {
                            binaryReader.ReadByte();
                        } else if (num != 33283) {
                            rSACryptoServiceProvider = null;
                            return rSACryptoServiceProvider;
                        } else {
                            binaryReader.ReadInt16();
                        }
                        if (binaryReader.ReadByte() == 0) {
                            num = binaryReader.ReadUInt16();
                            if (num == 33072) {
                                binaryReader.ReadByte();
                            } else if (num != 33328) {
                                rSACryptoServiceProvider = null;
                                return rSACryptoServiceProvider;
                            } else {
                                binaryReader.ReadInt16();
                            }
                            num = binaryReader.ReadUInt16();
                            byte num1 = 0;
                            byte num2 = 0;
                            if (num == 33026) {
                                num1 = binaryReader.ReadByte();
                            } else if (num != 33282) {
                                rSACryptoServiceProvider = null;
                                return rSACryptoServiceProvider;
                            } else {
                                num2 = binaryReader.ReadByte();
                                num1 = binaryReader.ReadByte();
                            }
                            byte[] numArray2 = new byte[] { num1, num2, 0, 0 };
                            int num3 = BitConverter.ToInt32(numArray2, 0);
                            byte num4 = binaryReader.ReadByte();
                            binaryReader.BaseStream.Seek((long)-1, SeekOrigin.Current);
                            if (num4 == 0) {
                                binaryReader.ReadByte();
                                num3--;
                            }
                            byte[] numArray3 = binaryReader.ReadBytes(num3);
                            if (binaryReader.ReadByte() == 2) {
                                byte[] numArray4 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                                this.ShowBytes("\nExponent", numArray4);
                                this.ShowBytes("\nModulus", numArray3);
                                RSACryptoServiceProvider rSACryptoServiceProvider1 = new RSACryptoServiceProvider();
                                RSAParameters rSAParameter = new RSAParameters() {
                                    Modulus = numArray3,
                                    Exponent = numArray4
                                };
                                rSACryptoServiceProvider1.ImportParameters(rSAParameter);
                                rSACryptoServiceProvider = rSACryptoServiceProvider1;
                            } else {
                                rSACryptoServiceProvider = null;
                            }
                        } else {
                            rSACryptoServiceProvider = null;
                        }
                    } else {
                        rSACryptoServiceProvider = null;
                    }
                } catch (Exception exception) {
                    Console.WriteLine(exception.Message);
                    rSACryptoServiceProvider = null;
                }
            } finally {
                binaryReader.Close();
            }
            return rSACryptoServiceProvider;
        }

        private byte[] DecryptPbdk2(byte[] edata, byte[] salt, byte[] IV, SecureString secpswd, int iterations, bool es3DES) {
            byte[] array;
            CryptoStream cryptoStream = null;
            IntPtr zero = IntPtr.Zero;
            byte[] numArray = new byte[secpswd.Length];
            zero = Marshal.SecureStringToGlobalAllocAnsi(secpswd);
            Marshal.Copy(zero, numArray, 0, numArray.Length);
            Marshal.ZeroFreeGlobalAllocAnsi(zero);
            try {
                Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(numArray, salt, iterations);
                MemoryStream memoryStream = new MemoryStream();
                if (!es3DES) {
                    DES bytes = DES.Create();
                    bytes.Key = rfc2898DeriveByte.GetBytes(8);
                    bytes.IV = IV;
                    cryptoStream = new CryptoStream(memoryStream, bytes.CreateDecryptor(), CryptoStreamMode.Write);
                } else {
                    TripleDES v = TripleDES.Create();
                    DES.Create();
                    v.Key = rfc2898DeriveByte.GetBytes(24);
                    v.IV = IV;
                    cryptoStream = new CryptoStream(memoryStream, v.CreateDecryptor(), CryptoStreamMode.Write);
                }
                cryptoStream.Write(edata, 0, edata.Length);
                cryptoStream.Flush();
                cryptoStream.Close();
                array = memoryStream.ToArray();
            } catch (Exception exception) {
                this.mensajeErrorField = string.Concat("Error al desencriptar la llave. ", exception.Message);
                this.codigoErrorField = 322;
                array = null;
            }
            return array;
        }

        private void ShowBytes(string info, byte[] data) {
            for (int i = 1; i <= data.Length; i++) {
                Console.Write("{0:X2}  ", data[i - 1]);
                if (i % 16 == 0) {
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n\n");
        }

        private SecureString ToSecureString(string source) {
            SecureString secureString;
            if (!string.IsNullOrEmpty(source)) {
                SecureString secureString1 = new SecureString();
                char[] charArray = source.ToCharArray();
                for (int i = 0; i < charArray.Length; i++) {
                    secureString1.AppendChar(charArray[i]);
                }
                secureString = secureString1;
            } else {
                secureString = null;
            }
            return secureString;
        }
        #endregion
    }
}