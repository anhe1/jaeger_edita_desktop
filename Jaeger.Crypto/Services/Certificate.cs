﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Jaeger.Crypto.Services {
    public class Certificate {
        #region declaraciones
        private string _MensajeError;
        private int _CodigoError;
        private static PrivateKey llaveField;
        private X509Certificate2 certificadoX509Field;
        private string tipoCertificadoField;
        private string _Version;
        private int longitudClaveField;
        private static string moduloField;
        private static string strXmlField;
        private bool _EsFiel;
        private string emisorField;
        private string asuntoField;
        private string validoDesdeField;
        private string validoHastaField;
        private string noSerieField;
        private string _RFC;
        private string _RazonSocial;
        private string autoridadEmisoraField;
        #endregion

        public Certificate() {
            this.Inicializar();
        }

        public Certificate(string pathFileCER) {
            this.Inicializar();
            this.Cargar(pathFileCER);
        }

        #region propiedades
        public string AutoridadEmisora {
            get {
                return this.autoridadEmisoraField;
            }
        }

        public string CadenaAsunto {
            get {
                return this.asuntoField;
            }
        }

        public string CadenaEmisor {
            get {
                return this.emisorField;
            }
        }

        public X509Certificate2 CertificadoOriginal {
            get {
                return this.certificadoX509Field;
            }
        }

        public int CodigoDeError {
            get {
                return this._CodigoError;
            }
        }

        public bool EsFiel {
            get {
                return this._EsFiel;
            }
        }

        private PrivateKey CryptoPrivateKey {
            get {
                return Certificate.llaveField;
            }
        }

        public int LongitudDeClavePublica {
            get {
                return this.longitudClaveField;
            }
        }

        public string MensajeDeError {
            get {
                return this._MensajeError;
            }
        }

        private string Modulus {
            get {
                return Certificate.moduloField;
            }
        }

        public string NoSerie {
            get {
                return this.noSerieField;
            }
        }

        public string RazonSocial {
            get {
                return this._RazonSocial;
            }
        }

        public string RFC {
            get {
                return this._RFC;
            }
        }

        public string TipoCertificado {
            get {
                return this.tipoCertificadoField;
            }
        }

        public string ValidoDesde {
            get {
                return this.validoDesdeField;
            }
        }

        public string ValidoHasta {
            get {
                return this.validoHastaField;
            }
        }

        public string Version {
            get {
                return this._Version;
            }
        }

        public DateTime? Fin {
            get {
                var d0 = DateTime.Parse(this.validoHastaField);
                if (d0 >= new DateTime(1900, 1, 1))
                    return d0;
                return null;
            }
        }

        public DateTime? Inicio {
            get {
                var d0 = DateTime.Parse(this.validoDesdeField);
                if (d0 >= new DateTime(1900, 1, 1))
                    return d0;
                return null;
            }
        }
        #endregion

        #region metodos publicos
        public void Cargar(string pathFileCER) {
            if (string.IsNullOrEmpty(pathFileCER)) {
                this._MensajeError = "La ruta del certificado no puede ser vacía.";
                this._CodigoError = 111;
            } else if (!File.Exists(pathFileCER)) {
                this._MensajeError = "Ruta de archivo de certificado inválida o no existe.";
                this._CodigoError = 211;
            } else {
                this.certificadoX509Field = this.GetFilecert(pathFileCER);
                if (this.certificadoX509Field != null) {
                    this.CargarPropiedades();
                } else {
                    this._MensajeError = "Archivo de certificado X509 no válido.";
                    this._CodigoError = 311;
                }
            }
        }

        public void Cargar(byte[] byteCertificado) {
            try {
                this.certificadoX509Field = new X509Certificate2(byteCertificado);
            } catch (Exception exception) {
                this._MensajeError = string.Concat("Buffer del certificado incorrecto. ", exception.Message);
                this._CodigoError = 312;
                return;
            }
            this.CargarPropiedades();
        }

        public void CargarB64(string certificadoB64) {
            if (!(certificadoB64 == "")) {
                try {
                    this.certificadoX509Field = new X509Certificate2(Convert.FromBase64String(certificadoB64));
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Certificado en formato base 64 no válido. ", exception.Message);
                    this._CodigoError = 313;
                    return;
                }
                this.CargarPropiedades();
            } else {
                this._MensajeError = "La cadena del certificado en base 64 no puede ser vacía.";
                this._CodigoError = 112;
            }
        }

        public void CargarLlavePrivada(PrivateKey claseLlavePrivada) {
            if (this.certificadoX509Field != null) {
                Certificate.llaveField = claseLlavePrivada;
                try {
                    this.certificadoX509Field.PrivateKey = claseLlavePrivada.LlavePrivadaRsa;
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error de correspondencia entre certificado y llave privada. ", exception.Message);
                    this._CodigoError = 314;
                    return;
                }
                if (Certificate.moduloField != Certificate.llaveField.Modulus) {
                    this._MensajeError = "Error de correspondencia entre certificado y llave privada.";
                    this._CodigoError = 314;
                }
            } else {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            }
        }

        public void SellarArchivoXml(string rutaXml) {
            if (this.certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            } else if (Certificate.llaveField == null) {
                this._MensajeError = "No ha cargado una llave privada al certificado.";
                this._CodigoError = 102;
            } else if (rutaXml == "") {
                this._MensajeError = "La ruta del XML no puede ser vacía.";
                this._CodigoError = 115;
            } else if (File.Exists(rutaXml)) {
                try {
                    XmlDocument xmlDocument = new XmlDocument() {
                        PreserveWhitespace = false
                    };
                    xmlDocument.Load(rutaXml);
                    SignedXml signedXml = new SignedXml(xmlDocument) {
                        SigningKey = this.certificadoX509Field.PrivateKey
                    };
                    Reference reference = new Reference() {
                        Uri = ""
                    };
                    reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                    signedXml.AddReference(reference);
                    KeyInfoX509Data keyInfoX509Datum = new KeyInfoX509Data(this.certificadoX509Field);
                    keyInfoX509Datum.AddIssuerSerial(this.certificadoX509Field.Issuer, this.certificadoX509Field.SerialNumber.ToString());
                    signedXml.KeyInfo.AddClause(keyInfoX509Datum);
                    signedXml.ComputeSignature();
                    XmlElement xml = signedXml.GetXml();
                    xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(xml, true));
                    if (xmlDocument.FirstChild is XmlDeclaration) {
                        xmlDocument.RemoveChild(xmlDocument.FirstChild);
                    }
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(rutaXml, new UTF8Encoding(false));
                    try {
                        xmlDocument.WriteTo(xmlTextWriter);
                    } finally {
                        if (xmlTextWriter != null) {
                            ((IDisposable)xmlTextWriter).Dispose();
                        }
                    }
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error al sellar la cadena XML. ", exception.Message);
                    this._CodigoError = 512;
                    return;
                }
            } else {
                this._MensajeError = "La ruta especificada no es válida o no existe.";
                this._CodigoError = 202;
            }
        }

        public string ExportarB64() {
            string base64String;
            if (this.certificadoX509Field != null) {
                base64String = Convert.ToBase64String(this.certificadoX509Field.RawData);
            } else {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
                base64String = null;
            }
            return base64String;
        }

        public void ExportarPem(string rutaArchivoPem) {
            if (rutaArchivoPem == "") {
                this._MensajeError = "La ruta del archivo PEM no puede ser vacía.";
                this._CodigoError = 0;
            } else if (this.certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            } else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPem))) {
                rutaArchivoPem = string.Concat(Path.GetDirectoryName(rutaArchivoPem), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPem));
                rutaArchivoPem = string.Concat(rutaArchivoPem, ".pem");
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN CERTIFICATE-----");
                    string base64String = Convert.ToBase64String(this.certificadoX509Field.Export(X509ContentType.Cert));
                    for (int i = 0; i < base64String.Length - 1; i += 64) {
                        if (i + 64 >= base64String.Length - 1) {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        } else {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END CERTIFICATE-----");
                    File.WriteAllText(rutaArchivoPem, stringBuilder.ToString(), new UTF8Encoding(false));
                    if (!File.Exists(rutaArchivoPem)) {
                        this._MensajeError = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this._CodigoError = 505;
                    }
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this._CodigoError = 506;
                }
            } else {
                this._MensajeError = "El directorio especificado no es válido o no existe.";
                this._CodigoError = 201;
            }
        }

        public string ExportarPemB64() {
            if (certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            } else {
                try {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine("-----BEGIN CERTIFICATE-----");
                    string base64String = Convert.ToBase64String(this.certificadoX509Field.Export(X509ContentType.Cert));
                    for (int i = 0; i < base64String.Length - 1; i += 64) {
                        if (i + 64 >= base64String.Length - 1) {
                            stringBuilder.AppendLine(base64String.Substring(i, base64String.Length - i));
                        } else {
                            stringBuilder.AppendLine(base64String.Substring(i, 64));
                        }
                    }
                    stringBuilder.AppendLine("-----END CERTIFICATE-----");
                    this._MensajeError = string.Empty;
                    this._CodigoError = 0;
                    return stringBuilder.ToString();
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error interno al exportar certificado a PEM: ", exception.Message);
                    this._CodigoError = 506;
                }
            }
            return string.Empty;
        }

        public void ExportarPfx(string rutaArchivoPfx, string nuevaContrasenia) {
            byte[] numArray;
            if (rutaArchivoPfx == "") {
                this._MensajeError = "La ruta del archivo PFX no puede ser vacía.";
                this._CodigoError = 113;
            } else if (this.certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            } else if (Certificate.llaveField == null) {
                this._MensajeError = "No ha cargado una llave privada al certificado.";
                this._CodigoError = 102;
            } else if (Directory.Exists(Path.GetDirectoryName(rutaArchivoPfx))) {
                rutaArchivoPfx = string.Concat(Path.GetDirectoryName(rutaArchivoPfx), "\\", Path.GetFileNameWithoutExtension(rutaArchivoPfx));
                rutaArchivoPfx = string.Concat(rutaArchivoPfx, ".pfx");
                try {
                    numArray = (!(nuevaContrasenia != "") ? this.certificadoX509Field.Export(X509ContentType.Pfx) : this.certificadoX509Field.Export(X509ContentType.Pfx, this.ToSecureString(nuevaContrasenia)));
                    File.WriteAllBytes(rutaArchivoPfx, numArray);
                    if (!File.Exists(rutaArchivoPfx)) {
                        this._MensajeError = "Error al crear el archivo. Verifique permisos de escritura en el directorio.";
                        this._CodigoError = 505;
                    }
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error interno al exportar certificado a PFX: ", exception.Message);
                    this._CodigoError = 506;
                }
            } else {
                this._MensajeError = "El directorio especificado no es válido o no existe.";
                this._CodigoError = 201;
            }
        }

        public string ExportarXml() {
            string str;
            if (this.certificadoX509Field != null) {
                str = Certificate.strXmlField;
            } else {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
                str = null;
            }
            return str;
        }

        public string SellarCadenaSha1(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA1");
        }

        public string SellarCadenaSha256(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA256");
        }

        public string SellarCadenaSha512(string cadenaOriginal) {
            return this.SellarCadenaCodificacion(cadenaOriginal, "SHA512");
        }

        public string SellarCadenaXml(string cadenaXml) {
            string innerXml;
            if (this.certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
                innerXml = null;
            } else if (!(cadenaXml.Trim() == "")) {
                try {
                    XmlDocument xmlDocument = new XmlDocument() {
                        PreserveWhitespace = false
                    };
                    xmlDocument.LoadXml(cadenaXml);
                    SignedXml signedXml = new SignedXml(xmlDocument) {
                        SigningKey = this.certificadoX509Field.PrivateKey
                    };
                    Reference reference = new Reference() {
                        Uri = ""
                    };
                    reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
                    signedXml.AddReference(reference);
                    KeyInfoX509Data keyInfoX509Datum = new KeyInfoX509Data(this.certificadoX509Field, X509IncludeOption.EndCertOnly);
                    keyInfoX509Datum.AddIssuerSerial(this.certificadoX509Field.Issuer, this.certificadoX509Field.SerialNumber.ToString());
                    signedXml.KeyInfo.AddClause(keyInfoX509Datum);
                    signedXml.ComputeSignature();
                    XmlElement xml = signedXml.GetXml();
                    xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(xml, true));
                    if (xmlDocument.FirstChild is XmlDeclaration) {
                        xmlDocument.RemoveChild(xmlDocument.FirstChild);
                    }
                    innerXml = xmlDocument.InnerXml;
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error al sellar la cadena XML. ", exception.Message);
                    this._CodigoError = 512;
                    innerXml = null;
                }
            } else {
                this._MensajeError = "Cadena de XML vacía.";
                this._CodigoError = 114;
                innerXml = "";
            }
            return innerXml;
        }
        #endregion

        ~Certificate() {
            this.certificadoX509Field = null;
            Certificate.llaveField = null;
            GC.Collect();
        }

        #region metodos privados
        private void CargarPropiedades() {
            if (this.certificadoX509Field != null) {
                try {
                    Certificate.strXmlField = this.CertToXml(this.certificadoX509Field);
                    this._Version = this.certificadoX509Field.Version.ToString();
                    this.asuntoField = this.certificadoX509Field.Subject;
                    this.emisorField = this.certificadoX509Field.Issuer;
                    DateTime notAfter = this.certificadoX509Field.NotAfter;
                    this.validoHastaField = notAfter.ToString("s");
                    notAfter = this.certificadoX509Field.NotBefore;
                    this.validoDesdeField = notAfter.ToString("s");
                    Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
                    int num = this.certificadoX509Field.Subject.IndexOf("OID.2.5.4.45=");
                    if (num < 0) {
                        num = 0;
                    }
                    Match match = regex.Match(this.certificadoX509Field.Subject, num);
                    this._RFC = match.Value.ToString();
                    this._EsFiel = this.certificadoX509Field.Subject.IndexOf("OU=") < 0;
                    if (!this._EsFiel) {
                        this.tipoCertificadoField = "SELLO";
                    } else {
                        this.tipoCertificadoField = "FIEL";
                    }
                    this.longitudClaveField = this.certificadoX509Field.PublicKey.Key.KeySize;
                    this.noSerieField = this.SerialAsciItoHex(this.certificadoX509Field.SerialNumber);
                    int num1 = this.certificadoX509Field.Subject.IndexOf(" OID.2.5.4.41=") + 14;
                    if (num1 == 13) {
                        num1 = this.certificadoX509Field.Subject.IndexOf("CN=") + 3;
                    }
                    int length = this.certificadoX509Field.Subject.IndexOf(", ", num1);
                    if (length == -1) {
                        length = this.certificadoX509Field.Subject.Length;
                    }
                    this._RazonSocial = this.certificadoX509Field.Subject.Substring(num1, length - num1).Trim();
                    num1 = this.certificadoX509Field.Issuer.IndexOf(" O=") + 3;
                    if (num1 == 2) {
                        num1 = this.certificadoX509Field.Issuer.IndexOf("CN=") + 3;
                    }
                    length = this.certificadoX509Field.Issuer.IndexOf(", ", num1);
                    if (length == -1) {
                        length = this.certificadoX509Field.Issuer.Length;
                    }
                    this.autoridadEmisoraField = this.certificadoX509Field.Issuer.Substring(num1, length - num1).Trim();
                } catch (Exception exception) {
                    this._MensajeError = string.Concat("Error al leer las propiedades del certificado. ", exception.Message);
                    this._CodigoError = 514;
                }
            } else {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
            }
        }

        private string CertToXml(X509Certificate2 cert) {
            string xmlString;
            BinaryReader binaryReader = new BinaryReader(new MemoryStream(cert.GetPublicKey()));
            ushort num = 0;
            try {
                try {
                    num = binaryReader.ReadUInt16();
                    if (num == 33072) {
                        binaryReader.ReadByte();
                    } else if (num != 33328) {
                        xmlString = null;
                        return xmlString;
                    } else {
                        binaryReader.ReadInt16();
                    }
                    num = binaryReader.ReadUInt16();
                    byte num1 = 0;
                    byte num2 = 0;
                    if (num == 33026) {
                        num1 = binaryReader.ReadByte();
                    } else if (num != 33282) {
                        xmlString = null;
                        return xmlString;
                    } else {
                        num2 = binaryReader.ReadByte();
                        num1 = binaryReader.ReadByte();
                    }
                    byte[] numArray = new byte[] { num1, num2, 0, 0 };
                    int num3 = BitConverter.ToInt32(numArray, 0);
                    if (binaryReader.PeekChar() == 0) {
                        binaryReader.ReadByte();
                        num3--;
                    }
                    byte[] numArray1 = binaryReader.ReadBytes(num3);
                    if (binaryReader.ReadByte() == 2) {
                        byte[] numArray2 = binaryReader.ReadBytes((int)binaryReader.ReadByte());
                        if (binaryReader.PeekChar() == -1) {
                            RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider();
                            RSAParameters rSAParameter = new RSAParameters() {
                                Modulus = numArray1
                            };
                            Certificate.moduloField = Encoding.UTF8.GetString(numArray1);
                            rSAParameter.Exponent = numArray2;
                            rSACryptoServiceProvider.ImportParameters(rSAParameter);
                            xmlString = rSACryptoServiceProvider.ToXmlString(false);
                        } else {
                            xmlString = null;
                        }
                    } else {
                        xmlString = null;
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    xmlString = null;
                }
            } finally {
                binaryReader.Close();
            }
            return xmlString;
        }

        private X509Certificate2 GetFilecert(string certfileName) {
            X509Certificate2 x509Certificate2;
            X509Certificate2 x509Certificate21 = new X509Certificate2();
            try {
                x509Certificate21.Import(certfileName);
                x509Certificate2 = x509Certificate21;
            } catch (CryptographicException cryptographicException1) {
                Console.WriteLine(cryptographicException1.Message);
                StreamReader streamReader = File.OpenText(certfileName);
                string end = streamReader.ReadToEnd();
                streamReader.Close();
                StringBuilder stringBuilder = new StringBuilder(end);
                stringBuilder.Replace("-----BEGIN CERTIFICATE-----", "");
                stringBuilder.Replace("-----END CERTIFICATE-----", "");
                try {
                    x509Certificate21 = new X509Certificate2(Convert.FromBase64String(stringBuilder.ToString()));
                    x509Certificate2 = x509Certificate21;
                } catch (FormatException formatException) {
                    Console.WriteLine(formatException.Message);
                    x509Certificate2 = null;
                } catch (CryptographicException cryptographicException) {
                    Console.WriteLine(cryptographicException.Message);
                    x509Certificate2 = null;
                }
            }
            return x509Certificate2;
        }

        private void Inicializar() {
            this._MensajeError = "";
            this._CodigoError = 0;
            this.tipoCertificadoField = "";
            Certificate.moduloField = "";
            this.validoDesdeField = "";
            this._Version = "";
            this.emisorField = "";
            this.asuntoField = "";
            this.validoDesdeField = "";
            this.validoHastaField = "";
            this.noSerieField = "";
            this._RFC = "";
            this._RazonSocial = "";
            this.autoridadEmisoraField = "";
            this._EsFiel = false;
        }

        private string SellarCadenaCodificacion(string cadenaOriginal, string codSha) {
            byte[] numArray;
            string base64String;
            if (this.certificadoX509Field == null) {
                this._MensajeError = "No ha cargado un certificado.";
                this._CodigoError = 101;
                base64String = null;
            } else if (this.certificadoX509Field.PrivateKey != null) {
                RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)this.certificadoX509Field.PrivateKey;
                try {
                    byte[] bytes = (new UTF8Encoding()).GetBytes(cadenaOriginal);
                    try {
                        try {
                            numArray = privateKey.SignData(bytes, CryptoConfig.MapNameToOID(codSha));
                        } catch (CryptographicException cryptographicException) {
                            this._MensajeError = string.Concat("Error interno al sellar la cadena. ", cryptographicException.Message);
                            this._CodigoError = 513;
                            base64String = null;
                            return base64String;
                        }
                    } finally {
                        privateKey.PersistKeyInCsp = false;
                    }
                } finally {
                    if (privateKey != null) {
                        ((IDisposable)privateKey).Dispose();
                    }
                }
                base64String = Convert.ToBase64String(numArray);
            } else {
                this._MensajeError = "No ha cargado una llave privada al certificado.";
                this._CodigoError = 102;
                base64String = null;
            }
            return base64String;
        }

        private string SerialAsciItoHex(string serialAscci) {
            string str = "";
            string str1 = "";
            for (int i = 0; i <= serialAscci.Length - 1; i += 2) {
                str1 = serialAscci.Substring(i, 2);
                str = string.Concat(str, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
            }
            return str;
        }

        private SecureString ToSecureString(string source) {
            SecureString secureString;
            if (!string.IsNullOrEmpty(source)) {
                SecureString secureString1 = new SecureString();
                char[] charArray = source.ToCharArray();
                for (int i = 0; i < charArray.Length; i++) {
                    secureString1.AppendChar(charArray[i]);
                }
                secureString = secureString1;
            } else {
                secureString = null;
            }
            return secureString;
        }
        #endregion
    }
}