﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace Jaeger.Crypto.Services {
    public class CertificateInfo {
        #region declaraciones
        private DateTime? _dtInicio;
        private DateTime? _dtFin;
        private string _noSerie;
        private string _userName;
        private FileInfo _fiCer;
        private string _cerB64;
        private string _rfc;
        private string _tipoCertificado;
        private X509Certificate x509Certificate = new X509Certificate();
        private string _message;
        private bool _valid = false;
        #endregion

        #region propiedades
        public FileInfo Cer {
            set {
                this._fiCer = value;
            }
        }

        public string CerB64 {
            get {
                return this._cerB64;
            }
        }

        public DateTime? Fin {
            get { if (this._dtFin >= new DateTime(1900, 1, 1))
                    return this._dtFin;
                return null;
            }
        }

        public DateTime? Inicio {
            get {if (this._dtInicio >= new DateTime(1900, 1, 1))
                    return this._dtInicio;
                return null;
            }
        }

        public string NoSerie {
            get {
                return this._noSerie;
            }
        }

        public string UserName {
            get {
                return this._userName;
            }
        }

        public string RFC {
            get { return this._rfc; }
        }

        public string TipoCertificado {
            get { return this._tipoCertificado; }
        }

        public string Message {
            get { return this._message; }
        }

        public bool Valid {
            get { return this._valid; }
        }
        #endregion

        public CertificateInfo(FileInfo fiCer) {
            this._fiCer = fiCer;
        }

        public CertificateInfo(string pathCert) {
            if (File.Exists(pathCert)) {
                try {
                    this._fiCer = new FileInfo(pathCert);
                    this.CargarCertificado();
                    this._valid = true;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                    this._message = ex.Message;
                    this._valid = false;
                }
            }
        }

        public string obtenCertificado64(string pathKey) {
            return Convert.ToBase64String(File.ReadAllBytes(pathKey));
        }

        private bool CargarCertificado() {
            try {
                x509Certificate = X509Certificate.CreateFromCertFile(this._fiCer.FullName);
                if (this.x509Certificate != null) {
                    this._cerB64 = Convert.ToBase64String(x509Certificate.GetRawCertData());
                    this._noSerie = this.ObtenerNoCertificado();
                    this._rfc = this.ObtenerRFC();
                    this.ObtenerFechas();
                    this.ObtenerTipoCertificado();
                    this._userName = this.ObtenerRazonSocial();
                }
                return true;
            } catch (Exception ex2) {
                Console.WriteLine(string.Concat("Ocurrió un error al leer el certificado.\r\nCertificado no válido. ", ex2.Message));
                //MessageBox.Show(string.Concat("Ocurrió un error al leer el certificado.\r\nCertificado no válido. ", ex2.Message), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return false;
        }

        private bool ObtenerFechas() {
            if (this.x509Certificate != null) {
                try {
                    this._dtInicio = DateTime.Parse(x509Certificate.GetEffectiveDateString());
                    this._dtFin = DateTime.Parse(x509Certificate.GetExpirationDateString());
                    return true;
                } catch (Exception exception) {
                    Console.WriteLine(exception.Message);
                }
            }
            return false;
        }

        private string ObtenerRFC() {
            if (this.x509Certificate != null) {
                try {
                    Regex regex = new Regex("[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9A]");
                    Match match = regex.Match(x509Certificate.Subject, x509Certificate.Subject.IndexOf("OID.2.5.4.45="));
                    return match.Value.ToString();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return string.Empty;
        }

        private bool ObtenerTipoCertificado() {
            if (this.x509Certificate != null) {
                if (x509Certificate.Subject.IndexOf("OU=") >= 0) {
                    this._tipoCertificado = "SELLO";
                } else {
                    this._tipoCertificado = "FIEL";
                }
                return true;
            }
            return false;
        }

        private string ObtenerNoCertificado() {
            if (this.x509Certificate != null) {
                return this.SerialAsciItoHex(x509Certificate.GetSerialNumberString());
            }
            return string.Empty;
        }

        private string ObtenerRazonSocial() {
            if (this.x509Certificate != null) {
                try {
                    int num1 = this.x509Certificate.Subject.IndexOf(" OID.2.5.4.41=") + 14;
                    if (num1 == 13) {
                        num1 = this.x509Certificate.Subject.IndexOf("CN=") + 3;
                    }
                    int length = this.x509Certificate.Subject.IndexOf(", ", num1);
                    if (length == -1) {
                        length = this.x509Certificate.Subject.Length;
                    }
                    return this.x509Certificate.Subject.Substring(num1, length - num1).Trim();
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            var x509Certificate2 = new X509Certificate2(this.x509Certificate);
            return x509Certificate2.IssuerName.Name;
        }

        private string SerialAsciItoHex(string serialAscci) {
            string str = "";
            string str1 = "";
            for (int i = 0; i <= serialAscci.Length - 1; i += 2) {
                str1 = serialAscci.Substring(i, 2);
                str = string.Concat(str, char.ConvertFromUtf32(Convert.ToInt32(str1, 16)));
            }
            return str;
        }
    }
}
