﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Jaeger.Crypto.Contracts;

namespace Jaeger.Crypto.Services {
    public class CertificateExport : ICertificateExport {
        public byte[] CerKey2Pfx(string cerB64, string keyB64, string keyPass) {
            try {
                byte[] berPkcs12File = (byte[])null;
                X509Certificate2 x509Cert = new X509Certificate2(Convert.FromBase64String(cerB64));
                AsymmetricKeyParameter key = PrivateKeyFactory.DecryptKey(keyPass.ToCharArray(), Convert.FromBase64String(keyB64));
                X509CertificateEntry[] chain = new X509CertificateEntry[1] {
                    new X509CertificateEntry(new Org.BouncyCastle.X509.X509Certificate(DotNetUtilities.FromX509Certificate((X509Certificate) x509Cert).CertificateStructure))
                };
                Pkcs12Store pkcs12Store = new Pkcs12Store();
                pkcs12Store.SetKeyEntry("", new AsymmetricKeyEntry(key), chain);
                using (MemoryStream memoryStream = new MemoryStream()) {
                    pkcs12Store.Save((Stream)memoryStream, keyPass.ToCharArray(), new SecureRandom());
                    berPkcs12File = memoryStream.ToArray();
                }
                return Pkcs12Utilities.ConvertToDefiniteLength(berPkcs12File, keyPass.ToCharArray());
            } catch (Exception ex) {
                Console.WriteLine((object)ex);
                return (byte[])null;
            }
        }
    }
}
