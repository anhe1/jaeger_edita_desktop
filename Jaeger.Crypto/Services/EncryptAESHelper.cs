﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Jaeger.Crypto.Services {
    public class EncryptAESHelp {
        private static string PrivateKey;

        private static byte[] _salt;

        static EncryptAESHelp() {
            EncryptAESHelp.PrivateKey = "5QDEy9vb+W1usYavrrpS1vlxSgBDDXJr7yguhoycd5o=";
            EncryptAESHelp._salt = Encoding.ASCII.GetBytes("123456789abcdefghijklmn");
        }

        public EncryptAESHelp() { }

        public static string DecryptKey(string inputText) {
            string end = "";
            if (!string.IsNullOrWhiteSpace(inputText)) {
                RijndaelManaged rijndaelManaged = null;
                try {
                    Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(EncryptAESHelp.PrivateKey, EncryptAESHelp._salt);
                    using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(inputText))) {
                        rijndaelManaged = new RijndaelManaged() {
                            Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8),
                            IV = EncryptAESHelp.ReadByteArray(memoryStream)
                        };
                        ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read)) {
                            using (StreamReader streamReader = new StreamReader(cryptoStream)) {
                                end = streamReader.ReadToEnd();
                            }
                        }
                    }
                } finally {
                    if (rijndaelManaged != null) {
                        rijndaelManaged.Clear();
                    }
                }
            }
            return end;
        }

        public static string EncryptKey(string cadena) {
            string base64String = "";
            if (!string.IsNullOrWhiteSpace(cadena)) {
                RijndaelManaged rijndaelManaged = null;
                try {
                    Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(EncryptAESHelp.PrivateKey, EncryptAESHelp._salt);
                    rijndaelManaged = new RijndaelManaged() {
                        Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8)
                    };
                    ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(rijndaelManaged.Key, rijndaelManaged.IV);
                    using (MemoryStream memoryStream = new MemoryStream()) {
                        memoryStream.Write(BitConverter.GetBytes((int)rijndaelManaged.IV.Length), 0, 4);
                        memoryStream.Write(rijndaelManaged.IV, 0, (int)rijndaelManaged.IV.Length);
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write)) {
                            using (StreamWriter streamWriter = new StreamWriter(cryptoStream)) {
                                streamWriter.Write(cadena);
                            }
                        }
                        base64String = Convert.ToBase64String(memoryStream.ToArray());
                    }
                } finally {
                    if (rijndaelManaged != null) {
                        rijndaelManaged.Clear();
                    }
                }
            }
            return base64String;
        }

        private static byte[] ReadByteArray(Stream streamStr) {
            byte[] numArray = new byte[4];
            if (streamStr.Read(numArray, 0, (int)numArray.Length) != (int)numArray.Length) {
                throw new SystemException();
            }
            byte[] numArray1 = new byte[BitConverter.ToInt32(numArray, 0)];
            if (streamStr.Read(numArray1, 0, (int)numArray1.Length) != (int)numArray1.Length) {
                throw new SystemException();
            }
            return numArray1;
        }
    }
}
