﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Crypto.Services {
    public static class Extensions {
        public static string ToHexString(this byte[] data) {
            return Convert.ToBase64String(data, 0, data.Length);
        }
    }
}
