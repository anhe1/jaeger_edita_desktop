﻿namespace Jaeger.Crypto.Contracts {
    public interface ICertificateExport {
        byte[] CerKey2Pfx(string cerB64, string keyB64, string keyPass);
    }
}
