 Attribute VBA_ModuleType=VBAModule
Sub Módulo1
 Dim archivosalida As String
 
 Sub Botón1_AlHacerClic()
 '
 ' Botón1_AlHacerClic Macro
 ' Macro grabada el 22/09/2004 por GFSS
 '
 ' Acceso directo: CTRL+l
 '
     Dim Resultado As String
     MsgBox GeneraL() & Chr(10) & Chr(10) & "Archivo : " & Replace(Application.ActiveWorkbook.FullName, ".xls", ".txt", 1, , vbTextCompare)
     
 
 End Sub
 
 Function QuitaRuta(Archivo As String) As String
 On Error GoTo err_QuitaRuta
 
     Dim UltDiag As Long
     Dim UltDot As Long
     Dim Tam As Long
     UltDiag = InStrRev(Archivo, "\")
     Tam = Len(Archivo)
     If UltDiag > 0 Then
         QuitaRuta = Right(Archivo, Tam - UltDiag)
     Else
         QuitaRuta = Archivo
     End If
     UltDot = InStrRev(QuitaRuta, ".")
     If UltDot > 0 Then
         QuitaRuta = Left(QuitaRuta, UltDot - 1)
     End If
 
 sal_QuitaRuta:
     Exit Function
     
 err_QuitaRuta:
     QuitaRuta = "Error: " & Err.Description
     Resume sal_QuitaRuta
     
 
 End Function
 Function Fll(Texto As String, Tamaño As Long) As String
 On Error GoTo err_Fll
     Dim TextL As String
     TextL = Left(Texto, Tamaño)
     TextL = Replace(TextL, "(", " ")
     TextL = Replace(TextL, ")", " ")
     TextL = Replace(TextL, ".", " ")
     TextL = Replace(TextL, ",", " ")
     TextL = Replace(TextL, "°", " ")
     TextL = Replace(TextL, "'", " ")
     TextL = Replace(TextL, "!", " ")
     TextL = Replace(TextL, "#", " ")
     TextL = Replace(TextL, "%", " ")
     TextL = Replace(TextL, "=", " ")
     TextL = Replace(TextL, "?", " ")
     TextL = Replace(TextL, "¡", " ")
     TextL = Replace(TextL, "¿", " ")
     TextL = Replace(TextL, "*", " ")
     TextL = Replace(TextL, "{", " ")
     TextL = Replace(TextL, "}", " ")
     TextL = Replace(TextL, "[", " ")
     TextL = Replace(TextL, "]", " ")
     TextL = Replace(TextL, ">", " ")
     TextL = Replace(TextL, "<", " ")
     TextL = Replace(TextL, ";", " ")
     TextL = Replace(TextL, ":", " ")
     TextL = Replace(TextL, "_", " ")
     TextL = Replace(TextL, "-", " ")
     TextL = Replace(TextL, "+", " ")
     TextL = Replace(TextL, "-", " ")
     TextL = Replace(TextL, "&", " ")
     TextL = Replace(TextL, "|", " ")
     TextL = Replace(TextL, "/", " ")
     TextL = Replace(TextL, "á", "A")
     TextL = Replace(TextL, "é", "E")
     TextL = Replace(TextL, "í", "I")
     TextL = Replace(TextL, "ó", "O")
     TextL = Replace(TextL, "ú", "U")
     TextL = Replace(TextL, "Á", "A")
     TextL = Replace(TextL, "É", "E")
     TextL = Replace(TextL, "Í", "I")
     TextL = Replace(TextL, "Ó", "O")
     TextL = Replace(TextL, "Ú", "U")
     TextL = Replace(TextL, "ü", "U")
     TextL = Replace(TextL, "ö", "O")
     TextL = Replace(TextL, "Á", "A")
     TextL = Replace(TextL, "É", "E")
     TextL = Replace(TextL, "Í", "I")
     TextL = Replace(TextL, "Ó", "O")
     TextL = Replace(TextL, "Ú", "U")
     TextL = Replace(TextL, "Ñ", "N")
     TextL = UCase(TextL)
     For i = 33 To 47
         TextL = Replace(TextL, Chr(i), " ")
     Next i
     For i = 123 To 255
         TextL = Replace(TextL, Chr(i), " ")
     Next i
     Fll = NZ(TextL, " ") & Space(Tamaño - Len(NZ(TextL, " ")))
 sal_Fll:
     Exit Function
 
 err_Fll:
     Fll = Space(Tamaño)
     Resume sal_Fll
 
 End Function
 Function NZ(Dato As Variant, Def As Variant) As Variant
     On Error GoTo err_NZ
     
     If IsNull(Dato) Then
         NZ = Def
     Else
         NZ = Dato
     End If
 sal_NZ:
     Exit Function
 
 err_NZ:
     NZ = Def
     Exit Function
     
 End Function
 
 
 Function GeneraL() As String
 On Error GoTo err_GeneraL
     Dim Registro As String
     Dim Cont As Long
     Dim RegEnt As String
     Dim SumaImporte As Double
     Dim ArchivoSal As String
     Dim Continua As Boolean
     Dim Fila As Long
     Dim FechaIng As Date
     Dim FechaIngS As String
     Dim RFC As String
     Dim importe As Double
     Dim total As Double
     Dim cuenta As String
     Dim fechaplica As Date
     Dim mes As String
     Dim dia As String
     Dim ano As String
             
     cuenta = Fll(NZ(Range("H" & 3), " "), 11)
     fechaplica = Fll(NZ(Range("H" & 4), " "), 10)
     mes = Format(Month(fechaplica), "00")
     dia = Format(Day(fechaplica), "00")
     ano = Format(Year(fechaplica), "0000")
     
     
     
     'ArchivoSal = Replace(Application.ActiveWorkbook.FullName, ".xls", ".txt", 1, , vbTextCompare)
     ArchivoSal = Application.GetSaveAsFilename(InitialFileName:="LOE_Supernomina_conceptos.txt", fileFilter:="Text Files (*.txt), *.txt")
       archivosalida = ArchivoSal
 If ArchivoSal = "Falso" Then
 GoTo sal_GeneraL
 End If
     
     Open ArchivoSal For Output As 1
     Print #1, "100001E" & Format(Month(Date), "00") & Format(Day(Date), "00") & Format(Year(Date), "0000") & cuenta & "     " & mes & dia & ano
     Continua = True
     Fila = 8
     Cont = 2
     SumaImporte = 0
     Do While Continua
         If Len(Range("G" & Fila)) > 0 Then
             Registro = "2" & Format(Cont, "00000")
             Registro = Registro & Fll(NZ(Range("C" & Fila), " "), 7) 'NUMERO DE EMLEADO
             Registro = Registro & Fll(NZ(Range("D" & Fila), " "), 30) 'APELLIDO PATERNO DEL EMPLEADO
             Registro = Registro & Fll(NZ(Range("E" & Fila), " "), 20) 'APELLIDO MATERNO DEL EMPLEADO
             Registro = Registro & Fll(NZ(Range("F" & Fila), " "), 30) 'NOMBRE DEL EMPLEADO
             Registro = Registro & Fll(NZ(Range("G" & Fila), " "), 16) 'NUMERO DE CUENTA
             Registro = Registro & Format(Fll(Round(NZ(Range("H" & Fila), 0), 2) * 100, 18), "000000000000000000") 'INGRESO MENSUAL}
           If Len(Range("I" & Fila)) > 0 Then
             If Range("I" & Fila) = "01 PAGO DE NOMINA" Then
              Registro = Registro & Fll(NZ("01", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "02 PAGO DE VACACIONES" Then
              Registro = Registro & Fll(NZ("02", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "03 PAGO DE GRATIFICACIONES" Then
              Registro = Registro & Fll(NZ("03", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "04 PAGO DE COMISIONES" Then
              Registro = Registro & Fll(NZ("04", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "05 PAGO DE BECA" Then
              Registro = Registro & Fll(NZ("05", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "06 PAGO DE PENSION" Then
              Registro = Registro & Fll(NZ("06", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "07 PAGO POR SUBSIDIOS" Then
              Registro = Registro & Fll(NZ("07", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "08 OTROS PAGOS POR TRANSFERENCIA" Then
              Registro = Registro & Fll(NZ("08", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "09 PAGO DE HONORARIOS" Then
              Registro = Registro & Fll(NZ("09", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "10 PAGO DE PRESTAMO" Then
              Registro = Registro & Fll(NZ("10", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "11 PAGO DE VIATICOS" Then
              Registro = Registro & Fll(NZ("11", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "12 ANTICIPO DE VIATICOS" Then
              Registro = Registro & Fll(NZ("12", " "), 2) 'ID CONCEPTO
             End If
             If Range("I" & Fila) = "13 PAGO DE FONDO DE AHORRO" Then
              Registro = Registro & Fll(NZ("13", " "), 2) 'ID CONCEPTO
             End If
           Else
             Registro = Registro & Fll(NZ("01", " "), 2) 'ID CONCEPTO
           End If
             
             importe = Format(Fll(Round(NZ(Range("H" & Fila), 0), 2) * 100, 18))
             total = total + importe
                      
             
             Print #1, UCase(Registro)
             Cont = Cont + 1
             Fila = Fila + 1
         Else
             Continua = False
         End If
     Loop
     Print #1, "3" & Format(Cont, "00000") & Format(Cont - 2, "00000") & Format(total, "000000000000000000")
     GeneraL = "Registros procesados " & Cont - 2
 
 sal_GeneraL:
     'Screen.MousePointer = 0
     Close #1
     Close #2
     Exit Function
     
 err_GeneraL:
     GeneraL = Err.Description
     Resume sal_GeneraL
     Resume Next
     
     
 End Function
 
 
 Function CHKFecha(Fecha As String) As String
 Fecha = RTrim(Replace(Fecha, "o", "0"))
 Fecha = LTrim(Fecha)
 Fecha = Right(Fecha, 8)
 CHKFecha = Fecha
 If Len(Fecha) = 7 Or Mid(Fecha, 8, 1) = " " Then
     If Val(Mid(Fecha, 2, 2)) > 12 And Val(Mid(Fecha, 2, 2)) < 32 Then
         CHKFecha = Mid(Fecha, 2, 2) & Format(Left(Fecha, 1), "00") & Right(Fecha, 4)
     Else
     End If
 Else
     If Len(Fecha) = 8 Then
         If Val(Left(Fecha, 2)) > 0 And Val(Left(Fecha, 2)) < 13 Then
             If Val(Mid(Fecha, 3, 2)) > 12 And Val(Mid(Fecha, 3, 2)) < 32 Then
                 CHKFecha = Mid(Fecha, 3, 2) & Left(Fecha, 2) & Right(Fecha, 4)
             Else
             End If
         Else
             If (Val(Left(Fecha, 2)) = 0 Or Val(Left(Fecha, 2)) > 31) And Val(Mid(Fecha, 3, 2)) > 0 And Val(Mid(Fecha, 3, 2)) < 13 And Val(Right(Fecha, 4)) > 1900 Then
                 CHKFecha = "01" & Mid(Fecha, 3, 2) & Right(Fecha, 4)
             Else
             End If
         End If
     Else
         If Len(Fecha) = 6 Then
             If Val(Right(CHKFecha, 2)) > 50 Then
                 CHKFecha = Left(CHKFecha, 2) & Mid(CHKFecha, 3, 2) & "19" & Right(CHKFecha, 2)
             Else
                 CHKFecha = Left(CHKFecha, 2) & Mid(CHKFecha, 3, 2) & "20" & Right(CHKFecha, 2)
             End If
         Else
             If Len(Fecha) = 5 Then
                 If Val(Right(CHKFecha, 2)) > 50 Then
                     CHKFecha = "0" & Left(CHKFecha, 1) & Mid(CHKFecha, 2, 2) & "19" & Right(CHKFecha, 2)
                 Else
                     CHKFecha = "0" & Left(CHKFecha, 1) & Mid(CHKFecha, 2, 2) & "20" & Right(CHKFecha, 2)
                 End If
             Else
             End If
         End If
     End If
 End If
 If IsNumeric(CHKFecha) Then
 Else
     CHKFecha = "        "
 End If
 
 End Function
 
End Sub