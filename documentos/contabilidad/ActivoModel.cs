using System;

public class ActivoModel {
    private int index;
    private int idArea;
    private int idDepartamento;
    private bool activo;
    private string clave;
    private string descripcion;
    private string marca;
    private string modelo;
    private string noSerie;
    private DateTime? fechaAdquiscion;
    private decimal montoOriginal;
    
    /// <summary>
    /// 
    /// </summary>
    public int  IdActivo {
        get { return this.index; }
        set { this.index = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool Activo {
        get { return this.activo; }
        set { this.activo = value; }
    }

    public int IdArea {
        get { return this.idArea; }
        set { this.idArea = value; }
    }

    public int IdDepartamento {
        get { return this.idDepartamento; }
        set { this.idDepartamento = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    public string Clave {
        get { return this.clave; }
        set { this.clave = value; }
    }

    /// <summary>
    /// obtener o establecer la descripcion generica del activo
    /// </summary>
    public string Descripcion { 
        get { return this.descripcion; }
        set { this.descripcion = value; }
    }

    /// <summary>
    /// obtener o establecer la marca del activo (maquinaria o equipo)
    /// </summary>
    public string Marca { 
        get { return this.marca; }
        set { this.marca = value; }
    }

    /// <summary>
    /// obtener o establecer el modelo del activo (maquinaria o equipo)
    /// </summary>
    public string Modelo { 
        get { return this.modelo; }
        set { this.modelo = value; }
    }

    /// <summary>
    /// obtener o establecer el numero de serie 
    /// </summary>
    public string NoSerie { 
        get { return this.noSerie; }
        set { this.noSerie = value; }
    }

    /// <summary>
    /// obtener o establecer la fecha de adquisicion del activo
    /// </summary>
    public DateTime? FechaAdquisicion {
        get { return this.fechaAdquiscion; }
        set { this.fechaAdquiscion = value; }
    }

    /// <summary>
    /// obtener o establecer el monto original
    /// </summary>
    public decimal MontoOriginal {
        get { return this.montoOriginal; }
        set { this.montoOriginal = value; }
    }
}