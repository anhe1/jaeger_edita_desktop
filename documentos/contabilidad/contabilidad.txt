*** CONTABILIDAD
CTBLC        -- Catálogo de cuentas
   - IdCuenta
   - IdCuentaP
   - Descripcion
   - CodigoAgrupador
   - Tipo (Acredora, Deudora)
   - Naturaleza
   - Status
   - Nivel
-- Crear una tabla para el catálogo de cuentas
CREATE TABLE CatalogoCuentas (
    IdCuenta INT PRIMARY KEY,
    Descripcion VARCHAR(100) NOT NULL,
    IdCuentaP INT, -- Referencia a la cuenta padre
    FOREIGN KEY IdCuentaP) REFERENCES CatalogoCuentas(IdCuenta)
);

-- Consulta recursiva para obtener el árbol jerárquico del catálogo de cuentas
WITH ArbolCuentas AS (
    SELECT id, nombre, cuenta_padre_id, 0 as nivel
    FROM CatalogoCuentas
    WHERE cuenta_padre_id IS NULL
    UNION ALL
    SELECT cc.id, cc.nombre, cc.cuenta_padre_id, ac.nivel + 1
    FROM CatalogoCuentas cc
    JOIN ArbolCuentas ac ON cc.cuenta_padre_id = ac.id
)
SELECT id, nombre, cuenta_padre_id, nivel
FROM ArbolCuentas
ORDER BY nivel, id;

CTBLT        -- Acumulados de cuentas
   - IdCuenta
   - Ejercicio
   - Periodo
   - Cargo
   - Abono
CTBLP        -- Pólizas
   - Identificador
   - Descripcion
   - FechaEmision
   - Ejercicio
   - Periodod
CTBLM        -- Movimientos
   - Identificador
   - IdCuenta
   - Descripcion
   - Cargo
   - Abono
   - Ejercicio
   - Periodod
CTBLA        -- AUXILIARES
   - Identificador
   - IdDocumento