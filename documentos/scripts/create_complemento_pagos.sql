/*
     SCRIPT PARA CREAR LAS TABLAS DEL COMPLEMENTO DE PAGOS
*/
CREATE TABLE `_cmppg` (
	`_cmppg_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`_cmppg_a` INT(1) NOT NULL DEFAULT b'1' COMMENT 'registro activo',
	`_cmppg_ver` VARCHAR(5) NOT NULL DEFAULT '2.0' COMMENT 'Version del complemento de pagos' COLLATE 'utf8_general_ci',
	`_cmppg_cfd_id` INT(11) NOT NULL COMMENT 'indice de relacion del comprobante fiscal',
	`_cmppg_fecpg` DATETIME NOT NULL COMMENT 'fecha y hora en la que el beneficiario recibe el pago',
	`_cmppg_frmpg` VARCHAR(50) NOT NULL COMMENT 'clave de forma en que se realiza el pago' COLLATE 'utf8_general_ci',
	`_cmppg_mndp` VARCHAR(5) NOT NULL COMMENT 'clave de moneda' COLLATE 'utf8_general_ci',
	`_cmppg_tpcmb` DECIMAL(10,6) NOT NULL DEFAULT '1.000000' COMMENT 'tipo de cambio de la moneda a la fecha en que se realizo el pago',
	`_cmppg_monto` DECIMAL(18,4) NOT NULL DEFAULT '0.0000' COMMENT 'importe del pago',
	`_cmppg_nmope` VARCHAR(100) NULL DEFAULT NULL COMMENT 'numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, linea de captura o algun numero de referencia' COLLATE 'utf8_general_ci',
	`_cmppg_rfcems` VARCHAR(13) NULL DEFAULT NULL COMMENT 'RFC de la entidad emisora de la cuenta de origen, es decir, la peradora, el banco, etc.' COLLATE 'utf8_general_ci',
	`_cmppg_nmbnc` VARCHAR(300) NULL DEFAULT NULL COMMENT 'nombre del banco ordenante' COLLATE 'utf8_general_ci',
	`_cmppg_ctord` VARCHAR(50) NULL DEFAULT NULL COMMENT 'numero de la cuenta con la que se realizo el pago' COLLATE 'utf8_general_ci',
	`_cmppg_rfcben` VARCHAR(13) NULL DEFAULT NULL COMMENT 'RFC de la entidad operadora de la cuenta destino' COLLATE 'utf8_general_ci',
	`_cmppg_tppg` VARCHAR(10) NULL DEFAULT NULL COMMENT 'tipo de cadena de pago que genera la entidad receptora del pago' COLLATE 'utf8_general_ci',
	`_cmppg_ctben` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de cuenta en donde se recibio el pago.' COLLATE 'utf8_general_ci',
	`_cmppg_cdnpg` LONGTEXT NULL DEFAULT NULL COMMENT 'cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria' COLLATE 'utf8_general_ci',
	`_cmppg_cerpg` TEXT NULL DEFAULT NULL COMMENT 'certificado que ampara al pago, como una cadena de texto en formato base 64' COLLATE 'utf8_general_ci',
	`_cmppg_sllpg` TEXT NULL DEFAULT NULL COMMENT 'sello digital que se asocie al pago.' COLLATE 'utf8_general_ci',
	`_cmppg_usr_n` VARCHAR(10) NOT NULL COMMENT 'clave de usuario que crea el registro' COLLATE 'utf8_general_ci',
	`_cmppg_fn` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`_cmppg_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifico el registro' COLLATE 'utf8_general_ci',
	`_cmppg_fm` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	PRIMARY KEY (`_cmppg_id`) USING BTREE,
	INDEX `_cmppg_cfd_id` (`_cmppg_cfd_id`) USING BTREE
)
COMMENT='CFDI: complemento de pagos version'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/* 
     SCRIPT PARA LA CREACION DE TABLA DE DOCUMENTOS RELACIONADOS AL COMPLEMENTO DE PAGOS
*/
CREATE TABLE `_cmppgd` (
	`_cmppgd_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`_cmppgd_a` INT(1) NOT NULL DEFAULT b'1' COMMENT 'registro activo',
	`_cmppgd_idpg` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de relacion con la tabla de pagos',
	`_cmppgd_sbid` INT(11) NOT NULL COMMENT 'indice de relacion con la tabla de comprobantes fiscales (_cfdi) del comprobante de pago',
	`_cmppgd_doc_id` INT(11) NULL DEFAULT NULL COMMENT 'tipo de documento (1=emitido, 2=recibido, 3=nomina)',
	`_cmppgd_cfdi_id` INT(11) NOT NULL COMMENT 'indice de relacion con la tabla de comprobantes fiscales (_cfdi)',
	`_cmppgd_uuid` VARCHAR(36) NOT NULL COMMENT 'identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital.' COLLATE 'utf8_general_ci',
	`_cmppgd_serie` VARCHAR(25) NULL DEFAULT NULL COMMENT 'serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.' COLLATE 'utf8_general_ci',
	`_cmppgd_folio` VARCHAR(40) NULL DEFAULT NULL COMMENT 'folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.' COLLATE 'utf8_general_ci',
	`_cmppgd_rfc` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc receptor del comprobante, esta es informacion adicional no se incluye en el complemento..' COLLATE 'utf8_general_ci',
	`_cmppgd_nom` VARCHAR(255) NULL DEFAULT NULL COMMENT 'receptor del comprobante, esta es informacion adicional no se incluye en el complemento.' COLLATE 'utf8_general_ci',
	`_cmppgd_fecems` DATETIME NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
	`_cmppgd_frmpg` VARCHAR(50) NULL DEFAULT NULL COMMENT 'clave de forma en que se realiza el pago' COLLATE 'utf8_general_ci',
	`_cmppgd_mtdpg` VARCHAR(50) NULL DEFAULT NULL COMMENT 'clave del método de pago que se registró en el documento relacionado' COLLATE 'utf8_general_ci',
	`_cmppgd_clvmnd` VARCHAR(5) NULL DEFAULT NULL COMMENT 'clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado' COLLATE 'utf8_general_ci',
	`_cmppgd_equi` DECIMAL(10,6) NULL DEFAULT NULL COMMENT 'tipo de cambio conforme con la moneda registrada en el documento relacionado.',
	`_cmppgd_nmpar` INT(11) NULL DEFAULT NULL COMMENT 'numero de parcialidad que corresponde al pago.',
	`_cmppgd_total` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'importe total del comprobante',
	`_cmppgd_sldant` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe contener el importe total del documento relacionado.',
	`_cmppgd_imppgd` DECIMAL(14,4) NOT NULL DEFAULT '0.0000' COMMENT 'importe pagado para el documento relacionado.',
	`_cmppgd_sldins` DECIMAL(14,4) NOT NULL DEFAULT '0.0000' COMMENT 'diferencia entre el importe del saldo anterior y el monto del pago.',
	`_cmppgd_clvobj` VARCHAR(3) NULL DEFAULT NULL COMMENT 'el pago del documento relacionado es objeto o no de impuesto.' COLLATE 'utf8_general_ci',
	`_cmppgd_jret` TEXT NULL DEFAULT NULL COMMENT 'objeto json con información de los impuestos retenciones aplicables' COLLATE 'utf8_general_ci',
	`_cmppgd_jtra` TEXT NULL DEFAULT NULL COMMENT 'objeto json con información de los impuestos traslados aplicables' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_cmppgd_id`) USING BTREE,
	INDEX `_cmppgd_idpg` (`_cmppgd_idpg`) USING BTREE,
	INDEX `_cmppgd_sbid` (`_cmppgd_sbid`) USING BTREE,
	INDEX `_cmppgd_doc_id` (`_cmppgd_doc_id`) USING BTREE,
	INDEX `_cmppgd_cfdi_id` (`_cmppgd_cfdi_id`) USING BTREE,
	INDEX `_cmppgd_uuid` (`_cmppgd_uuid`) USING BTREE
)
COMMENT='CFDI complemento pagos: documento relacionado'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
