/*
  Tabla de datos del repositorio de comprobantes fiscales (version Mysql o mariaDB)
*/
CREATE TABLE `Comprobantes` (
	`IdDocumento` VARCHAR(36) NOT NULL COMMENT '36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122' COLLATE 'utf8_general_ci',
	`Tipo` VARCHAR(10) NULL DEFAULT NULL COMMENT 'tipo del comprobante (Emitido o Recibido)' COLLATE 'utf8_general_ci',
	`Version` VARCHAR(3) NULL DEFAULT NULL COMMENT 'version del estandar bajo el que se encuentra expresado el comprobante' COLLATE 'utf8_general_ci',
	`Efecto` VARCHAR(16) NULL DEFAULT NULL COMMENT 'efecto o tipo de comprobante' COLLATE 'utf8_general_ci',
	`Folio` VARCHAR(50) NULL DEFAULT NULL COMMENT 'control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. ' COLLATE 'utf8_general_ci',
	`Serie` VARCHAR(25) NULL DEFAULT NULL COMMENT 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.' COLLATE 'utf8_general_ci',
	`EmisorRFC` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del emisor del comprobante' COLLATE 'utf8_general_ci',
	`Emisor` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre, denominacion o razon social del emisor' COLLATE 'utf8_general_ci',
	`ReceptorRFC` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del receptor del comprobante' COLLATE 'utf8_general_ci',
	`Receptor` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre, denominacion o razon social del receptor' COLLATE 'utf8_general_ci',
	`ReceptorDomicilioFiscal` VARCHAR(5) NULL DEFAULT NULL COMMENT 'codigo postal del domicilio fiscal del receptor del comprobante' COLLATE 'utf8_general_ci',
	`FechaEmision` DATETIME NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
	`FechaCerfificacion` DATETIME NULL DEFAULT NULL COMMENT 'fecha de certificacion',
	`Pac` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del proveedor autorizado de certificacion' COLLATE 'utf8_general_ci',
	`RetencionISR` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total de impuesto retenido ISR',
	`RetencionIVA` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total de impuesto retenido IVA',
	`RetencionIEPS` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total del impuesto retenido IEPS',
	`TrasladoIVA` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total de impuesto traslado IVA',
	`TrasladoIEPS` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total de impuesto traslado IEPS',
	`SubTotal` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'sub total',
	`Descuento` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total del descuento',
	`Total` DECIMAL(14,2) NULL DEFAULT '0.00' COMMENT 'total',
	`Estado` VARCHAR(10) NULL DEFAULT NULL COMMENT 'estado del comprobante Cancelado o Vigente' COLLATE 'utf8_general_ci',
	`StatusCancelacion` VARCHAR(32) NULL DEFAULT NULL COMMENT 'leyenda de Estatus de cancelación (si es posible cancelar con o sin aceptación)' COLLATE 'utf8_general_ci',
	`StatusProcesoCancelar` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Estatus de Proceso de Cancelacion' COLLATE 'utf8_general_ci',
	`FechaStatusCancela` DATETIME NULL DEFAULT NULL COMMENT 'fecha de status de cancelacion',
	`FechaValidacion` DATETIME NULL DEFAULT NULL COMMENT 'fecha de validacion del comprobante',
	`FechaNuevo` DATETIME NULL DEFAULT NULL COMMENT 'fecha de registro',
	`Comprobado` INT(11) NULL DEFAULT NULL COMMENT 'bandera que identifica el status de la consilicacion (0 = Sin Comprobar, 1 = Comprobado 2 = No Encontrado)',
	`CveExportacion` VARCHAR(2) NULL DEFAULT NULL COMMENT 'clave de exportacion' COLLATE 'utf8_general_ci',
	`CveRegimenFiscal` VARCHAR(255) NULL DEFAULT NULL COMMENT 'clave de regimen fiscal' COLLATE 'utf8_general_ci',
	`CveMetodoPago` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de metodo de pago' COLLATE 'utf8_general_ci',
	`CveFormaPago` VARCHAR(2) NULL DEFAULT NULL COMMENT 'clave de forma de pago' COLLATE 'utf8_general_ci',
	`CveUsoCFDI` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de uso de CFDI' COLLATE 'utf8_general_ci',
	`CveMoneda` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de moneda' COLLATE 'utf8_general_ci',
	`Parcialidad` INT(11) NULL DEFAULT NULL COMMENT 'numero de parcialidad',
	`TerceroRFC` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc a cuenta de terceros' COLLATE 'utf8_general_ci',
	`Motivo` VARCHAR(255) NULL DEFAULT NULL COMMENT 'motivo de cancelacion' COLLATE 'utf8_general_ci',
	`FolioSustitucion` VARCHAR(255) NULL DEFAULT NULL COMMENT 'folio de sustitucion' COLLATE 'utf8_general_ci',
	`PathXML` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`PathPDF` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`Resultado` VARCHAR(16) NULL DEFAULT NULL COMMENT 'resultado' COLLATE 'utf8_general_ci',
	`XMLB64` TEXT NULL DEFAULT NULL COMMENT 'xml en base64' COLLATE 'utf8_general_ci',
	`PDFB64` TEXT NULL DEFAULT NULL COMMENT 'pdf en base64 (obsoleto)' COLLATE 'utf8_general_ci',
	`AcuseB64` TEXT NULL DEFAULT NULL COMMENT 'xml de acuse base64' COLLATE 'utf8_general_ci',
	`AcuseFinalB64` TEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`IdDocumento`) USING BTREE,
	INDEX `EmisorRFC` (`EmisorRFC`) USING BTREE,
	INDEX `ReceptorRFC` (`ReceptorRFC`) USING BTREE,
	INDEX `FechaEmision` (`FechaEmision`) USING BTREE,
	INDEX `Comprobado` (`Comprobado`) USING BTREE,
	INDEX `TerceroRFC` (`TerceroRFC`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
