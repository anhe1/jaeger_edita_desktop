/*
    CATALOGO DE UNIDADES UTILIZADO PARA LOS ALMACENES, (MP, PT, DP Y PP)
       VERSION MYSQL
*/
CREATE TABLE `_ctund` (
	`_ctund_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`_ctund_a` BIT(1) NOT NULL COMMENT 'registro activo',
	`_ctund_alm_id` INT(11) NULL DEFAULT NULL COMMENT 'indice del almacen al que pertenece el catalogo',
	`_ctund_nom` VARCHAR(32) NOT NULL COMMENT 'nombre de la unidad' COLLATE 'utf8_general_ci',
	`_ctund_fac` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'factor de la unidad',
	`_ctund_fn` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`_ctund_usr_n` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`_ctund_fm` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`_ctund_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_ctund_id`) USING BTREE
)
COMMENT='almacen de materia prima: catalogo de unidades'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/* DATOS INICIALES DE LA TABLA */
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (1, b'1', 1, 'Hoja', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (2, b'1', 1, 'Kilo', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (3, b'1', 1, 'Galón', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (4, b'1', 1, 'Litro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (5, b'1', 1, 'Metro Lineal', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (6, b'1', 1, 'Pieza', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (7, b'1', 1, 'Lámina', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (8, b'1', 1, 'Gramos', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (9, b'1', 1, 'Centímetro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (10, b'1', 1, 'Juego', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (11, b'1', 1, 'Caja', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (12, b'1', 1, 'Lata', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (13, b'1', 1, 'Kilómetro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (14, b'1', 1, 'Metro/2', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (15, b'1', 1, 'Ciento', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (16, b'1', 1, 'Millar', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (17, b'1', 1, 'linea', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (18, b'1', 1, 'cm/2', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (19, b'1', 1, 'Rollo', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (20, b'1', 1, 'N/A', '2019-04-26 00:00:00', 'ANHE1');
INSERT INTO `_ctund` (`_ctund_id`, `_ctund_a`, `_ctund_alm_id`, `_ctund_nom`, `_ctund_fn`, `_ctund_usr_n`) VALUES (21, b'1', 1, 'Puntos', '2020-08-12 16:20:20', 'ANHE1');

CREATE TABLE `CTUND` (
	`CTUND_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`CTUND_A` BIT(1) NOT NULL COMMENT 'registro activo',
	`CTUND_ALM_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del almacen al que pertenece el catalogo',
	`CTUND_NOM` VARCHAR(32) NOT NULL COMMENT 'nombre de la unidad' COLLATE 'utf8_general_ci',
	`CTUND_CLVUND` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave sat' COLLATE 'utf8_general_ci',
	`CTUND_FAC` DECIMAL(14,6) NULL DEFAULT NULL COMMENT 'factor de la unidad',
	`CTUND_FN` DATETIME NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	`CTUND_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`CTUND_FM` DATETIME NULL DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
	`CTUND_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`CTUND_ID`) USING BTREE,
	INDEX `CTUND_ALM_ID` (`CTUND_ALM_ID`) USING BTREE
)
COMMENT='almacen: catalogo de unidades'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (1, b'1', 1, 'Hoja', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (2, b'1', 1, 'Kilo', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (3, b'1', 1, 'Galón', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (4, b'1', 1, 'Litro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (5, b'1', 1, 'Metro Lineal', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (6, b'1', 1, 'Pieza', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (7, b'1', 1, 'Lámina', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (8, b'1', 1, 'Gramos', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (9, b'1', 1, 'Centímetro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (10, b'1', 1, 'Juego', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (11, b'1', 1, 'Caja', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (12, b'1', 1, 'Lata', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (13, b'1', 1, 'Kilómetro', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (14, b'1', 1, 'Metro/2', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (15, b'1', 1, 'Ciento', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (16, b'1', 1, 'Millar', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (17, b'1', 1, 'linea', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (18, b'1', 1, 'cm/2', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (19, b'1', 1, 'Rollo', '2019-04-07 23:46:39', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (20, b'1', 1, 'N/A', '2019-04-26 00:00:00', 'ANHE1');
INSERT INTO `ctund` (`CTUND_ID`, `CTUND_A`, `CTUND_ALM_ID`, `CTUND_NOM`, `CTUND_FN`, `CTUND_USR_N`) VALUES (21, b'1', 1, 'Puntos', '2020-08-12 16:20:20', 'ANHE1');

/*
      CATALOGO DE UNIDADES VERSION FIREBIRD
*/
CREATE TABLE CTUND
(
  CTUND_ID Integer NOT NULL,
  CTUND_A Smallint DEFAULT 1 NOT NULL,
  CTUND_ALM_ID Integer,
  CTUND_NOM Varchar(50),
  CTUND_FAC Numeric(18,4),
  CTUND_FN Timestamp NOT NULL,
  CTUND_USR_N Varchar(10),
  CTUND_FM Date,
  CTUND_USR_M Varchar(10),
  CONSTRAINT PK_CTUND_ID PRIMARY KEY (CTUND_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTUND_ID' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo de la tabla'  where RDB$FIELD_NAME = 'CTUND_A' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del almacen al que pertenece la unidad'  where RDB$FIELD_NAME = 'CTUND_ALM_ID' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de la unidad'  where RDB$FIELD_NAME = 'CTUND_NOM' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de conversion de la unidad'  where RDB$FIELD_NAME = 'CTUND_FAC' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTUND_FN' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTUND_USR_N' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'CTUND_FM' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario en modificar el registro'  where RDB$FIELD_NAME = 'CTUND_USR_M' and RDB$RELATION_NAME = 'CTUND';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de unidades del almacen'
where RDB$RELATION_NAME = 'CTUND';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTUND TO  SYSDBA WITH GRANT OPTION;

/* GENERADOR PARA AUTO-INCREMENTAL */
CREATE GENERATOR GEN_CTUND_ID;

SET TERM ^ ;
CREATE TRIGGER CTUND_BI FOR CTUND ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* --------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 16122021 16:05
    Purpose: disparador para indice incremental
-------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTUND_ID IS NULL) THEN
    NEW.CTUND_ID = GEN_ID(GEN_CTUND_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTUND_ID, 0);
    if (tmp < new.CTUND_ID) then
      tmp = GEN_ID(GEN_CTUND_ID, new.CTUND_ID-tmp);
  END
END^
SET TERM ; ^

INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('0', '1', '2', '...', '0.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('1', '1', '2', 'Pieza', '1.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('2', '1', '2', 'Docena', '12.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('3', '0', '2', 'Caja 50 pzs.', '50.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('4', '0', '2', 'Caja 100 pzs.', '100.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('5', '0', '2', 'Caja 40 pzs.', '40.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('6', '0', '2', 'Caja 80 pzs.', '80.0000', '09.09.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('7', '1', '2', 'Paq. 25 pzs.', '25.0000', '21.10.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('8', '0', '2', 'Paq. 20 pzs.', '20.0000', '23.10.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('9', '1', '2', 'Bolsa Mini 10 pzas.', '1.0000', '06.11.2012, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('10', '1', '2', 'Planilla (10 pzs)', '10.0000', '11.08.2014, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('11', '1', '2', 'Invitacion (10 pzs)', '1.0000', '30.09.2014, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('12', '1', '2', 'Calabera (3 pzs.)', '3.0000', '30.09.2014, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('13', '1', '2', 'Calabera (6 pzs.)', '6.0000', '30.09.2014, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('14', '1', '2', 'Paq. (6 pzas)', '1.0000', '21.05.2015, 00:00:00.000', 'ANHE1');
INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_FN, CTUND_USR_N) VALUES ('15', '1', '2', 'Paq. (papel 10 pzas)', '10.0000', '27.10.2015, 00:00:00.000', 'ANHE1');
