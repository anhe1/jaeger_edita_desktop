/* 
 *** Disparadores para tabla CFDI 
 *** Develop: ANHE1 05052023 2249
 *** Actualización de los registros actuales de la base de datos; se debe ejecutar antes de 
 crear los disparadores
 *** Revisiones:
 */

ALTER TABLE `_cfdi`
	ADD COLUMN `_cfdi_anio` SMALLINT(4) NULL DEFAULT NULL COMMENT 'ejercicio al que pertenece el comprobante' AFTER `_cfdi_fenvi`,
	ADD COLUMN `_cfdi_mes` SMALLINT(2) NULL DEFAULT NULL COMMENT 'periodo al que pertenece el comprobante' AFTER `_cfdi_anio`,
	ADD INDEX `_cfdi_anio` (`_cfdi_anio`),
	ADD INDEX `_cfdi_mes` (`_cfdi_mes`);

UPDATE _cfdi SET _cfdi_anio = YEAR(_cfdi_fecems), _cfdi_mes = MONTH(_cfdi_fecems)

/* *** Disparadores para la tabla CFDI al insertar o actualizar */
CREATE DEFINER=`root`@`localhost` TRIGGER `_cfdi_before_insert` BEFORE INSERT ON `_cfdi` FOR EACH ROW BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 05052023 2249
    Purpose: al insertar un nuevo registro las columnas de ejercicio y periodo se obtiene de la fecha de emision
    el ejercicio y periodo correspondiente
------------------------------------------------------------------------------------------------------------- */
	SET NEW._cfdi_anio = YEAR(NEW._cfdi_fecems);
	SET NEW._cfdi_mes = MONTH(NEW._cfdi_fecems);
END

CREATE DEFINER=`root`@`localhost` TRIGGER `_cfdi_before_update` BEFORE UPDATE ON `_cfdi` FOR EACH ROW BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 05052023 2249
    Purpose: al actualizar la fecha de emision se actualiza ejercicio y periodo
------------------------------------------------------------------------------------------------------------- */
	IF NEW._cfdi_fecems <> OLD._cfdi_fecems
		THEN
			SET NEW._cfdi_anio = YEAR(NEW._cfdi_fecems);
			SET NEW._cfdi_mes = MONTH(NEW._cfdi_fecems);
	END IF;
END

/*
 Versión Firebird
*/

ALTER TABLE RMSN ADD RMSN_MES Smallint;
ALTER TABLE RMSN ADD RMSN_ANIO Smallint;
CREATE INDEX IDX_RMSN_ANIO ON RMSN (RMSN_ANIO);
CREATE INDEX IDX_RMSN_MES ON RMSN (RMSN_MES);

/* Actualizar datos del periodo y ejercicio */
UPDATE RMSN 
SET RMSN.RMSN_ANIO = EXTRACT(YEAR FROM RMSN.RMSN_FECEMS),
    RMSN.RMSN_MES = EXTRACT(MONTH FROM RMSN.RMSN_FECEMS);

SET TERM ^ ;

CREATE TRIGGER TRG_RMSN_PERIODO FOR RMSN
ACTIVE BEFORE INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 05052023 2249
    Purpose: al insertar un nuevo registro las columnas de ejercicio y periodo se obtiene de la fecha de emision
    el ejercicio y periodo correspondiente
------------------------------------------------------------------------------------------------------------- */
    IF (NEW.RMSN_FECEMS <> OLD.RMSN_FECEMS) THEN
        BEGIN
            NEW.RMSN_ANIO = EXTRACT(YEAR FROM NEW.RMSN_FECEMS);
            NEW.RMSN_MES = EXTRACT(MONTH FROM NEW.RMSN_FECEMS);
        END
END^

SET TERM ; ^ 
