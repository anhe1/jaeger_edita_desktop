/* *** ALMACEN DE MATERIA PRIMA ***

vales de entrada y salida
registro de inventario
existencias

*/

/***  TABLA DE VALES, INVENTARIO  ***/
CREATE TABLE `ALMMP` (
	`ALMMP_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`ALMMP_CTALM_ID` INT(11) NULL DEFAULT '0' COMMENT 'indice de la tabla de almacenes',
	`ALMMP_TPALM_ID` INT(11) NULL DEFAULT '0' COMMENT 'incide de tipo de almacen (MP, PT, DP, PP)',
	`ALMMP_VER` VARCHAR(3) NULL DEFAULT NULL COMMENT 'version del comprobante' COLLATE 'utf8_general_ci',
	`ALMMP_DOC_ID` INT(11) NULL DEFAULT NULL COMMENT 'tipo de documento',
	`ALMMP_CTEFC_ID` INT(11) NULL DEFAULT NULL COMMENT 'efecto del comprobante',
	`ALMMP_CTDEV_ID` INT(11) NULL DEFAULT NULL COMMENT 'clave del catalogo de tipos de devolucion',
	`ALMMP_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de status del comprobante',
	`ALMMP_UUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'identificador del documento' COLLATE 'utf8_general_ci',
	`ALMMP_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
	`ALMMP_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'folio de control interno',
	`ALMMP_CTSR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la serie de control interno',
	`ALMMP_SERIE` VARCHAR(20) NULL DEFAULT NULL COMMENT 'serie de control interno' COLLATE 'utf8_general_ci',
	`ALMMP_FECEMS` DATETIME NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de emision',
	`ALMMP_FCING` DATETIME NULL DEFAULT NULL COMMENT 'fecha de ingreso al almacen',
	`ALMMP_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMMP_CTDEP_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del departamento',
	`ALMMP_RFCR` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del receptor' COLLATE 'utf8_general_ci',
	`ALMMP_NOM` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre del receptor' COLLATE 'utf8_general_ci',
	`ALMMP_CNTCT` VARCHAR(50) NULL DEFAULT NULL COMMENT 'nombre del contacto' COLLATE 'utf8_general_ci',
	`ALMMP_RFRNC` VARCHAR(50) NULL DEFAULT NULL COMMENT 'referencia' COLLATE 'utf8_general_ci',
	`ALMMP_SBTTL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'sub total',
	`ALMMP_DSCT` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'descuento',
	`ALMMP_TRIVA` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe de traslado de iva',
	`ALMMP_GTOTAL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'gran total',
	`ALMMP_OBSRV` VARCHAR(255) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`ALMMP_USU_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`ALMMP_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	`ALMMP_USU_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`ALMMP_FM` DATETIME NULL DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
	`ALMMP_URL_PDF` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'url de descarga para la representación impresa' COLLATE 'utf8_general_ci',
	`ALMMP_ANIO` INT(11) NULL DEFAULT year(`ALMMP_FECEMS`) COMMENT 'ejercicio',
	`ALMMP_MES` INT(11) NULL DEFAULT month(`ALMMP_FECEMS`) COMMENT 'periodo o mes',
	PRIMARY KEY (`ALMMP_ID`) USING BTREE,
	INDEX `ALMMP_CTALM_ID` (`ALMMP_CTALM_ID`) USING BTREE,
	INDEX `ALMMP_DOC_ID` (`ALMMP_DOC_ID`) USING BTREE,
	INDEX `ALMMP_UUID` (`ALMMP_UUID`) USING BTREE,
	INDEX `ALMMP_CTEFC_ID` (`ALMMP_CTEFC_ID`) USING BTREE,
	INDEX `ALMMP_STTS_ID` (`ALMMP_STTS_ID`) USING BTREE,
	INDEX `ALMMP_CTREL_ID` (`ALMMP_CTREL_ID`) USING BTREE,
	INDEX `ALMMP_DRCTR_ID` (`ALMMP_DRCTR_ID`) USING BTREE,
	INDEX `ALMMP_ANIO` (`ALMMP_ANIO`) USING BTREE,
	INDEX `ALMMP_MES` (`ALMMP_MES`) USING BTREE,
	INDEX `ALMMP_DEPTO_ID` (`ALMMP_CTDEP_ID`) USING BTREE,
	INDEX `ALMPT_CTSR_ID` (`ALMMP_CTSR_ID`) USING BTREE,
	INDEX `ALMMP_TPALM_ID` (`ALMMP_TPALM_ID`) USING BTREE
)
COMMENT='almacen de materia prima: vales, inventarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE RELACIONES ENTRE DOCUMENTOS DEL ALMACÉN  ***/
CREATE TABLE `ALMMPR` (
	`ALMMPR_ALMMP_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de documentos de almacen (ALMMP)',
	`ALMMPR_DOC_ID` INT(11) NULL DEFAULT NULL COMMENT 'tipo de documento ',
	`ALMMPR_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
	`ALMMPR_CTREL` VARCHAR(60) NULL DEFAULT NULL COMMENT 'descripcion de la clave de relacion con otros comprobantes' COLLATE 'utf8_general_ci',
	`ALMMPR_UUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'id de documento' COLLATE 'utf8_general_ci',
	`ALMMPR_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMMPR_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
	`ALMMPR_SERIE` VARCHAR(50) NULL DEFAULT NULL COMMENT 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres' COLLATE 'utf8_general_ci',
	`ALMMPR_FECEMS` DATETIME NULL DEFAULT NULL,
	`ALMMPR_CLV` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de control del directorio' COLLATE 'utf8_general_ci',
	`ALMMPR_RFCR` VARCHAR(14) NULL DEFAULT NULL COMMENT 'clave de control del directorio' COLLATE 'utf8_general_ci',
	`ALMMPR_NOM` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre o razon social del receptor' COLLATE 'utf8_general_ci',
	`ALMMPR_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`ALMMPR_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha y hora de expedicion del comprobante.',
	UNIQUE INDEX `INDICE` (`ALMMPR_ALMMP_ID`, `ALMMPR_DOC_ID`, `ALMMPR_CTREL_ID`, `ALMMPR_UUID`, `ALMMPR_DRCTR_ID`, `ALMMPR_FOLIO`) USING BTREE
)
COMMENT='almacen de materia prima: documentos relacionados'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE REGISTRO DE CAMBIOS DE STATUS ***/
CREATE TABLE `ALMMPS` (
	`ALMMPS_ALMMP_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de almacen de materia prima',
	`ALMMPS_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status original',
	`ALMMPS_STTSB_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status del status modificado',
	`ALMMPS_CTMTV_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de motivos de modificacion del status',
	`ALMMPS_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMMPS_CTMTV` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave del motivo del cambio de status' COLLATE 'utf8_general_ci',
	`ALMMPS_NOTA` VARCHAR(50) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`ALMMPS_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio' COLLATE 'utf8_general_ci',
	`ALMMPS_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	UNIQUE INDEX `ALMMPS_ALMMP_ID_ALMMPS_STTS_ID_ALMMPS_STTSB_ID_ALMMPS_CTMTV_ID` (`ALMMPS_ALMMP_ID`, `ALMMPS_STTS_ID`, `ALMMPS_STTSB_ID`, `ALMMPS_CTMTV_ID`) USING BTREE
)
COMMENT='almacen de materia prima: cambios de status'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE REGISTRO DE MOVIMIENTOS DE PRODUCTOS O SERVICIOS DE LOS DOCUMENTOS DEL ALMACÉN ***/
CREATE TABLE `MVAMP` (
	`MVAMP_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`MVAMP_A` INT(11) NULL DEFAULT '1' COMMENT 'registro activo',
	`MVAMP_CTEFC_ID` INT(11) NULL DEFAULT NULL COMMENT 'efecto',
	`MVAMP_ALMMP_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de materia prima',
	`MVAMP_DOC_ID` INT(11) NULL DEFAULT NULL COMMENT 'tipo',
	`MVAMP_CTPRD_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de productos',
	`MVAMP_CTMDL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de modelos',
	`MVAMP_CTUND_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de unidades',
	`MVAMP_OCMP` INT(11) NULL DEFAULT NULL COMMENT 'orden de compra',
	`MVAMP_OPRD` INT(11) NULL DEFAULT NULL COMMENT 'orden de produccion',
	`MVAMP_CNTE` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'cantidad entrada',
	`MVAMP_CNTS` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'cantidad salida',
	`MVAMP_CTUND` VARCHAR(20) NULL DEFAULT NULL COMMENT 'descripcion de la unidad' COLLATE 'utf8_general_ci',
	`MVAMP_NOM` VARCHAR(255) NULL DEFAULT NULL COMMENT 'descripcion del producto o servicio' COLLATE 'utf8_general_ci',
	`MVAMP_MRC` VARCHAR(255) NULL DEFAULT NULL COMMENT 'marca' COLLATE 'utf8_general_ci',
	`MVAMP_ESPC` VARCHAR(255) NULL DEFAULT NULL COMMENT 'especificacion' COLLATE 'utf8_general_ci',
	`MVAMP_SKU` VARCHAR(50) NULL DEFAULT NULL COMMENT 'numero de identificacion' COLLATE 'utf8_general_ci',
	`MVAMP_UNTC` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'costo unitario del producto / modelo',
	`MVAMP_UNDC` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'costo unitario por la unidad',
	`MVAMP_UNTR` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'valor unitario por pieza',
	`MVAMP_UNDF` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'factor de conversion',
	`MVAMP_UNTR2` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'unitario de la unidad (valor unitario por el factor de la unidad)',
	`MVAMP_SBTTL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'subtotal',
	`MVAMP_DESC` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe del descuento aplicado al producto',
	`MVAMP_IMPRT` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe = subtotal - descuento',
	`MVAMP_TSIVA` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'tasa del iva aplicable',
	`MVAMP_TRIVA` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe del impuesto traslado IVA',
	`MVAMP_TOTAL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'total = importe + importe del iva',
	`MVAMP_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	`MVAMP_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuiario creador del registro' COLLATE 'utf8_general_ci',
	`MVAMP_FM` DATETIME NULL DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
	`MVAMP_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`MVAMP_ID`) USING BTREE,
	INDEX `MVAMP_CTEFC_ID` (`MVAMP_CTEFC_ID`) USING BTREE,
	INDEX `MVAMP_ALMMP_ID` (`MVAMP_ALMMP_ID`) USING BTREE,
	INDEX `MVAMP_CTPRD_ID` (`MVAMP_CTPRD_ID`) USING BTREE,
	INDEX `MVAMP_CTMDL_ID` (`MVAMP_CTMDL_ID`) USING BTREE,
	INDEX `MVAMP_CTUND_ID` (`MVAMP_CTUND_ID`) USING BTREE,
	INDEX `MVAMP_OCMP` (`MVAMP_OCMP`) USING BTREE,
	INDEX `MVAMP_OPRD` (`MVAMP_OPRD`) USING BTREE,
	INDEX `MVAMP_CTTIPO_ID` (`MVAMP_DOC_ID`) USING BTREE
)
COMMENT='movimientos de almacen de materia prima: partidas de vales e inventario'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
