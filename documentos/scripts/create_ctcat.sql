/* *** CATÁLOGO DE CLASIFICACIONES GENERALES *** 
categorias de bienes, productos y servicios (BPS) 
*/

CREATE TABLE `CTCAT` (
	`CTCAT_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la categoria',
	`CTCAT_CTCAT_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con categorias',
	`CTCAT_ALM_ID` SMALLINT(1) NULL DEFAULT '1' COMMENT 'tipo de almacen (MP=1, PT = 2, DP = 3, TW = 4)',
	`CTCAT_A` SMALLINT(1) NULL DEFAULT '1' COMMENT 'registro activo',
	`CTCAT_CLV` VARCHAR(128) NULL DEFAULT NULL COMMENT 'clave' COLLATE 'utf8_general_ci',
	`CTCAT_NOM` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre o descripcion de la categoria' COLLATE 'utf8_general_ci',
	`CTCAT_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`CTCAT_FN` TIMESTAMP NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	PRIMARY KEY (`CTCAT_ID`) USING BTREE,
	INDEX `CTCAT_CTCAT_ID` (`CTCAT_CTCAT_ID`) USING BTREE
)
COMMENT='categorias de bienes, productos y servicios (BPS) '
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE CTCAT
(
  CTCAT_ID Integer NOT NULL,
  CTCAT_CTCAT_ID Integer,
  CTCAT_ALM_ID Integer,
  CTCAT_A Smallint,
  CTCAT_CLV Varchar(128),
  CTCAT_NOM Varchar(128),
  CTCAT_USR_N Varchar(10),
  CTCAT_FN Timestamp,
  CONSTRAINT PK_CTCAT_ID PRIMARY KEY (CTCAT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la categoria'  where RDB$FIELD_NAME = 'CTCAT_ID' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con categorias'  where RDB$FIELD_NAME = 'CTCAT_CTCAT_ID' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTCAT_A' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave'  where RDB$FIELD_NAME = 'CTCAT_CLV' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de la categoria'  where RDB$FIELD_NAME = 'CTCAT_NOM' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTCAT_USR_N' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTCAT_FN' and RDB$RELATION_NAME = 'CTCAT';
CREATE INDEX IDX_CTCAT_CTCAT_ID ON CTCAT (CTCAT_CTCAT_ID);
CREATE INDEX IDX_CTCAT_ID ON CTCAT (CTCAT_ID);
UPDATE RDB$RELATIONS set RDB$DESCRIPTION = 'categorias de bienes, productos y servicios (BPS) '
where RDB$RELATION_NAME = 'CTCAT';

/* GENERADOR PARA AUTO-INCREMENTAL */
CREATE GENERATOR GEN_CTCAT_ID;

SET TERM !! ;
CREATE TRIGGER CTCAT_BI FOR CTCAT
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* --------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 16122021 16:05
    Purpose: disparador para indice incremental
-------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTCAT_ID IS NULL) THEN
    NEW.CTCAT_ID = GEN_ID(GEN_CTCAT_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTCAT_ID, 0);
    if (tmp < new.CTCAT_ID) then
      tmp = GEN_ID(GEN_CTCAT_ID, new.CTCAT_ID-tmp);
  END
END!!
SET TERM ; !!




SELECT `_ctlmdl_id` AS CTMDL_ID, 
`_ctlmdl_idcat` AS CTMDL_CTCLS_ID, 
`_ctlmdl_idpro` AS CTMDL_CTPRD_ID, 
`_ctlmdl_idpre` AS CTMDL_CTPRC_ID, 
`_ctlmdl_atrzd` AS CTMDL_ATRZD, 
`_ctlmdl_alm_id` AS CTMDL_ALM_ID, 
`_ctlmdl_a`       AS CTMDL_A, 
`_ctlmdl_tipo`    AS CTMDL_TIPO, 
`_ctlmdl_vis`     AS CTMDL_VIS, 
`_ctlmdl_unddxy`  AS CTMDL_UNDDXY, 
`_ctlmdl_unddz`   AS CTMDL_UNDDZ, 
`_ctlmdl_undda`   AS CTMDL_UNDDA, 
`_ctlmdl_retisr`  AS CTMDL_RETISR, 
`_ctlmdl_retisrf` AS CTMDL_RETISRF, 
`_ctlmdl_retiva`  AS CTMDL_RETIVA, 
`_ctlmdl_retivaf` AS CTMDL_RETIVAF, 
`_ctlmdl_trsiva`  AS CTMDL_TRSIVA, 
`_ctlmdl_trsivaf` AS CTMDL_TRSIVAF, 
`_ctlmdl_retieps` AS CTMDL_RETIEPS, 
`_ctlmdl_retiepsf`AS CTMDL_RETIEPSF, 
`_ctlmdl_trsieps` AS CTMDL_TRSIEPS, 
`_ctlmdl_trsiepsf`AS CTMDL_TRSIEPSF, 
`_ctlmdl_untr`    AS CTMDL_UNTR, 
`_ctlmdl_untr1`   AS CTMDL_UNTR1, 
`_ctlmdl_untr2`   AS CTMDL_UNTR2,   
`_ctlmdl_cant1`   AS CTMDL_CANT1,
`_ctlmdl_cant2`   AS CTMDL_CANT2, 
`_ctlmdl_ext`     AS CTMDL_EXT, 
`_ctlmdl_min`     AS CTMDL_MIN, 
`_ctlmdl_max`     AS CTMDL_MAX, 
`_ctlmdl_reord`   AS CTMDL_REORD, 
`_ctlmdl_largo`   AS CTMDL_LARGO, 
`_ctlmdl_ancho`   AS CTMDL_ANCHO, 
`_ctlmdl_alto`    AS CTMDL_ALTO, 
`_ctlmdl_peso`    AS CTMDL_PESO, 
`_ctlmdl_clvund`  AS CTMDL_CLVUND, 
`_ctlmdl_clvprds` AS CTMDL_CLVPRDS, 
`_ctlmdl_clvobj`  AS CTMDL_CLVOBJ, 
`_ctlmdl_cdg`     AS CTMDL_CDG, 
`_ctlmdl_undd`    AS CTMDL_UNDD, 
`_ctlmdl_ctapre`  AS CTMDL_CTAPRE, 
`_ctlmdl_sku`     AS CTMDL_SKU, 
`_ctlmdl_numreq`  AS CTMDL_NUMREQ, 
`_ctlmdl_clv`     AS CTMDL_CLV, 
`_ctlmdl_mrc`     AS CTMDL_MRC, 
`_ctlmdl_espc`    AS CTMDL_ESPC, 
`_ctlmdl_dscrc`   AS CTMDL_DSCRC, 
`_ctlmdl_etqts`   AS CTMDL_ETQTS, 
`_ctlmdl_dscr`    AS CTMDL_DSCR, 
`_ctlmdl_usr_n`   AS CTMDL_USR_N, 
`_ctlmdl_usr_m`   AS CTMDL_USR_M, 
`_ctlmdl_fn`      AS CTMDL_FN, 
`_ctlmdl_fm`      AS CTMDL_FM
FROM jaeger_ipr98112test.`_ctlmdl`;


SELECT `_ctlprd_id` AS CTPRD_ID, 
`_ctlprd_a`     AS CTPRD_A, 
`_ctlprd_alm_id`AS CTPRD_ALM_ID, 
`_ctlprd_tipo`  AS CTPRD_TIPO, 
`_ctlprd_subid` AS CTPRD_SUBID, 
`_ctlprv_cnvr`  AS CTPRV_CNVR, 
`_ctlprd_clv`   AS CTPRD_CLV, 
`_ctlprd_nom`   AS CTPRD_NOM, 
`_ctlprd_usr_n` AS CTPRD_USR_N, 
`_ctlprd_fn`    AS CTPRD_FN 
FROM jaeger_ipr98112test.`_ctlprd`;



INSERT OR UPDATE INTO ALMMPS ( ALMMPS_ALMMP_ID, ALMMPS_STTS_ID, ALMMPS_STTSB_ID, ALMMPS_CTMTV_ID, ALMMPS_DRCTR_ID, ALMMPS_CTMTV, ALMMPS_NOTA, ALMMPS_USR_N, ALMMPS_FN)
                      VALUES (@ALMMPS_ALMMP_ID,@ALMMPS_STTS_ID,@ALMMPS_STTSB_ID,@ALMMPS_CTMTV_ID,@ALMMPS_DRCTR_ID,@ALMMPS_CTMTV,@ALMMPS_NOTA,@ALMMPS_USR_N,@ALMMPS_FN)
					  
INSERT OR UPDATE INTO ALMMPS ( ALMMPS_ALMMP_ID, ALMMPS_STTS_ID, ALMMPS_STTSB_ID, ALMMPS_CTMTV_ID, ALMMPS_DRCTR_ID, ALMMPS_CTMTV, ALMMPS_NOTA, ALMMPS_USR_N, ALMMPS_FN)
                      VALUES (@ALMMPS_ALMMP_ID,@ALMMPS_STTS_ID,@ALMMPS_STTSB_ID,@ALMMPS_CTMTV_ID,@ALMMPS_DRCTR_ID,@ALMMPS_CTMTV,@ALMMPS_NOTA,@ALMMPS_USR_N,@ALMMPS_FN)
					  
					  
INSERT INTO `ALMMPS` (`ALMMPS_ALMMP_ID`, `ALMMPS_STTS_ID`, `ALMMPS_STTSB_ID`, `ALMMPS_CTMTV_ID`, `ALMMPS_DRCTR_ID`, `ALMMPS_CTMTV`, `ALMMPS_NOTA`, `ALMMPS_USR_N`) 
VALUES (1, 1, 0, 0, 1, 'N:', 'NOTA', 'ANHE1') ON DUPLICATE KEY UPDATE ALMMPS_ALMMP_ID = 1, ALMMPS_STTS_ID=0, ALMMPS_STTSB_ID=1, ALMMPS_CTMTV_ID=1, ALMMPS_DRCTR_ID=1, ALMMPS_CTMTV = '', ALMMPS_USR_N = ''



SELECT 
 `p1`.`_ctlprd_id` AS `IdProducto` ,
 `p1`.`_ctlprd_subid` AS `IdCategoria` ,
 `c3`.`IdSubCategoria` AS `IdSubCategoria` ,
 `c3`.`Tipo` AS `Tipo` ,
 `c3`.`Clase` AS `Clase` ,
 `p1`.`_ctlprd_nom` AS `Producto` ,
 ( CASE  WHEN ( `p1`.`_ctlprd_nom` = @Nombre0 ) THEN `c3`.`Descripcion`  ELSE  concat(`c3`.`Descripcion`,@MethodConst1,`p1`.`_ctlprd_nom`)  END ) AS `Descripcion`  FROM  (SELECT  `c3`.`CTCAT_ID` AS `IdCategoria` , `c2`.`CTCAT_CTCAT_ID` AS `IdSubCategoria` , (CASE  WHEN (( `c1`.`CTCAT_NOM` = @Descripcion0Join0 ) AND ( `c2`.`CTCAT_NOM` = @Descripcion1Join0 ))  THEN `c3`.`CTCAT_NOM`  WHEN ( `c1`.`CTCAT_NOM` = @Descripcion2Join0 )  THEN `c2`.`CTCAT_NOM` ELSE `c1`.`CTCAT_NOM` END ) AS `Tipo` , 
 ( CASE  WHEN ( `c2`.`CTCAT_NOM` = @Descripcion3Join0 ) THEN `c3`.`CTCAT_NOM`  ELSE `c2`.`CTCAT_NOM` END ) AS `Clase` , 
 `c3`.`CTCAT_NOM` AS `Descripcion`  
 FROM `CTCAT` c1 
 Inner JOIN `CTCAT` c2 ON ( `c2`.`CTCAT_CTCAT_ID` = `c1`.`CTCAT_ID` )  
 Inner JOIN `CTCAT` c3 ON ( `c3`.`CTCAT_CTCAT_ID` = `c2`.`CTCAT_ID` )) 
 c3  Right JOIN  (SELECT `_ctlprd_id`,`_ctlprd_a`,`_ctlprd_alm_id`,`_ctlprd_tipo`,`_ctlprd_subid`,`_ctlprd_clv`,`_ctlprd_nom`,`_ctlprd_usr_n`,`_ctlprd_fn` FROM `_ctlprd`  ) p1    ON ( `c3`.`IdCategoria` = `p1`.`_ctlprd_subid` ) 
 
 
 SELECT 
 P1.CTPRD_ID AS IDPRODUCTO,
 P1.CTPRD_CTCLS_ID AS IDCATEGORIA,
 C3.IDSUBCATEGORIA AS IDSUBCATEGORIA,
 C3.TIPO AS TIPO ,
 C3.CLASE AS CLASE ,
 P1.CTPRD_NOM AS PRODUCTO ,
 ( CASE  WHEN ( P1.CTPRD_NOM = '' ) THEN C3.DESCRIPCION  ELSE  (C3.DESCRIPCION ||'/ ' ||P1.CTPRD_NOM)  END ) AS DESCRIPCION FROM (SELECT  C3.CTCLS_ID AS IDCATEGORIA , C2.CTCLS_SBID AS IDSUBCATEGORIA , (CASE  WHEN (( C1.CTCLS_CLASS2 = '' ) AND ( C2.CTCLS_CLASS2 = '' ))   THEN C3.CTCLS_CLASS2  WHEN ( C1.CTCLS_CLASS2 = '' )  THEN C2.CTCLS_CLASS2 ELSE C1.CTCLS_CLASS2 END ) AS TIPO , 
 ( CASE  WHEN ( C2.CTCLS_CLASS2 = '' ) THEN C3.CTCLS_CLASS2  ELSE C2.CTCLS_CLASS2 END ) AS CLASE ,  C3.CTCLS_CLASS2 AS DESCRIPCION  FROM CTCLS C1 
 INNER JOIN CTCLS C2 ON ( C2.CTCLS_SBID = C1.CTCLS_ID )  
 INNER JOIN CTCLS C3 ON ( C3.CTCLS_SBID = C2.CTCLS_ID )) 
 C3  RIGHT JOIN  (SELECT CTPRD_ID,CTPRD_A,CTPRD_ALM_ID,CTPRD_TIPO,CTPRD_CTCLS_ID,CTPRD_CLV,CTPRD_NOM,CTPRD_USR_N,CTPRD_FN FROM CTPRD  ) P1    ON ( C3.IDCATEGORIA = P1.CTPRD_CTCLS_ID ) 
 
SELECT  C1.CTCLS_ID AS IDCATEGORIA, C2.CTCLS_ID AS IDSUBCATEGORIA, C3.CTCLS_ID AS IDCLASE, 
(CASE WHEN ((C1.CTCLS_CLASS2 = '') AND (C2.CTCLS_CLASS2 = '' )) THEN C3.CTCLS_CLASS2 WHEN (C1.CTCLS_CLASS2 = '') THEN C2.CTCLS_CLASS2 ELSE C1.CTCLS_CLASS2 END) AS TIPO, 
(CASE WHEN (C2.CTCLS_CLASS2 = '') THEN C3.CTCLS_CLASS2 ELSE C2.CTCLS_CLASS2 END) AS CLASE, C3.CTCLS_CLASS2 AS DESCRIPCION FROM CTCLS C1 INNER JOIN CTCLS C2 ON (C2.CTCLS_SBID = C1.CTCLS_ID ) INNER JOIN CTCLS C3 ON (C3.CTCLS_SBID = C2.CTCLS_ID) 


