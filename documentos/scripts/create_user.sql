/*
https://tree.nathanfriend.com/?s=(%27options!(%27fancy!true~fullPath9~trazingSlash9~rootDot9)~Z(%27Z%27EditaG3EmpresaG33Kaiju-x7.k5Clave5X5Maz5Password-3xDetaz%3Ax7l5H3Y4Rol%3E-_7V5X5YUI4%3E-6IP8fiVQvo5q*2Qon%20%5B0000000000%5D5Json%3A%20B!B%27%2C%20%5C%27P8miso%5C!%20()-6IMenu7.Label5q25Parent5ToolTip5Form5AssemblyQon5Object%3AInt8face-_Rol7V5kG%27)~v8sion!%271%27)*3W-G*.l-WH2%20%7Bq%7DW_P8fz5-*6%20U7Mode8er9!falseB%5C%27X%5CG%5CnHProp8ties5Q5ActiV.Id42W3%20XNameYList%3CZsource!_%204kIdUs82qKeyx6s8zil%01zxqk_ZYXWVQHGB98765432.-*

Edita
└── Empresa
    └── Kaiju
        ├── UserModel
        │   ├── Properties
        │   │   ├── IdUser (Key)
        │   │   ├── Clave
        │   │   ├── Name
        │   │   ├── Mail
        │   │   └── Password
        │   └── UserDetail: UserModel
        │       └── Properties
        │           └── List<PerfilRol>
        ├── PerfilModel
        │   └── Properties
        │       ├── IdPerfil (Key)
        │       ├── Name
        │       └── List<UIPerfil>
        ├── UIPerfil
        │   └── Properties
        │       ├── IdPerfil (Key)
        │       ├── Activo
        │       ├── Key      (Key)
        │       ├── Action [0000000000]
        │       └── Json: "Name":"Name", "Permiso": {}
        ├── UIMenuModel
        │   └── Properties
        │       ├── Label
        │       ├── Key (Key)
        │       ├── Parent
        │       ├── ToolTip
        │       ├── Form
        │       ├── Assembly
        │       ├── Action
        │       └── Object:Interface
        └── PerfilRolModel
            └── Properties
                ├── IdPerfil (Key)
                └── IdUser (Key)
*/
/*
     TABLA DE USUARIOS DEL SISTEMA (VERSION FIREBIRD) 
     CREACIÓN: 16/12/2022
*/
CREATE TABLE "USER"
(
  USER_ID Integer NOT NULL,
  USER_A Smallint,
  USER_IDROLL Integer,
  USER_IDDIR Integer,
  USER_CLV Varchar(14),
  USER_APIKEY Varchar(32),
  USER_PSW Varchar(64),
  USER_MAIL Varchar(128),
  USER_NOM Varchar(128),
  USER_FN Timestamp,
  USER_USR_N Varchar(10),
  USER_FM Date,
  USER_USR_M Varchar(10),
  CONSTRAINT PK_USER_ID PRIMARY KEY (USER_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id principal de la tabla'  where RDB$FIELD_NAME = 'USER_ID' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'USER_A' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ID de roll'  where RDB$FIELD_NAME = 'USER_IDROLL' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ID del directorio'  where RDB$FIELD_NAME = 'USER_IDDIR' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave unica de registro en sistema'  where RDB$FIELD_NAME = 'USER_CLV' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de acceso al API de forma externa'  where RDB$FIELD_NAME = 'USER_APIKEY' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'password (md5 y base64)'  where RDB$FIELD_NAME = 'USER_PSW' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'correo electronico'  where RDB$FIELD_NAME = 'USER_MAIL' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre'  where RDB$FIELD_NAME = 'USER_NOM' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de sistema'  where RDB$FIELD_NAME = 'USER_FN' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'usuario creo'  where RDB$FIELD_NAME = 'USER_USR_N' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de modificacion'  where RDB$FIELD_NAME = 'USER_FM' and RDB$RELATION_NAME = 'USER';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'usuario modifica'  where RDB$FIELD_NAME = 'USER_USR_M' and RDB$RELATION_NAME = 'USER';
CREATE INDEX IDX_USER_IDDIR ON "USER" (USER_IDDIR);
CREATE INDEX IDX_USER_IDROLL ON "USER" (USER_IDROLL);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de usuarios'
where RDB$RELATION_NAME = 'USER';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON "USER" TO  SYSDBA WITH GRANT OPTION;

/*
      DISPARADOR PARA EL INDICE INCREMENTAL
*/

/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 23112021 1130
    Purpose: Generador para el indice incremental
------------------------------------------------------------------------------------------------------------- */
CREATE GENERATOR GEN_USER_ID;

SET TERM ^ ;
CREATE TRIGGER USER_BI FOR "USER" ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* **********************************************************
    Develop: ANHE1-28/08/2021 00:03
    Purpose: Disparador para indice incremental 
********************************************************** */
  IF (NEW.USER_ID IS NULL) THEN
    NEW.USER_ID = GEN_ID(GEN_USER_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_USER_ID, 0);
    if (tmp < new.USER_ID) then
      tmp = GEN_ID(GEN_USER_ID, new.USER_ID-tmp);
  END
END^
SET TERM ; ^

/*
    PRIMER REGISTRO DE LA TABLA 
*/
INSERT INTO "USER" (USER_ID, USER_A, USER_IDROLL, USER_IDDIR, USER_CLV, USER_APIKEY, USER_PSW, USER_MAIL, USER_NOM, USER_FN, USER_USR_N) 
VALUES ('1', '1', '1', '0', 'ANHE1', NULL, '$2y$10$OVjw0y0oxXtSXXumBGr0HuG93uxFAmSO4eEVXwg75yOisqqlp5mAy', 'sistemas.anhe@ipo.com.mx', 'Antonio Hernández', '23.02.2018, 14:37:49.000', 'ANHE1');


/*
*** TABLA DE USUARIOS DEL SISTEMA (VERSION MARIADB) 
*** CREACIÓN: 16/12/2022
*/
CREATE TABLE `_user` (
	`_user_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
	`_user_idroll` INT(11) NOT NULL DEFAULT '0' COMMENT 'ID de roll',
	`_user_iddir` INT(11) NOT NULL DEFAULT '0' COMMENT 'ID de directorio',
	`_user_a` SMALLINT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`_user_clv` VARCHAR(14) NULL DEFAULT NULL COMMENT 'clave unica de registro en sistema' COLLATE 'utf8_general_ci',
	`_user_apikey` VARCHAR(32) NULL DEFAULT NULL COMMENT 'clave de acceso al API de forma externa' COLLATE 'utf8_general_ci',
	`_user_psw` VARCHAR(64) NOT NULL COMMENT 'password (md5 y base64)' COLLATE 'utf8_general_ci',
	`_user_mail` VARCHAR(128) NOT NULL COMMENT 'correo electronic, separados por (;)' COLLATE 'utf8_general_ci',
	`_user_nom` VARCHAR(128) NOT NULL COMMENT 'nombre' COLLATE 'utf8_general_ci',
	`_user_fn` TIMESTAMP NULL DEFAULT current_timestamp() COMMENT 'fec. sist.',
	`_user_usr_n` VARCHAR(10) NULL DEFAULT NULL COMMENT 'usuario creo' COLLATE 'utf8_general_ci',
	`_user_fm` DATETIME NULL DEFAULT NULL COMMENT 'Fecha de modificacion',
	`_user_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'Usuario modifica' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_user_id`) USING BTREE
)
COMMENT='catalogo de usuarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
*** PRIMER REGISTRO DE LA TABLA
*/
INSERT INTO `_user` (`_user_id`, `_user_idroll`, `_user_iddir`, `_user_a`, `_user_clv`, `_user_apikey`, `_user_psw`, `_user_mail`, `_user_nom`, `_user_fn`, `_user_usr_n`, `_user_fm`, `_user_usr_m`) 
VALUES (1, 1, 0, 1, 'ANHE1', '8969CC9DAFE1DFFC9A55CA7325D121D6', '$2y$10$rJIZADB4Fb6o4CRxpdKZXeV6qBFokrJUBh9bwtLp9evAoqV8XRybe', 'sistemas.anhe@ipo.com.mx', 'Antonio Hernández', '2018-02-23 20:37:49', 'ANHE1', '2024-01-25 14:48:14', 'ANHE1');

