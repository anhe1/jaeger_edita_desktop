CREATE TABLE `RLSUSR` (
	`RLSUSR_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice',
	`RLSUSR_A` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`RLSUSR_M` SMALLINT(6) NOT NULL COMMENT 'marca del usuario maestro',
	`RLSUSR_CLV` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave de rol' COLLATE 'utf8_general_ci',
	`RLSUSR_NMBR` VARCHAR(100) NULL DEFAULT NULL COMMENT 'nombre o descripcion del ROL' COLLATE 'utf8_general_ci',
	`RLSUSR_FN` TIMESTAMP NOT NULL COMMENT 'fecha de creacion del registro',
	`RLSUSR_USR_N` VARCHAR(50) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`RLSUSR_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`RLSUSR_USR_M` VARCHAR(50) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`RLSUSR_ID`) USING BTREE,
	INDEX `RLSUSR_ID` (`RLSUSR_ID`) USING BTREE
)
COMMENT='roles de usuario'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `rlsusr` (`RLSUSR_ID`, `RLSUSR_A`, `RLSUSR_M`, `RLSUSR_CLV`, `RLSUSR_NMBR`, `RLSUSR_FN`, `RLSUSR_USR_N`, `RLSUSR_FM`, `RLSUSR_USR_M`) VALUES (1, 1, 0, 'administrador', 'Administrador', '2022-09-26 22:26:46', NULL, NULL, NULL);
