CREATE TABLE `DRCTRR` (
	`DRCTRR_A` SMALLINT(6) NULL DEFAULT '1' COMMENT 'registro activo',
	`DRCTRR_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con el directorio de clientes',
	`DRCTRR_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del tipo de relacion comercial',
	`DRCTRR_CTCMS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con tabla de comisiones en caso de ser una relacion con vendedor',
	`DRCTRR_CTDSC_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de descuentos en caso de ser cliente',
	`DRCTRR_CTPRC_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de precios en caso de que la relacion sea cliente',
	`DRCTRR_NOTA` VARCHAR(100) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`DRCTRR_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de usuario' COLLATE 'utf8_general_ci',
	`DRCTRR_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'fecha de modificacion',
	UNIQUE INDEX `DRCTRR_DRCTR_ID_DRCTRR_CTREL_ID` (`DRCTRR_DRCTR_ID`, `DRCTRR_CTREL_ID`) USING BTREE,
	INDEX `DRCTRR_DRCTR_ID` (`DRCTRR_DRCTR_ID`) USING BTREE,
	INDEX `DRCTRR_CTREL_ID` (`DRCTRR_CTREL_ID`) USING BTREE
)
COMMENT='directorio: relacion comercial del directorio'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
