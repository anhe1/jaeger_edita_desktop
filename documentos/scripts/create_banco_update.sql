/* ************************************************************************************************************** 
 Bancos: catalogo de formas de pago de los movimientos bancarios
************************************************************************************************************** */

CREATE TABLE `BNCFRM` (
	`BNCFRM_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`BNCFRM_A` INT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCFRM_AFS` INT(1) NOT NULL DEFAULT '1' COMMENT 'afectar saldo de la cuenta',
	`BNCFRM_CLV` VARCHAR(3) NOT NULL COMMENT 'clave de forma de pago' COLLATE 'utf8_general_ci',
	`BNCFRM_CLVS` VARCHAR(3) NOT NULL COMMENT 'clave SAT' COLLATE 'utf8_general_ci',
	`BNCFRM_NOM` VARCHAR(64) NOT NULL COMMENT 'descripcion de la forma de pago' COLLATE 'utf8_general_ci',
	`BNCFRM_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCFRM_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`BNCFRM_FM` DATETIME NULL DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
	`BNCFRM_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCFRM_ID`) USING BTREE
)
COMMENT='Bancos: formas de pago'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

/* ---------------------------------------------------------------------------------------------------------------
 registros iniciales formas de pago
---------------------------------------------------------------------------------------------------------------- */
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (1, b'1', b'1', '05', '01', 'Deposito', '2021-10-30 00:28:09', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (2, b'1', b'1', '02', '02', 'Cheque nominativo', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (3, b'1', b'1', '01', '01', 'Efectivo', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (4, b'1', b'0', '17', '99', 'Comisiones', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (5, b'1', b'0', '12', '99', 'Intercambio', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (6, b'1', b'1', '03', '03', 'Transferencia electrónica de fondos', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (7, b'0', b'1', '02', '02', 'Cheque nominativo', '2021-10-30 00:29:03', 'ANHE1');
INSERT INTO `BNCFRM` (`BNCFRM_ID`, `BNCFRM_A`, `BNCFRM_AFS`, `BNCFRM_CLV`, `BNCFRM_CLVS`, `BNCFRM_NOM`, `BNCFRM_FN`, `BNCFRM_USR_N`) VALUES (8, b'1', b'0', '12', '99', 'Nota de Descuento', '2021-10-30 00:29:03', 'ANHE1');

/* **************************************************************************************************************
 Bancos\Conceptos o tipos de operación: script SQL para crear la tabla de conceptos de movimientos bancarios o tipos de operación
************************************************************************************************************** */

CREATE TABLE `BNCCNP` (
	`BNCCNP_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del concepto',
	`BNCCNP_A` INT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCCNP_R` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'registro de solo lectura',
	`BNCCNP_REQ` INT(1) NOT NULL DEFAULT '0' COMMENT 'requiere comprobante',
	`BNCCNP_AFS` INT(1) NOT NULL DEFAULT '0' COMMENT 'afectactacion de saldos de comprobantes',
	`BNCCNP_IDSBT` INT(11) NOT NULL DEFAULT '0' COMMENT 'sub tipo de comprobante',
	`BNCCNP_STTS_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice del status del comporbante',
	`BNCCNP_IDOP` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de tipo de operacion',
	`BNCCNP_IDTP` INT(11) NOT NULL DEFAULT '1' COMMENT 'indice de tipo o efecto',
	`BNCCNP_IDTPCMB` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de tipo de comprobante',
	`BNCCNP_FRMT_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'nombre del formato impreso',
	`BNCCNP_IVA` DECIMAL(14,4) NOT NULL DEFAULT '0.0000' COMMENT 'porcentaje del iva aplicable a la operacion',
	`BNCCNP_TPCMP` VARCHAR(40) NULL DEFAULT NULL COMMENT 'tipos de comprobante aceptado por el movimiento' COLLATE 'utf8_general_ci',
	`BNCCNP_STATUS` VARCHAR(40) NULL DEFAULT NULL COMMENT 'status de los comprobantes' COLLATE 'utf8_general_ci',
	`BNCCNP_RLCCMP` VARCHAR(40) NULL DEFAULT NULL COMMENT 'lista de indice de relacion entre comprobantes' COLLATE 'utf8_general_ci',
	`BNCCNP_CLASS` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clasificacion' COLLATE 'utf8_general_ci',
	`BNCCNP_DESC` VARCHAR(40) NOT NULL COMMENT 'descipcion del concepto' COLLATE 'utf8_general_ci',
	`BNCCNP_NOM` VARCHAR(40) NOT NULL COMMENT 'nombre, concepto o descripcion' COLLATE 'utf8_general_ci',
	`BNCCNP_CNTA` VARCHAR(40) NULL DEFAULT NULL COMMENT 'cuenta contable' COLLATE 'utf8_general_ci',
	`BNCCNP_RLCDIR` VARCHAR(20) NULL DEFAULT NULL COMMENT 'objetos de relacion del directorio' COLLATE 'utf8_general_ci',
	`BNCCNP_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCCNP_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`BNCCNP_FM` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`BNCCNP_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCCNP_ID`) USING BTREE
)
COMMENT='Bancos: conceptos bancarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=11
;

INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (1, b'1', 0, b'0', b'0', 0, 0, 1, 1, 0, 0, 0, '', NULL, '', NULL, 'Transferencia entre cuentas', 'Transferencia entre cuentas', NULL, '', '2021-11-06 21:07:42', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (2, b'1', 0, b'1', b'1', 1, 4, 4, 1, 0, 0, 0, '2,3', NULL, NULL, NULL, 'Cobro a Cliente', 'Cobro a Cliente', NULL, '1,2,4', '2021-10-22 20:21:18', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (3, b'1', 0, b'1', b'1', 2, 4, 3, 2, 0, 0, 0, '2', NULL, NULL, NULL, 'Emisión de Cheque', 'Emisión de Cheque', NULL, '1,2,3,4,5', '2021-10-22 20:21:24', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (4, b'1', 0, b'1', b'1', 2, 4, 5, 2, 0, 3, 0, '2', NULL, NULL, NULL, 'Pago a Proveedor', 'Pago a Proveedor', NULL, '2', '2021-10-22 20:21:28', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (5, b'1', 0, b'1', b'1', 0, 0, 0, 2, 0, 0, 0, '', NULL, NULL, NULL, 'Pago de Impuestos', 'Pago de Impuestos', NULL, '2', '2021-10-22 20:21:33', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (6, b'1', 0, b'1', b'1', 0, 0, 0, 2, 0, 0, 0, '', NULL, NULL, NULL, 'Pago de Seguro (IMSS)', 'Pago de Seguro (IMSS)', NULL, '2', '2021-10-22 20:21:38', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (7, b'1', 0, b'1', b'1', 0, 0, 0, 2, 0, 0, 0, '', NULL, NULL, NULL, 'Pago de Comisión Bancaria', 'Pago de Comisión Bancaria', NULL, '', '2021-10-22 20:21:43', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (8, b'1', 0, b'1', b'1', 1, 5, 99, 2, 0, 4, 0, '3', NULL, NULL, NULL, 'Pago de Comisión a Vendedor', 'Pago de Comisión a Vendedor', NULL, '4,5', '2021-10-22 20:21:47', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (9, b'1', 0, b'1', b'1', 3, 4, 0, 2, 0, 0, 0, '2', NULL, NULL, NULL, 'Pago de Nómina', 'Pago de Nómina', NULL, '', '2021-10-22 20:22:07', 'ANHE1', NULL, NULL);
INSERT INTO `BNCCNP` (`BNCCNP_ID`, `BNCCNP_A`, `BNCCNP_R`, `BNCCNP_REQ`, `BNCCNP_AFS`, `BNCCNP_IDSBT`, `BNCCNP_STTS_ID`, `BNCCNP_IDOP`, `BNCCNP_IDTP`, `BNCCNP_IDTPCMB`, `BNCCNP_FRMT_ID`, `BNCCNP_IVA`, `BNCCNP_TPCMP`, `BNCCNP_STATUS`, `BNCCNP_RLCCMP`, `BNCCNP_CLASS`, `BNCCNP_DESC`, `BNCCNP_NOM`, `BNCCNP_CNTA`, `BNCCNP_RLCDIR`, `BNCCNP_FN`, `BNCCNP_USR_N`, `BNCCNP_FM`, `BNCCNP_USR_M`) VALUES (10, b'1', 0, b'1', b'1', 0, 0, 0, 2, 0, 0, 0, '', NULL, NULL, NULL, 'Pago de Nómina Eventuales', 'Pago de Nómina Eventuales', NULL, '', '2021-10-22 20:22:13', 'ANHE1', NULL, NULL);

CREATE TABLE `BNCAUX` (
	`BNCAUX_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del movimiento',
	`BNCAUX_A` INT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCAUX_SBID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de relacion con la tabla de movimientos',
	`BNCAUX_DESC` VARCHAR(256) NOT NULL COMMENT 'descripcion del auxiliar' COLLATE 'utf8_general_ci',
	`BNCAUX_NOM` VARCHAR(64) NOT NULL COMMENT 'nombre del archivo' COLLATE 'utf8_general_ci',
	`BNCAUX_TP` VARCHAR(32) NULL DEFAULT NULL COMMENT 'descripcion del movimiento' COLLATE 'utf8_general_ci',
	`BNCAUX_URL` TEXT NULL DEFAULT NULL COMMENT 'url de descarga para el archivo' COLLATE 'utf8_general_ci',
	`BNCAUX_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCAUX_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCAUX_ID`) USING BTREE,
	INDEX `BNCAUX_SBID` (`BNCAUX_SBID`) USING BTREE
)
COMMENT='documentos relacionados a movmientos bancarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `BNCCTA` (
	`BNCCTA_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
	`BNCCTA_A` SMALLINT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCCTA_R` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'registro de solo lectura',
	`BNCCTA_E` SMALLINT(1) NOT NULL DEFAULT '0' COMMENT 'indicar si el banco es extranjero',
	`BNCCTA_DCRT` SMALLINT(2) NOT NULL DEFAULT '1' COMMENT 'dia del corte',
	`BNCCTA_NOCHQ` INT(11) NULL DEFAULT NULL COMMENT 'numero de cheque',
	`BNCCTA_SLDINI` DECIMAL(11,4) NOT NULL DEFAULT '0.0000' COMMENT 'saldo inicial',
	`BNCCTA_CLVB` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave del banco segun catalogo del SAT' COLLATE 'utf8_general_ci',
	`BNCCTA_CLV` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de la moneda a utilizar segun catalgo SAT' COLLATE 'utf8_general_ci',
	`BNCCTA_RFCB` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del beneficiario de la cuenta de banco' COLLATE 'utf8_general_ci',
	`BNCCTA_RFC` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del banco' COLLATE 'utf8_general_ci',
	`BNCCTA_NUMCLI` VARCHAR(14) NULL DEFAULT NULL COMMENT 'numero de identificación del cliente, numero de contrato' COLLATE 'utf8_general_ci',
	`BNCCTA_CLABE` VARCHAR(18) NULL DEFAULT NULL COMMENT 'cuenta clabe' COLLATE 'utf8_general_ci',
	`BNCCTA_NUMCTA` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de cuenta' COLLATE 'utf8_general_ci',
	`BNCCTA_SCRSL` VARCHAR(20) NULL DEFAULT NULL COMMENT 'sucursal bancaria' COLLATE 'utf8_general_ci',
	`BNCCTA_FUNC` VARCHAR(20) NULL DEFAULT NULL COMMENT 'nombre del asesor o funcionario' COLLATE 'utf8_general_ci',
	`BNCCTA_PLAZA` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de plaza' COLLATE 'utf8_general_ci',
	`BNCCTA_TEL` VARCHAR(20) NULL DEFAULT NULL COMMENT 'telefono de contacto' COLLATE 'utf8_general_ci',
	`BNCCTA_ALIAS` VARCHAR(20) NULL DEFAULT NULL COMMENT 'alias de la cuenta' COLLATE 'utf8_general_ci',
	`BNCCTA_CNTA` VARCHAR(40) NULL DEFAULT NULL COMMENT 'numero de cuenta contable' COLLATE 'utf8_general_ci',
	`BNCCTA_BANCO` VARCHAR(64) NULL DEFAULT NULL COMMENT 'nombre del banco' COLLATE 'utf8_general_ci',
	`BNCCTA_BENEF` VARCHAR(256) NULL DEFAULT NULL COMMENT 'nombre del beneficiario de la cuenta de banco' COLLATE 'utf8_general_ci',
	`BNCCTA_FECAPE` DATETIME NULL DEFAULT NULL COMMENT 'fecha de apertura',
	`BNCCTA_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCCTA_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`BNCCTA_FM` DATETIME NULL DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
	`BNCCTA_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCCTA_ID`) USING BTREE
)
COMMENT='Bancos: registro de cuentas bancarias'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

CREATE TABLE `BNCCMP` (
	`BNCCMP_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
	`BNCCMP_A` SMALLINT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCCMP_SUBID` INT(11) NOT NULL COMMENT 'indice de relacion con el movimiento bancario',
	`BNCCMP_NOIDEN` VARCHAR(11) NULL DEFAULT NULL COMMENT 'identificador de la prepoliza' COLLATE 'utf8_general_ci',
	`BNCCMP_VER` VARCHAR(3) NULL DEFAULT NULL COMMENT 'registro activo' COLLATE 'utf8_general_ci',
	`BNCCMP_EFECTO` VARCHAR(10) NOT NULL COMMENT 'efecto del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_SBTP` INT(11) NOT NULL COMMENT 'subtipo del comprobante, emitido, recibido, nomina, etc',
	`BNCCMP_TIPO` VARCHAR(10) NOT NULL COMMENT 'tipo de comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_IDCOM` INT(11) NOT NULL COMMENT 'indice del comprobante fiscal',
	`BNCCMP_FOLIO` VARCHAR(22) NULL DEFAULT NULL COMMENT 'folio del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_SERIE` VARCHAR(25) NULL DEFAULT NULL COMMENT 'serie del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_RFCE` VARCHAR(15) NOT NULL COMMENT 'rfc emisor del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_EMISOR` VARCHAR(255) NOT NULL COMMENT 'nombre del emisor del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_RFCR` VARCHAR(15) NOT NULL COMMENT 'rfc del receptor del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_RECEPTOR` VARCHAR(255) NOT NULL COMMENT 'nombre del receptor del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_FECEMS` DATETIME NOT NULL COMMENT 'fecha de emision del comprobante',
	`BNCCMP_MTDPG` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave del metodo de pago' COLLATE 'utf8_general_ci',
	`BNCCMP_FRMPG` VARCHAR(2) NULL DEFAULT NULL COMMENT 'clave de la forma de pago' COLLATE 'utf8_general_ci',
	`BNCCMP_MONEDA` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de moneda' COLLATE 'utf8_general_ci',
	`BNCCMP_UUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'folio fiscal (uuid)' COLLATE 'utf8_general_ci',
	`BNCCMP_PAR` INT(11) NULL DEFAULT NULL COMMENT 'numero de partida',
	`BNCCMP_TOTAL` DECIMAL(11,4) NOT NULL COMMENT 'total del comprobante',
	`BNCCMP_XCBR` DECIMAL(11,4) NOT NULL COMMENT 'importe por cobrar una vez aplicado el descuento',
	`BNCCMP_CARGO` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'cargo',
	`BNCCMP_ABONO` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'abono',
	`BNCCMP_CBRD` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'acumulado del comprobante',
	`BNCCMP_ESTADO` VARCHAR(10) NULL DEFAULT NULL COMMENT 'ultimo estado del comprobante' COLLATE 'utf8_general_ci',
	`BNCCMP_NOTA` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`BNCCMP_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCCMP_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`BNCCMP_FM` DATETIME NULL DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
	`BNCCMP_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCCMP_ID`) USING BTREE,
	INDEX `BNCCMP_SUBID` (`BNCCMP_SUBID`) USING BTREE,
	INDEX `BNCCMP_IDCOM` (`BNCCMP_IDCOM`) USING BTREE,
	INDEX `BNCCMP_NOIDEN` (`BNCCMP_NOIDEN`) USING BTREE
)
COMMENT='comprobantes relacionados a movmientos bancarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;


CREATE TABLE `BNCSLD` (
	`BNCSLD_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice',
	`BNCSLD_SBID` INT(11) NOT NULL COMMENT 'indice de relacion de la cuenta de bancos.',
	`BNCSLD_ANIO` INT(2) NOT NULL COMMENT 'ejecicio',
	`BNCSLD_MES` INT(2) NOT NULL COMMENT 'periodo',
	`BNCSLD_INICIAL` DECIMAL(14,4) NOT NULL COMMENT 'saldo inicial de la cuenta',
	`BNCSLD_FINAL` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'saldo final de la cuenta',
	`BNCSLD_FS` DATETIME NOT NULL COMMENT 'fecha inicial del saldo',
	`BNCSLD_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del nuevo registro',
	`BNCSLD_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`BNCSLD_NOTA` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nota' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCSLD_ID`) USING BTREE
)
COMMENT='bancos: saldos del control de bancos'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `BNCMOV` (
	`BNCMOV_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del movimiento',
	`BNCMOV_A` INT(1) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`BNCMOV_VER` VARCHAR(3) NOT NULL DEFAULT '3.0' COMMENT 'version del documento' COLLATE 'utf8_general_ci',
	`BNCMOV_STTS_ID` INT(11) NOT NULL DEFAULT '1' COMMENT 'status del movimiento',
	`BNCMOV_TIPO_ID` INT(11) NOT NULL DEFAULT '1' COMMENT 'tipo de movimiento',
	`BNCMOV_OPER_ID` INT(11) NOT NULL DEFAULT '1' COMMENT 'tipo de operacion',
	`BNCMOV_CNCP_ID` INT(11) NOT NULL COMMENT 'indice del concepto de movimiento',
	`BNCMOV_FRMT_ID` INT(11) NOT NULL COMMENT 'indice del concepto de movimiento',
	`BNCMOV_ASCTA` INT(1) NOT NULL COMMENT 'indice del concepto de movimiento',
	`BNCMOV_ASCOM` INT(1) NOT NULL COMMENT 'indice del concepto de movimiento',
	`BNCMOV_DRCTR_ID` INT(11) NOT NULL COMMENT 'indice del directorio',
	`BNCMOV_CTAE_ID` INT(11) NOT NULL COMMENT 'indice de la cuenta de banco a utilizar',
	`BNCMOV_CTAR_ID` INT(11) NOT NULL COMMENT 'indice de la cuenta del banco receptor',
	`BNCMOV_POR` INT(1) NOT NULL COMMENT 'bandera solo para indicar si el gasto es por justificar',
	`BNCMOV_NOIDEN` VARCHAR(11) NULL DEFAULT NULL COMMENT 'identificador de la prepoliza' COLLATE 'utf8_general_ci',
	`BNCMOV_NUMCTAP` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de cuenta' COLLATE 'utf8_general_ci',
	`BNCMOV_CLVBP` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave del banco segun catalogo del SAT' COLLATE 'utf8_general_ci',
	`BNCMOV_FECEMS` DATETIME NULL DEFAULT NULL COMMENT 'fecha de la prepoliza',
	`BNCMOV_FECDOC` DATETIME NULL DEFAULT NULL COMMENT 'fecha del documento',
	`BNCMOV_FCVNC` DATETIME NULL DEFAULT NULL COMMENT 'fecha de vencimiento',
	`BNCMOV_FCCBR` DATETIME NULL DEFAULT NULL COMMENT 'fecha de cobro o pago',
	`BNCMOV_FCLIB` DATETIME NULL DEFAULT NULL COMMENT 'fecha de liberación (transmición al banco)',
	`BNCMOV_FCCNCL` DATETIME NULL DEFAULT NULL COMMENT 'fecha de cancelacion',
	`BNCMOV_BENF` VARCHAR(256) NULL DEFAULT NULL COMMENT 'nombre deel beneficiario' COLLATE 'utf8_general_ci',
	`BNCMOV_RFCR` VARCHAR(15) NULL DEFAULT NULL COMMENT 'registro federal de causante del receptor' COLLATE 'utf8_general_ci',
	`BNCMOV_CLVBT` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave del banco segun catalogo del SAT' COLLATE 'utf8_general_ci',
	`BNCMOV_BANCOT` VARCHAR(255) NULL DEFAULT NULL COMMENT 'numero de sucursal' COLLATE 'utf8_general_ci',
	`BNCMOV_NUMCTAT` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de cuenta' COLLATE 'utf8_general_ci',
	`BNCMOV_CLABET` VARCHAR(18) NULL DEFAULT NULL COMMENT 'Clave Bancaria Estandarizada (CLABE)' COLLATE 'utf8_general_ci',
	`BNCMOV_SCRSLT` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de sucursal' COLLATE 'utf8_general_ci',
	`BNCMOV_CLVFRM` VARCHAR(2) NOT NULL COMMENT 'clave forma de pago' COLLATE 'utf8_general_ci',
	`BNCMOV_FRMPG` VARCHAR(64) NULL DEFAULT NULL COMMENT 'descripcion de la forma de pago o cobro' COLLATE 'utf8_general_ci',
	`BNCMOV_NODOCTO` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de documento' COLLATE 'utf8_general_ci',
	`BNCMOV_DESC` VARCHAR(256) NOT NULL COMMENT 'descripcion del movimiento' COLLATE 'utf8_general_ci',
	`BNCMOV_REF` VARCHAR(24) NULL DEFAULT NULL COMMENT 'referencia alfanumerica' COLLATE 'utf8_general_ci',
	`BNCMOV_NUMAUTO` VARCHAR(24) NULL DEFAULT NULL COMMENT 'numero de autorizacion' COLLATE 'utf8_general_ci',
	`BNCMOV_NUMOPE` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de operacion' COLLATE 'utf8_general_ci',
	`BNCMOV_REFNUM` VARCHAR(20) NULL DEFAULT NULL COMMENT 'referencia numerica' COLLATE 'utf8_general_ci',
	`BNCMOV_TPCMB` DECIMAL(4,2) NOT NULL COMMENT 'tipo de cambio',
	`BNCMOV_CARGO` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'cargo / retiro',
	`BNCMOV_ABONO` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'abono / deposito',
	`BNCMOV_CLVMND` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave SAT de moneda' COLLATE 'utf8_general_ci',
	`BNCMOV_NOTA` VARCHAR(64) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`BNCMOV_USR_C` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que cancela el comprobante' COLLATE 'utf8_general_ci',
	`BNCMOV_USR_A` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que cancela' COLLATE 'utf8_general_ci',
	`BNCMOV_FN` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`BNCMOV_USR_N` VARCHAR(10) NOT NULL COMMENT 'clave del usuario que creo el registro' COLLATE 'utf8_general_ci',
	`BNCMOV_FM` DATETIME NULL DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
	`BNCMOV_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`BNCMOV_JSON` TEXT NULL DEFAULT NULL COMMENT 'json' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`BNCMOV_ID`) USING BTREE,
	INDEX `BNCMOV_OPER_ID` (`BNCMOV_OPER_ID`) USING BTREE,
	INDEX `BNCMOV_CNCP_ID` (`BNCMOV_CNCP_ID`) USING BTREE,
	INDEX `BNCMOV_FRMT_ID` (`BNCMOV_FRMT_ID`) USING BTREE,
	INDEX `BNCMOV_DRCTR_ID` (`BNCMOV_DRCTR_ID`) USING BTREE,
	INDEX `BNCMOV_CTAE_ID` (`BNCMOV_CTAE_ID`) USING BTREE,
	INDEX `BNCMOV_CTAR_ID` (`BNCMOV_CTAR_ID`) USING BTREE,
	INDEX `BNCMOV_STTS_ID` (`BNCMOV_STTS_ID`) USING BTREE,
	INDEX `BNCMOV_NOIDEN` (`BNCMOV_NOIDEN`) USING BTREE
)
COMMENT='movimientos bancarios'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

-- eliminar contenido de las tablas
TRUNCATE `BNCMOV`;
TRUNCATE `BNCCMP`;
/* --------------------------------------------------------------------------------------------------------------------------------
 Bancos\Movimientos: importacion de datos de la tabla anterior
-------------------------------------------------------------------------------------------------------------------------------- */
INSERT INTO `BNCMOV` (`bncmov_id`, `bncmov_a`, `bncmov_stts_id`, `bncmov_oper_id`, `bncmov_tipo_id`, `bncmov_cncp_id`, `bncmov_frmt_id`, `bncmov_ascta`, `bncmov_ascom`, `bncmov_drctr_id`, `bncmov_ctae_id`, `bncmov_ctar_id`, `bncmov_por`, `bncmov_noiden`, `bncmov_numctap`, `bncmov_clvbp`, `bncmov_fecems`, `bncmov_fecdoc`, `bncmov_fcvnc`, `bncmov_fccbr`, `bncmov_fclib`, `bncmov_fccncl`, `bncmov_benf`, `bncmov_rfcr`, `bncmov_clvbt`, `bncmov_bancot`, `bncmov_numctat`, `bncmov_clabet`, `bncmov_scrslt`, `bncmov_clvfrm`, `bncmov_frmpg`, `bncmov_nodocto`, `bncmov_desc`, `bncmov_ref`, `bncmov_numauto`, `bncmov_numope`, `bncmov_refnum`, `bncmov_tpcmb`, `bncmov_cargo`, `bncmov_abono`, `bncmov_clvmnd`, `bncmov_nota`, `bncmov_usr_c`, `bncmov_usr_a`, `bncmov_fn`, `bncmov_usr_n`, `bncmov_fm`, `bncmov_usr_m`, `bncmov_json`) 
SELECT _cntbl3._cntbl3_id _bncmov_id,
_cntbl3._cntbl3_a _bncmov_a,
(case 
when _cntbl3._cntbl3_status = "Cancelado" then 0
when _cntbl3._cntbl3_status = "NoAplicado" then 1
when _cntbl3._cntbl3_status = "Aplicado" then 2
when _cntbl3._cntbl3_status = "Auditado" then 3
else 0 end) _bncmov_stts_id,
(case 
when _cntbl3._cntbl3_tipo = "Ingreso" then 4
when _cntbl3._cntbl3_tipo = "Egreso" then 5
when _cntbl3._cntbl3_tipo = "Diario" then 1
end) _bncmov_oper_id,
(case 
when _cntbl3._cntbl3_tipo = "Ingreso" then 1
when _cntbl3._cntbl3_tipo = "Egreso" then 2
when _cntbl3._cntbl3_tipo = "Diario" then 2
end) _bncmov_tipo_id, 
(case 
when _cntbl3._cntbl3_tipo = "Ingreso" then 2
when _cntbl3._cntbl3_tipo = "Egreso" then 4
when _cntbl3._cntbl3_tipo = "Diario" then 1
end) _bncmov_cncp_id,
0 _bncmov_frmt_id, 
1 _bncmov_ascta, 
1 _bncmov_ascom, 
0 _bncmov_drctr_id, 
_cntbl3._cntbl3_sbid _bncmov_ctae_id, 
0 _bncmov_ctar_id, 
_cntbl3._cntbl3_por _bncmov_por, 
_cntbl3._cntbl3_noiden _bncmov_noiden, 
_cntbl3._cntbl3_nmctae _bncmov_numctap, 
_cntbl3._cntbl3_bncclve _bncmov_clvbp, 
_cntbl3._cntbl3_fecems _bncmov_fecems, 
_cntbl3._cntbl3_fecdoc _bncmov_fecdoc, 
_cntbl3._cntbl3_fcvnc _bncmov_fcvnc, 
_cntbl3._cntbl3_fccbr _bncmov_fccbr, 
_cntbl3._cntbl3_fclib _bncmov_fclib, 
_cntbl3._cntbl3_fccncl _bncmov_fccncl, 
_cntbl3._cntbl3_benef _bncmov_benf, 
_cntbl3._cntbl3_rfcr _bncmov_rfcr, 
_cntbl3._cntbl3_bncclvr _bncmov_clvbt, 
_cntbl3._cntbl3_bancor _bncmov_bancot, 
_cntbl3._cntbl3_nmctar _bncmov_numctat, 
SUBSTRING(_cntbl3._cntbl3_clbr, 1, 18) _bncmov_clabet, 
_cntbl3._cntbl3_scrslr _bncmov_scrslt, 
(case when _cntbl3._cntbl3_frmclv IS NULL then '99' ELSE _cntbl3._cntbl3_frmclv end) _bncmov_clvfrm, 
_cntbl3._cntbl3_frmpg  _bncmov_frmpg, 
_cntbl3._cntbl3_nodocto _bncmov_nodocto, 
_cntbl3._cntbl3_cncpt _bncmov_desc, 
_cntbl3._cntbl3_ref _bncmov_ref, 
_cntbl3._cntbl3_numauto _bncmov_numauto, 
null _bncmov_numope, 
null _bncmov_refnum, 
1 _bncmov_tpcmb, 
_cntbl3._cntbl3_cargo _bncmov_cargo, 
_cntbl3._cntbl3_abono _bncmov_abono,
'MXN' _bncmov_clvmnd, 
_cntbl3._cntbl3_nota _bncmov_nota, 
_cntbl3._cntbl3_usr_c _bncmov_usr_c, 
_cntbl3._cntbl3_usr_a _bncmov_usr_a, 
_cntbl3._cntbl3_fn _bncmov_fn, 
_cntbl3._cntbl3_usr_n _bncmov_usr_n, 
_cntbl3._cntbl3_fm _bncmov_fm, 
_cntbl3._cntbl3_usr_m _bncmov_usr_m, 
_cntbl3._cntbl3_json _bncmov_json
FROM _cntbl3
ORDER BY _cntbl3._cntbl3_id asc

/* --------------------------------------------------------------------------------------------------------------------------------
 Bancos\Movimientos: importacion de datos de la tabla anterior de comporbantes relacionados al movimiento bancario
-------------------------------------------------------------------------------------------------------------------------------- */
INSERT INTO `BNCCMP` (`BNCCMP_ID`, `BNCCMP_A`, `BNCCMP_SUBID`, `BNCCMP_NOIDEN`, `BNCCMP_VER`, `BNCCMP_EFECTO`, `BNCCMP_SBTP`, `BNCCMP_TIPO`, `BNCCMP_IDCOM`, `BNCCMP_FOLIO`, `BNCCMP_SERIE`, `BNCCMP_RFCE`, 
`BNCCMP_EMISOR`, `BNCCMP_RFCR`, `BNCCMP_RECEPTOR`, `BNCCMP_FECEMS`, `BNCCMP_MTDPG`, `BNCCMP_FRMPG`, `BNCCMP_MONEDA`, `BNCCMP_UUID`, `BNCCMP_PAR`, `BNCCMP_TOTAL`, `BNCCMP_XCBR`, `BNCCMP_CARGO`, `BNCCMP_ABONO`, 
`BNCCMP_CBRD`, `BNCCMP_ESTADO`, `BNCCMP_USR_N`, `BNCCMP_FN`, `BNCCMP_USR_M`, `BNCCMP_FM`) 
SELECT 
_cntbl3c._cntbl3c_id _bnccmp_id, 
_cntbl3c._cntbl3c_a _bnccmp_a,
_cntbl3c._cntbl3c_subId _bnccmp_subid,
_cntbl3c._cntbl3c_noiden _bnccmp_noiden,
_cntbl3c._cntbl3c_ver _bnccmp_ver,
(case when _cfdi._cfdi_efecto IS NULL then '' ELSE _cfdi._cfdi_efecto END) _bnccmp_efecto,
_cntbl3c._cntbl3c_sbtp _bnccmp_sbtp,
_cntbl3c._cntbl3c_tipo _bnccmp_tipo,
_cntbl3c._cntbl3c_idcom _bnccmp_idcom,
(case when _cntbl3c._cntbl3c_folio IS NULL then '' ELSE _cntbl3c._cntbl3c_folio END) _bnccmp_folio,
(case when _cntbl3c._cntbl3c_serie IS NULL then '' ELSE _cntbl3c._cntbl3c_serie END) _bnccmp_serie,
_cntbl3c._cntbl3c_rfce _bnccmp_rfce,
_cntbl3c._cntbl3c_nome _bnccmp_emisor,
_cntbl3c._cntbl3c_rfcr _bnccmp_rfcr,
(case when _cfdi._cfdi_nomr IS NULL then '' ELSE _cfdi._cfdi_nomr END) _bnccmp_receptor,
_cntbl3c._cntbl3c_fecems _bnccmp_fecems,
(case when _cfdi._cfdi_mtdpg IS NULL then '' ELSE substring(_cfdi._cfdi_mtdpg, 1, 3) END) _bnccmp_mtdpg,
(case when _cfdi._cfdi_frmpg is null then '' else substring(_cfdi._cfdi_frmpg, 1, 2) END) _bnccmp_frmpg,
substring(_cfdi._cfdi_moneda, 1, 3) _bnccmp_moneda,
(case when _cfdi._cfdi_uuid is null then '' else _cfdi._cfdi_uuid END) _bnccmp_uuid,
0 _bnccmp_par,
_cntbl3c._cntbl3c_total _bnccmp_total,
_cntbl3c._cntbl3c_total _bnccmp_xcbr,
_cntbl3c._cntbl3c_cargo _bnccmp_cargo,
_cntbl3c._cntbl3c_abono _bnccmp_abono,
_cntbl3c._cntbl3c_acumulado _bnccmp_cbrd,
_cntbl3c._cntbl3c_estado _bnccmp_estado,
(case when _cntbl3c._cntbl3c_usr_n IS NULL then 'SYSDBA' ELSE _cntbl3c._cntbl3c_usr_n END) _bnccmp_usr_n,
_cntbl3c._cntbl3c_fn _bnccmp_fn,
_cntbl3c._cntbl3c_usr_m _bnccmp_usr_m,
_cntbl3c._cntbl3c_fm _bnccmp_fm
FROM _cntbl3c
LEFT JOIN _cfdi ON _cntbl3c._cntbl3c_idcom = _cfdi._cfdi_id

-- adecuaciones para la implementacion de esta version del banco
ALTER TABLE `BNCMOV` CHANGE COLUMN `BNCMOV_DRCTR_ID` `BNCMOV_DRCTR_ID` INT(11) NULL COMMENT 'indice del directorio' AFTER `BNCMOV_ASCOM`;
/* actualizar indice del directorio */
UPDATE BNCMOV SET BNCMOV.BNCMOV_DRCTR_ID = (SELECT 
case 
when _drctr._drctr_id IS NULL then 0
ELSE _drctr._drctr_id end
from _drctr 
where _drctr._drctr_rfc = BNCMOV.BNCMOV_RFCR LIMIT 1)
WHERE BNCMOV.BNCMOV_RFCR IS NOT NULL 

-- ultima modificacion
ALTER TABLE `BNCMOV` CHANGE COLUMN `BNCMOV_DRCTR_ID` `BNCMOV_DRCTR_ID` INT(11) NOT NULL COMMENT 'indice del directorio' AFTER `BNCMOV_ASCOM`;