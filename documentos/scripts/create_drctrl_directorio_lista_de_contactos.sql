CREATE TABLE `_drctrl` (
	`_drctrl_id` INT(11) NOT NULL COMMENT 'indice de la tabla',
	`_drctrl_a` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`_drctrl_drctr_id` INT(11) NOT NULL COMMENT 'indice de relacion con la tabla del directorio',
	`_drctrl_ttel` VARCHAR(16) NULL DEFAULT NULL COMMENT 'tipo de telefono del contacto' COLLATE 'utf8_general_ci',
	`_drctrl_tmail` VARCHAR(16) NULL DEFAULT NULL COMMENT 'tipo de correo laboral, personal' COLLATE 'utf8_general_ci',
	`_drctrl_sug` VARCHAR(32) NULL DEFAULT NULL COMMENT 'trato sugerido' COLLATE 'utf8_general_ci',
	`_drctrl_nom` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre del contacto' COLLATE 'utf8_general_ci',
	`_drctrl_pst` VARCHAR(128) NULL DEFAULT NULL COMMENT 'puesto' COLLATE 'utf8_general_ci',
	`_drctrl_tel` VARCHAR(64) NULL DEFAULT NULL COMMENT 'numero telefonico del contacto' COLLATE 'utf8_general_ci',
	`_drctrl_mail` VARCHAR(64) NULL DEFAULT NULL COMMENT 'correo electronico' COLLATE 'utf8_general_ci',
	`_drctrl_nota` VARCHAR(128) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`_drctrl_usr_n` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro ' COLLATE 'utf8_general_ci',
	`_drctrl_fn` TIMESTAMP NOT NULL COMMENT 'fecha de creacion del registro',
	`_drctrl_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`_drctrl_fm` TIMESTAMP NULL DEFAULT NULL COMMENT '\nultima fecha de modificacion del registro'
)
COMMENT='directorio: lista de contactos relacionados'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
