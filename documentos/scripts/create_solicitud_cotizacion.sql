/* 
    Adquisiciones: Tablas de solicitud de cotizaciones a proveedor
*/
CREATE TABLE `_ordcms` (
	`_ordcms_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice',
	`_ordcms_ver` VARCHAR(3) NOT NULL COMMENT 'version del comprobante' COLLATE 'utf8_general_ci',
	`_ordcms_folio` INT(11) NOT NULL COMMENT 'folio de la orden de compra',
	`_ordcms_sre` VARCHAR(10) NULL DEFAULT NULL COMMENT 'serie' COLLATE 'utf8_general_ci',
	`_ordcms_stts` INT(1) NOT NULL COMMENT 'estado del comprobante',
	`_ordcms_drctr_id` INT(11) NOT NULL COMMENT 'indice del directorio de proveedores',
	`_ordcms_nom` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre del proveedor' COLLATE 'utf8_general_ci',
	`_ordcms_rfc` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del proveedor' COLLATE 'utf8_general_ci',
	`_ordcms_cntc` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre del contacto del proveedor' COLLATE 'utf8_general_ci',
	`_ordcms_cmail` VARCHAR(128) NULL DEFAULT NULL COMMENT 'correo del contacto' COLLATE 'utf8_general_ci',
	`_ordcms_fn` DATETIME NOT NULL COMMENT 'fecha del sistema',
	`_ordcms_fecord` DATETIME NULL DEFAULT NULL COMMENT 'fecha de recepcion',
	`_ordcms_freq` DATETIME NULL DEFAULT NULL COMMENT 'fecha requerida',
	`_ordcms_nota` VARCHAR(128) NULL DEFAULT NULL COMMENT 'notas' COLLATE 'utf8_general_ci',
	`_ordcms_mtdenv` VARCHAR(128) NULL DEFAULT NULL COMMENT 'metodo del envio' COLLATE 'utf8_general_ci',
	`_ordcms_dent` TEXT NULL DEFAULT NULL COMMENT 'direccion de entrega' COLLATE 'utf8_general_ci',
	`_ordcms_usr_n` VARCHAR(10) NOT NULL COMMENT 'clave del usuario' COLLATE 'utf8_general_ci',
	`_ordcms_fm` DATETIME NULL DEFAULT NULL COMMENT 'fecha de modificacion',
	`_ordcms_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifico el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_ordcms_id`) USING BTREE,
	INDEX `_ordcms_drctr_id` (`_ordcms_drctr_id`) USING BTREE
)
COMMENT='proveedor: solicitud de cotizacion'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;


CREATE TABLE `_ordcns` (
	`_ordcns_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`_ordcns_a` BIT(1) NOT NULL COMMENT 'obtener o establecer si el registro se encuentra activo',
	`_ordcns_sbid` INT(11) NOT NULL COMMENT 'obtener o establecer el indice de relacion con la orden de compra',
	`_ordcns_alm_id` INT(11) NOT NULL COMMENT 'obtener o establecer el almacen al que pertenece',
	`_ordcns_tipo` INT(11) NOT NULL COMMENT 'obtener o establcer el tipo de producto (producto o servicio)',
	`_ordcns_prd_id` INT(11) NOT NULL COMMENT 'obtener o establecer el indice del producto o servicio',
	`_ordcns_mdl_id` INT(11) NOT NULL COMMENT 'obtener o establecer el indice del modelo',
	`_ordcns_undd_id` INT(11) NOT NULL COMMENT 'obtener o establecer la unidad de almacen',
	`_ordcns_cant` DECIMAL(14,4) NOT NULL COMMENT 'obtener o establecer la cantidad requerida del producto o servicio',
	`_ordcns_undd` VARCHAR(20) NULL DEFAULT NULL COMMENT 'obtener o establecer la unidad del almacen' COLLATE 'utf8_general_ci',
	`_ordcns_prdct` VARCHAR(200) NULL DEFAULT NULL COMMENT 'obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.' COLLATE 'utf8_general_ci',
	`_ordcns_dscrc` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'obtener o establecer la descripción del bien o servicio cubierto por el presente concepto.' COLLATE 'utf8_general_ci',
	`_ordcns_espc` VARCHAR(128) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| desc: espcificaciones' COLLATE 'utf8_general_ci',
	`_ordcns_mrc` VARCHAR(128) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| desc: marca o fabricante' COLLATE 'utf8_general_ci',
	`_ordcns_nota` VARCHAR(128) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| observaciones' COLLATE 'utf8_general_ci',
	`_ordcns_fecreq` DATETIME NULL DEFAULT NULL COMMENT 'obtener o establecer la fecha de requerimiento',
	`_ordcns_fn` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
	`_ordcns_usr_n` VARCHAR(255) NULL DEFAULT NULL COMMENT 'clave del usuario que crea el registro' COLLATE 'utf8_general_ci',
	`_ordcns_fm` DATETIME NULL DEFAULT NULL COMMENT 'fecha de creacion del registro',
	`_ordcns_usr_m` VARCHAR(255) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_ordcns_id`) USING BTREE,
	INDEX `_ordcns_sbid` (`_ordcns_sbid`) USING BTREE,
	INDEX `_ordcns_alm_id` (`_ordcns_alm_id`) USING BTREE,
	INDEX `_ordcns_mdl_id` (`_ordcns_mdl_id`) USING BTREE,
	INDEX `_ordcns_prd_id` (`_ordcns_prd_id`) USING BTREE,
	INDEX `_ordcns_undd_id` (`_ordcns_undd_id`) USING BTREE
)
COMMENT='proveedor: partidas solicitud de cotizaciones'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

