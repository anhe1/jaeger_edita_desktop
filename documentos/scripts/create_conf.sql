/* *** VERSION ANTERIOR *** */
CREATE TABLE CONF
(
  CONF_ID Integer DEFAULT 0 NOT NULL,
  CONF_KEY Varchar(20),
  CONF_DATA Varchar(8000),
  CONSTRAINT PK_CONF_ID PRIMARY KEY (CONF_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CONF_ID' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'llave de la configuracion'  where RDB$FIELD_NAME = 'CONF_KEY' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'datos de la configuracion formato json'  where RDB$FIELD_NAME = 'CONF_DATA' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'configuraciones de la empresa'
where RDB$RELATION_NAME = 'CONF';

/*
*** tabla de configuraciones (version mysql)
*/
CREATE TABLE `CONF` (
	`CONF_KEY` INT(11) NOT NULL DEFAULT '0' COMMENT 'llave',
	`CONF_GRP` INT(11) NOT NULL DEFAULT '0' COMMENT 'grupo',
	`CONF_DATA` VARCHAR(500) NULL DEFAULT NULL COMMENT 'configuracion o data' COLLATE 'utf8_general_ci',
	`CONF_NOTA` VARCHAR(50) NULL DEFAULT NULL COMMENT 'nota o comentario' COLLATE 'utf8_general_ci',
	`CONF_FM` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`CONF_KEY`, `CONF_GRP`) USING BTREE
)
COMMENT='configuraciones de la empresa'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/* *** CONSULTA PARA INSERTAR O ACTUALIZAR LOS VALORES DE LA TABLA */
INSERT INTO CONF (CONF_KEY, CONF_GRP, CONF_DATA) VALUES('{0}',{1},'{2}') ON DUPLICATE KEY UPDATE CONF_DATA = '{3}', CONF_KEY = '{0}', CONF_GRP={1}

/*
*** tabla de configuraciones (version firebird)
*/
CREATE TABLE CONF
(
  CONF_KEY Integer DEFAULT 0 NOT NULL,
  CONF_GRP Integer DEFAULT 0 NOT NULL,
  CONF_DATA Varchar(500),
  CONF_NOTA Varchar(50) DEFAULT '',
  CONF_FM Timestamp DEFAULT CURRENT_TIMESTAMP  NOT NULL,
  CONSTRAINT PK_CONF_1 PRIMARY KEY (CONF_KEY,CONF_GRP)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de llave'  where RDB$FIELD_NAME = 'CONF_KEY' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero o clave de grupo'  where RDB$FIELD_NAME = 'CONF_GRP' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'datos de la configuracion'  where RDB$FIELD_NAME = 'CONF_DATA' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'configuraciones de la empresa'
where RDB$RELATION_NAME = 'CONF';

/* *** CONSULTA PARA INSERTAR O ACTUALIZAR LOS VALORES DE LA TABLA */
UPDATE OR INSERT INTO CONF (CONF_KEY, CONF_GRP, CONF_DATA, CONF_NOTA, CONF_FM) 
                    VALUES ('1', '3', 'IPR981125PN9', 'NOTA', CURRENT_TIMESTAMP) MATCHING(CONF_KEY, CONF_GRP) 
					RETURNING CONF_KEY, CONF_GRP;

/* ********************************************************************************************** *
Categorías o Grupos
----------------------------------------------
Administrador       = 00
Contribuyente       = 01
Domicilio Fiscal    = 02
CFDI                = 03
CFDRetencion        = 04
Nomina              = 05
Carta Porte         = 06
Recepcion de Pago   = 07
Validador           = 08
Adquisiciones       = 09
Bancos              = 10
Almacenes           = 11
Tienda              = 12
Servicio Paypal     = 13
Serv. Mercado Pago  = 14
Redes Sociales      = 15
Almacén PT          = 16
Almacén MP          = 17
Almacén PP          = 18
Remisión PT         = 19
Repositorio         = 21 
Sessión SAT         = 22
Repositorio         = 23
Directorio          = 24
Bucket AWS          = 92
Serv. PAC           = 93
Menús               = 94
Perfiles            = 95
Usuarios            = 96
Configuracion       = 97
CP                  = 98
CPLite              = 99

 Llave (KEY)         | CV | Descripción
---------------------|----|----------------------------------------------------------------------------------
Configuracion        | 00 | configuracion generica 
RFC                  | 01 | Registro federal de contribuyentes, personas físicas está compuesta de 13 caracteres y personas morales por 12 caracteres:
CURP                 | 02 | Clave Única de Registro de Población, contiene 18 elementos de un código alfanumérico;
Razon Social         | 03 | Nombre o Razón Social
Nombre Comercial     | 04 | Nombre Comercial
Sociedad Capital     | 05 | Sociedad Captial
Regimen Fiscal       | 06 | Clave de Régimen Fiscal
Uso de CFDI          | 07 | Clave de uso de CFDI
Registro Patronal    | 08 | Registro Patronal
CIF                  | 09 | numero de Cedula de Identificación Fiscal
CIF Url              | 10 | url bucketName = '{rfc-Emisor}/documentos/{cif-{numero-rfc-yyyyMMdd-hhrrss}.pdf'
Correo Electrónico   | 11 | correo del administrador
Dominio              | 12 | nombre de sub dominio asignado en edita
Sub Dominio          | 13 | sub dominio
Peronsa Tipo         | 14 | Fisica / Moral
Tipo Vialidad        | 15 | tipo de vialidad calle, avenida, etc.
Calle o Nom. Vialidad| 16 | nombre de calle o vialidad
Codigo Postal        | 17 | codigo postal de 5 digitos
Colonia	             | 18 | colonia
Num. Exterior        | 19 | numero exterior
Num. Interior        | 20 | numero interior
Municipio Delegacion | 21 | delegacion o municipio
Estado               | 22 | estado, entidad federativa
Codigo de País       | 23 | codigo de pais
País                 | 24 | país
Ciudad               | 25 | ciudad
Telefono             | 26 | telefono
Referencia           | 27 | referencia
Localidad            | 28 | localidad
Lugar de Expedición  | 29 | Codigo postal del lugar de expedición del comprobante fiscal (dato utilizado en la emisión de comprobantes)
Ocultar Percepcion   | 30 | array de claves {"Clv":"Clv"}
Ocultar Deduccion    | 31 | array de claves {"Clv":"Clv"}
Ocultar OtrosPagos   | 32 | array de claves {"Clv":"Clv"}
Token                | 33 | llave o token de acceso
User ID              | 34 | nombre usuario, correo, accessKey del servicio utilizado
Password             | 35 | contraseña, password
Modo Productivo      | 36 | bandera para indicar modo productivo del servicio (0=Falso, 1 = Verdadero
Facebook             | 37 | https://www.facebook.com/Somos-IP-107801364827683/
Youtube              | 38 | https://www.youtube.com/channel/UCd_WGpQe3mUOLE3IQnTeeRA
Telegram             | 39 | https://t.me/SomosIP
Whatsapp             | 40 | numero de whatsapp
Cuentas bancarias    | 41 | array de cuenta bancaria {"cuenta":"", "sucursal":"", "cliente": "", "":""}
Horarios de atención | 42 | {"label":"Horario de atención", "valor": "Lun.-Vie. 9:00 am a 12:00"}
Moneda               | 43 | clave de moneda a utilizar en la facturación (ejemplo (MXN)
Proveedor PAC        | 44 | Proveedor de Cerificación
Url Update           | 45 | https://
Solucion Factible    | 46 | solucion factible
Configuracion BD     | 47 | {"database":"baseDatos","host":"ipo.mx","port":3050,"charset":null,"pooling":false,"pagesize":0,"forcewrite":false,"user":"sysdba","pass":"password"}
Pagare               | 48 | información de pagaré
Leyenda              | 49 |
Expresion            | 50 |
Folder               | 51 | nombre de folder
Templete             | 52 |
View                 | 53 |
Decimales            | 54 | numero de decimales
Redondeo             | 55 | 
Ruta (Path)          | 56 | representa ruta local
ReciboCobroID        | 57 | indice del movimiento aplicado al recibo de cobro 
ReciboPagoID         | 58 | indice del movimiento aplicado al recibo de cobropago
ReciboComisionID     | 59 | indice del movimiento aplicado al recibo de comisiones
Chat ID              | 60 | identificador del chat (Telegram)
PublicKey            | 61 | Llave pública
Instagram            | 62 | https://www.instagram.com
Pinterest            | 63 | https://www.pinterest.com
Twitter (X)          | 64 | https://www.twitter.com
Google Maps          | 65 | https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.0868839334917!2d-99.06421897744953!3d19.408651634964574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fc5f24f15bf9%3A0x9f42a241a18456c0!2sCalle%206%20208C%2C%20Agr%C3%ADcola%20Pantitl%C3%A1n%2C%20Iztacalco%2C%2008100%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX!5e0!3m2!1ses!2smx!4v1717005431152!5m2!1ses!2smx
Region               | 66 | Region de AWS (us-east-1)

Version              | 99 | versión 

-------------------------------------------------------------------------------------------------------------

 Llave (KEY)         | CV | GRP| Valor (500 caracteres)
---------------------|----|----------------------------------------------------------------------------------
 Datos del contribuyente Emisor
---------------------|----|----------------------------------------------------------------------------------
rfc                  |  1 |  1 | Registro federal de contribuyentes, personas físicas está compuesta de 13 caracteres y personas morales por 12 caracteres:
curp                 |  2 |  1 | 
Razon Social         |  2 |  1 | Razón Social
Nombre Comercial     |  3 |  1 | Nombre Comercial
Sociedad Capital     |  4 |  1 | Sociedad Captial
regimenFiscal        |  5 |  1 | Clave de Régimen Fiscal
cif                  | 09 |  1 | numero de Cedula de Identificación Fiscal
cifurl               | 10 |  1 | url bucketName = 'Emisor/documentos'
correo               | 11 |  1 | correo del administrador
Persona Tipo         | 42 |  1 | Fisica / Moral
registroPatronal     | 19 |  1 | Registro Patronal
Dominio              | 11 |  1 | 
Persona Tipo         | 12 |  1 | Fisica / Moral
---------------------|----|----------------------------------------------------------------------------------
Domicilio Fiscal     |    |  2 |
---------------------|----|----------------------------------------------------------------------------------
calle        contraseña        |  9 |  2 | nombre de calle o vialidad
codigoPostal         | 10 |  2 | codigo postal de 5 digitos
colonia              | 11 |  2 | colonia
numExterior          | 12 |  2 | numero exterior
numInterior          | 13 |  2 | numero interior
municipioDelegacion  | 14 |  2 | delegacion o municipio
estado               | 15 |  2 | estado, entidad federativa
telefono             | 16 |  2 | telefono
referencia           | 17 |  2 | referencia
localidad            | 18 |  2 | localidad
---------------------|----|----------------------------------------------------------------------------------
Servicio PAC         |    | 93 |
---------------------|----|----------------------------------------------------------------------------------
Proveedor PAC        | 38 | 93 | Proveedor de Cerificación
User ID              | 27 | 93 | nombre usuario, correo, accessKey del servicio utilizado
Password             | 28 | 93 | contraseña, password
Modo Productivo      | 29 | 93 | bandera para indicar modo productivo del servicio (0=Falso, 1 = Verdadero
---------------------|----|----------------------------------------------------------------------------------
Nominas              |    |  5 |
---------------------|----|----------------------------------------------------------------------------------
Configuracion BD     | 47 |  5 | {"database":"baseDatos","host":"ipo.mx","port":3050,"charset":null,"pooling":false,"pagesize":0,"forcewrite":false,"user":"sysdba","pass":"password"}
modoProductivo       | 32 |  5 | bandera para indicar modo productivo del servicio (0=Falso, 1 = Verdadero
Folder               | 51 | 05 | nombre de folder utilizado en bucket de amazon
ocultarPercepcion    | 20 |  5 | {}
ocultarDeduccion     | 21 |  5 | {}
ocultarOtrosPagos    | 22 |  5 | {}
---------------------|----|----------------------------------------------------------------------------------
Validación de comprobantes| 08 |
configuracion        | 00 | 08 | {"v":[000000000000000],CP[Regex]}
codigoPostal         | 10 | 08 | 
ExpresionRex         | 50 | 08 | expresion regular para validar lugar de expedicion del comprobante version 3.3
---------------------|----|----------------------------------------------------------------------------------
 BANCOS              |    | 10 |
---------------------|----|----------------------------------------------------------------------------------
Nombre Comercial     |  3 | 10 |
folder               | 51 | 10 |
decimales            | 54 | 10 |
redondeo             | 55 | 10 |
reciboCobroID        | 57 | 10 | indice del movimiento aplicado al recibo de cobro 
reciboPagoID         | 58 | 10 | indice del movimiento aplicado al recibo de cobropago
reciboComisionID     | 59 | 10 | indice del movimiento aplicado al recibo de comisiones
---------------------|----|----------------------------------------------------------------------------------
Servicio PayPal      |    | 13 |
---------------------|----|----------------------------------------------------------------------------------
User ID              | 34 | 13 | nombre usuario, correo, accessKey del servicio utilizado (clientID)
Token                | 33 | 13 | llave o token de acceso (clientAccessKey)
Modo Productivo      | 36 | 13 | bandera para indicar modo productivo del servicio (0=Falso, 1 = Verdadero)
PublicKey            | 61 | 13 | Llave pública
---------------------|----|----------------------------------------------------------------------------------
Servicio Mercado Libre    | 14 |
---------------------|----|----------------------------------------------------------------------------------
User ID              | 34 | 14 | nombre usuario, correo, accessKey del servicio utilizado (clientID)
Token                | 33 | 14 | llave o token de acceso (clientAccessKey)
Modo Productivo      | 36 | 14 | bandera para indicar modo productivo del servicio (0=Falso, 1 = Verdadero)
PublicKey            | 61 | 14 | Llave pública
---------------------|----|----------------------------------------------------------------------------------
Redes Sociales       |    | 15 |
---------------------|----|----------------------------------------------------------------------------------
facebook             | 26 | 15 | https://www.facebook.com/Somos-IP-107801364827683/
youtube              | 27 | 15 | https://www.youtube.com/channel/UCd_WGpQe3mUOLE3IQnTeeRA
telegram             | 28 | 15 | https://t.me/SomosIP
whatsapp             | 29 | 15 | numero de contacto whatsapp
---------------------|----|----------------------------------------------------------------------------------
 Almacén PT          |    | 16 |
configuracion        | 00 | 16 |
Pagare               | 48 | 16 |
Leyenda              | 49 | 16 |
Expresion            | 50 | 16 |
---------------------|----|----------------------------------------------------------------------------------
 Remisionado PT      |    | 19 |
configuracion        | 00 | 19 |
Pagare               | 48 | 19 |
Leyenda              | 49 | 19 |
Expresion            | 50 | 19 |
---------------------|----|----------------------------------------------------------------------------------
 Repositorio         |    | 21 |
configuracion        | 00 | 21 | {0=Local?|}
configuracion BD     | 47 | 21 | configuración de base de datos
Folder               | 51 | 21 | nombre del folder (bucket/'folder')
User ID              | 34 | 21 | clave ciec
Password             | 35 | 21 | contraseña, password
Version              | 99 | 21 | versión de base de datos
---------------------|----|----------------------------------------------------------------------------------
 CP Lite             |    | 99 |
 ---------------------|----|----------------------------------------------------------------------------------
configuracion BD     | 47 | 99 | configuración de base de datos
---------------------|----|----------------------------------------------------------------------------------
  Tienda web         |    | 12 |
cuentas-bancarias    | 30 | 12 | array de cuenta bancaria {"cuenta":"", "sucursal":"", "cliente": "", "":""}
horarios             | 31 | 12 | [{"dia":{"inicio":"Lunes","termino":"Vierne"},"hora":{"inicio":"09:00","termino":"17:00"}}]
telefono             | 16 | 12 | telefono
correo               |  8 | 12 | adm.acalidad@ipo.com.mx
modo productivo      | 36 | 12 | 1 o 0
chat-id              | 60 | 12 | 11106648
whatsapp             | 29 | 12 | 5533605567
facebook             | 26 | 12 | https://www.facebook.com/Somos-IP-107801364827683/
youtube              | 27 | 12 | https://www.youtube.com/channel/UCd_WGpQe3mUOLE3IQnTeeRA
telegram             | 28 | 12 | https://t.me/SomosIP
Folder               | 51 | 12 | nombre del folder (bucket/'folder')
---------------------|----|----------------------------------------------------------------------------------
Uso de CFDI          | 07 | 24 | clave default para el uso de CFDI del directorio

NOTA: 
1. el valor del campo CONF_KEY deben ser minusculas para evitar duplicados
2. los nombre de los valores de las llaves no deben ser mayores a 20 caracteres
3. 
*/