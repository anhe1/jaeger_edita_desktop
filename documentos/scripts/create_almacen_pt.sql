/***

***/

/***  TABLA DE DOCUMENTOS VALES, INVENTARIO DEL ALMACÉN ***/
CREATE TABLE `ALMPT` (
	`ALMPT_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`ALMPT_CTALM_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de tipo de almacen',
	`ALMPT_TPALM_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del almacen',
	`ALMPT_CTDEV_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de devoluciones',
	`ALMPT_VER` VARCHAR(3) NULL DEFAULT '2.0' COMMENT 'version del comprobante' COLLATE 'utf8_general_ci',
	`ALMPT_DOC_ID` INT(11) NULL DEFAULT NULL COMMENT 'tipo de documento',
	`ALMPT_CTEFC_ID` INT(11) NULL DEFAULT NULL COMMENT 'efecto del comprobante',
	`ALMPT_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de status del comprobante',
	`ALMPT_UUID` VARCHAR(36) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`ALMPT_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
	`ALMPT_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'folio de control interno',
	`ALMPT_CTSR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la serie de control interno',
	`ALMPT_SERIE` VARCHAR(20) NULL DEFAULT NULL COMMENT 'serie de control interno' COLLATE 'utf8_general_ci',
	`ALMPT_FECEMS` DATETIME NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
	`ALMPT_FCING` DATETIME NULL DEFAULT NULL COMMENT 'fecha de ingreso al almacen',
	`ALMPT_CTDEP_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del departamento',
	`ALMPT_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMPT_RFCR` VARCHAR(14) NULL DEFAULT NULL COMMENT 'rfc del receptor' COLLATE 'utf8_general_ci',
	`ALMPT_NOM` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre del receptor' COLLATE 'utf8_general_ci',
	`ALMPT_CNTCT` VARCHAR(50) NULL DEFAULT NULL COMMENT 'nombre del contacto' COLLATE 'utf8_general_ci',
	`ALMPT_RFRNC` VARCHAR(50) NULL DEFAULT NULL COMMENT 'referencia' COLLATE 'utf8_general_ci',
	`ALMPT_SBTTL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'subtotal',
	`ALMPT_DSCT` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe total del descuento',
	`ALMPT_TRIVA` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'importe total de impuesto traslado iva',
	`ALMPT_GTOTAL` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'gran total',
	`ALMPT_OBSRV` VARCHAR(50) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`ALMPT_CTVND_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de vendedores',
	`ALMPT_VNDR` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del vendedor' COLLATE 'utf8_general_ci',
	`ALMPT_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	`ALMPT_USU_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`ALMPT_FM` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`ALMPT_USU_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`ALMPT_URL_PDF` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'url de descarga para la representación impresa' COLLATE 'utf8_general_ci',
	`ALMPT_ANIO` INT(11) NULL DEFAULT year(`ALMPT_FECEMS`) COMMENT 'ejercicio',
	`ALMPT_MES` INT(11) NULL DEFAULT month(`ALMPT_FECEMS`) COMMENT 'mes',
	PRIMARY KEY (`ALMPT_ID`) USING BTREE,
	INDEX `ALMPT_CTALM_ID` (`ALMPT_CTALM_ID`) USING BTREE,
	INDEX `ALMPT_DOC_ID` (`ALMPT_DOC_ID`) USING BTREE,
	INDEX `ALMPT_CTEFC_ID` (`ALMPT_CTEFC_ID`) USING BTREE,
	INDEX `ALMPT_STTS_ID` (`ALMPT_STTS_ID`) USING BTREE,
	INDEX `ALMPT_CTREL_ID` (`ALMPT_CTREL_ID`) USING BTREE,
	INDEX `ALMPT_CTSR_ID` (`ALMPT_CTSR_ID`) USING BTREE,
	INDEX `ALMPT_ANIO` (`ALMPT_ANIO`) USING BTREE,
	INDEX `ALMPT_MES` (`ALMPT_MES`) USING BTREE,
	INDEX `ALMPT_DRCTR_ID` (`ALMPT_DRCTR_ID`) USING BTREE,
	INDEX `ALMPT_CTDEP_ID` (`ALMPT_CTDEP_ID`) USING BTREE,
	INDEX `ALMPT_UUID` (`ALMPT_UUID`) USING BTREE
)
COMMENT='ALMPT: movimientos'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE RELACIONES DE DOCUMENTOS DE ALMACÉN  ***/
CREATE TABLE `ALMPTR` (
	`ALMPTR_ALMPT_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de documentos de almacen (ALMMP)',
	`ALMPTR_DOC_ID` INT(11) NULL DEFAULT NULL COMMENT 'tipo de documento ',
	`ALMPTR_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
	`ALMPTR_CTREL` VARCHAR(60) NULL DEFAULT NULL COMMENT 'descripcion de la clave de relacion con otros comprobantes' COLLATE 'utf8_general_ci',
	`ALMPTR_UUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'id de documento' COLLATE 'utf8_general_ci',
	`ALMPTR_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMPTR_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
	`ALMPTR_SERIE` VARCHAR(50) NULL DEFAULT NULL COMMENT 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres' COLLATE 'utf8_general_ci',
	`ALMPTR_FECEMS` DATETIME NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
	`ALMPTR_CLV` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de control del directorio' COLLATE 'utf8_general_ci',
	`ALMPTR_RFCR` VARCHAR(14) NULL DEFAULT NULL COMMENT 'clave de control del directorio' COLLATE 'utf8_general_ci',
	`ALMPTR_NOM` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre o razon social del receptor' COLLATE 'utf8_general_ci',
	`ALMPTR_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`ALMPTR_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha y hora de expedicion del comprobante.',
	UNIQUE INDEX `INDICE` (`ALMPTR_ALMPT_ID`, `ALMPTR_DOC_ID`, `ALMPTR_CTREL_ID`, `ALMPTR_UUID`, `ALMPTR_DRCTR_ID`, `ALMPTR_FOLIO`) USING BTREE
)
COMMENT='Almacen PT: documentos relacionados'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE REGISTROS DE CAMBIOS DE STATUS DE DOCUMENTOS DEL ALMACÉN  ***/
CREATE TABLE `ALMPTS` (
	`ALMPTS_ALMPT_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de almacen de materia prima',
	`ALMPTS_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status original',
	`ALMPTS_STTSB_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status del status modificado',
	`ALMPTS_CTMTV_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de motivos de modificacion del status',
	`ALMPTS_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`ALMPTS_CTMTV` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave del motivo del cambio de status' COLLATE 'utf8_general_ci',
	`ALMPTS_NOTA` VARCHAR(50) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`ALMPTS_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio' COLLATE 'utf8_general_ci',
	`ALMPTS_FN` DATETIME NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	UNIQUE INDEX `ALMPTS_ALMMP_ID_ALMPTS_STTS_ID_ALMPTS_STTSB_ID_ALMPTS_CTMTV_ID` (`ALMPTS_ALMPT_ID`, `ALMPTS_STTS_ID`, `ALMPTS_STTSB_ID`, `ALMPTS_CTMTV_ID`) USING BTREE,
	INDEX `ALMPTS_DRCTR_ID` (`ALMPTS_DRCTR_ID`) USING BTREE
)
COMMENT='almacen de producto terminado cambios de status'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/***  TABLA DE REMISIONADO AL CLIENTE  ***/
CREATE TABLE `RMSN` (
	`RMSN_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`RMSN_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`RMSN_CTDOC_ID` SMALLINT(6) NOT NULL COMMENT 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)',
	`RMSN_DECI` SMALLINT(6) NOT NULL COMMENT 'numero de decimales',
	`RMSN_VER` VARCHAR(3) NULL DEFAULT NULL COMMENT 'version de la remision' COLLATE 'utf8_general_ci',
	`RMSN_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'folio de control interno',
	`RMSN_CTSR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con tabla de series y folios',
	`RMSN_SERIE` VARCHAR(10) NULL DEFAULT NULL COMMENT 'serie de control interno del  documento en modo texto' COLLATE 'utf8_general_ci',
	`RMSN_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status de la remision',
	`RMSN_PDD_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o numero de pedido del cliente',
	`RMSN_CTCLS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de productos',
	`RMSN_CTENV_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de envios',
	`RMSN_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio ',
	`RMSN_DRCCN_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del domicilio del directorio',
	`RMSN_VNDDR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del vendedor',
	`RMSN_GUIA_ID` INT(11) NULL DEFAULT NULL COMMENT 'numero de guia o referencia del metodo de envio',
	`RMSN_CTREL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
	`RMSN_RFCE` VARCHAR(14) NULL DEFAULT NULL COMMENT 'registro federal de contribuyentes del emisor' COLLATE 'utf8_general_ci',
	`RMSN_RFCR` VARCHAR(14) NULL DEFAULT NULL COMMENT 'registro federal de contribuyentes del receptor' COLLATE 'utf8_general_ci',
	`RMSN_NOMR` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre o razon social del receptor' COLLATE 'utf8_general_ci',
	`RMSN_CLVR` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de control interno del directorio' COLLATE 'utf8_general_ci',
	`RMSN_CNTCT` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre del contacto' COLLATE 'utf8_general_ci',
	`RMSN_UUID` VARCHAR(36) NULL DEFAULT NULL COMMENT 'id de documento' COLLATE 'utf8_general_ci',
	`RMSN_FECEMS` TIMESTAMP NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
	`RMSN_FECENT` TIMESTAMP NULL DEFAULT NULL COMMENT 'fecha de entrega',
	`RMSN_FECUPC` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima de fecha de pago del comprobante',
	`RMSN_FECCBR` TIMESTAMP NULL DEFAULT NULL COMMENT 'fecha de recepcion de cobranza',
	`RMSN_FCCNCL` DATE NULL DEFAULT NULL COMMENT 'fecha de cancelacion del comprobante',
	`RMSN_FECVNC` DATE NULL DEFAULT NULL COMMENT 'fecha de vencimiento del pagare',
	`RMSN_TPCMB` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'tipo de cambio',
	`RMSN_SBTTL` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'sub total',
	`RMSN_DSCNT` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'total del importe del descuento',
	`RMSN_TRSIVA` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'total del impuesto traslado de IVA',
	`RMSN_GTOTAL` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'gran total (subtotal - descuento + traslado de iva)',
	`RMSN_FACIVA` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'factor del iva pactado con el cliente',
	`RMSN_IVAPAC` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe del iva pactado (SUBTOTAL * IVAPAC)',
	`RMSN_TOTAL` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'total del comprobante',
	`RMSN_FACPAC` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'factor pactado',
	`RMSN_XCBPAC` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'por cobrar pactado',
	`RMSN_DESC` VARCHAR(100) NULL DEFAULT NULL COMMENT 'descripcion del tipo de descuento aplicado' COLLATE 'utf8_general_ci',
	`RMSN_FACDES` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'factor del descuento aplicado',
	`RMSN_DESCT` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe total del descuento (TOTAL X FACDES)',
	`RMSN_XCBRR` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe por cobrar del comprobante',
	`RMSN_CBRD` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'acumulado de la cobranza del comprobante',
	`RMSN_MONEDA` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificacion ISO 4217.' COLLATE 'utf8_general_ci',
	`RMSN_MTDPG` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave del metodo de pago que aplica para este comprobante fiscal digital por Internet, conforme al Articulo 29-A fraccion VII incisos a y b del CFF.' COLLATE 'utf8_general_ci',
	`RMSN_FRMPG` VARCHAR(2) NULL DEFAULT NULL COMMENT 'clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.' COLLATE 'utf8_general_ci',
	`RMSN_NOTA` VARCHAR(255) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`RMSN_VNDDR` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de vendedor asociado' COLLATE 'utf8_general_ci',
	`RMSN_USR_C` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que cancela el comprobante' COLLATE 'utf8_general_ci',
	`RMSN_CVCAN` SMALLINT(6) NULL DEFAULT NULL COMMENT 'clave de cancelacion',
	`RMSN_CLMTV` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave de cancelacion en modo texto {00:Descripcion}' COLLATE 'utf8_general_ci',
	`RMSN_CLNTA` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave de cancelacion' COLLATE 'utf8_general_ci',
	`RMSN_DRCCN` VARCHAR(400) NULL DEFAULT NULL COMMENT 'direccion del envio en modo texto' COLLATE 'utf8_general_ci',
	`RMSN_CNDNS` VARCHAR(30) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`RMSN_URL_PDF` VARCHAR(100) NULL DEFAULT NULL COMMENT '/remision/{0:36}.pdf' COLLATE 'utf8_general_ci',
	`RMSN_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
	`RMSN_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que crea el registro' COLLATE 'utf8_general_ci',
	`RMSN_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`RMSN_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica' COLLATE 'utf8_general_ci',
	`RMSN_SALDO` DECIMAL(20,6) NULL DEFAULT NULL COMMENT 'saldo por cobrar del documento',
	`RMSN_MES` INT(11) NULL DEFAULT month(`RMSN_FECEMS`) COMMENT 'periodo',
	`RMSN_ANIO` INT(11) NULL DEFAULT year(`RMSN_FECEMS`) COMMENT 'ejercicio',
	PRIMARY KEY (`RMSN_ID`) USING BTREE,
	INDEX `RMSN_CTDOC_ID` (`RMSN_CTDOC_ID`) USING BTREE,
	INDEX `RMSN_CTSR_ID` (`RMSN_CTSR_ID`) USING BTREE,
	INDEX `RMSN_STTS_ID` (`RMSN_STTS_ID`) USING BTREE,
	INDEX `RMSN_DRCTR_ID` (`RMSN_DRCTR_ID`) USING BTREE,
	INDEX `RMSN_DRCCN_ID` (`RMSN_DRCCN_ID`) USING BTREE,
	INDEX `RMSN_VNDDR_ID` (`RMSN_VNDDR_ID`) USING BTREE,
	INDEX `RMSN_PDD_ID` (`RMSN_PDD_ID`) USING BTREE,
	INDEX `RMSN_CTCLS_ID` (`RMSN_CTCLS_ID`) USING BTREE,
	INDEX `RMSN_CTENV_ID` (`RMSN_CTENV_ID`) USING BTREE,
	INDEX `RMSN_CTREL_ID` (`RMSN_CTREL_ID`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*** TABLA DE COMISIONES A VENDEDORES POR REMISIONES ***/
CREATE TABLE `RMSNC` (
	`RMSNC_RMSN_ID` INT(11) NOT NULL COMMENT 'indice de la tabla remision',
	`RMSNC_A` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`RMSNC_CTCMS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del catalogo de comisiones',
	`RMSNC_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio',
	`RMSNC_VNDR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de vendedores',
	`RMSNC_CTDIS_ID` INT(11) NULL DEFAULT NULL,
	`RMSNC_DESC` VARCHAR(150) NULL DEFAULT NULL COMMENT 'descripcion' COLLATE 'utf8_general_ci',
	`RMSNC_FCPAC` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'factor pactado',
	`RMSNC_FCACT` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'factor actualizado',
	`RMSNC_IMPR` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe calculado de la comision',
	`RMSNC_ACUML` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe pagado o acumulado',
	`RMSNC_VNDR` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del vendedor' COLLATE 'utf8_general_ci',
	`RMSNC_FN` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de moficacion del registro',
	`RMSNC_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'ultima clave del usuario que modifica el registro' COLLATE 'utf8_general_ci',
	UNIQUE INDEX `RMSNC_RMSN_ID` (`RMSNC_RMSN_ID`) USING BTREE,
	INDEX `RMSNC_CTCMS_ID` (`RMSNC_CTCMS_ID`) USING BTREE,
	INDEX `RMSNC_DRCTR_ID` (`RMSNC_DRCTR_ID`) USING BTREE,
	INDEX `RMSNC_VNDR_ID` (`RMSNC_VNDR_ID`) USING BTREE,
	INDEX `RMSNC_CTDIS_ID` (`RMSNC_CTDIS_ID`) USING BTREE
)
COMMENT='comisiones a vendedores'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*** TABLA DE RELACIONES DE REMISIONES ***/
CREATE TABLE `RMSNR` (
	`RMSNR_RMSN_ID` INT(11) NOT NULL COMMENT 'indice de relacion con la tabla de remisiones',
	`RMSNR_DRCTR_ID` INT(11) NOT NULL COMMENT 'indice de relacion del directorio',
	`RMSNR_CTREL_ID` INT(11) NOT NULL COMMENT 'indice de la tabla de tipos de relacion',
	`RMSNR_RELN` VARCHAR(60) NULL DEFAULT NULL COMMENT 'descripcion de la relacion del comprobante' COLLATE 'utf8_general_ci',
	`RMSNR_UUID` VARCHAR(36) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`RMSNR_SERIE` VARCHAR(25) NULL DEFAULT NULL COMMENT 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres' COLLATE 'utf8_general_ci',
	`RMSNR_FOLIO` INT(11) NULL DEFAULT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
	`RMSNR_NOMR` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el RFC, del receptor del comprobante.' COLLATE 'utf8_general_ci',
	`RMSNR_FECEMS` DATE NULL DEFAULT NULL COMMENT 'fecha y hora de expedicion del comprobante.',
	`RMSNR_TOTAL` DECIMAL(18,4) NULL DEFAULT NULL COMMENT 'importe total del comprobante',
	`RMSNR_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro ' COLLATE 'utf8_general_ci',
	`RMSNR_FN` TIMESTAMP NULL DEFAULT NULL COMMENT 'fecha de creacion del registro',
	UNIQUE INDEX `RMSNR_RMSN_ID_RMSNR_DRCTR_ID_RMSNR_CTREL_ID_RMSNR_UUID` (`RMSNR_RMSN_ID`, `RMSNR_DRCTR_ID`, `RMSNR_CTREL_ID`, `RMSNR_UUID`) USING BTREE
)
COMMENT='comprobantes relacionados remisiones'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*** REMISIONADO: TABLA DE REGISTRO DE CAMBIOS DE STATUS ***/
CREATE TABLE `RMSNS` (
	`RMSNS_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice incremental',
	`RMSNS_A` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`RMSNS_RMSN_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de remisiones',
	`RMSNS_RMSN_STTS_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status anterior',
	`RMSNS_RMSN_STTSA_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del status autorizado',
	`RMSNS_DRCTR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla del directorio',
	`RMSNS_CVMTV` VARCHAR(100) NULL DEFAULT NULL COMMENT 'clave del motivo del cambio de status' COLLATE 'utf8_general_ci',
	`RMSNS_NOTA` VARCHAR(100) NULL DEFAULT NULL COMMENT 'obserevaciones' COLLATE 'utf8_general_ci',
	`RMSNS_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio' COLLATE 'utf8_general_ci',
	`RMSNS_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
	PRIMARY KEY (`RMSNS_ID`) USING BTREE,
	INDEX `RMSNS_RMSN_ID` (`RMSNS_RMSN_ID`) USING BTREE,
	INDEX `RMSNS_DRCTR_ID` (`RMSNS_DRCTR_ID`) USING BTREE,
	INDEX `RMSNS_RMSN_STTSA_ID` (`RMSNS_RMSN_STTSA_ID`) USING BTREE
)
COMMENT='remisionado: tabla de registro de cambios de status'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
