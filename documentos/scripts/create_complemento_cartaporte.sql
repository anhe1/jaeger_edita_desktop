/*
    SCRIPT PARA LA CREACION DE LA TABLA DEL COMPLEMENTRO DE CARTA PORTE
*/
CREATE TABLE `_cmpcp` (
	`_cmpcp_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`_cmpcp_a` BIT(1) NOT NULL DEFAULT b'1' COMMENT 'registro activo',
	`_cmpcp_ver` VARCHAR(3) NOT NULL COMMENT 'version del complemento' COLLATE 'utf8_general_ci',
	`_cmpcp_cfd_id` INT(11) NOT NULL COMMENT 'indice de relacion del comprobante fiscal',
	`_cmpcp_inter` INT(11) NOT NULL COMMENT 'para expresar si los bienes y/o mercancías que son transportadas ingresan o salen del territorio nacional.',
	`_cmpcp_etmer` VARCHAR(10) NULL DEFAULT NULL COMMENT 'condicional para precisar si los bienes y/o mercancías ingresan o salen del territorio nacional.' COLLATE 'utf8_general_ci',
	`_cmpcp_pais` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave de pais de origen o destino' COLLATE 'utf8_general_ci',
	`_cmpcp_via` VARCHAR(2) NULL DEFAULT NULL COMMENT 'via de ingreso o salida de mercancia' COLLATE 'utf8_general_ci',
	`_cmpcp_total` DECIMAL(16,4) NULL DEFAULT NULL COMMENT 'indicar en kilómetros, la suma de las distancias recorridas, registradas en el atributo DistanciaRecorrida, para el traslado de los bienes y/o mercancías',
	`_cmpcp_ubica` TEXT NULL DEFAULT NULL COMMENT 'listado de ubicaciones en formato json' COLLATE 'utf8_general_ci',
	`_cmpcp_merca` TEXT NULL DEFAULT NULL COMMENT 'informacion de los bienes y/o servicios que se trasladan' COLLATE 'utf8_general_ci',
	`_cmpcp_figt` TEXT NULL DEFAULT NULL COMMENT 'datos de la(s) figura(s) del transporte que interviene' COLLATE 'utf8_general_ci',
	`_cmpcp_usr_n` VARCHAR(10) NOT NULL COMMENT 'clave de usuario que crea el registro' COLLATE 'utf8_general_ci',
	`_cmpcp_fn` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`_cmpcp_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifico el registro' COLLATE 'utf8_general_ci',
	`_cmpcp_fm` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	PRIMARY KEY (`_cmpcp_id`) USING BTREE,
	INDEX `_cmpcp_cfd_id` (`_cmpcp_cfd_id`) USING BTREE
)
COMMENT='complemento carta porte'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
