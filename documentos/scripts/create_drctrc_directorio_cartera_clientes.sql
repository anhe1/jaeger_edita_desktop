CREATE TABLE `_drctrc` (
	`_drctrc_a` INT(11) NULL DEFAULT NULL COMMENT 'registro activo',
	`_drctrc_vndr_id` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion con la tabla de vendedores',
	`_drctrc_drctr_id` INT(11) NULL DEFAULT NULL COMMENT 'indice del directorio de clientes',
	`_drctrc_nota` VARCHAR(100) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`_drctrc_fm` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion',
	`_drctrc_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci'
)
COMMENT='vendedores: cartera de clientes, relacion entre clientes y vendedores'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
