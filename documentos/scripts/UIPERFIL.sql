CREATE TABLE `UIPERFIL` (
	`UIPERFIL_ID` INT(11) NOT NULL COMMENT 'indice',
	`UIPERFIL_A` SMALLINT(6) NOT NULL,
	`UIPERFIL_UIMENU_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del menu de opciones',
	`UIPERFIL_RLSUSR_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del rol del usuario',
	`UIPERFIL_ACTION` VARCHAR(10) NULL DEFAULT NULL COMMENT 'acciones permitidas' COLLATE 'utf8_general_ci',
	`UIPERFIL_KEY` VARCHAR(25) NULL DEFAULT NULL COMMENT 'llave del menu' COLLATE 'utf8_general_ci',
	`UIPERFIL_FN` TIMESTAMP NOT NULL COMMENT 'fecha de creacion del registro',
	`UIPERFIL_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`UIPERFIL_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registr',
	`UIPERFIL_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifico el registr' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`UIPERFIL_ID`) USING BTREE,
	INDEX `UIPERFIL_ID` (`UIPERFIL_ID`) USING BTREE,
	INDEX `UIPERFIL_UIMENU_ID` (`UIPERFIL_UIMENU_ID`) USING BTREE,
	INDEX `UIPERFIL_RLSUSR_ID` (`UIPERFIL_RLSUSR_ID`) USING BTREE,
	INDEX `UIPERFIL_KEY` (`UIPERFIL_KEY`) USING BTREE
)
COMMENT='permisos del perfil de usuario'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `_uiperfil` (
	`_uiperfil_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla de perfiles',
	`_uiperfil_a` INT(11) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`_uiperfil_uimenu_id` INT(11) NOT NULL COMMENT 'indice de la ',
	`_uiperfil_rlsusr_id` INT(11) NOT NULL COMMENT 'indice del rol',
	`_uiperfil_key` VARCHAR(25) NOT NULL COMMENT 'llave del menu' COLLATE 'utf8_general_ci',
	`_uiperfil_action` VARCHAR(10) NOT NULL COLLATE 'utf8_general_ci',
	`_uiperfil_fn` DATETIME NOT NULL COMMENT 'fecha de creacion del registro',
	`_uiperfil_usr_n` VARCHAR(10) NOT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`_uiperfil_fm` DATETIME NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`_uiperfil_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`_uiperfil_id`) USING BTREE
)
COMMENT='perfiles: opciones del menu por perfil'
COLLATE='utf8_general_ci'
ENGINE=InnoDB

;
