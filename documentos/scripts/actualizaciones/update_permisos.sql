/* **************
  TABLA PARA RELACION ENTRE TABLA DE USUARIOS Y PEFILES

  1. AGREGAR COMENTARIO PARA LA TABLA DE PERFILES
*****************/
ALTER TABLE `_uiperfil`	COMMENT='perfiles: opciones del menu por perfil';

/*
  CREACION DE TABLA DE RELACIONES DE USUARIOS Y PERFILES
*/
CREATE TABLE `_rlusrl` (
	`_rlusrl_id` INT(11) NOT NULL COMMENT 'indice de la tabla',
	`_rlusrl_a` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`_rlusrl_usr_id` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion del usuario',
	`_rlusrl_rlsusr_id` INT(11) NULL DEFAULT NULL COMMENT 'indice de relacion del ROL',
	PRIMARY KEY (`_rlusrl_id`) USING BTREE,
	INDEX `_rlusrl_id` (`_rlusrl_id`) USING BTREE,
	INDEX `_rlusrl_usr_id` (`_rlusrl_usr_id`) USING BTREE,
	INDEX `_rlusrl_rlsusr_id` (`_rlusrl_rlsusr_id`) USING BTREE
)
COMMENT='perfiles: tabla de relaciones entre usuarios y perfiles'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/* PRIMER RELACION DEL ADMINISTRADOR */
INSERT INTO `_rlusrl` (`_rlusrl_id`, `_rlusrl_a`, `_rlusrl_usr_id`, `_rlusrl_rlsusr_id`) VALUES (1, 1, 1, 1);
