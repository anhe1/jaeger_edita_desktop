/* 
*** ACTUALIZACION BANCOS 01/02/2024
*** INCLUYE EL CAMPO BNCAUX_NOIDEN EN LA TABLA DE AUXILIARES 
*/
ALTER TABLE `BNCAUX` ADD COLUMN `BNCAUX_NOIDEN` VARCHAR(11) NULL DEFAULT NULL AFTER `BNCAUX_SBID`;

ALTER TABLE `BNCAUX`
	CHANGE COLUMN `BNCAUX_NOIDEN` `BNCAUX_NOIDEN` VARCHAR(11) NULL DEFAULT NULL COMMENT 'identificador de la prepoliza' COLLATE 'utf8_general_ci' AFTER `BNCAUX_SBID`,
	ADD INDEX `BNCCMP_NOIDEN` (`BNCAUX_NOIDEN`);
	
/*
*** CONSULTA PARA ACTUALIZACION DE LA TABLA AUXILIARES

UPDATE BNCAUX 
INNER JOIN BNCMOV ON BNCMOV.BNCMOV_ID = BNCAUX.BNCAUX_SBID
SET BNCAUX.BNCAUX_NOIDEN = BNCMOV.BNCMOV_NOIDEN

*/