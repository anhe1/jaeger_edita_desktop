/* 
	* cambiar tipos de datos a SMALLINT para evitar errores 
	* agregar campo _drctr_nomc nombre comercial
	*/
ALTER TABLE `_drctr`
ADD COLUMN `_drctr_nomc` VARCHAR(255) NULL DEFAULT NULL COMMENT 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.' AFTER `_drctr_nom`,
ADD COLUMN `_drctr_pdfc` VARCHAR(255) NULL DEFAULT NULL COMMENT 'url constancia de situacion fiscal' AFTER `_drctr_nom`;

ALTER TABLE `_drctr`
	CHANGE COLUMN `_drctr_prncpl` `_drctr_prncpl` SMALLINT NOT NULL DEFAULT 0 COMMENT 'Registgro principal' AFTER `_drctr_extrnjr`,
	CHANGE COLUMN `_drctr_cmsn_id` `_drctr_cmsn_id` SMALLINT NOT NULL DEFAULT 0 COMMENT 'id del catalogo de comisiones en el caso de ser vendedor' AFTER `_drctr_prncpl`,
	CHANGE COLUMN `_drctr_dscnt_id` `_drctr_dscnt_id` SMALLINT NOT NULL DEFAULT 0 COMMENT 'id de catalogo de descuentos asignado cuando es cliente' AFTER `_drctr_cmsn_id`,
	CHANGE COLUMN `_drctr_clv` `_drctr_clv` VARCHAR(14) NULL DEFAULT NULL COMMENT 'clave unica de registro en sistema' COLLATE 'utf8_general_ci' AFTER `_drctr_dscnt_id`,
	CHANGE COLUMN `_drctr_nom` `_drctr_nom` VARCHAR(255) NULL DEFAULT NULL COMMENT 'nombre y apellido o razon social' COLLATE 'utf8_general_ci' AFTER `_drctr_rfc`,
	CHANGE COLUMN `_drctr_nomc` `_drctr_nomc` VARCHAR(255) NULL DEFAULT NULL COMMENT 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.' COLLATE 'utf8_general_ci' AFTER `_drctr_nom`,
	CHANGE COLUMN `_drctr_extrnjr` `_drctr_extrnjr` SMALLINT(6) NULL DEFAULT '0' COMMENT 'Bandera para identificar la nacionalidad (0 => Nacional, 1 => Extranjero)' AFTER `_drctr_mail`,
	CHANGE COLUMN `_drctr_usocfdi` `_drctr_usocfdi` VARCHAR(4) NULL DEFAULT NULL COMMENT 'clave de uso de cfdi por default' COLLATE 'utf8_general_ci' AFTER `_drctr_extrnjr`,
	CHANGE COLUMN `_drctr_rlcn` `_drctr_rlcn` VARCHAR(64) NULL DEFAULT NULL COMMENT 'relacion comercial con la empresa (27=Cliente,163=Proveedor)' COLLATE 'utf8_general_ci' AFTER `_drctr_iva`,
	CHANGE COLUMN `_drctr_usr` `_drctr_usr` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `_drctr_rlcn`,
	CHANGE COLUMN `_drctr_dsc` `_drctr_dsc` SMALLINT(6) NULL DEFAULT '0' AFTER `_drctr_usr`,
	CHANGE COLUMN `_drctr_rgstrptrnl` `_drctr_rgstrptrnl` VARCHAR(12) NULL DEFAULT NULL COMMENT 'Registgro patronal' COLLATE 'utf8_general_ci' AFTER `_drctr_dsp`,
	CHANGE COLUMN `_drctr_usr_n` `_drctr_usr_n` VARCHAR(10) NULL DEFAULT NULL COMMENT 'usuario creo' COLLATE 'utf8_general_ci' AFTER `_drctr_fn`,
	CHANGE COLUMN `_drctr_usr_m` `_drctr_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'ultimo usuario que modifico' COLLATE 'utf8_general_ci' AFTER `_drctr_fm`,
	CHANGE COLUMN `_drctr_dsp` `_drctr_dsp` INT NULL DEFAULT 0 COMMENT 'dias pactados de entrega' AFTER `_drctr_dsc`; 

/*  ACTUALIZACION DE LA TABLA DE DIRECCIONES DEL DIRECTORIO
	* agregar campo _drccn_ctdrc_id para el tipo de domicilio
*/
ALTER TABLE `_drccn`
	ADD COLUMN `_drccn_ctdrc_id` SMALLINT NULL DEFAULT NULL COMMENT 'alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)' AFTER `_drccn_cp`,
	CHANGE COLUMN `_drccn_tp` `_drccn_tp` VARCHAR(64) NOT NULL DEFAULT '' COMMENT 'alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=Envio)' COLLATE 'utf8_general_ci' AFTER `_drccn_ctdrc_id`,
	CHANGE COLUMN `_drccn_cll` `_drccn_cll` VARCHAR(124) NOT NULL DEFAULT '' COMMENT 'calle' COLLATE 'utf8_general_ci' AFTER `_drccn_tp`,
	CHANGE COLUMN `_drccn_asnh` `_drccn_asnh` VARCHAR(64) NULL DEFAULT '' COMMENT 'asentamiento humano' COLLATE 'utf8_general_ci' AFTER `_drccn_intr`,
	CHANGE COLUMN `_drccn_cdgps` `_drccn_cdgps` VARCHAR(3) NULL DEFAULT NULL COMMENT 'codigo de pais' COLLATE 'utf8_general_ci' AFTER `_drccn_ps`,
	CHANGE COLUMN `_drccn_usr_n` `_drccn_usr_n` VARCHAR(10) NOT NULL DEFAULT '' COMMENT 'usuario creo' COLLATE 'utf8_general_ci' AFTER `_drccn_fn`,
	CHANGE COLUMN `_drccn_usr_m` `_drccn_usr_m` VARCHAR(10) NULL DEFAULT NULL COMMENT 'ultimo usuario que modifico' COLLATE 'utf8_general_ci' AFTER `_drccn_fm`;


/*
	tabla de contactos del directorio 
*/
CREATE TABLE `DRCTRL` (
	`DRCTRL_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`DRCTRL_A` SMALLINT(6) NULL DEFAULT '1' COMMENT 'registro activo',
	`DRCTRL_DRCTR_ID` INT(11) NULL DEFAULT '0' COMMENT 'indice de relacion con la tabla del directorio',
	`DRCTRL_SUG` VARCHAR(32) NULL DEFAULT NULL COMMENT 'trato sugerido' COLLATE 'utf8_general_ci',
	`DRCTRL_NOM` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre del contacto' COLLATE 'utf8_general_ci',
	`DRCTRL_PST` VARCHAR(128) NULL DEFAULT NULL COMMENT 'puesto' COLLATE 'utf8_general_ci',
	`DRCTRL_TTEL` VARCHAR(16) NULL DEFAULT NULL COMMENT 'tipo de telefono' COLLATE 'utf8_general_ci',
	`DRCTRL_TEL` VARCHAR(64) NULL DEFAULT NULL COMMENT 'telefono de contacto' COLLATE 'utf8_general_ci',
	`DRCTRL_TMAIL` VARCHAR(16) NULL DEFAULT NULL COMMENT 'tipo de correo' COLLATE 'utf8_general_ci',
	`DRCTRL_MAIL` VARCHAR(64) NULL DEFAULT NULL COMMENT 'correo de contacto' COLLATE 'utf8_general_ci',
	`DRCTRL_NOTA` VARCHAR(128) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`DRCTRL_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`DRCTRL_FN` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha de creacion del registro',
	`DRCTRL_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`DRCTRL_FM` DATE NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	PRIMARY KEY (`DRCTRL_ID`) USING BTREE
)
COMMENT='litsa de contactos relacionados al contribuyentes del directorio'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
    1. tabla de relaciones del directorio, utilizar el archivo
	create_drctrr_directorio_relacion_comercial

	2. para la tabla de cartera de clientes, utilizar el archivo
	create_drctrc_directorio_cartera_clientes
*/