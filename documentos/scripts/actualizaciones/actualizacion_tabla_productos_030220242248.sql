/*
*** ACTUALIZACION PARA CATALOGO DE PRODUCTOS (MARIADB)
*** TABLAS QUE SE AGREGAN A LA BASE DE DATOS
*/

CREATE TABLE `CTPRD` (
	`CTPRD_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`CTPRD_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTPRD_ALM_ID` SMALLINT(6) NOT NULL COMMENT 'indice del almacen',
	`CTPRD_SEC` INT(11) NULL DEFAULT NULL,
	`CTPRD_TIPO` SMALLINT(6) NOT NULL,
	`CTPRD_CTCLS_ID` SMALLINT(6) NOT NULL COMMENT 'indice del catalogo de categorias',
	`CTPRV_CNVR` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'factor de conversion a la unidad de almacen',
	`CTPRD_CNVR` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'clave de producto para formar url',
	`CTPRD_CLV` VARCHAR(128) NULL DEFAULT NULL COMMENT 'clave de producto para formar url' COLLATE 'utf8_general_ci',
	`CTPRD_NOM` VARCHAR(128) NULL DEFAULT NULL COMMENT 'nombre o descripcion del producto' COLLATE 'utf8_general_ci',
	`CTPRD_USR_N` VARCHAR(20) NULL DEFAULT NULL COMMENT 'clave de usuario que crea el registro' COLLATE 'utf8_general_ci',
	`CTPRD_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`CTPRD_URL` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`CTPRD_ID`) USING BTREE
)
COMMENT='catalogo de productos'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTPRDM` (
	`CTPRDM_ID` INT(11) NOT NULL COMMENT 'indice de la tabla',
	`CTPRDM_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTPRDM_CTPRD_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de productos',
	`CTPRDM_URL` VARCHAR(8000) NULL DEFAULT NULL COMMENT 'url amazon' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`CTPRDM_ID`) USING BTREE,
	INDEX `CTPRDM_CTPRD_ID` (`CTPRDM_CTPRD_ID`) USING BTREE
)
COMMENT='catalogo de productos: tabla de imagenes relacionadas a productos'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTMDL` (
	`CTMDL_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice principal de la tabla',
	`CTMDL_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTMDL_CTCLS_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de categorias',
	`CTMDL_CTPRD_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de la tabla de productos',
	`CTMDL_CTPRC_ID` INT(11) NULL DEFAULT '0' COMMENT 'indice del catalogo de precios',
	`CTMDL_ALM_ID` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'indice del almacen mp = 1, pt = 2, tw = 3',
	`CTMDL_ATRZD` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'precio autorizado',
	`CTMDL_TIPO` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). es utilizado en cfdi para hacer la distincion de producto o servicio.',
	`CTMDL_VIS` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)',
	`CTMDL_CLV` VARCHAR(128) NULL DEFAULT NULL COMMENT 'clave del modelo (url)' COLLATE 'utf8_general_ci',
	`CTMDL_DSCRC` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'descripcion corta' COLLATE 'utf8_general_ci',
	`CTMDL_MRC` VARCHAR(128) NULL DEFAULT NULL COMMENT 'marca o fabricante' COLLATE 'utf8_general_ci',
	`CTMDL_ESPC` VARCHAR(128) NULL DEFAULT NULL COMMENT 'especifcaciones' COLLATE 'utf8_general_ci',
	`CTMDL_UNDD` VARCHAR(20) NULL DEFAULT NULL COMMENT 'unidad personalizada' COLLATE 'utf8_general_ci',
	`CTMDL_UNTR` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'precio unitario',
	`CTMDL_TRSIVAF` VARCHAR(6) NULL DEFAULT NULL COMMENT 'factor del iva traslado' COLLATE 'utf8_general_ci',
	`CTMDL_TRSIVA` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'importe del impuesto del iva trasladado',
	`CTMDL_UNDDZ` INT(11) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad utiliza en alto o calibre eje z',
	`CTMDL_UNDDXY` INT(11) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad utilizada en largo y ancho',
	`CTMDL_UNDDA` INT(11) NULL DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad de almacenamiento en el el almacen',
	`CTMDL_LARGO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'largo',
	`CTMDL_ANCHO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'ancho',
	`CTMDL_ALTO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'alto',
	`CTMDL_PESO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'peso',
	`CTMDL_CDG` VARCHAR(12) NULL DEFAULT NULL COMMENT 'codigo de barras' COLLATE 'utf8_general_ci',
	`CTMDL_SKU` VARCHAR(100) NULL DEFAULT NULL COMMENT 'obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, sku o equivalente, propia de la operacion del emisor (en cfdi: noidentificacion) (publicacion)' COLLATE 'utf8_general_ci',
	`CTMDL_MIN` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'stock minimo del almacen',
	`CTMDL_MAX` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'stock maximo del almacen',
	`CTMDL_REORD` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'punto de reorden',
	`CTMDL_EXT` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'cantidad de existencia',
	`CTMDL_CTAPRE` VARCHAR(20) NULL DEFAULT NULL COMMENT 'numero de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificacion del certificado de participacion inmobiliaria no amortizable.' COLLATE 'utf8_general_ci',
	`CTMDL_NUMREQ` VARCHAR(50) NULL DEFAULT NULL COMMENT 'numero de requerimiento de aduana' COLLATE 'utf8_general_ci',
	`CTMDL_CLVUND` VARCHAR(3) NULL DEFAULT NULL COMMENT 'clave de unidad sat' COLLATE 'utf8_general_ci',
	`CTMDL_CLVPRDS` VARCHAR(8) NULL DEFAULT NULL COMMENT 'clave del producto o del servicio sat' COLLATE 'utf8_general_ci',
	`CTMDL_CLVOBJ` VARCHAR(2) NULL DEFAULT NULL COMMENT 'clave si la operacion es objeto o no de impuesto sat' COLLATE 'utf8_general_ci',
	`CTMDL_ETQTS` VARCHAR(254) NULL DEFAULT NULL COMMENT 'etiquetas de busqueda (mover a publicacion)' COLLATE 'utf8_general_ci',
	`CTMDL_DSCR` VARCHAR(8000) NULL DEFAULT NULL COMMENT 'descripcion larga del producto (publicacion)' COLLATE 'utf8_general_ci',
	`CTMDL_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario que crea el registro' COLLATE 'utf8_general_ci',
	`CTMDL_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
	`CTMDL_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`CTMDL_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`CTMDL_SEC` INT(11) NULL DEFAULT NULL,
	`CTMDL_DIS` SMALLINT(6) NOT NULL DEFAULT '0',
	`CTMDL_RETIVAF` VARCHAR(5) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`CTMDL_RETIEPS` DECIMAL(14,4) NULL DEFAULT NULL,
	`CTMDL_RETISR` DECIMAL(14,4) NULL DEFAULT NULL,
	`CTMDL_RETIEPSF` VARCHAR(6) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`CTMDL_RETISRF` VARCHAR(5) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`CTMDL_TRSIEPSF` VARCHAR(6) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`CTMDL_UNTR1` DECIMAL(11,4) NULL DEFAULT NULL COMMENT '(eliminar)',
	`CTMDL_TRSIEPS` DECIMAL(14,4) NULL DEFAULT NULL,
	`CTMDL_UNTR2` DECIMAL(11,4) NULL DEFAULT NULL COMMENT '(eliminar)',
	`CTMDL_RETIVA` DECIMAL(14,4) NULL DEFAULT NULL,
	`CTMDL_CANT1` SMALLINT(6) NULL DEFAULT NULL COMMENT '(eliminar)',
	`CTMDL_CANT2` SMALLINT(6) NULL DEFAULT NULL COMMENT '(eliminar)',
	PRIMARY KEY (`CTMDL_ID`) USING BTREE,
	INDEX `CTMDL_CTPRD_ID` (`CTMDL_CTPRD_ID`) USING BTREE,
	INDEX `CTMDL_CTCLS_ID` (`CTMDL_CTCLS_ID`) USING BTREE,
	INDEX `CTMDL_CTPRC_ID` (`CTMDL_CTPRC_ID`) USING BTREE,
	INDEX `CTMDL_ATRZD` (`CTMDL_ATRZD`) USING BTREE
)
COMMENT='catalogo de modelos de productos'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTMDLM` (
	`CTMDLM_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`CTMDLM_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTMDLM_CTMDL_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice del modelo',
	`CTMDLM_CTPRD_ID` INT(11) NULL DEFAULT NULL COMMENT 'indice de la tabla de productos',
	`CTMDLM_URL` VARCHAR(8000) NULL DEFAULT NULL COMMENT 'url amazon' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`CTMDLM_ID`) USING BTREE,
	INDEX `CTMDLM_CTMDL_ID` (`CTMDLM_CTMDL_ID`) USING BTREE,
	INDEX `CTMDLM_CTPRD_ID` (`CTMDLM_CTPRD_ID`) USING BTREE
)
COMMENT='catalogo de modelos: imagenes relacionadas al modelo'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTESPC` (
	`CTESPC_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`CTESPC_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTESPC_NOM` VARCHAR(50) NULL DEFAULT NULL COMMENT 'nombre o descripcion' COLLATE 'utf8_general_ci',
	`CTESPC_NOTA` VARCHAR(50) NULL DEFAULT NULL COMMENT 'observaciones' COLLATE 'utf8_general_ci',
	`CTESPC_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
	`CTESPC_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`CTESPC_FM` DATE NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`CTESPC_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'ultima clave del usuario que modifica el registro' COLLATE 'utf8_general_ci',
	PRIMARY KEY (`CTESPC_ID`) USING BTREE
)
COMMENT='catalogo de especificaciones (tamanio) '
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `CTMDLE` (
	`CTMDLE_ID` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
	`CTMDLE_A` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'registro activo',
	`CTMDLE_CTPRD_ID` INT(11) NOT NULL COMMENT 'indice de relacion con la tabla de productos',
	`CTMDLE_CTMDL_ID` INT(11) NOT NULL COMMENT 'indice de relacion de la tabla de modelos',
	`CTMDLE_CTESPC_ID` INT(11) NOT NULL COMMENT 'indice de relacion de la tabla de ctespc_id',
	`CTMDLE_ATRZD` SMALLINT(6) NOT NULL COMMENT 'autorizacion',
	`CTMDLE_ALM_ID` SMALLINT(6) NOT NULL COMMENT 'indice de tipo de almacen',
	`CTMDLE_VIS` SMALLINT(6) NOT NULL COMMENT 'bandera para el registro visible',
	`CTMDLE_EXT` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'existencia',
	`CTMDLE_MIN` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'minimo',
	`CTMDLE_MAX` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'maximo',
	`CTMDLE_REORD` DECIMAL(11,4) NULL DEFAULT NULL COMMENT 'reorden',
	`CTMDLE_USR_N` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del usuario creador del registro' COLLATE 'utf8_general_ci',
	`CTMDLE_FN` TIMESTAMP NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
	`CTMDLE_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`CTMDLE_FM` TIMESTAMP NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
	`CTMDLE_DIS` SMALLINT(6) NOT NULL,
	PRIMARY KEY (`CTMDLE_ID`) USING BTREE,
	INDEX `CTMDLE_CTPRD_ID` (`CTMDLE_CTPRD_ID`) USING BTREE,
	INDEX `CTMDLE_CTMDL_ID` (`CTMDLE_CTMDL_ID`) USING BTREE,
	INDEX `CTMDLE_CTESPC_ID` (`CTMDLE_CTESPC_ID`) USING BTREE
)
COMMENT='catalogo de modelos: tabla de relacion modelos vs especificaciones (tamanios)'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTMDLT` (
	`CTMDLT_CTMDL_ID` INT(11) NOT NULL COMMENT 'indice de la tabla',
	`CTMDLT_VIS` SMALLINT(6) NOT NULL COMMENT 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)',
	`CTMDLT_SEC` INT(11) NULL DEFAULT NULL COMMENT 'secuencia de aparicion',
	`CTMDLT_LARGO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'largo',
	`CTMDLT_ANCHO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'ancho',
	`CTMDLT_ALTO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'alto o calibre',
	`CTMDLT_PESO` DECIMAL(14,4) NULL DEFAULT NULL COMMENT 'peso',
	`CTMDLT_SKU` VARCHAR(100) NULL DEFAULT NULL COMMENT 'obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, sku o equivalente, propia de la operacion del emisor (en cfdi: noidentificacion)' COLLATE 'utf8_general_ci',
	`CTMDLT_DSCRC` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'descripcion corta' COLLATE 'utf8_general_ci',
	`CTMDLT_DSCR` VARCHAR(8000) NULL DEFAULT NULL COMMENT 'descripcion larga de producto' COLLATE 'utf8_general_ci',
	`CTMDLT_ETQTS` VARCHAR(254) NULL DEFAULT NULL COMMENT 'etiquetas de busqueda' COLLATE 'utf8_general_ci',
	`CTMDLT_USR_M` VARCHAR(10) NULL DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro' COLLATE 'utf8_general_ci',
	`CTMDLT_FM` TIMESTAMP NULL DEFAULT current_timestamp() COMMENT 'fecha de ultima modificacion',
	PRIMARY KEY (`CTMDLT_CTMDL_ID`) USING BTREE
)
COMMENT='catalogo de modelos, publicacion para la tienda web'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `CTMDLX` (
	`CTMDLX_CTPRD_ID` INT(11) NOT NULL COMMENT 'indice de la tabla de productos',
	`CTMDLX_CTMDL_ID` INT(11) NOT NULL COMMENT 'indice de la tabla de modelos',
	`CTMDLX_CTESPC_ID` INT(11) NOT NULL DEFAULT -1 COMMENT 'indice de la tabla de especificaciones',
	`CTMDLX_CTALM_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice de la tabla de almacenes',
	`CTMDLX_TPALM_ID` INT(11) NOT NULL DEFAULT '0' COMMENT 'indice del tipo de almacen mp = 1, pt = 2, tw = 3',
	`CTMDLX_EXT` DECIMAL(11,4) NULL DEFAULT '0.0000' COMMENT 'existencia',
	`CTMDLX_MIN` DECIMAL(11,4) NULL DEFAULT '0.0000' COMMENT 'minimo',
	`CTMDLX_MAX` DECIMAL(11,4) NULL DEFAULT '0.0000' COMMENT 'maximo',
	`CTMDLX_REORD` DECIMAL(11,4) NULL DEFAULT '0.0000' COMMENT 'reorden',
	PRIMARY KEY (`CTMDLX_CTPRD_ID`, `CTMDLX_CTMDL_ID`, `CTMDLX_CTESPC_ID`, `CTMDLX_CTALM_ID`, `CTMDLX_TPALM_ID`) USING BTREE
)
COMMENT='catalogo de modelos: existencias'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

/*
*** IMPORTACION DE DATOS DE TABLAS DE PRODUCTOS Y MODELOS
*/
INSERT INTO CTPRD (CTPRD_ID,CTPRD_A,CTPRD_ALM_ID,CTPRD_TIPO,CTPRD_CTCLS_ID,CTPRD_CNVR,CTPRD_CLV,CTPRD_NOM,CTPRD_USR_N,CTPRD_FN)
SELECT 
_ctlprd_id AS CTPRD_ID, 
_ctlprd_a  AS CTPRD_A, 
_ctlprd_alm_id AS CTPRD_ALM_ID, 
_ctlprd_tipo   AS CTPRD_TIPO, 
_ctlprd_subid  AS CTPRD_CTCLS_ID, 
_ctlprv_cnvr   AS CTPRD_CNVR, 
_ctlprd_clv    AS CTPRD_CLV, 
_ctlprd_nom    AS CTPRD_NOM, 
_ctlprd_usr_n  AS CTPRD_USR_N, 
_ctlprd_fn     AS CTPRD_FN
FROM _ctlprd	

INSERT INTO CTMDL (
CTMDL_ID, 
CTMDL_CTCLS_ID, 
CTMDL_CTPRD_ID, 
CTMDL_CTPRC_ID, 
CTMDL_ATRZD, 
CTMDL_ALM_ID, 
CTMDL_A, 
CTMDL_TIPO, 
CTMDL_VIS, 
CTMDL_UNDDXY, 
CTMDL_UNDDZ, 
CTMDL_UNDDA, 
CTMDL_RETISR, 
CTMDL_RETISRF,
CTMDL_RETIVA, 
CTMDL_RETIVAF, 
CTMDL_TRSIVA, 
CTMDL_TRSIVAF, 
CTMDL_RETIEPS, 
CTMDL_RETIEPSF,  
CTMDL_TRSIEPS, 
CTMDL_TRSIEPSF, 
CTMDL_UNTR, 
CTMDL_UNTR1, 
CTMDL_UNTR2, 
CTMDL_CANT1, 
CTMDL_CANT2, 
CTMDL_EXT, 
CTMDL_MIN, 
CTMDL_MAX,
CTMDL_REORD, 
CTMDL_LARGO, 
CTMDL_ANCHO, 
CTMDL_ALTO, 
CTMDL_PESO, 
CTMDL_CLVUND, 
CTMDL_CLVPRDS, 
CTMDL_CLVOBJ, 
CTMDL_CDG, 
CTMDL_UNDD, 
CTMDL_CTAPRE, 
CTMDL_SKU, 
CTMDL_NUMREQ, 
CTMDL_CLV, 
CTMDL_MRC, 
CTMDL_ESPC, 
CTMDL_DSCRC, 
CTMDL_ETQTS, 
CTMDL_DSCR, 
CTMDL_USR_N, 
CTMDL_USR_M, 
CTMDL_FN, 
CTMDL_FM)
SELECT 
_ctlmdl_id AS CTMDL_ID, 
_ctlmdl_idcat AS CTMDL_CTCLS_ID, 
_ctlmdl_idpro AS CTMDL_CTPRD_ID, 
_ctlmdl_idpre AS CTMDL_CTPRC_ID, 
_ctlmdl_atrzd AS CTMDL_ATRZD, 
_ctlmdl_alm_id AS CTMDL_ALM_ID, 
_ctlmdl_a      AS CTMDL_A, 
_ctlmdl_tipo   AS CTMDL_TIPO, 
_ctlmdl_vis    AS CTMDL_VIS, 
_ctlmdl_unddxy AS CTMDL_UNDDXY, 
_ctlmdl_unddz  AS CTMDL_UNDDZ, 
_ctlmdl_undda  AS CTMDL_UNDDA, 
_ctlmdl_retisr AS CTMDL_RETISR, 
_ctlmdl_retisrf AS CTMDL_RETISRF,
_ctlmdl_retiva  AS CTMDL_RETIVA, 
_ctlmdl_retivaf AS CTMDL_RETIVAF, 
_ctlmdl_trsiva AS CTMDL_TRSIVA, 
_ctlmdl_trsivaf AS CTMDL_TRSIVAF, 
_ctlmdl_retieps AS CTMDL_RETIEPS, 
_ctlmdl_retiepsf AS CTMDL_RETIEPSF,  
_ctlmdl_trsieps  AS CTMDL_TRSIEPS, 
_ctlmdl_trsiepsf AS CTMDL_TRSIEPSF, 
_ctlmdl_untr   AS CTMDL_UNTR, 
_ctlmdl_untr1  AS CTMDL_UNTR1, 
_ctlmdl_untr2  AS CTMDL_UNTR2, 
_ctlmdl_cant1  AS CTMDL_CANT1, 
_ctlmdl_cant2  AS CTMDL_CANT2, 
_ctlmdl_ext    AS CTMDL_EXT, 
_ctlmdl_min    AS CTMDL_MIN, 
_ctlmdl_max    AS CTMDL_MAX,
_ctlmdl_reord  AS CTMDL_REORD, 
_ctlmdl_largo  AS CTMDL_LARGO, 
_ctlmdl_ancho  AS CTMDL_ANCHO, 
_ctlmdl_alto   AS CTMDL_ALTO, 
_ctlmdl_peso   AS CTMDL_PESO, 
_ctlmdl_clvund AS CTMDL_CLVUND, 
_ctlmdl_clvprds AS CTMDL_CLVPRDS, 
_ctlmdl_clvobj  AS CTMDL_CLVOBJ, 
_ctlmdl_cdg    AS CTMDL_CDG, 
_ctlmdl_undd   AS CTMDL_UNDD, 
_ctlmdl_ctapre AS CTMDL_CTAPRE, 
_ctlmdl_sku    AS CTMDL_SKU, 
_ctlmdl_numreq AS CTMDL_NUMREQ, 
_ctlmdl_clv    AS CTMDL_CLV, 
_ctlmdl_mrc    AS CTMDL_MRC, 
_ctlmdl_espc   AS CTMDL_ESPC, 
_ctlmdl_dscrc  AS CTMDL_DSCRC, 
_ctlmdl_etqts  AS CTMDL_ETQTS, 
_ctlmdl_dscr   AS CTMDL_DSCR, 
_ctlmdl_usr_n  AS CTMDL_USR_N, 
_ctlmdl_usr_m  AS CTMDL_USR_M, 
_ctlmdl_fn     AS CTMDL_FN, 
_ctlmdl_fm     AS CTMDL_FM
FROM _ctlmdl


INSERT INTO CTMDLT (CTMDLT_CTMDL_ID, CTMDLT_VIS, CTMDLT_SEC, CTMDLT_LARGO, CTMDLT_ANCHO, CTMDLT_ALTO, CTMDLT_PESO, CTMDLT_SKU, CTMDLT_DSCRC, CTMDLT_DSCR, CTMDLT_ETQTS, CTMDLT_USR_M, CTMDLT_FM)
SELECT 
CTMDL_ID AS CTMDLT_CTMDL_ID, 
CTMDL_VIS AS CTMDLT_VIS, 
CTMDL_SEC AS CTMDLT_SEC , 
CTMDL_LARGO AS CTMDLT_LARGO, 
CTMDL_ANCHO AS CTMDLT_ANCHO, 
CTMDL_ALTO  AS CTMDLT_ALTO, 
CTMDL_PESO  AS CTMDLT_PESO, 
CTMDL_SKU   AS CTMDLT_SKU, 
CTMDL_DSCRC AS CTMDLT_DSCRC, 
CTMDL_DSCR  AS CTMDLT_DSCR, 
CTMDL_ETQTS AS CTMDLT_ETQTS, 
CTMDL_USR_M AS CTMDLT_USR_M, 
CTMDL_FM    AS CTMDLT_FM
 FROM CTMDL
 WHERE CTMDL_ID >=2319