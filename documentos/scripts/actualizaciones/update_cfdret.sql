/* Script creado para actualizar el esquema de la tabla _cfdret (Comprobante de Retenciones) */

ALTER TABLE _cfdret
	ADD COLUMN _cfdret_comrel TEXT NULL DEFAULT NULL COMMENT 'Comprobantes relacionados (JSON)' AFTER _cfdret_compl,
	ADD COLUMN _cfdret_domfis VARCHAR(5) NULL DEFAULT NULL COMMENT 'Domicilio fiscal del receptor' AFTER _cfdret_nomr,
	ADD COLUMN _cfdret_utldd NUMERIC(14,4) NULL DEFAULT NULL COMMENT 'Utilidad bimestral' AFTER _cfdret_tot,
	ADD COLUMN _cfdret_isrc NUMERIC(14,4) NULL DEFAULT NULL COMMENT 'ISR Correspondiente' AFTER _cfdret_utldd,
	ADD COLUMN _cfdret_lgrexp VARCHAR(5) NULL DEFAULT NULL COMMENT 'Lugar de expedicion del comprobante de retenciones' AFTER _cfdret_uuid;