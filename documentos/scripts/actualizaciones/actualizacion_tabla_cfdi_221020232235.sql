/*
*** ACTUALIZACION DE CAMPO FOLIO (_cfdi_folio) ahora la longitug debe de ser de 40 caracteres
*/
ALTER TABLE `_cfdi`
	CHANGE COLUMN `_cfdi_folio` `_cfdi_folio` VARCHAR(40) NULL DEFAULT NULL COMMENT 'para control interno del contribuyente que acepta un valor numerico entero superior a 0 que expresa el folio del comprobante' COLLATE 'utf8_general_ci' AFTER `_cfdi_moneda`;
