﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:52:03
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCFRM"
#

DROP TABLE IF EXISTS `BNCFRM`;
CREATE TABLE `BNCFRM` (
  `BNCFRM_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `BNCFRM_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCFRM_AFS` int(1) NOT NULL DEFAULT 1 COMMENT 'afectar saldo de la cuenta',
  `BNCFRM_CLV` varchar(3) NOT NULL COMMENT 'clave de forma de pago',
  `BNCFRM_CLVS` varchar(3) NOT NULL COMMENT 'clave SAT',
  `BNCFRM_NOM` varchar(64) NOT NULL COMMENT 'descripcion de la forma de pago',
  `BNCFRM_FN` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `BNCFRM_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCFRM_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCFRM_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCFRM_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Bancos: formas de pago';