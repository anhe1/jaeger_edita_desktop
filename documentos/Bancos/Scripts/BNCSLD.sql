﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:52:41
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCSLD"
#

DROP TABLE IF EXISTS `BNCSLD`;
CREATE TABLE `BNCSLD` (
  `BNCSLD_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice',
  `BNCSLD_SBID` int(11) NOT NULL COMMENT 'indice de relacion de la cuenta de bancos.',
  `BNCSLD_ANIO` int(2) NOT NULL COMMENT 'ejecicio',
  `BNCSLD_MES` int(2) NOT NULL COMMENT 'periodo',
  `BNCSLD_INICIAL` decimal(14,4) NOT NULL COMMENT 'saldo inicial de la cuenta',
  `BNCSLD_FINAL` decimal(14,4) DEFAULT NULL COMMENT 'saldo final de la cuenta',
  `BNCSLD_FS` datetime NOT NULL COMMENT 'fecha inicial del saldo',
  `BNCSLD_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del nuevo registro',
  `BNCSLD_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCSLD_NOTA` varchar(255) DEFAULT NULL COMMENT 'nota',
  PRIMARY KEY (`BNCSLD_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: saldos del control de bancos';