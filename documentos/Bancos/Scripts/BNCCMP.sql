﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:51:28
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCCMP"
#

DROP TABLE IF EXISTS `BNCCMP`;
CREATE TABLE `BNCCMP` (
  `BNCCMP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
  `BNCCMP_A` smallint(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCMP_SUBID` int(11) NOT NULL COMMENT 'indice de relacion con el movimiento bancario',
  `BNCCMP_NOIDEN` varchar(11) DEFAULT NULL COMMENT 'identificador de la prepoliza',
  `BNCCMP_VER` varchar(3) DEFAULT NULL COMMENT 'registro activo',
  `BNCCMP_EFECTO` varchar(10) NOT NULL COMMENT 'efecto del comprobante',
  `BNCCMP_SBTP` int(11) NOT NULL COMMENT 'subtipo del comprobante, emitido, recibido, nomina, etc',
  `BNCCMP_TIPO` varchar(10) NOT NULL COMMENT 'tipo de comprobante',
  `BNCCMP_IDCOM` int(11) NOT NULL COMMENT 'indice del comprobante fiscal',
  `BNCCMP_FOLIO` varchar(22) DEFAULT NULL COMMENT 'folio del comprobante',
  `BNCCMP_SERIE` varchar(25) DEFAULT NULL COMMENT 'serie del comprobante',
  `BNCCMP_RFCE` varchar(15) NOT NULL COMMENT 'rfc emisor del comprobante',
  `BNCCMP_EMISOR` varchar(255) NOT NULL COMMENT 'nombre del emisor del comprobante',
  `BNCCMP_RFCR` varchar(15) NOT NULL COMMENT 'rfc del receptor del comprobante',
  `BNCCMP_RECEPTOR` varchar(255) DEFAULT NULL COMMENT 'nombre del receptor del comprobante',
  `BNCCMP_FECEMS` datetime NOT NULL COMMENT 'fecha de emision del comprobante',
  `BNCCMP_MTDPG` varchar(3) DEFAULT NULL COMMENT 'clave del metodo de pago',
  `BNCCMP_FRMPG` varchar(2) DEFAULT NULL COMMENT 'clave de la forma de pago',
  `BNCCMP_MONEDA` varchar(3) DEFAULT NULL COMMENT 'clave de moneda',
  `BNCCMP_UUID` varchar(36) DEFAULT NULL COMMENT 'folio fiscal (uuid)',
  `BNCCMP_PAR` int(11) DEFAULT NULL COMMENT 'numero de partida',
  `BNCCMP_TOTAL` decimal(11,4) NOT NULL COMMENT 'total del comprobante',
  `BNCCMP_XCBR` decimal(11,4) NOT NULL COMMENT 'importe por cobrar una vez aplicado el descuento',
  `BNCCMP_CARGO` decimal(11,4) DEFAULT NULL COMMENT 'cargo',
  `BNCCMP_ABONO` decimal(11,4) DEFAULT NULL COMMENT 'abono',
  `BNCCMP_CBRD` decimal(11,4) DEFAULT NULL COMMENT 'acumulado del comprobante',
  `BNCCMP_ESTADO` varchar(10) DEFAULT NULL COMMENT 'ultimo estado del comprobante',
  `BNCCMP_NOTA` varchar(255) DEFAULT NULL,
  `BNCCMP_FN` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `BNCCMP_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCCMP_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCCMP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCMP_ID`) USING BTREE,
  KEY `BNCCMP_SUBID` (`BNCCMP_SUBID`) USING BTREE,
  KEY `BNCCMP_IDCOM` (`BNCCMP_IDCOM`) USING BTREE,
  KEY `BNCCMP_NOIDEN` (`BNCCMP_NOIDEN`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='comprobantes relacionados a movmientos bancarios';