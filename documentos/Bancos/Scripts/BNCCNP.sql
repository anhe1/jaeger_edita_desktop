﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:51:38
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCCNP"
#

DROP TABLE IF EXISTS `BNCCNP`;
CREATE TABLE `BNCCNP` (
  `BNCCNP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del concepto',
  `BNCCNP_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCNP_R` smallint(6) NOT NULL DEFAULT 0 COMMENT 'registro de solo lectura',
  `BNCCNP_REQ` int(1) NOT NULL DEFAULT 0 COMMENT 'requiere comprobante',
  `BNCCNP_AFS` int(1) NOT NULL DEFAULT 0 COMMENT 'afectactacion de saldos de comprobantes',
  `BNCCNP_IDSBT` int(11) NOT NULL DEFAULT 0 COMMENT 'sub tipo de comprobante',
  `BNCCNP_STTS_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice del status del comporbante',
  `BNCCNP_IDOP` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de tipo de operacion',
  `BNCCNP_IDTP` int(11) NOT NULL DEFAULT 1 COMMENT 'indice de tipo o efecto',
  `BNCCNP_IDTPCMB` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de tipo de comprobante',
  `BNCCNP_FRMT_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'nombre del formato impreso',
  `BNCCNP_IVA` decimal(14,4) NOT NULL DEFAULT 0.0000 COMMENT 'porcentaje del iva aplicable a la operacion',
  `BNCCNP_TPCMP` varchar(40) DEFAULT NULL COMMENT 'tipos de comprobante aceptado por el movimiento',
  `BNCCNP_STATUS` varchar(40) DEFAULT NULL COMMENT 'status de los comprobantes',
  `BNCCNP_RLCCMP` varchar(40) DEFAULT NULL COMMENT 'lista de indice de relacion entre comprobantes',
  `BNCCNP_CLASS` varchar(3) DEFAULT NULL COMMENT 'clasificacion',
  `BNCCNP_DESC` varchar(40) NOT NULL COMMENT 'descipcion del concepto',
  `BNCCNP_NOM` varchar(40) NOT NULL COMMENT 'nombre, concepto o descripcion',
  `BNCCNP_CNTA` varchar(40) DEFAULT NULL COMMENT 'cuenta contable',
  `BNCCNP_RLCDIR` varchar(20) DEFAULT NULL COMMENT 'objetos de relacion del directorio',
  `BNCCNP_FN` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `BNCCNP_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario creador del registro',
  `BNCCNP_FM` datetime DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `BNCCNP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCNP_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Bancos: conceptos bancarios';
