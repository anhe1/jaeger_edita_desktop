﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:51:48
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCCTA"
#

DROP TABLE IF EXISTS `BNCCTA`;
CREATE TABLE `BNCCTA` (
  `BNCCTA_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
  `BNCCTA_A` smallint(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCTA_R` smallint(6) NOT NULL DEFAULT 0 COMMENT 'registro de solo lectura',
  `BNCCTA_E` smallint(1) NOT NULL DEFAULT 0 COMMENT 'indicar si el banco es extranjero',
  `BNCCTA_DCRT` smallint(2) NOT NULL DEFAULT 1 COMMENT 'dia del corte',
  `BNCCTA_NOCHQ` int(11) DEFAULT NULL COMMENT 'numero de cheque',
  `BNCCTA_SLDINI` decimal(11,4) NOT NULL DEFAULT 0.0000 COMMENT 'saldo inicial',
  `BNCCTA_CLVB` varchar(3) DEFAULT NULL COMMENT 'clave del banco segun catalogo del SAT',
  `BNCCTA_CLV` varchar(3) DEFAULT NULL COMMENT 'clave de la moneda a utilizar segun catalgo SAT',
  `BNCCTA_RFCB` varchar(14) DEFAULT NULL COMMENT 'rfc del beneficiario de la cuenta de banco',
  `BNCCTA_RFC` varchar(14) DEFAULT NULL COMMENT 'rfc del banco',
  `BNCCTA_NUMCLI` varchar(14) DEFAULT NULL COMMENT 'numero de identificación del cliente, numero de contrato',
  `BNCCTA_CLABE` varchar(18) DEFAULT NULL COMMENT 'cuenta clabe',
  `BNCCTA_NUMCTA` varchar(20) DEFAULT NULL COMMENT 'numero de cuenta',
  `BNCCTA_SCRSL` varchar(20) DEFAULT NULL COMMENT 'sucursal bancaria',
  `BNCCTA_FUNC` varchar(20) DEFAULT NULL COMMENT 'nombre del asesor o funcionario',
  `BNCCTA_PLAZA` varchar(20) DEFAULT NULL COMMENT 'numero de plaza',
  `BNCCTA_TEL` varchar(20) DEFAULT NULL COMMENT 'telefono de contacto',
  `BNCCTA_ALIAS` varchar(20) DEFAULT NULL COMMENT 'alias de la cuenta',
  `BNCCTA_CNTA` varchar(40) DEFAULT NULL COMMENT 'numero de cuenta contable',
  `BNCCTA_BANCO` varchar(64) DEFAULT NULL COMMENT 'nombre del banco',
  `BNCCTA_BENEF` varchar(256) DEFAULT NULL COMMENT 'nombre del beneficiario de la cuenta de banco',
  `BNCCTA_FECAPE` datetime DEFAULT NULL COMMENT 'fecha de apertura',
  `BNCCTA_FN` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `BNCCTA_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCCTA_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCCTA_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCTA_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='Bancos: registro de cuentas bancarias';