﻿# Host: grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com:4530  (Version 5.5.5-10.4.34-MariaDB-log)
# Date: 2024-12-11 12:51:17
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "BNCAUX"
#

DROP TABLE IF EXISTS `BNCAUX`;
CREATE TABLE `BNCAUX` (
  `BNCAUX_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del movimiento',
  `BNCAUX_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCAUX_SBID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de relacion con la tabla de movimientos',
  `BNCAUX_NOIDEN` varchar(11) DEFAULT NULL COMMENT 'identificador de la prepoliza',
  `BNCAUX_DESC` varchar(256) NOT NULL COMMENT 'descripcion del auxiliar',
  `BNCAUX_NOM` varchar(256) NOT NULL COMMENT 'nombre del archivo',
  `BNCAUX_TP` varchar(32) DEFAULT NULL COMMENT 'descripcion del movimiento',
  `BNCAUX_URL` text DEFAULT NULL COMMENT 'url de descarga para el archivo',
  `BNCAUX_FN` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `BNCAUX_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  PRIMARY KEY (`BNCAUX_ID`) USING BTREE,
  KEY `BNCAUX_SBID` (`BNCAUX_SBID`) USING BTREE,
  KEY `BNCCMP_NOIDEN` (`BNCAUX_NOIDEN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='documentos relacionados a movmientos bancarios';