﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// repositorio de catalogo de productos
    /// </summary>
    public class SqlFbProdServRepository : RepositoryMaster<ProductoServicioModel>, ISqlProdServRepository {
        #region declaraciones
        protected ISqlModelosRepository modelosRepository;
        protected ISqlMediosRepository mediosRepository;
        #endregion

        public SqlFbProdServRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.modelosRepository = new SqlFbModelosRepository(configuracion, user);
            this.mediosRepository = new SqlFbMediosRepository(configuracion, user);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTPRD WHERE CTPRD_ID = @CTPRD_ID"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRD_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ProductoServicioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTPRD (CTPRD_ID, CTPRD_A, CTPRD_ALM_ID, CTPRD_CTCLS_ID, CTPRD_TIPO, CTPRD_CLV, CTPRD_NOM, CTPRD_USR_N, CTPRD_FN, CTPRD_SEC) 
                                          VALUES (@CTPRD_ID,@CTPRD_A,@CTPRD_ALM_ID,@CTPRD_CTCLS_ID,@CTPRD_TIPO,@CTPRD_CLV,@CTPRD_NOM,@CTPRD_USR_N,@CTPRD_FN,@CTPRD_SEC) RETURNING CTPRD_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRD_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTPRD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTPRD_CTCLS_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTPRD_TIPO", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTPRD_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@CTPRD_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_SEC", item.Secuencia);
            item.IdProducto = ExecuteScalar(sqlCommand);
            if (item.IdProducto > 0)
                return item.IdProducto;
            return 0;
        }

        public int Update(ProductoServicioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTPRD SET CTPRD_A = @CTPRD_A, CTPRD_ALM_ID = @CTPRD_ALM_ID, CTPRD_CTCLS_ID = @CTPRD_CTCLS_ID, CTPRD_TIPO = @CTPRD_TIPO, CTPRD_CLV = @CTPRD_CLV, CTPRD_NOM = @CTPRD_NOM, CTPRD_USR_N = @CTPRD_USR_N, CTPRD_FN = @CTPRD_FN, CTPRD_SEC = @CTPRD_SEC WHERE (CTPRD_ID = @CTPRD_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTPRD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTPRD_CTCLS_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTPRD_TIPO", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTPRD_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@CTPRD_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_SEC", item.Secuencia);
            return ExecuteTransaction(sqlCommand);
        }

        public ProductoServicioModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 CTPRD.* FROM CTPRD WHERE CTPRD_ID = @CTPRD_ID"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRD_ID", index);
            return GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<ProductoServicioModel> GetList() {
            return GetList<ProductoServicioModel>(new List<IConditional>());
        }
        #endregion

        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CTPRD @wcondiciones ORDER BY CTPRD.CTPRD_NOM ASC"
            };
            if (typeof(T1) == typeof(ProductoModeloExistenciaModel)) {
                sqlCommand.CommandText = @"SELECT CTPRD.*, CTMDL.*, CTMDLX.*, (CTPRD.CTPRD_A*CTMDL.CTMDL_A) AS ACTIVO FROM CTPRD LEFT JOIN CTMDL ON CTMDL.CTMDL_CTPRD_ID = CTPRD.CTPRD_ID LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID = CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID = CTPRD.CTPRD_ID @wcondiciones";
            }
            var result = GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
            return result;
        }

        public ProductoServicioDetailModel Save(ProductoServicioDetailModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.IdProducto = Insert(model);
            } else {
                Update(model);
            }
            // modelos
            for (int i = 0; i < model.Modelos.Count; i++) {
                model.Modelos[i].IdProducto = model.IdProducto;
                model.Modelos[i].IdCategoria = model.IdCategoria;
                model.Modelos[i] = modelosRepository.Save(model.Modelos[i]);
            }

            return model;
        }

        public ProductoServicioModel Save(ProductoServicioModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.IdProducto = Insert(model);
            } else {
                Update(model);
            }
            return model;
        }
    }
}
