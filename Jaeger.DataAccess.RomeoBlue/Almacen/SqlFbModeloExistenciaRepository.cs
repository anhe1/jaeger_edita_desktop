﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    public class SqlFbModeloExistenciaRepository : RepositoryMaster<ModeloExistenciaModel>, ISqlModeloExistenciaRepository {
        public SqlFbModeloExistenciaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public int Insert(ModeloExistenciaModel item) {
            throw new NotImplementedException();
        }

        public int Update(ModeloExistenciaModel item) {
            throw new NotImplementedException();
        }

        public ModeloExistenciaModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<ModeloExistenciaModel> GetList() {
            return this.GetList<ModeloExistenciaModel>(new List<IConditional>());
        }
        #endregion

        public bool Existencia(List<IConditional> conditionals) {
            throw new NotImplementedException();
        }

        public IModeloExistenciaModel Save(ModeloExistenciaModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO CTMDLX (CTMDLX_CTPRD_ID, CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTALM_ID, CTMDLX_TPALM_ID, CTMDLX_EXT, CTMDLX_MIN, CTMDLX_MAX, CTMDLX_REORD) 
                                          VALUES (@CTMDLX_CTPRD_ID,@CTMDLX_CTMDL_ID,@CTMDLX_CTESPC_ID,@CTMDLX_CTALM_ID,@CTMDLX_TPALM_ID,@CTMDLX_EXT,@CTMDLX_MIN,@CTMDLX_MAX,@CTMDLX_REORD)
                                MATCHING (CTMDLX_CTPRD_ID, CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTALM_ID, CTMDLX_TPALM_ID)"
            };

            var parameters = new List<FbParameter>() {
                new FbParameter("@CTMDLX_CTPRD_ID", model.IdProducto),
                new FbParameter("@CTMDLX_CTMDL_ID", model.IdModelo),
                new FbParameter("@CTMDLX_CTESPC_ID", model.IdEspecificacion),
                new FbParameter("@CTMDLX_CTALM_ID", model.IdAlmacen),
                new FbParameter("@CTMDLX_TPALM_ID", model.IdTipoAlmacen),
                new FbParameter("@CTMDLX_EXT", model.Existencia),
                new FbParameter("@CTMDLX_MIN", model.Minimo),
                new FbParameter("@CTMDLX_MAX", model.Maximo),
                new FbParameter("@CTMDLX_REORD", model.ReOrden)
            };
            sqlCommand.Parameters.AddRange(parameters);
            this.ExecuteTransaction(sqlCommand);
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand();
            if (typeof(T1) == typeof(ModeloDetailModel)) {
                var sql = @"SELECT * FROM CTMDL LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID=CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID=CTMDL.CTMDL_CTPRD_ID @wcondiciones";
                sqlCommand.CommandText = sql;
                return GetMapper<T1>(sqlCommand, conditionals).ToList();
            } else if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                var sql = @"SELECT * FROM CTPRD P INNER JOIN CTMDL M ON (P.CTPRD_ID = M.CTMDL_CTPRD_ID) @wcondiciones";
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    sql = sql.Replace("@wcondiciones", "where (p.ctprd_nom like lower(concat('%',@search,'%'))) or lower((m.ctmdl_dscrc like concat('%',@search,'%'))) @condiciones");
                    sql = sql.Replace("@search", "'" + d0.FieldValue + "'");
                    conditionals = conditionals.Where(it => !it.FieldName.ToLower().Contains("@search")).ToList();
                }
                sqlCommand.CommandText = sql;
                return GetMapper<T1>(sqlCommand, conditionals).ToList();
            }
            return this.GetList<T1>(conditionals);
        }
    }
}
