﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    public class SqlFbRemisionStatusRepository : RepositoryMaster<RemisionStatusModel>, ISqlRemisionStatusRepository {
        public SqlFbRemisionStatusRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public RemisionStatusModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> condidional) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RMSNS.* FROM RMSNS @wcondiciones ORDER BY RMSNS_ID ASC"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, condidional);
            return GetMapper<T1>(sqlCommand);
        }

        public IEnumerable<RemisionStatusModel> GetList() {
            throw new NotImplementedException();
        }

        public int Insert(RemisionStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RMSNS (RMSNS_ID, RMSNS_A, RMSNS_RMSN_ID, RMSNS_RMSN_STTS_ID, RMSNS_RMSN_STTSA_ID, RMSNS_DRCTR_ID, RMSNS_CVMTV, RMSNS_NOTA, RMSNS_USR_N, RMSNS_FN)
                                          VALUES (@RMSNS_ID,@RMSNS_A,@RMSNS_RMSN_ID,@RMSNS_RMSN_STTS_ID,@RMSNS_RMSN_STTSA_ID,@RMSNS_DRCTR_ID,@RMSNS_CVMTV,@RMSNS_NOTA,@RMSNS_USR_N,@RMSNS_FN) RETURNING RMSNS_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSNS_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RMSNS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_STTS_ID", item.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_STTSA_ID", item.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@RMSNS_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSNS_CVMTV", item.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@RMSNS_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSNS_USR_N", item.User);
            sqlCommand.Parameters.AddWithValue("@RMSNS_FN", item.FechaNuevo);

            item.IdAuto = ExecuteScalar(sqlCommand);
            if (item.IdAuto > 0)
                return item.IdAuto;
            return 0;
        }

        public RemisionStatusModel Save(RemisionStatusModel item) {
            item.FechaNuevo = DateTime.Now;
            item.User = User;
            if (item.IdAuto == 0) {
                item.IdAuto = Insert(item);
            } else {
                Update(item);
            }
            if (item.IdAuto > 0) {
                Status(item);
            }
            return item;
        }

        public int Saveable(RemisionStatusModel item) {
            throw new NotImplementedException();
        }

        public int Update(RemisionStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSNS SET RMSNS_A = @RMSNS_A, RMSNS_RMSN_ID = @RMSNS_RMSN_ID, RMSNS_RMSN_STTS_ID = @RMSNS_RMSN_STTS_ID, RMSNS_RMSN_STTSA_ID = @RMSNS_RMSN_STTSA_ID, RMSNS_DRCTR_ID = @RMSNS_DRCTR_ID, RMSNS_CVMTV = @RMSNS_CVMTV, RMSNS_NOTA = @RMSNS_NOTA, RMSNS_USR_N = @RMSNS_USR_N, RMSNS_FN = @RMSNS_FN WHERE RMSNS_ID = @RMSNS_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSNS_ID", item.IdAuto);
            sqlCommand.Parameters.AddWithValue("@RMSNS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_STTS_ID", item.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@RMSNS_RMSN_STTSA_ID", item.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@RMSNS_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSNS_CVMTV", item.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@RMSNS_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSNS_USR_N", item.User);
            sqlCommand.Parameters.AddWithValue("@RMSNS_FN", item.FechaNuevo);
            return ExecuteTransaction(sqlCommand);
        }

        private bool Status(RemisionStatusModel model) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE RMSN SET RMSN_STTS_ID = @RMSN_STTS_ID WHERE RMSN_ID = @RMSN_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSN_ID", model.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSN_STTS_ID", model.IdStatusB);

            // en caso de ser cancelacion
            if (model.IdStatusB == 0) {
                sqlCommand.CommandText = "UPDATE RMSN SET RMSN_STTS_ID = @RMSN_STTS_ID, RMSN_USR_C = @RMSN_USR_C, RMSN_FCCNCL = @RMSN_FCCNCL WHERE RMSN_ID = @RMSN_ID";
                sqlCommand.Parameters.AddWithValue("@RMSN_USR_C", model.User);
                sqlCommand.Parameters.AddWithValue("@RMSN_FCCNCL", model.FechaNuevo);
            }
            return ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
