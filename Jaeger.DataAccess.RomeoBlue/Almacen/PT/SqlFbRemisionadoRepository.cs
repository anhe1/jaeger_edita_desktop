﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    /// <summary>
    /// repositorio Remisionado (RMSN)
    /// </summary>
    public class SqlFbRemisionadoRepository : RepositoryMaster<RemisionModel>, ISqlRemisionadoPTRepository {
        public SqlFbRemisionadoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public int Insert(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RMSN (RMSN_ID, RMSN_A, RMSN_CTDOC_ID, RMSN_DECI, RMSN_VER, RMSN_FOLIO, RMSN_CTSR_ID, RMSN_SERIE, RMSN_STTS_ID, RMSN_PDD_ID, RMSN_CTCLS_ID, RMSN_CTENV_ID, RMSN_DRCTR_ID, RMSN_DRCCN_ID, RMSN_VNDDR_ID, RMSN_GUIA_ID, RMSN_CTREL_ID, RMSN_RFCE, RMSN_RFCR, RMSN_NOMR, RMSN_CLVR, RMSN_CNTCT, RMSN_UUID, RMSN_FECEMS, RMSN_FECENT, RMSN_FECUPC, RMSN_FECCBR, RMSN_FCCNCL, RMSN_FECVNC, RMSN_TPCMB, RMSN_SBTTL, RMSN_DSCNT, RMSN_TRSIVA, RMSN_GTOTAL, RMSN_FACIVA, RMSN_IVAPAC, RMSN_TOTAL, RMSN_FACPAC, RMSN_XCBPAC, RMSN_DESC, RMSN_FACDES, RMSN_DESCT, RMSN_XCBRR, RMSN_CBRD, RMSN_MONEDA, RMSN_MTDPG, RMSN_FRMPG, RMSN_NOTA, RMSN_VNDDR, RMSN_USR_C, RMSN_FN, RMSN_USR_N, RMSN_CNDNS, RMSN_DRCCN) 
                                         VALUES (@RMSN_ID,@RMSN_A,@RMSN_CTDOC_ID,@RMSN_DECI,@RMSN_VER,@RMSN_FOLIO,@RMSN_CTSR_ID,@RMSN_SERIE,@RMSN_STTS_ID,@RMSN_PDD_ID,@RMSN_CTCLS_ID,@RMSN_CTENV_ID,@RMSN_DRCTR_ID,@RMSN_DRCCN_ID,@RMSN_VNDDR_ID,@RMSN_GUIA_ID,@RMSN_CTREL_ID,@RMSN_RFCE,@RMSN_RFCR,@RMSN_NOMR,@RMSN_CLVR,@RMSN_CNTCT,@RMSN_UUID,@RMSN_FECEMS,@RMSN_FECENT,@RMSN_FECUPC,@RMSN_FECCBR,@RMSN_FCCNCL,@RMSN_FECVNC,@RMSN_TPCMB,@RMSN_SBTTL,@RMSN_DSCNT,@RMSN_TRSIVA,@RMSN_GTOTAL,@RMSN_FACIVA,@RMSN_IVAPAC,@RMSN_TOTAL,@RMSN_FACPAC,@RMSN_XCBPAC,@RMSN_DESC,@RMSN_FACDES,@RMSN_DESCT,@RMSN_XCBRR,@RMSN_CBRD,@RMSN_MONEDA,@RMSN_MTDPG,@RMSN_FRMPG,@RMSN_NOTA,@RMSN_VNDDR,@RMSN_USR_C,@RMSN_FN,@RMSN_USR_N,@RMSN_CNDNS,@RMSN_DRCCN) RETURNING RMSN_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@RMSN_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RMSN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTDOC_ID", item.IdTipoDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSN_DECI", item.PrecisionDecimal);
            sqlCommand.Parameters.AddWithValue("@RMSN_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@RMSN_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTSR_ID", item.IdSerie);
            sqlCommand.Parameters.AddWithValue("@RMSN_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSN_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSN_PDD_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTCLS_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTENV_ID", item.IdMetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCCN_ID", item.IdDomicilio);
            sqlCommand.Parameters.AddWithValue("@RMSN_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSN_GUIA_ID", item.NoGuia);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTREL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSN_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSN_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSN_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSN_CLVR", item.ReceptorClave);
            sqlCommand.Parameters.AddWithValue("@RMSN_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@RMSN_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECENT", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECUPC", item.FechaUltPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECCBR", item.FechaCobranza);
            sqlCommand.Parameters.AddWithValue("@RMSN_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECVNC", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@RMSN_TPCMB", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@RMSN_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_DSCNT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@RMSN_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@RMSN_GTOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACIVA", item.TasaIVAPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_IVAPAC", item.TotalIVAPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_TOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACPAC", item.FactorPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_XCBPAC", item.PorCobrarPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_DESC", item.DescuentoTipo);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACDES", item.DescuentoFactor);
            sqlCommand.Parameters.AddWithValue("@RMSN_DESCT", item.DescuentoTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_XCBRR", item.PorCobrar);
            sqlCommand.Parameters.AddWithValue("@RMSN_CBRD", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@RMSN_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@RMSN_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_FRMPG", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_CNDNS", item.CondicionPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCCN", item.DomicilioEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSN_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSN_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSN_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@RMSN_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@RMSN_USR_N", item.Creo);

            item.IdRemision = ExecuteScalar(sqlCommand);
            if (item.IdRemision > 0)
                return item.IdRemision;
            return 0;
        }

        public int Update(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSN SET RMSN_A = @RMSN_A, RMSN_CTDOC_ID = @RMSN_CTDOC_ID, RMSN_DECI = @RMSN_DECI, RMSN_VER = @RMSN_VER, RMSN_FOLIO = @RMSN_FOLIO, RMSN_CTSR_ID = @RMSN_CTSR_ID, RMSN_SERIE = @RMSN_SERIE, 
RMSN_STTS_ID = @RMSN_STTS_ID, RMSN_PDD_ID = @RMSN_PDD_ID, RMSN_CTCLS_ID = @RMSN_CTCLS_ID, RMSN_CTENV_ID = @RMSN_CTENV_ID, RMSN_DRCTR_ID = @RMSN_DRCTR_ID, RMSN_DRCCN_ID = @RMSN_DRCCN_ID, RMSN_VNDDR_ID = @RMSN_VNDDR_ID, 
RMSN_GUIA_ID = @RMSN_GUIA_ID, RMSN_CTREL_ID = @RMSN_CTREL_ID, RMSN_RFCE = @RMSN_RFCE, RMSN_RFCR = @RMSN_RFCR, RMSN_NOMR = @RMSN_NOMR, RMSN_CLVR = @RMSN_CLVR, RMSN_CNTCT = @RMSN_CNTCT, RMSN_UUID = @RMSN_UUID, RMSN_FECEMS = @RMSN_FECEMS, 
RMSN_FECENT = @RMSN_FECENT, RMSN_FECUPC = @RMSN_FECUPC, RMSN_FECCBR = @RMSN_FECCBR, RMSN_FCCNCL = @RMSN_FCCNCL, RMSN_FECVNC = @RMSN_FECVNC, RMSN_TPCMB = @RMSN_TPCMB, RMSN_SBTTL = @RMSN_SBTTL, RMSN_DSCNT = @RMSN_DSCNT, RMSN_TRSIVA = @RMSN_TRSIVA, 
RMSN_GTOTAL = @RMSN_GTOTAL, RMSN_FACIVA = @RMSN_FACIVA, RMSN_IVAPAC = @RMSN_IVAPAC, RMSN_TOTAL = @RMSN_TOTAL, RMSN_FACPAC = @RMSN_FACPAC, RMSN_XCBPAC = @RMSN_XCBPAC, RMSN_DESC = @RMSN_DESC, RMSN_FACDES = @RMSN_FACDES, RMSN_DESCT = @RMSN_DESCT, 
RMSN_XCBRR = @RMSN_XCBRR, RMSN_CBRD = @RMSN_CBRD, RMSN_MONEDA = @RMSN_MONEDA, RMSN_MTDPG = @RMSN_MTDPG, RMSN_FRMPG = @RMSN_FRMPG, RMSN_NOTA = @RMSN_NOTA, RMSN_VNDDR = @RMSN_VNDDR, RMSN_USR_C = @RMSN_USR_C, RMSN_FM = @RMSN_FM, RMSN_USR_M = @RMSN_USR_M, 
RMSN_CNDNS = @RMSN_CNDNS, RMSN_DRCCN = @RMSN_DRCCN WHERE ((RMSN_ID = @RMSN_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTDOC_ID", item.IdTipoDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSN_DECI", item.PrecisionDecimal);
            sqlCommand.Parameters.AddWithValue("@RMSN_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@RMSN_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTSR_ID", item.IdSerie);
            sqlCommand.Parameters.AddWithValue("@RMSN_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSN_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSN_PDD_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTCLS_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTENV_ID", item.IdMetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCCN_ID", item.IdDomicilio);
            sqlCommand.Parameters.AddWithValue("@RMSN_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSN_GUIA_ID", item.NoGuia);
            sqlCommand.Parameters.AddWithValue("@RMSN_CTREL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSN_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSN_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSN_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSN_CLVR", item.ReceptorClave);
            sqlCommand.Parameters.AddWithValue("@RMSN_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@RMSN_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECENT", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECUPC", item.FechaUltPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECCBR", item.FechaCobranza);
            sqlCommand.Parameters.AddWithValue("@RMSN_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@RMSN_FECVNC", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@RMSN_TPCMB", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@RMSN_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_DSCNT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@RMSN_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@RMSN_GTOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACIVA", item.TasaIVAPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_IVAPAC", item.TotalIVAPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_TOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACPAC", item.FactorPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_XCBPAC", item.PorCobrarPactado);
            sqlCommand.Parameters.AddWithValue("@RMSN_DESC", item.DescuentoTipo);
            sqlCommand.Parameters.AddWithValue("@RMSN_FACDES", item.DescuentoFactor);
            sqlCommand.Parameters.AddWithValue("@RMSN_DESCT", item.DescuentoTotal);
            sqlCommand.Parameters.AddWithValue("@RMSN_XCBRR", item.PorCobrar);
            sqlCommand.Parameters.AddWithValue("@RMSN_CBRD", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@RMSN_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@RMSN_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_FRMPG", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_CNDNS", item.CondicionPago);
            sqlCommand.Parameters.AddWithValue("@RMSN_DRCCN", item.DomicilioEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSN_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSN_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSN_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@RMSN_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSN_USR_M", item.Modifica);
            return ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM RMSN WHERE ((RMSN_ID = @RMSN_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@RMSN_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionDetailModel GetById(int index) {
            return GetList<RemisionDetailModel>(new List<IConditional>() {
                new Conditional("RMSN_ID", index.ToString()) }).FirstOrDefault();
        }

        RemisionModel IGenericRepository<RemisionModel>.GetById(int index) {
            return GetList<RemisionDetailModel>(new List<IConditional>() {
                new Conditional("RMSN_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<RemisionModel> GetList() {
            return GetList<RemisionDetailModel>(new List<IConditional>());
        }
        #endregion

        /// <summary>
        /// obtener listado por condiciones
        /// </summary>
        /// <typeparam name="T1">objetos aceptados Remision y Remision Partida</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RMSN.*, RMSNC.* FROM RMSN LEFT JOIN RMSNC ON RMSNC.RMSNC_RMSN_ID = RMSN.RMSN_ID @wcondiciones ORDER BY RMSN_FOLIO DESC"
            };

            if (typeof(T1) == typeof(RemisionPartidaModel)) { // solo registros activos
                sqlCommand.CommandText = @"SELECT RMSN.*, MVAPT.* FROM RMSN LEFT JOIN MVAPT ON RMSN.RMSN_ID = MVAPT.MVAPT_RMSN_ID AND MVAPT.MVAPT_DOC_ID = 26 AND MVAPT.MVAPT_A = 1 @wcondiciones";
            } else if (typeof(T1) == typeof(RemisionReceptorModel)) {
                sqlCommand.CommandText = @"SELECT RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR FROM RMSN @wcondiciones GROUP BY RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR  ORDER BY RMSN.RMSN_NOMR ASC";
            }
            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        /// <summary>
        /// obtener listado por receptores
        /// </summary>
        /// <typeparam name="T1">objetos aceptados RemisionReceptor</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        public IEnumerable<T1> GetListR<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR FROM RMSN @wcondiciones GROUP BY RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR  ORDER BY RMSN.RMSN_NOMR ASC"
            };

            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public bool Update(string fieldName, object fieldValue, int indexPK) {
            var _sqlCommand = new FbCommand {
                CommandText = string.Format("UPDATE RMSN SET {0} = @{0} WHERE RMSN_ID = @index", fieldName)
            };
            _sqlCommand.Parameters.AddWithValue("@" + fieldName, fieldValue);
            _sqlCommand.Parameters.AddWithValue("@index", indexPK);
            return ExecuteTransaction(_sqlCommand) > 0;
        }

        public bool Update(IRemisionStatusDetailModel model) {
            var sqlCommand = new FbCommand() {
                CommandText = @"UPDATE RMSN SET RMSN_STTS_ID = @RMSN_STTS_ID, RMSN_USR_C = @RMSN_USR_C, RMSN_CVCAN = @RMSN_CVCAN, RMSN_CLMTV = @RMSN_CLMTV, RMSN_CLNTA = @RMSN_CLNTA, RMSN_FCCNCL = @RMSN_FCCNCL WHERE RMSN_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@RMSN_STTS_ID", model.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@RMSN_USR_C", model.User);
            sqlCommand.Parameters.AddWithValue("@RMSN_FCCNCL", model.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CLMTV", model.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@RMSN_CLNTA", model.IdRemision);
            sqlCommand.Parameters.AddWithValue("@index", model.IdRemision);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionDetailModel Save(RemisionDetailModel remision) {
            if (remision.IdRemision == 0) {
                // el folio tambien esta disparador en la base
                remision.Folio = this.Folio(remision);
                remision.FechaNuevo = DateTime.Now;
                remision.Creo = User;
                remision.IdRemision = Insert(remision);
            } else {
                remision.FechaModifica = DateTime.Now;
                remision.Modifica = User;
                Update(remision);
            }
            return remision;
        }

        public bool UploadPDF(IUploadPDF model) {
            var _sqlCommand = new FbCommand {
                CommandText = "UPDATE RMSN SET RMSN_URL_PDF = @pdf WHERE RMSN_ID = @index"
            };
            _sqlCommand.Parameters.AddWithValue("@pdf", model.UrlFilePDF);
            _sqlCommand.Parameters.AddWithValue("@index", model.IdRemision);
            return ExecuteTransaction(_sqlCommand) > 0;
        }

        public int Folio(IRemisionDetailModel item) {
            try {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT MAX(RMSN.RMSN_FOLIO) + 1 FROM RMSN"
                };
                return this.ExecuteScalar(sqlCommand);
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }
    }
}
