﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.DataBase.Contracts;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    public class SqlFbValeAlmacenRepository : RepositoryMaster<ValeAlmacenModel>, ISqlValeAlmacenRepository {
        #region 
        protected internal ISqlMovimientoAlmacenPT movimientoAlmacenPT;
        protected internal ISqlValeRelacionRepository sqlValeRelacionRepository;
        #endregion

        public SqlFbValeAlmacenRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
            this.movimientoAlmacenPT = new SqlFbMovimientoAlmacenRepository(configuracion, user);
            this.sqlValeRelacionRepository = new SqlFbValeRelacionRepository(configuracion, user);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM ALMPT WHERE ((ALMPT_ID = @ALMPT_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@ALMPT_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        ValeAlmacenModel IGenericRepository<ValeAlmacenModel>.GetById(int index) {
            return GetList(new List<Conditional>() {
                new Conditional("ALMPT_ID", index.ToString()) }).FirstOrDefault();
        }

        public int Insert(ValeAlmacenModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO ALMPT  ( ALMPT_ID, ALMPT_A, ALMPT_CTALM_ID, ALMPT_TPALM_ID, ALMPT_VER, ALMPT_DOC_ID, ALMPT_STTS_ID, ALMPT_CTREL_ID, ALMPT_PDCLN_ID, ALMPT_CTDP_ID, ALMPT_DRCTR_ID, ALMPT_VNDDR_ID, ALMPT_CVDEV, ALMPT_FOLIO, ALMPT_SERIE, ALMPT_FECEMS, ALMPT_CLV, ALMPT_NOM, ALMPT_CNTCT, ALMPT_RFRNC, ALMPT_SBTTL, ALMPT_DSCT, ALMPT_TRIVA, ALMPT_GTOTAL, ALMPT_FACIVA, ALMPT_OBSRV, ALMPT_VNDDR, ALMPT_FCING, ALMPT_USU_N, ALMPT_FN, ALMPT_CTEFC_ID, ALMPT_UUID) 
                                            VALUES (@ALMPT_ID,@ALMPT_A,@ALMPT_CTALM_ID,@ALMPT_TPALM_ID,@ALMPT_VER,@ALMPT_DOC_ID,@ALMPT_STTS_ID,@ALMPT_CTREL_ID,@ALMPT_PDCLN_ID,@ALMPT_CTDP_ID,@ALMPT_DRCTR_ID,@ALMPT_VNDDR_ID,@ALMPT_CVDEV,@ALMPT_FOLIO,@ALMPT_SERIE,@ALMPT_FECEMS,@ALMPT_CLV,@ALMPT_NOM,@ALMPT_CNTCT,@ALMPT_RFRNC,@ALMPT_SBTTL,@ALMPT_DSCT,@ALMPT_TRIVA,@ALMPT_GTOTAL,@ALMPT_FACIVA,@ALMPT_OBSRV,@ALMPT_VNDDR,@ALMPT_FCING,@ALMPT_USU_N,@ALMPT_FN,@ALMPT_CTEFC_ID,@ALMPT_UUID) RETURNING ALMPT_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@ALMPT_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@ALMPT_A", 1);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@ALMPT_TPALM_ID", item.IdTipoAlmacen);
            sqlCommand.Parameters.AddWithValue("@ALMPT_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@ALMPT_DOC_ID", item.IdTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTEFC_ID", item.IdTipoMovimiento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTREL_ID", item.IdRelacion);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_CTPRD_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@ALMPT_PDCLN_ID", 0);// item.IdPedido);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_NTDSC_ID", item.IdNotaDescuento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@ALMPT_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CVDEV", item.IdCveDevolucion);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@ALMPT_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CLV", "");// item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@ALMPT_NOM", item.Receptor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@ALMPT_RFRNC", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@ALMPT_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@ALMPT_DSCT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_TRIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@ALMPT_GTOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FACIVA", 0);// este valor no existe en el modelo item.TasaIVAPactado);
            sqlCommand.Parameters.AddWithValue("@ALMPT_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@ALMPT_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FCING", item.FechaIngreso);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_USU_C", item.Cancela);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_FCCNCL", item.FechaCancela);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_CVCAN", item.ClaveCancela);
            sqlCommand.Parameters.AddWithValue("@ALMPT_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FN", item.FechaNuevo);

            item.IdComprobante = ExecuteScalar(sqlCommand);
            if (item.IdComprobante > 0)
                return item.IdComprobante;
            return 0;
        }

        public int Update(ValeAlmacenModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE ALMPT SET ALMPT_A = @ALMPT_A, ALMPT_CTALM_ID = @ALMPT_CTALM_ID, ALMPT_TPALM_ID = @ALMPT_TPALM_ID, ALMPT_STTS_ID = @ALMPT_STTS_ID, ALMPT_CTREL_ID = @ALMPT_CTREL_ID, ALMPT_PDCLN_ID=@ALMPT_PDCLN_ID, 
ALMPT_CTDP_ID = @ALMPT_CTDP_ID, ALMPT_DRCTR_ID = @ALMPT_DRCTR_ID, ALMPT_VNDDR_ID = @ALMPT_VNDDR_ID, ALMPT_CVDEV = @ALMPT_CVDEV, ALMPT_CLV = @ALMPT_CLV, ALMPT_NOM = @ALMPT_NOM, ALMPT_CNTCT = @ALMPT_CNTCT, ALMPT_RFRNC = @ALMPT_RFRNC, 
ALMPT_SBTTL = @ALMPT_SBTTL, ALMPT_DSCT = @ALMPT_DSCT, ALMPT_TRIVA = @ALMPT_TRIVA, ALMPT_GTOTAL = @ALMPT_GTOTAL, ALMPT_FACIVA = @ALMPT_FACIVA, ALMPT_OBSRV = @ALMPT_OBSRV, ALMPT_VNDDR = @ALMPT_VNDDR, ALMPT_FCING = @ALMPT_FCING, 
ALMPT_USU_M = @ALMPT_USU_M, ALMPT_FM = @ALMPT_FM, ALMPT_CTEFC_ID = @ALMPT_CTEFC_ID WHERE (ALMPT_ID = @ALMPT_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@ALMPT_A", 1);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@ALMPT_TPALM_ID", item.IdTipoAlmacen);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTEFC_ID", item.IdTipoMovimiento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTREL_ID", item.IdRelacion);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_CTPRD_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@ALMPT_PDCLN_ID", 0);// item.IdPedido);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_NTDSC_ID", item.IdNotaDescuento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CTDP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@ALMPT_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CVDEV", item.IdCveDevolucion);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CLV", "");// item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@ALMPT_NOM", item.Receptor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@ALMPT_RFRNC", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@ALMPT_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@ALMPT_DSCT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@ALMPT_TRIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@ALMPT_GTOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FACIVA", 0);// este valor no existe en el modelo item.TasaIVAPactado);
            sqlCommand.Parameters.AddWithValue("@ALMPT_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@ALMPT_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FCING", item.FechaIngreso);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_USU_C", item.Cancela);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_FCCNCL", item.FechaCancela);
            //sqlCommand.Parameters.AddWithValue("@ALMPT_CVCAN", item.ClaveCancela);

            sqlCommand.Parameters.AddWithValue("@ALMPT_USU_M", item.Creo);
            sqlCommand.Parameters.AddWithValue("@ALMPT_FM", item.FechaNuevo);

            return ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<ValeAlmacenModel> GetList() {
            return GetList(new List<Conditional>());
        }
        #endregion

        public IEnumerable<ValeAlmacenDetailModel> GetList(List<Conditional> condidional) {
            return GetList<ValeAlmacenDetailModel>(condidional);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM ALMPT LEFT JOIN ALMPTS ON ALMPT.ALMPT_ID = ALMPTS.ALMPTS_ALMPT_ID AND ALMPTS.ALMPTS_STTSB_ID = 0 @wcondiciones"
                };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = GetMapper<T1>(sqlCommand).ToList();
                return result;
            } else if (typeof(T1) == typeof(ValeAlmacenProductoView)) {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM MVAPT LEFT JOIN ALMPT ON MVAPT.MVAPT_ALMPT_ID = ALMPT.ALMPT_ID @wcondiciones"
                };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = GetMapper<T1>(sqlCommand).ToList();
                return result;
            } else {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM MVAPT LEFT JOIN ALMPT ON MVAPT.MVAPT_ALMPT_ID = ALMPT.ALMPT_ID @wcondiciones"
                };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = GetMapper<T1>(sqlCommand).ToList();
                return result;
            }
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM ALMPT LEFT JOIN ALMPTS ON ALMPT.ALMPT_ID = ALMPTS.ALMPTS_ALMPT_ID AND ALMPTS.ALMPTS_STTSB_ID = 0 @wcondiciones"
                };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = GetMapper<T1>(sqlCommand).ToList();
                return result;
            } else if (typeof(T1) == typeof(ValeAlmacenProductoView)) {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM MVAPT LEFT JOIN ALMPT ON MVAPT.MVAPT_ALMPT_ID = ALMPT.ALMPT_ID @wcondiciones"
                };
                sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
                var result = GetMapper<T1>(sqlCommand).ToList();
                return result;
            }
            return null;
        }

        public ValeAlmacenDetailModel GetById(int index) {
            var response = this.GetList<ValeAlmacenDetailModel>(new List<IConditional>() {
                new Conditional("ALMPT_ID", index.ToString()) }).FirstOrDefault();

            if (response != null) {
                var conceptos = this.movimientoAlmacenPT.GetList<ValeAlmacenConceptoModel>(new List<IConditional> { new Conditional("MVAPT_ALMPT_ID", index.ToString()) }).ToList();
                if (conceptos != null) {
                    response.Conceptos = new System.ComponentModel.BindingList<ValeAlmacenConceptoModel>(conceptos);
                }

                var relaciones = this.sqlValeRelacionRepository.GetList<ValeAlmacenRelacionModel>(new List<IConditional> { new Conditional("ALMPTR_ALMPT_ID", index.ToString()) }).ToList();
                if (relaciones != null) {
                    response.Relaciones = new System.ComponentModel.BindingList<ValeAlmacenRelacionModel>(relaciones);
                }
            }
            return response;
        }

        public IValeAlmacenDetailModel Save(IValeAlmacenDetailModel item) {
            bool isInsert = false;
            if (item.IdComprobante == 0) {
                isInsert = true;
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.Folio = this.Folio(item);
                item.IdDocumento = this.CreateGuid(new string[] { item.GetOriginalString });
                item.IdComprobante = this.Insert((ValeAlmacenModel)item);
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                this.Update((ValeAlmacenModel)item);
            }

            if (item.IdComprobante > 0) {
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    if (item.Conceptos[i].IdMovimiento == 0) {
                        item.Conceptos[i].IdComprobante = item.IdComprobante;
                        item.Conceptos[i].IdTipoComprobante = item.IdTipoComprobante;
                        item.Conceptos[i].IdTipoMovimiento = item.IdTipoMovimiento;
                        item.Conceptos[i].FechaNuevo = DateTime.Now;
                        item.Conceptos[i].Creo = this.User;
                        item.Conceptos[i].IdMovimiento = this.movimientoAlmacenPT.Insert(item.Conceptos[i]);
                    } else {
                        item.Conceptos[i].Modifica = this.User;
                        item.Conceptos[i].FechaModifica = DateTime.Now;
                        this.movimientoAlmacenPT.Update(item.Conceptos[i]);
                    }
                }
            }
            // si es insert entonces actualizamos las existencias
            if (isInsert) {
                if (!this.movimientoAlmacenPT.Existencia(new Domain.Almacen.PT.Builder.ValeAlmacenQueryBuilder().SetStock().Documento(item.IdComprobante).IsCancel(false).Build())) {
                    Console.WriteLine("No se afectaron las existencias");
                }
            } else if (item.IdStatus == 0) {
                if (!this.movimientoAlmacenPT.Existencia(new Domain.Almacen.PT.Builder.ValeAlmacenQueryBuilder().SetStock().Documento(item.IdComprobante).IsCancel(true).Build())) {
                    Console.WriteLine("No se afectaron las existencias");
                }
            }
            return item;
        }

        public bool CrearTablas() {
            throw new NotImplementedException();
        }

        public int Folio(IValeAlmacenModel item) {
            try {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT MAX(ALMPT_FOLIO) + 1 FROM ALMPT WHERE ALMPT_DOC_ID = @ALMPT_DOC_ID AND ALMPT_CTALM_ID = @ALMPT_CTALM_ID AND ALMPT_TPALM_ID = @ALMPT_TPALM_ID"
                };
                sqlCommand.Parameters.AddWithValue("@ALMPT_DOC_ID", item.IdTipoComprobante);
                sqlCommand.Parameters.AddWithValue("@ALMPT_CTALM_ID", item.IdAlmacen);
                sqlCommand.Parameters.AddWithValue("@ALMPT_TPALM_ID", item.IdTipoAlmacen);
                return this.ExecuteScalar(sqlCommand);
            } catch (Exception ex) {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }
    }
}
