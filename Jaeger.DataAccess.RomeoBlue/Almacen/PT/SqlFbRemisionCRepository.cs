﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.Almacen.PT.Repositories {
    /// <summary>
    /// comisiones a vendedor
    /// </summary>
    public class SqlFbRemisionCRepository : RepositoryMaster<RemisionComisionModel>, ISqlRemisionCRepository {
        public SqlFbRemisionCRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM RMSNC WHERE ((RMSNC_ID = @RMSNC_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@RMSNC_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public RemisionComisionModel GetById(int index) {
            return GetList<RemisionComisionModel>(new List<IConditional> { new Conditional("RMSNC_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<RemisionComisionModel> GetList() {
            return GetList<RemisionComisionModel>(new List<IConditional>());
        }

        public int Insert(RemisionComisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RMSNC (RMSNC_RMSN_ID, RMSNC_A, RMSNC_CTCMS_ID, RMSNC_DRCTR_ID, RMSNC_VNDR_ID, RMSNC_CTDIS_ID, RMSNC_DESC, RMSNC_FCPAC, RMSNC_FCACT, RMSNC_IMPR, RMSNC_ACUML, RMSNC_VNDR, RMSNC_FN, RMSNC_USR_N) VALUES (@RMSNC_RMSN_ID, @RMSNC_A, @RMSNC_CTCMS_ID, @RMSNC_DRCTR_ID, @RMSNC_VNDR_ID, @RMSNC_CTDIS_ID, @RMSNC_DESC, @RMSNC_FCPAC, @RMSNC_FCACT, @RMSNC_IMPR, @RMSNC_ACUML, @RMSNC_VNDR, @RMSNC_FN, @RMSNC_USR_N) RETURNING RMSNC_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSNC_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTCMS_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTDIS_ID", item.IsDisponible);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DESC", item.Comision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCPAC", item.ComisionFactor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCACT", item.ComisionFactorActualizado);
            sqlCommand.Parameters.AddWithValue("@RMSNC_IMPR", item.ComisionPorPagar);
            sqlCommand.Parameters.AddWithValue("@RMSNC_ACUML", item.ComisionPagada);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FN", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSNC_USR_N", item.Modifica);

            item.IdRemision = ExecuteScalar(sqlCommand);
            if (item.IdRemision > 0)
                return item.IdRemision;
            return 0;
        }

        public int Update(RemisionComisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSNC SET RMSNC_RMSN_ID = @RMSNC_RMSN_ID, RMSNC_A = @RMSNC_A, RMSNC_CTCMS_ID = @RMSNC_CTCMS_ID, RMSNC_DRCTR_ID = @RMSNC_DRCTR_ID, RMSNC_VNDR_ID = @RMSNC_VNDR_ID, RMSNC_CTDIS_ID = @RMSNC_CTDIS_ID, RMSNC_DESC = @RMSNC_DESC, RMSNC_FCPAC = @RMSNC_FCPAC, RMSNC_FCACT = @RMSNC_FCACT, RMSNC_IMPR = @RMSNC_IMPR, RMSNC_ACUML = @RMSNC_ACUML, RMSNC_VNDR = @RMSNC_VNDR, RMSNC_FN = @RMSNC_FN, RMSNC_USR_N = @RMSNC_USR_N WHERE (RMSNC_ID = @RMSNC_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@RMSNC_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTCMS_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTDIS_ID", item.IsDisponible);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DESC", item.Comision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCPAC", item.ComisionFactor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCACT", item.ComisionFactorActualizado);
            sqlCommand.Parameters.AddWithValue("@RMSNC_IMPR", item.ComisionPorPagar);
            sqlCommand.Parameters.AddWithValue("@RMSNC_ACUML", item.ComisionPagada);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FN", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSNC_USR_N", item.Modifica);

            return ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> condidional) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RMSN.*, RMSNC.* FROM RMSN LEFT JOIN RMSNC ON RMSNC.RMSNC_RMSN_ID = RMSN.RMSN_ID @wcondiciones ORDER BY RMSN_FOLIO DESC"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, condidional);
            return GetMapper<T1>(sqlCommand);
        }

        public IEnumerable<T1> GetToList<T1>(List<IConditional> condidional) where T1 : class, new() {
            var concepto = condidional.Where(it => it.FieldName.ToUpper() == "BNCMOV_CNCP_ID").FirstOrDefault();
            var lista = condidional.Where(it => it.FieldName.ToUpper() != "BNCMOV_CNCP_ID").ToList();
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RMSNC.*, RMSN.*
                                FROM RMSNC
                                LEFT JOIN RMSN ON RMSN.RMSN_ID = RMSNC.RMSNC_RMSN_ID
                                LEFT JOIN BNCCMP ON BNCCMP.BNCCMP_IDCOM = RMSNC.RMSNC_RMSN_ID 
                                LEFT JOIN BNCMOV ON BNCMOV.BNCMOV_ID = BNCCMP.BNCCMP_SUBID AND BNCMOV.BNCMOV_CNCP_ID IN (@CONCEPTO)
                                WHERE BNCCMP.BNCCMP_IDCOM IS NULL @condiciones
                                ORDER BY RMSNC.RMSNC_RMSN_ID DESC"
            };
            sqlCommand = ExpressionTool.Where(sqlCommand, lista);
            sqlCommand.Parameters.Add("@CONCEPTO", concepto.FieldValue);
            return GetMapper<T1>(sqlCommand);
        }

        public int Saveable(RemisionComisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO RMSNC ( RMSNC_RMSN_ID, RMSNC_A, RMSNC_CTCMS_ID, RMSNC_DRCTR_ID, RMSNC_VNDR_ID, RMSNC_CTDIS_ID, RMSNC_DESC, RMSNC_FCPAC, RMSNC_FCACT, RMSNC_IMPR, RMSNC_ACUML, RMSNC_VNDR, RMSNC_FN, RMSNC_USR_N)
                                                     VALUES (@RMSNC_RMSN_ID,@RMSNC_A,@RMSNC_CTCMS_ID,@RMSNC_DRCTR_ID,@RMSNC_VNDR_ID,@RMSNC_CTDIS_ID,@RMSNC_DESC,@RMSNC_FCPAC,@RMSNC_FCACT,@RMSNC_IMPR,@RMSNC_ACUML,@RMSNC_VNDR,@RMSNC_FN,@RMSNC_USR_N) MATCHING (RMSNC_RMSN_ID);"
            };
            item.Modifica = User;
            item.FechaModifica = DateTime.Now;
            sqlCommand.Parameters.AddWithValue("@RMSNC_RMSN_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTCMS_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_CTDIS_ID", item.IsDisponible);
            sqlCommand.Parameters.AddWithValue("@RMSNC_DESC", item.Comision);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCPAC", item.ComisionFactor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FCACT", item.ComisionFactorActualizado);
            sqlCommand.Parameters.AddWithValue("@RMSNC_IMPR", item.ComisionPorPagar);
            sqlCommand.Parameters.AddWithValue("@RMSNC_ACUML", item.ComisionPagada);
            sqlCommand.Parameters.AddWithValue("@RMSNC_VNDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSNC_FN", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSNC_USR_N", item.Modifica);

            return ExecuteTransaction(sqlCommand);
        }

        /// <summary>
        /// actualizar bandera de comisiones
        /// </summary>
        /// <param name="idRemision"></param>
        /// <param name="value">0 <-no esta agregada a ningun recibo, 1 <- comsion autorizada y esta agregada a un recibo, 2 <-comision pagada y ya esta autorizada en un recibo</param>
        public bool Update(int[] idRemision, int value) {
            // ctlrms_catcms_id = 0 < --no esta agregada a ningun recibo
            // ctlrms_catcms_id = 1 < --la comsion autorizada y esta agregada a un recibo
            // ctlrms_catcms_id = 2 < --comision pagada y ya esta autorizada en un recibo
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE RMSNC SET RMSNC_CTDIS_ID=@RMSNC_CTDIS_ID WHERE RMSNC_RMSN_ID IN (@INDEXS)"
            };

            if (idRemision.Length > 0) {
                sqlCommand.Parameters.AddWithValue("@RMSNC_CTDIS_ID", value);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@INDEXS", string.Join(",", idRemision));

                return this.ExecuteTransaction(sqlCommand) > 0;
            }
            return false;
        }

        public RemisionComisionModel Save(RemisionComisionModel item) {
            item.FechaModifica = DateTime.Now;
            item.Modifica = User;
            if (item.IdRemision == 0) {
                item.IdRemision = Insert(item);
            } else {
                Update(item);
            }
            return item;
        }
    }
}
