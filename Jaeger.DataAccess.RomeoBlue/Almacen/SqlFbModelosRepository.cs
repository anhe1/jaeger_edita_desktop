﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// catalogo de modelos, utilizado para productos del almacen 
    /// </summary>
    public class SqlFbModelosRepository : RepositoryMaster<ModeloModel>, ISqlModelosRepository {
        protected ISqlMediosRepository mediosRepository;
        protected ISqlModeloExistenciaRepository modeloExistenciaRepository;

        public SqlFbModelosRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.mediosRepository = new SqlFbMediosRepository(configuracion, user);
            this.modeloExistenciaRepository = new SqlFbModeloExistenciaRepository(configuracion);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDL SET CTMDL_A = 0, CTMDL_USR_M = @CTMDL_USR_M, CTMDL_FM = @CTMDL_FM WHERE (CTMDL_ID = @CTMDL_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@CTMDL_USR_M", User);
            sqlCommand.Parameters.AddWithValue("@CTMDL_FM", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ModeloModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTMDL ( CTMDL_ID, CTMDL_CTCLS_ID, CTMDL_CTPRD_ID, CTMDL_CTPRC_ID, CTMDL_ATRZD, CTMDL_ALM_ID, CTMDL_A, CTMDL_TIPO, CTMDL_VIS, CTMDL_UNDDXY, CTMDL_UNDDZ, CTMDL_UNDDA, CTMDL_RETISR, CTMDL_RETISRF, CTMDL_RETIVA, CTMDL_RETIVAF, CTMDL_TRSIVA, CTMDL_TRSIVAF, CTMDL_RETIEPS, CTMDL_RETIEPSF, CTMDL_TRSIEPS, CTMDL_TRSIEPSF, CTMDL_UNTR, CTMDL_LARGO, CTMDL_ANCHO, CTMDL_ALTO, CTMDL_PESO, CTMDL_CLVUND, CTMDL_CLVPRDS, CTMDL_CLVOBJ, CTMDL_CDG, CTMDL_UNDD, CTMDL_CTAPRE, CTMDL_SKU, CTMDL_NUMREQ, CTMDL_CLV, CTMDL_MRC, CTMDL_ESPC, CTMDL_DSCRC, CTMDL_USR_N, CTMDL_FN, CTMDL_SEC, CTMDL_DIS)
                                           VALUES (@CTMDL_ID,@CTMDL_CTCLS_ID,@CTMDL_CTPRD_ID,@CTMDL_CTPRC_ID,@CTMDL_ATRZD,@CTMDL_ALM_ID,@CTMDL_A,@CTMDL_TIPO,@CTMDL_VIS,@CTMDL_UNDDXY,@CTMDL_UNDDZ,@CTMDL_UNDDA,@CTMDL_RETISR,@CTMDL_RETISRF,@CTMDL_RETIVA,@CTMDL_RETIVAF,@CTMDL_TRSIVA,@CTMDL_TRSIVAF,@CTMDL_RETIEPS,@CTMDL_RETIEPSF,@CTMDL_TRSIEPS,@CTMDL_TRSIEPSF,@CTMDL_UNTR,@CTMDL_LARGO,@CTMDL_ANCHO,@CTMDL_ALTO,@CTMDL_PESO,@CTMDL_CLVUND,@CTMDL_CLVPRDS,@CTMDL_CLVOBJ,@CTMDL_CDG,@CTMDL_UNDD,@CTMDL_CTAPRE,@CTMDL_SKU,@CTMDL_NUMREQ,@CTMDL_CLV,@CTMDL_MRC,@CTMDL_ESPC,@CTMDL_DSCRC,@CTMDL_USR_N,@CTMDL_FN,@CTMDL_SEC,@CTMDL_DIS) RETURNING CTMDL_ID"
            };

            sqlCommand.Parameters.AddWithValue("@CTMDL_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTCLS_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ATRZD", item.Autorizado);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTMDL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_SEC", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TIPO", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_VIS", item.IdVisible);
            sqlCommand.Parameters.AddWithValue("@CTMDL_DIS", item.IsAvailable);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDXY", item.UnidadXY);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDZ", item.UnidadZ);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDA", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETISR", item.ValorRetecionISR);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETISRF", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIVA", item.ValorRetencionIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIVAF", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIVA", item.ValorTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIVAF", item.FactorTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIEPS", item.ValorRetencionIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIEPSF", item.FactorRetencionIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIEPS", item.ValorTrasladoIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIEPSF", item.FactorTrasladoIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNTR", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@CTMDL_LARGO", item.Largo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ANCHO", item.Ancho);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ALTO", item.Alto);
            sqlCommand.Parameters.AddWithValue("@CTMDL_PESO", item.Peso);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVUND", item.ClaveUnidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVPRDS", item.ClaveProdServ);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVOBJ", item.ObjetoImp);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CDG", item.Codigo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDD", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTAPRE", item.CtaPredial);
            sqlCommand.Parameters.AddWithValue("@CTMDL_SKU", item.NoIdentificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_NUMREQ", item.NumRequerimiento);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTMDL_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_DSCRC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_FN", item.FechaNuevo);

            item.IdModelo = ExecuteScalar(sqlCommand);
            if (item.IdModelo > 0)
                return item.IdModelo;
            return 0;
        }

        public int Update(ModeloModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDL SET CTMDL_CTCLS_ID = @CTMDL_CTCLS_ID, CTMDL_CTPRD_ID = @CTMDL_CTPRD_ID, CTMDL_CTPRC_ID = @CTMDL_CTPRC_ID, CTMDL_ATRZD = @CTMDL_ATRZD, CTMDL_ALM_ID = @CTMDL_ALM_ID, CTMDL_A = @CTMDL_A, 
                CTMDL_TIPO = @CTMDL_TIPO, CTMDL_VIS = @CTMDL_VIS, CTMDL_UNDDXY = @CTMDL_UNDDXY, CTMDL_UNDDZ = @CTMDL_UNDDZ, CTMDL_UNDDA = @CTMDL_UNDDA, CTMDL_RETISR = @CTMDL_RETISR, CTMDL_RETISRF = @CTMDL_RETISRF, 
                CTMDL_RETIVA = @CTMDL_RETIVA, CTMDL_RETIVAF = @CTMDL_RETIVAF, CTMDL_TRSIVA = @CTMDL_TRSIVA, CTMDL_TRSIVAF = @CTMDL_TRSIVAF, CTMDL_RETIEPS = @CTMDL_RETIEPS, CTMDL_RETIEPSF = @CTMDL_RETIEPSF, 
                CTMDL_TRSIEPS = @CTMDL_TRSIEPS, CTMDL_TRSIEPSF = @CTMDL_TRSIEPSF, CTMDL_UNTR = @CTMDL_UNTR, CTMDL_LARGO = @CTMDL_LARGO, CTMDL_ANCHO = @CTMDL_ANCHO, CTMDL_ALTO = @CTMDL_ALTO, 
                CTMDL_PESO = @CTMDL_PESO, CTMDL_CLVUND = @CTMDL_CLVUND, CTMDL_CLVPRDS = @CTMDL_CLVPRDS, CTMDL_CLVOBJ = @CTMDL_CLVOBJ, CTMDL_CDG = @CTMDL_CDG, CTMDL_UNDD = @CTMDL_UNDD, CTMDL_CTAPRE = @CTMDL_CTAPRE, 
                CTMDL_SKU = @CTMDL_SKU, CTMDL_NUMREQ = @CTMDL_NUMREQ, CTMDL_CLV = @CTMDL_CLV, CTMDL_MRC = @CTMDL_MRC, CTMDL_ESPC = @CTMDL_ESPC, CTMDL_DSCRC = @CTMDL_DSCRC, 
                CTMDL_USR_M = @CTMDL_USR_M, CTMDL_FM = @CTMDL_FM, CTMDL_SEC = @CTMDL_SEC, CTMDL_DIS = @CTMDL_DIS WHERE (CTMDL_ID = @CTMDL_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTCLS_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ATRZD", item.Autorizado);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTMDL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_SEC", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TIPO", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_VIS", item.IdVisible);
            sqlCommand.Parameters.AddWithValue("@CTMDL_DIS", item.IsAvailable);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDXY", item.UnidadXY);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDZ", item.UnidadZ);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDDA", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETISR", item.ValorRetecionISR);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETISRF", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIVA", item.ValorRetencionIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIVAF", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIVA", item.ValorTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIVAF", item.FactorTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIEPS", item.ValorRetencionIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_RETIEPSF", item.FactorRetencionIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIEPS", item.ValorTrasladoIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_TRSIEPSF", item.FactorTrasladoIEPS);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNTR", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@CTMDL_LARGO", item.Largo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ANCHO", item.Ancho);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ALTO", item.Alto);
            sqlCommand.Parameters.AddWithValue("@CTMDL_PESO", item.Peso);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVUND", item.ClaveUnidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVPRDS", item.ClaveProdServ);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLVOBJ", item.ObjetoImp);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CDG", item.Codigo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_UNDD", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CTAPRE", item.CtaPredial);
            sqlCommand.Parameters.AddWithValue("@CTMDL_SKU", item.NoIdentificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_NUMREQ", item.NumRequerimiento);
            sqlCommand.Parameters.AddWithValue("@CTMDL_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTMDL_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_DSCRC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTMDL_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@CTMDL_FM", item.FechaModifica);
            return ExecuteTransaction(sqlCommand);
        }

        public ModeloModel GetById(int index) {
            return GetList<ModeloModel>(new List<IConditional>() { new Conditional("CTMDL_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<ModeloModel> GetList() {
            return GetList<ModeloModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand();
            if (typeof(T1) == typeof(ModeloDetailModel)) {
                sqlCommand.CommandText = @"SELECT * FROM CTMDL LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID=CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID=CTMDL.CTMDL_CTPRD_ID @wcondiciones";
                return GetMapper<T1>(sqlCommand, conditionals).ToList();
            } else if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                sqlCommand.CommandText = @"SELECT * FROM CTPRD P LEFT JOIN CTMDL M ON (P.CTPRD_ID = M.CTMDL_CTPRD_ID) LEFT JOIN CTMDLX X ON  (M.CTMDL_ID = X.CTMDLX_CTMDL_ID) @wcondiciones";
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "WHERE (P.CTPRD_NOM LIKE LOWER(CONCAT('%',@search,'%'))) OR LOWER((M.CTMDL_DSCRC LIKE CONCAT('%',@search,'%'))) @condiciones");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@search", "'" + d0.FieldValue + "'");
                    conditionals = conditionals.Where(it => !it.FieldName.ToLower().Contains("@search")).ToList();
                }
                return GetMapper<T1>(sqlCommand, conditionals).ToList();
            }
            throw new NotImplementedException(typeof(T1).FullName);
        }

        public IEnumerable<ModeloDetailModel> GetList(AlmacenEnum tipo, int index, bool onlyActive = true) {
            var sqlCommand = new FbCommand {
                CommandText = @""
            };
            throw new NotImplementedException();
        }

        public bool Autoriza(ModeloModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDL SET CTMDL_ATRZD = @CTMDL_ATRZD, CTMDL_USR_M = @CTMDL_USR_M, CTMDL_FM = @CTMDL_FM WHERE (CTMDL_ID = @CTMDL_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@CTMDL_ID", model.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDL_ATRZD", model.Autorizado);
            sqlCommand.Parameters.AddWithValue("@CTMDL_USR_M", model.Modifica);
            sqlCommand.Parameters.AddWithValue("@CTMDL_FM", model.FechaModifica);

            return ExecuteTransaction(sqlCommand) > 0;
        }

        /// <summary>
        /// almacenar modelo
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ModeloModel Save(ModeloModel item) {
            if (item.IdModelo == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.IdModelo = this.Insert(item);
            } else {
                item.FechaModifica = DateTime.Now;
                item.Modifica = this.User;
                this.Update(item);
            }
            return item;
        }

        public ModeloDetailModel Save(ModeloDetailModel model) {
            if (model.IdModelo == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdModelo = Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                Update(model);
            }
            //if (model.Existencias.IdEspecificacion == -1) {
            //    model.Existencias.IdProducto = model.IdProducto;
            //    model.Existencias.IdModelo = model.IdModelo;
            //    model.Existencias.IdEspecificacion = -1;
            //    model.Existencias.IdTipoAlmacen = model.IdTipo;
            //    model.Existencias.IdAlmacen = model.IdAlmacen;
            //    this.modeloExistenciaRepository.Save(model.Existencias);
            //}
            return model;
        }
    }
}
