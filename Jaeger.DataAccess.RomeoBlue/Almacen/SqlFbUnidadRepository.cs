﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Contracts;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// catalogo de unidades
    /// </summary>
    public class SqlFbUnidadRepository : RepositoryMaster<UnidadModel>, ISqlUnidadRepository {
        public SqlFbUnidadRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTUND WHERE ((CTUND_ID = @CTUND_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTUND_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(UnidadModel item) {
            var sqlCommand = new FbCommand { CommandText = @"INSERT INTO CTUND (CTUND_ID, CTUND_A, CTUND_ALM_ID, CTUND_NOM, CTUND_FAC, CTUND_CLVUND, CTUND_FN, CTUND_USR_N) VALUES (@CTUND_ID, @CTUND_A, @CTUND_ALM_ID, @CTUND_NOM, @CTUND_FAC, @CTUND_CLVUND, @CTUND_FN, @CTUND_USR_N) RETURNING CTUND_ID" };
            sqlCommand.Parameters.AddWithValue("@CTUND_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTUND_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTUND_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTUND_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTUND_FAC", item.Factor);
            sqlCommand.Parameters.AddWithValue("@CTUND_CLVUND", item.ClaveUnidad);
            sqlCommand.Parameters.AddWithValue("@CTUND_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTUND_USR_N", item.Creo);
            item.IdUnidad = ExecuteScalar(sqlCommand);
            if (item.IdUnidad > 0)
                return item.IdUnidad;
            return 0;
        }

        public int Update(UnidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTUND SET CTUND_A = @CTUND_A, CTUND_ALM_ID = @CTUND_ALM_ID, CTUND_NOM = @CTUND_NOM, CTUND_FAC = @CTUND_FAC, CTUND_CLVUND = @CTUND_CLVUND, CTUND_FM = @CTUND_FM, CTUND_USR_M = @CTUND_USR_M WHERE ((CTUND_ID = @CTUND_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@CTUND_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTUND_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTUND_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTUND_FAC", item.Factor);
            sqlCommand.Parameters.AddWithValue("@CTUND_CLVUND", item.ClaveUnidad);
            sqlCommand.Parameters.AddWithValue("@CTUND_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTUND_USR_M", item.Modifica);
            return ExecuteTransaction(sqlCommand);
        }

        public UnidadModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 CTUND.* FROM CTUND WHERE CTUND_ID = @CTUND_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTUND_ID", index);
            return GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<UnidadModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT * FROM CTUND" };
            return GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTUND.* FROM CTUND @wcondiciones"
            };
            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public UnidadModel Save(UnidadModel model) {
            if (model.IdUnidad == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = User;
                model.IdUnidad = Insert(model);
                model.SetModified = false;
            } else {
                model.Modifica = User;
                model.FechaModifica = DateTime.Now;
                if (Update(model) > 0)
                    model.SetModified = false;
            }
            return model;
        }
    }
}
