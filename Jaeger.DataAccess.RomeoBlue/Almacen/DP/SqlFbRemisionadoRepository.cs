﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Almacen.DP.Contracts;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.Almacen.DP.Repositories {
    public class SqlFbRemisionadoRepository : RepositoryMaster<RemisionModel>, ISqlRemisionadoRepository {
        public SqlFbRemisionadoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region declaraciones
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM RMSDP WHERE ((RMSDP_ID = @p1))" };
            throw new NotImplementedException();
        }

        public int Insert(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RMSDP (RMSDP_ID, RMSDP_A, RMSDP_CTDEP_ID, RMSDP_CTDOC_ID, RMSDP_DECI, RMSDP_STTS_ID, RMSDP_CTSR_ID, RMSDP_CTCLS_ID, RMSDP_CTENV_ID, RMSDP_DRCTR_ID, RMSDP_DRCCN_ID, RMSDP_VNDDR_ID, RMSDP_GUIA_ID, RMSDP_CTREL_ID, RMSDP_VER, RMSDP_FOLIO, RMSDP_SERIE, RMSDP_RFCE, RMSDP_CLVR, RMSDP_RFCR, RMSDP_NOMR, RMSDP_CNTCT, RMSDP_FECEMS, RMSDP_FECENT, RMSDP_TPCMB, RMSDP_SBTTL, RMSDP_DSCNT, RMSDP_TRSIVA, RMSDP_GTOTAL, RMSDP_MONEDA, RMSDP_DRCCN, RMSDP_NOTA, RMSDP_VNDDR, RMSDP_UUID, RMSDP_FCCNCL, RMSDP_USR_C, RMSDP_URL_PDF, RMSDP_FN, RMSDP_USR_N) 
                                          VALUES (@RMSDP_ID,@RMSDP_A,@RMSDP_CTDEP_ID,@RMSDP_CTDOC_ID,@RMSDP_DECI,@RMSDP_STTS_ID,@RMSDP_CTSR_ID,@RMSDP_CTCLS_ID,@RMSDP_CTENV_ID,@RMSDP_DRCTR_ID,@RMSDP_DRCCN_ID,@RMSDP_VNDDR_ID,@RMSDP_GUIA_ID,@RMSDP_CTREL_ID,@RMSDP_VER,@RMSDP_FOLIO,@RMSDP_SERIE,@RMSDP_RFCE,@RMSDP_CLVR,@RMSDP_RFCR,@RMSDP_NOMR,@RMSDP_CNTCT,@RMSDP_FECEMS,@RMSDP_FECENT,@RMSDP_TPCMB,@RMSDP_SBTTL,@RMSDP_DSCNT,@RMSDP_TRSIVA,@RMSDP_GTOTAL,@RMSDP_MONEDA,@RMSDP_DRCCN,@RMSDP_NOTA,@RMSDP_VNDDR,@RMSDP_UUID,@RMSDP_FCCNCL,@RMSDP_USR_C,@RMSDP_URL_PDF,@RMSDP_FN,@RMSDP_USR_N) RETURNING RMSDP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSDP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RMSDP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTDOC_ID", item.IdTipoDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DECI", item.PrecisionDecimal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTSR_ID", item.IdSerie);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTCLS_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTENV_ID", item.IdMetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCCN_ID", item.IdDomicilio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSDP_GUIA_ID", item.NoGuia);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTREL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FOLIO", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RMSDP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSDP_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CLVR", item.ReceptorClave);
            sqlCommand.Parameters.AddWithValue("@RMSDP_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSDP_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FECENT", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSDP_TPCMB", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DSCNT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@RMSDP_GTOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCCN", item.DomicilioEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSDP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSDP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_URL_PDF", item.UrlFilePDF);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_N", item.Creo);

            item.IdRemision = ExecuteScalar(sqlCommand);
            if (item.IdRemision > 0)
                return item.IdRemision;
            return 0;
        }

        public int Update(RemisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSDP SET RMSDP_A = @RMSDP_A, RMSDP_CTDEP_ID = @RMSDP_CTDEP_ID, RMSDP_CTDOC_ID = @RMSDP_CTDOC_ID, RMSDP_DECI = @RMSDP_DECI, RMSDP_STTS_ID = @RMSDP_STTS_ID, RMSDP_CTSR_ID = @RMSDP_CTSR_ID, RMSDP_CTCLS_ID = @RMSDP_CTCLS_ID, RMSDP_CTENV_ID = @RMSDP_CTENV_ID, RMSDP_DRCTR_ID = @RMSDP_DRCTR_ID, RMSDP_DRCCN_ID = @RMSDP_DRCCN_ID, RMSDP_VNDDR_ID = @RMSDP_VNDDR_ID, RMSDP_GUIA_ID = @RMSDP_GUIA_ID, RMSDP_CTREL_ID = @RMSDP_CTREL_ID, RMSDP_VER = @RMSDP_VER, RMSDP_FOLIO = @RMSDP_FOLIO, RMSDP_SERIE = @RMSDP_SERIE, RMSDP_RFCE = @RMSDP_RFCE, RMSDP_CLVR = @RMSDP_CLVR, RMSDP_RFCR = @RMSDP_RFCR, RMSDP_NOMR = @RMSDP_NOMR, RMSDP_CNTCT = @RMSDP_CNTCT, RMSDP_FECEMS = @RMSDP_FECEMS, RMSDP_FECENT = @RMSDP_FECENT, RMSDP_TPCMB = @RMSDP_TPCMB, RMSDP_SBTTL = @RMSDP_SBTTL, RMSDP_DSCNT = @RMSDP_DSCNT, RMSDP_TRSIVA = @RMSDP_TRSIVA, RMSDP_GTOTAL = @RMSDP_GTOTAL, RMSDP_MONEDA = @RMSDP_MONEDA, RMSDP_DRCCN = @RMSDP_DRCCN, RMSDP_NOTA = @RMSDP_NOTA, RMSDP_VNDDR = @RMSDP_VNDDR, RMSDP_UUID = @RMSDP_UUID, RMSDP_FCCNCL = @RMSDP_FCCNCL, RMSDP_USR_C = @RMSDP_USR_C, RMSDP_URL_PDF = @RMSDP_URL_PDF, RMSDP_FM = @RMSDP_FM, RMSDP_USR_M = @RMSDP_USR_M WHERE (RMSDP_ID = @RMSDP_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@RMSDP_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSDP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTDOC_ID", item.IdTipoDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DECI", item.PrecisionDecimal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTSR_ID", item.IdSerie);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTCLS_ID", item.IdCatalogo);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTENV_ID", item.IdMetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCCN_ID", item.IdDomicilio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VNDDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@RMSDP_GUIA_ID", item.NoGuia);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTREL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@RMSDP_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CLVR", item.ReceptorClave);
            sqlCommand.Parameters.AddWithValue("@RMSDP_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@RMSDP_NOMR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FECENT", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSDP_TPCMB", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@RMSDP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DSCNT", item.TotalDescuento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@RMSDP_GTOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@RMSDP_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@RMSDP_DRCCN", item.DomicilioEntrega);
            sqlCommand.Parameters.AddWithValue("@RMSDP_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@RMSDP_VNDDR", item.Vendedor);
            sqlCommand.Parameters.AddWithValue("@RMSDP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_URL_PDF", item.UrlFilePDF);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_M", item.Modifica);

            return ExecuteTransaction(sqlCommand);
        }

        public RemisionModel GetById(int index) {
            throw new NotImplementedException();
        }
        #endregion

        public T1 GetById<T1>(int index) where T1 : class, new() {
            return GetList<T1>(new List<Conditional> {
                new Conditional("RMSDP_ID", index.ToString())
            }).FirstOrDefault();
        }

        public IEnumerable<RemisionModel> GetList() {
            return GetList<RemisionModel>(new List<Conditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT * FROM RMSDP @wcondiciones ORDER BY RMSDP_FOLIO DESC" };
            if (typeof(T1) == typeof(RemisionPartidaModel)) {
                sqlCommand = new FbCommand { CommandText = @"SELECT * FROM RMSDP LEFT JOIN MVADP ON RMSDP.RMSDP_ID = MVADP.MVADP_RMSN_ID @wcondiciones ORDER BY RMSDP_FOLIO DESC" };
            }
            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT * FROM RMSDP @wcondiciones ORDER BY RMSDP_FOLIO DESC" };
            if (typeof(T1) == typeof(RemisionPartidaModel)) {
                sqlCommand = new FbCommand { CommandText = @"SELECT * FROM RMSDP LEFT JOIN MVADP ON RMSDP.RMSDP_ID = MVADP.MVADP_RMSN_ID @wcondiciones ORDER BY RMSDP_FOLIO DESC" };
            } 

            return GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public RemisionDetailModel Save(RemisionDetailModel model) {
            if (model.IdRemision == 0) {
                model.Creo = User;
                model.FechaNuevo = DateTime.Now;
                model.IdDocumento = CreateGuid(new string[] { model.Version, model.IdSerie.ToString(), model.IdDepartamento.ToString(), model.IdCliente.ToString(), model.FechaEmision.ToString() });
                model.IdRemision = Insert(model);
            } else {
                model.Modifica = User;
                model.FechaModifica = DateTime.Now;
                Update(model);
            }
            return model;
        }

        public bool Cancelar(RemisionCancelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSDP SET RMSDP_STTS_ID = @RMSDP_STTS_ID, RMSDP_CTREL_ID = @RMSDP_CTREL_ID, RMSDP_USR_C = @RMSDP_USR_C, RMSDP_FCCNCL = @RMSDP_FCCNCL, RMSDP_CVCAN = @RMSDP_CVCAN,
RMSDP_CLMTV=@RMSDP_CLMTV, RMSDP_CLNTA = @RMSDP_CLNTA
WHERE RMSDP_ID = @RMSDP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSDP_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CTREL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CVCAN", item.ClaveCancela);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CLMTV", item.ClaveCancelacion);
            sqlCommand.Parameters.AddWithValue("@RMSDP_CLNTA", item.NotaCancelacion);

            return ExecuteTransaction(sqlCommand) > 0;
        }

        public bool SetStatus(RemisionStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RMSDP SET RMSDP_STTS_ID = @RMSDP_STTS_ID, RMSDP_FM = @RMSDP_FM, RMSDP_USR_M = @RMSDP_USR_M WHERE RMSDP_ID = @RMSDP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@RMSDP_ID", item.IdRemision);
            sqlCommand.Parameters.AddWithValue("@RMSDP_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@RMSDP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RMSDP_USR_M", item.Modifica);
            return ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
