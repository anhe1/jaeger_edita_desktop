﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.FB.Almacen.Repositories {
    /// <summary>
    /// clase para control de proveedores de productos o servicios
    /// </summary>
    public class SqlFbProdServProveedorRepository : RepositoryMaster<ModeloProveedorModel>, ISqlProdServProveedorRepository {
        public SqlFbProdServProveedorRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public int Insert(ModeloProveedorModel item) {
            var sqlCommand = new FbCommand();
            throw new NotImplementedException();
        }
        public int Update(ModeloProveedorModel item) {
            throw new NotImplementedException();
        }

        public ModeloProveedorModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<ModeloProveedorModel> GetList() {
            throw new NotImplementedException();
        }
        #endregion

        public ModeloProveedorModel Save(ModeloProveedorModel proveedor) {
            throw new NotImplementedException();
        }
    }
}
