﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbNoConformidadStatusRepository : RepositoryMaster<NoConformidadStatusModel>, ISqlNoConformidadStatusRepository {
        public SqlFbNoConformidadStatusRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public int Update(NoConformidadStatusModel item) {
            throw new NotImplementedException();
        }

        public int Insert(NoConformidadStatusModel item) {
            throw new NotImplementedException();
        }

        public NoConformidadStatusModel GetById(int index) {
            return this.GetList<NoConformidadStatusModel>(new List<IConditional>() { new Conditional("NOCFS_NOCF_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<NoConformidadStatusModel> GetList() {
            return this.GetList<NoConformidadStatusModel>(new List<IConditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFS @wcondiciones "
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public NoConformidadStatusModel Save(NoConformidadStatusModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO NOCFS (NOCFS_NOCF_ID, NOCFS_CTSTA_ID, NOCFS_CTSTB_ID, NOCFS_NOCFR_ID, NOCFS_CVMTV, NOCFS_NOTA, NOCFS_USU_N, NOCFS_FN)
                                                    VALUES (@NOCFS_NOCF_ID,@NOCFS_CTSTA_ID,@NOCFS_CTSTB_ID,@NOCFS_NOCFR_ID,@NOCFS_CVMTV,@NOCFS_NOTA,@NOCFS_USU_N,@NOCFS_FN)
                                MATCHING (NOCFS_NOCF_ID, NOCFS_CTSTA_ID, NOCFS_CTSTB_ID)
                                RETURNIG NOCFS_NOCF_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFS_NOCF_ID", model.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFS_CTSTA_ID", model.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@NOCFS_CTSTB_ID", model.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@NOCFS_NOCFR_ID", model.IdNoConformidadR);
            sqlCommand.Parameters.AddWithValue("@NOCFS_CVMTV", model.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@NOCFS_NOTA", model.Nota);
            sqlCommand.Parameters.AddWithValue("@NOCFS_USU_N", model.Usuario);
            sqlCommand.Parameters.AddWithValue("@NOCFS_FN", model.FechaNuevo);
            var d0 = ExecuteTransaction(sqlCommand) > 0;
            return model;
        }
    }
}
