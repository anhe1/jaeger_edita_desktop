﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbMediaRepository : RepositoryMaster<MediaModel>, ISqlMediaRepository {
        public SqlFbMediaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) { this.User = user; }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFM WHERE NOCFM_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MediaModel GetById(int index) {
            return this.GetList<MediaModel>(new List<IConditional>() { new Conditional("NOCFM_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<MediaModel> GetList() {
            return this.GetList<MediaModel>(new List<IConditional>());
        }

        public int Insert(MediaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFM (NOCFM_ID, NOCFM_A, NOCFM_NOCF_ID, NOCFM_URL, NOCFM_DESC, NOCFM_FN) VALUES (@NOCFM_ID,@NOCFM_A,@NOCFM_NOCF_ID,@NOCFM_URL,@NOCFM_DESC,@NOCFM_FN) RETURNING NOCFM_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFM_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFM_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFM_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@NOCFM_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFM_FN", item.FechaNuevo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(MediaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFM SET NOCFM_A = @NOCFM_A, NOCFM_NOCF_ID = @NOCFM_NOCF_ID, NOCFM_URL = @NOCFM_URL, NOCFM_DESC = @NOCFM_DESC, NOCFM_FN = @NOCFM_FN WHERE NOCFM_ID = @NOCFM_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFM_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFM_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFM_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@NOCFM_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFM_FN", item.FechaNuevo);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFM @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public MediaModel Save(MediaModel model) {
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
