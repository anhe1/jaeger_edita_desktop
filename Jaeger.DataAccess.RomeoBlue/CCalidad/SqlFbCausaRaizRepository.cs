﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbCausaRaizRepository : RepositoryMaster<CausaRaizModel>, ISqlCausaRaizRepository {
        public SqlFbCausaRaizRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFCR WHERE NOCFCR_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CausaRaizModel GetById(int index) {
            return this.GetList<CausaRaizModel>(new List<IConditional>() { new Conditional("NOCFCR_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<CausaRaizModel> GetList() {
            return this.GetList<CausaRaizModel>(new List<IConditional>());
        }

        public int Insert(CausaRaizModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFCR (NOCFCR_ID, NOCFCR_NOCF_ID, NOCFCR_A, NOCFCR_CLS5M_ID, NOCFCR_CTEFE_ID, NOCFCR_CTDEP_ID, NOCFCR_DESC, NOCFCR_CTUSR_ID, NOCFCR_RESP, NOCFCR_RESP_C, NOCFCR_FN, NOCFCR_USR_N) 
                                           VALUES (@NOCFCR_ID,@NOCFCR_NOCF_ID,@NOCFCR_A,@NOCFCR_CLS5M_ID,@NOCFCR_CTEFE_ID,@NOCFCR_CTDEP_ID,@NOCFCR_DESC,@NOCFCR_CTUSR_ID,@NOCFCR_RESP,@NOCFCR_RESP_C,@NOCFCR_FN,@NOCFCR_USR_N) RETURNING NOCFCR_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFCR_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CLS5M_ID", item.IdClasificacion5);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTEFE_ID", item.IdEfecto);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTUSR_ID", item.IdResponsable);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_RESP_C", item.IsResponsable);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(CausaRaizModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFCR SET NOCFCR_NOCF_ID = @NOCFCR_NOCF_ID, NOCFCR_A = @NOCFCR_A, NOCFCR_CLS5M_ID = @NOCFCR_CLS5M_ID, NOCFCR_CTEFE_ID = @NOCFCR_CTEFE_ID, NOCFCR_CTDEP_ID = @NOCFCR_CTDEP_ID, NOCFCR_DESC = @NOCFCR_DESC, NOCFCR_CTUSR_ID = @NOCFCR_CTUSR_ID, NOCFCR_RESP = @NOCFCR_RESP, NOCFCR_RESP_C = @NOCFCR_RESP_C, NOCFCR_FN = @NOCFCR_FN, NOCFCR_USR_N = @NOCFCR_USR_N WHERE NOCFCR_ID = @NOCFCR_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFCR_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CLS5M_ID", item.IdClasificacion5);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTEFE_ID", item.IdEfecto);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_CTUSR_ID", item.IdResponsable);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NOCFCR_RESP_C", item.IsResponsable);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFCR @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public CausaRaizDetailModel Save(CausaRaizDetailModel model) {
            model.FechaNuevo = DateTime.Now;
            model.Creo = this.User;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                model.FechaNuevo = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
