﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbAccionPreventivaRepository : RepositoryMaster<AccionPreventivaModel>, ISqlAccionPreventivaRepository {
        public SqlFbAccionPreventivaRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFAP WHERE NOCFAP_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public AccionPreventivaModel GetById(int index) {
            return this.GetList<AccionPreventivaModel>(new List<IConditional>() { new Conditional("NOCFAP_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<AccionPreventivaModel> GetList() {
            return this.GetList<AccionPreventivaModel>(new List<IConditional>());
        }

        public int Insert(AccionPreventivaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFAP (NOCFAP_ID, NOCFAP_A, NOCFAP_NOCF_ID, NOCFAP_FEC, NOCFAP_RESP, NOCFAP_DESC) VALUES (@NOCFAP_ID,@NOCFAP_A,@NOCFAP_NOCF_ID,@NOCFAP_FEC,@NOCFAP_RESP,@NOCFAP_DESC) RETURNING NOCFAP_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@NOCFAP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_DESC", item.Descripcion);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(AccionPreventivaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFAP SET NOCFAP_A = @NOCFAP_A, NOCFAP_NOCF_ID = @NOCFAP_NOCF_ID, NOCFAP_FEC = @NOCFAP_FEC, NOCFAP_RESP = @NOCFAP_RESP, NOCFAP_DESC = @NOCFAP_DESC WHERE NOCFAP_ID = @NOCFAP_ID"
            };
            sqlCommand.Parameters.AddWithValue("@NOCFAP_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFAP_DESC", item.Descripcion);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFAP @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public AccionPreventivaDetailModel Save(AccionPreventivaDetailModel model) {
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
