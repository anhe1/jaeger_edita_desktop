﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbCostoNoCalidadRepository : RepositoryMaster<CostoNoCalidadModel>, ISqlCostoNoCalidadRepository {
        public SqlFbCostoNoCalidadRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) { this.User = user; }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFC WHERE NOCFC_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CostoNoCalidadModel GetById(int index) {
            return this.GetList<CostoNoCalidadModel>(new List<IConditional>() { new Conditional("NOCFC_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<CostoNoCalidadModel> GetList() {
            return this.GetList<CostoNoCalidadModel>(new List<IConditional>());
        }

        public int Insert(CostoNoCalidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFC (NOCFC_ID, NOCFC_A, NOCFC_NOCF_ID, NOCFC_PEDPRD_ID, NOCFC_NOM, NOCFC_DESC, NOCFC_MAQ, NOCFC_MO, NOCFC_MA, NOCFC_MP, NOCFC_CONS, NOCFC_FN) VALUES (@NOCFC_ID,@NOCFC_A,@NOCFC_NOCF_ID,@NOCFC_PEDPRD_ID,@NOCFC_NOM,@NOCFC_DESC,@NOCFC_MAQ,@NOCFC_MO,@NOCFC_MA,@NOCFC_MP,@NOCFC_CONS,@NOCFC_FN)"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFC_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFC_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@NOCFC_NOM", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@NOCFC_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MAQ", item.Maquinaria);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MO", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MA", item.MedioAmbiente);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MP", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@NOCFC_CONS", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@NOCFC_FN", item.FechaNuevo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(CostoNoCalidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFC SET NOCFC_A = @NOCFC_A, NOCFC_NOCF_ID = @NOCFC_NOCF_ID, NOCFC_PEDPRD_ID = @NOCFC_PEDPRD_ID, NOCFC_NOM = @NOCFC_NOM, NOCFC_DESC = @NOCFC_DESC, NOCFC_MAQ = @NOCFC_MAQ, NOCFC_MO = @NOCFC_MO, NOCFC_MA = @NOCFC_MA, NOCFC_MP = @NOCFC_MP, NOCFC_CONS = @NOCFC_CONS, NOCFC_FN = @NOCFC_FN WHERE NOCFC_ID = @NOCFC_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFC_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NOCFC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFC_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFC_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@NOCFC_NOM", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@NOCFC_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MAQ", item.Maquinaria);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MO", item.ManoObra);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MA", item.MedioAmbiente);
            sqlCommand.Parameters.AddWithValue("@NOCFC_MP", item.MateriaPrima);
            sqlCommand.Parameters.AddWithValue("@NOCFC_CONS", item.Consumibles);
            sqlCommand.Parameters.AddWithValue("@NOCFC_FN", item.FechaNuevo);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFC @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public CostoNoCalidadDetailModel Save(CostoNoCalidadDetailModel model) {
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
