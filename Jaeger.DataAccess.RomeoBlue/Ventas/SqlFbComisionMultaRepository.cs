﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.Ventas {
    /// <summary>
    /// Partidas del catalogo de comisiones (CMSNM)
    /// </summary>
    public class SqlFbComisionMultaRepository : RepositoryMaster<ComisionMultaModel>, ISqlComisionMultaRepository {
        public SqlFbComisionMultaRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CMSNM WHERE CMSNM_ID = @CMSNM_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNM_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ComisionMultaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CMSNM ( CMSNM_ID, CMSNM_A, CMSNM_CMSNT_ID, CMSNM_RNG1, CMSNM_RNG2, CMSNM_FACTOR, CMSNM_NOM, CMSNM_FN, CMSNM_USU_N) 
                                       VALUES (@CMSNM_ID,@CMSNM_A,@CMSNM_CMSNT_ID,@CMSNM_RNG1,@CMSNM_RNG2,@CMSNM_FACTOR,@CMSNM_NOM,@CMSNM_FN,@CMSNM_USU_N) RETURNING CMSNM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNM_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CMSNM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CMSNM_CMSNT_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@CMSNM_RNG1", item.Inicio);
            sqlCommand.Parameters.AddWithValue("@CMSNM_RNG2", item.Final);
            sqlCommand.Parameters.AddWithValue("@CMSNM_FACTOR", item.Factor);
            sqlCommand.Parameters.AddWithValue("@CMSNM_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CMSNM_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CMSNM_USU_N", item.Creo);

            item.IdDescuento = this.ExecuteScalar(sqlCommand);
            if (item.IdDescuento > 0)
                return item.IdDescuento;
            return 0;
        }

        public int Update(ComisionMultaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CMSNM SET CMSNM_A = @CMSNM_A, CMSNM_CMSNT_ID = @CMSNM_CMSNT_ID, CMSNM_RNG1 = @CMSNM_RNG1, CMSNM_RNG2 = @CMSNM_RNG2, CMSNM_FACTOR = @CMSNM_FACTOR, CMSNM_NOM = @CMSNM_NOM, CMSNM_FM = @CMSNM_FM, CMSNM_USU_M = @CMSNM_USU_M WHERE CMSNM_ID = @CMSNM_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNM_ID", item.IdDescuento);
            sqlCommand.Parameters.AddWithValue("@CMSNM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CMSNM_CMSNT_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@CMSNM_RNG1", item.Inicio);
            sqlCommand.Parameters.AddWithValue("@CMSNM_RNG2", item.Final);
            sqlCommand.Parameters.AddWithValue("@CMSNM_FACTOR", item.Factor);
            sqlCommand.Parameters.AddWithValue("@CMSNM_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CMSNM_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CMSNM_USU_N", item.Creo);
            return this.ExecuteTransaction(sqlCommand);
        }
        
        public ComisionMultaModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CMSNM WHERE CMSNM_ID = @CMSNM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNM_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<ComisionMultaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CMSNM ORDER BY CMSNM_RNG1 ASC, CMSNM_RNG2 ASC"
            };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CMSNM @condiciones ORDER BY CMSNM_RNG1 ASC, CMSNM_RNG2 ASC"
            };

            if (conditionals == null) {
                conditionals = new List<IConditional>();
            }

            if (conditionals.Count > 0) {
                var d = FireBird.Services.ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "WHERE" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }
            return this.GetMapper<T1>(sqlCommand).ToList();
        }

        public ComisionMultaModel Save(ComisionMultaModel model) {
            if (model.IdDescuento == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdDescuento = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }
    }
}
