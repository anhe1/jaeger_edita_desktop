﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.Ventas {
    /// <summary>
    /// Catalogo de comisiones a Vendedor (CMSNT)
    /// </summary>
    public class SqlFbComisionRepository : RepositoryMaster<ComisionModel>, ISqlComisionRepository {
        protected ISqlComisionMultaRepository comisionMultaRepository;
        protected ISqlComisionDescuentoRepository descuentoRepository;

        /// <summary>
        /// catalogo de comisiones
        /// </summary>
        public SqlFbComisionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.comisionMultaRepository = new SqlFbComisionMultaRepository(configuracion, user);
            this.descuentoRepository = new SqlFbComisionDescuentoRepository(configuracion, user);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM CMSNT WHERE CMSNT_ID = @CMSNT_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNT_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
            throw new NotImplementedException();
        }

        public int Insert(ComisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CMSNT ( CMSNT_ID, CMSNT_A, CMSNT_SEC_ID, CMSNT_VMIN, CMSNT_VMAX, CMSNT_NOM, CMSNT_NOTA, CMSNT_FCHINI, CMSNT_FCHVIG, CMSNT_FN, CMSNT_USU_N) 
                                           VALUES (@CMSNT_ID,@CMSNT_A,@CMSNT_SEC_ID,@CMSNT_VMIN,@CMSNT_VMAX,@CMSNT_NOM,@CMSNT_NOTA,@CMSNT_FCHINI,@CMSNT_FCHVIG,@CMSNT_FN,@CMSNT_USU_N) RETURNING CMSNT_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNT_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@CMSNT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CMSNT_SEC_ID", item.Orden);
            sqlCommand.Parameters.AddWithValue("@CMSNT_VMIN", item.VentaMinima);
            sqlCommand.Parameters.AddWithValue("@CMSNT_VMAX", item.VentaMaxima);
            sqlCommand.Parameters.AddWithValue("@CMSNT_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CMSNT_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FCHINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FCHVIG", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CMSNT_USU_N", item.Creo);
            throw new NotImplementedException();
        }

        public int Update(ComisionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CMSNT SET CMSNT_A = @CMSNT_A, CMSNT_SEC_ID = @CMSNT_SEC_ID, CMSNT_VMIN = @CMSNT_VMIN, CMSNT_VMAX = @CMSNT_VMAX, CMSNT_NOM = @CMSNT_NOM, CMSNT_NOTA = @CMSNT_NOTA, 
CMSNT_FCHINI = @CMSNT_FCHINI, CMSNT_FCHVIG = @CMSNT_FCHVIG, CMSNT_FM = @CMSNT_FM, CMSNT_USU_M = @CMSNT_USU_M WHERE CMSNT_ID = @CMSNT_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@CMSNT_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@CMSNT_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CMSNT_SEC_ID", item.Orden);
            sqlCommand.Parameters.AddWithValue("@CMSNT_VMIN", item.VentaMinima);
            sqlCommand.Parameters.AddWithValue("@CMSNT_VMAX", item.VentaMaxima);
            sqlCommand.Parameters.AddWithValue("@CMSNT_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CMSNT_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FCHINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FCHVIG", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@CMSNT_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CMSNT_USU_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }
        
        public IEnumerable<ComisionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CMSNT"
            };
            return this.GetMapper(sqlCommand);
        }

        public ComisionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CMSNT WHERE CMSNT_ID = @CMSNT_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CMSNT_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }
        #endregion

        public ComisionDetailModel Save(ComisionDetailModel model) {
            if (model.IdComision == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdComision = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CMSNT @condiciones ORDER BY CMSNT_SEC_ID ASC"
            };

            if (conditionals == null) {
                conditionals = new List<IConditional>();
            }

            if (conditionals.Count > 0) {
                var d = FireBird.Services.ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "WHERE" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }

            var result = this.GetMapper<T1>(sqlCommand).ToList();
            var ids = result.Select(it => (it as ComisionDetailModel).IdComision).ToArray();
            var _tablaDescuento = this.descuentoRepository.GetList<ComisionDescuentoModel>(new List<IConditional> { new Conditional("CMSND_CMSNT_ID", string.Join(",", ids), ConditionalTypeEnum.In), new Conditional("CMSND_A", "1") });
            var _tablaMulta = this.comisionMultaRepository.GetList<ComisionMultaModel>(new List<IConditional> { new Conditional("CMSNM_CMSNT_ID", string.Join(",", ids), ConditionalTypeEnum.In), new Conditional("CMSNM_A", "1") });

            for (int i = 0; i < result.Count(); i++) {
                (result[i] as ComisionDetailModel).Descuento = new System.ComponentModel.BindingList<ComisionDescuentoModel>(_tablaDescuento.Where(it => it.IdComision == (result[i] as ComisionDetailModel).IdComision).ToList());
                (result[i] as ComisionDetailModel).Multa = new System.ComponentModel.BindingList<ComisionMultaModel>(_tablaMulta.Where(it => it.IdComision == (result[i] as ComisionDetailModel).IdComision).ToList());
            }
            return result;
        }
    }
}
