﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    public class SqlFbVendedorCarteraRepository : Abstractions.RepositoryMaster<CarteraModel>, ISqlCarteraRepository {
        /// <summary>
        /// Cartera de Clientes y Vendedores
        /// </summary>
        /// <param name="configuracion">Configuracion</param>
        /// <param name="user">usuario</param>
        public SqlFbVendedorCarteraRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(CarteraModel item) {
            return this.Salveable(item);
        }

        public int Update(CarteraModel item) {
            return this.Salveable(item);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand() {
                CommandText = @"DELETE FROM DRCTRC WHERE DRCTRC_DRCTR_ID = @DRCTRC_DRCTR_ID"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRC_DRCTR_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Salveable(ICarteraModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO DRCTRC ( DRCTRC_A, DRCTRC_VNDR_ID, DRCTRC_DRCTR_ID, DRCTRC_NOTA, DRCTRC_USR_M, DRCTRC_FM) 
                                                      VALUES (@DRCTRC_A,@DRCTRC_VNDR_ID,@DRCTRC_DRCTR_ID,@DRCTRC_NOTA,@DRCTRC_USR_M,@DRCTRC_FM) MATCHING (DRCTRC_VNDR_ID, DRCTRC_DRCTR_ID);"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRC_A", model.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRC_VNDR_ID", model.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@DRCTRC_DRCTR_ID", model.IdCliente);
            sqlCommand.Parameters.AddWithValue("@DRCTRC_NOTA", model.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRC_USR_M", model.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTRC_FM", model.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        [Obsolete("Metodo no utilizado")]
        public CarteraModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<CarteraModel> GetList() {
            return this.GetList<CarteraModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetVendedores<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRC.*, DRCTR.* FROM DRCTRC LEFT JOIN DRCTR ON DRCTR.DRCTR_ID = DRCTRC.DRCTRC_VNDR_ID @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public IEnumerable<T1> GetClientes<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRC.*, DRCTR.* FROM DRCTRC LEFT JOIN DRCTR ON DRCTR.DRCTR_ID = DRCTRC.DRCTRC_DRCTR_ID @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRC.*, DRCTR.* FROM DRCTRC LEFT JOIN DRCTR ON DRCTR.DRCTR_ID = DRCTRC.DRCTRC_DRCTR_ID @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
