﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio del directorio de contribuyentes
    /// </summary>
    public class SqlFbContribuyenteRepository : SqlFbContribuyenteCRUD, ISqlContribuyenteRepository {
        protected ISqlDomicilioRepository domicilioRepository;
        protected ISqlCuentaBancariaRepository cuentaBancariaRepository;
        protected ISqlRelacionComercialRepository relacionComercialRepository;
        protected ISqlCarteraRepository carteraRepository;
        protected ISqlContactoRepository contactoRepository;

        public SqlFbContribuyenteRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion, user) {
            this.User = user;
            this.domicilioRepository = new SqlFbDomicilioRepository(configuracion, user);
            this.cuentaBancariaRepository = new SqlFbContribuyenteCuentaBancariaRepository(configuracion, user);
            this.relacionComercialRepository = new SqlFbRelacionComercialRepository(configuracion, user);
            this.carteraRepository = new SqlFbVendedorCarteraRepository(configuracion, user);
            this.contactoRepository = new SqlFbContactoRepository(configuracion, user);
        }

        public bool Update(int index, string mail) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR SET DRCTR_MAIL = @DRCTR_MAIL WHERE (DRCTR_ID = @DRCTR_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", index);
            sqlCommand.Parameters.AddWithValue("@DRCTR_MAIL", mail);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int ReturnId(string rfc) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTR.DRCTR_ID FROM DRCTR WHERE UPPER(DRCTR.DRCTR_RFC) LIKE @clave"
            };
            sqlCommand.Parameters.AddWithValue("@clave", rfc.ToUpper());
            var _indice = this.ExecuteScalar(sqlCommand);
            return _indice;
        }

        public bool ValidaRFC(IValidoRFC valido) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR SET DRCTR_RFCV = @DRCTR_RFCV, DRCTR_DOMFIS = @DRCTR_DOMFIS WHERE (DRCTR_ID = @DRCTR_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", valido.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFCV", valido.IsValidRFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DOMFIS", valido.DomicilioFiscal);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool ValidaRFC(int index, string razonSocial, string nombreComercial, string domicilioFiscal, bool valido) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR SET DRCTR_RFCV = @DRCTR_RFCV, DRCTR_DOMFIS = @DRCTR_DOMFIS WHERE (DRCTR_ID = @DRCTR_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", index);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFCV", valido);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DOMFIS", domicilioFiscal);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        /// <summary>
        /// comprobar la existencia de la clave de usuario
        /// </summary>
        public bool ExistClave(string clave) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTR.DRCTR_ID FROM DRCTR WHERE UPPER(DRCTR.DRCTR_CLV) LIKE @clave"
            };
            sqlCommand.Parameters.AddWithValue("@clave", clave.ToUpper());
            var _indice = this.ExecuteScalar(sqlCommand);
            return _indice > 0;
        }

        public T1 GetById<T1>(int index) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT FIRST 1 DRCTR.* FROM DRCTR WHERE DRCTR_ID = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);

            return this.GetMapper<T1>(sqlCommand).FirstOrDefault();
        }

        public new IContribuyenteDetailModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM drctr WHERE drctr_id = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.GetMapper<ContribuyenteDetailModel>(sqlCommand).FirstOrDefault();
        }

        /// <summary>
        /// obtener un objeto contribuyente por su registro federal de contribuyentes
        /// </summary>
        public IContribuyenteDetailModel GetByRFC(string rfc) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM DRCTR WHERE DRCTR_RFC = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", rfc);
            var result = this.GetMapper<ContribuyenteDetailModel>(sqlCommand).FirstOrDefault();
            return result;
        }

        public IContribuyenteDetailModel Save(IContribuyenteDetailModel response) {
            if (response.IdDirectorio == 0) {
                response.FechaNuevo = DateTime.Now;
                response.Creo = this.User;
                response.IdDirectorio = this.Insert((ContribuyenteDetailModel)response);
            } else {
                response.Modifica = this.User;
                response.FechaModifica = DateTime.Now;
                this.Update((ContribuyenteDetailModel)response);
            }
            // domicilios
            for (int i = 0; i < response.Domicilios.Count; i++) {
                response.Domicilios[i].IdContribuyente = response.IdDirectorio;
                response.Domicilios[i] = this.domicilioRepository.Save(response.Domicilios[i]);
            }
            // cuentas bancarias
            for (int i = 0; i < response.CuentasBancarias.Count; i++) {
                response.CuentasBancarias[i].IdDirectorio = response.IdDirectorio;
                response.CuentasBancarias[i] = this.cuentaBancariaRepository.Save(response.CuentasBancarias[i]);
            }
            // contactos
            for (int i = 0; i < response.Contactos.Count; i++) {
                response.Contactos[i].IdDirectorio = response.IdDirectorio;
                response.Contactos[i] = this.contactoRepository.Save(response.Contactos[i]);
            }

            for (int i = 0; i < response.Vendedores.Count; i++) {
                response.Vendedores[i].IdCliente = response.IdDirectorio;
                this.carteraRepository.Salveable((CarteraModel)response.Vendedores[i]);
            }
            return response;
        }

        /// <summary>
        /// obtener listado de beneficiarios del directorio, generalmente son todos los registros
        /// </summary>
        public IEnumerable<T1> GetBeneficiarios<T1>(List<IConditional> keyValues) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };
            var d1 = keyValues.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (d1 != null) {
                if (d1.FieldValue != string.Empty) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "WHERE LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DRCTR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DRCTR_CLV) || ','|| LOWER(DRCTR_RFC) LIKE @SEARCH @condiciones");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@SEARCH", "'" + d1.FieldValue.ToLower() + "'");
                }
                keyValues.Remove(d1);
            }
            return this.GetMapper<T1>(sqlCommand, keyValues).ToList();
        }

        /// <summary>
        /// Listado de objetos ContribuyenteDomicilioSingleModel, ContribuyenteDetailModel y ContribuyenteModel
        /// </summary>
        /// <typeparam name="T1">dependiendo de cada objeto se agrega la relacion</typeparam>
        /// <param name="conditionals">sin se incluye @SEARCH se utiliza como busqueda</param>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand();
            if (typeof(T1) == typeof(ContribuyenteDomicilioSingleModel)) {
                sqlCommand.CommandText = @"SELECT DRCTR.*, DRCCN.* FROM DRCTR LEFT JOIN DRCCN ON DRCCN.DRCCN_ID = (SELECT FIRST 1 DRCCN.DRCCN_ID FROM DRCCN WHERE DRCCN.DRCCN_DRCTR_ID = DRCTR.DRCTR_ID AND DRCCN.DRCCN_A = 1 ORDER BY DRCCN.DRCCN_CTDRC_ID ASC) @wcondiciones ORDER BY DRCTR_NOM";
            } else if (typeof(T1) == typeof(ContribuyenteDetailModel) | typeof(T1) == typeof(ContribuyenteModel)) {
                sqlCommand.CommandText = @"SELECT DRCTR.* FROM DRCTR @wcondiciones ORDER BY DRCTR_NOM";
            }
            // en caso de busqueda
            var dBusqueda = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (dBusqueda != null) {
                if (dBusqueda.FieldValue != string.Empty) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "WHERE LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DRCTR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DRCTR_CLV) || ','|| LOWER(DRCTR_RFC) LIKE @SEARCH @condiciones ");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@SEARCH", "'%" + dBusqueda.FieldValue.ToLower() + "%'");
                }
                conditionals.Remove(dBusqueda);
            }

            var dRelacion = conditionals.Where(it => it.FieldName.ToLower().Contains("drctrr")).FirstOrDefault();
            if (dRelacion != null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("FROM DRCTR", ", DRCTRR.* FROM DRCTR LEFT JOIN DRCTRR ON DRCTR.DRCTR_ID = DRCTRR.DRCTRR_DRCTR_ID ");
            }
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        public IEnumerable<T1> Get2List<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand();

            if (typeof(T1) == typeof(ContribuyenteDomicilioSingleModel)) {
                sqlCommand.CommandText = @"SELECT DRCTR.*, DRCCN.* FROM DRCTR LEFT JOIN DRCCN ON DRCCN.DRCCN_ID = (SELECT FIRST 1 DRCCN.DRCCN_ID FROM DRCCN WHERE DRCCN.DRCCN_DRCTR_ID = DRCTR.DRCTR_ID AND DRCCN.DRCCN_A = 1 ORDER BY DRCCN.DRCCN_CTDRC_ID ASC) @wcondiciones ORDER BY DRCTR_NOM";
            } else if (typeof(T1) == typeof(ContribuyenteDetailModel) | typeof(T1) == typeof(ContribuyenteModel)) {
                sqlCommand.CommandText = @"SELECT DRCTR.* FROM DRCTR @wcondiciones ORDER BY DRCTR_NOM";
            }

            // en caso de busqueda
            var d1 = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
            if (d1 != null) {
                if (d1.FieldValue != string.Empty) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "WHERE LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DRCTR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DRCTR_CLV) || ','|| LOWER(DRCTR_RFC) LIKE @SEARCH @condiciones ");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@SEARCH", "'%" + d1.FieldValue.ToLower() + "%'");
                }
                conditionals.Remove(d1);
            }

            var d2 = conditionals.Where(it => it.FieldName.ToLower().Contains("drctrr")).FirstOrDefault();
            if (d2 != null) {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("FROM DRCTR", ", DRCTRR.* FROM DRCTR LEFT JOIN DRCTRR ON DRCTR.DRCTR_ID = DRCTRR.DRCTRR_DRCTR_ID ");
            }

            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }
    }
}