﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    public class SqlFbContribuyenteCRUD : Abstractions.RepositoryMaster<ContribuyenteModel> {
        public SqlFbContribuyenteCRUD(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Insert(ContribuyenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTR ( DRCTR_ID, DRCTR_CVROLL, DRCTR_APIKEY, DRCTR_A, DRCTR_EXTRNJR, DRCTR_PRNCPL, DRCTR_CMSN_ID, DRCTR_DSCNT_ID, DRCTR_USR, DRCTR_DSP, DRCTR_USOCFDI, DRCTR_USR_N, DRCTR_RFCV, DRCTR_RGSTRPTRNL, DRCTR_RFC, DRCTR_CLV, DRCTR_CURP, DRCTR_RESFIS, DRCTR_NMREG, DRCTR_DOMFIS, DRCTR_PSW, DRCTR_RGF, DRCTR_RLCN, DRCTR_WEB, DRCTR_TEL, DRCTR_MAIL, DRCTR_NOM, DRCTR_NOMC, DRCTR_RGFSC, DRCTR_NTS, DRCTR_CRD, DRCTR_SCRD, DRCTR_PCTD, DRCTR_IVA, DRCTR_USR_ID, DRCTR_SYNC_ID, DRCTR_DSCRD, DRCTR_CTPRC_ID, DRCTR_CTRG_ID, DRCTR_FN) 
                                           VALUES (@DRCTR_ID,@DRCTR_CVROLL,@DRCTR_APIKEY,@DRCTR_A,@DRCTR_EXTRNJR,@DRCTR_PRNCPL,@DRCTR_CMSN_ID,@DRCTR_DSCNT_ID,@DRCTR_USR,@DRCTR_DSP,@DRCTR_USOCFDI,@DRCTR_USR_N,@DRCTR_RFCV,@DRCTR_RGSTRPTRNL,@DRCTR_RFC,@DRCTR_CLV,@DRCTR_CURP,@DRCTR_RESFIS,@DRCTR_NMREG,@DRCTR_DOMFIS,@DRCTR_PSW,@DRCTR_RGF,@DRCTR_RLCN,@DRCTR_WEB,@DRCTR_TEL,@DRCTR_MAIL,@DRCTR_NOM,@DRCTR_NOMC,@DRCTR_RGFSC,@DRCTR_NTS,@DRCTR_CRD,@DRCTR_SCRD,@DRCTR_PCTD,@DRCTR_IVA,@DRCTR_USR_ID,@DRCTR_SYNC_ID,@DRCTR_DSCRD,@DRCTR_CTPRC_ID,@DRCTR_CTRG_ID,@DRCTR_FN) RETURNING DRCTR_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CVROLL", item.ClaveRoll);
            sqlCommand.Parameters.AddWithValue("@DRCTR_APIKEY", item.ApiKEY);
            sqlCommand.Parameters.AddWithValue("@DRCTR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_EXTRNJR", item.Extranjero);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PRNCPL", item.Principal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CMSN_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSCNT_ID", item.IdDescuento);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR", item.User);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSP", item.DiasEntrega);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USOCFDI", item.ClaveUsoCFDI);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFCV", item.IsValidRFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGSTRPTRNL", item.RegistroPatronal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RESFIS", item.ResidenciaFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NMREG", item.NumRegIdTrib);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DOMFIS", item.DomicilioFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PSW", item.Password);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGF", item.Regimen);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RLCN", item.Relacion);
            sqlCommand.Parameters.AddWithValue("@DRCTR_WEB", item.SitioWEB);
            sqlCommand.Parameters.AddWithValue("@DRCTR_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTR_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOMC", item.NombreComercial);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGFSC", item.RegimenFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NTS", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CRD", item.Credito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_SCRD", item.SobreCredito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PCTD", item.FactorDescuento);
            sqlCommand.Parameters.AddWithValue("@DRCTR_IVA", item.FactorIvaPactado);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR_ID", item.IdUsuario);
            sqlCommand.Parameters.AddWithValue("@DRCTR_SYNC_ID", item.IdSincronizado);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSCRD", item.DiasCredito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CTRG_ID", item.IdRegimen);
            sqlCommand.Parameters.AddWithValue("@DRCTR_FN", item.FechaNuevo);
            //sqlCommand.Parameters.AddWithValue("@DRCTR_FILE", item.fi);
            item.IdDirectorio = this.ExecuteScalar(sqlCommand);
            if (item.IdDirectorio > 0)
                return item.IdDirectorio;
            return 0;
        }

        public int Update(ContribuyenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR SET DRCTR_CVROLL = @DRCTR_CVROLL, DRCTR_APIKEY = @DRCTR_APIKEY, DRCTR_A = @DRCTR_A, DRCTR_EXTRNJR = @DRCTR_EXTRNJR, DRCTR_PRNCPL = @DRCTR_PRNCPL,
DRCTR_CMSN_ID = @DRCTR_CMSN_ID, DRCTR_DSCNT_ID = @DRCTR_DSCNT_ID, DRCTR_USR = @DRCTR_USR, DRCTR_DSP = @DRCTR_DSP, DRCTR_USOCFDI = @DRCTR_USOCFDI, DRCTR_USR_M = @DRCTR_USR_M,
DRCTR_RFCV = @DRCTR_RFCV, DRCTR_RGSTRPTRNL = @DRCTR_RGSTRPTRNL, DRCTR_RFC = @DRCTR_RFC, DRCTR_CLV = @DRCTR_CLV, DRCTR_CURP = @DRCTR_CURP, DRCTR_RESFIS = @DRCTR_RESFIS, DRCTR_NMREG = @DRCTR_NMREG, 
DRCTR_DOMFIS = @DRCTR_DOMFIS, DRCTR_PSW = @DRCTR_PSW, DRCTR_RGF = @DRCTR_RGF, DRCTR_RLCN = @DRCTR_RLCN, DRCTR_WEB = @DRCTR_WEB, DRCTR_TEL = @DRCTR_TEL, DRCTR_MAIL = @DRCTR_MAIL, DRCTR_NOM = @DRCTR_NOM, 
DRCTR_NOMC = @DRCTR_NOMC, DRCTR_RGFSC = @DRCTR_RGFSC, DRCTR_NTS = @DRCTR_NTS, DRCTR_CRD = @DRCTR_CRD, DRCTR_SCRD = @DRCTR_SCRD, DRCTR_PCTD = @DRCTR_PCTD, DRCTR_IVA = @DRCTR_IVA, DRCTR_USR_ID = @DRCTR_USR_ID, 
DRCTR_SYNC_ID = @DRCTR_SYNC_ID, DRCTR_DSCRD = @DRCTR_DSCRD, DRCTR_CTPRC_ID = @DRCTR_CTPRC_ID, 
DRCTR_FM = @DRCTR_FM, DRCTR_CTRG_ID = @DRCTR_CTRG_ID WHERE (DRCTR_ID = @DRCTR_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CVROLL", item.ClaveRoll);
            sqlCommand.Parameters.AddWithValue("@DRCTR_APIKEY", item.ApiKEY);
            sqlCommand.Parameters.AddWithValue("@DRCTR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_EXTRNJR", item.Extranjero);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PRNCPL", item.Principal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CMSN_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSCNT_ID", item.IdDescuento);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR", item.User);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSP", item.DiasEntrega);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USOCFDI", item.ClaveUsoCFDI);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFCV", item.IsValidRFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGSTRPTRNL", item.RegistroPatronal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RESFIS", item.ResidenciaFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NMREG", item.NumRegIdTrib);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DOMFIS", item.DomicilioFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PSW", item.Password);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGF", item.Regimen);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RLCN", item.Relacion);
            sqlCommand.Parameters.AddWithValue("@DRCTR_WEB", item.SitioWEB);
            sqlCommand.Parameters.AddWithValue("@DRCTR_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTR_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOMC", item.NombreComercial);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RGFSC", item.RegimenFiscal);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NTS", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CRD", item.Credito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_SCRD", item.SobreCredito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_PCTD", item.FactorDescuento);
            sqlCommand.Parameters.AddWithValue("@DRCTR_IVA", item.FactorIvaPactado);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USR_ID", item.IdUsuario);
            sqlCommand.Parameters.AddWithValue("@DRCTR_SYNC_ID", item.IdSincronizado);
            sqlCommand.Parameters.AddWithValue("@DRCTR_DSCRD", item.DiasCredito);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CTRG_ID", item.IdRegimen);

            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE drctr SET drctr_a = 0 WHERE drctr_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM drctr WHERE drctr_id = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            var tabla = ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<ContribuyenteModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<ContribuyenteModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM DRCTR"
            };
            return GetMapper(sqlCommand);
        }
    }
}