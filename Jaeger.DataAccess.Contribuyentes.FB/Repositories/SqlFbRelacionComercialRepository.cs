﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de relaciones comerciales
    /// </summary>
    public class SqlFbRelacionComercialRepository : Abstractions.RepositoryMaster<RelacionComercialModel>, ISqlRelacionComercialRepository {
        public SqlFbRelacionComercialRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE DRCTRR WHERE DRCTRR_ID = @DRCTRR_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRR_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        [Obsolete("utilizar metodo Saveable")]
        public int Insert(RelacionComercialModel item) {
            return this.Saveable(item);
        }

        [Obsolete("utilizar metodo Saveable")]
        public int Update(RelacionComercialModel item) {
            return this.Saveable(item);
        }

        public RelacionComercialModel GetById(int index) {
            return this.GetList<RelacionComercialModel>(new List<IConditional> { new Conditional("DRCTRR_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<RelacionComercialModel> GetList() {
            return this.GetList<RelacionComercialModel>(new List<IConditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.* FROM DRCTRR @wcondiciones ORDER BY DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID"
            };
            return  this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<RelacionComercialDetailModel> GetList(List<IConditional> conditionals) {
            throw new NotImplementedException();
        }

        public int Saveable(RelacionComercialDetailModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO DRCTRR ( DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID, DRCTRR_CTPRC_ID, DRCTRR_NOTA, DRCTRR_USR_M, DRCTRR_FM, DRCTRR_A) 
                                                      VALUES (@DRCTRR_DRCTR_ID,@DRCTRR_CTREL_ID,@DRCTRR_CTCMS_ID,@DRCTRR_CTDSC_ID,@DRCTRR_CTPRC_ID,@DRCTRR_NOTA,@DRCTRR_USR_M,@DRCTRR_FM,@DRCTRR_A) MATCHING (DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID);"
            };
            item.Modifica = this.User;
            item.FechaModifica = DateTime.Now;
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTREL_ID", item.IdTipoRelacion);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_FM", item.FechaModifica);
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_USR_N", item.Creo);
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTCMS_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTPRC_ID", 0);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTDSC_ID", 0);

            return this.ExecuteTransaction(sqlCommand);
        }

        public int Saveable(IRelacionComercialModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO DRCTRR ( DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID, DRCTRR_CTCMS_ID, DRCTRR_CTDSC_ID, DRCTRR_CTPRC_ID, DRCTRR_NOTA, DRCTRR_USR_M, DRCTRR_FM, DRCTRR_A) 
                                                      VALUES (@DRCTRR_DRCTR_ID,@DRCTRR_CTREL_ID,@DRCTRR_CTCMS_ID,@DRCTRR_CTDSC_ID,@DRCTRR_CTPRC_ID,@DRCTRR_NOTA,@DRCTRR_USR_M,@DRCTRR_FM,@DRCTRR_A) MATCHING (DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID);"
            };
            item.Modifica = this.User;
            item.FechaModifica = DateTime.Now;
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTREL_ID", item.IdTipoRelacion);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_FM", item.FechaModifica);
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_USR_N", item.Creo);
            //sqlCommand.Parameters.AddWithValue("@DRCTRR_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTCMS_ID", item.IdComision);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTPRC_ID", 0);
            sqlCommand.Parameters.AddWithValue("@DRCTRR_CTDSC_ID", 0);

            return this.ExecuteTransaction(sqlCommand);
        }

        public void Saveable(List<IRelacionComercialModel> items) {
            foreach (var item in items) {
                this.Saveable(item);
            }
        }
    }
}
