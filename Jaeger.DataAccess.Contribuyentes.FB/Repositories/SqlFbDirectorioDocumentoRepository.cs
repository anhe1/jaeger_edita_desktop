﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de documentos del directorio
    /// </summary>
    public class SqlFbDirectorioDocumentoRepository : Abstractions.RepositoryMaster<ContribuyenteDocumentoModel>, ISqlDirectorioDocumentoRepository {
        public SqlFbDirectorioDocumentoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM DRCTRM WHERE DRCTRM_ID = @DRCTRM_ID"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRM_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public ContribuyenteDocumentoModel GetById(int index) {
            return this.GetList<ContribuyenteDocumentoModel>(
                new List<IConditional> {
                    new Conditional("DRCTRM_ID", index.ToString())
                }).FirstOrDefault();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRM.* FROM DRCTRM @wcondiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<ContribuyenteDocumentoModel> GetList() {
            return this.GetList<ContribuyenteDocumentoModel>(null);
        }

        public int Insert(ContribuyenteDocumentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTRM  (DRCTRM_ID, DRCTRM_A, DRCTRM_DRCTR_ID, DRCTRM_URL, DRCTRM_NOTA, DRCTRM_FN, DRCTRM_USR_N)
                                            VALUES (@DRCTRM_ID,@DRCTRM_A,@DRCTRM_DRCTR_ID,@DRCTRM_URL,@DRCTRM_NOTA,@DRCTRM_FN,@DRCTRM_USR_N) RETURNING DRCTRM_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTRM_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_USR_N", item.Modifica);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(ContribuyenteDocumentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTRM DRCTRM_A=@DRCTRM_A, DRCTRM_DRCTR_ID=@DRCTRM_DRCTR_ID, DRCTRM_URL=@DRCTRM_URL, DRCTRM_NOTA=@DRCTRM_NOTA, DRCTRM_FN=@DRCTRM_FN, DRCTRM_USR_N=@DRCTRM_USR_N WHERE (DRCTRM_ID=@DRCTRM_ID)"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRM_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRM_USR_N", item.Modifica);

            return this.ExecuteTransaction(sqlCommand);
        }
    }
}
