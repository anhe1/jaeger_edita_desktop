﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Contribuyentes.Contribuyente {
    public class SqlFbFactorDescuentoRepository : Abstractions.RepositoryMaster<FactorPactadoModel>, ISqlFactorDescuentoRepository {
        public SqlFbFactorDescuentoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM DRCTRL WHERE ((DRCTRL_ID = @DRCTRL_ID))" };
            sqlCommand.Parameters.AddWithValue("@DRCTRL_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public FactorPactadoModel GetById(int index) {
            return this.GetList<FactorPactadoModel>(new List<IConditional> {
                new Conditional("DRCTRF_ID", index.ToString())
            }).FirstOrDefault();
        }

        public IEnumerable<FactorPactadoModel> GetList() {
            return this.GetList<FactorPactadoModel>(null);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> condiciones) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRF.* FROM DRCTRF @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, condiciones);
        }

        public int Insert(FactorPactadoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTRF (DRCTRF_ID, DRCTRF_A, DRCTRF_DRCTR_ID, DRCTRF_FACTOR, DRCTRF_FM, DRCTRF_USR_M) 
                                           VALUES (@DRCTRF_ID,@DRCTRF_A,@DRCTRF_DRCTR_ID,@DRCTRF_FACTOR,@DRCTRF_FM,@DRCTRF_USR_M) RETURNING DRCTRF_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRF_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FACTOR", item.Factor);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FM", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_USR_M", item.Modifica);
            if (ExecuteTransaction(sqlCommand) > 0)
                return item.Id;
            return 0;
        }

        public FactorPactadoModel Save(FactorPactadoModel model) {
            model.Modifica = this.User;
            model.FechaNuevo = DateTime.Now;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public int Update(FactorPactadoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTRF (DRCTRF_A=@DRCTRF_a, DRCTRF_DRCTR_ID=@DRCTRF_DRCTR_ID, DRCTRF_FACTOR=@DRCTRF_FACTOR, DRCTRF_FM=@DRCTRF_FM, DRCTRF_USR_M=@DRCTRF_USR_M) WHERE DRCTRF_ID=@DRCTRF_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRF_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FACTOR", item.Factor);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FM", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_USR_M", item.Modifica);

            return ExecuteTransaction(sqlCommand); 
        }

        public int Salveable(IFactorPactadoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO DRCTRR (DRCTRF_DRCTR_ID, DRCTRF_FACTOR, DRCTRF_FM, DRCTRF_USR_M) 
                                                     VALUES (@DRCTRF_DRCTR_ID,@DRCTRF_FACTOR,@DRCTRF_FM,@DRCTRF_USR_M) MATCHING (DRCTRF_DRCTR_ID, DRCTRF_FACTOR);"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTRF_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FACTOR", item.Factor);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_FM", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCTRF_USR_M", item.Modifica);

            return ExecuteTransaction(sqlCommand);
        }
    }
}
