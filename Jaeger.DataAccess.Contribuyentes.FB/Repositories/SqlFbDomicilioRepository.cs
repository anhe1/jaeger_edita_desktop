﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Domicilios, relacion con el directorio
    /// </summary>
    public class SqlFbDomicilioRepository : Abstractions.RepositoryMaster<DomicilioFiscalModel>, ISqlDomicilioRepository {

        public SqlFbDomicilioRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Insert(DomicilioFiscalModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCCN (DRCCN_ID, DRCCN_A, DRCCN_DRCTR_ID, DRCCN_TTL, DRCCN_CDGPS, DRCCN_CP, DRCCN_TP, DRCCN_ASNH, DRCCN_ASNTMNT_ID, DRCCN_EXTR, DRCCN_INTR, DRCCN_CLL, DRCCN_CLN, DRCCN_DLG, DRCCN_CDD, DRCCN_STD, DRCCN_PS, DRCCN_LCLDD, DRCCN_RFRNC, DRCCN_TLFNS, DRCCN_DSP, DRCCN_REQ, DRCCN_USR_N, DRCCN_FN, DRCCN_AUTO_ID, DRCCN_CTDRC_ID)
                                          VALUES (@DRCCN_ID,@DRCCN_A,@DRCCN_DRCTR_ID,@DRCCN_TTL,@DRCCN_CDGPS,@DRCCN_CP,@DRCCN_TP,@DRCCN_ASNH,@DRCCN_ASNTMNT_ID,@DRCCN_EXTR,@DRCCN_INTR,@DRCCN_CLL,@DRCCN_CLN,@DRCCN_DLG,@DRCCN_CDD,@DRCCN_STD,@DRCCN_PS,@DRCCN_LCLDD,@DRCCN_RFRNC,@DRCCN_TLFNS,@DRCCN_DSP,@DRCCN_REQ,@DRCCN_USR_N,@DRCCN_FN,@DRCCN_AUTO_ID,@DRCCN_CTDRC_ID) RETURNING DRCCN_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@DRCCN_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCCN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DRCTR_ID", item.IdContribuyente);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TTL", item.Titulo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CDGPS", item.CodigoPais);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CP", item.CodigoPostal);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TP", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_ASNH", item.Asentamiento);
            sqlCommand.Parameters.AddWithValue("@DRCCN_ASNTMNT_ID", 0);
            sqlCommand.Parameters.AddWithValue("@DRCCN_EXTR", item.NoExterior);
            sqlCommand.Parameters.AddWithValue("@DRCCN_INTR", item.NoInterior);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CLL", item.Calle);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CLN", item.Colonia);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DLG", item.Municipio);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CDD", item.Ciudad);
            sqlCommand.Parameters.AddWithValue("@DRCCN_STD", item.Estado);
            sqlCommand.Parameters.AddWithValue("@DRCCN_PS", item.Pais);
            sqlCommand.Parameters.AddWithValue("@DRCCN_LCLDD", item.Localidad);
            sqlCommand.Parameters.AddWithValue("@DRCCN_RFRNC", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TLFNS", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DSP", item.Notas);
            sqlCommand.Parameters.AddWithValue("@DRCCN_REQ", item.Requerimiento);
            sqlCommand.Parameters.AddWithValue("@DRCCN_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCCN_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@DRCCN_AUTO_ID", item.IdAutorizado);
            //sqlCommand.Parameters.AddWithValue("@DRCCN_SYNC_ID", item.sy);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CTDRC_ID", item.IdTipoDomicilio);
            item.IdDomicilio = this.ExecuteScalar(sqlCommand);
            if (item.IdDomicilio > 0)
                return item.IdDomicilio;
            return 0;
        }

        public int Update(DomicilioFiscalModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCCN SET DRCCN_A = @DRCCN_A, DRCCN_DRCTR_ID = @DRCCN_DRCTR_ID, DRCCN_TTL = @DRCCN_TTL, DRCCN_CDGPS = @DRCCN_CDGPS, DRCCN_CP = @DRCCN_CP, DRCCN_TP = @DRCCN_TP, 
DRCCN_ASNH = @DRCCN_ASNH, DRCCN_ASNTMNT_ID = @DRCCN_ASNTMNT_ID, DRCCN_EXTR = @DRCCN_EXTR, DRCCN_INTR = @DRCCN_INTR, DRCCN_CLL = @DRCCN_CLL, DRCCN_CLN = @DRCCN_CLN, DRCCN_DLG = @DRCCN_DLG, DRCCN_CDD = @DRCCN_CDD, 
DRCCN_STD = @DRCCN_STD, DRCCN_PS = @DRCCN_PS, DRCCN_LCLDD = @DRCCN_LCLDD, DRCCN_RFRNC = @DRCCN_RFRNC, DRCCN_TLFNS = @DRCCN_TLFNS, DRCCN_DSP = @DRCCN_DSP, DRCCN_REQ = @DRCCN_REQ, DRCCN_USR_M = @DRCCN_USR_M, 
DRCCN_FM = @DRCCN_FM, DRCCN_AUTO_ID = @DRCCN_AUTO_ID, DRCCN_CTDRC_ID = @DRCCN_CTDRC_ID WHERE ((DRCCN_ID = @DRCCN_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@DRCCN_ID", item.IdDomicilio);
            sqlCommand.Parameters.AddWithValue("@DRCCN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DRCTR_ID", item.IdContribuyente);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TTL", item.Titulo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CDGPS", item.CodigoPais);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CP", item.CodigoPostal);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TP", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_ASNH", item.Asentamiento);
            sqlCommand.Parameters.AddWithValue("@DRCCN_ASNTMNT_ID", 0);
            sqlCommand.Parameters.AddWithValue("@DRCCN_EXTR", item.NoExterior);
            sqlCommand.Parameters.AddWithValue("@DRCCN_INTR", item.NoInterior);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CLL", item.Calle);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CLN", item.Colonia);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DLG", item.Municipio);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CDD", item.Ciudad);
            sqlCommand.Parameters.AddWithValue("@DRCCN_STD", item.Estado);
            sqlCommand.Parameters.AddWithValue("@DRCCN_PS", item.Pais);
            sqlCommand.Parameters.AddWithValue("@DRCCN_LCLDD", item.Localidad);
            sqlCommand.Parameters.AddWithValue("@DRCCN_RFRNC", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@DRCCN_TLFNS", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCCN_DSP", item.Notas);
            sqlCommand.Parameters.AddWithValue("@DRCCN_REQ", item.Requerimiento);
            sqlCommand.Parameters.AddWithValue("@DRCCN_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@DRCCN_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCCN_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@DRCCN_AUTO_ID", item.IdAutorizado);
            //sqlCommand.Parameters.AddWithValue("@DRCCN_SYNC_ID", item.id);
            sqlCommand.Parameters.AddWithValue("@DRCCN_CTDRC_ID", item.IdTipoDomicilio);

            return ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCCN SET DRCCN_A = 0 WHERE DRCCN_ID = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public DomicilioFiscalModel GetById(int id) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM DRCCN WHERE DRCCN_ID = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", id);
            return this.GetMapper<DomicilioFiscalModel>(sqlCommand).FirstOrDefault();
        }

        public IDomicilioFiscalModel Save(IDomicilioFiscalModel model) {
            if (model.IdDomicilio == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Activo = true;
                model.Creo = this.User;
                model.IdDomicilio = this.Insert((DomicilioFiscalModel)model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update((DomicilioFiscalModel)model);
            }
            return model;
        }

        public IDomicilioFiscalDetailModel Save(IDomicilioFiscalDetailModel model) {
            if (model.IdDomicilio == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Activo = true;
                model.Creo = this.User;
                model.IdDomicilio = this.Insert((DomicilioFiscalModel)model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update((DomicilioFiscalModel)model);
            }
            return model;
        }

        public IEnumerable<DomicilioFiscalModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM DRCCN WHERE drccn_a > 0")
            };
            return this.GetMapper<DomicilioFiscalModel>(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCCN.* FROM DRCCN @wcondiciones ORDER BY DRCCN_ID DESC"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }
    }
}
