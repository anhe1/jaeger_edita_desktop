﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de contactos del directorio
    /// </summary>
    public class SqlFbContactoRepository : Abstractions.RepositoryMaster<ContactoModel>, ISqlContactoRepository {
        public SqlFbContactoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM DRCTRL WHERE ((DRCTRL_ID = @DRCTRL_ID))" };
            sqlCommand.Parameters.AddWithValue("@DRCTRL_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ContactoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTRL (DRCTRL_ID, DRCTRL_A, DRCTRL_DRCTR_ID, DRCTRL_TTEL, DRCTRL_TMAIL, DRCTRL_SUG, DRCTRL_NOM, DRCTRL_PST, DRCTRL_TEL, DRCTRL_MAIL, DRCTRL_NOTA, DRCTRL_USR_N, DRCTRL_FN) 
                                           VALUES (@DRCTRL_ID,@DRCTRL_A,@DRCTRL_DRCTR_ID,@DRCTRL_TTEL,@DRCTRL_TMAIL,@DRCTRL_SUG,@DRCTRL_NOM,@DRCTRL_PST,@DRCTRL_TEL,@DRCTRL_MAIL,@DRCTRL_NOTA,@DRCTRL_USR_N,@DRCTRL_FN) RETURNING DRCTRL_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRL_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TTEL", item.TelefonoTipo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TMAIL", item.CorreoTipo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_SUG", item.TratoSugerido);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_PST", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_FN", item.FechaNuevo);

            item.IdContacto = this.ExecuteScalar(sqlCommand);
            if (item.IdContacto > 0)
                return item.IdContacto;
            return 0;
        }

        public int Update(ContactoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTRL SET DRCTRL_A = @DRCTRL_A, DRCTRL_DRCTR_ID = @DRCTRL_DRCTR_ID, DRCTRL_TTEL = @DRCTRL_TTEL, DRCTRL_TMAIL = @DRCTRL_TMAIL, DRCTRL_SUG = @DRCTRL_SUG, DRCTRL_NOM = @DRCTRL_NOM, DRCTRL_PST = @DRCTRL_PST, DRCTRL_TEL = @DRCTRL_TEL, DRCTRL_MAIL = @DRCTRL_MAIL, DRCTRL_NOTA = @DRCTRL_NOTA, DRCTRL_USR_M = @DRCTRL_USR_M, DRCTRL_FM = @DRCTRL_FM WHERE (DRCTRL_ID = @DRCTRL_ID)"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRL_ID", item.IdContacto);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TTEL", item.TelefonoTipo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TMAIL", item.CorreoTipo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_SUG", item.TratoSugerido);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_PST", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_TEL", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTRL_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public ContactoModel GetById(int index) {
            return this.GetList<ContactoModel>(new List<IConditional> { new Conditional("DRCTRL_ID", index.ToString())}).FirstOrDefault();
        }

        public IEnumerable<ContactoModel> GetList() {
            return this.GetList<ContactoModel>(new List<IConditional>());
        }
        #endregion

        /// <summary>
        /// Lista de contactos del directorio
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="conditionals">IConditional</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRL.* FROM DRCTRL @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// Almacenar un contacto del directorio
        /// </summary>
        public IContactoDetailModel Save(IContactoDetailModel contacto) {
            if (contacto.IdContacto == 0) {
                contacto.Creo = this.User;
                contacto.FechaNuevo = DateTime.Now;
                contacto.IdContacto = this.Insert((ContactoDetailModel)contacto);
            } else {
                contacto.Modifica = this.User;
                contacto.FechaModifica = DateTime.Now;
                this.Update((ContactoDetailModel)contacto);
            }
            return contacto;
        }
    }
}
