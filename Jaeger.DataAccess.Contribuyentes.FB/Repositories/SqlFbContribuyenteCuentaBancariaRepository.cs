﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.DataAccess.FB.Contribuyentes.Repositories {
    /// <summary>
    /// Repositorio de cuentas bancarias del directorio
    /// </summary>
    public class SqlFbContribuyenteCuentaBancariaRepository : Abstractions.RepositoryMaster<CuentaBancariaModel>, ISqlCuentaBancariaRepository {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuracion">IDataBase</param>
        /// <param name="user">IUser</param>
        public SqlFbContribuyenteCuentaBancariaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM DRCTRB WHERE ((DRCTRB_ID = @DRCTRB_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTRB_ID", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(CuentaBancariaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTRB (DRCTRB_ID, DRCTRB_A, DRCTRB_VRFCD, DRCTRB_DRCTR_ID, DRCTRB_CRGMX, DRCTRB_CLV, DRCTRB_REFNUM, DRCTRB_MND, DRCTRB_RFC, DRCTRB_SCRSL, DRCTRB_TIPO, DRCTRB_REFALF, DRCTRB_NMCTA, DRCTRB_CNTCLB, DRCTRB_NMCRT, DRCTRB_PRAPLL, DRCTRB_SGAPLL, DRCTRB_ALIAS, DRCTRB_BANCO, DRCTRB_BNFCR, DRCTRB_USR_N, DRCTRB_FN) 
                                           VALUES (@DRCTRB_ID, @DRCTRB_A, @DRCTRB_VRFCD, @DRCTRB_DRCTR_ID, @DRCTRB_CRGMX, @DRCTRB_CLV, @DRCTRB_REFNUM, @DRCTRB_MND, @DRCTRB_RFC, @DRCTRB_SCRSL, @DRCTRB_TIPO, @DRCTRB_REFALF, @DRCTRB_NMCTA, @DRCTRB_CNTCLB, @DRCTRB_NMCRT, @DRCTRB_PRAPLL, @DRCTRB_SGAPLL, @DRCTRB_ALIAS, @DRCTRB_BANCO, @DRCTRB_BNFCR, @DRCTRB_USR_N, @DRCTRB_FN) RETURNING DRCTRB_ID"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTRB_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_VRFCD", item.Verificado);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CRGMX", item.CargoMaximo);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_REFNUM", item.RefNumerica);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_MND", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_TIPO", item.TipoCuenta);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_REFALF", item.RefAlfanumerica);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_NMCTA", item.NumeroDeCuenta);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CNTCLB", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_NMCRT", item.InsitucionBancaria);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_PRAPLL", item.PrimerApellido);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_SGAPLL", item.SegundoApellido);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_ALIAS", item.Alias);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_BANCO", item.Banco);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_BNFCR", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_FN", item.FechaNuevo);
            item.IdCuenta = this.ExecuteScalar(sqlCommand);
            if (item.IdCuenta > 0)
                return item.IdCuenta;
            return 0;
        }

        public int Update(CuentaBancariaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTRB SET DRCTRB_A = @DRCTRB_A, DRCTRB_VRFCD = @DRCTRB_VRFCD, DRCTRB_DRCTR_ID = @DRCTRB_DRCTR_ID, DRCTRB_CRGMX = @DRCTRB_CRGMX, DRCTRB_CLV = @DRCTRB_CLV, DRCTRB_REFNUM = @DRCTRB_REFNUM, DRCTRB_MND = @DRCTRB_MND, DRCTRB_RFC = @DRCTRB_RFC, DRCTRB_SCRSL = @DRCTRB_SCRSL, DRCTRB_TIPO = @DRCTRB_TIPO, DRCTRB_REFALF = @DRCTRB_REFALF, DRCTRB_NMCTA = @DRCTRB_NMCTA, DRCTRB_CNTCLB = @DRCTRB_CNTCLB, DRCTRB_NMCRT = @DRCTRB_NMCRT, DRCTRB_PRAPLL = @DRCTRB_PRAPLL, DRCTRB_SGAPLL = @DRCTRB_SGAPLL, DRCTRB_ALIAS = @DRCTRB_ALIAS, DRCTRB_BANCO = @DRCTRB_BANCO, DRCTRB_BNFCR = @DRCTRB_BNFCR, DRCTRB_USR_M = @DRCTRB_USR_M, DRCTRB_FM = @DRCTRB_FM WHERE (DRCTRB_ID = @DRCTRB_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@DRCTRB_ID", item.IdCuenta);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_VRFCD", item.Verificado);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CRGMX", item.CargoMaximo);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_REFNUM", item.RefNumerica);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_MND", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_TIPO", item.TipoCuenta);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_REFALF", item.RefAlfanumerica);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_NMCTA", item.NumeroDeCuenta);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_CNTCLB", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_NMCRT", item.InsitucionBancaria);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_PRAPLL", item.PrimerApellido);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_SGAPLL", item.SegundoApellido);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_ALIAS", item.Alias);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_BANCO", item.Banco);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_BNFCR", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTRB_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public CuentaBancariaModel GetById(int index) {
            return this.GetList<CuentaBancariaModel>(new List<IConditional> { new Conditional("DRCTRB_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<CuentaBancariaModel> GetList() {
            return this.GetList<CuentaBancariaModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRB.* FROM DRCTRB @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public ICuentaBancariaModel Save(ICuentaBancariaModel cuentaBancaria) {
            if (cuentaBancaria.IdCuenta == 0) {
                cuentaBancaria.FechaNuevo = DateTime.Now;
                cuentaBancaria.Creo = this.User;
                cuentaBancaria.IdCuenta = this.Insert((CuentaBancariaModel)cuentaBancaria);
            } else {
                cuentaBancaria.FechaModifica = DateTime.Now;
                cuentaBancaria.Modifica = this.User;
                this.Update((CuentaBancariaModel)cuentaBancaria);
            }
            return cuentaBancaria;
        }
    }
}
