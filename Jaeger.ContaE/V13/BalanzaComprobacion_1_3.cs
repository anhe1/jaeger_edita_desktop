﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
// 
// Este código fuente fue generado automáticamente por xsd, Versión=4.0.30319.33440.
// 
namespace Jaeger.ContaE.V13 {
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion", IsNullable = false)]
    public partial class Balanza {
        private BalanzaCtas[] ctasField;

        private string versionField;

        private string rFCField;

        private string mesField; // BalanzaMes

        private int anioField;

        private string tipoEnvioField;

        private System.DateTime fechaModBalField;

        private bool fechaModBalFieldSpecified;

        private string selloField;

        private string noCertificadoField;

        private string certificadoField;

        public Balanza() {
            this.versionField = "1.3";
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute("Ctas")]
        public BalanzaCtas[] Ctas {
            get {
                return this.ctasField;
            }
            set {
                this.ctasField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Mes {
            get {
                return this.mesField;
            }
            set {
                this.mesField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Anio {
            get {
                return this.anioField;
            }
            set {
                this.anioField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TipoEnvio {
            get {
                return this.tipoEnvioField;
            }
            set {
                this.tipoEnvioField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime FechaModBal {
            get {
                return this.fechaModBalField;
            }
            set {
                this.fechaModBalField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FechaModBalSpecified {
            get {
                return this.fechaModBalFieldSpecified;
            }
            set {
                this.fechaModBalFieldSpecified = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sello {
            get {
                return this.selloField;
            }
            set {
                this.selloField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string noCertificado {
            get {
                return this.noCertificadoField;
            }
            set {
                this.noCertificadoField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Certificado {
            get {
                return this.certificadoField;
            }
            set {
                this.certificadoField = value;
            }
        }

        #region metodos

        public Catalogo Load(string archivo) {
            return null;
        }

        public void Save(string archivo) {

        }
        #endregion
    }
}

namespace Jaeger.ContaE.V13 {
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/BalanzaComprobacion")]
    public partial class BalanzaCtas {
        private string numCtaField;

        private decimal saldoIniField;

        private decimal debeField;

        private decimal haberField;

        private decimal saldoFinField;

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumCta {
            get {
                return this.numCtaField;
            }
            set {
                this.numCtaField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal SaldoIni {
            get {
                return this.saldoIniField;
            }
            set {
                this.saldoIniField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Debe {
            get {
                return this.debeField;
            }
            set {
                this.debeField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Haber {
            get {
                return this.haberField;
            }
            set {
                this.haberField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal SaldoFin {
            get {
                return this.saldoFinField;
            }
            set {
                this.saldoFinField = value;
            }
        }
    }
}