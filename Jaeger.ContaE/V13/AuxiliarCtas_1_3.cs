﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
// 
// Este código fuente fue generado automáticamente por xsd, Versión=4.6.1055.0.
// 

namespace Jaeger.ContaE.V13 {
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/AuxiliarCtas")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/AuxiliarCtas", IsNullable = false)]
    public partial class AuxiliarCtas {
        private AuxiliarCtasCuenta[] cuentaField;

        private string versionField;

        private string rFCField;

        private AuxiliarCtasMes mesField;

        private int anioField;

        private string tipoSolicitudField;

        private string numOrdenField;

        private string numTramiteField;

        private string selloField;

        private string noCertificadoField;

        private string certificadoField;

        public AuxiliarCtas() {
            this.versionField = "1.3";
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Cuenta")]
        public AuxiliarCtasCuenta[] Cuenta {
            get {
                return this.cuentaField;
            }
            set {
                this.cuentaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public AuxiliarCtasMes Mes {
            get {
                return this.mesField;
            }
            set {
                this.mesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Anio {
            get {
                return this.anioField;
            }
            set {
                this.anioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TipoSolicitud {
            get {
                return this.tipoSolicitudField;
            }
            set {
                this.tipoSolicitudField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumOrden {
            get {
                return this.numOrdenField;
            }
            set {
                this.numOrdenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumTramite {
            get {
                return this.numTramiteField;
            }
            set {
                this.numTramiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sello {
            get {
                return this.selloField;
            }
            set {
                this.selloField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string noCertificado {
            get {
                return this.noCertificadoField;
            }
            set {
                this.noCertificadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Certificado {
            get {
                return this.certificadoField;
            }
            set {
                this.certificadoField = value;
            }
        }
    }
}

namespace Jaeger.ContaE.V13 {
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/AuxiliarCtas")]
    public partial class AuxiliarCtasCuenta {
        private AuxiliarCtasCuentaDetalleAux[] detalleAuxField;

        private string numCtaField;

        private string desCtaField;

        private decimal saldoIniField;

        private decimal saldoFinField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DetalleAux")]
        public AuxiliarCtasCuentaDetalleAux[] DetalleAux {
            get {
                return this.detalleAuxField;
            }
            set {
                this.detalleAuxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumCta {
            get {
                return this.numCtaField;
            }
            set {
                this.numCtaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string DesCta {
            get {
                return this.desCtaField;
            }
            set {
                this.desCtaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal SaldoIni {
            get {
                return this.saldoIniField;
            }
            set {
                this.saldoIniField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal SaldoFin {
            get {
                return this.saldoFinField;
            }
            set {
                this.saldoFinField = value;
            }
        }
    }
}

namespace Jaeger.ContaE.V13 {
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/AuxiliarCtas")]
    public partial class AuxiliarCtasCuentaDetalleAux {
        private System.DateTime fechaField;

        private string numUnIdenPolField;

        private string conceptoField;

        private decimal debeField;

        private decimal haberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime Fecha {
            get {
                return this.fechaField;
            }
            set {
                this.fechaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumUnIdenPol {
            get {
                return this.numUnIdenPolField;
            }
            set {
                this.numUnIdenPolField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Concepto {
            get {
                return this.conceptoField;
            }
            set {
                this.conceptoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Debe {
            get {
                return this.debeField;
            }
            set {
                this.debeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Haber {
            get {
                return this.haberField;
            }
            set {
                this.haberField = value;
            }
        }
    }
}