﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
// 
// Este código fuente fue generado automáticamente por xsd, Versión=4.0.30319.33440.
// 

using Jaeger.ContaE.Services;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Jaeger.ContaE.V13 {
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas", IsNullable = false)]
    public partial class Catalogo {

        private CatalogoCtas[] ctasField;

        private string versionField;

        private string rFCField;

        private string mesField; //CatalogoMes

        private int anioField;

        private string selloField;

        private string noCertificadoField;

        private string certificadoField;

        public Catalogo() {
            this.versionField = "1.3";
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlElementAttribute("Ctas")]
        public CatalogoCtas[] Ctas {
            get {
                return this.ctasField;
            }
            set {
                this.ctasField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Mes {
            get {
                return this.mesField;
            }
            set {
                this.mesField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Anio {
            get {
                return this.anioField;
            }
            set {
                this.anioField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sello {
            get {
                return this.selloField;
            }
            set {
                this.selloField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string noCertificado {
            get {
                return this.noCertificadoField;
            }
            set {
                this.noCertificadoField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Certificado {
            get {
                return this.certificadoField;
            }
            set {
                this.certificadoField = value;
            }
        }

        private static XmlSerializer objSerializer;

        [XmlAttribute("schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string XsiSchemaLocation;
        [XmlIgnore]
        public string OriginalXmlString {
            set; get;
        }

        private static XmlSerializer Serializer {
            get {
                if (Catalogo.objSerializer == null) {
                    Catalogo.objSerializer = (new XmlSerializerFactory()).CreateSerializer(typeof(Catalogo));
                }
                return Catalogo.objSerializer;
            }
        }

        public static Catalogo Deserialize(string input) {
            Catalogo objComprobante;
            using (StringReader stringReader = new StringReader(input)) {
                Catalogo objComprobante1 = (Catalogo)Catalogo.Serializer.Deserialize(XmlReader.Create(stringReader));
                objComprobante1.OriginalXmlString = input;
                objComprobante = objComprobante1;
            }
            return objComprobante;
        }

        #region metodos

        public static Catalogo Load(string archivo) {
            UTF8Encoding objUtf8WithoutBom = new UTF8Encoding(false);
            Catalogo comprobante;
            try {
                using (FileStream fileStream = new FileStream(archivo, FileMode.Open, FileAccess.Read)) {
                    using (StreamReader streamReader = new StreamReader(fileStream, objUtf8WithoutBom)) {
                        comprobante = Catalogo.Deserialize(streamReader.ReadToEnd());
                    }
                }
                return comprobante;
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("CatalogoCuentas_1_2", ex.Message));
            }
            return null;
        }

        public static Catalogo LoadXml(string xmlString) {
            try {
                return Catalogo.Deserialize(xmlString);
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("CatalogoCuentas_1_3: ", ex.Message));
                return null;
            }
        }

        public static Catalogo LoadBytes(byte[] xmlBytes) {
            Catalogo outComprobante;
            try {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Catalogo));
                XmlTextReader xmlTextReader = new XmlTextReader(new MemoryStream(xmlBytes));
                Catalogo t1 = (Catalogo)xmlSerializer.Deserialize(xmlTextReader);
                xmlTextReader.Close();
                outComprobante = t1;
            } catch (Exception ex) {
                Console.WriteLine(string.Concat("CatalogoCuentas_1_3: ", ex.Message));
                return null;
            }
            return outComprobante;
        }

        public virtual void Save(string fileName) {
            using (StreamWriter streamWriter = new StreamWriter(fileName, false, new UTF8Encoding(false))) {
                streamWriter.WriteLine(this.Serialize());
            }
        }

        public virtual string Serialize() {
            string outString;
            XmlWriterSettings xmlWriterSetting = new XmlWriterSettings() {
                Encoding = Encoding.UTF8,
                Indent = true,
                CloseOutput = false,
                OmitXmlDeclaration = false
            };
            using (MemoryStream memoryStream = new MemoryStream()) {
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSetting);
                XmlSerializerNamespaces xmlSerializerNamespace = new XmlSerializerNamespaces();
                xmlSerializerNamespace.Add("catalogocuentas", "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas");
                xmlSerializerNamespace.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                this.XsiSchemaLocation = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas/CatalogoCuentas_1_3.xsd";
                Catalogo.Serializer.Serialize(xmlWriter, this, xmlSerializerNamespace);
                memoryStream.Seek((long)0, SeekOrigin.Begin);
                using (StreamReader streamReader = new StreamReader(memoryStream, Encoding.UTF8)) {
                    outString = streamReader.ReadToEnd();
                }
            }
            return outString;
        }

        [XmlIgnore]
        public string CadenaOriginal {
            get {
                string outOriginalString;
                using (Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Jaeger.xslt.CatalogoCuentas_1_3.xslt")) {
                    if (manifestResourceStream == null) {
                        outOriginalString = null;
                    } else {
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.Load(manifestResourceStream);
                        using (StringWriter stringWriter = new StringWriter()) {
                            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter)) {
                                XslCompiledTransform xslCompiledTransform = new XslCompiledTransform();
                                xslCompiledTransform.Load(xmlDocument, null, new XmlResolverService());
                                xslCompiledTransform.Transform(new XPathDocument(new StringReader(this.Serialize())), null, xmlTextWriter);
                                outOriginalString = stringWriter.ToString().Replace("&amp;", "&");
                            }
                        }
                    }
                }
                return outOriginalString;
            }
        }

        [XmlIgnore]
        public string CadenaOriginalOff {
            get {
                string outOriginalString;
                using (Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Jaeger.xslt.CatalogoCuentas_1_3.xslt")) {
                    if (manifestResourceStream == null) {
                        outOriginalString = null;
                    } else {
                        XmlDocument xmlDocument = new XmlDocument();
                        xmlDocument.Load(manifestResourceStream);
                        using (StringWriter stringWriter = new StringWriter()) {
                            using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter)) {
                                XslCompiledTransform xslCompiledTransform = new XslCompiledTransform();
                                xslCompiledTransform.Load(xmlDocument, null, new XmlResolverService());
                                xslCompiledTransform.Transform(new XPathDocument(new StringReader(this.Serialize())), null, xmlTextWriter);
                                outOriginalString = stringWriter.ToString().Replace("&amp;", "&");
                            }
                        }
                    }
                }
                return outOriginalString;
            }
        }

        #endregion
    }
}

namespace Jaeger.ContaE.V13 {
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.sat.gob.mx/esquemas/ContabilidadE/1_3/CatalogoCuentas")]
    public partial class CatalogoCtas {
        private string codAgrupField; //c_CodAgrup

        private string numCtaField;

        private string descField;

        private string subCtaDeField;

        private int nivelField;

        private string naturField;

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string CodAgrup {
            get {
                return this.codAgrupField;
            }
            set {
                this.codAgrupField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NumCta {
            get {
                return this.numCtaField;
            }
            set {
                this.numCtaField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Desc {
            get {
                return this.descField;
            }
            set {
                this.descField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SubCtaDe {
            get {
                return this.subCtaDeField;
            }
            set {
                this.subCtaDeField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Nivel {
            get {
                return this.nivelField;
            }
            set {
                this.nivelField = value;
            }
        }

        /// <comentarios/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Natur {
            get {
                return this.naturField;
            }
            set {
                this.naturField = value;
            }
        }
    }
}

