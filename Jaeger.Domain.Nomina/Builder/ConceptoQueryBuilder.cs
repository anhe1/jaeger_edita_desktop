﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Nomina.Builder {
    /// <summary>
    /// clase para consulta de conceptos de nomina
    /// </summary>
    public class ConceptoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConceptoQueryBuilder, IConceptoOnlyActiveQueryBuilder, IConceptoByIdEmpleadoQueryBuilder {
        public IConceptoOnlyActiveQueryBuilder IsActive(bool isTrue = true) {
            if (isTrue)
                this._Conditionals.Add(new Conditional("NMCNP_A", "1"));
            return this;
        }

        public IConceptoByIdEmpleadoQueryBuilder IdEmpleado(int idEmpleado) {
            return this;
        }
    }
}
