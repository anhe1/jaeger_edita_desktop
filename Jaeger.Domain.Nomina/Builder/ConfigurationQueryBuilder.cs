﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Nomina.Builder {
    public class ConfigurationQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConfigurationQueryBuilder, IConfigurationLastQueryBuilder {
        public ConfigurationQueryBuilder(): base() { }

        public IConfigurationLastQueryBuilder Last() {
            this._Conditionals.Add(new Conditional("NMCNF_ID", "3"));
            return this;
        }
    }
}
