﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface INominaControlQueryBuilder : IConditionalBuilder, IConditionalBuild {
        INominaControlIdQueryBuilder IdControl(int index);
        INominaControlYearQueryBuilder Year(int year);
        INominaComprobanteIdComprobanteQueryBuilder IdComprobante(string uuid);
        INominaControlDepartamentoQueryBuilder IdDepartamento(int id);
        INominaControlDepartamentoQueryBuilder IdDepartamento(string descripcion);
        INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(int id);
        INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(string nombre);
        INominaControlFechaEmisionQueryBuilder FechaInicial(System.DateTime fecha);
    }

    public interface INominaControlIdQueryBuilder : IConditionalBuilder {
        INominaControlDepartamentoQueryBuilder IdDepartamento(int id);
        INominaControlDepartamentoQueryBuilder IdDepartamento(string descripcion);
        INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(int id);
        INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(string nombre);
    }

    public interface INominaControlYearQueryBuilder : IConditionalBuilder {
        INominaControlMonthQueryBuilder Month(int month);
    }

    public interface INominaControlMonthQueryBuilder : IConditionalBuilder {

    }

    public interface INominaComprobanteIdEmpleadoQueryBuilder : IConditionalBuilder {

    }

    public interface INominaComprobanteIdComprobanteQueryBuilder : IConditionalBuilder {

    }

    public interface INominaControlDepartamentoQueryBuilder : IConditionalBuilder {

    }

    public interface INominaControlFechaEmisionQueryBuilder : IConditionalBuilder {
        INominaControlFechaEmisionQueryBuilder FechaFinal(System.DateTime fecha);
    }
}
