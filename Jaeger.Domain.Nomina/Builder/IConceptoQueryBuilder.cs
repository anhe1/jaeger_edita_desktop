﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IConceptoQueryBuilder : IConditionalBuilder, IConditionalBuild, IConceptoByIdEmpleadoQueryBuilder {
        IConceptoOnlyActiveQueryBuilder IsActive(bool isTrue = true);
    }

    public interface IConceptoOnlyActiveQueryBuilder : IConditionalBuilder {
        IConceptoByIdEmpleadoQueryBuilder IdEmpleado(int idEmpleado);
    }

    public interface IConceptoByIdEmpleadoQueryBuilder : IConditionalBuilder { }
}
