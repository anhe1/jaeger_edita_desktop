﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Nomina.Builder {
    public class EmpleadoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IEmpleadoQueryBuilder, IEmpleadoActiveQueryBuilder {
        public EmpleadoQueryBuilder() : base() { }

        public IEmpleadoActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) {
                this.Conditionals.Add(new Conditional("CTEMP_A", "1"));
            }
            return this;
        }
    }
}
