﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IEmpleadoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IEmpleadoActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IEmpleadoActiveQueryBuilder : IConditionalBuilder, IConditionalBuild {
    }
}
