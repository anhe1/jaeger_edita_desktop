﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IConfigurationQueryBuilder {
        IConfigurationLastQueryBuilder Last();
    }

    public interface IConfigurationLastQueryBuilder : IConditionalBuilder {
    }
}
