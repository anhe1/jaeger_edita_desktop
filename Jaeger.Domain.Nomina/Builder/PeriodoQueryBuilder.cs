﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Nomina.Builder {
    public class PeriodoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IPeriodoQueryBuilder, IPeriodoByIdQueryBuilder, IPeriodoYearQueryBuilder {

        public PeriodoQueryBuilder() : base() { }

        public IPeriodoByIdQueryBuilder ById(int index) {
            this._Conditionals.Add(new Conditional("NMCAL_NOM_ID", index.ToString()));
            return this;
        }

        public IPeriodoYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("EXTRACT(YEAR FROM NMPRD_FCINI)", year.ToString()));
            return this;
        }

        public IConditionalBuilder Month(int month) {
            this._Conditionals.Add(new Conditional("EXTRACT(MONTH FROM NMPRD_FCINI", month.ToString()));
            return this;
        }
    }
}
