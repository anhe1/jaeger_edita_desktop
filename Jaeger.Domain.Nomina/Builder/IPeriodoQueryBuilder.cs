﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IPeriodoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IPeriodoByIdQueryBuilder ById(int index);

        IPeriodoYearQueryBuilder Year(int year);
    }

    public interface IPeriodoByIdQueryBuilder : IConditionalBuilder {

    }

    public interface IPeriodoYearQueryBuilder : IConditionalBuilder {
        IConditionalBuilder Month(int month);
    }

    public interface IPeriodoMonthQueryBuilder : IConditionalBuilder {

    }
}
