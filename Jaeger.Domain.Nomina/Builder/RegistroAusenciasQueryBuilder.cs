﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Builder {
    public class RegistroAusenciasQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IRegistroAusenciasQueryBuilder, IRegistroAusenciasIdNominaQueryBuilder, IRegistroAusenciasIdEmpleadoQueryBuilder, 
        IRegistroAusenciasTipoQueryBuilder, IRegistroAusenciasYearQueryBuilder {

        public RegistroAusenciasQueryBuilder() : base() { }

        public IRegistroAusenciasYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("EXTRACT(YEAR FROM NMRD_FEC)", year.ToString()));
            return this;
        }

        public IRegistroAusenciasIdNominaQueryBuilder IdNomina(int idPeriodo) {
            this._Conditionals.Add(new Conditional("NMRD_NMPRD_ID", idPeriodo.ToString()));
            return this;
        }

        public IRegistroAusenciasIdEmpleadoQueryBuilder IdEmpleado(int idNomina) {
            this._Conditionals.Add(new Conditional("NMRD_EMP_ID", idNomina.ToString()));
            return this;
        }

        public IRegistroAusenciasTipoQueryBuilder Tipo(DiasTipoEnum tipo) {
            var index = (int)tipo;
            this._Conditionals.Add(new Conditional("NMRD_CTTP_ID", index.ToString()));
            return this;
        }
    }
}
