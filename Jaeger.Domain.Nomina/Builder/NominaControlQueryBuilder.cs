﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Nomina.Builder {
    public class NominaControlQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, INominaControlIdQueryBuilder, INominaControlQueryBuilder, INominaControlYearQueryBuilder, INominaControlMonthQueryBuilder,
        INominaComprobanteIdComprobanteQueryBuilder, INominaComprobanteIdEmpleadoQueryBuilder, INominaControlDepartamentoQueryBuilder, INominaControlFechaEmisionQueryBuilder {
        public NominaControlQueryBuilder() : base() { }

        public INominaControlIdQueryBuilder IdControl(int index) {
            this._Conditionals.Add(new Conditional("_cfdnmn_nmnctrl_id", index.ToString()));
            return this;
        }

        public INominaComprobanteIdComprobanteQueryBuilder IdComprobante(string uuid) {
            this._Conditionals.Add(new Conditional("_cfdi_uuid", uuid));
            return this;
        }

        public INominaControlYearQueryBuilder Year(int year) {
            this._Conditionals.Add(new Conditional("_cfdi_anio", year.ToString()));
            return this;
        }

        public INominaControlMonthQueryBuilder Month(int month) {
            if (month > 0)
                this._Conditionals.Add(new Conditional("_cfdi_mes", month.ToString()));
            return this;
        }

        public INominaControlDepartamentoQueryBuilder IdDepartamento(int id) {
            return this;
        }

        public INominaControlDepartamentoQueryBuilder IdDepartamento(string descripcion) {
            this._Conditionals.Add(new Conditional("_cfdnmn_depto", descripcion, Base.ValueObjects.ConditionalTypeEnum.Like));
            return this;
        }

        public INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(int id) {
            return this;
        }

        public INominaComprobanteIdEmpleadoQueryBuilder IdEmpleado(string nombre) {
            this._Conditionals.Add(new Conditional("_cfdi_nomr", nombre, Base.ValueObjects.ConditionalTypeEnum.Like));
            return this;
        }

        public INominaControlFechaEmisionQueryBuilder FechaInicial(System.DateTime dateTime) {
            this._Conditionals.Add(new Conditional("_cfdi_fecems", dateTime.ToString("yyyy-MM-dd"), Base.ValueObjects.ConditionalTypeEnum.GreaterThanOrEqual));
            return this;
        }

        public INominaControlFechaEmisionQueryBuilder FechaFinal(System.DateTime dateTime) {
            this._Conditionals.Add(new Conditional("_cfdi_fecems", dateTime.ToString("yyyy-MM-dd"), Base.ValueObjects.ConditionalTypeEnum.LessThanOrEqual));
            return this;
        }
    }
}
