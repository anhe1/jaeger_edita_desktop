﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IHorasExtraQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IHorasExtraIdNominaQueryBuilder IdNomina(int  id);
        IHorasExtraIdEmpleadoQueryBuilder WithIdEmpleado(int id);
    }

    public interface IHorasExtraIdEmpleadoQueryBuilder : IConditionalBuilder {

    }

    public interface IHorasExtraIdNominaQueryBuilder : IConditionalBuilder {
        IHorasExtraIdEmpleadoQueryBuilder WithIdEmpleado(int id);
    }
}
