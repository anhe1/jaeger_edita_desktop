﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Nomina.Builder {
    public class HorasExtraQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IHorasExtraQueryBuilder, IHorasExtraIdEmpleadoQueryBuilder, IHorasExtraIdNominaQueryBuilder {
        public HorasExtraQueryBuilder() : base() { }

        public IHorasExtraIdNominaQueryBuilder IdNomina(int id) {
            return this;
        }
        public IHorasExtraIdEmpleadoQueryBuilder WithIdEmpleado(int id) {
            return this;
        }
    }
}
