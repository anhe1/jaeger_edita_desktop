﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Builder {
    public interface IRegistroAusenciasQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IRegistroAusenciasIdNominaQueryBuilder IdNomina(int idPeriodo);
        IRegistroAusenciasIdEmpleadoQueryBuilder IdEmpleado(int idEmpleado);
        IRegistroAusenciasYearQueryBuilder Year(int year);
    }

    public interface IRegistroAusenciasIdNominaQueryBuilder : IConditionalBuilder {
        IRegistroAusenciasIdEmpleadoQueryBuilder IdEmpleado(int idEmpleado);
    }

    public interface IRegistroAusenciasIdEmpleadoQueryBuilder : IConditionalBuilder {
        IRegistroAusenciasTipoQueryBuilder Tipo(DiasTipoEnum tipo);
    }

    public interface IRegistroAusenciasTipoQueryBuilder : IConditionalBuilder {
        
    }

    public interface IRegistroAusenciasYearQueryBuilder : IConditionalBuilder {
        IRegistroAusenciasIdEmpleadoQueryBuilder IdEmpleado(int idEmpleado);
    }
}
