﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// Vista simple de empleados, para el caso de los comprobantes de nomina 
    /// </summary>
    public class EmpleadoSingleModel {
        public EmpleadoSingleModel() { }

        [DataNames("CTEMP_NOM", "_ctlemp_nom", "_cfdi_nomr")]
        public string Nombre { get; set; }
    }
}
