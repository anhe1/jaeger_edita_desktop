﻿namespace Jaeger.Domain.Nomina.Entities {
    public class DiasTipoModel : Base.Abstractions.BaseSingleModel {
        public DiasTipoModel() {
        }

        public DiasTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
