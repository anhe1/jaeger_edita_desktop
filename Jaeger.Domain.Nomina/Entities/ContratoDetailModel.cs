﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class ContratoDetailModel : ContratoModel, IContratoModel, IEmpleadoModel, IContratoDetailModel {
        #region declaraciones
        private string _clave;
        private string _rfc;
        private string _curp;
        private string _nombre;
        private string _primerApellido;
        private string _segundoApellido;
        private string _numeroSeguridadSocial;
        private string _unidadMedicaFamiliar;
        private string _fonacot;
        private string _afore;
        private int _idEntidadFed;
        private string _lugarNacimiento;
        private string _nacionalidad;
        private string _correo;
        private string _sitio;
        private DateTime? _fecNacimiento;
        private int _edad;
        private int _idGenero;
        private int _idEdoCivil;
        private string _telefono1;
        private string _telefono2;
        #endregion

        public ContratoDetailModel() : base() { }

        #region informacion del empleado
        [DataNames("CTEMP_DRCTR_ID")]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave de registro en sistema
        /// </summary>
        [DataNames("CTEMP_CLV")]
        public string Clave {
            get { return this._clave; }
            set {
                this._clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        [DataNames("CTEMP_RFC")]
        public string RFC {
            get { return this._rfc; }
            set {
                this._rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        [DataNames("CTEMP_CURP")]
        public string CURP {
            get { return this._curp; }
            set {
                this._curp = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_NOM")]
        public string Nombre {
            get { return this._nombre; }
            set {
                this._nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_PAPE")]
        public string ApellidoPaterno {
            get { return this._primerApellido; }
            set {
                this._primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SAPE")]
        public string ApellidoMaterno {
            get { return this._segundoApellido; }
            set {
                this._segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de seguridad social (NSS)
        /// </summary>
        [DataNames("CTEMP_NSS")]
        public string NSS {
            get { return this._numeroSeguridadSocial; }
            set {
                this._numeroSeguridadSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad medica familiar (UMF)
        /// </summary>
        [DataNames("CTEMP_UMF")]
        public string UMF {
            get { return this._unidadMedicaFamiliar; }
            set {
                this._unidadMedicaFamiliar = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_FONAC")]
        public string FONACOT {
            get { return this._fonacot; }
            set {
                this._fonacot = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_AFORE")]
        public string AFORE {
            get { return this._afore; }
            set {
                this._afore = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_ENTFED")]
        public int IdEntidadFed {
            get { return this._idEntidadFed; }
            set {
                this._idEntidadFed = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_LGNAC")]
        public string LugarNacimiento {
            get { return this._lugarNacimiento; }
            set {
                this._lugarNacimiento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_NAC")]
        public string Nacionalidad {
            get { return this._nacionalidad; }
            set {
                this._nacionalidad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_MAIL")]
        public string Correo {
            get { return this._correo; }
            set {
                this._correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SITIO")]
        public string Sitio {
            get { return this._sitio; }
            set {
                this._sitio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_FNAC")]
        public DateTime? FecNacimiento {
            get { return this._fecNacimiento; }
            set {
                this._fecNacimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la edad del empleado (solo lectura porque se calcula en la base CTEMP_EDAD)
        /// </summary>
        [DataNames("CTEMP_EDAD")]
        public int Edad {
            get { return this._edad; }
            set {
                this._edad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SEXO")]
        public int IdGenero {
            get { return this._idGenero; }
            set {
                this._idGenero = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_ECVL")]
        public int IdEdoCivil {
            get { return this._idEdoCivil; }
            set {
                this._idEdoCivil = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_TLFN")]
        public string Telefono1 {
            get { return this._telefono1; }
            set {
                this._telefono1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_TLFN2")]
        public string Telefono2 {
            get { return this._telefono2; }
            set {
                this._telefono2 = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        public string FullName {
            get { return string.Format("{0} {1} {2}", this.ApellidoPaterno, this.ApellidoMaterno, this.Nombre); }
        }
    }
}
