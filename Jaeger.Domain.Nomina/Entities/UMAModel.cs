﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class UMAModel : Base.Abstractions.BasePropertyChangeImplementation, IUMAModel {
        private int _index;
        private int _anio;
        private decimal _diario;
        private decimal _mensual;
        private decimal _anual;

        public UMAModel() {
        }

        [DataNames("UMA_ID")]
        public int Id {
            get { return _index; }
            set {
                _index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("UMA_ANIO")]
        public int Anio {
            get { return _anio; }
            set { _anio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("UMA_DIARIO")]
        public decimal Diario {
            get { return _diario; }
            set {
                _diario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("UMA_MENSUAL")]
        public decimal Mensual {
            get { return _mensual; }
            set {
                this._mensual = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("UMA_ANUAL")]
        public decimal Anual {
            get { return _anual; }
            set {
                this._anual = value;
                this.OnPropertyChanged();
            }
        }
    }
}
