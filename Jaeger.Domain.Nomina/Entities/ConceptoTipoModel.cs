﻿namespace Jaeger.Domain.Nomina.Entities {
    public class ConceptoTipoModel : Base.Abstractions.BaseSingleModel {
        public ConceptoTipoModel() {

        }

        public ConceptoTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
