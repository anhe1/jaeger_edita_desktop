﻿namespace Jaeger.Domain.Nomina.Entities {
    public class CuentaBancariaTipoModel : Base.Abstractions.BaseSingleModel {
        public CuentaBancariaTipoModel() {

        }

        public CuentaBancariaTipoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
