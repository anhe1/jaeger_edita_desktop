﻿namespace Jaeger.Domain.Nomina.Entities {
    public class ConceptoBaseFiscal : Base.Abstractions.BaseSingleModel {
        public ConceptoBaseFiscal() {
        }

        public ConceptoBaseFiscal(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
