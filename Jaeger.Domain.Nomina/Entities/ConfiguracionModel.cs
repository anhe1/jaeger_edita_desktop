﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class ConfiguracionModel : Base.Abstractions.BasePropertyChangeImplementation, IConfiguracionModel {
        #region declaraciones
        private int _index;
        private int _ejercicio;
        private int _idTablaISR;
        private int _idTablaSubsidio;
        private int _idPeriodo;
        private int _diasSemana;
        private int _diasPorAnio;
        private int _diasPeriodo;
        private int _horasDia;
        private decimal _salarioMinimo;
        private decimal _uma;
        private string _creo;
        private DateTime _fechaNuevo;
        private string _modifica;
        private DateTime? _fechaNodifica;
        private decimal _ExcedenteE;
        private decimal _CuotaFijaP;
        private decimal _PrestacionDineroE;
        private decimal _PensionadosYBeneficiarios;
        private decimal _InvalidezYVidaP;
        private decimal _CesntiaYVejezE;
        private decimal _ExcedenteP;
        private decimal _PrestacionDineroP;
        private decimal _PensionadosYBeneficiariosP;
        private decimal _RiesgoTrabajo;
        private decimal _GuarderiasYPrestacionesSociales;
        private decimal _SeguroDeRetiro;
        private decimal _CesantiaYVejezP;
        private decimal _Infonavit;
        private decimal _InvalidezYVidaE;
        #endregion

        public ConfiguracionModel() {

        }

        [DataNames("NMCNF_ID")]
        public int IdConfiguracion {
            get { return this._index; }
            set {
                this._index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_ANIO")]
        public int Ejercicio {
            get { return this._ejercicio; }
            set {
                this._ejercicio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_TISR_ID")]
        public int IdTablaISR {
            get { return this._idTablaISR; }
            set {
                this._idTablaISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_TSBD_ID")]
        public int IdTablaSubsidio {
            get { return this._idTablaSubsidio; }
            set {
                this._idTablaSubsidio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_PRD_ID")]
        public int IdPeriodo {
            get { return this._idPeriodo; }
            set {
                this._idPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_DXS")]
        public int DiasSemana {
            get { return this._diasSemana; }
            set {
                this._diasSemana = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_HD")]
        public int HorasDia {
            get { return this._horasDia; }
            set {
                this._horasDia = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_DXA")]
        public int DiasXAnio {
            get { return this._diasPorAnio; }
            set {
                _diasPorAnio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_DP")]
        public int DiasPeriodo {
            get { return this._diasPeriodo; }
            set {
                this._diasPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Salario Minimo Vigente para el periodo
        /// </summary>
        [DataNames("NMCNF_SMV")]
        public decimal SalarioMinimo {
            get { return this._salarioMinimo; }
            set {
                this._salarioMinimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer la Unidad de Medida y Actualización (UMA)
        /// </summary>
        [DataNames("NMCNF_UMAV")]
        public decimal UMA {
            get { return this._uma; }
            set {
                this._uma = value;
                this.OnPropertyChanged();
            }
        }

        #region cuotas imss empleado
        /// <summary>
        /// Enfermedad y Maternidad, Especie Cuota Fija (CUOTAS IMSS Empleado) 
        /// </summary>
        [DataNames("NMCNF_EEX_E")]
        public decimal ExcedenteE {
            get { return this._ExcedenteE; }
            set {
                this._ExcedenteE = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_PED_E")]
        public decimal PrestacionDineroE {
            get { return this._PrestacionDineroE; }
            set {
                this._PrestacionDineroE = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_PYB_E")]
        public decimal PensionadosYBeneficiariosE {
            get { return this._PensionadosYBeneficiarios; }
            set {
                this._PensionadosYBeneficiarios = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_IYV_E")]
        public decimal InvalidezYVidaE {
            get { return this._InvalidezYVidaE; }
            set {
                this._InvalidezYVidaE = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_CYV_E")]
        public decimal CesantiaYVejezE {
            get { return this._CesntiaYVejezE; }
            set {
                this._CesntiaYVejezE = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cuotas imss patron
        [DataNames("NMCNF_ECF_P")]
        public decimal CuotaFijaP {
            get { return this._CuotaFijaP; }
            set {
                this._CuotaFijaP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_EEX_P")]
        public decimal ExcedenteP {
            get { return this._ExcedenteP; }
            set {
                this._ExcedenteP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_PED_P")]
        public decimal PrestacionDineroP {
            get { return this._PrestacionDineroP; }
            set {
                this._PrestacionDineroP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_PYB_P")]
        public decimal PensionadosYBeneficiariosP {
            get { return this._PensionadosYBeneficiariosP; }
            set {
                this._PensionadosYBeneficiariosP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_IYV_P")]
        public decimal InvalidezYVidaP {
            get { return this._InvalidezYVidaP; }
            set {
                this._InvalidezYVidaP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_RDT_P")]
        public decimal RiesgoTrabajo {
            get { return this._RiesgoTrabajo; }
            set {
                this._RiesgoTrabajo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_GPS_P")]
        public decimal PrestacionesSociales {
            get { return this._GuarderiasYPrestacionesSociales; }
            set {
                this._GuarderiasYPrestacionesSociales = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_SAR_P")]
        public decimal SeguroDeRetiro {
            get { return this._SeguroDeRetiro; }
            set {
                this._SeguroDeRetiro = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("NMCNF_CYV_P")]
        public decimal CesantiaYVejezP {
            get { return this._CesantiaYVejezP; }
            set {
                this._CesantiaYVejezP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_INF_P")]
        public decimal Infonavit {
            get { return this._Infonavit; }
            set {
                this._Infonavit = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        [DataNames("NMCNF_USR_N")]
        public string Creo {
            get { return this._creo; }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CNCNF_FN")]
        public DateTime FechaNuevo {
            get { return this._fechaNuevo; }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNF_USR_M")]
        public string Modifica {
            get { return this._modifica; }
            set {
                this._modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CNCNF_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._fechaNodifica >= new DateTime(1900, 1, 1))
                    return this._fechaNodifica;
                return null;
            }
            set {
                this._fechaNodifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
