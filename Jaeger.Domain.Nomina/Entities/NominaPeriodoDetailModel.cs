﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// nomina: periodo de pago de nomina de empleados (tabla: _nmnprd)
    /// </summary>
    public class NominaPeriodoDetailModel : PeriodoModel, INominaPeriodoDetailModel, IPeriodoModel {

        private BindingList<INominaPeriodoEmpleadoDetailModel> empleados;

        public NominaPeriodoDetailModel() : base() {
            this.Status = PeriodoStatusEnum.PorAutorizar;
            this.empleados = new BindingList<INominaPeriodoEmpleadoDetailModel>();
        }

        /// <summary>
        /// obtener o establecer el status de la nomina
        /// </summary>
        public PeriodoStatusEnum Status {
            get { return (PeriodoStatusEnum)base.IdStatus; }
            set {
                base.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de nomina Normal o Extraordinaria
        /// </summary>
        public NominaTipoEnum Tipo {
            get { return (NominaTipoEnum)base.IdTipo; }
            set {
                base.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de periodo, semanal, quincenal, etc
        /// </summary>
        public PeriodoTipoEnum TipoPeriodo {
            get { return (PeriodoTipoEnum)base.IdPeriodoTipo; }
            set {
                base.IdPeriodoTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<INominaPeriodoEmpleadoDetailModel> Empleados {
            get { return this.empleados; }
            set {
                this.empleados = value;
                this.OnPropertyChanged();
            }
        }
    }
}
