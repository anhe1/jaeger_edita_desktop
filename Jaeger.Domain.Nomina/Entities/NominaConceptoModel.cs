﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase para el catalogo de concepto de nomina
    /// </summary>
    public class NominaConceptoModel : Base.Abstractions.BasePropertyChangeImplementation, INominaConceptoModel {
        #region declaraciones
        private int index;
        private bool activo;
        private int subTipo;
        private string clave;
        private string tipo;
        private string concepto;
        private int idaplicacion;
        private bool visible;
        private int idTipoNomina;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifico;
        private int idTabla;
        private string formula;
        private int idBaseISR;
        private int idBaseIMS;
        private string formulaISR;
        private string formulaIMS;
        private bool _PagoEspecie;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public NominaConceptoModel() {
            this.subTipo = (int)NominaConceptoTipoEnum.Percepcion;
            this.activo = true;
        }

        /// <summary>
        /// constructor
        /// </summary>
        public NominaConceptoModel(NominaConceptoTipoEnum subTipo, string clave, string tipo, string concepto) {
            this.IdTipo = (int)subTipo;
            this.Clave = clave;
            this.ClaveSAT = tipo;
            this.Concepto = concepto;
        }

        /// <summary>
        /// obtener o establecer el indice del concepto
        /// </summary>
        [DataNames("NMCNP_ID")]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("NMCNP_A")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                if (this.activo != value) {
                    this.activo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de concepto de nomina (Percepcion o deduccion)
        /// </summary>
        [DataNames("NMCNP_TP_ID")]
        public int IdTipo {
            get {
                return this.subTipo;
            }
            set {
                if (this.subTipo != value) {
                    this.subTipo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de nomina a aplicar
        /// </summary>
        [DataNames("NMCNP_TNM_ID")]
        public int IdTipoNomina {
            get { return this.idTipoNomina; }
            set { this.idTipoNomina = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_TBL_ID")]
        public int IdTabla {
            get {
                return this.idTabla;
            }
            set {
                this.idTabla = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno 
        /// </summary>
        [DataNames("NMCNP_CLV")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                if (this.clave != value) {
                    this.clave = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el tipo SAT (clave del catalogo de conceptos de nomina SAT)
        /// </summary>
        [DataNames("NMCNP_CLVSAT")]
        public string ClaveSAT {
            get {
                return this.tipo;
            }
            set {
                if (this.tipo != value) {
                    this.tipo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el concepto de nomina
        /// </summary>
        [DataNames("NMCNP_NOM")]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                if (this.concepto != value) {
                    this.concepto = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("NMCNP_FORM")]
        public string Formula {
            get {
                return this.formula;
            }
            set {
                this.formula = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la aplicacion, verdadero si es aplicacion general, falso para aplicacion individual
        /// </summary>
        [DataNames("NMCNP_APL_ID")]
        public int IdAplicacion {
            get {
                return this.idaplicacion;
            }
            set {
                if (this.idaplicacion != value) {
                    this.idaplicacion = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer si el concepto se mostrara en la representacion impresa
        /// </summary>
        [DataNames("NMCNP_VIS")]
        public bool Visible {
            get {
                return this.visible;
            }
            set {
                if (this.visible != value) {
                    this.visible = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [DataNames("NMCNP_ISR_BF")]
        public int IdBaseISR {
            get { return this.idBaseISR; }
            set { this.idBaseISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_ISR_FORM")]
        public string FormulaISR {
            get { return this.formulaISR; }
            set { this.formulaISR = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_IMS_BF")]
        public int IdBaseIMS {
            get { return this.idBaseIMS; }
            set { this.idBaseIMS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_IMS_FORM")]
        public string FormulaIMS {
            get { return this.formulaIMS; }
            set { this.formulaIMS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_FRMPG_ID")]
        public bool PagoEspecie {
            get { return this._PagoEspecie; }
            set { this._PagoEspecie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("NMCNP_FN")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("NMCNP_FM")]
        public DateTime? FechaModifico {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("NMCNP_USR_N")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("NMCNP_USR_M")]
        public string Modifico {
            get {
                return this.modifico;
            }
            set {
                this.modifico = value;
                this.OnPropertyChanged();
            }
        }
    }
}
