﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {

    public class RegistroAusenciasModel : Base.Abstractions.BasePropertyChangeImplementation, IRegistroAusenciasModel {
        #region declaraciones
        private int _IdEmpleado;
        private DateTime _FechaInicial;
        private DateTime _fechaNuevo;
        private string _creo;
        private int _IdNomina;
        private int _IdConcepto;
        private int _IdTipo;
        private string _Nota;
        #endregion

        public RegistroAusenciasModel() { }

        /// <summary>
        /// obtener o establecer indice del concepto de nomina
        /// </summary>
        [DataNames("NMRD_NMCNP_ID")]
        public int IdConcepto {
            get { return this._IdConcepto; }
            set {
                this._IdConcepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion del empleado
        /// </summary>
        [DataNames("NMRD_EMP_ID")]
        public int IdEmpleado {
            get { return this._IdEmpleado; }
            set {
                this._IdEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de nomina
        /// </summary>
        [DataNames("NMRD_NMPRD_ID")]
        public int IdNomina {
            get { return this._IdNomina; }
            set {
                this._IdNomina = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMRD_CTTP_ID")]
        public int IdTipo {
            get {  return this._IdTipo; }
            set { this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMRD_FEC")]
        public DateTime Fecha {
            get { return this._FechaInicial; }
            set {
                this._FechaInicial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("NMRD_NOTA")]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMRD_USR_N")]
        public string Creo {
            get { return this._creo; }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMRD_FN")]
        public DateTime FechaNuevo {
            get { return this._fechaNuevo; }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        public DiasTipoEnum Tipo {
            get { return (DiasTipoEnum)this.IdTipo; }
            set { this.IdTipo = (int)value;
            this.OnPropertyChanged();
            }
        }
    }
}
