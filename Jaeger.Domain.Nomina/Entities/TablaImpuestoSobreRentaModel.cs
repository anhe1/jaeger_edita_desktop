﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Domain.Nomina.Entities {
    public class TablaImpuestoSobreRentaModel : Base.Abstractions.BasePropertyChangeImplementation, ITablaImpuestoSobreRentaModel {
        private int indiceField;
        private BindingList<ImpuestoSobreRentaModel> rangos;
        private int etiquetaField;
        private int periodoField;

        public TablaImpuestoSobreRentaModel() {
            this.rangos = new BindingList<ImpuestoSobreRentaModel>();
        }

        [DataNames("ISR_ID")]
        public int Id {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }
        [DataNames("ISR_ANIO")]
        public int Anio {
            get {
                return etiquetaField;
            }
            set {
                etiquetaField = value;
                this.OnPropertyChanged();
            }
        }
        [DataNames("ISR_PRD")]
        public int Periodo {
            get {
                return periodoField;
            }
            set {
                periodoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion {
            get {
                if (this.Periodo > 0) {
                    var _periodo = Enum.Parse(typeof(PeriodoTipoEnum), this.Periodo.ToString());
                    return _periodo.ToString() + " - " + this.Anio.ToString();
                }
                return this.Periodo.ToString();
            }
        }

        public BindingList<ImpuestoSobreRentaModel> Rangos {
            get {
                return this.rangos;
            }
            set {
                this.rangos = value;
                this.OnPropertyChanged();
            }
        }

        public ImpuestoSobreRentaModel GetImpuestoSobreRenta(decimal baseGravable) {
            try {
                return this.rangos.First((ImpuestoSobreRentaModel p) => baseGravable >= p.LimiteInferior & baseGravable <= p.LimiteSuperior);
            } catch (Exception) {
                ImpuestoSobreRentaModel isr = new ImpuestoSobreRentaModel() { LimiteInferior = 0 };
                return isr;
            }
        }

        public decimal Calcular(decimal baseGravable) {
            ImpuestoSobreRentaModel t = this.GetImpuestoSobreRenta(baseGravable);
            decimal excedenteLimiteInferior = baseGravable - t.LimiteInferior;
            decimal impuestoMarginal = (excedenteLimiteInferior * t.PorcentajeExcedente) / 100;
            decimal impuestoDeterminado = impuestoMarginal + t.CuotaFija;
            return impuestoDeterminado;
        }
    }
}
