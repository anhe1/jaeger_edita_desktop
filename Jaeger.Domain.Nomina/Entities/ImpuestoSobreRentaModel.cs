﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class ImpuestoSobreRentaModel : BasePropertyChangeImplementation {
        private int index;
        private int subIndex;
        private decimal limiteInferiorField;
        private decimal limiteSuperiorField;
        private decimal cuotaFijaField;
        private decimal excendenteInferiorField;

        public ImpuestoSobreRentaModel() {

        }

        public ImpuestoSobreRentaModel(decimal limiteInferiorField, decimal limiteSuperiorField, decimal cuotaFijaField, decimal excendenteInferior) {
            this.limiteInferiorField = limiteInferiorField;
            this.limiteSuperiorField = limiteSuperiorField;
            this.cuotaFijaField = cuotaFijaField;
            this.excendenteInferiorField = excendenteInferior;
        }

        [DataNames("ISRC_ID")]
        public int Id {
            get { return index; }
            set { this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISRC_ISR_ID")]
        public int SubId {
            get { return subIndex; }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISRC_LI")]
        public decimal LimiteInferior {
            get {
                return this.limiteInferiorField;
            }
            set {
                this.limiteInferiorField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISRC_LS")]
        public decimal LimiteSuperior {
            get {
                return this.limiteSuperiorField;
            }
            set {
                this.limiteSuperiorField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISRC_CUOTA")]
        public decimal CuotaFija {
            get {
                return this.cuotaFijaField;
            }
            set {
                this.cuotaFijaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISRC_EXC")]
        public decimal PorcentajeExcedente {
            get {
                return this.excendenteInferiorField;
            }
            set {
                this.excendenteInferiorField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal ISR(decimal baseGravable) {
            decimal excedenteLimiteInferior = baseGravable - this.LimiteInferior;
            decimal impuestoMarginal = (excedenteLimiteInferior * this.PorcentajeExcedente) / 100;
            decimal impuestoDeterminado = impuestoMarginal + this.CuotaFija;
            return impuestoDeterminado;
        }
    }
}
