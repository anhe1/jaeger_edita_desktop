﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// Nomina: concepto de nomina aplicable al empleado
    /// </summary>
    public class EmpleadoConceptoNominaModel : BasePropertyChangeImplementation, IEmpleadoConceptoNominaModel {
        private int index;
        private bool activo;
        private int idConceptoNomina;
        private int idEmpleado;
        private decimal importeGravado;
        private int idTabla;

        public EmpleadoConceptoNominaModel() {
            this.activo = true;
        }

        /// <summary>
        /// obtener o establecer indice principal de la tabla (_ctlempc_id)
        /// </summary>
        [DataNames("CTEMPD_ID")]
        public int Id {
            get { return index; }
            set {
                index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CTEMPD_a")]
        public bool Activo {
            get { return activo; }
            set {
                activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla del catalogo de empleados
        /// </summary>
        [DataNames("CTEMPD_CTEMP_ID")]
        public int IdEmpleado {
            get { return idEmpleado; }
            set {
                idEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de conceptos de nomina
        /// </summary>
        [DataNames("CTEMPD_NMCNP_ID")]
        public int IdConcepto {
            get { return idConceptoNomina; }
            set {
                idConceptoNomina = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tablas de sistema
        /// </summary>
        [DataNames("CTEMPD_TBL_ID")]
        public int IdTabla {
            get { return this.idTabla; }
            set {
                this.idTabla = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer monto del importe fijo
        /// </summary>
        [DataNames("CTEMPD_IMPFJ")]
        public decimal ImporteGravado {
            get { return importeGravado; }
            set {
                importeGravado = value;
                this.OnPropertyChanged();
            }
        }
    }
}
