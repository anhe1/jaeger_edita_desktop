﻿namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase model para tipos de nomina Ordinaria, Extraordinaria
    /// </summary>
    public class TipoNominaModel : Base.Abstractions.BaseSingleModel {
        public TipoNominaModel() {
        }
        public TipoNominaModel(int id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }
    }
}
