﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase modelo para puesto
    /// </summary>
    public class PuestoModel : Base.Abstractions.BasePropertyChangeImplementation, IPuestoModel {
        #region declaraciones
        private int _IdPuesto;
        private bool _Activo;
        private int _IdDepartamento;
        private string _Descripcion;
        private string _creo;
        private DateTime _fechaNuevo;
        private string _modifica;
        private DateTime? _fechaNodifica;
        private string _CtaContable;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public PuestoModel() {
            this._Activo = true;
            this._fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice del puesto
        /// </summary>
        [DataNames("CTDPTP_ID")]
        public int IdPuesto {
            get { return this._IdPuesto; }
            set {
                this._IdPuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("CTDPTP_A")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento
        /// </summary>
        [DataNames("CTDPTP_CTDPT_ID")]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del puesto
        /// </summary>
        [DataNames("CTDPTP_NOM")]
        public string Descripcion {
            get { return this._Descripcion; }
            set {
                this._Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el sueldo del puesto
        /// </summary>
        [DataNames("CTDPTP_SLDSG")]
        public decimal Sueldo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el sueldo maximo
        /// </summary>
        [DataNames("CTDPTP_SLDMX")]
        public decimal SueldoMaximo {
            get; set;
        }

        /// <summary>
        /// obtener o establecer clave del riesgo de puesto
        /// </summary>
        [DataNames("CTDPTP_RSGPST")]
        public int RiesgoPuesto {
            get; set;
        }

        public string CtaContable {
            get { return this._CtaContable; }
            set {
                this._CtaContable = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("CTDPTP_USR_N")]
        public string Creo {
            get { return this._creo; }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("CTDPTP_FN")]
        public DateTime FechaNuevo {
            get { return this._fechaNuevo; }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTDPTP_USR_M")]
        public string Modifica {
            get { return this._modifica; }
            set {
                this._modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTDPTP_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._fechaNodifica >= new DateTime(1900, 1, 1))
                    return this._fechaNodifica;
                return null;
            }
            set {
                this._fechaNodifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
