﻿namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase para configuraciones
    /// </summary>
    public class Configuration : Base.Abstractions.Configuration, Contracts.IConfiguration, Base.Contracts.IConfiguration {
        private DataBase.Contracts.IDataBaseConfiguracion _DataBase;

        /// <summary>
        /// constructor
        /// </summary>
        public  Configuration() {
            this.ModoProductivo = false;
        }

        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        public bool ModoProductivo { get; set; }

        /// <summary>
        /// obtener o establecer carpeta en el bucket
        /// </summary>
        public string Folder { get; set; }

        public string Percepciones { get; set; }
        
        public string Deducciones { get; set; }

        public string OtrosPagos { get; set; }

        /// <summary>
        /// obtener o establecer configuración de base de datos
        /// </summary>
        public DataBase.Contracts.IDataBaseConfiguracion DataBase {
            get {
                return this._DataBase;
            }
            set {
                this._DataBase = value;
                this.OnPropertyChanged();
            }
        }
    }
}
