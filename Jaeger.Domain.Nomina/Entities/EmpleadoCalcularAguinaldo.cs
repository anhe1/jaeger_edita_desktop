﻿using System;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Domain.Nomina.Entities {
    public class EmpleadoCalcularAguinaldo : Base.Abstractions.BasePropertyChangeImplementation, IEmpleadoCalcularAguinaldo {
        #region declaraciones
        private readonly ITablaImpuestoSobreRentaModel tablaIsr;
        private readonly ITablaSubsidioAlEmpleoModel tablaSubsidio;
        private int idEmpleado;
        private string clave;
        private string nombrecompleto;
        private decimal salarioDiario;
        private decimal diasLaborados;
        private DateTime? _fecInicioRelLaboral;
        private decimal _valorUMA;
        private decimal _salarioMinimo;
        private decimal _diasAguinaldo;
        private decimal _Ausencias;
        #endregion

        public EmpleadoCalcularAguinaldo(int idEmpleado, string clave, string nombre, decimal salarioDiario, ITablaImpuestoSobreRentaModel tablaISR, ITablaSubsidioAlEmpleoModel tablaSubsidio, decimal valorUMA) {
            this.idEmpleado = idEmpleado;
            this.clave = clave;
            this.nombrecompleto = nombre;
            this.salarioDiario = salarioDiario;
            this.tablaIsr = tablaISR;
            this.tablaSubsidio = tablaSubsidio;
            this.diasLaborados = 365;
            this._valorUMA = valorUMA;
            this.DiasAguinaldo = 15;
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer indice o numero de empleado
        /// </summary>
        public int IdEmpleado {
            get {
                return this.idEmpleado;
            }
            set {
                this.idEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del empleado
        /// </summary>
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre completo del empleado o trabajador
        /// </summary>
        public string Nombre {
            get {
                return this.nombrecompleto;
            }
            set {
                this.nombrecompleto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del salario diario
        /// </summary>
        public decimal SalarioDiario {
            get {
                return this.salarioDiario;
            }
            set {
                this.salarioDiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del salario mensual
        /// </summary>
        public decimal SalarioMensual {
            get {
                return this.SalarioDiario * new decimal(30);
            }
        }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        public DateTime? FecInicioRelLaboral {
            get {
                if (this._fecInicioRelLaboral >= new DateTime(1900, 1, 1))
                    return this._fecInicioRelLaboral;
                return null;
            }
            set {
                this._fecInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario minimo general
        /// </summary>
        public decimal SalarioMinimoGeneral {
            get { return this._salarioMinimo; }
            set {
                this._salarioMinimo = value;
                this.OnPropertyChanged();
            }
        }

        public decimal DiasAguinaldo {
            get { return this._diasAguinaldo; }
            set {
                this._diasAguinaldo = value;
                this.OnPropertyChanged();
            }
        }

        public decimal UMA {
            get { return this._valorUMA; }
            set {
                this._valorUMA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el monto del aguinaldo
        /// </summary>
        public decimal Importe {
            get {
                //return ((this.SalarioDiario * this.DiasAguinaldo) / 365) * this.DiasLaborados;
                return ((this.SalarioDiario * (this.DiasAguinaldo / 365)) * this.DiasLaborados);
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de dias laborados para el aguinaldo
        /// </summary>
        public decimal DiasLaborados {
            get {
                return this.diasLaborados;
            }
            set {
                this.diasLaborados = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Ausencias {
            get {
                return this._Ausencias;
            }
            set {
                this._Ausencias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer base gravable
        /// </summary>
        public decimal BaseGravable {
            get {
                //if (this.configuracion.ArticuloISR == Configuracion.EnumArticuloISR.Articulo96) {
                // return (this.SalarioMensual + this.Importe) - this.BaseExento;
                //}
                return this.SalarioMensual + (((this.Importe - this.BaseExento) / 365) * new decimal(30.4));
            }
        }

        /// <summary>
        /// obtener o establecer base exento
        /// </summary>
        public decimal BaseExento {
            get {
                //if (this.configuracion.UsarParaCalcular == Configuracion.EnumUsarParaCalcular.UMA) {
                return this.UMA * 30;
                //}
                //return this.SalarioMinimoGeneral * 30;
            }
        }

        /// <summary>
        /// obtener o establecer base gravada
        /// </summary>
        public decimal BaseGravada {
            get {
                decimal c = this.Importe - this.BaseExento;
                if (c <= 0) {
                    return 0;
                }
                return this.Importe - this.BaseExento;
            }
        }

        public decimal ISR {
            get {
                decimal c = this.tablaIsr.Calcular(this.BaseGravable);
                if (c < 0) {
                    return 0;
                }
                return c;
            }
        }

        public decimal Subsidio {
            get {
                return this.tablaSubsidio.GetSubsidio(this.BaseGravable).Cantidad;
            }
        }

        public decimal ISRSalarioMasAguinaldo {
            get {
                return this.ISR - this.Subsidio;
            }
        }

        public decimal ISRSueldo {
            get {
                decimal c = this.tablaIsr.Calcular(this.SalarioMensual) - this.tablaSubsidio.GetSubsidio(this.SalarioMensual).Cantidad;
                if (c < 0) {
                    return 0;
                }
                return c;
            }
        }

        public decimal ISRAguinaldo {
            get {
                decimal c = (this.ISRSalarioMasAguinaldo - this.ISRSueldo) / (((this.Importe - this.BaseExento) / 365) * new decimal(30.4)) * (this.Importe - this.BaseExento);
                if (c < 0) {
                    return 0;
                }
                return c;
            }
        }

        public decimal AguinaldoPagar {
            get {
                return this.Importe - this.ISRAguinaldo;
            }
        }
        #endregion
    }
}
