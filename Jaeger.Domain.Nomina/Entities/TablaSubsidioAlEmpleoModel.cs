﻿using System;
using System.Linq;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.Domain.Nomina.Contracts;
using System.Collections.Generic;

namespace Jaeger.Domain.Nomina.Entities {
    public class TablaSubsidioAlEmpleoModel : Base.Abstractions.BasePropertyChangeImplementation, ITablaSubsidioAlEmpleoModel {
        private List<ISubsidio> subsidios;
        private int indiceField;
        private int etiquetaField;
        private int periodoField;

        public TablaSubsidioAlEmpleoModel() {
            this.subsidios = new List<ISubsidio>();
        }

        [DataNames("ISR_ID")]
        public int Id {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISR_ANIO")]
        public int Anio {
            get {
                return etiquetaField;
            }
            set {
                etiquetaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ISR_PRD")]
        public int Periodo {
            get {
                return periodoField;
            }
            set {
                periodoField = value;
                this.OnPropertyChanged();
            }
        }

        public string Descripcion {
            get {
                if (this.Periodo > 0) {
                    var _periodo = Enum.Parse(typeof(PeriodoTipoEnum), this.Periodo.ToString());
                    return _periodo.ToString() + " - " + this.Anio.ToString();
                }
                return this.Periodo.ToString();
            }
        }

        public List<ISubsidio> Rangos {
            get {
                return this.subsidios;
            }
            set {
                this.subsidios = value;
                this.OnPropertyChanged();
            }
        }

        public ISubsidio GetSubsidio(decimal baseGravable) {
            try {
                return this.Rangos.First((ISubsidio p) => baseGravable >= p.IngresosDesde & baseGravable <= p.IngresosHasta);
            } catch (Exception) {
                ISubsidio t = new Subsidio();
                return t;
            }
        }
    }
}
