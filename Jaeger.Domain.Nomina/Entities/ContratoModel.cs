﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase para contratos de empleados
    /// </summary>
    public class ContratoModel : BasePropertyChangeImplementation, IContratoModel {
        #region declaraciones
        private int _idContrato;
        private bool _activo;
        private int _idEmpleado;
        private int _idDepartamento;
        private int _idPuesto;
        private string _claveTipoRegimen;
        private string _claveRiesgoPuesto;
        private string _clavePeriodicidadPago;
        private int _claveMetodoPago;
        private string _claveTipoContrato;
        private string _claveTipoJornada;
        private string _registroPatronal;
        private string _departamento;
        private string _puesto;
        private DateTime? _fecInicioRelLaboral;
        private DateTime? _fecTerminoRelLaboral;
        private decimal _salarioBase;
        private decimal _salarioBaseCotizacion;
        private decimal _salarioDiario;
        private decimal _salarioDiarioIntegrado;
        private int _jornadas;
        private int _horas;
        private string _claveBanco;
        private string _numeroCuenta;
        private string _sucursal;
        private int _idTipoCuenta;
        private string _clabe;
        private string _vales;
        private string _creo;
        private DateTime _fechaNuevo;
        private string _modifica;
        private DateTime? _fechaNodifica;
        private int _premioID;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ContratoModel() {
            this._activo = true;
        }

        /// <summary>
        /// obtener o establecer indice del contrato
        /// </summary>
        [DataNames("CTEMPC_ID")]
        public int IdContrato {
            get { return this._idContrato; }
            set { this._idContrato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("CTEMPC_A")]
        public bool Activo {
            get { return this._activo; }
            set {
                this._activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio de empleados (CTEMPC_CTEMP_ID)
        /// </summary>
        [DataNames("CTEMPC_CTEMP_ID")]
        public int IdEmpleado {
            get { return this._idEmpleado; }
            set {
                this._idEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del departamento
        /// </summary>
        [DataNames("CTEMPC_DEPTOID")]
        public int IdDepartamento {
            get { return this._idDepartamento; }
            set {
                this._idDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del puesto ocupado
        /// </summary>
        [DataNames("CTEMPC_PSTID")]
        public int IdPuesto {
            get { return this._idPuesto; }
            set {
                this._idPuesto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_PPRD_ID")]
        public int IdPremio {
            get { return this._premioID; }
            set { this._premioID = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de tipo de regimen
        /// </summary>
        [DataNames("CTEMPC_TPREG")]
        public string ClaveTipoRegimen {
            get { return this._claveTipoRegimen; }
            set {
                this._claveTipoRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave riesgo de puesto.
        /// </summary>
        [DataNames("CTEMPC_RSGPST")]
        public string ClaveRiesgoPuesto {
            get { return this._claveRiesgoPuesto; }
            set {
                this._claveRiesgoPuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave periodicidad de pago
        /// </summary>
        [DataNames("CTEMPC_PRDDPG")]
        public string ClavePeriodicidadPago {
            get { return this._clavePeriodicidadPago; }
            set {
                this._clavePeriodicidadPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de metodo de pago
        /// </summary>
        [DataNames("CTEMPC_MTDPG")]
        public int ClaveMetodoPago {
            get {
                return this._claveMetodoPago;
            }
            set {
                this._claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave tipo de contrato
        /// </summary>
        [DataNames("CTEMPC_TPCNTR")]
        public string ClaveTipoContrato {
            get { return this._claveTipoContrato; }
            set {
                this._claveTipoContrato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de tipo de jornada
        /// </summary>
        [DataNames("CTEMPC_TPJRN")]
        public string ClaveTipoJornada {
            get { return this._claveTipoJornada; }
            set {
                this._claveTipoJornada = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        [DataNames("CTEMPC_RGP")]
        public string RegistroPatronal {
            get { return this._registroPatronal; }
            set {
                this._registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_DEPTO")]
        public string Departamento {
            get { return this._departamento; }
            set {
                this._departamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer puesto
        /// </summary>
        [DataNames("CTEMPC_PUESTO")]
        public string Puesto {
            get { return this._puesto; }
            set {
                this._puesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        [DataNames("CTEMPC_FCHREL")]
        public DateTime? FecInicioRelLaboral {
            get { return this._fecInicioRelLaboral; }
            set {
                this._fecInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de termino de relacion laboral
        /// </summary>
        [DataNames("CTEMPC_FCHTERLAB")]
        public DateTime? FecTerminoRelLaboral {
            get { return this._fecTerminoRelLaboral; }
            set {
                this._fecTerminoRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario base
        /// </summary>
        [DataNames("CTEMPC_SB")]
        public decimal SalarioBase {
            get { return this._salarioBase; }
            set {
                this._salarioBase = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_SBC")]
        public decimal SalarioBaseCotizacion {
            get { return this._salarioBaseCotizacion; }
            set { this._salarioBaseCotizacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario diario
        /// </summary>
        [DataNames("CTEMPC_SD")]
        public decimal SalarioDiario {
            get { return this._salarioDiario; }
            set {
                this._salarioDiario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_SDI")]
        public decimal SalarioDiarioIntegrado {
            get { return this._salarioDiarioIntegrado; }
            set { this._salarioDiarioIntegrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de jornadas de trabajo.
        /// </summary>
        [DataNames("CTEMPC_JRNTR")]
        public int Jornadas {
            get { return this._jornadas; }
            set {
                this._jornadas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de horas de la jornadas de trabajo
        /// </summary>
        [DataNames("CTEMPC_JRNHRS")]
        public int Horas {
            get { return this._horas; }
            set {
                this._horas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de banco
        /// </summary>
        [DataNames("CTEMPC_CLVBNC")]
        public string ClaveBanco {
            get { return this._claveBanco; }
            set {
                this._claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cuenta bancaria
        /// </summary>
        [DataNames("CTEMPC_CTABAN")]
        public string NumeroCuenta {
            get { return this._numeroCuenta; }
            set {
                this._numeroCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de sucursal bancaria
        /// </summary>
        [DataNames("CTEMPC_SCRSL")]
        public string Sucursal {
            get { return this._sucursal; }
            set {
                this._sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        [DataNames("CTEMPC_TPCTA")]
        public int IdTipoCuenta {
            get { return this._idTipoCuenta; }
            set {
                this._idTipoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cuenta interbancaria
        /// </summary>
        [DataNames("CTEMPC_CLABE")]
        public string Clabe {
            get { return this._clabe; }
            set {
                this._clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cuenta de tarjeta de vales de despensa.
        /// </summary>
        [DataNames("CTEMPC_CTAVAL")]
        public string Vales {
            get { return this._vales; }
            set {
                this._vales = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_USR_N")]
        public string Creo {
            get { return this._creo; }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_FN")]
        public DateTime FechaNuevo {
            get { return this._fechaNuevo; }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_USR_M")]
        public string Modifica {
            get { return this._modifica; }
            set {
                this._modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPC_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._fechaNodifica >= new DateTime(1900, 1, 1))
                    return this._fechaNodifica;
                return null;
            }
            set {
                this._fechaNodifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
