﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class NominaPeriodoEmpleadoModel : Base.Abstractions.BasePropertyChangeImplementation, INominaPeriodoEmpleadoModel {
        #region declaraciones
        private int index;
        private int idPeriodo;
        private int idEmpleado;
        private bool puntualidad;
        private int ausencias;
        private int incapacidad;
        private decimal horasRelojField;
        private decimal horasAutorizadas;
        private decimal salarioDiario;
        private decimal salarioDiarioIntegrado;
        private decimal horasJornadaTrabajo;
        private int jornadasTrabajo;
        private int productividad;
        private int diasVacaciones;
        private decimal _descuentoInfonavit;
        private int _DiasPeriodo;
        private int _HorasXDia;
        private int _DiasXAnio;
        private int _DiasAguinaldo;
        private decimal _PrimaVacacional;
        private decimal _SalarioMinimo;
        #endregion

        public NominaPeriodoEmpleadoModel() { }

        /// <summary>
        /// _nmnprde_id 
        /// </summary>
        [DataNames("NMCAL_ID")]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del periodo NMCAL_PRD_ID
        /// </summary>
        [DataNames("NMCAL_NOM_ID")]
        public int IdPeriodo {
            get { return idPeriodo; }
            set {
                idPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indce del empleado del catalogo (EMP_ID)
        /// </summary>
        [DataNames("EMP_ID")]
        public int IdEmpleado {
            get { return idEmpleado; }
            set {
                idEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno
        /// </summary>
        [DataNames("EMP_CLV")]
        public string Clave { get; set; }

        /// <summary>
        /// obtener o establecer fecha calculo
        /// </summary>
        [DataNames("FCC")]
        public DateTime? FechaCal { get; set; }

        /// <summary>
        /// obtener o establecer fecha de ingreso
        /// </summary>
        [DataNames("FIRL")]
        public DateTime? FechaIngreso { get; set; }

        /// <summary>
        /// obtener o establecer bandera para puntualidad y asistencia
        /// </summary>
        [DataNames("BPP")]
        public bool Puntualidad {
            get { return this.puntualidad; }
            set {
                this.puntualidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtene o establecer las horas de la jornada de trabajo
        /// </summary>
        [DataNames("HRJR")]
        public decimal HorasJornadaTrabajo {
            get {
                return this.horasJornadaTrabajo;
            }
            set {
                this.horasJornadaTrabajo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer horas de relog
        /// </summary>
        [DataNames("HRLJ")]
        public decimal HorasReloj {
            get { return this.horasRelojField; }
            set {
                this.horasRelojField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las horas extra autorizadas
        /// </summary>
        [DataNames("HRAT")]
        public decimal HorasAutorizadas {
            get { return this.horasAutorizadas; }
            set {
                this.horasAutorizadas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el % del premio de produccion
        /// </summary>
        [DataNames("EFI")]
        [Obsolete]
        public int Productividad {
            get {
                return this.productividad;
            }
            set {
                this.productividad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de incapacidad
        /// </summary>
        [DataNames("INC")]
        public int Incapacidad {
            get { return this.incapacidad; }
            set {
                this.incapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ausencias
        /// </summary>
        [DataNames("AUS")]
        public int Ausencia {
            get { return this.ausencias; }
            set {
                this.ausencias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias de vacaciones
        /// </summary>
        [DataNames("VAC")]
        public int TotalVacaciones {
            get { return this.diasVacaciones; }
            set {
                this.diasVacaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descuento diario por pago de credito de vivienda (infonavit)
        /// </summary>
        [DataNames("INF")]
        public decimal DescuentoInfonavit {
            get { return this._descuentoInfonavit; }
            set {
                this._descuentoInfonavit = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descuento por anticipo a salarios
        /// </summary>
        [DataNames("DASL")]
        public decimal DescuentoPrestamo { get; set; }

        /// <summary>
        /// obtener o establecer salario diario
        /// </summary>
        [DataNames("SD")]
        public decimal SalarioDiario {
            get { return this.salarioDiario; }
            set {
                this.salarioDiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer salario diario integrado personalizado
        /// </summary>
        [DataNames("SDIP")]
        public decimal SDIP {
            get { return this.salarioDiarioIntegrado; }
            set {
                this.salarioDiarioIntegrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de jornadas de trabajo
        /// </summary>
        [DataNames("JT")]
        public int JornadasTrabajo {
            get {
                return this.jornadasTrabajo;
            }
            set {
                this.jornadasTrabajo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer horas por dia
        /// </summary>
        [DataNames("HD")]
        public int HorasXDia {
            get { return this._HorasXDia; }
            set {
                this._HorasXDia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias del periodo
        /// </summary>
        [DataNames("DP")]
        public int DiasPeriodo {
            get { return this._DiasPeriodo; }
            set {
                this._DiasPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("DXA")]
        public int DiasXAnio {
            get { return this._DiasXAnio; }
            set {
                this._DiasXAnio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("DAG")]
        public int DiasAguinaldo {
            get { return this._DiasAguinaldo; }
            set {
                this._DiasAguinaldo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("PV")]
        public decimal PrimaVacacional {
            get { return this._PrimaVacacional; }
            set {
                this._PrimaVacacional = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("SM")]
        public decimal SalarioMinimo {
            get { return this._SalarioMinimo; }
            set {
                this._SalarioMinimo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// unidad de medida y actulizacion
        /// </summary>
        [DataNames("UMA")]
        public decimal UMA { get; set; }

        [DataNames("TISR_ID")]
        public int IdTablaISR { get; set; }

        [DataNames("TSBD_ID")]
        public int IdTablaSube { get; set; }

        [DataNames("SM3")]
        public decimal SM3 { get; set; }

        [DataNames("UMA3")]
        public decimal UMA3 { get; set; }

        /// <summary>
        /// obtener o establecer numero de horas extra autorizadas (horas por dia menos horas de reloj)
        /// </summary>
        [DataNames("HREXT")]
        public decimal TotalHorasExtra { get; set; }

        /// <summary>
        /// obtener o establecer años de servicio
        /// </summary>
        [DataNames("ADS")]
        public decimal AnioServicio { get; set; }

        [DataNames("DVC")]
        public decimal DiasVacaciones {
            get; set;
        }

        [DataNames("FSDI")]
        public decimal FactorSDI { get; set; }

        [DataNames("SDI")]
        public decimal SDI { get; set; }

        [DataNames("EXC_E")]
        public decimal Exedente { get; set; }

        [DataNames("TPGRV")]
        public decimal PercepcionGravado { get; set; }

        [DataNames("TPEXE")]
        public decimal PercepcionExento { get; set; }

        [DataNames("TDEDU")]
        public decimal Deducciones { get; set; }

        [DataNames("ISPT")]
        public decimal ISPT { get; set; }

        [DataNames("SUBE")]
        public decimal SubSidioAlEmpleo { get; set; }

        [DataNames("IMSS_MENE")]
        public decimal IMSSMensualE { get; set; }

        [DataNames("IMSS_BIME")]
        public decimal IMSSBimestralE { get; set; }

        [DataNames("IMSS_TOTE")]
        public decimal IMSSTotalE { get; set; }

        [DataNames("IMSS_MENP")]
        public decimal IMSSMensualP { get; set; }

        [DataNames("IMSS_BIMP")]
        public decimal IMSSBimestralP { get; set; }

        [DataNames("IMSS_INFP")]
        public decimal Infonavit { get; set; }

        [DataNames("IMSS_TOTP")]
        public decimal IMSSTotalP { get; set; }
    }
}
