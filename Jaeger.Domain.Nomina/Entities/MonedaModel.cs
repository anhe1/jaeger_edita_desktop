﻿namespace Jaeger.Domain.Nomina.Entities {
    public class MonedaModel : Base.Abstractions.BaseSingleTipoModel {
        public MonedaModel() { }

        public MonedaModel(string id, string descripcion) : base(id, descripcion) {
        }
    }
}
