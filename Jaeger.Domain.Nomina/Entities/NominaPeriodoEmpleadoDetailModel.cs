﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase 
    /// </summary>
    public class NominaPeriodoEmpleadoDetailModel : NominaPeriodoEmpleadoModel, INominaPeriodoEmpleadoModel, INominaPeriodoEmpleadoDetailModel {
        #region declaraciones
        private string _rfc;
        private string _curp;
        private string _nombre;
        private string _primerApellido;
        private string _segundoApellido;
        private BindingList<IPeriodoConceptoDetailModel> conceptos;
        private bool _IsChange = false;
        #endregion

        public NominaPeriodoEmpleadoDetailModel() {
            this.Puntualidad = true;
            this._IsChange = false;
        }

        #region datos generales del empleado
        /// <summary>
        /// obtener o establecer clave de registro en sistema
        /// </summary>
        [DataNames("CTEMP_CLV")]
        public new string Clave {
            get { return base.Clave; }
            set {
                base.Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        [DataNames("CTEMP_RFC")]
        public string RFC {
            get { return this._rfc; }
            set {
                this._rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        [DataNames("CTEMP_CURP")]
        public string CURP {
            get { return this._curp; }
            set {
                this._curp = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del empleado
        /// </summary>
        [DataNames("CTEMP_NOM")]
        public string Nombre {
            get { return this._nombre; }
            set {
                this._nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        [DataNames("CTEMP_PAPE")]
        public string ApellidoPaterno {
            get { return this._primerApellido; }
            set {
                this._primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        [DataNames("CTEMP_SAPE")]
        public string ApellidoMaterno {
            get { return this._segundoApellido; }
            set {
                this._segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<IPeriodoConceptoDetailModel> Conceptos {
            get { return this.conceptos; }
            set {
                this.conceptos = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<IRegistroHoraExtraModel> HorasExtra { get; set; }

        //public new decimal TotalHorasExtra {
        //    get {
        //        if (this.HorasExtra != null) {
        //            return this.HorasExtra.Where(it => it.Activo == true).Sum(it => it.Cantidad);
        //        }
        //        return 0;
        //    }
        //}

        public BindingList<IRegistroAusenciasModel> Ausencias { get; set; }

        public string Empleado {
            get { return string.Format("{0} {1} {2}", this.ApellidoPaterno, this.ApellidoMaterno, this.Nombre); }
        }
        #endregion

        public decimal TotalPercepciones {
            get { return PercepcionGravado + this.PercepcionExento; }
        }

        public decimal SueldoNeto {
            get { return this.TotalPercepciones - this.Deducciones; }
        }

        public decimal Sueldo {
            get { return this.SueldoNeto - (this.Conceptos.Where(it => it.PagoEspecie).Sum(it=> it.Total)); }
        }

        public bool IsChange {
            get { return this._IsChange; }
            set {
                this._IsChange = value;
            }
        }
    }
}