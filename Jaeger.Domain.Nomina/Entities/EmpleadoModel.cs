﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// obejto empleado
    /// </summary>
    [SugarTable("_ctlemp", "nomina: catalogo de empleados")]
    public class EmpleadoModel : BasePropertyChangeImplementation, IEmpleadoModel {
        #region declaraciones
        private int index;
        private bool activo;
        private int idDirectorio;
        private string tipoRegimen;
        private string riesgoPuesto;
        private string periodicidadPago;
        private int metodoPago;
        private string tipoContrato;
        private string tipoJornada;
        private string numSeguridadSocial;
        private string umf;
        private string rfc;
        private string clave;
        private string claveBanco;
        private int idPuesto;
        private string numFonacot;
        private string cuentaBancaria;
        private string cLABE;
        private string cuentaTarjetaValesDespensa;
        private string sucursal;
        private CuentaBancariaTipoEnum tipoCuenta;
        private int num;
        private string cURP;
        private string registroPatronal;
        private string afore;
        private string telefono;
        private string telefono2;
        private string departamento;
        private string puesto;
        private int entidadFederativaNacimiento;
        private string ciudadNacimiento;
        private string nacionalidad;
        private string nombre;
        private string primerApellido;
        private string segundoApellido;
        private string correo;
        private string sitio;
        private DateTime? fechaNacimiento;
        private int edad;
        private DateTime? fechaInicioRelLaboral;
        private BaseCotizacionEnum baseCotizacion;
        private decimal salarioBase;
        private decimal salarioDiario;
        private decimal salarioDiarioIntegrado;
        private int genero;
        private int estadoCivil;
        private int jornadasTrabajo;
        private decimal horasJornadaTrabajo;
        private byte[] avatar;
        private DateTime fechaNuevo;
        private string creo;
        private string modifica;
        private DateTime? fechaModifica;
        private int idDepartamento;
        private string _CodigoPostal;
        private string _ClaveRegFis;
        private bool _statusSAT;
        #endregion

        public EmpleadoModel() {
            this.Activo = true;
            this.FechaInicioRelLaboral = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice de la tabla de empleados
        /// </summary>
        [DataNames("CTEMP_ID", "_ctlemp_id")]
        [SugarColumn(ColumnName = "_ctlemp_id", IsPrimaryKey = true, IsIdentity = true)]
        public int IdEmpleado {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_A", "_ctlemp_a")]
        [SugarColumn(ColumnName = "_ctlemp_a", ColumnDescription = "registro activo")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_DRCTR_ID", "_ctlemp_drctr_id")]
        [SugarColumn(ColumnName = "_ctlemp_drctr_id", IsNullable = true, ColumnDescription = "indice con el directorio principal")]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        [DataNames("CTEMP_CLV", "_ctlemp_clv")]
        [SugarColumn(ColumnName = "_ctlemp_clv", Length = 10, ColumnDescription = "clave de control de empleado")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer Registro Federal de Contribuyentes
        /// </summary
        [DataNames("CTEMP_RFC", "_ctlemp_rfc")]
        [SugarColumn(ColumnName = "_ctlemp_rfc", Length = 14, IsNullable = true, ColumnDescription = "registro federal de causantes")]
        public string RFC {
            get {
                return this.rfc;
            }
            set {
                this.rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Población
        /// </summary>
        [DataNames("CTEMP_CURP", "_ctlemp_curp")]
        [SugarColumn(ColumnName = "_ctlemp_curp", Length = 20, IsNullable = true, ColumnDescription = "clave unica de registro")]
        public string CURP {
            get {
                return this.cURP;
            }
            set {
                this.cURP = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_NOM", "_ctlemp_nom", "_cfdi_nomr")]
        [SugarColumn(ColumnName = "_ctlemp_nom", Length = 80, ColumnDescription = "nombre(s) del empleado")]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_PAPE", "_ctlemp_pape")]
        [SugarColumn(ColumnName = "_ctlemp_pape", Length = 80, ColumnDescription = "primer apellido")]
        public string ApellidoPaterno {
            get {
                return this.primerApellido;
            }
            set {
                this.primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SAPE", "_ctlemp_sape")]
        [SugarColumn(ColumnName = "_ctlemp_sape", Length = 80, ColumnDescription = "segundo apellido")]
        public string ApellidoMaterno {
            get {
                return this.segundoApellido;
            }
            set {
                this.segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// número de seguridad social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales. [0-9]{1,15} NumSeguridadSocial
        /// </summary>
        [DataNames("CTEMP_NSS", "_ctlemp_nss")]
        [SugarColumn(ColumnName = "_ctlemp_nss", Length = 16, IsNullable = true, ColumnDescription = "NumSeguridadSocial: opcional para la expresion del numero de seguridad social aplicable al trabajador")]
        public string NSS {
            get {
                return this.numSeguridadSocial;
            }
            set {
                this.numSeguridadSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la unidad medica familiar
        /// </summary>
        [DataNames("CTEMP_UMF", "_ctlemp_umf")]
        [SugarColumn(ColumnName = "_ctlemp_umf", Length = 16, IsNullable = true, ColumnDescription = "unidad medica familiar")]
        public string UMF {
            get {
                return this.umf;
            }
            set {
                this.umf = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_FONAC", "_ctlemp_fonac")]
        [SugarColumn(ColumnName = "_ctlemp_fonac", Length = 16, IsNullable = true, ColumnDescription = "cuenta fonacot")]
        public string FONACOT {
            get {
                return this.numFonacot;
            }
            set {
                this.numFonacot = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_AFORE", "_ctlemp_afore")]
        [SugarColumn(ColumnName = "_ctlemp_afore", Length = 20, IsNullable = true, ColumnDescription = "AFORE")]
        public string AFORE {
            get {
                return this.afore;
            }
            set {
                this.afore = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_ENTFED", "_ctlemp_entfed")]
        [SugarColumn(ColumnName = "_ctlemp_entfed", IsNullable = true, ColumnDescription = "clave de entidad federativa de nacimiento")]
        public int IdEntidadFed {
            get {
                return this.entidadFederativaNacimiento;
            }
            set {
                this.entidadFederativaNacimiento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_LGNAC", "_ctlemp_lgnac")]
        [SugarColumn(ColumnName = "_ctlemp_lgnac", Length = 32, IsNullable = true, ColumnDescription = "lugar de nacimiento")]
        public string LugarNacimiento {
            get {
                return this.ciudadNacimiento;
            }
            set {
                this.ciudadNacimiento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_NAC", "_ctlemp_nac")]
        [SugarColumn(ColumnName = "_ctlemp_nac", Length = 32, IsNullable = true, ColumnDescription = "nacionalidad")]
        public string Nacionalidad {
            get {
                return this.nacionalidad;
            }
            set {
                this.nacionalidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer correo electronico
        /// </summary>
        [DataNames("CTEMP_MAIL", "_ctlemp_mail")]
        [SugarColumn(ColumnName = "_ctlemp_mail", Length = 80, IsNullable = true, ColumnDescription = "correo electronico")]
        public string Correo {
            get {
                return this.correo;
            }
            set {
                this.correo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SITIO", "_ctlemp_sitio")]
        [SugarColumn(ColumnName = "_ctlemp_sitio", Length = 80, IsNullable = true, ColumnDescription = "sitio web personal")]
        public string Sitio {
            get {
                return this.sitio;
            }
            set {
                this.sitio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_FNAC", "_ctlemp_fnac")]
        [SugarColumn(ColumnName = "_ctlemp_fnac", IsNullable = true, ColumnDescription = "fecha de nacimiento")]
        public DateTime? FecNacimiento {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaNacimiento >= firstGoodDate)
                    return this.fechaNacimiento;
                return null;
            }
            set {
                this.fechaNacimiento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_EDAD", "_ctlemp_edad")]
        [SugarColumn(ColumnName = "_ctlemp_edad", IsNullable = true, ColumnDescription = "edad")]
        public int Edad {
            get {
                return this.edad;
            }
            set {
                this.edad = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SEXO", "_ctlemp_sexo")]
        [SugarColumn(ColumnName = "_ctlemp_sexo", IsNullable = true, ColumnDescription = "genero")]
        public int IdGenero {
            get {
                return this.genero;
            }
            set {
                this.genero = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_ECVL", "_ctlemp_ecvl")]
        [SugarColumn(ColumnName = "_ctlemp_ecvl", IsNullable = true, ColumnDescription = "estado civil")]
        public int IdEdoCivil {
            get {
                return this.estadoCivil;
            }
            set {
                this.estadoCivil = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [DataNames("CTEMP_TLFN", "_ctlemp_tlfn")]
        [SugarColumn(ColumnName = "_ctlemp_tlfn", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono")]
        public string Telefono1 {
            get {
                return this.telefono;
            }
            set {
                this.telefono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer telefono
        /// </summary>
        [DataNames("CTEMP_TLFN2", "_ctlemp_tlfn2")]
        [SugarColumn(ColumnName = "_ctlemp_tlfn2", Length = 20, IsNullable = true, ColumnDescription = "numero de telefono 2, opcional")]
        public string Telefono2 {
            get {
                return this.telefono2;
            }
            set {
                this.telefono2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("CTEMP_DOMFIS")]
        [SugarColumn(ColumnName = "CTEMP_DOMFIS", Length = 5, IsNullable = true, ColumnDescription = "", IsIgnore = true)]
        public string CodigoPostal {
            get { return this._CodigoPostal; }
            set {
                this._CodigoPostal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del regimen fiscal
        /// </summary>
        [DataNames("CTEMP_RGFSC")]
        [SugarColumn(ColumnName = "CTEMP_RGFSC", Length = 3, IsNullable = true, ColumnDescription = "", IsIgnore = true)]
        public string ClaveRegFis {
            get { return this._ClaveRegFis; }
            set {
                this._ClaveRegFis = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro del empleado ya fue validado (RFC, Nombre y Domicilio Fiscal)
        /// </summary>
        [DataNames("CTEMP_VSAT")]
        [SugarColumn(ColumnName = "CTEMP_VSAT", IsNullable = true, ColumnDescription = "", DefaultValue = "0", IsIgnore = true)]
        public bool StatusSAT {
            get { return this._statusSAT; }
            set {
                this._statusSAT = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_fn")]
        [SugarColumn(ColumnName = "_ctlemp_fn", ColumnDescription = "fecha de registro")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_usr_n")]
        [SugarColumn(ColumnName = "_ctlemp_usr_n", IsNullable = true, ColumnDescription = "usuario creo registro", Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_fm")]
        [SugarColumn(ColumnName = "_ctlemp_fm", IsNullable = true, ColumnDescription = "ultima fecha de modificaciones", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_usr_m")]
        [SugarColumn(ColumnName = "_ctlemp_usr_m", IsNullable = true, ColumnDescription = "ultima usuario que modifico", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        #region VERSION ANTERIOR
        /// <summary>
        /// Expresar la fecha de inicio de la relación laboral entre el empleador y el empleado. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales
        /// </summary>
        [DataNames("_ctlemp_fchrel")]
        [SugarColumn(ColumnName = "_ctlemp_fchrel", ColumnDescription = "fecha de inicio de relacion laboral")]
        public DateTime? FechaInicioRelLaboral {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioRelLaboral > firstGoodDate)
                    return this.fechaInicioRelLaboral;
                return null;
            }
            set {
                this.fechaInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_deptoid")]
        [SugarColumn(ColumnName = "_ctlemp_deptoid", IsNullable = true, ColumnDescription = "indice del departamento")]
        public int IdDepto {
            get {
                return this.idDepartamento;
            }
            set {
                this.idDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_pstid")]
        [SugarColumn(ColumnName = "_ctlemp_pstid", IsNullable = true, ColumnDescription = "indice del puesto")]
        public int IdPuesto {
            get {
                return this.idPuesto;
            }
            set {
                this.idPuesto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_tpreg")]
        [SugarColumn(ColumnName = "_ctlemp_tpreg", Length = 2, IsNullable = true, ColumnDescription = "clave de tipo de regimen")]
        public string TipoRegimen {
            get {
                return this.tipoRegimen;
            }
            set {
                this.tipoRegimen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo con las actividades que desempeñan sus trabajadores, 
        /// según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación y Fiscalización, o conforme con la normatividad del 
        /// Instituto de Seguridad Social del trabajador.  Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [DataNames("_ctlemp_rsgpst")]
        [SugarColumn(ColumnName = "_ctlemp_rsgpst", Length = 3, IsNullable = true, ColumnDescription = "clave riesgo de puesto")]
        public string RiesgoPuesto {
            get {
                return this.riesgoPuesto;
            }
            set {
                this.riesgoPuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// requerido para la forma en que se establece el pago del salario.
        /// </summary>
        [DataNames("_ctlemp_prddpg")]
        [SugarColumn(ColumnName = "_ctlemp_prddpg", Length = 3, IsNullable = true, ColumnDescription = "clave periodicidad de pago")]
        public string PeriodicidadPago {
            get {
                return this.periodicidadPago;
            }
            set {
                this.periodicidadPago = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_mtdpg")]
        [SugarColumn(ColumnName = "_ctlemp_mtdpg", Length = 3, IsNullable = true, ColumnDescription = "clave metodo de pago")]
        public int MetodoPagoInt {
            get {
                return this.metodoPago;
            }
            set {
                this.metodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el tipo de contrato que tiene el trabajador
        /// </summary>
        [DataNames("_ctlemp_tpcntr")]
        [SugarColumn(ColumnName = "_ctlemp_tpcntr", Length = 3, IsNullable = true, ColumnDescription = "clave tipo de contrato")]
        public string TipoContrato {
            get {
                return this.tipoContrato;
            }
            set {
                this.tipoContrato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el tipo de jornada que cubre el trabajador. Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales
        /// </summary>
        [DataNames("_ctlemp_tpjrn")]
        [SugarColumn(ColumnName = "_ctlemp_tpjrn", Length = 3, IsNullable = true, ColumnDescription = "clave de tipo de jornada")]
        public string TipoJornada {
            get {
                return this.tipoJornada;
            }
            set {
                this.tipoJornada = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_clvbnc")]
        [SugarColumn(ColumnName = "_ctlemp_clvbnc", Length = 3, IsNullable = true, ColumnDescription = "clave banco")]
        public string ClaveBanco {
            get {
                return this.claveBanco;
            }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer el numero de la cuenta de banco o numero de tarjeta
        /// </summary>
        [DataNames("_ctlemp_ctaban")]
        [SugarColumn(ColumnName = "_ctlemp_ctaban", Length = 16, IsNullable = true, ColumnDescription = "numero de cuenta de banco")]
        public string CuentaBancaria {
            get {
                return this.cuentaBancaria;
            }
            set {
                this.cuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer numero de la cuenta interbancaria
        /// </summary>
        [DataNames("_ctlemp_clabe")]
        [SugarColumn(ColumnName = "_ctlemp_clabe", Length = 18, IsNullable = true, ColumnDescription = "numero de cuenta interbancaria")]
        public string CLABE {
            get {
                return this.cLABE;
            }
            set {
                this.cLABE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer cuenta o numero de tarjeta de vales de despensa
        /// </summary>
        [DataNames("_ctlemp_ctaval")]
        [SugarColumn(ColumnName = "_ctlemp_ctaval", Length = 18, IsNullable = true, ColumnDescription = "numero de cuenta de tarjeta de vales de despensa")]
        public string CuentaTarjetaValesDespensa {
            get {
                return this.cuentaTarjetaValesDespensa;
            }
            set {
                this.cuentaTarjetaValesDespensa = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de la sucursal bancaria
        /// </summary>
        [DataNames("_ctlemp_scrsl")]
        [SugarColumn(ColumnName = "_ctlemp_scrsl", Length = 16, IsNullable = true, ColumnDescription = "numero de sucursal bancaria")]
        public string Sucursal {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_tpcta")]
        [SugarColumn(ColumnName = "_ctlemp_tpcta", Length = 16, IsNullable = true, ColumnDescription = "tipo de cuenta bancaria")]
        public CuentaBancariaTipoEnum TipoCuenta {
            get {
                return this.tipoCuenta;
            }
            set {
                this.tipoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_numem")]
        [SugarColumn(ColumnName = "_ctlemp_numem", Length = 20, ColumnDescription = "numero de empleado")]
        public int Num {
            get {
                return this.num;
            }
            set {
                this.num = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_rgp")]
        [SugarColumn(ColumnName = "_ctlemp_rgp", Length = 20, IsNullable = true, ColumnDescription = "registro patronal")]
        public string RegistroPatronal {
            get {
                return this.registroPatronal;
            }
            set {
                this.registroPatronal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para la expresión del departamento o área a la que pertenece el trabajador. [^|]{1,100}
        /// </summary>
        [DataNames("_ctlemp_depto")]
        [SugarColumn(ColumnName = "_ctlemp_depto", Length = 32, IsNullable = true, ColumnDescription = "departamento")]
        public string Departamento {
            get {
                return this.departamento;
            }
            set {
                this.departamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para la expresión del puesto asignado al empleado o actividad que realiza. [^|]{1,100}
        /// </summary>
        [DataNames("_ctlemp_puesto")]
        [SugarColumn(ColumnName = "_ctlemp_puesto", Length = 32, IsNullable = true, ColumnDescription = "puesto")]
        public string Puesto {
            get {
                return this.puesto;
            }
            set {
                this.puesto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_bscot")]
        [SugarColumn(ColumnName = "_ctlemp_bscot", IsNullable = true, ColumnDescription = "base de cotizacion")]
        public BaseCotizacionEnum BaseCotizacion {
            get {
                return this.baseCotizacion;
            }
            set {
                this.baseCotizacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_sb")]
        [SugarColumn(ColumnName = "_ctlemp_sb", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public decimal SalarioBase {
            get {
                return this.salarioBase;
            }
            set {
                this.salarioBase = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_drintg")]
        [SugarColumn(ColumnName = "_ctlemp_drintg", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario diario integrado")]
        public decimal SalarioDiario {
            get {
                return this.salarioDiario;
            }
            set {
                this.salarioDiario = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_sd")]
        [SugarColumn(ColumnName = "_ctlemp_sd", DecimalDigits = 4, IsNullable = true, ColumnDescription = "salario base")]
        public decimal SalarioDiarioIntegrado {
            get {
                return this.salarioDiarioIntegrado;
            }
            set {
                this.salarioDiarioIntegrado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_jrntr")]
        [SugarColumn(ColumnName = "_ctlemp_jrntr", IsNullable = true, ColumnDescription = "numero de jornadas de trabajo")]
        public int JornadasTrabajo {
            get {
                return this.jornadasTrabajo;
            }
            set {
                this.jornadasTrabajo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_jrnhrs")]
        [SugarColumn(ColumnName = "_ctlemp_jrnhrs", IsNullable = true, ColumnDescription = "numero de horas de la jornadas de trabajo")]
        public decimal HorasJornadaTrabajo {
            get {
                return this.horasJornadaTrabajo;
            }
            set {
                this.horasJornadaTrabajo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_ctlemp_img")]
        [SugarColumn(ColumnName = "_ctlemp_img", IsNullable = true)]
        public byte[] Avatar {
            get {
                return this.avatar;
            }
            set {
                this.avatar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener la concatenacion del nombre del empleado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string NombreCompleto {
            get {
                return string.Format("{0} {1} {2}", this.ApellidoPaterno, this.ApellidoMaterno, this.Nombre).TrimEnd();
            }
        }
        #endregion
    }
}
