﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class Subsidio : Base.Abstractions.BasePropertyChangeImplementation, ISubsidio {
        private decimal ingresosDesdeField;
        private decimal ingresosHastaField;
        private decimal cantidadField;

        public Subsidio() {

        }

        public Subsidio(decimal ingresosDesdeField, decimal ingresosHastaField, decimal cantidadField) {
            this.ingresosDesdeField = ingresosDesdeField;
            this.ingresosHastaField = ingresosHastaField;
            this.cantidadField = cantidadField;
        }

        [DataNames("SBSD_ID")]
        public int Id { get; set; }

        [DataNames("SBSD_ISR_ID")]
        public int SubId { get; set; }

        [DataNames("SBSD_PARA")]
        public decimal IngresosDesde {
            get {
                return this.ingresosDesdeField;
            }
            set {
                this.ingresosDesdeField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("SBSD_HASTA")]
        public decimal IngresosHasta {
            get {
                return this.ingresosHastaField;
            }
            set {
                this.ingresosHastaField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("SBSD_CNTDD")]
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
