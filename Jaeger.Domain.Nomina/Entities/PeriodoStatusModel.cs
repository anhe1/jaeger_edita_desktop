﻿namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase modelo de status de nomina
    /// </summary>
    public class PeriodoStatusModel : Base.Abstractions.BaseSingleModel {

        public PeriodoStatusModel() : base() {
        }

        public PeriodoStatusModel(int id, string descripcion) {
            this.Id = id;
            this.Descripcion = descripcion;
        }
    }
}
