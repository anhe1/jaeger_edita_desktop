﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// modelo para horas extra (NMHRE)
    /// </summary>
    public class RegistroHoraExtraModel : Base.Abstractions.BasePropertyChangeImplementation, IRegistroHoraExtraModel {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _IdEmpleado;
        private int _Cantidad;
        private int _IdTipoHora;
        private DateTime _Fecha;
        private string _Nota;
        private string _creo;
        private DateTime _fechaNuevo;
        private string _modifica;
        private DateTime? _fechaNodifica;
        private int _IdNomina;
        private int _IdConcepto;
        #endregion

        public RegistroHoraExtraModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        [DataNames("NMHRE_ID")]
        public int Id {
            get { return this._Index; }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("NMHRE_A")]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del concepto de nomina
        /// </summary>
        [DataNames("NMHRE_NMCNP_ID")]
        public int IdConcepto {
            get { return this._IdConcepto; }
            set {
                this._IdConcepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion del empleado
        /// </summary>
        [DataNames("NMHRE_EMP_ID")]
        public int IdEmpleado {
            get { return this._IdEmpleado; }
            set {
                this._IdEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de nomina
        /// </summary>
        [DataNames("NMHRE_NOM_ID")]
        public int IdNomina {
            get { return this._IdNomina; }
            set {
                this._IdNomina = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de horas extra
        /// </summary>
        [DataNames("NMHRE_TP")]
        public int IdTipoHora {
            get { return this._IdTipoHora; }
            set {
                this._IdTipoHora = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de hora extra
        /// </summary>
        [DataNames("NMHRE_FEC")]
        public DateTime Fecha {
            get { return this._Fecha; }
            set {
                this._Fecha = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cantidad
        /// </summary>
        [DataNames("NMHRE_CANT")]
        public int Cantidad {
            get { return this._Cantidad; }
            set {
                this._Cantidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        [DataNames("NMHRE_NOTA")]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("NMHRE_USR_N")]
        public string Creo {
            get { return this._creo; }
            set {
                this._creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("NMHRE_FN")]
        public DateTime FechaNuevo {
            get { return this._fechaNuevo; }
            set {
                this._fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("NMHRE_USR_M")]
        public string Modifica {
            get { return this._modifica; }
            set {
                this._modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        [DataNames("NMHRE_FM")]
        public DateTime? FechaModifica {
            get {
                if (this._fechaNodifica >= new DateTime(1900, 1, 1))
                    return this._fechaNodifica;
                return null;
            }
            set {
                this._fechaNodifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
