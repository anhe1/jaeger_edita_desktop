﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase para la vista de la consulta de nominas
    /// </summary>
    public class ComprobanteNominaSingleModel {
        private DateTime? fechaEstadoSAT;

        public ComprobanteNominaSingleModel() { }

        /// <sumary>
        /// obtener o establecer _cfdnmn_id
        /// </sumary>
        [DataNames("_cfdnmn_id")]
        public int IdNomina { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_a
        /// </sumary>
        [DataNames("_cfdnmn_a")]
        public int Activo { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_drctr_id
        /// </sumary>
        [DataNames("_cfdnmn_drctr_id")]
        public int IdDirectorio { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_cfdi_id
        /// </sumary>
        [DataNames("_cfdnmn_cfdi_id")]
        public int IdComprobante { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_tpreg
        /// </sumary>
        [DataNames("_cfdnmn_tpreg")]
        public string ClaveTipoRegimen { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_rsgpst
        /// </sumary>
        [DataNames("_cfdnmn_rsgpst")]
        public string ClaveRiesgoPuesto { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_qnc
        /// </sumary>
        [DataNames("_cfdnmn_qnc")]
        public int Quincena { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_anio
        /// </sumary>
        [DataNames("_cfdnmn_anio")]
        public int Anio { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_antgdd
        /// </sumary>
        [DataNames("_cfdnmn_antgdd")]
        public string Antiguedad { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_dspgds
        /// </sumary>
        [DataNames("_cfdnmn_dspgds")]
        public decimal NumDiasPagados { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_bsctap
        /// </sumary>
        [DataNames("_cfdnmn_bsctap")]
        public decimal SalarioBaseCotApor { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_drintg
        /// </sumary>
        [DataNames("_cfdnmn_drintg")]
        public decimal SalarioDiarioIntegrado { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_pttlgrv
        /// </sumary>
        [DataNames("_cfdnmn_pttlgrv")]
        public decimal PercepcionTotalGravado { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_pttlexn
        /// </sumary>
        [DataNames("_cfdnmn_pttlexn")]
        public decimal PercepcionTotalExento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_dttlgrv
        /// </sumary>
        [DataNames("_cfdnmn_dttlgrv")]
        public decimal DeduccionTotalGravado { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_dttlexn
        /// </sumary>
        [DataNames("_cfdnmn_dttlexn")]
        public decimal DeduccionTotalExento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_dscnt 
        /// </sumary>
        [DataNames("_cfdnmn_dscnt")]
        public decimal DescuentoIncapacidad { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_hrsxtr
        /// </sumary>
        [DataNames("_cfdnmn_hrsxtr")]
        public decimal HorasExtra { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_ver
        /// </sumary>
        [DataNames("_cfdnmn_ver")]
        public string Version { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_banco
        /// </sumary>
        [DataNames("_cfdnmn_banco")]
        public string Banco { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_rgp
        /// </sumary>
        [DataNames("_cfdnmn_rgp")]
        public string RegistroPatronal { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_numem
        /// </sumary>
        [DataNames("_cfdnmn_numem")]
        public string NoEmpleado { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_curp
        /// </sumary>
        [DataNames("_cfdnmn_curp")]
        public string CURP { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_numsgr
        /// </sumary>
        [DataNames("_cfdnmn_numsgr")]
        public string NumeroSeguridadSocial { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_clabe
        /// </sumary>
        [DataNames("_cfdnmn_clabe")]
        public string CLABE { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_tipo
        /// </sumary>
        [DataNames("_cfdnmn_tipo")]
        public string ClaveTipoNomina { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_uuid
        /// </sumary>
        [DataNames("_cfdnmn_uuid")]
        public string Id2Documento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_uuid
        /// </sumary>
        [DataNames("_cfdi_uuid")]
        public string IdDocumento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_depto
        /// </sumary>
        [DataNames("_cfdnmn_depto")]
        public string Departamento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_puesto
        /// </sumary>
        [DataNames("_cfdnmn_puesto")]
        public string Puesto { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_tpcntr
        /// </sumary>
        [DataNames("_cfdnmn_tpcntr")]
        public string ClaveTipoContrato { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_tpjrn
        /// </sumary>
        [DataNames("_cfdnmn_tpjrn")]
        public string ClaveTipoJornada { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_prddpg
        /// </sumary>
        [DataNames("_cfdnmn_prddpg")]
        public string ClavePeriricidadPago { get; set; }

        private DateTime? fechaPago;
        /// <sumary>
        /// obtener o establecer _cfdnmn_fchpgo
        /// </sumary>
        [DataNames("_cfdnmn_fchpgo")]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPago > firstGoodDate)
                    return this.fechaPago;
                return null;
            }
            set {
                this.fechaPago = value;
            }
        }

        private DateTime? fechaInicialPago;
        /// <sumary>
        /// obtener o establecer _cfdnmn_fchini
        /// </sumary>
        [DataNames("_cfdnmn_fchini")]
        public DateTime? FechaInicialPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicialPago > firstGoodDate)
                    return this.fechaInicialPago;
                return null;
            }
            set {
                this.fechaInicialPago = value;
            }
        }

        private DateTime? fechaFinalPago;
        /// <sumary>
        /// obtener o establecer _cfdnmn_fchfnl
        /// </sumary>
        [DataNames("_cfdnmn_fchfnl")]
        public DateTime? FechaFinalPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinalPago > firstGoodDate)
                    return this.fechaFinalPago;
                return null;
            }
            set {
                this.fechaFinalPago = value;
            }
        }

        private DateTime? fechaInicioRelLaboral;
        /// <sumary>
        /// obtener o establecer _cfdnmn_fchrel
        /// </sumary>
        [DataNames("_cfdnmn_fchrel")]
        public DateTime? FechaInicioRelLaboral {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioRelLaboral > firstGoodDate)
                    return this.fechaInicioRelLaboral;
                return null;
            }
            set {
                this.fechaInicioRelLaboral = value;
            }
        }

        /// <sumary>
        /// obtener o establecer _cfdnmn_fn
        /// </sumary>
        [DataNames("_cfdnmn_fn")]
        public DateTime FechaNuevo { get; set; }

        private DateTime? fechaModificacion;
        /// <sumary>
        /// obtener o establecer _cfdnmn_fm
        /// </sumary>
        [DataNames("_cfdnmn_fm")]
        public DateTime? FechaModificacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificacion > firstGoodDate)
                    return this.fechaModificacion;
                return null;
            }
            set {
                this.fechaModificacion = value;
            }
        }

        /// <sumary>
        /// obtener o establecer _cfdnmn_usr_n
        /// </sumary>
        [DataNames("_cfdnmn_usr_n")]
        public string Creo { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdnmn_usr_m
        /// </sumary>
        [DataNames("_cfdnmn_usr_m")]
        public string Modifico { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_nomr
        /// </sumary>
        [DataNames("_cfdi_nomr")]
        public string ReceptorNombre { get; set; }

        [DataNames("_cfdi_sbttl")]
        public decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el total del descuentos en el CFDI
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        public decimal Descuento { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_total
        /// </sumary>
        [DataNames("_cfdi_total")]
        public decimal Total { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_estado
        /// </sumary>
        [DataNames("_cfdi_estado")]
        public string Estado { get; set; }


        /// <sumary>
        /// obtener o establecer _cfdi_fecedo
        /// </sumary>
        [DataNames("_cfdi_fecedo")]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstadoSAT > firstGoodDate)
                    return this.fechaEstadoSAT;
                return null;
            }
            set {
                this.fechaEstadoSAT = value;
            }
        }

        private DateTime? fechaCertificacion;
        /// <sumary>
        /// obtener o establecer _cfdi_feccert
        /// </sumary>
        [DataNames("_cfdi_feccert")]
        public DateTime? FechaTimbre {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCertificacion > firstGoodDate)
                    return this.fechaCertificacion;
                return null;
            }
            set {
                this.fechaCertificacion = value;
            }
        }

        /// <sumary>
        /// obtener o establecer _cfdi_rfce
        /// </sumary>
        [DataNames("_cfdi_rfce")]
        public string EmisorRFC { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_rfcr
        /// </sumary>
        [DataNames("_cfdi_rfcr")]
        public string ReceptorRFC { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_url_xml
        /// </sumary>
        [DataNames("_cfdi_url_xml")]
        public string UrlFileXML { get; set; }

        /// <sumary>
        /// obtener o establecer _cfdi_url_pdf
        /// </sumary>
        [DataNames("_cfdi_url_pdf")]
        public string UrlFilePDF { get; set; }

        [DataNames("_cfdi_folio")]
        public string Folio { get; set; }

        [DataNames("_cfdi_serie")]
        public string Serie { get; set; }
    }
}
