﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class DepartamentoModel : Empresa.Entities.DepartamentoModel, Empresa.Contracts.IDepatamentoModel, IDepartamentoModel {
        #region declaraciones
        private string _modifica;
        private DateTime? _fechaNodifica;
        #endregion

        public DepartamentoModel() {
            base.Activo = true;
            base.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice del departamento
        /// </summary>
        [DataNames("CTDPT_ID")]
        public new int IdDepartamento {
            get { return base.IdDepartamento; }
            set { base.IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTDPT_A")]
        public new bool Activo {
            get { return base.Activo; }
            set { base.Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion del departamento
        /// </summary>
        [DataNames("CTDPT_NOM", "_cfdnmn_depto")]
        public new string Descripcion {
            get { return base.Descripcion; }
            set { base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTDPT_USR_N")]
        public new string Creo {
            get { return base.Creo; }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTDPT_FN")]
        public new DateTime FechaNuevo {
            get { return base.FechaNuevo; }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
