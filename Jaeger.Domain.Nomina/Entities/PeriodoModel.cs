﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// nomina: periodo de pago de nomina de empleados (tabla: _NMPRD)
    /// </summary>
    public class PeriodoModel : Base.Abstractions.BasePropertyChangeImplementation, IPeriodoModel {
        #region declaraciones
        private int idPeriodo;
        private bool activo;
        private string descripcion;
        private DateTime fechaInicio;
        private DateTime fechaFinal;
        private DateTime? fechaPago;
        private DateTime? fechaAutoriza;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private DateTime? fechaCancela;
        private string autoriza;
        private string cancela;
        private string creo;
        private string modifica;
        private int idStatus;
        private int idTipo;
        private int idTipoPeriodo;
        #endregion

        /// <summary>
        /// constructor de la clase
        /// </summary>
        public PeriodoModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer el indice del periodo (indice de la tabla)
        /// </summary>
        [DataNames("NMPRD_ID")]
        public int IdPeriodo {
            get { return this.idPeriodo; }
            set { this.idPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("NMPRD_A")]
        public bool Activo {
            get { return this.activo; }
            set { this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el status de la nomina
        /// </summary>
        [DataNames("NMPRD_STTS_ID")]
        public int IdStatus {
            get {
                return (int)this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de nomina Normal o Extraordinaria
        /// </summary>
        [DataNames("NMPRD_TP_ID")]
        public int IdTipo {
            get {
                return (int)this.idTipo;
            }
            set {
                this.idTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de periodo, semanal, quincenal, etc
        /// </summary>
        [DataNames("NMPRD_TPPRD")]
        public int IdPeriodoTipo {
            get {
                return this.idTipoPeriodo;
            }
            set {
                this.idTipoPeriodo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la nomina
        /// </summary>
        [DataNames("NMPRD_NOM")]
        public string Descripcion {
            get { return descripcion; }
            set { descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de inicio del periodo de la nomina
        /// </summary>
        [DataNames("NMPRD_FCINI")]
        public DateTime FechaInicio {
            get { return fechaInicio; }
            set { fechaInicio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha final del periodo de nomina
        /// </summary>
        [DataNames("NMPRD_FCFIN")]
        public DateTime FechaFinal {
            get { return this.fechaFinal; }
            set {
                fechaFinal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de pago de la nomina
        /// </summary>
        [DataNames("NMPRD_FCPGO")]
        public DateTime? FechaPago {
            get {
                if (this.fechaPago >= new DateTime(1900, 1, 1))
                    return fechaPago;
                return null;
            }
            set { fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de autorizacion de la nomina
        /// </summary>
        [DataNames("_NMPRD_fecaut")]
        public DateTime? FechaAutoriza {
            get {
                if (this.fechaAutoriza >= new DateTime(1900, 1, 1))
                    return fechaAutoriza;
                return null;
            }
            set { fechaAutoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de canclacion del periodo de nomina
        /// </summary>
        [DataNames("NMPRD_FCAUT")]
        public DateTime? FechaCancela {
            get {
                if (this.fechaCancela >= new DateTime(1900, 1, 1))
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro del periodo
        /// </summary>
        [DataNames("NMPRD_FN")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set { this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de ultima modificacion del registro
        /// </summary>
        [DataNames("NMPRD_FM")]
        public DateTime? FechaModifica {
            get {
                if (this.fechaModifica >= new DateTime(1900, 1, 1))
                    return this.fechaModifica;
                return null;
            }
            set { this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clavel del usuario que autoriza el periodo de nomina
        /// </summary>
        [DataNames("NMPRD_USR_A")]
        public string Autoriza {
            get { return this.autoriza; }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea cancela el periodo
        /// </summary>
        [DataNames("NMPRD_USR_C")]
        public string Cancela {
            get { return this.cancela; }
            set { this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        [DataNames("NMPRD_USR_N")]
        public string Creo {
            get { return this.creo; }
            set { this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("NMPRD_USR_M")]
        public string Modifica {
            get { return this.modifica; }
            set { this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener dias del periodo
        /// </summary>
        public int DiasPeriodo {
            get {
                TimeSpan tspan = this.FechaFinal - this.FechaInicio;
                return tspan.Days + 1;
            }
        }
    }
}
