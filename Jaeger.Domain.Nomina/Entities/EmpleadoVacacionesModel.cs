﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class EmpleadoVacacionesModel : Base.Abstractions.BasePropertyChangeImplementation, IEmpleadoVacacionesModel {
        private int index;
        private bool activo;
        private string clave;
        private string nombre;
        private string primerApellido;
        private string segundoApellido;
        private DateTime? _fecInicioRelLaboral;

        public EmpleadoVacacionesModel() : base() { }

        /// <summary>
        /// obtener o establecer el indice de la tabla de empleados
        /// </summary>
        [DataNames("CTEMP_ID", "_ctlemp_id")]
        [SugarColumn(ColumnName = "_ctlemp_id", IsPrimaryKey = true, IsIdentity = true)]
        public int IdEmpleado {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_A", "_ctlemp_a")]
        [SugarColumn(ColumnName = "_ctlemp_a", ColumnDescription = "registro activo")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        [DataNames("CTEMP_CLV", "_ctlemp_clv")]
        [SugarColumn(ColumnName = "_ctlemp_clv", Length = 10, ColumnDescription = "clave de control de empleado")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_NOM", "_ctlemp_nom")]
        [SugarColumn(ColumnName = "_ctlemp_nom", Length = 80, ColumnDescription = "nombre(s) del empleado")]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_PAPE", "_ctlemp_pape")]
        [SugarColumn(ColumnName = "_ctlemp_pape", Length = 80, ColumnDescription = "primer apellido")]
        public string ApellidoPaterno {
            get {
                return this.primerApellido;
            }
            set {
                this.primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMP_SAPE", "_ctlemp_sape")]
        [SugarColumn(ColumnName = "_ctlemp_sape", Length = 80, ColumnDescription = "segundo apellido")]
        public string ApellidoMaterno {
            get {
                return this.segundoApellido;
            }
            set {
                this.segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        public string Empleado {
            get { return string.Format("{0} {1} {2}", this.ApellidoPaterno, this.ApellidoMaterno, this.Nombre); }
        }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        [DataNames("CTEMPC_FCHREL")]
        public DateTime? FecInicioRelLaboral {
            get { return this._fecInicioRelLaboral; }
            set {
                this._fecInicioRelLaboral = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<IRegistroAusenciasModel> Ausencias { get; set; }

        public DateTime? FecAniversario {
            get {
                if (this.FecInicioRelLaboral != null) {
                    if (this.FecInicioRelLaboral.Value.Year == DateTime.Now.Year) {
                        return new DateTime(DateTime.Now.Year + 1, this.FecInicioRelLaboral.Value.Month, this.FecInicioRelLaboral.Value.Day);
                    }
                    return new DateTime(DateTime.Now.Year, this.FecInicioRelLaboral.Value.Month, this.FecInicioRelLaboral.Value.Day);
                }
                return null;
            }
        }

        public int AniosServicio {
            get {
                return DateTime.Now.Year - this.FecInicioRelLaboral.Value.Year;
            }
        }

        public int Dias {
            get {
                switch (this.AniosServicio) {
                    case 1:
                        return 12;
                    case 2:
                        return 14;
                    case 3:
                        return 16;
                    case 4:
                        return 18;
                    case 5:
                        return 20;
                    default:
                        return 0;
                }
            }
        }

        public int Disfrutados {
            get {
                if (this.Ausencias != null) {
                    return this.Ausencias.Where(it => it.Tipo == ValueObjects.DiasTipoEnum.Vacaciones).Count();
                }
                return 0;
            }
        }

        public int Restan {
            get { return this.Dias - this.Disfrutados; }
        }
    }
}
