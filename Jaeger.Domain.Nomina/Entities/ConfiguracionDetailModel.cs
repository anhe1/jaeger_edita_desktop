﻿using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Domain.Nomina.Entities {
    public class ConfiguracionDetailModel : ConfiguracionModel, IConfiguracionDetailModel, IConfiguracionModel {
        public ConfiguracionDetailModel() {
        }
    }
}
