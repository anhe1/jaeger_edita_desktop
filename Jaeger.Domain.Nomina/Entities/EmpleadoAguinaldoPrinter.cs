﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Domain.Nomina.Entities {
    public class EmpleadoAguinaldoPrinter {
        public EmpleadoAguinaldoPrinter() {
            this.Empleados = new BindingList<IEmpleadoCalcularAguinaldo>();
            this.Resumen = false;
        }
        public DateTime Fecha { get; set; }

        public bool Resumen { get; set; }

        public BindingList<IEmpleadoCalcularAguinaldo> Empleados { get; set; }
    }
}
