﻿using System;
using SqlSugar;

namespace Jaeger.Domain.Nomina.Entities {
    [SugarTable("CTAREA")]
    public class AreaModel : Empresa.Entities.AreaModel {
        public AreaModel() {
            base.FechaNuevo = DateTime.Now;
        }
    }
}
