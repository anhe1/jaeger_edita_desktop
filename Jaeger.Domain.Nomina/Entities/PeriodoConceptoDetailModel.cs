﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class PeriodoConceptoDetailModel : PeriodoConceptoModel, IPeriodoConceptoModel, IPeriodoConceptoDetailModel {
        #region declaraciones
        private string _Clave;
        private string _ClaveSAT;
        private string _Concepto;
        private bool _PagoEspecie;
        #endregion

        public PeriodoConceptoDetailModel() : base() { }

        /// <summary>
        /// obtener o establecer clave del empleado
        /// </summary>
        [DataNames("NMCNP_CLV")]
        public string Clave {
            get { return this._Clave; }
            set {
                this._Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave sAT
        /// </summary>
        [DataNames("NMCNP_CLVSAT")]
        public string Tipo {
            get { return this._ClaveSAT; }
            set {
                this._ClaveSAT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del concepto
        /// </summary>
        [DataNames("NMCNP_NOM")]
        public string Concepto {
            get { return this._Concepto; }
            set {
                this._Concepto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMCNP_FRMPG_ID")]
        public bool PagoEspecie {
            get { return this._PagoEspecie; }
            set {
                this._PagoEspecie = value;
                this.OnPropertyChanged();
            }
        }
    }
}
