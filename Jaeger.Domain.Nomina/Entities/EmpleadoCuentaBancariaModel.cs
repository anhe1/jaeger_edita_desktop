﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// cuenta bancaria
    /// </summary>
    public class EmpleadoCuentaBancariaModel : Base.Abstractions.BasePropertyChangeImplementationDataError, IDataErrorInfo, ICuentaBancariaModel {
        #region declaraciones
        private int index;
        private int subId;
        private bool activo;
        private string banco;
        private decimal cargoMaximo;
        private string clabe;
        private string clave;
        private string insitucionBancaria;
        private string moneda;
        private string numeroDeCuenta;
        private string sucursal;
        private string rFC;
        private string beneficiario;
        private int tipoCuenta;
        private string refNumerica;
        private string refAlfanumerica;
        private bool verificado;
        private bool _Extranjero;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        #endregion

        public EmpleadoCuentaBancariaModel() {
            this.activo = true;
            this.SetModified = false;
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// indice de la cuenta
        /// </summary>
        [DataNames("CTEMPB_ID")]
        public int IdCuentaB {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("CTEMPB_CTEMP_ID")]
        public int IdEmpleado {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        [DataNames("CTEMPB_A")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del banco
        /// </summary>
        [DataNames("CTEMPB_BANCO")]
        public string Banco {
            get {
                return this.banco;
            }
            set {
                this.banco = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// cargo maximo que se puede hacer a la cuenta
        /// </summary>
        [DataNames("CTEMPB_CRGMX")]
        public decimal CargoMaximo {
            get {
                return this.cargoMaximo;
            }
            set {
                this.cargoMaximo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE
        /// </summary>
        [DataNames("CTEMPB_CLABE")]
        public string Clabe {
            get {
                return this.clabe;
            }
            set {
                this.clabe = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("CTEMPB_CLV")]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el nombre corto de la institución bancaria
        /// </summary>
        [DataNames("CTEMPB_NMCRT")]
        public string InsitucionBancaria {
            get {
                return this.insitucionBancaria;
            }
            set {
                this.insitucionBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda
        /// </summary>
        [DataNames("CTEMPB_MND")]
        public string Moneda {
            get {
                return this.moneda;
            }
            set {
                this.moneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de cuenta bancaria
        /// </summary>
        [DataNames("CTEMPB_NMCTA")]
        public string NumCuenta {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la sucursal
        /// </summary>
        [DataNames("CTEMPB_SCRSL")]
        public string Sucursal {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del beneficiario
        /// </summary>
        [DataNames("CTEMPB_BRFC")]
        public string RFC {
            get {
                return this.rFC;
            }
            set {
                this.rFC = value.ToUpper().Trim();
                this.OnPropertyChanged();
            }
        }

        [DataNames("CTEMPB_BENEF")]
        public string Beneficiario {
            get { return this.beneficiario; }
            set { this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        [DataNames("CTEMPB_TIPO")]
        public int IdTipoCuenta {
            get {
                return this.tipoCuenta;
            }
            set {
                this.tipoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia numerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("CTEMPB_REFNUM")]
        public string RefNumerica {
            get {
                return this.refNumerica;
            }
            set {
                this.refNumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la referencia alfanumerica constante utilizada por el beneficiario de la cuenta
        /// </summary>
        [DataNames("CTEMPB_REFALF")]
        public string RefAlfanumerica {
            get {
                return this.refAlfanumerica;
            }
            set {
                if (this.Verificado == false)
                    this.refAlfanumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si la cuenta ya se encuentra verificada
        /// </summary>
        [DataNames("CTEMPB_VRFCD")]
        public bool Verificado {
            get {
                return this.verificado;
            }
            set {
                this.verificado = value;
                this.OnPropertyChanged();
            }
        }
        
        public bool Extranjero {
            get { return this._Extranjero; }
            set { this._Extranjero = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del nuevo registro
        /// </summary>
        [DataNames("CTEMPB_FN")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("CTEMPB_FM")]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate) {
                    return this.fechaModifica;
                }
                return null;
            }
            set {
                this.fechaModifica = value;
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("CTEMPB_USR_N")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("CTEMPB_USR_M")]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        public bool SetModified { get; set; }

        #region metodos

        public string Error {
            get {
                if (!this.RegexValido(this.Clabe, "[0-9]{10,18}$")) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        public string this[string columnName] {
            get {
                if (columnName == "Clabe" && !this.RegexValido(this.Clabe, "[0-9]{10,18}$")) {
                    return "Formato de CLABE no válido!";
                }
                return string.Empty;
            }
        }

        #endregion
    }
}
