﻿namespace Jaeger.Domain.Nomina.Entities {
    public class NominaPeriodoTipoModel : Base.Abstractions.BaseSingleModel {
        public NominaPeriodoTipoModel() { }

        public NominaPeriodoTipoModel(int id, string descripcion) : base(id, descripcion) { }
    }
}
