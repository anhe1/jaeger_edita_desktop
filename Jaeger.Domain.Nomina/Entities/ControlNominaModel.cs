﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    [SugarTable("_nmnctrl")]
    public class ControlNominaModel : BasePropertyChangeImplementation, IControlNominaModel {
        #region declaraciones
        private string tipoNominaField;
        private string descripconField;
        private int indiceField;
        private bool activoField;
        private string archivoExcel;
        private string archivoExcelNombre;
        private int registrosCargados;
        private int enviados;
        private int totalFilas;
        private DateTime? fechaSubida;
        #endregion

        public ControlNominaModel() { }

        /// <summary>
        /// Desc:indice
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("NMNCTRL_ID")]
        [SugarColumn(ColumnName = "_nmnctrl_id", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int IdControl {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:activo
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("NMNCTRL_A")]
        [SugarColumn(ColumnName = "_nmnctrl_a", ColumnDescription = "registro activo", Length = 1)]
        public bool Activo {
            get {
                return this.activoField;
            }
            set {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de nomina
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("NMNCTRL_TP")]
        [SugarColumn(ColumnName = "_nmnctrl_tp", ColumnDescription = "tipo de nomina", Length = 2)]
        public string TipoNomina {
            get {
                return this.tipoNominaField;
            }
            set {
                this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripcion
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("NMNCTRL_TTL")]
        [SugarColumn(ColumnName = "_nmnctrl_ttl", ColumnDescription = "descripcion", Length = 65)]
        public string Descripcion {
            get {
                return this.descripconField;
            }
            set {
                this.descripconField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMNCTRL_EXCL")]
        [SugarColumn(ColumnName = "_nmnctrl_excl", ColumnDescription = "archivo excel", Length = 255)]
        public string ArchivoExcel {
            get {
                return this.archivoExcel;
            }
            set {
                this.archivoExcel = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_exclnm", ColumnDescription = "nombre del archivo de excel", Length = 255)]
        public string NombreArchivo {
            get {
                return this.archivoExcelNombre;
            }
            set {
                this.archivoExcelNombre = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_fch", ColumnDescription = "fecha de subida")]
        public DateTime? FechaSubida {
            get {
                return this.fechaSubida;
            }
            set {
                this.fechaSubida = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_fls", ColumnDescription = "total de filas", Length = 4)]
        public int TotalFilas {
            get {
                return this.totalFilas;
            }
            set {
                this.totalFilas = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_crgds", ColumnDescription = "registros cargados", Length = 4)]
        public int RegistrosCargados {
            get {
                return this.registrosCargados;
            }
            set {
                this.registrosCargados = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_nmnctrl_envds", ColumnDescription = "enviados por correo", Length = 11)]
        public int Enviados {
            get {
                return this.enviados;
            }
            set {
                this.enviados = value;
                this.OnPropertyChanged();
            }
        }
    }
}
