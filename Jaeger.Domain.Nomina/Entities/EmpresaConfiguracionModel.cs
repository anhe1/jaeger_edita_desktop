﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// configuraciones de la empresa
    /// </summary>
    public class EmpresaConfiguracionModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private string keyname;
        private string data;
        #endregion

        #region propidades
        /// <summary>
        /// obtener o establecer el indice 
        /// </summary>
        [DataNames("conf_id")]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la llave de la configuración
        /// </summary>
        [DataNames("conf_key")]
        public string Key {
            get { return this.keyname; }
            set {
                this.keyname = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el objeto json de configuracion
        /// </summary>
        [DataNames("conf_data")]
        public string Data {
            get { return this.data; }
            set {
                this.data = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
