﻿namespace Jaeger.Domain.Nomina.Entities {
    public class EstadoModel : Base.Abstractions.BaseSingleModel {
        public EstadoModel() { }

        public EstadoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
