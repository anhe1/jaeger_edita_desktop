﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Entities {
    public class NominaConceptoDetailModel : NominaConceptoModel, INominaConceptoDetailModel, INominaConceptoModel {
        public NominaConceptoDetailModel() : base() {
        }

        public NominaTipoEnum TipoNomina {
            get { return (NominaTipoEnum)this.IdTipoNomina; }
            set { this.IdTipoNomina = (int)value;
                this.OnPropertyChanged();
            }
        }

        public ConceptoAplicacionEnum Aplicacion {
            get { return (ConceptoAplicacionEnum)this.IdAplicacion; }
            set { this.IdAplicacion = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de concepto de nomina (Percepcion o deduccion)
        /// </summary>
        public NominaConceptoTipoEnum Tipo {
            get {
                return (NominaConceptoTipoEnum)base.IdTipo;
            }
            set {
                if (this.IdTipo != (int)value) {
                    this.IdTipo = (int)value;
                    this.OnPropertyChanged();
                }
            }
        }
    }
}
