﻿namespace Jaeger.Domain.Nomina.Entities {
    public class GeneroModel : Base.Abstractions.BaseSingleModel {
        public GeneroModel() {
        }

        public GeneroModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
