﻿namespace Jaeger.Domain.Nomina.Entities {
    public class AplicacionModel : Base.Abstractions.BaseSingleModel {
        public AplicacionModel() {
        }

        public AplicacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
