﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.Domain.Nomina.Entities {
    public class EmpleadoDetailModel : EmpleadoModel, IEmpleadoDetailModel, IEmpleadoModel {
        private BindingList<IContratoModel> contratos;
        private BindingList<ICuentaBancariaModel> cuentas;
        private BindingList<IEmpleadoConceptoNominaModel> conceptos;

        public EmpleadoDetailModel() {
            this.contratos = new BindingList<IContratoModel>();
            this.cuentas = new BindingList<ICuentaBancariaModel>();
            this.conceptos = new BindingList<IEmpleadoConceptoNominaModel>();
        }

        public BindingList<IContratoModel> Contratos {
            get { return this.contratos; }
            set { this.contratos = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<ICuentaBancariaModel> Cuentas {
            get { return this.cuentas; }
            set { this.cuentas = value;
                this.OnPropertyChanged();
            }
        }

        public BindingList<IEmpleadoConceptoNominaModel> Conceptos {
            get { return this.conceptos; }
            set { this.conceptos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
