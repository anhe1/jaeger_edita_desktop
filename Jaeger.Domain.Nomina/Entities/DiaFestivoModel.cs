﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// modelo de clase para dias festivos
    /// </summary>
    public class DiaFestivoModel : Contracts.IDiaFestivoModel {
        /// <summary>
        /// constructor
        /// </summary>
        public DiaFestivoModel() { }

        /// <summary>
        /// obtener o establecer fecha 
        /// </summary>
        [DataNames("FECHA")]
        public System.DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer descripcion
        /// </summary>
        [DataNames("descripcion")]
        public string Descripcion { get; set; }
    }
}
