﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    /// <summary>
    /// clase simple para lista departamentos
    /// </summary>
    public class DepartamentoSingleModel {
        [DataNames("CTDPT_ID")]
        public int IdDepartamento { get; set; } 

        [DataNames("CTDPT_NOM", "_cfdnmn_depto")]
        public string Descripcion { get; set; }
    }
}
