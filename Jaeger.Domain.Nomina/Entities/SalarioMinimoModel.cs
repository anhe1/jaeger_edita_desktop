﻿using System;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class SalarioMinimoModel : Base.Abstractions.BasePropertyChangeImplementation, ISalarioMinimoModel {
        #region declaraciones
        private int indiceField;
        private decimal salarioMinimoField;
        private int anio;
        private DateTime fechaNuevoField;
        #endregion

        public SalarioMinimoModel() {
            this.anio = DateTime.Now.Year;
            this.fechaNuevoField = DateTime.Now;
        }

        [DataNames("SM_ID")]
        public int Id {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("SM_SALARIO")]
        public decimal Valor {
            get {
                return this.salarioMinimoField;
            }
            set {
                this.salarioMinimoField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("SM_ANIO")]
        public int Anio {
            get {
                return this.anio;
            }
            set {
                this.anio = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime FecNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
