﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class PeriodoConceptoModel : Base.Abstractions.BasePropertyChangeImplementation, IPeriodoConceptoModel {
        #region declaraciones
        private int _index;
        private int _IdEmpleado;
        private int _IdConcepto;
        private int _IdConceptoTipo;
        private int _IdBaseFiscal;
        private int _IdTablaIncidencias;
        private decimal _Total;
        private decimal _TPercepcionGravado;
        private decimal _TPercepcionExento;
        private decimal _TDeducciones;
        #endregion

        public PeriodoConceptoModel() {

        }

        /// <summary>
        /// indice de la tabla[edit]
        /// </summary>
        [DataNames("NMEMP_ID")]
        public int Id { get; set; }

        /// <summary>
        /// indice del catalogo de empleados[edit]
        /// </summary>
        [DataNames("NMEMP_CTEMP_ID")]
        public int IdEmpleado {
            get { return this._IdEmpleado; }
            set { this._IdEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// indice de relacion con la tabla de concepto de nomina(percepciones o deducciones)
        /// </summary>
        [DataNames("NMEMP_NMCNP_ID")]
        public int IdConcepto {
            get { return this._IdConcepto; }
            set {
                this._IdConcepto = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMEMP_CNP_TP_ID")]
        public int IdConceptoTipo {
            get { return this._IdConceptoTipo; }
            set {
                this._IdConceptoTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer base fiscal
        /// </summary>
        [DataNames("NMEMP_NMCNP_ISR_BF")]
        public int IdBaseFiscal {
            get { return this._IdBaseFiscal; }
            set { this._IdBaseFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de incidencias
        /// </summary>
        [DataNames("NMEMP_NMCAL_ID")]
        public int IdTablaIncidencias {
            get { return this._IdTablaIncidencias; }
            set { this._IdTablaIncidencias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del concepto
        /// </summary>
        [DataNames("NMEMP_TOTAL")]
        public decimal Total {
            get { return this._Total; }
            set { this._Total = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMEMP_TPGRA")]
        public decimal TPercepcionGravado {
            get { return this._TPercepcionGravado; }
            set { this._TPercepcionGravado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMEMP_TPEXE")]
        public decimal TPercepcionExento {
            get { return this._TPercepcionExento; }
            set { this._TPercepcionExento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("NMEMP_TDEDU")]
        public decimal TDeducciones {
            get { return this._TDeducciones; }
            set { this._TDeducciones = value;
                this.OnPropertyChanged();
            }
        }
    }
}
