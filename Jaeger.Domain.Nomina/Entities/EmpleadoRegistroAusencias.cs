﻿using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Entities {
    public class EmpleadoRegistroAusencias : RegistroAusenciasModel, IEmpleadoRegistroAusencias, IRegistroAusenciasModel {
        #region
        private string _Clave;
        private string _rfc;
        private string _curp;
        private string _nombre;
        private string _primerApellido;
        private string _segundoApellido;
        #endregion

        public EmpleadoRegistroAusencias() : base() { }

        /// <summary>
        /// obtener o establecer clave de registro en sistema
        /// </summary>
        [DataNames("CTEMP_CLV")]
        public string Clave {
            get { return this._Clave; }
            set {
                this._Clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        [DataNames("CTEMP_RFC")]
        public string RFC {
            get { return this._rfc; }
            set {
                this._rfc = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        [DataNames("CTEMP_CURP")]
        public string CURP {
            get { return this._curp; }
            set {
                this._curp = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del empleado
        /// </summary>
        [DataNames("CTEMP_NOM")]
        public string Nombre {
            get { return this._nombre; }
            set {
                this._nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido paterno
        /// </summary>
        [DataNames("CTEMP_PAPE")]
        public string ApellidoPaterno {
            get { return this._primerApellido; }
            set {
                this._primerApellido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer apellido materno
        /// </summary>
        [DataNames("CTEMP_SAPE")]
        public string ApellidoMaterno {
            get { return this._segundoApellido; }
            set {
                this._segundoApellido = value;
                this.OnPropertyChanged();
            }
        }

        public string Empleado {
            get { return string.Format("{0} {1} {2}", this.ApellidoPaterno, this.ApellidoMaterno, this.Nombre); }
        }
    }
}
