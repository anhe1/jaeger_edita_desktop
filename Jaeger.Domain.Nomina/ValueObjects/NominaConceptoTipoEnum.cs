﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    /// <summary>
    /// tipo de concepto de nomina, Percepcion o Deduccion
    /// </summary>
    public enum NominaConceptoTipoEnum {
        [Description("Percepción")]
        Percepcion = 1,
        [Description("Deducción")]
        Deduccion = 2
    }
}
