﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    /// <summary>
    /// concepto de aplicacion del concepto
    /// </summary>
    public enum ConceptoAplicacionEnum {
        [Description("General")]
        General = 1,
        [Description("Individual")]
        Individual = 2
    }
}
