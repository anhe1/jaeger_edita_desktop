﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum EstadoEnum {
        [Description("Aguascalientes")]
        Aguascalientes = 1,
        [Description("Baja California")]
        Baja_California = 2,
        [Description("Baja California Sur")]
        Baja_California_Sur = 3,
        [Description("Campeche")]
        Campeche = 4,
        [Description("Coahuila")]
        Coahuila = 5,
        [Description("Colima")]
        Colima = 6,
        [Description("Chiapas")]
        Chiapas = 7,
        [Description("Chihuahua")]
        Chihuahua = 8,
        [Description("Distrito Federal")]
        Distrito_Federal = 9,
        [Description("Durango")]
        Durango = 10,
        [Description("Guanajuato")]
        Guanajuato = 11,
        [Description("Guerrero")]
        Guerrero = 12,
        [Description("Hidalgo")]
        Hidalgo = 13,
        [Description("Jalisco")]
        Jalisco = 14,
        [Description("México")]
        Mexico = 15,
        [Description("Michoacan")]
        Michoacan = 16,
        [Description("Morelos")]
        Morelos = 17,
        [Description("Nayarit")]
        Nayarit = 18,
        [Description("Nuevo Leon")]
        Nuevo_Leon = 19,
        [Description("Oaxaca")]
        Oaxaca = 20,
        [Description("Puebla")]
        Puebla = 21,
        [Description("Queretaro")]
        Queretaro = 22,
        [Description("Quintana Roo")]
        Quintana_Roo = 23,
        [Description("San Luis Potosi")]
        San_Luis_Potosi = 24,
        [Description("Sinaloa")]
        Sinaloa = 25,
        [Description("Sonora")]
        Sonora = 26,
        [Description("Tabasco")]
        Tabasco = 27,
        [Description("Tamaulipas")]
        Tamaulipas = 28,
        [Description("Tlaxcala")]
        Tlaxcala = 29,
        [Description("Veracruz")]
        Veracruz = 30,
        [Description("Yucatan")]
        Yucatan = 31,
        [Description("Zacatecas")]
        Zacatecas = 32,
        [Description("Extranjero")]
        Extranjero = 33
    }
}
