﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    /// <summary>
    /// status aplicables a un periodo de nomina
    /// </summary>
    public enum PeriodoStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Por Autorizar")]
        PorAutorizar = 1,
        [Description("Autorizada")]
        Autorizada = 2
    }
}
