﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    /// <summary>
    /// tipo de calculo aplicable al concepto de nomina
    /// </summary>
    public enum NominaCalculoTipoEnum {
            [Description("Importe Fijo")]
            MontoFijo,
            [Description("Resultado de Formula")]
            Formula,
            [Description("Tabla de sistema")]
            Tabla
        }
}
