﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum ArticuloISREnum {
        [Description("Artículo 96")]
        Articulo96 = 96,
        [Description("Artículo 174")]
        Articulo174 = 174
    }
}
