﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum DiasTipoEnum {
        /// <summary>
        /// Día de vacaciones
        /// </summary>
        [Description("Vacaciones")]
        Vacaciones = 15,
        /// <summary>
        /// Incapacidad
        /// </summary>
        [Description("Incapacidad")]
        Incapacidad = 16,
        /// <summary>
        /// falta o ausencia
        /// </summary>
        [Description("Falta")]
        Ausencia = 17
    }
}
