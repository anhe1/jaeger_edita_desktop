﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum UsarParaCalcularEnum {
        [Description("UMA")]
        UMA,
        [Description("Salario Mínimo General")]
        SalarioMinimoGeneral
    }
}
