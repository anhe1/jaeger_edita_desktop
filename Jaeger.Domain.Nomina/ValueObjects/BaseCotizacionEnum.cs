namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum BaseCotizacionEnum {
        Fijo,
        Variable,
        Mixto
    }
}