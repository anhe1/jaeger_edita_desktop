﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum NominaTipoEnum {
        [Description("Ordinaria")]
        Ordinaria = 1,
        [Description("ExtraOrdinaria")]
        Extraordinaria = 2,
    }
}
