﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum ConceptoBaseEnum {
        [Description("No definido")]
        NoDefinido = 0,
        [Description("Gravable")]
        Gravable = 1,
        [Description("Grava Parcialmente")]
        GravableParcial = 2,
        [Description("Exento")]
        Exento = 3,
        [Description("Exento Parcialmente")]
        ExentoParcial = 4
    }
}
