﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum CuentaBancariaTipoEnum {
        [Description("No definido")]
        NoDefinido = 0,
        [Description("Debito")]
        Nomina = 1,
        [Description("Vales de Despensa")]
        ValeDespensa = 2
    }
}
