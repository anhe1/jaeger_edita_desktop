﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.ValueObjects {
    public enum PeriodoTipoEnum {
        [Description("Diario")]
        Diario = 1,
        [Description("Semanal")]
        Semanal = 7,
        [Description("Decenal")]
        Decenal = 10,
        [Description("Quincenal")]
        Quincenal = 15,
        [Description("Mensual")]
        Mensual = 30,
        [Description("Anual")]
        Anual = 365
    }
}
