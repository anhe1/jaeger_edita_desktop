﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IRegistroHoraExtraModel {
        /// <summary>
        /// obtener o establecer indice
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del concepto de nomina
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion del empleado
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer indice de nomina
        /// </summary>
        int IdNomina { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de horas extra
        /// </summary>
        int IdTipoHora { get; set; }

        /// <summary>
        /// obtener o establecer fecha de hora extra
        /// </summary>
        DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer cantidad
        /// </summary>
        int Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}