﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Defines the contract for the Departamento repository.
    /// </summary>
    public interface ISqlDepartamentoRepository : Empresa.Contracts.ISqlDepartamentoRepository, IGenericRepository<DepartamentoModel> {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="depatamento"></param>
        /// <returns></returns>
        DepartamentoModel Save(DepartamentoModel depatamento);
    }
}
