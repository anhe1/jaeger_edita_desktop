﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// define la interfaz para el repositorio de contrato de empleado
    /// </summary>
    public interface ISqlEmpleadoContratoRepository : IGenericRepository<ContratoModel> {
        /// <summary>
        /// desactivar contrato
        /// </summary>
        bool Remove(int index);

        IContratoModel Save(IContratoModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
