﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISubsidio {
        int Id { get; set; }

        int SubId { get; set; }

        decimal IngresosDesde { get; set; }

        decimal IngresosHasta { get; set; }

        decimal Cantidad { get; set; }
    }
}
