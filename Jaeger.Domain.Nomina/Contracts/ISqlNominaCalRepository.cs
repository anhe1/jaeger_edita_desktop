﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// define la interfaz para el repositorio de nomina
    /// </summary>
    public interface ISqlNominaCalRepository : IGenericRepository<NominaPeriodoEmpleadoModel> {
        IEnumerable<INominaPeriodoEmpleadoDetailModel> GetList(List<IConditional> conditionals);

        INominaPeriodoEmpleadoDetailModel Save(INominaPeriodoEmpleadoDetailModel model);
    }
}
