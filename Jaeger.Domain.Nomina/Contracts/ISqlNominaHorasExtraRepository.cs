﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Define la interfaz para el repositorio de horas extra
    /// </summary>
    public interface ISqlNominaHorasExtraRepository : IGenericRepository<RegistroHoraExtraModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        IRegistroHoraExtraModel Save(IRegistroHoraExtraModel model);
    }
}
