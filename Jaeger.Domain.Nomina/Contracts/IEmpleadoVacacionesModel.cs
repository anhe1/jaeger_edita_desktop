﻿using System;
using System.ComponentModel;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoVacacionesModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla de empleados
        /// </summary>
        int IdEmpleado { get; set; }

        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer la clave interna del empleado
        /// </summary>
        string Clave { get; set; }

        string Nombre { get; set; }

        string ApellidoPaterno { get; set; }

        string ApellidoMaterno { get; set; }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        DateTime? FecInicioRelLaboral { get; set; }

        BindingList<IRegistroAusenciasModel> Ausencias { get; set; }

        DateTime? FecAniversario { get; }

        string Empleado { get; }

        int AniosServicio { get; }

        int Dias { get; }

        int Disfrutados { get; }

        int Restan { get; }
    }
}
