﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// repositorio de configuraciones del sistema
    /// </summary>
    public interface ISqlConfiguracionRepository : IGenericRepository<EmpresaConfiguracionModel> {
        /// <summary>
        /// obtener la configuracion para la empresa segun la llave
        /// </summary>
        EmpresaConfiguracionModel GetByKey(string key);

        /// <summary>
        /// almacenar configuracion
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        EmpresaConfiguracionModel Save(EmpresaConfiguracionModel item);
    }
}
