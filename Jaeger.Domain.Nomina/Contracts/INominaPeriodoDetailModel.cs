﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface INominaPeriodoDetailModel : IPeriodoModel {
        /// <summary>
        /// obtener o establecer el status de la nomina
        /// </summary>
        PeriodoStatusEnum Status { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de nomina Normal o Extraordinaria
        /// </summary>
        NominaTipoEnum Tipo { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de periodo, semanal, quincenal, etc
        /// </summary>
        PeriodoTipoEnum TipoPeriodo { get; set; }

        BindingList<INominaPeriodoEmpleadoDetailModel> Empleados { get; set; }
    }
}
