﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IPeriodoModel {
        /// <summary>
        /// obtener o establecer el indice del periodo (indice de la tabla)
        /// </summary>
        int IdPeriodo { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el status de la nomina
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de nomina Normal o Extraordinaria
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de periodo, semanal, quincenal, etc
        /// </summary>
        int IdPeriodoTipo { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion de la nomina
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de inicio del periodo de la nomina
        /// </summary>
        DateTime FechaInicio { get; set; }

        /// <summary>
        /// obtener o establecer la fecha final del periodo de nomina
        /// </summary>
        DateTime FechaFinal { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de pago de la nomina
        /// </summary>
        DateTime? FechaPago { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de autorizacion de la nomina
        /// </summary>
        DateTime? FechaAutoriza { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de canclacion del periodo de nomina
        /// </summary>
        DateTime? FechaCancela { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro del periodo
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de ultima modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clavel del usuario que autoriza el periodo de nomina
        /// </summary>
        string Autoriza { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que crea cancela el periodo
        /// </summary>
        string Cancela { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        int DiasPeriodo { get; }
    }
}
