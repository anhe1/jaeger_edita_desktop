﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoRegistroAusencias : IRegistroAusenciasModel {
        string Clave { get; set; }
        string Nombre { get; set; }
        string ApellidoPaterno { get; set; }
        string ApellidoMaterno { get; set; }
        string Empleado { get; }
    }
}
