﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Defines the contract for the EmpleadoConceptoNomina repository.
    /// </summary>
    public interface ISqlEmpleadoConceptoNominaRepository : IGenericRepository<EmpleadoConceptoNominaModel> {
        /// <summary>
        /// guarda un elemento
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        EmpleadoConceptoNominaModel Save(EmpleadoConceptoNominaModel item);

        /// <summary>
        /// obtiene una lista de elementos
        /// </summary>
        /// <typeparam name="T1">Entidad</typeparam>
        /// <param name="condiciones">condicionales</param>
        /// <returns></returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> condiciones) where T1: class, new();
    }
}
