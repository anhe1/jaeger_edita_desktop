﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISalarioMinimoModel {
        int Id { get; set; }

        decimal Valor { get; set; }

        int Anio { get; set; }
    }
}
