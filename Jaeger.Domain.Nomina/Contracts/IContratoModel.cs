﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// interface para contratos de empleados
    /// </summary>
    public interface IContratoModel {
        /// <summary>
        /// obtener o establecer indice del contrato
        /// </summary>
        int IdContrato { get; set; }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio de empleados (CTEMPC_CTEMP_ID)
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer el indice del departamento
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer el indice del puesto ocupado
        /// </summary>
        int IdPuesto { get; set; }

        /// <summary>
        /// obtener o establecer la clave de tipo de regimen
        /// </summary>
        string ClaveTipoRegimen { get; set; }

        /// <summary>
        /// obtener o establecer clave riesgo de puesto.
        /// </summary>
        string ClaveRiesgoPuesto { get; set; }

        /// <summary>
        /// obtener o establecer clave periodicidad de pago
        /// </summary>
        string ClavePeriodicidadPago { get; set; }

        /// <summary>
        /// obtener o establecer clave de metodo de pago
        /// </summary>
        int ClaveMetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer clave tipo de contrato
        /// </summary>
        string ClaveTipoContrato { get; set; }

        /// <summary>
        /// obtener o establecer clave de tipo de jornada
        /// </summary>
        string ClaveTipoJornada { get; set; }

        /// <summary>
        /// obtener o establecer registro patronal
        /// </summary>
        string RegistroPatronal { get; set; }

        string Departamento { get; set; }

        /// <summary>
        /// obtener o establecer puesto
        /// </summary>
        string Puesto { get; set; }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        DateTime? FecInicioRelLaboral { get; set; }

        /// <summary>
        /// obtener o establecer fecha de termino de relacion laboral
        /// </summary>
        DateTime? FecTerminoRelLaboral { get; set; }

        /// <summary>
        /// obtener o establecer salario base
        /// </summary>
        decimal SalarioBase { get; set; }

        decimal SalarioBaseCotizacion { get; set; }

        /// <summary>
        /// obtener o establecer salario diario
        /// </summary>
        decimal SalarioDiario { get; set; }

        int Jornadas { get; set; }

        int Horas { get; set; }

        /// <summary>
        /// obtener o establecer clave de banco
        /// </summary>
        string ClaveBanco { get; set; }

        /// <summary>
        /// obtener o establecer numero de cuenta bancaria
        /// </summary>
        string NumeroCuenta { get; set; }

        /// <summary>
        /// obtener o establecer numero de sucursal bancaria
        /// </summary>
        string Sucursal { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cuenta
        /// </summary>
        int IdTipoCuenta { get; set; }

        string Clabe { get; set; }

        /// <summary>
        /// obtener o establecer numero de cuenta de tarjeta de vales de despensa.
        /// </summary>
        string Vales { get;set; }

        string Creo { get; set; }

        DateTime FechaNuevo { get; set; }

        string Modifica { get; set; }

        DateTime? FechaModifica { get; set; }
    }
}
