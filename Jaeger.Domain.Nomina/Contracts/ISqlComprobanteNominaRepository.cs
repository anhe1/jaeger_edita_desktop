﻿using System;
using System.Collections.Generic;
using System.Data;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISqlComprobanteNominaRepository : IGenericRepository<ComplementoNominaModel> {

        /// <summary>
        /// obtener listado de comprobantes de nómina por el indice del control 
        /// </summary>
        /// <param name="index">indice de control (_cfdnmn_nmnctrl_id)</param>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(int index, bool onlyActive = true);

        /// <summary>
        /// obtener listado de comprobantes de nomina por el nombre del empleado
        /// </summary>
        /// <param name="employee">nombre del empleado</param>
        IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string employee);

        /// <summary>
        /// obtener listado de comprobantes por campo uuid
        /// </summary>
        /// <param name="idDocumento">folio fiscal (uuid)</param>
        /// <param name="onlyActive">solo registros activos</param>
        IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(string idDocumento, bool onlyActive);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateBy"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="depto"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        IEnumerable<ComprobanteNominaSingleModel> GetSearchBy(CFDIFechaEnum dateBy, DateTime dateStart, DateTime dateEnd, string depto, string employee);

        /// <summary>
        ///  obtener lista de resumen de nomina para tabla dinamica
        /// </summary>
        /// <param name="year"></param>
        /// <param name="estado"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        DataTable GetResumen(int year, int month = 0, int estado = 1);
        DataTable GetResumen(int indice);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
