﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla de empleados
        /// </summary>
        [DataNames("CTEMP_ID")]
        int IdEmpleado { get; set; }

        [DataNames("CTEMP_A")]
        bool Activo { get; set; }

        [DataNames("CTEMP_DRCTR_ID")]
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer clave de registro en sistema
        /// </summary>
        [DataNames("CTEMP_CLV")]
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer Registro Federal de Contribuyentes (RFC)
        /// </summary>
        [DataNames("CTEMP_RFC")]
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer Clave Unica de Registro de Poblacion (CURP)
        /// </summary>
        [DataNames("CTEMP_CURP")]
        string CURP { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del empleado
        /// </summary>
        [DataNames("CTEMP_NOM")]
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer el apellido aterno del empleado
        /// </summary>
        [DataNames("CTEMP_PAPE")]
        string ApellidoPaterno { get; set; }

        /// <summary>
        /// obtener o establecer el ape´llido materno del empleado
        /// </summary>
        [DataNames("CTEMP_SAPE")]
        string ApellidoMaterno { get; set; }

        /// <summary>
        /// obtener o establecer numero de seguridad social (NSS)
        /// </summary>
        [DataNames("CTEMP_NSS")]
        string NSS { get; set; }

        /// <summary>
        /// obtener o establecer la unidad medica familiar (UMF)
        /// </summary>
        [DataNames("CTEMP_UMF")]
        string UMF { get; set; }

        [DataNames("CTEMP_FONAC")]
        string FONACOT { get; set; }

        [DataNames("CTEMP_AFORE")]
        string AFORE { get; set; }

        [DataNames("CTEMP_ENTFED")]
        int IdEntidadFed { get; set; }

        [DataNames("CTEMP_LGNAC")]
        string LugarNacimiento { get; set; }

        [DataNames("CTEMP_NAC")]
        string Nacionalidad { get; set; }

        [DataNames("CTEMP_MAIL")]
        string Correo { get; set; }

        [DataNames("CTEMP_SITIO")]
        string Sitio { get; set; }

        [DataNames("CTEMP_FNAC")]
        DateTime? FecNacimiento { get; set; }

        [DataNames("CTEMP_EDAD")]
        int Edad { get; set; }

        [DataNames("CTEMP_SEXO")]
        int IdGenero { get; set; }

        [DataNames("CTEMP_ECVL")]
        int IdEdoCivil { get; set; }

        [DataNames("CTEMP_TLFN")]
        string Telefono1 { get; set; }

        [DataNames("CTEMP_TLFN2")]
        string Telefono2 { get; set; }

        [DataNames("CTEMP_USR_N")]
        string Creo { get; set; }

        [DataNames("CTEMP_FN")]
        DateTime FechaNuevo { get; set; }

        [DataNames("CTEMP_USR_N")]
        string Modifica { get; set; }

        [DataNames("CTEMP_FM")]
        DateTime? FechaModifica { get; set; }
    }
}
