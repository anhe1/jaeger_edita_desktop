﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISqlEmpleadoRepository : IGenericRepository<EmpleadoModel> {
        IEmpleadoDetailModel Save(IEmpleadoDetailModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
