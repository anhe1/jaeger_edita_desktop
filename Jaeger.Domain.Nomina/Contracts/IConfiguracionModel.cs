﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IConfiguracionModel {
        decimal CesantiaYVejezE { get; set; }
        decimal CesantiaYVejezP { get; set; }
        string Creo { get; set; }
        decimal CuotaFijaP { get; set; }
        int DiasPeriodo { get; set; }
        int DiasSemana { get; set; }
        int DiasXAnio { get; set; }
        int Ejercicio { get; set; }
        decimal ExcedenteE { get; set; }
        decimal ExcedenteP { get; set; }
        DateTime? FechaModifica { get; set; }
        DateTime FechaNuevo { get; set; }
        int HorasDia { get; set; }
        int IdConfiguracion { get; set; }
        int IdPeriodo { get; set; }
        int IdTablaISR { get; set; }
        int IdTablaSubsidio { get; set; }
        decimal Infonavit { get; set; }
        decimal InvalidezYVidaE { get; set; }
        decimal InvalidezYVidaP { get; set; }
        string Modifica { get; set; }
        decimal PensionadosYBeneficiariosE { get; set; }
        decimal PensionadosYBeneficiariosP { get; set; }
        decimal PrestacionDineroE { get; set; }
        decimal PrestacionDineroP { get; set; }
        decimal PrestacionesSociales { get; set; }
        decimal RiesgoTrabajo { get; set; }
        decimal SalarioMinimo { get; set; }
        decimal SeguroDeRetiro { get; set; }
        decimal UMA { get; set; }
    }
}