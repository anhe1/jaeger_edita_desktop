﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Define la interfaz para el repositorio de cuenta bancaria de empleado
    /// </summary>
    public interface ISqlEmpleadoCuentaBancariaRepository : IGenericRepository<EmpleadoCuentaBancariaModel> {
        EmpleadoCuentaBancariaModel Save(EmpleadoCuentaBancariaModel model);

        IEnumerable<T1> GetList<T1>(List<IConditional> condiciones) where T1 : class, new();
    }
}
