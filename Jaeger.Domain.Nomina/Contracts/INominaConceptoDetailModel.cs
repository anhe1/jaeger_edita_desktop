﻿using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface INominaConceptoDetailModel : INominaConceptoModel {
        NominaTipoEnum TipoNomina { get; set; }
        ConceptoAplicacionEnum Aplicacion { get; set; }
        NominaConceptoTipoEnum Tipo { get; set; }
    }
}
