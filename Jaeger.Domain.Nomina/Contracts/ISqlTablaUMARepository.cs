﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla UMA.
    /// </summary>
    public interface ISqlTablaUMARepository : IGenericRepository<UMAModel> {
        UMAModel GetByYear(int year);

        UMAModel Save(UMAModel model);
    }
}
