﻿using System;
using Jaeger.Domain.Nomina.ValueObjects;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IRegistroAusenciasModel {
        /// <summary>
        /// obtener o establecer indice del concepto de nomina
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion del empleado
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer indice de nomina
        /// </summary>
        int IdNomina { get; set; }

        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de hora extra
        /// </summary>
        DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer nota
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer 
        /// </summary>
        DateTime FechaNuevo { get; set; }

        DiasTipoEnum Tipo { get; set; }
    }
}
