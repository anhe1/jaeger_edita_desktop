﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla Subsidio al Empleo.
    /// </summary>
    public interface ISqlTablaSubsidioAlEmpleoRepository : IGenericRepository<TablaSubsidioAlEmpleoModel> {
        TablaSubsidioAlEmpleoModel GetById(int year, int periodo);
    }
}
