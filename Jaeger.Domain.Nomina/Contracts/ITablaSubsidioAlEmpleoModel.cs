﻿using System.Collections.Generic;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ITablaSubsidioAlEmpleoModel {
        int Id { get; set; }

        int Anio { get; set; }

        int Periodo { get; set; }

        string Descripcion { get; }

        List<ISubsidio> Rangos { get; set; }

        ISubsidio GetSubsidio(decimal baseGravable);
    }
}
