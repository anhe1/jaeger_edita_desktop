﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// interface de puestos
    /// </summary>
    public interface IPuestoModel {
        /// <summary>
        /// obtener o establecer indice del puesto
        /// </summary>
        int IdPuesto { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice del departamento
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del puesto
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer el sueldo del puesto
        /// </summary>
        decimal Sueldo { get; set; }

        /// <summary>
        /// obtener o establecer el sueldo maximo
        /// </summary>
        decimal SueldoMaximo { get; set; }

        /// <summary>
        /// obtener o establecer clave del riesgo de puesto
        /// </summary>
        int RiesgoPuesto { get; set; }

        string CtaContable { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        string Modifica { get; set; }

        DateTime? FechaModifica { get; set; }
    }
}
