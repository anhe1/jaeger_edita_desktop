﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface IPeriodoConceptoDetailModel : IPeriodoConceptoModel {
        string Clave { get; set; }
        
        string Tipo { get; set; }

        string Concepto { get; set; }

        bool PagoEspecie { get; set; }
    }
}
