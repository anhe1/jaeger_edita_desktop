﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface IUMAModel {
        int Id { get; set; }

        int Anio { get; set; }

        decimal Diario { get; set; }

        decimal Mensual { get; set; }

        decimal Anual { get; set; }
    }
}
