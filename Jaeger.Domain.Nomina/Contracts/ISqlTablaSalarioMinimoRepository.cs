﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla Salario Mínimo.
    /// </summary>
    public interface ISqlTablaSalarioMinimoRepository : IGenericRepository<SalarioMinimoModel> {
        SalarioMinimoModel GetByEjercicio(int year);

        SalarioMinimoModel Save(SalarioMinimoModel model);
    }
}
