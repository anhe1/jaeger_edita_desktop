﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface INominaPeriodoEmpleadoDetailModel : INominaPeriodoEmpleadoModel {
        BindingList<IPeriodoConceptoDetailModel> Conceptos { get; set; }
        BindingList<IRegistroHoraExtraModel> HorasExtra { get; set; }
        BindingList<IRegistroAusenciasModel> Ausencias { get; set; }
        decimal TotalPercepciones { get; }
        decimal SueldoNeto { get; }
        bool IsChange { get; set; }
    }
}
