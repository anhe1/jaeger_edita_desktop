﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoCalcularAguinaldo {
        #region propiedades
        /// <summary>
        /// obtener o establecer indice o numero de empleado
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer clave del empleado
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer nombre completo del empleado o trabajador
        /// </summary>
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer el importe del salario diario
        /// </summary>
        decimal SalarioDiario { get; set; }

        /// <summary>
        /// obtener el monto del salario mensual
        /// </summary>
        decimal SalarioMensual { get; }

        /// <summary>
        /// obtener o establecer fecha de inicio de relacion laboral
        /// </summary>
        DateTime? FecInicioRelLaboral { get; set; }

        /// <summary>
        /// obtener o establecer salario minimo general
        /// </summary>
        decimal SalarioMinimoGeneral { get; set; }

        /// <summary>
        /// obtener o establecer dias de aguinaldo
        /// </summary>
        decimal DiasAguinaldo { get; set; }

        /// <summary>
        /// obtener o establecer dias de ausencias registradas
        /// </summary>
        decimal Ausencias { get; set; }

        /// <summary>
        /// obtener o estableecr Unidad de Medida Actualizada
        /// </summary>
        decimal UMA { get; set; }

        /// <summary>
        /// obtener el monto del aguinaldo
        /// </summary>
        decimal Importe { get; }

        /// <summary>
        /// obtener o establecer la cantidad de dias laborados para el aguinaldo
        /// </summary>
        decimal DiasLaborados { get; set; }

        /// <summary>
        /// obtener o establecer base gravable
        /// </summary>
        decimal BaseGravable { get; }

        /// <summary>
        /// obtener o establecer base exento
        /// </summary>
        decimal BaseExento { get; }

        /// <summary>
        /// obtener o establecer base gravada
        /// </summary>
        decimal BaseGravada { get; }

        decimal ISR { get; }

        /// <summary>
        /// importe del subsidio
        /// </summary>
        decimal Subsidio { get; }

        decimal ISRSalarioMasAguinaldo { get; }

        decimal ISRSueldo { get; }

        decimal ISRAguinaldo { get; }

        decimal AguinaldoPagar { get; }
        #endregion
    }
}
