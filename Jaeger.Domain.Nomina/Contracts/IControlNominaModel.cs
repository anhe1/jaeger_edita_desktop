﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IControlNominaModel {
        int IdControl { get; set; }
        bool Activo { get; set; }
        string TipoNomina { get; set; }
        string Descripcion { get; set; }

        string ArchivoExcel { get; set; }

        string NombreArchivo { get; set; }

        DateTime? FechaSubida { get; set; }

        int TotalFilas { get; set; }

        int RegistrosCargados { get; set; }

        int Enviados { get; set; }
    }
}
