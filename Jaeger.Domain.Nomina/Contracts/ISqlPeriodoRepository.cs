﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Define la interfaz para el repositorio de periodos de nomina
    /// </summary>
    public interface ISqlPeriodoRepository : IGenericRepository<PeriodoModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        IEnumerable<T1> GetLast<T1>() where T1 : class, new();

        INominaPeriodoDetailModel Save(INominaPeriodoDetailModel model);

        int CrearPeriodo(INominaPeriodoDetailModel periodo);

        int Calcular(INominaPeriodoDetailModel periodo);
    }
}
