﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISqlPeriodoConceptoRepository : IGenericRepository<PeriodoConceptoModel> {
        IEnumerable<PeriodoConceptoDetailModel> GetList(List<Conditional> conditionals);
    }
}
