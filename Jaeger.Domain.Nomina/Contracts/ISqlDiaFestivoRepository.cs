﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interface que define los métodos a implementar en el repositorio de Dias Festivos
    /// </summary>
    public interface ISqlDiaFestivoRepository : IGenericRepository<DiaFestivoModel> {
        /// <summary>
        /// Obtiene una lista de dias festivos
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="conditionals"></param>
        /// <returns></returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// Guarda un dia festivo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IDiaFestivoModel Save(IDiaFestivoModel model);

        /// <summary>
        /// Guarda una lista de dias festivos
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        List<IDiaFestivoModel> Save(List<IDiaFestivoModel> models);
    }
}
