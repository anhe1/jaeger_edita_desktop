﻿namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// interface de configuración
    /// </summary>
    public interface IConfiguration : Base.Contracts.IConfiguration {
        /// <summary>
        /// obtener o establecer modo productivo
        /// </summary>
        bool ModoProductivo { get; set; }

        /// <summary>
        /// obtener o establecer carpeta
        /// </summary>
        string Folder { get; set; }

        string Percepciones { get; set; }

        string Deducciones { get; set; }
        
        string OtrosPagos { get; set; }

        /// <summary>
        /// obtener o establecer configuración de base de datos
        /// </summary>
        DataBase.Contracts.IDataBaseConfiguracion DataBase { get; set; }
    }
}
