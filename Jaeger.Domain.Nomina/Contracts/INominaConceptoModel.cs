﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface INominaConceptoModel {
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer el registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de concepto de nomina (Percepcion o deduccion)
        /// </summary>
        int IdTipo { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de nomina a aplicar
        /// </summary>
        int IdTipoNomina { get; set; }

        int IdTabla { get; set; }

        /// <summary>
        /// obtener o establecer clave de control interno 
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer el tipo SAT (clave del catalogo de conceptos de nomina SAT)
        /// </summary>
        string ClaveSAT { get; set; }

        /// <summary>
        /// obtener o establecer el concepto de nomina
        /// </summary>
        string Concepto { get; set; }

        string Formula { get; set; }

        /// <summary>
        /// obtener o establecer la aplicacion, verdadero si es aplicacion general, falso para aplicacion individual
        /// </summary>
        int IdAplicacion { get; set; }

        /// <summary>
        /// obtener o establecer si el concepto se mostrara en la representacion impresa
        /// </summary>
        bool Visible { get; set; }

        int IdBaseISR { get; set; }

        string FormulaISR { get; set; }

        int IdBaseIMS { get; set; }

        string FormulaIMS { get; set; }

        bool PagoEspecie { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifico { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifico { get; set; }

    }
}
