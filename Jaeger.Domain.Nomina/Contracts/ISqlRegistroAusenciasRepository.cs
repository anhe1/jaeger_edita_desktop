﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla Registro de Ausencias.
    /// </summary>
    public interface ISqlRegistroAusenciasRepository : IGenericRepository<RegistroAusenciasModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
        IRegistroAusenciasModel Save(IRegistroAusenciasModel model);
        List<IRegistroAusenciasModel> Save(List<IRegistroAusenciasModel> models);
    }
}
