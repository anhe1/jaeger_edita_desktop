﻿using System.ComponentModel;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoDetailModel : IEmpleadoModel {
        BindingList<IContratoModel> Contratos { get; set; }
        BindingList<ICuentaBancariaModel> Cuentas {  get; set; }
        BindingList<IEmpleadoConceptoNominaModel> Conceptos {  get; set; }
    }
}
