﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoSingleModel : IContratoModel {
        /// <summary>
        /// obtener o establecer el indice de la tabla de empleados
        /// </summary>
        [DataNames("CTEMP_ID")]
        new int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer clave de registro en sistema
        /// </summary>
        [DataNames("CTEMP_CLV")]
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del empleado
        /// </summary>
        [DataNames("CTEMP_NOM")]
        string Nombre { get; set; }

        /// <summary>
        /// obtener o establecer el apellido aterno del empleado
        /// </summary>
        [DataNames("CTEMP_PAPE")]
        string ApellidoPaterno { get; set; }

        /// <summary>
        /// obtener o establecer el ape´llido materno del empleado
        /// </summary>
        [DataNames("CTEMP_SAPE")]
        string ApellidoMaterno { get; set; }
    }
}
