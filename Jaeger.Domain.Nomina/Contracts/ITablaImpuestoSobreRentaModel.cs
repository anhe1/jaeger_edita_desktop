﻿using System.ComponentModel;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ITablaImpuestoSobreRentaModel {
        int Id { get; set; }

        int Anio { get; set; }

        int Periodo { get; set; }

        string Descripcion { get; }

        BindingList<ImpuestoSobreRentaModel> Rangos { get; set; }

        ImpuestoSobreRentaModel GetImpuestoSobreRenta(decimal baseGravable);

        decimal Calcular(decimal baseGravable);
    }
}
