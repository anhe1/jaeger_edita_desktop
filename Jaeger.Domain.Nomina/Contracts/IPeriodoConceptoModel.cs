﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface IPeriodoConceptoModel {
        /// <summary>
        /// indice de la tabla[edit]
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// indice del catalogo de empleados[edit]
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// indice de relacion con la tabla de concepto de nomina(percepciones o deducciones)
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer tipo de concepto 1 = Percepcion, 2 = Deduccion
        /// </summary>
        int IdConceptoTipo { get; set; }

        /// <summary>
        /// obtener o establecer base fiscal
        /// </summary>
        int IdBaseFiscal { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de incidencias
        /// </summary>
        int IdTablaIncidencias { get; set; }

        /// <summary>
        /// obtener o establecer el total del concepto
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer Total de Percepciones Gravado
        /// </summary>
        decimal TPercepcionGravado { get; set; }

        /// <summary>
        /// obtener o establecer Total de Percepciones Exento
        /// </summary>
        decimal TPercepcionExento { get; set; }

        /// <summary>
        /// obtener o establecer Total de Deducciones
        /// </summary>
        decimal TDeducciones { get; set; }
    }
}
