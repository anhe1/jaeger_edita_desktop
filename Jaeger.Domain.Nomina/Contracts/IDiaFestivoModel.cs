﻿namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// interface Dias Festivos
    /// </summary>
    public interface IDiaFestivoModel {
        /// <summary>
        /// obtener o establecer fecha 
        /// </summary>
        System.DateTime Fecha { get; set; }

        /// <summary>
        /// obtener o establecer descripcion
        /// </summary>
        string Descripcion { get; set; }
    }
}
