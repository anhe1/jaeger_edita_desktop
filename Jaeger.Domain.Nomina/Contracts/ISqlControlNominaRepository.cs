﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface ISqlControlNominaRepository : IGenericRepository<ControlNominaModel> {
        IEnumerable<ControlNominaModel> GetList(bool onlyActive);
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
