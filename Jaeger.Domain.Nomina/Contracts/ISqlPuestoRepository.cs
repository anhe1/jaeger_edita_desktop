﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla Puesto.
    /// </summary>
    public interface ISqlPuestoRepository : IGenericRepository<PuestoModel> {
        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new();

        PuestoModel Save(PuestoModel model);
    }
}
