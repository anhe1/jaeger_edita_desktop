﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// define la interfaz para el repositorio de concepto de nomina
    /// </summary>
    public interface ISqlNominaConceptoRepository : IGenericRepository<NominaConceptoModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        INominaConceptoDetailModel Save(INominaConceptoDetailModel model);
    }
}
