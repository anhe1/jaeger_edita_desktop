﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.Domain.Nomina.Contracts {
    /// <summary>
    /// Interfaz que define los métodos para el repositorio de la tabla de impuesto sobre la renta.
    /// </summary>
    public interface ISqlTablaImpuestoSobreRentaRepository : IGenericRepository<TablaImpuestoSobreRentaModel> {
        /// <summary>
        /// Obtiene una lista de tablas de impuesto sobre la renta.
        /// </summary>
        /// <param name="conditionals">condicionales</param>
        /// <returns>IEnumerable</returns>
        IEnumerable<TablaImpuestoSobreRentaModel> GetList(List<IConditional> conditionals);

        /// <summary>
        /// Obtiene una tabla de impuesto sobre la renta por año y periodo.
        /// </summary>
        /// <param name="year">año</param>
        /// <param name="periodo">periodo</param>
        /// <returns>ITablaImpuestoSobreRentaModel</returns>
        TablaImpuestoSobreRentaModel GetById(int year, int periodo);

        /// <summary>
        /// Guarda una tabla de impuesto sobre la renta.
        /// </summary>
        /// <param name="tabla">List</param>
        /// <returns>List</returns>
        BindingList<TablaImpuestoSobreRentaModel> Save(BindingList<TablaImpuestoSobreRentaModel> tabla);
    }
}
