﻿using System;

namespace Jaeger.Domain.Nomina.Contracts {
    public interface INominaPeriodoEmpleadoModel {
        /// <summary>
        /// _nmnprde_id 
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer el indice del periodo NMCAL_PRD_ID
        /// </summary>
        int IdPeriodo { get; set; }

        /// <summary>
        /// obtener o establecer el indce del empleado del catalogo (EMP_ID)
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer clave de control interno
        /// </summary>
        string Clave { get; set; }

        /// <summary>
        /// obtener o establecer fecha calculo
        /// </summary>
        DateTime? FechaCal { get; set; }

        /// <summary>
        /// obtener o establecer fecha de ingreso
        /// </summary>
        DateTime? FechaIngreso { get; set; }

        /// <summary>
        /// obtener o establecer bandera para puntualidad y asistencia
        /// </summary>
        bool Puntualidad { get; set; }

        /// <summary>
        /// obtene o establecer las horas de la jornada de trabajo
        /// </summary>
        decimal HorasJornadaTrabajo { get; set; }

        /// <summary>
        /// obtener o establecer horas de relog
        /// </summary>
        decimal HorasReloj { get; set; }

        /// <summary>
        /// obtener o establecer las horas extra autorizadas
        /// </summary>
        decimal HorasAutorizadas { get; set; }

        /// <summary>
        /// obtener o establecer el % del premio de produccion
        /// </summary>
        int Productividad { get; set; }

        /// <summary>
        /// obtener o establecer dias de incapacidad
        /// </summary>
        int Incapacidad { get; set; }

        /// <summary>
        /// obtener o establecer ausencias
        /// </summary>
        int Ausencia { get; set; }

        /// <summary>
        /// obtener o establecer dias de vacaciones
        /// </summary>
        int TotalVacaciones { get; set; }

        /// <summary>
        /// obtener o establecer descuento diario por pago de credito de vivienda (infonavit)
        /// </summary>
        decimal DescuentoInfonavit { get; set; }

        /// <summary>
        /// obtener o establecer descuento por anticipo a salarios
        /// </summary>
        decimal DescuentoPrestamo { get; set; }

        /// <summary>
        /// obtener o establecer salario diario
        /// </summary>
        decimal SalarioDiario { get; set; }

        /// <summary>
        /// obtener o establecer salario diario integrado personalizado
        /// </summary>
        decimal SDIP { get; set; }

        /// <summary>
        /// obtener o establecer numero de jornadas de trabajo
        /// </summary>
        int JornadasTrabajo { get; set; }

        /// <summary>
        /// obtener o establecer horas por dia
        /// </summary>
        int HorasXDia { get; set; }

        /// <summary>
        /// obtener o establecer dias del periodo
        /// </summary>
        int DiasPeriodo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int DiasXAnio { get; set; }

        int DiasAguinaldo { get; set; }

        decimal PrimaVacacional { get; set; }

        decimal SalarioMinimo { get; set; }

        /// <summary>
        /// unidad de medida y actulizacion
        /// </summary>
        decimal UMA { get; set; }

        int IdTablaISR { get; set; }

        int IdTablaSube { get; set; }

        decimal SM3 { get; set; }

        decimal UMA3 { get; set; }

        /// <summary>
        /// obtener o establecer numero de horas extra autorizadas (horas por dia menos horas de reloj)
        /// </summary>
        decimal TotalHorasExtra { get; set; }

        /// <summary>
        /// obtener o establecer años de servicio
        /// </summary>
        decimal AnioServicio { get; set; }

        decimal DiasVacaciones { get; set; }

        decimal FactorSDI { get; set; }

        decimal SDI { get; set; }

        decimal Exedente { get; set; }

        decimal PercepcionGravado { get; set; }

        decimal PercepcionExento { get; set; }

        decimal Deducciones { get; set; }

        decimal ISPT { get; set; }

        decimal SubSidioAlEmpleo { get; set; }

        decimal IMSSMensualE { get; set; }

        decimal IMSSBimestralE { get; set; }

        decimal IMSSTotalE { get; set; }

        decimal IMSSMensualP { get; set; }

        decimal IMSSBimestralP { get; set; }

        decimal Infonavit { get; set; }

        decimal IMSSTotalP { get; set; }
    }
}
