﻿namespace Jaeger.Domain.Nomina.Contracts {
    public interface IEmpleadoConceptoNominaModel {
        /// <summary>
        /// obtener o establecer indice principal de la tabla (_ctlempc_id)
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla del catalogo de empleados
        /// </summary>
        int IdEmpleado { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de conceptos de nomina
        /// </summary>
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con la tablas de sistema
        /// </summary>
        int IdTabla { get; set; }

        /// <summary>
        /// obtener o establecer monto del importe fijo
        /// </summary>
        decimal ImporteGravado { get; set; }
    }
}
