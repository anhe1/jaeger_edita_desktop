﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;

namespace Jaeger.QRCode.Helpers {
    /// <summary>
    /// funciones para qr con la libreria ThoughtWorks
    /// </summary>
    public class QRCodeExtension {
        /// <summary>
        /// constructor
        /// </summary>
        public QRCodeExtension() { }

        /// <summary>
        /// obtener imagen QR de una cadena 
        /// </summary>
        public static Image GetQRImage(string contenido) {
            Image image = null;
            var qRCodeEncoder = new QRCodeEncoder() {
                QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE,
                QRCodeScale = 2,
                QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q
            };
            qRCodeEncoder.QRCodeVersion = 0;

            try {
                image = qRCodeEncoder.Encode(contenido, Encoding.UTF8);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return image;
        }

        /// <summary>
        /// obtener QrCode en base 64
        /// </summary>
        public static string GetQRBase64(string[] contenido) {
            return Convert.ToBase64String(GetQRBytes(contenido));
        }

        /// <summary>
        /// obtener QrCode en cadena de bytes
        /// </summary>
        public static byte[] GetQRBytes(string[] contenido) {
            return QRCodeExtension.CopyImageToByteArray(QRCodeExtension.GetQRImage(string.Concat(contenido)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image">image</param>
        /// <returns>string</returns>
        public static string GetString(Bitmap image) {
            try {
                var bitmap = new Bitmap(image);
                var qRCodeDecoder = new QRCodeDecoder();
                var message = qRCodeDecoder.decode(new QRCodeBitmapImage(bitmap), Encoding.UTF8);
                if (string.IsNullOrEmpty(message)) {
                    return "No se obtuvo ningún resultado de la imagen seleccionada";
                }
                return message;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// copiar contenido de imagen a arreglo de bytes
        /// </summary>
        /// <param name="theImage"></param>
        public static byte[] CopyImageToByteArray(Image theImage) {
            try {
                using (var memoryStream = new MemoryStream()) {
                    theImage.Save(memoryStream, ImageFormat.Png);
                    return memoryStream.ToArray();
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
