﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Aplication.Ventas.Contracts {
    public interface IComisionService : IVendedoresService {
        /// <summary>
        /// obtener catalogo de comision por indice
        /// </summary>
        ComisionDetailModel GetComision(int index);

        /// <summary>
        /// obtener listado del catalogo de comisiones
        /// </summary>
        BindingList<ComisionDetailModel> GetComisiones(bool onlyActive);

        /// <summary>
        /// almacenar objeto catalogo de comision
        /// </summary>
        ComisionDetailModel Save(ComisionDetailModel model);

        /// <summary>
        /// obtener listado de comisiones por pagar en remisiones
        /// </summary>
        BindingList<RemisionComisionDetailModel> GetComisionesPorPagar(int year, int idVendedor);

        BindingList<IRemisionComisionDetailModel> GetTo(List<IConditional> conditionals);

        /// <summary>
        /// almacenar objeto remision-comision
        /// </summary>
        RemisionComisionDetailModel Saveable(RemisionComisionDetailModel item);

        /// <summary>
        /// actualizar bandera de comisiones
        /// </summary>
        /// <param name="idRemision"></param>
        /// <param name="value">0 <-no esta agregada a ningun recibo, 1 <- comsion autorizada y esta agregada a un recibo, 2 <-comision pagada y ya esta autorizada en un recibo</param>
        bool Update(int[] idRemision, int value);

        /// <summary>
        /// obtener catalogo de vendedores
        /// </summary>
        //BindingList<VendedorModel> GetVendedores(bool onlyActive);

        /// <summary>
        /// calcular comisiones de un listao de remisiones
        /// </summary>
        void Calcular(ComisionDetailModel catalogo, BindingList<RemisionComisionDetailModel> remisiones);

        /// <summary>
        /// listado de prioridades
        /// </summary>
        //List<PrioridadModel> GetPrioridad();
    }
}
