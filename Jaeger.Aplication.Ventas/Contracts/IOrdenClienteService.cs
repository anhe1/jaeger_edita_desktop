﻿using System.ComponentModel;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Aplication.Ventas {
    /// <summary>
    /// orden de cliente
    /// </summary>
    public interface IOrdenClienteService {
        /// <summary>
        /// listado de clientes
        /// </summary>
        BindingList<ClienteDetailModel> GetClientes();

        /// <summary>
        /// listado de ordenes de cliente
        /// </summary>
        BindingList<OrdenClienteModel> GetList(int year, int month);

        /// <summary>
        /// almacenar orden de compra
        /// </summary>
        OrdenClienteDetailModel Save(OrdenClienteDetailModel item);

        /// <summary>
        /// obtener orden de cliente
        /// </summary>
        OrdenClienteDetailModel GetById(int index);

        /// <summary>
        /// crear tablas necesarias
        /// </summary>
        bool Create();
    }
}
