﻿using System;
using System.Linq;
using System.ComponentModel;
using Jaeger.DataAccess.Repositories;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Amazon.S3.Contracts;
using Jaeger.Amazon.S3.Helpers;
using Jaeger.DataAccess.Contribuyentes.Repositories;
using Jaeger.Domain.Ventas.Builder;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.Aplication.Ventas {
    /// <summary>
    /// servicio de orden de cliente
    /// </summary>
    public class OrdenClienteService : IOrdenClienteService {
        protected ISqlOrdenClienteRepository ordenRepository;
        protected ISqlContribuyenteRepository clienteRepository;
        protected ISimpleStorageService s3;

        /// <summary>
        /// constructor
        /// </summary>
        public OrdenClienteService() {
            this.clienteRepository = new SqlSugarContribuyenteRepository(ConfigService.Synapsis.RDS.Edita, ConfigService.Piloto.Clave);
            this.ordenRepository = new SqlSugarOrdenClienteRepository(ConfigService.Synapsis.RDS.Edita);
            this.s3 = new SimpleStorageServiceAWS3(ConfigService.Synapsis.Amazon.S3.AccessKeyId, ConfigService.Synapsis.Amazon.S3.SecretAccessKey, ConfigService.Synapsis.Amazon.S3.Region);
        }

        public BindingList<ClienteDetailModel> GetClientes() {
            return new BindingList<ClienteDetailModel>(this.ordenRepository.GetList<ClienteDetailModel>(OrdenClienteService.Query().Year(0).Month(0).Build()).ToList());
        }

        /// <summary>
        /// listado de ordenes de clientes
        /// </summary>
        public BindingList<OrdenClienteModel> GetList(int year, int month) {
            return new BindingList<OrdenClienteModel>(this.ordenRepository.GetList<OrdenClienteModel>(OrdenClienteService.Query().Year(0).Month(0).Build()).ToList());
        }

        public OrdenClienteDetailModel GetById(int index) {
            return this.ordenRepository.GetOrden(index);
        }

        public OrdenClienteDetailModel Save(OrdenClienteDetailModel item) {
            if (item.Id == 0) {
                item.Creo = ConfigService.Piloto.Clave;
                item.FechaNuevo = DateTime.Now;
            }
            else {
                item.Modifica = ConfigService.Piloto.Clave;
                item.FechaModifica = DateTime.Now;
            }

            for (int i = 0; i < item.Documentos.Count; i++) {
                if (item.Documentos[i].Id == 0) {
                    item.Documentos[i].FechaNuevo = DateTime.Now;
                    item.Documentos[i].Creo = ConfigService.Piloto.Clave;
                    item.Documentos[i].URL = this.Upload(item.Documentos[i]);
                }
            }

            item = this.ordenRepository.Save(item);
            
            return item;
        }

        public bool Create() {
            return this.ordenRepository.Create();
        }

        private string Upload(OrdenClienteDoctoModel item) {
            return this.s3.Upload(item.Tag, item.KeyName(), item.Content, ConfigService.Synapsis.Amazon.S3.BucketName, ConfigService.Synapsis.Amazon.S3.Region, true);
        }

        public static IOrdenClienteQueryBuilder Query() {
            return new OrdenClienteQueryBuilder();
        }
    }
}
