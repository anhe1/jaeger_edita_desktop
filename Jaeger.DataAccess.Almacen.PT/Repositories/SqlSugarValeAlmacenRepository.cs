﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.PT.Repositories {
    public class SqlSugarValeAlmacenRepository : MySqlSugarContext<ValeAlmacenModel>, ISqlValeAlmacenRepository {
        public SqlSugarValeAlmacenRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sql = @"SELECT * FROM ALMPT LEFT JOIN ALMPTS ON ALMPT.ALMPT_ID = ALMPTS.ALMPTS_ALMPT_ID AND ALMPTS.ALMPTS_STTSB_ID = 0 @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            } else if (typeof(T1) == typeof(ValeAlmacenProductoView)) {
                var sql = @"SELECT * FROM MVAPT LEFT JOIN ALMPT ON MVAPT.MVAPT_ALMPT_ID = ALMPT.ALMPT_ID @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        /// <summary>
        /// recuperar objeto vale de almacen por su indice
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public new ValeAlmacenDetailModel GetById(int index) {
            //Manual mode
            var response = this.Db.Queryable<ValeAlmacenDetailModel>().Where(it => it.IdComprobante == index).Select<ValeAlmacenDetailModel>().Mapper((vale, cache) => {
                var allItems = cache.Get(valeList => {
                    var allIds = valeList.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenConceptoModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                var status = cache.Get(item1 => {
                    var allIds = item1.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenStatusModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                var relaciones = cache.Get(item1 => {
                    var allIds = item1.Select(it => it.IdComprobante).ToList();
                    return this.Db.Queryable<ValeAlmacenRelacionModel>().Where(it => allIds.Contains(it.IdComprobante)).ToList();//Execute only once
                });

                vale.Conceptos = new BindingList<ValeAlmacenConceptoModel>(allItems.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
                vale.Autorizacion = new BindingList<ValeAlmacenStatusModel>(status.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
                vale.Relaciones = new BindingList<ValeAlmacenRelacionModel>(relaciones.Where(it => it.IdComprobante == vale.IdComprobante).ToList());//Every time it's executed
            }).Single();

            return response;
        }

        /// <summary>
        /// almacenar un vale de almacen
        /// </summary>
        public IValeAlmacenDetailModel Save(IValeAlmacenDetailModel item) {
            if (item.IdComprobante == 0) {
                item.Folio = this.Folio(item as ValeAlmacenDetailModel);
                item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString(), item.Serie, item.FechaEmision.ToShortDateString(), item.IdDirectorio.ToString(), item.ReceptorRFC, item.Receptor, item.IdComprobante.ToString() });
                item.Periodo = item.FechaEmision.Month;
                item.Ejercicio = item.FechaEmision.Year;
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                //item.IdComprobante = this.Insert(item as ValeAlmacenDetailModel);
                var result = this.Db.Insertable(item as ValeAlmacenDetailModel);
                item.IdComprobante = this.Execute(result);
            } else {
                item.FechaModifica = DateTime.Now;
                item.Modifica = this.User;
                var result = this.Db.Updateable(item as ValeAlmacenDetailModel);
                this.Execute(result);
            }

            if (item.IdComprobante > 0) {
                for (int i = 0; i < item.Conceptos.Count; i++) {
                    if (item.Conceptos[i].IdMovimiento == 0) {
                        item.Conceptos[i].IdComprobante = item.IdComprobante;
                        item.Conceptos[i].IdTipoComprobante = item.IdTipoComprobante;
                        item.Conceptos[i].IdTipoMovimiento = item.IdTipoMovimiento;
                        item.Conceptos[i].FechaNuevo = DateTime.Now;
                        item.Conceptos[i].Creo = this.User;
                        item.Conceptos[i].IdMovimiento = this.Db.Insertable<ValeAlmacenConceptoModel>(item.Conceptos[i]).ExecuteReturnIdentity();
                    } else {
                        item.Conceptos[i].Modifica = this.User;
                        item.Conceptos[i].FechaModifica = DateTime.Now;
                        this.Db.Updateable<ValeAlmacenConceptoModel>(item.Conceptos[i]).ExecuteCommand();
                    }
                }
            }
            return item;
        }

        /// <summary>
        /// crear tablas del sistema de almacen
        /// </summary>
        /// <returns>verdadero si la tarea se completa correctamente.</returns>
        public bool CrearTablas() {
            try {
                base.Db.CodeFirst.InitTables<ValeAlmacenModel, ValeAlmacenConceptoModel>();
            } catch (Exception ex) {
                var exSugar = ex as SqlSugarException;
                if (exSugar != null)
                    Console.WriteLine("SQL: " + exSugar.Message);
                Console.WriteLine(ex.Message);
            }
            return this.CreateTable();
        }

        public int Folio(IValeAlmacenModel objeto) {
            int folio;
            try {
                folio = this.Db.Queryable<ValeAlmacenModel>().Where(it => it.IdTipoComprobante == objeto.IdTipoComprobante).Where(it => it.IdAlmacen == objeto.IdAlmacen)
                    .Select(it => new { maximus = SqlFunc.AggregateMax(it.Folio) }).Single().maximus + 1;
            } catch (SqlSugarException ex) {
                Console.WriteLine("SQL:" + ex.Message);
                folio = 0;
            }

            return folio;
        }
    }
}
