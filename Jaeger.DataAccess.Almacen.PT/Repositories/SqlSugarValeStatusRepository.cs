﻿using System.Linq;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.PT.Repositories {
    public class SqlSugarValeStatusRepository : MySqlSugarContext<ValeAlmacenStatusModel>, ISqlValeStatusRepository {
        public SqlSugarValeStatusRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenStatusModel)) {
                var sql = @"SELECT * FROM ALMPTS @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        public IValeAlmacenStatusModel Save(IValeAlmacenStatusModel item) {
            var sql = @"INSERT INTO ALMPTS ( ALMPTS_ALMPT_ID, ALMPTS_STTS_ID, ALMPTS_STTSB_ID, ALMPTS_CTMTV_ID, ALMPTS_DRCTR_ID, ALMPTS_CTMTV, ALMPTS_NOTA, ALMPTS_USR_N, ALMPTS_FN) 
                                    VALUES (@ALMPTS_ALMPT_ID,@ALMPTS_STTS_ID,@ALMPTS_STTSB_ID,@ALMPTS_CTMTV_ID,@ALMPTS_DRCTR_ID,@ALMPTS_CTMTV,@ALMPTS_NOTA,@ALMPTS_USR_N,@ALMPTS_FN) 
                    ON DUPLICATE KEY UPDATE ALMPTS_ALMPT_ID = @ALMPTS_ALMPT_ID, ALMPTS_STTS_ID = @ALMPTS_STTS_ID, ALMPTS_STTSB_ID = @ALMPTS_STTSB_ID, ALMPTS_CTMTV_ID = @ALMPTS_CTMTV_ID,
ALMPTS_DRCTR_ID = @ALMPTS_DRCTR_ID, ALMPTS_CTMTV = @ALMPTS_CTMTV, ALMPTS_NOTA = @ALMPTS_NOTA, ALMPTS_USR_N = @ALMPTS_USR_N, ALMPTS_FN = @ALMPTS_FN;";

            var parametros = new List<SugarParameter> {
                new SugarParameter("@ALMPTS_ALMPT_ID", item.IdComprobante),
                new SugarParameter("@ALMPTS_STTS_ID", item.IdStatus),
                new SugarParameter("@ALMPTS_STTSB_ID", item.IdStatusB),
                new SugarParameter("@ALMPTS_CTMTV_ID", item.IdCveMotivo),
                new SugarParameter("@ALMPTS_DRCTR_ID", item.IdDirectorio),
                new SugarParameter("@ALMPTS_CTMTV", item.CveMotivo),
                new SugarParameter("@ALMPTS_NOTA", item.Nota),
                new SugarParameter("@ALMPTS_USR_N", item.Creo),
                new SugarParameter("@ALMPTS_FN", item.FechaNuevo)
            };
            var d0 = this.Db.Ado.ExecuteCommand(sql, parametros) > 0;

            return item;
        }
    }
}
