﻿using System;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.PT.Repositories {
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones de Bienes, Productos y Servicios (BPS) del almacen de producto terminado
    /// </summary>
    public class SqlSugarCategoriaRepository : MySqlSugarContext<CategoriaModel>, ISqlCategoriaRepository {

        public SqlSugarCategoriaRepository(DataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public new IEnumerable<CategoriaModel> GetList() {
            return Db.Queryable<CategoriaModel>().Select((m) => new CategoriaModel {
                Activo = m.Activo,
                IdAlmacen = m.IdAlmacen,
                Clave = m.Clave,
                Creo = m.Creo,
                Descripcion = m.Descripcion,
                FechaNuevo = m.FechaNuevo,
                IdCategoria = m.IdCategoria,
                IdSubCategoria = m.IdSubCategoria
            }).ToList();
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = "SELECT CTCLS.* FROM CTCLS @condiciones";
            if (typeof(T1) == typeof(CategoriaModel)) {
                return GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(ClasificacionProductoModel)) {
                sqlCommand = @"WITH RECURSIVE Clasificacion (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, URL, NIVEL, CHILDS) AS
                            (
                              SELECT CTCLS_ID AS IDCATEGORIA, CTCLS_SBID AS SUBID, CTCLS_A AS ACTIVO,CTCLS_CLASS1 AS CLASE, CTCLS_CLASS1 || '/' AS DESCRIPCION, CTCLS_URL AS URL, 1,
                              (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = CTCLS_ID) AS CHILDS
                                FROM CTCLS
                                WHERE CTCLS_ID = 1 -- THE TREE NODE
                              UNION ALL
                              SELECT T.CTCLS_ID AS IDCATEGORIA, T.CTCLS_SBID AS SUBID, (T.CTCLS_A * TP.ACTIVO) AS ACTIVO, T.CTCLS_CLASS1 AS  CLASE, (TP.DESCRIPCION || T.CTCLS_CLASS1|| ' | ') AS DESCRIPCION, CTCLS_URL AS URL, TP.NIVEL + 1,
                              (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = T.CTCLS_ID) AS CHILDS
                                FROM Clasificacion AS TP 
                                 JOIN CTCLS AS T ON TP.IDCATEGORIA = T.CTCLS_SBID
                            )
                            SELECT * FROM Clasificacion
                            RIGHT JOIN CTPRD ON Clasificacion.IdCategoria = CTPRD.CTPRD_CTCLS_ID 
                            WHERE CTPRD.CTPRD_A = 1;
                            ORDER BY NIVEL, CHILDS;";

                return GetMapper<T1>(sqlCommand, conditionals);
            }
            //else if (typeof(T1) == typeof(CategoriaProductoTerminado)) {
            //    sqlCommand = @"
            //            WITH RECURSIVE Categorias (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, URL, NIVEL, CHILDS) AS
            //            (
            //              SELECT CTCLS_ID AS IDCATEGORIA, CTCLS_SBID AS SUBID, CTCLS_A AS ACTIVO,CTCLS_CLASS1 AS CLASE, CTCLS_CLASS1 || ' | ' AS DESCRIPCION, CTCLS_URL AS URL, 1,
            //              (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = CTCLS_ID) AS CHILDS
            //                FROM CTCLS
            //                WHERE CTCLS_ID = 1 -- THE TREE NODE
            //              UNION ALL
            //              SELECT T.CTCLS_ID AS IDCATEGORIA, T.CTCLS_SBID AS SUBID, (T.CTCLS_A * TP.ACTIVO) AS ACTIVO, T.CTCLS_CLASS1 AS  CLASE, (TP.DESCRIPCION || T.CTCLS_CLASS1|| ' | ') AS DESCRIPCION, CTCLS_URL AS URL, TP.NIVEL + 1,
            //              (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = T.CTCLS_ID) AS CHILDS
            //                FROM Categorias AS TP 
            //                 JOIN CTCLS AS T ON TP.IDCATEGORIA = T.CTCLS_SBID
            //            )
            //            SELECT * FROM Categorias
            //            ORDER BY NIVEL, CHILDS;";

            //    return GetMapper<T1>(sqlCommand, conditionals);
            //}
            throw new NotImplementedException();
        }
    }
}
