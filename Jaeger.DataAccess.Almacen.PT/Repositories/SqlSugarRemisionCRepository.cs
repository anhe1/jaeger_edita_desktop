﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarRemisionCRepository : MySqlSugarContext<RemisionComisionModel>, ISqlRemisionCRepository {
        public SqlSugarRemisionCRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public RemisionComisionModel Save(RemisionComisionModel item) {
            item.FechaModifica = DateTime.Now;
            item.Modifica = this.User;
            if (item.IdRemision == 0) {
                item.IdRemision = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }

        public int Saveable(RemisionComisionModel item) {
            var 
                CommandText = @"UPDATE OR INSERT INTO RMSNC ( RMSNC_RMSN_ID, RMSNC_A, RMSNC_CTCMS_ID, RMSNC_DRCTR_ID, RMSNC_VNDR_ID, RMSNC_CTDIS_ID, RMSNC_DESC, RMSNC_FCPAC, RMSNC_FCACT, RMSNC_IMPR, RMSNC_ACUML, RMSNC_VNDR, RMSNC_FN, RMSNC_USR_N)
                                                     VALUES (@RMSNC_RMSN_ID,@RMSNC_A,@RMSNC_CTCMS_ID,@RMSNC_DRCTR_ID,@RMSNC_VNDR_ID,@RMSNC_CTDIS_ID,@RMSNC_DESC,@RMSNC_FCPAC,@RMSNC_FCACT,@RMSNC_IMPR,@RMSNC_ACUML,@RMSNC_VNDR,@RMSNC_FN,@RMSNC_USR_N) MATCHING (RMSNC_RMSN_ID);"
            ;
            item.Modifica = this.User;
            item.FechaModifica = DateTime.Now;
            var sqlCommand = new List<SugarParameter> {
                new SugarParameter("@RMSNC_RMSN_ID", item.IdRemision),
                new SugarParameter("@RMSNC_A", item.Activo),
                new SugarParameter("@RMSNC_CTCMS_ID", item.IdComision),
                new SugarParameter("@RMSNC_DRCTR_ID", item.IdCliente),
                new SugarParameter("@RMSNC_VNDR_ID", item.IdVendedor),
                new SugarParameter("@RMSNC_CTDIS_ID", item.IsDisponible),
                new SugarParameter("@RMSNC_DESC", item.Comision),
                new SugarParameter("@RMSNC_FCPAC", item.ComisionFactor),
                new SugarParameter("@RMSNC_FCACT", item.ComisionFactorActualizado),
                new SugarParameter("@RMSNC_IMPR", item.ComisionPorPagar),
                new SugarParameter("@RMSNC_ACUML", item.ComisionPagada),
                new SugarParameter("@RMSNC_VNDR", item.Vendedor),
                new SugarParameter("@RMSNC_FN", item.FechaModifica),
                new SugarParameter("@RMSNC_USR_N", item.Modifica)
            };

            return this.Db.Ado.ExecuteCommand(CommandText, sqlCommand);
        }

        public bool Update(int[] idRemision, int value) {
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetToList<T1>(List<IConditional> condidional) where T1 : class, new() {
            throw new NotImplementedException();
        }
    }
}
