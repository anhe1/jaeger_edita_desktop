﻿using System.Linq;
using System.Collections.Generic;
using System;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Almacen.PT.Repositories {
    public class SqlSugarValeRelacionRepository : MySqlSugarContext<ValeAlmacenRelacionModel>, ISqlValeRelacionRepository {
        public SqlSugarValeRelacionRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ValeAlmacenSingleModel)) {
                var sql = @"SELECT * FROM ALMPTR @wcondiciones";
                return base.GetMapper<T1>(sql, conditionals).ToList();
            }
            return base.GetList<T1>(conditionals);
        }

        public int Save(IValeAlmacenRelacionModel item) {
            item.FechaNuevo = DateTime.Now;
            item.Creo = this.User;
            var sql = @"INSERT INTO ALMPTR ( ALMPTR_ALMPT_ID, ALMPTR_DOC_ID, ALMPTR_CTREL_ID, ALMPTR_CTREL, ALMPTR_UUID, ALMPTR_DRCTR_ID, ALMPTR_FOLIO, ALMPTR_SERIE, ALMPTR_CLV, ALMPTR_RFCR, ALMPTR_NOM, ALMPTR_USR_N, ALMPTR_FN, ALMPTR_FECEMS) 
                                    VALUES (@ALMPTR_ALMPT_ID,@ALMPTR_DOC_ID,@ALMPTR_CTREL_ID,@ALMPTR_CTREL,@ALMPTR_UUID,@ALMPTR_DRCTR_ID,@ALMPTR_FOLIO,@ALMPTR_SERIE,@ALMPTR_CLV,@ALMPTR_RFCR,@ALMPTR_NOM,@ALMPTR_USR_N,@ALMPTR_FN,@ALMPTR_FECEMS)
                       ON DUPLICATE KEY UPDATE  
                                     ALMPTR_ALMPT_ID=@ALMPTR_ALMPT_ID, ALMPTR_DOC_ID=@ALMPTR_DOC_ID, ALMPTR_CTREL_ID=@ALMPTR_CTREL_ID, ALMPTR_CTREL=@ALMPTR_CTREL, ALMPTR_UUID=@ALMPTR_UUID, ALMPTR_DRCTR_ID=@ALMPTR_DRCTR_ID, 
                                              ALMPTR_FOLIO=@ALMPTR_FOLIO, ALMPTR_SERIE=@ALMPTR_SERIE, ALMPTR_CLV=@ALMPTR_CLV, ALMPTR_RFCR=@ALMPTR_RFCR, ALMPTR_NOM=@ALMPTR_NOM, ALMPTR_USR_N=@ALMPTR_USR_N, ALMPTR_FN=@ALMPTR_FN;";
            var parametros = new List<SugarParameter> {
                new SugarParameter("@ALMPTR_ALMPT_ID", item.IdComprobante),
                new SugarParameter("@ALMPTR_DOC_ID", item.IdTipoComprobante),
                new SugarParameter("@ALMPTR_CTREL_ID", item.IdClaveRelacion),
                new SugarParameter("@ALMPTR_CTREL", item.ClaveRelacion),
                new SugarParameter("@ALMPTR_UUID", item.IdDocumento),
                new SugarParameter("@ALMPTR_DRCTR_ID", item.IdDirectorio),
                new SugarParameter("@ALMPTR_FOLIO", item.Folio),
                new SugarParameter("@ALMPTR_SERIE", item.Serie),
                new SugarParameter("@ALMPTR_CLV", item.Clave),
                new SugarParameter("@ALMPTR_RFCR", item.ReceptorRFC),
                new SugarParameter("@ALMPTR_NOM", item.Receptor),
                new SugarParameter("@ALMPTR_USR_N", item.Creo),
                new SugarParameter("@ALMPTR_FN", item.FechaNuevo),
                new SugarParameter("@ALMPTR_FECEMS", item.FechaEmision)
            };
            var d0 = this.Db.Ado.ExecuteCommand(sql, parametros);
            return d0;
        }
    }
}
