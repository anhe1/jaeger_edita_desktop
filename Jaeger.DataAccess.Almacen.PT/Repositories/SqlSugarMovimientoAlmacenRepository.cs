﻿using System;
using SqlSugar;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarMovimientoAlmacenRepository : MySqlSugarContext<MovimientoModel>, ISqlMovimientoAlmacenPT {
        public SqlSugarMovimientoAlmacenRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT * FROM MVAPT @wcondiciones";
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<MovimientoDetailModel> GetList(List<IConditional> condidional) {
            return this.GetList<MovimientoDetailModel>(condidional);
        }

        public MovimientoDetailModel Save(MovimientoDetailModel model) {
            if (model.IdMovimiento == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdMovimiento = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }

        public ValeAlmacenConceptoModel Save(ValeAlmacenConceptoModel vale) {
            if (vale.IdMovimiento == 0) {
                vale.FechaNuevo = DateTime.Now;
                vale.Creo = this.User;
                vale.IdMovimiento = this.Insert(vale);
            } else {
                vale.FechaModifica = DateTime.Now;
                vale.Modifica = this.User;
                this.Update(vale);
            }
            return vale;
        }

        public bool Existencia(List<IConditional> conditionals) {
            var sqlCommand = @"INSERT INTO CTMDLX (CTMDLX_CTMDL_ID, CTMDLX_CTESPC_ID, CTMDLX_CTPRD_ID, CTMDLX_EXT)
SELECT  MVAMP_CTMDL_ID,  -1,  MVAMP_CTPRD_ID, IF (MVAMP_CTEFC_ID = 1, MVAMP_CNTE, (-1*MVAMP_CNTS))
FROM MVAMP
WHERE MVAMP_ALMMP_ID = @INDEX ON DUPLICATE KEY UPDATE CTMDLX_EXT= CTMDLX_EXT + IF (MVAMP_CTEFC_ID = 1, MVAMP_CNTE, (-1*MVAMP_CNTS))";
            return this.ExecuteTransaction(MySql.Services.ExpressionTool.Where(sqlCommand, conditionals)) > 0;
        }
    }

}
