﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    /// <summary>
    /// 
    /// </summary>
    public class SqlRemisionadoRepository : MySqlSugarContext<RemisionModel>, ISqlRemisionadoPTRepository {

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de base de datos</param>
        public SqlRemisionadoRepository(DataBaseConfiguracion configuracion, string user)
            : base(configuracion) {
            this.User = user;
        }

        public bool Update(string fieldName, object fieldValue, int indexPK) {
            throw new NotImplementedException();
        }

        public bool UploadPDF(IUploadPDF model) {
            return this.Db.Updateable<RemisionModel>()
                .SetColumns(it => new RemisionModel() {
                    UrlFilePDF = model.UrlFilePDF
                }).Where(it => it.IdRemision == model.IdRemision).ExecuteCommand() > 0;
        }

        /// <summary>
        /// obtener listado por condiciones
        /// </summary>
        /// <typeparam name="T1">objetos aceptados Remision y Remision Partida</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = @"SELECT RMSN.*, RMSNC.* FROM RMSN LEFT JOIN RMSNC ON RMSNC.RMSNC_RMSN_ID = RMSN.RMSN_ID @wcondiciones ORDER BY RMSN_FOLIO DESC";

            if (typeof(T1) == typeof(RemisionPartidaModel)) {
                sqlCommand = @"SELECT RMSN.*, MVAPT.* FROM RMSN LEFT JOIN MVAPT ON RMSN.RMSN_ID = MVAPT.MVAPT_RMSN_ID AND MVAPT.MVAPT_DOC_ID = 26 AND MVAPT.MVAPT_A = 1 @wcondiciones";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        public IEnumerable<T1> GetListR<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = @"SELECT RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR FROM RMSN @wcondiciones GROUP BY RMSN.RMSN_DRCTR_ID, RMSN.RMSN_NOMR, RMSN.RMSN_RFCR  ORDER BY RMSN.RMSN_NOMR ASC";
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        #region anterior
        /// <summary>
        /// obtener remision detalle
        /// </summary>
        /// <param name="index">indice de la remision</param>
        public new RemisionDetailModel GetById(int index) {
            //Manual mode
            var result = this.Db.Queryable<RemisionDetailModel>().Where(it => it.IdRemision == index).Mapper((itemModel, cache) => {
                var allItems = cache.Get(remisionList => {
                    var allIds = remisionList.Select(it => it.IdRemision).ToList();
                    return this.Db.Queryable<RemisionConceptoDetailModel>().Where(it => it.Activo == true).Where(it => allIds.Contains(it.IdRemision)).ToList();
                });
                itemModel.Conceptos = new BindingList<RemisionConceptoDetailModel>(allItems.Where(it => it.IdRemision == itemModel.IdRemision).ToList());
            }).Single();
            return result;
        }
        
        /// <summary>
        /// almacenar remision fiscal
        /// </summary>
        public RemisionDetailModel Save(RemisionDetailModel item) {
            if (item.IdRemision == 0) {
                if (item.Folio == 0) item.Folio = this.GetFolio(item.EmisorRFC, item.Serie);
                item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString("000000#"), item.EmisorRFC, item.ReceptorRFC, item.FechaEmision.ToString() });
                item.Creo = this.User;
                var _insert = this.Db.Insertable<RemisionDetailModel>(item);
                item.IdRemision = this.Execute<RemisionDetailModel>(_insert);
            }
            else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                var _update = this.Db.Updateable<RemisionDetailModel>(item);
                this.Execute(_update);
            }

            if (item.RemisionRelacionada != null) {

            }

            //if (item.Descuento != null) {
            //    if (item.Descuento.Id == 0) {
            //        item.Descuento.IdRemision = item.IdRemision;
            //        item.Descuento.Id = this.Insert(item.Descuento);
            //    }
            //    else {
            //        this.Update(item.Descuento);
            //    }
            //}

            for (int i = 0; i < item.Conceptos.Count; i++) {
                if (item.Conceptos[i].IdMovimiento == 0) {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    item.Conceptos[i].FechaNuevo = DateTime.Now;
                    var _insert = this.Db.Insertable<RemisionConceptoDetailModel>(item.Conceptos[i]);
                    item.Conceptos[i].IdMovimiento = this.Execute(_insert);
                }
                else {
                    item.Conceptos[i].IdRemision = item.IdRemision;
                    item.Conceptos[i].FechaModifica = DateTime.Now;
                    item.Conceptos[i].Modifica = this.User;
                    var _update = this.Db.Updateable<RemisionConceptoDetailModel>(item.Conceptos[i]);
                    this.Execute(_update);
                }
            }
            return item;
        }

        public bool CrearTablas() {
            try {
                this.Db.CodeFirst.InitTables<RemisionModel, RemisionConceptoModel, RemisionComisionModel, ValeAlmacenModel>();
                this.Db.CodeFirst.InitTables<MovimientoModel, RemisionRelacionadaModel, RemisionStatusModel>();
                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        ///  Generar folio para un comprobante
        /// </summary>
        /// <param name="rfc">RFC del emisor del comprobante</param>
        /// <param name="serie">Serie del comprobante fiscal</param>
        /// <returns>numero entero convertido en cadena</returns>
        private int GetFolio(string rfc, string serie) {
            try {
                int folio;
                if (serie != null)
                    folio = this.Db.Ado.GetInt("select (Max(cast(_aptrms._aptrms_folio as UNSIGNED)) + 1) as folio from _aptrms where _aptrms_rfce = @rfc and _aptrms._aptrms_serie = @serie", new { rfc = rfc, serie = serie });
                else
                    folio = this.Db.Ado.GetInt("select (Max(cast(_aptrms._aptrms_folio as UNSIGNED)) + 1) as folio from _aptrms where _aptrms_rfce = @rfc ", new { rfc = rfc });
                return folio;
            }
            catch (SqlSugarException ex) {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        public bool Update(IRemisionStatusDetailModel model) {
            throw new NotImplementedException();
        }
        #endregion
    }
}
