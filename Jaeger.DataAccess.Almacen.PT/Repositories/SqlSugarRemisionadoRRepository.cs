﻿using SqlSugar;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarRemisionadoRRepository : MySqlSugarContext<RemisionRelacionadaModel>, ISqlRemisionadoRRepository {
        public SqlSugarRemisionadoRRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<RemisionRelacionadaModel> GetList(List<IConditional> condidional) {
            return this.GetList<RemisionRelacionadaModel>(condidional);
        }

        public int Saveable(RemisionRelacionadaModel item) {
            var CommandText = @"UPDATE OR INSERT INTO RMSNR ( RMSNR_RMSN_ID, RMSNR_DRCTR_ID, RMSNR_CTREL_ID, RMSNR_RELN, RMSNR_UUID, RMSNR_SERIE, RMSNR_FOLIO, RMSNR_NOMR, RMSNR_FECEMS, RMSNR_TOTAL, RMSNR_USR_N, RMSNR_FN)
                                                     VALUES (@RMSNR_RMSN_ID,@RMSNR_DRCTR_ID,@RMSNR_CTREL_ID,@RMSNR_RELN,@RMSNR_UUID,@RMSNR_SERIE,@RMSNR_FOLIO,@RMSNR_NOMR,@RMSNR_FECEMS,@RMSNR_TOTAL,@RMSNR_USR_N,@RMSNR_FN)
                                                            MATCHING (RMSNR_RMSN_ID, RMSNR_DRCTR_ID, RMSNR_CTREL_ID, RMSNR_UUID)";

            var parametros = new List<SugarParameter> {
                new SugarParameter("@RMSNR_RMSN_ID", item.IdRemision),
                new SugarParameter("@RMSNR_DRCTR_ID", item.IdDirectorio),
                new SugarParameter("@RMSNR_CTREL_ID", item.IdTipoRelacion),
                new SugarParameter("@RMSNR_RELN", item.Relacion),
                new SugarParameter("@RMSNR_UUID", item.IdDocumento),
                new SugarParameter("@RMSNR_SERIE", item.Serie),
                new SugarParameter("@RMSNR_FOLIO", item.Folio),
                new SugarParameter("@RMSNR_NOMR", item.ReceptorNombre),
                new SugarParameter("@RMSNR_FECEMS", item.FechaEmision),
                new SugarParameter("@RMSNR_TOTAL", item.GTotal),
                new SugarParameter("@RMSNR_USR_N", item.Creo),
                new SugarParameter("@RMSNR_FN", item.FechaNuevo)
            };
            return this.Db.Ado.ExecuteCommand(CommandText, parametros);
        }
    }
}
