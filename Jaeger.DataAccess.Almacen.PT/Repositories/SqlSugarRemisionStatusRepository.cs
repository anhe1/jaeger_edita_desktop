﻿using System;
using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Entities;

namespace Jaeger.DataAccess.Repositories {
    public class SqlSugarRemisionStatusRepository : MySqlSugarContext<RemisionStatusModel>, ISqlRemisionStatusRepository {
        public SqlSugarRemisionStatusRepository(DataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public RemisionStatusModel Save(RemisionStatusModel item) {
            item.FechaNuevo = DateTime.Now;
            item.User = this.User;
            if (item.IdAuto == 0) {
                item.IdAuto = this.Insert(item);
            } else {
                this.Update(item);
            }
            if (item.IdAuto > 0) {
                this.Status(item);
            }
            return item;
        }

        public int Saveable(RemisionStatusModel item) {
            throw new NotImplementedException();
        }

        private bool Status(RemisionStatusModel model) {
            var CommandText = "UPDATE RMSN SET RMSN_STTS_ID = @RMSN_STTS_ID WHERE RMSN_ID = @RMSN_ID";
            var sqlCommand = new List<SugarParameter> {
                new SugarParameter("@RMSN_ID", model.IdRemision),
                new SugarParameter("@RMSN_STTS_ID", model.IdStatusB)
            };

            // en caso de ser cancelacion
            if (model.IdStatusB == 0) {
                CommandText = "UPDATE RMSN SET RMSN_STTS_ID = @RMSN_STTS_ID, RMSN_USR_C = @RMSN_USR_C, RMSN_FCCNCL = @RMSN_FCCNCL WHERE RMSN_ID = @RMSN_ID";
                sqlCommand.Add(new SugarParameter("@RMSN_USR_C", model.User));
                sqlCommand.Add(new SugarParameter("@RMSN_FCCNCL", model.FechaNuevo));
            }
            return this.Db.Ado.ExecuteCommand(CommandText, sqlCommand) > 0;
        }
    }
}
