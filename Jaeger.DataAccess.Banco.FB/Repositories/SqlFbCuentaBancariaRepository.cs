﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    /// <summary>
    /// repositorio de cuentas bancarias
    /// </summary>
    public class SqlFbCuentaBancariaRepository : RepositoryMaster<BancoCuentaModel>, ISqlCuentaBancariaRepository {
        public SqlFbCuentaBancariaRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(BancoCuentaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO bnccta (bnccta_id, bnccta_a, bnccta_e, bnccta_dcrt, bnccta_nochq, bnccta_sldini, bnccta_clvb, bnccta_clv, bnccta_rfcb, bnccta_rfc, bnccta_numcli, bnccta_clabe, bnccta_numcta, bnccta_scrsl, bnccta_func, bnccta_plaza, bnccta_tel, bnccta_alias, bnccta_cnta, bnccta_banco, bnccta_benef, bnccta_fecape, bnccta_fn, bnccta_usr_n) 
                    			           VALUES (@bnccta_id,@bnccta_a,@bnccta_e,@bnccta_dcrt,@bnccta_nochq,@bnccta_sldini,@bnccta_clvb,@bnccta_clv,@bnccta_rfcb,@bnccta_rfc,@bnccta_numcli,@bnccta_clabe,@bnccta_numcta,@bnccta_scrsl,@bnccta_func,@bnccta_plaza,@bnccta_tel,@bnccta_alias,@bnccta_cnta,@bnccta_banco,@bnccta_benef,@bnccta_fecape,@bnccta_fn,@bnccta_usr_n)
                                returning bnccta_id"
            };
            sqlCommand.Parameters.AddWithValue("@bnccta_id", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@bnccta_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bnccta_e", item.Extranjero);
            sqlCommand.Parameters.AddWithValue("@bnccta_dcrt", item.DiaCorte);
            sqlCommand.Parameters.AddWithValue("@bnccta_nochq", item.NoCheque);
            sqlCommand.Parameters.AddWithValue("@bnccta_sldini", item.SaldoInicial);
            sqlCommand.Parameters.AddWithValue("@bnccta_clvb", item.ClaveBanco);
            sqlCommand.Parameters.AddWithValue("@bnccta_clv", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@bnccta_rfcb", item.RFCBenficiario);
            sqlCommand.Parameters.AddWithValue("@bnccta_rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@bnccta_numcli", item.NumCliente);
            sqlCommand.Parameters.AddWithValue("@bnccta_clabe", item.CLABE);
            sqlCommand.Parameters.AddWithValue("@bnccta_numcta", item.NumCuenta);
            sqlCommand.Parameters.AddWithValue("@bnccta_scrsl", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@bnccta_func", item.Funcionario);
            sqlCommand.Parameters.AddWithValue("@bnccta_plaza", item.Plaza);
            sqlCommand.Parameters.AddWithValue("@bnccta_tel", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@bnccta_alias", item.Alias);
            sqlCommand.Parameters.AddWithValue("@bnccta_cnta", item.CuentaContable);
            sqlCommand.Parameters.AddWithValue("@bnccta_banco", item.Banco);
            sqlCommand.Parameters.AddWithValue("@bnccta_benef", item.Beneficiario);
            sqlCommand.Parameters.AddWithValue("@bnccta_fecape", item.FechaApertura);
            sqlCommand.Parameters.AddWithValue("@bnccta_fn", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@bnccta_usr_n", item.Creo);
            item.IdCuenta = this.ExecuteScalar(sqlCommand);
            if (item.IdCuenta > 0)
                return item.IdCuenta;
            return 0;
        }

        public int Update(BancoCuentaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE bnccta 
                            SET bnccta_a = @bnccta_a, bnccta_e = @bnccta_e, bnccta_dcrt = @bnccta_dcrt, bnccta_nochq = @bnccta_nochq, bnccta_sldini = @bnccta_sldini, bnccta_clvb = @bnccta_clvb, bnccta_clv = @bnccta_clv, bnccta_rfcb = @bnccta_rfcb, bnccta_rfc = @bnccta_rfc, bnccta_numcli = @bnccta_numcli, bnccta_clabe = @bnccta_clabe, bnccta_numcta = @bnccta_numcta, bnccta_scrsl = @bnccta_scrsl, bnccta_func = @bnccta_func, bnccta_plaza = @bnccta_plaza, bnccta_tel = @bnccta_tel, bnccta_alias = @bnccta_alias, bnccta_cnta = @bnccta_cnta, bnccta_banco = @bnccta_banco, bnccta_benef = @bnccta_benef, bnccta_fecape = @bnccta_fecape, bnccta_fm = @bnccta_fm, bnccta_usr_m = @bnccta_usr_m
                            WHERE bnccta_id = @bnccta_id"
            };
            sqlCommand.Parameters.AddWithValue("@bnccta_id", item.IdCuenta);
            sqlCommand.Parameters.AddWithValue("@bnccta_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bnccta_e", item.Extranjero);
            sqlCommand.Parameters.AddWithValue("@bnccta_dcrt", item.DiaCorte);
            sqlCommand.Parameters.AddWithValue("@bnccta_nochq", item.NoCheque);
            sqlCommand.Parameters.AddWithValue("@bnccta_sldini", item.SaldoInicial);
            sqlCommand.Parameters.AddWithValue("@bnccta_clvb", item.ClaveBanco);
            sqlCommand.Parameters.AddWithValue("@bnccta_clv", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@bnccta_rfcb", item.RFCBenficiario);
            sqlCommand.Parameters.AddWithValue("@bnccta_rfc", item.RFC);
            sqlCommand.Parameters.AddWithValue("@bnccta_numcli", item.NumCliente);
            sqlCommand.Parameters.AddWithValue("@bnccta_clabe", item.CLABE);
            sqlCommand.Parameters.AddWithValue("@bnccta_numcta", item.NumCuenta);
            sqlCommand.Parameters.AddWithValue("@bnccta_scrsl", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@bnccta_func", item.Funcionario);
            sqlCommand.Parameters.AddWithValue("@bnccta_plaza", item.Plaza);
            sqlCommand.Parameters.AddWithValue("@bnccta_tel", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@bnccta_alias", item.Alias);
            sqlCommand.Parameters.AddWithValue("@bnccta_cnta", item.CuentaContable);
            sqlCommand.Parameters.AddWithValue("@bnccta_banco", item.Banco);
            sqlCommand.Parameters.AddWithValue("@bnccta_benef", item.Beneficiario);
            sqlCommand.Parameters.AddWithValue("@bnccta_fecape", item.FechaApertura);
            sqlCommand.Parameters.AddWithValue("@bnccta_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@bnccta_usr_m", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCCTA SET BNCCTA_A = 0 WHERE BNCCTA_ID = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteScalar(sqlCommand) > 0;
        }

        public IEnumerable<BancoCuentaModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM bnccta")
            };

            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<BancoCuentaModel>();
            return mapper.Map(tabla).ToList();
        }

        BancoCuentaModel IGenericRepository<BancoCuentaModel>.GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM BNCCTA WHERE BNCCTA_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<BancoCuentaModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }
        #endregion

        public BancoCuentaDetailModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 BNCCTA.* FROM BNCCTA WHERE BNCCTA_ID = @index;"
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper<BancoCuentaDetailModel>(sqlCommand).FirstOrDefault();
        }

        /// <summary>
        /// obtener listado de cuentas bancarias
        /// </summary>
        /// <param name="onlyActive">solo activos</param>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM bnccta @condiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IEnumerable<IBancoCuentaDetailModel> GetList(bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM bnccta "
            };

            if (onlyActive == true) {
                sqlCommand.CommandText = sqlCommand.CommandText + "WHERE bnccta_a = 1";
            }
            return this.GetMapper<BancoCuentaDetailModel>(sqlCommand);
        }

        /// <summary>
        /// almacenar cuenta bancaria
        /// </summary>
        /// <param name="model">modelo</param>
        public IBancoCuentaDetailModel Save(IBancoCuentaDetailModel model) {
            if (model.IdCuenta == 0) {
                model.IdCuenta = this.Insert((BancoCuentaDetailModel)model);
            } else {
                this.Update((BancoCuentaDetailModel)model);
            }
            return model;
        }

        public void Create() {
            throw new NotImplementedException();
        }
    }
}
