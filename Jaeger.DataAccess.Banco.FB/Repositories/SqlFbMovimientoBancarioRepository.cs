﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    public class SqlFbMovimientoBancarioRepository : RepositoryMaster<MovimientoBancarioModel>, ISqlMovimientoBancarioRepository {
        protected ISqlMovmientoComprobanteRepository comprobanteRepository;
        protected ISqlMovimientoAuxiliarRepository auxiliarRepository;

        public SqlFbMovimientoBancarioRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.auxiliarRepository = new SqlFbMovimientoAuxiliarRepository(configuracion, user);
            this.comprobanteRepository = new SqlFbMovimientoComprobanteRepository(configuracion, user);
            this.User = user;
        }

        #region CRUD
        public int Insert(MovimientoBancarioModel item) {
            var sqlCommand = new FbCommand() {
                CommandText = @"INSERT INTO bncmov  
           ( bncmov_id, bncmov_a, bncmov_stts_id, bncmov_tipo_id, bncmov_oper_id, bncmov_cncp_id, bncmov_frmt_id, bncmov_ascta, bncmov_ascom, bncmov_drctr_id, bncmov_ctae_id, bncmov_ctar_id, bncmov_por, bncmov_noiden, bncmov_numctap, bncmov_clvbp, bncmov_fecems, bncmov_fecdoc, bncmov_fcvnc, bncmov_fccbr, bncmov_fclib, bncmov_benf, bncmov_rfcr, bncmov_clvbt, bncmov_bancot, bncmov_numctat, bncmov_clabet, bncmov_scrslt, bncmov_clvfrm, bncmov_frmpg, bncmov_nodocto, bncmov_desc, bncmov_ref, bncmov_numauto, bncmov_numope, bncmov_refnum, bncmov_tpcmb, bncmov_cargo, bncmov_abono, bncmov_clvmnd, bncmov_nota, bncmov_fn, bncmov_usr_n, bncmov_json) 
VALUES     (@bncmov_id,@bncmov_a,@bncmov_stts_id,@bncmov_tipo_id,@bncmov_oper_id,@bncmov_cncp_id,@bncmov_frmt_id,@bncmov_ascta,@bncmov_ascom,@bncmov_drctr_id,@bncmov_ctae_id,@bncmov_ctar_id,@bncmov_por,@bncmov_noiden,@bncmov_numctap,@bncmov_clvbp,@bncmov_fecems,@bncmov_fecdoc,@bncmov_fcvnc,@bncmov_fccbr,@bncmov_fclib,@bncmov_benf,@bncmov_rfcr,@bncmov_clvbt,@bncmov_bancot,@bncmov_numctat,@bncmov_clabet,@bncmov_scrslt,@bncmov_clvfrm,@bncmov_frmpg,@bncmov_nodocto,@bncmov_desc,@bncmov_ref,@bncmov_numauto,@bncmov_numope,@bncmov_refnum,@bncmov_tpcmb,@bncmov_cargo,@bncmov_abono,@bncmov_clvmnd,@bncmov_nota,@bncmov_fn,@bncmov_usr_n,@bncmov_json) returning bncmov_id;"
            };
            //item.Id = this.Max("bncmov_id");
            sqlCommand.Parameters.AddWithValue("@bncmov_id", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@bncmov_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bncmov_stts_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@bncmov_tipo_id", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@bncmov_oper_id", item.IdTipoOperacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_cncp_id", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@bncmov_frmt_id", item.IdFormato);
            sqlCommand.Parameters.AddWithValue("@bncmov_ascta", item.AfectaSaldoCuenta);
            sqlCommand.Parameters.AddWithValue("@bncmov_ascom", item.AfectaSaldoComprobantes);
            sqlCommand.Parameters.AddWithValue("@bncmov_drctr_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@bncmov_ctae_id", item.IdCuentaP);
            sqlCommand.Parameters.AddWithValue("@bncmov_ctar_id", item.IdCuentaT);
            sqlCommand.Parameters.AddWithValue("@bncmov_por", item.PorComprobar);
            sqlCommand.Parameters.AddWithValue("@bncmov_noiden", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@bncmov_numctap", item.NumeroCuentaP);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvbp", item.ClaveBancoP);
            sqlCommand.Parameters.AddWithValue("@bncmov_fecems", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@bncmov_fecdoc", item.FechaDocto);
            sqlCommand.Parameters.AddWithValue("@bncmov_fcvnc", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@bncmov_fccbr", item.FechaAplicacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_fclib", item.FechaBoveda);
            sqlCommand.Parameters.AddWithValue("@bncmov_benf", item.BeneficiarioT);
            sqlCommand.Parameters.AddWithValue("@bncmov_rfcr", item.BeneficiarioRFCT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvbt", item.ClaveBancoT);
            sqlCommand.Parameters.AddWithValue("@bncmov_bancot", item.BancoT);
            sqlCommand.Parameters.AddWithValue("@bncmov_numctat", item.NumeroCuentaT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clabet", item.CuentaCLABET);
            sqlCommand.Parameters.AddWithValue("@bncmov_scrslt", item.SucursalT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvfrm", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@bncmov_frmpg", item.FormaPagoDescripcion);
            sqlCommand.Parameters.AddWithValue("@bncmov_nodocto", item.NumDocto);
            sqlCommand.Parameters.AddWithValue("@bncmov_desc", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@bncmov_ref", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@bncmov_numauto", item.NumAutorizacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_numope", item.NumOperacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_refnum", item.ReferenciaNumerica);
            sqlCommand.Parameters.AddWithValue("@bncmov_tpcmb", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@bncmov_cargo", item.Cargo);
            sqlCommand.Parameters.AddWithValue("@bncmov_abono", item.Abono);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvmnd", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@bncmov_nota", item.Nota);
            sqlCommand.Parameters.AddWithValue("@bncmov_fn", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@bncmov_usr_n", item.Creo);
            sqlCommand.Parameters.AddWithValue("@bncmov_json", Domain.Base.Services.StringCompression.Compress(item.InfoAuxiliar));

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(MovimientoBancarioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE bncmov SET bncmov_a=@bncmov_a,bncmov_stts_id=@bncmov_stts_id,bncmov_tipo_id=@bncmov_tipo_id,bncmov_oper_id=@bncmov_oper_id,bncmov_cncp_id=@bncmov_cncp_id,bncmov_frmt_id=@bncmov_frmt_id,bncmov_ascta=@bncmov_ascta,bncmov_ascom=@bncmov_ascom,bncmov_drctr_id=@bncmov_drctr_id,bncmov_ctae_id=@bncmov_ctae_id,bncmov_ctar_id=@bncmov_ctar_id,
            bncmov_por=@bncmov_por,bncmov_noiden=@bncmov_noiden,bncmov_numctap=@bncmov_numctap,bncmov_clvbp=@bncmov_clvbp,bncmov_fecems=@bncmov_fecems,bncmov_fecdoc=@bncmov_fecdoc,bncmov_fcvnc=@bncmov_fcvnc,bncmov_fccbr=@bncmov_fccbr,bncmov_fclib=@bncmov_fclib,bncmov_fccncl=@bncmov_fccncl,
            bncmov_benf=@bncmov_benf,bncmov_rfcr=@bncmov_rfcr,bncmov_clvbt=@bncmov_clvbt,bncmov_bancot=@bncmov_bancot,bncmov_numctat=@bncmov_numctat,bncmov_clabet=@bncmov_clabet,bncmov_scrslt=@bncmov_scrslt,bncmov_clvfrm=@bncmov_clvfrm,bncmov_frmpg=@bncmov_frmpg,bncmov_nodocto=@bncmov_nodocto,
            bncmov_desc=@bncmov_desc,bncmov_ref=@bncmov_ref,bncmov_numauto=@bncmov_numauto,bncmov_numope=@bncmov_numope,bncmov_refnum=@bncmov_refnum,bncmov_tpcmb=@bncmov_tpcmb,bncmov_cargo=@bncmov_cargo,bncmov_abono=@bncmov_abono,bncmov_clvmnd=@bncmov_clvmnd,bncmov_nota=@bncmov_nota,
            bncmov_usr_c=@bncmov_usr_c,bncmov_usr_a=@bncmov_usr_a,bncmov_fm=@bncmov_fm,bncmov_usr_m=@bncmov_usr_m,bncmov_json=@bncmov_json  WHERE bncmov_id=@bncmov_id"
            };
            sqlCommand.Parameters.AddWithValue("@bncmov_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@bncmov_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bncmov_stts_id", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@bncmov_tipo_id", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@bncmov_oper_id", item.IdTipoOperacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_cncp_id", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@bncmov_frmt_id", item.IdFormato);
            sqlCommand.Parameters.AddWithValue("@bncmov_ascta", item.AfectaSaldoCuenta);
            sqlCommand.Parameters.AddWithValue("@bncmov_ascom", item.AfectaSaldoComprobantes);
            sqlCommand.Parameters.AddWithValue("@bncmov_drctr_id", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@bncmov_ctae_id", item.IdCuentaP);
            sqlCommand.Parameters.AddWithValue("@bncmov_ctar_id", item.IdCuentaT);
            sqlCommand.Parameters.AddWithValue("@bncmov_por", item.PorComprobar);
            sqlCommand.Parameters.AddWithValue("@bncmov_noiden", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@bncmov_numctap", item.NumeroCuentaT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvbp", item.ClaveBancoP);
            sqlCommand.Parameters.AddWithValue("@bncmov_fecems", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@bncmov_fecdoc", item.FechaDocto);
            sqlCommand.Parameters.AddWithValue("@bncmov_fcvnc", item.FechaVence);
            sqlCommand.Parameters.AddWithValue("@bncmov_fccbr", item.FechaAplicacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_fclib", item.FechaBoveda);
            sqlCommand.Parameters.AddWithValue("@bncmov_fccncl", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@bncmov_benf", item.BeneficiarioT);
            sqlCommand.Parameters.AddWithValue("@bncmov_rfcr", item.BeneficiarioRFCT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvbt", item.ClaveBancoT);
            sqlCommand.Parameters.AddWithValue("@bncmov_bancot", item.BancoT);
            sqlCommand.Parameters.AddWithValue("@bncmov_numctat", item.NumeroCuentaT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clabet", item.CuentaCLABET);
            sqlCommand.Parameters.AddWithValue("@bncmov_scrslt", item.SucursalT);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvfrm", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@bncmov_frmpg", item.FormaPagoDescripcion);
            sqlCommand.Parameters.AddWithValue("@bncmov_nodocto", item.NumDocto);
            sqlCommand.Parameters.AddWithValue("@bncmov_desc", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@bncmov_ref", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@bncmov_numauto", item.NumAutorizacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_numope", item.NumOperacion);
            sqlCommand.Parameters.AddWithValue("@bncmov_refnum", item.ReferenciaNumerica);
            sqlCommand.Parameters.AddWithValue("@bncmov_tpcmb", item.TipoCambio);
            sqlCommand.Parameters.AddWithValue("@bncmov_cargo", item.Cargo);
            sqlCommand.Parameters.AddWithValue("@bncmov_abono", item.Abono);
            sqlCommand.Parameters.AddWithValue("@bncmov_clvmnd", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@bncmov_nota", item.Nota);
            sqlCommand.Parameters.AddWithValue("@bncmov_usr_c", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@bncmov_usr_a", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@bncmov_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@bncmov_usr_m", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@bncmov_json", Domain.Base.Services.StringCompression.Compress(item.InfoAuxiliar));
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<MovimientoBancarioModel> GetList() {
            var sqlCommand = new FbCommand() { CommandText = "SELECT * FROM bncmov" };
            return this.GetMapper(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM bncmov WHERE bncmov_id IN (@index) "
            };
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MovimientoBancarioModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "select first 1 * from bncmov where bncmov_id = @bncmov_id"
            };
            sqlCommand.Parameters.AddWithValue("@bncmov_id", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }
        #endregion

        /// <summary>
        /// actualizar status del moviminto
        /// </summary>
        /// <param name="idStatus">indice del nuevo status</param>
        /// <param name="index">indice del movimiento</param>
        /// <returns>verdadero si transaccion</returns>
        public bool Update(int idStatus, int index) {
            var _sqlCommand = new FbCommand {
                CommandText = "update bncmov set bncmov_stts_id=@bncmov_stts_id, bncmov_fm=@bncmov_fm, bncmov_usr_m=@bncmov_usr_m where bncmov_id=@bncmov_id;"
            };
            _sqlCommand.Parameters.AddWithValue("@bncmov_stts_id", idStatus);
            _sqlCommand.Parameters.AddWithValue("@bncmov_fm", DateTime.Now);
            _sqlCommand.Parameters.AddWithValue("@bncmov_usr_m", this.User);
            _sqlCommand.Parameters.AddWithValue("@bncmov_id", index);
            return this.ExecuteTransaction(_sqlCommand) > 0;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(MovimientoBancarioDetailModel)) {
                var sqlCommand = new FbCommand { CommandText = @"SELECT BNCMOV.*, BNCCTA.* FROM BNCMOV LEFT JOIN BNCCTA ON BNCMOV.BNCMOV_CTAE_ID = BNCCTA.BNCCTA_ID @condiciones" };
                var result = this.GetMapper<T1>(sqlCommand, conditionals).ToList();
                if (result.Count > 0) {
                    var indexs = (result as List<MovimientoBancarioDetailModel>).Select((MovimientoBancarioDetailModel it) => it.Id).ToArray<int>();
                    var sql1 = "SELECT * FROM BNCCMP WHERE BNCCMP_SUBID IN (@indexs) AND BNCCMP_A > 0";
                    sql1 = sql1.Replace("@indexs", string.Join(",", indexs));
                    var resul1 = this.GetMapper<MovimientoBancarioComprobanteDetailModel>(sql1).ToList();
                    for (int i = 0; i < result.Count(); i++) {
                        var d0 = resul1.Where(it => it.IdMovimiento == (result as List<MovimientoBancarioDetailModel>)[i].Id).ToArray();
                        if (d0.Count() > 0) {
                            (result as List<MovimientoBancarioDetailModel>)[i].Comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>(
                                resul1.Where(it => it.IdMovimiento == (result as List<MovimientoBancarioDetailModel>)[i].Id).ToList<MovimientoBancarioComprobanteDetailModel>()
                                );
                        }
                    }
                    return result;
                }
            } else if (typeof(T1) == typeof(MovimientoComprobanteModel) | typeof(T1) == typeof(MovimientoComprobanteMergeModelView)) {
                var sql = new FbCommand { CommandText = @"SELECT BNCCMP.*, BNCMOV.* FROM BNCCMP LEFT JOIN BNCMOV ON BNCCMP.BNCCMP_SUBID = BNCMOV.BNCMOV_ID @condiciones" };
                return base.GetMapper<T1>(sql, conditionals);
            } else if (typeof(T1) == typeof(MovimientoBancarioComprobanteDetailModel)) {
                var isRemision = conditionals.Where(it => it.FieldName.ToLower().Contains("rmsn")).Count() > 0;
                if (isRemision == false) {
                    var sql = this.GetQueryComprobante();
                    return base.GetMapper<T1>(sql, conditionals);
                } else {
                    var sql = this.GetQueryRemision();
                    return base.GetMapper<T1>(sql, conditionals);
                }
            }
            return new List<T1>();
        }

        public bool Aplicar(IMovimientoBancarioDetailModel model) {
            var sqlCommand = new FbCommand() { CommandText = "update bncmov set bncmov_stts_id=@idstatus, bncmov_fm=@fecha, bncmov_usr_m=@modifica where bncmov_id =@index;" };
            sqlCommand.Parameters.AddWithValue("@index", model.Id);
            sqlCommand.Parameters.AddWithValue("@fecha", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@modifica", this.User);
            sqlCommand.Parameters.AddWithValue("@idstatus", model.IdStatus);

            // en caso de que el movimiento sea cancelado
            if (model.Status == MovimientoBancarioStatusEnum.Cancelado) {
                sqlCommand = new FbCommand() { CommandText = "update bncmov set bncmov_stts_id=@idstatus, bncmov_fm=@fecha, bncmov_usr_m=@modifica, BNCMOV_FCCNCL=@fecha2, BNCMOV_USR_C=@cancela where bncmov_id =@index;" };
                sqlCommand.Parameters.AddWithValue("@index", model.Id);
                sqlCommand.Parameters.AddWithValue("@fecha", DateTime.Now);
                sqlCommand.Parameters.AddWithValue("@fecha2", DateTime.Now);
                sqlCommand.Parameters.AddWithValue("@modifica", this.User);
                sqlCommand.Parameters.AddWithValue("@cancela", this.User);
                sqlCommand.Parameters.AddWithValue("@idstatus", model.IdStatus);
            }

            if (this.ExecuteTransaction(sqlCommand) > 0) {
                if (model.AfectaSaldoComprobantes) {
                    if (this.Aplicar(model.Id) == false) {
                        Services.LogErrorService.LogWrite("No se actualizaron las partidas: " + model.Identificador);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// TODO: NECESITAS REVISAR ESTE PROCEDIMIENTO
        /// PARA EL CASO DE LAS REMISIONES DE BOLSA DE REGALO UTILIZAMOS EL VALOR DEL CAMPO RMSN_XCBRR EN VEZ DE RMSN_TOTAL DE ESTA FORMA CONSIDERAMOS EL CAMBIO DE STATUS DE LA REMISION A COBRADO
        /// </summary>
        public bool Aplicar(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT a.bncmov_tipo_id, a.bncmov_ascom afecta, b.bnccmp_tipo as documento, b.bnccmp_idcom as idcomprobante, b.bnccmp_efecto as efecto, b.bnccmp_uuid as uuid, r.RMSN_TOTAL as total, b.bnccmp_total as total1, r.RMSN_XCBRR AS XCOBRAR, b.bnccmp_sbtp, r.RMSN_STTS_ID as IdStatus,
                        (SELECT SUM(c.bnccmp_cargo) FROM bnccmp c, bncmov d where c.bnccmp_subid = d.bncmov_id AND c.bnccmp_idcom = b.bnccmp_idcom AND (d.bncmov_stts_id = @status1 OR d.bncmov_stts_id = @status2)) AS cargo,
                        (SELECT SUM(c.bnccmp_abono) FROM bnccmp c, bncmov d where c.bnccmp_subid = d.bncmov_id AND c.bnccmp_idcom = b.bnccmp_idcom AND (d.bncmov_stts_id = @status1 OR d.bncmov_stts_id = @status2)) AS abono,
                        (SELECT MAX(d.bncmov_fccbr) FROM bnccmp c, bncmov d where c.bnccmp_subid = d.bncmov_id AND c.bnccmp_idcom = b.bnccmp_idcom AND (d.bncmov_stts_id = @status1 OR d.bncmov_stts_id = @status2)) AS aplicacion
                        FROM bncmov a, bnccmp b, rmsn r
                        WHERE b.bnccmp_subid = a.bncmov_id
                        and b.BNCCMP_IDCOM = r.RMSN_ID
                        AND a.bncmov_id = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", id);
            sqlCommand.Parameters.AddWithValue("@status1", (int)MovimientoBancarioStatusEnum.Aplicado);
            sqlCommand.Parameters.AddWithValue("@status2", (int)MovimientoBancarioStatusEnum.Auditado);
            var result = this.GetMapper<MovimientoBancarioFactory>(sqlCommand).ToList();
            int contador = 0;
            if (result.Count > 0) {
                foreach (var item in result) {
                    if (item.TipoDocumento == MovimientoBancarioTipoComprobanteEnum.Remision) {
                        try {
                            if (item.AfectaSaldo) {
                                var sqlCommand2 = new FbCommand {
                                    CommandText = @"update RMSN set RMSN_FECUPC=@fecha1, RMSN_STTS_ID=@idstatus, RMSN_CBRD=@acumulado where RMSN_ID=@index;"
                                };

                                sqlCommand2.Parameters.AddWithValue("@fecha1", item.FechaAplicacion);
                                sqlCommand2.Parameters.AddWithValue("@idstatus", item.Status);
                                sqlCommand2.Parameters.AddWithValue("@acumulado", item.Acumulado);
                                sqlCommand2.Parameters.AddWithValue("@index", item.IdComprobante);
                                this.ExecuteTransaction(sqlCommand2);
                            }
                        } catch (Exception ex) {
                            Services.LogErrorService.LogWrite(ex.Message + "| No se actualizaron las partidas: " + item.IdDocumento);
                        }
                    }
                    contador++;
                }
            }
            return result.Count == contador;
        }

        /// <summary>
        /// obtener listado de movimientos bancarios por condicionantes
        /// </summary>
        public IEnumerable<IMovimientoBancarioDetailModel> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"select bncmov.*, bnccta.* from bncmov left join bnccta on bncmov.bncmov_ctae_id = bnccta.bnccta_id @condiciones"
            };

            if (conditionals.Count > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
                var result = this.GetMapper<MovimientoBancarioDetailModel>(sqlCommand).ToList();

                var indexs = result.Select(it => it.Id).ToArray();
                if (indexs.Count() > 0) {
                    var sqlc = new FbCommand { CommandText = "select * from bnccmp where BNCCMP_SUBID in (@indexs) and BNCCMP_A > 0" };
                    sqlc.CommandText = sqlc.CommandText.Replace("@indexs", string.Join(",", indexs));
                    var resul1 = this.GetMapper<MovimientoBancarioComprobanteDetailModel>(sqlc).ToList();

                    for (int i = 0; i < result.Count(); i++) {
                        var d0 = resul1.Where(it => it.IdMovimiento == result[i].Id).ToArray();
                        result[i].Comprobantes = new BindingList<MovimientoBancarioComprobanteDetailModel>(
                            resul1.Where(it => it.IdMovimiento == result[i].Id).ToList<MovimientoBancarioComprobanteDetailModel>()
                            );
                    }
                }
                return result;
            }
            return new List<MovimientoBancarioDetailModel>();
        }

        public IMovimientoBancarioDetailModel Save(IMovimientoBancarioDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Identificador = this.NoIndetificacion(model);
                // actualizar el identificador antes de generar el json
                if (model.Comprobantes.Count > 0) {
                    for (int i = 0; i < model.Comprobantes.Count; i++) {
                        model.Comprobantes[i].Identificador = model.Identificador;
                    }
                }
                model.InfoAuxiliar = model.Json();
                model.Id = this.Insert((MovimientoBancarioDetailModel)model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.InfoAuxiliar = model.Json();
                this.Update((MovimientoBancarioDetailModel)model);
            }

            if (model.Comprobantes != null) {
                if (model.Comprobantes.Count > 0) {
                    for (int i = 0; i < model.Comprobantes.Count; i++) {
                        model.Comprobantes[i].IdMovimiento = model.Id;
                        model.Comprobantes[i].Identificador = model.Identificador;
                        model.Comprobantes[i] = this.comprobanteRepository.Save(model.Comprobantes[i]);
                    }
                }
            }

            if (model.Auxiliar != null) {
                if (model.Auxiliar.Count > 0) {
                    for (int i = 0; i < model.Auxiliar.Count; i++) {
                        if (model.Auxiliar[i].IdAuxiliar == 0) {
                            model.Auxiliar[i].IdMovimiento = model.Id;
                            model.Auxiliar[i] = this.auxiliarRepository.Save(model.Auxiliar[i]);
                        }
                    }
                }
            }
            return model;
        }

        public bool CreateTable() {
            throw new NotImplementedException();
        }

        public bool CreateTables() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// calcular el numero de indentifiacion de la prepoliza
        /// </summary>
        private string NoIndetificacion(IMovimientoBancarioDetailModel modelo) {
            int mes = modelo.FechaEmision.Month;
            int anio = modelo.FechaEmision.Year;
            int indice = 0;

            try {
                var sqlCommand = new FbCommand {
                    CommandText = @"SELECT COUNT(bncmov_fecems) AS maximus  FROM bncmov  WHERE (extract(Year from bncmov_fecems) = @Year1  AND (extract(Month from bncmov_fecems) = @Month3 )  AND (bncmov_tipo_id = @IdTipo4 ));"
                };
                sqlCommand.Parameters.AddWithValue("@Year1", anio);
                sqlCommand.Parameters.AddWithValue("@Month3", mes);
                sqlCommand.Parameters.AddWithValue("@IdTipo4", modelo.IdTipo);
                indice = this.ExecuteScalar(sqlCommand) + 1;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                indice = 0;
            }

            return string.Format("{0}{1}{2}",
                modelo.Tipo.ToString()[0],
                modelo.FechaEmision.ToString("yyMM"),
                indice.ToString("000#"));
        }

        private FbCommand GetQueryComprobante() {
            var sqlCommand = new FbCommand() {
                CommandText = @"SELECT BNCMOV.*, BNCCTA.* FROM BNCMOV LEFT JOIN BNCCTA ON BNCMOV.BNCMOV_CTAE_ID = BNCCTA.BNCCTA_ID @wcondiciones"
            };
            return sqlCommand;
        }

        private FbCommand GetQueryRemision() {
            var sqlCommand = new FbCommand() {
                CommandText = @"SELECT 
                            'Remision'          AS BNCCMP_TIPO,
                            RMSN.RMSN_VER       AS BNCCMP_VER,
                            'Ingreso'           AS BNCCMP_EFECTO, 
                            RMSN.RMSN_STTS_ID   AS CFDI_STATUS,
                            (CASE RMSN.RMSN_STTS_ID WHEN 0 THEN 'Cancelado' 
                                                           WHEN 1 THEN 'Pendiente' 
                                                           WHEN 2 THEN 'En Ruta...' 
                                                           WHEN 3 THEN 'Entregado' 
                                                           WHEN 4 THEN 'Por Cobrar' 
                                                           WHEN 5 THEN 'Cobrado' 
                                                           WHEN 6 THEN 'Facturado' 
                                                           WHEN 7 THEN 'Especial' END) BNCCMP_STATUS,
                            1                   AS BNCCMP_SBTP, 
                            RMSN.RMSN_ID        AS BNCCMP_IDCOM,
                            RMSN.RMSN_FOLIO     AS BNCCMP_FOLIO, 
                            RMSN.RMSN_SERIE     AS BNCCMP_SERIE,
                            RMSN.RMSN_RFCE      AS BNCCMP_RFCE,
                            CONF.CONF_DATA      AS BNCCMP_EMISOR,  
                            RMSN.RMSN_RFCR      AS BNCCMP_RFCR,
                            RMSN.RMSN_NOMR      AS BNCCMP_RECEPTOR,
                            RMSN.RMSN_FECEMS    AS BNCCMP_FECEMS,
                            RMSN.RMSN_MTDPG     AS BNCCMP_MTDPG,
                            RMSN.RMSN_FRMPG     AS BNCCMP_FRMPG, 
                            RMSN.RMSN_MONEDA    AS BNCCMP_MONEDA,
                            RMSN.RMSN_UUID      AS BNCCMP_UUID,
                            0                   AS BNCCMP_PAR,
                            RMSN.RMSN_TOTAL     AS BNCCMP_TOTAL1,
                            RMSN.RMSN_CBRD      AS BNCCMP_CBRD,
                            RMSN.RMSN_XCBRR     AS BNCCMP_TOTAL,
                            RMSN.RMSN_XCBRR     AS BNCCMP_XCBR,
                            (CASE RMSN.RMSN_STTS_ID WHEN 0 THEN 'Cancelado' ELSE 'Vigente' END) AS BNCCMP_ESTADO
                            FROM RMSN
                            LEFT JOIN CONF ON CONF.CONF_KEY = @nombreComercial
                            @condiciones ORDER BY RMSN.RMSN_FECEMS DESC"
            };
            // reemplazar nombre comercial
            var nombreComercial = (int)Domain.Empresa.Enums.ConfigKeyEnum.NombreComercial;
            sqlCommand.CommandText = sqlCommand.CommandText.Replace("@nombreComercial", nombreComercial.ToString());
            return sqlCommand;
        }
    }
}
