﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    public class SqlFbCuentaSaldoRepository : RepositoryMaster<BancoCuentaSaldoModel>, ISqlCuentaSaldoRepository {
        public SqlFbCuentaSaldoRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }
        
        #region CRUD
        public int Insert(BancoCuentaSaldoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO BNCSLD (BNCSLD_ID, BNCSLD_SBID, BNCSLD_ANIO, BNCSLD_MES, BNCSLD_INICIAL, BNCSLD_FINAL, BNCSLD_FS, BNCSLD_FN, BNCSLD_USR_N, BNCSLD_NOTA)
                                           VALUES (@BNCSLD_ID,@BNCSLD_SBID,@BNCSLD_ANIO,@BNCSLD_MES,@BNCSLD_INICIAL,@BNCSLD_FINAL,@BNCSLD_FS,@BNCSLD_FN,@BNCSLD_USR_N,@BNCSLD_NOTA) returning BNCSLD_ID"
            };
            sqlCommand.Parameters.AddWithValue("@BNCSLD_ID", item.IdSaldo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_SBID", item.IdCuenta);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_ANIO", item.Ejercicio);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_MES", item.Periodo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_INICIAL", item.SaldoInicial);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FINAL", item.SaldoFinal);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FS", item.FechaInicial);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_USR_N", this.User);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_NOTA", item.Nota);

            item.IdSaldo = this.ExecuteScalar(sqlCommand);
            if (item.IdSaldo > 0)
                return item.IdSaldo;
            return 0;
        }

        public int Update(BancoCuentaSaldoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCSLD BNCSLD_SBID = @BNCSLD_SBID, BNCSLD_ANIO = @BNCSLD_ANIO, BNCSLD_MES = @BNCSLD_MES, BNCSLD_INICIAL = @BNCSLD_INICIAL, BNCSLD_FINAL = @BNCSLD_FINAL, 
BNCSLD_FS = @BNCSLD_FS,BNCSLD_FN = @BNCSLD_FN,BNCSLD_USR_N = @BNCSLD_USR_N,BNCSLD_NOTA = @BNCSLD_NOTA WHERE BNCSLD_ID = @BNCSLD_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@BNCSLD_ID", item.IdSaldo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_SBID", item.IdCuenta);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_ANIO", item.Ejercicio);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_MES", item.Periodo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_INICIAL", item.SaldoInicial);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FINAL", item.SaldoFinal);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FS", item.FechaInicial);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_USR_N", this.User);
            sqlCommand.Parameters.AddWithValue("@BNCSLD_NOTA", DateTime.Now);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"delete from BNCSLD where BNCSLD_ID = @BNCSLD_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@BNCSLD_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public BancoCuentaSaldoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select BNCSLD.* from BNCSLD where BNCSLD_ID = @BNCSLD_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@BNCSLD_ID", index);
            return this.GetMapper<BancoCuentaSaldoModel>(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<BancoCuentaSaldoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select BNCSLD.* from BNCSLD"
            };
            return this.GetMapper<BancoCuentaSaldoModel>(sqlCommand).ToList();
        }
        #endregion

        #region metodos personalidados
        public BancoCuentaSaldoDetailModel GetSaldo(BancoCuentaModel objeto, DateTime fecha) {
            var sqlCommand = new FbCommand {
                CommandText = @"select first 1 bncsld.* from bncsld where bncsld_mes = @bncsld_mes and bncsld_anio=@bncsld_anio and bncsld_id=@bncsld_id"
            };
            int mes = fecha.Month;
            int anio = fecha.Year;
            sqlCommand.Parameters.AddWithValue("@bncsld_id", objeto.IdCuenta);
            sqlCommand.Parameters.AddWithValue("@bncsld_mes", mes);
            sqlCommand.Parameters.AddWithValue("@bncsld_anio", anio);
            var result = this.GetMapper<BancoCuentaSaldoDetailModel>(sqlCommand).FirstOrDefault();
            if (result == null) {
                return new BancoCuentaSaldoDetailModel();
            } else {
                return result;
            }
        }

        /// <summary>
        /// obtener saldo de una cuenta bancaria
        /// </summary>
        public BancoCuentaSaldoDetailModel GetSaldo(int idCuenta, DateTime fecha) {
            var sqlCommand = new FbCommand {
                CommandText = @"select first 1 bncsld.* from bncsld where bncsld_mes = @bncsld_mes and bncsld_anio=@bncsld_anio and bncsld_id=@bncsld_id"
            };
            int mes = fecha.Month;
            int anio = fecha.Year;
            sqlCommand.Parameters.AddWithValue("@bncsld_id", idCuenta);
            sqlCommand.Parameters.AddWithValue("@bncsld_mes", mes);
            sqlCommand.Parameters.AddWithValue("@bncsld_anio", anio);
            var result = this.GetMapper<BancoCuentaSaldoDetailModel>(sqlCommand).FirstOrDefault();

            if (result == null) {
                return new BancoCuentaSaldoDetailModel();
            } else {
                return result;
            }
        }

        public BancoCuentaSaldoDetailModel Save(BancoCuentaSaldoDetailModel model) {
            if (model.IdSaldo == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Ejercicio = model.FechaInicial.Year;
                model.Periodo = model.FechaInicial.Month;
                model.IdSaldo = this.Insert(model);
            } else {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Ejercicio = model.FechaInicial.Year;
                model.Periodo = model.FechaInicial.Month;
                this.Update(model);
            }
            return model;
        }

        public void Create() {
            //this.CreateTable();
        }
        #endregion
    }
}
