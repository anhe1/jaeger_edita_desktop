﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    /// <summary>
    /// repositorio de auxiliares de movimientos bancarios
    /// </summary>
    public class SqlFbMovimientoComprobanteRepository : RepositoryMaster<MovimientoBancarioComprobanteModel>, ISqlMovmientoComprobanteRepository {

        public SqlFbMovimientoComprobanteRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(MovimientoBancarioComprobanteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO BNCCMP 
                    ( BNCCMP_ID, BNCCMP_A, BNCCMP_SUBID, BNCCMP_NOIDEN, BNCCMP_VER, BNCCMP_EFECTO, BNCCMP_SBTP, BNCCMP_TIPO, BNCCMP_IDCOM, BNCCMP_FOLIO, BNCCMP_SERIE, BNCCMP_RFCE, BNCCMP_EMISOR, BNCCMP_RFCR, BNCCMP_RECEPTOR, BNCCMP_FECEMS, BNCCMP_MTDPG, BNCCMP_FRMPG, BNCCMP_MONEDA, BNCCMP_UUID, BNCCMP_PAR, BNCCMP_TOTAL, BNCCMP_XCBR, BNCCMP_CARGO, BNCCMP_ABONO, BNCCMP_CBRD, BNCCMP_ESTADO, BNCCMP_USR_N, BNCCMP_FN)
             VALUES (@BNCCMP_ID,@BNCCMP_A,@BNCCMP_SUBID,@BNCCMP_NOIDEN,@BNCCMP_VER,@BNCCMP_EFECTO,@BNCCMP_SBTP,@BNCCMP_TIPO,@BNCCMP_IDCOM,@BNCCMP_FOLIO,@BNCCMP_SERIE,@BNCCMP_RFCE,@BNCCMP_EMISOR,@BNCCMP_RFCR,@BNCCMP_RECEPTOR,@BNCCMP_FECEMS,@BNCCMP_MTDPG,@BNCCMP_FRMPG,@BNCCMP_MONEDA,@BNCCMP_UUID,@BNCCMP_PAR,@BNCCMP_TOTAL,@BNCCMP_XCBR,@BNCCMP_CARGO,@BNCCMP_ABONO,@BNCCMP_CBRD,@BNCCMP_ESTADO,@BNCCMP_USR_N,@BNCCMP_FN) RETURNING BNCCMP_ID;"
            };
            //item.Id = this.Max("BNCCMP_ID");
            sqlCommand.Parameters.AddWithValue("@BNCCMP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SUBID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_NOIDEN", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_EFECTO", item.TipoComprobanteText);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SBTP", item.IdSubTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_TIPO", item.TipoDocumentoText);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_IDCOM", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_EMISOR", item.EmisorNombre);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RECEPTOR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FRMPG", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_PAR", item.NumParcialidad);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_XCBR", item.PorCobrar);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_CARGO", item.Cargo);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_ABONO", item.Abono);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_CBRD", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_ESTADO", item.Estado);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FN", item.FechaNuevo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(MovimientoBancarioComprobanteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCCMP SET BNCCMP_A=@BNCCMP_A,BNCCMP_SUBID=@BNCCMP_SUBID,BNCCMP_NOIDEN=@BNCCMP_NOIDEN,BNCCMP_VER=@BNCCMP_VER,BNCCMP_EFECTO=@BNCCMP_EFECTO,BNCCMP_SBTP=@BNCCMP_SBTP,BNCCMP_TIPO=@BNCCMP_TIPO,BNCCMP_IDCOM=@BNCCMP_IDCOM,
BNCCMP_FOLIO=@BNCCMP_FOLIO,BNCCMP_SERIE=@BNCCMP_SERIE,BNCCMP_RFCE=@BNCCMP_RFCE,BNCCMP_EMISOR=@BNCCMP_EMISOR,BNCCMP_RFCR=@BNCCMP_RFCR,BNCCMP_RECEPTOR=@BNCCMP_RECEPTOR,BNCCMP_FECEMS=@BNCCMP_FECEMS,BNCCMP_MTDPG=@BNCCMP_MTDPG,
BNCCMP_FRMPG=@BNCCMP_FRMPG,BNCCMP_MONEDA=@BNCCMP_MONEDA,BNCCMP_UUID=@BNCCMP_UUID,BNCCMP_PAR=@BNCCMP_PAR,BNCCMP_TOTAL=@BNCCMP_TOTAL,BNCCMP_XCBR=@BNCCMP_XCBR,BNCCMP_CARGO=@BNCCMP_CARGO,BNCCMP_ABONO=@BNCCMP_ABONO,BNCCMP_CBRD=@BNCCMP_CBRD,BNCCMP_ESTADO=@BNCCMP_ESTADO,
BNCCMP_USR_M=@BNCCMP_USR_M,BNCCMP_FM=@BNCCMP_FM WHERE BNCCMP_ID=@BNCCMP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@BNCCMP_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SUBID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_NOIDEN", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_VER", item.Version);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_EFECTO", item.TipoComprobanteText);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SBTP", item.IdSubTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_TIPO", item.TipoDocumentoText);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_IDCOM", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RFCE", item.EmisorRFC);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_EMISOR", item.EmisorNombre);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RFCR", item.ReceptorRFC);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_RECEPTOR", item.ReceptorNombre);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FRMPG", item.ClaveFormaPago);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_MONEDA", item.ClaveMoneda);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_PAR", item.NumParcialidad);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_XCBR", item.PorCobrar);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_CARGO", item.Cargo);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_ABONO", item.Abono);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_CBRD", item.Acumulado);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_ESTADO", item.Estado);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@BNCCMP_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"Delete from BNCCMP where BNCCMP_ID in (@index);"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MovimientoBancarioComprobanteModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select first 1 BNCCMP.* from BNCCMP where BNCCMP_ID = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<MovimientoBancarioComprobanteModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select BNCCMP.* from BNCCMP"
            };

            return this.GetMapper(sqlCommand);
        }

        public MovimientoBancarioComprobanteDetailModel Save(MovimientoBancarioComprobanteDetailModel model) {
            if (model.Id == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Id = this.Insert((MovimientoBancarioComprobanteDetailModel)model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update((MovimientoBancarioComprobanteDetailModel)model);
            }
            return model;
        }

        public IEnumerable<MovimientoBancarioComprobanteDetailModel> Save(List<MovimientoBancarioComprobanteDetailModel> models) {
            for (int i = 0; i < models.Count(); i++) {
                models[i] = this.Save(models[i]);
            }
            return models;
        }

        public IEnumerable<MovimientoBancarioComprobanteDetailModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"select BNCCMP.* from BNCCMP @wcondiciones"
            };

            if (conditionals.Count() > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
                return this.GetMapper<MovimientoBancarioComprobanteDetailModel>(sqlCommand);
            }
            return new List<MovimientoBancarioComprobanteDetailModel>();
        }

        public void Create() {

        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT BNCCMP.*, BNCMOV.* FROM BNCCMP LEFT JOIN BNCMOV ON BNCCMP.BNCCMP_SUBID = BNCMOV.BNCMOV_ID @condiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
