﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    public class SqlFbBeneficiarioRepository : RepositoryMaster<BeneficiarioModel>, ISqlBeneficiarioRepository {
        public SqlFbBeneficiarioRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public BeneficiarioModel GetById(int index) {
            throw new NotImplementedException();
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE drctr SET drctr_a = 0 WHERE drctr_id = @id;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(BeneficiarioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO DRCTR ( DRCTR_ID, DRCTR_A, DRCTR_IDCLASS, DRCTR_DOC_ID, DRCTR_STTSDCS_ID, DRCTR_DRCTR_ID, DRCTR_CTLGESP_ID, DRCTR_CTLGCNTTC_ID, DRCTR_WEB_ID, DRCTR_DIR_ID, DRCTR_PRFL_ID, DRCTR_CTLGRGF_ID, DRCTR_SYNC_ID, DRCTR_DSCRDT, DRCTR_TIPO_ID, DRCTR_PRCS_ID, DRCTR_CTLGDSCNT_ID, DRCTR_LMTCRDT$, DRCTR_SBRCRDT$, DRCTR_CLV, DRCTR_CURP, DRCTR_IMSS, DRCTR_RFC, DRCTR_TLFN, DRCTR_MAIL, DRCTR_WEB, DRCTR_OBSR, DRCTR_PSW, DRCTR_NOM, DRCTR_FCHHCMNT, DRCTR_USU_N, DRCTR_FN, RCTR_FACTOR, DRCTR_DSPCT, DRCTR_FFCTR, DRCTR_CDD,  DRCTR_USR_ID, DRCTR_FILE, DRCTR_USOCFDI) 
                    			           VALUES (@DRCTR_ID,@DRCTR_A,@DRCTR_IDCLASS,@DRCTR_DOC_ID,@DRCTR_STTSDCS_ID,@DRCTR_DRCTR_ID,@DRCTR_CTLGESP_ID,@DRCTR_CTLGCNTTC_ID,@DRCTR_WEB_ID,@DRCTR_DIR_ID,@DRCTR_PRFL_ID,@DRCTR_CTLGRGF_ID,@DRCTR_SYNC_ID,@DRCTR_DSCRDT,@DRCTR_TIPO_ID,@DRCTR_PRCS_ID,@DRCTR_CTLGDSCNT_ID,@DRCTR_LMTCRDT$,@DRCTR_SBRCRDT$,@DRCTR_CLV,@DRCTR_CURP,@DRCTR_IMSS,@DRCTR_RFC,@DRCTR_TLFN,@DRCTR_MAIL,@DRCTR_WEB,@DRCTR_OBSR,@DRCTR_PSW,@DRCTR_NOM,@DRCTR_FCHHCMNT,@DRCTR_USU_N,@DRCTR_FN,@DRCTR_FACTOR,@DRCTR_DSPCT,@DRCTR_FFCTR,@DRCTR_CDD,@DRCTR_USR_ID,@DRCTR_FILE,@DRCTR_USOCFDI) RETURNING DRCTR_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_TLFN", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTR_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_FN", item.FechaNuevo);
            item.IdDirectorio = this.ExecuteScalar(sqlCommand);

            if (item.IdDirectorio > 0)
                return item.IdDirectorio;
            return 0;
        }

        public int Update(BeneficiarioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR 
                    			SET DRCTR_A = @DRCTR_A, DRCTR_IDCLASS = @DRCTR_IDCLASS, DRCTR_DOC_ID = @DRCTR_DOC_ID, DRCTR_STTSDCS_ID = @DRCTR_STTSDCS_ID, DRCTR_DRCTR_ID = @DRCTR_DRCTR_ID, DRCTR_CTLGESP_ID = @DRCTR_CTLGESP_ID, 
                                DRCTR_CTLGCNTTC_ID = @DRCTR_CTLGCNTTC_ID, DRCTR_WEB_ID = @DRCTR_WEB_ID, DRCTR_DIR_ID = @DRCTR_DIR_ID, DRCTR_PRFL_ID = @DRCTR_PRFL_ID, DRCTR_CTLGRGF_ID = @DRCTR_CTLGRGF_ID, DRCTR_SYNC_ID = @DRCTR_SYNC_ID, 
                                DRCTR_DSCRDT = @DRCTR_DSCRDT, DRCTR_TIPO_ID = @DRCTR_TIPO_ID, DRCTR_PRCS_ID = @DRCTR_PRCS_ID, DRCTR_CTLGDSCNT_ID = @DRCTR_CTLGDSCNT_ID, DRCTR_LMTCRDT$ = @DRCTR_LMTCRDT$, DRCTR_SBRCRDT$ = @DRCTR_SBRCRDT$, 
                                DRCTR_CLV = @DRCTR_CLV, DRCTR_CURP = @DRCTR_CURP, DRCTR_IMSS = @DRCTR_IMSS, DRCTR_RFC = @DRCTR_RFC, DRCTR_TLFN = @DRCTR_TLFN, 
                                DRCTR_MAIL = @DRCTR_MAIL, DRCTR_WEB = @DRCTR_WEB, DRCTR_OBSR = @DRCTR_OBSR, DRCTR_PSW = @DRCTR_PSW, DRCTR_NOM = @DRCTR_NOM, DRCTR_FCHHCMNT = @DRCTR_FCHHCMNT, DRCTR_USU_M = @DRCTR_USU_M, 
                                DRCTR_FM = @DRCTR_FM, DRCTR_FACTOR = @DRCTR_FACTOR, DRCTR_DSPCT = @DRCTR_DSPCT, DRCTR_FFCTR = @DRCTR_FFCTR, DRCTR_CDD = @DRCTR_CDD, DRCTR_USR_ID = @DRCTR_USR_ID, DRCTR_FILE = @DRCTR_FILE, DRCTR_USOCFDI = @DRCTR_USOCFDI
                    			WHERE DRCTR_ID = @DRCTR_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@DRCTR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@DRCTR_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@DRCTR_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@DRCTR_TLFN", item.Telefono);
            sqlCommand.Parameters.AddWithValue("@DRCTR_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@DRCTR_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@DRCTR_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@DRCTR_FM", item.FechaModifica);
            return ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<BeneficiarioModel> GetList() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// obtener listado de beneficiarios del directorio, generalmente son todos los registros
        /// </summary>
        public IEnumerable<T1> GetBeneficiarios<T1>(List<Conditional> keyValues, string search = null) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, keyValues)).ToList();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public BeneficiarioDetailModel Save(BeneficiarioDetailModel item) {
            if (item.IdDirectorio == 0)
                item.IdDirectorio = this.Insert(item);
            else
                this.Update(item);

            // cuentas bancarias
            if (item.CuentasBancarias != null) {
                for (int i = 0; i < item.CuentasBancarias.Count; i++) {
                    if (item.CuentasBancarias[i].IdCuenta == 0) {
                        item.CuentasBancarias[i].IdDirectorio = item.IdDirectorio;
                        item.CuentasBancarias[i].FechaNuevo = DateTime.Now;
                        item.CuentasBancarias[i].IdCuenta = this.Insert(item);
                    } else {
                        //item.CuentasBancarias[i].FechaMod = DateTime.Now;
                        //this.Update(item.CuentasBancarias[i]).ExecuteCommand();
                    }
                }
            }

            return item;
        }

        #region metodos personlizados
        public BeneficiarioDetailModel GetBeneficiario(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM drctr WHERE drctr_id = @id")
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            var tabla = ExecuteReader(sqlCommand);

            return this.GetMapper<BeneficiarioDetailModel>(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(bool onlyActive) {
            return this.GetBeneficiarios(new List<Conditional> { new Conditional("drctr_a", "0", ConditionalTypeEnum.GreaterThanOrEqual) });
        }

        /// <summary>
        /// obtener listado de beneficiarios del directorio, generalmente son todos los registros
        /// </summary>
        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<Conditional> keyValues) {
            var sqlCommand = new FbCommand {
                CommandText = @"/* relacion del directorio */
select drctr.*
from drctr
left join rlcdrc on rlcdrc_drctr_id = drctr_id
left join lstsys on rlcdrc_lstsys_id = lstsys_sec_id 
where rlcdrc_doc_id = 1030 
and rlcdrc_a > 0
and lstsys_doc_id = 1030
and lstsys_a > 0
@condiciones
order by drctr_nom"
            };

            if (keyValues.Count > 0) {
                var d = ExpressionTool.ConditionalModelToSql(keyValues);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "and" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }
            return this.GetMapper<BeneficiarioDetailModel>(sqlCommand).ToList();
        }

        public IEnumerable<BeneficiarioDetailModel> GetBeneficiarios(List<string> relacion, bool onlyActive, string search = "") {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID @condiciones"
            };

            var d0 = new List<IConditional>();
            d0.Add(new Conditional("DRCTR_A", "1"));
            
            return this.GetMapper<BeneficiarioDetailModel>(sqlCommand, d0).ToList();
        }
        #endregion
    }
}
