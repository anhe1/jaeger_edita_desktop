﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    /// <summary>
    /// repositorio de auxiliares a los movimientos bancarios
    /// </summary>
    public class SqlFbMovimientoAuxiliarRepository : RepositoryMaster<MovimientoBancarioAuxiliarModel>, ISqlMovimientoAuxiliarRepository {
        public SqlFbMovimientoAuxiliarRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public int Insert(MovimientoBancarioAuxiliarModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO BNCAUX ( BNCAUX_ID, BNCAUX_A, BNCAUX_SBID, BNCAUX_DESC, BNCAUX_NOM, BNCAUX_TP, BNCAUX_URL, BNCAUX_FN, BNCAUX_USR_N)
                                            VALUES (@BNCAUX_ID,@BNCAUX_A,@BNCAUX_SBID,@BNCAUX_DESC,@BNCAUX_NOM,@BNCAUX_TP,@BNCAUX_URL,@BNCAUX_FN,@BNCAUX_USR_N)
                RETURNING BNCAUX_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@BNCAUX_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_SBID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_NOM", item.FileName);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_TP", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_USR_N", item.Creo);
            item.IdAuxiliar = this.ExecuteScalar(sqlCommand);
            if (item.IdAuxiliar > 0)
                return item.IdAuxiliar;
            return 0;
        }

        public int Update(MovimientoBancarioAuxiliarModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCAUX SET BNCAUX_A=@BNCAUX_A, BNCAUX_SBID=@BNCAUX_SBID, BNCAUX_DESC=@BNCAUX_DESC, BNCAUX_NOM=@BNCAUX_NOM, BNCAUX_TP=@BNCAUX_TP, BNCAUX_URL=@BNCAUX_URL, BNCAUX_FN=@BNCAUX_FN, BNCAUX_USR_N=@BNCAUX_USR_N WHERE BNCAUX_ID = @BNCAUX_ID"
            };
            sqlCommand.Parameters.AddWithValue("@BNCAUX_ID", item.IdAuxiliar);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_SBID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_NOM", item.FileName);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_TP", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_USR_N", item.Creo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public int Update(MovimientoBancarioAuxiliarDetailModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCAUX SET BNCAUX_A=@BNCAUX_A, BNCAUX_SBID=@BNCAUX_SBID, BNCAUX_DESC=@BNCAUX_DESC, BNCAUX_NOM=@BNCAUX_NOM, BNCAUX_TP=@BNCAUX_TP, BNCAUX_URL=@BNCAUX_URL, BNCAUX_FN=@BNCAUX_FN, BNCAUX_USR_N=@BNCAUX_USR_N WHERE BNCAUX_ID = @BNCAUX_ID"
            };
            sqlCommand.Parameters.AddWithValue("@BNCAUX_ID", item.IdAuxiliar);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_SBID", item.IdMovimiento);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_NOM", item.FileName);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_TP", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_URL", item.URL);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCAUX_USR_N", item.Creo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM bncaux WHERE bncmov_id IN (@index) "
            };
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MovimientoBancarioAuxiliarModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"select first 1 bncaux.* from bncaux where bncaux_id = @bncaux_id"
            };
            sqlCommand.Parameters.AddWithValue("@bncaux_id", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<MovimientoBancarioAuxiliarModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"select bncaux.* from bncaux"
            };
            return this.GetMapper(sqlCommand);
        }

        /// <summary>
        /// listado de auxiliares por condiciones
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        public IEnumerable<MovimientoBancarioAuxiliarDetailModel> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"select bncaux.* from bncaux @wcondiciones"
            };
            if (conditionals.Count > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
                return this.GetMapper<MovimientoBancarioAuxiliarDetailModel>(sqlCommand);
            }
            return new List<MovimientoBancarioAuxiliarDetailModel>();
        }

        /// <summary>
        /// listado de auxiliares por condiciones
        /// </summary>
        /// <param name="conditionals">listado de condiciones</param>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1: class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"select bncaux.* from bncaux @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);  
        }

        /// <summary>
        /// almacenar movimiento bancario
        /// </summary>
        public MovimientoBancarioAuxiliarDetailModel Save(MovimientoBancarioAuxiliarDetailModel item) {
            if (item.IdAuxiliar == 0) {
                item.FechaNuevo = DateTime.Now;
                item.Creo = this.User;
                item.IdAuxiliar = this.Insert((MovimientoBancarioAuxiliarDetailModel)item);
            } else {
                item.FechaNuevo = DateTime.Now;
                item.Creo = this.User;
                this.Update(item);
            }
            return item;
        }

        public bool CreateTables() {
            return false;
        }
    }
}
