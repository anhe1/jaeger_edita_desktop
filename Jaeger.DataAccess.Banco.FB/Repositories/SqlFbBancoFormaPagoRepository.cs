﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    /// <summary>
    /// repositorio de formas de pago 
    /// </summary>
    public class SqlFbBancoFormaPagoRepository : RepositoryMaster<BancoFormaPagoDetailModel>, ISqlBancoFormaPagoRepository {
        public SqlFbBancoFormaPagoRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(BancoFormaPagoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO bncfrm ( bncfrm_id, bncfrm_a, bncfrm_afs, bncfrm_clv, bncfrm_clvs, bncfrm_nom, bncfrm_fn, bncfrm_usr_n) 
                    			            VALUES (@bncfrm_id,@bncfrm_a,@bncfrm_afs,@bncfrm_clv,@bncfrm_clvs,@bncfrm_nom,@bncfrm_fn,@bncfrm_usr_n)
                returning bncfrm_id"
            };

            sqlCommand.Parameters.AddWithValue("@bncfrm_id", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@bncfrm_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bncfrm_afs", item.AfectarSaldo);
            sqlCommand.Parameters.AddWithValue("@bncfrm_clv", item.Clave);
            sqlCommand.Parameters.AddWithValue("@bncfrm_clvs", item.ClaveSAT);
            sqlCommand.Parameters.AddWithValue("@bncfrm_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@bncfrm_fn", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@bncfrm_usr_n", item.Creo);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(BancoFormaPagoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE bncfrm 
                            SET bncfrm_a=@bncfrm_a, bncfrm_afs=@bncfrm_afs, bncfrm_clv = @bncfrm_clv, bncfrm_clvs = @bncfrm_clvs, bncfrm_nom = @bncfrm_nom, bncfrm_fm = @bncfrm_fm, bncfrm_usr_m = @bncfrm_usr_m
                            WHERE bncfrm_id = @bncfrm_id"
            };

            sqlCommand.Parameters.AddWithValue("@bncfrm_id", item.Id);
            sqlCommand.Parameters.AddWithValue("@bncfrm_a", item.Activo);
            sqlCommand.Parameters.AddWithValue("@bncfrm_afs", item.AfectarSaldo);
            sqlCommand.Parameters.AddWithValue("@bncfrm_clv", item.Clave);
            sqlCommand.Parameters.AddWithValue("@bncfrm_clvs", item.ClaveSAT);
            sqlCommand.Parameters.AddWithValue("@bncfrm_nom", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@bncfrm_fm", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@bncfrm_usr_m", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCFRM SET BNCFRM_A = 0, BNCFRM_FM = @BNCFRM_FM, BNCFRM_USR_M=@BNCFRM_USR_M WHERE BNCFRM_ID = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@bncfrm_fm", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@bncfrm_usr_m", this.User);
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public BancoFormaPagoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = string.Format("SELECT * FROM BNCFRM WHERE BNCFRM_ID = @index")
            };

            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper<BancoFormaPagoModel>(sqlCommand).SingleOrDefault();
        }

        public IEnumerable<BancoFormaPagoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM bncfrm"
            };

            return this.GetMapper<BancoFormaPagoModel>(sqlCommand).ToList();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM BNCFRM @condiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        /// <summary>
        /// almacenar forma de pago
        /// </summary>
        /// <param name="item">BancoFormaPagoDetailModel</param>
        public IBancoFormaPagoDetailModel Save(IBancoFormaPagoDetailModel item) {
            if (item.Id == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.Id = this.Insert((BancoFormaPagoModel)item);
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                this.Update((BancoFormaPagoModel)item);
            }
            return item;
        }

        public bool CrearTabla() {
            throw new NotImplementedException();
        }
    }
}
