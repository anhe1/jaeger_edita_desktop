﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Banco.Repositories {
    /// <summary>
    /// repositorio de conceptos de movimientos bancarios
    /// </summary>
    public class SqlFbMovimientoConceptoRepository : RepositoryMaster<MovimientoConceptoModel>, ISqlMovimientoConceptoRepository {
        public SqlFbMovimientoConceptoRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(MovimientoConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO BNCCNP
                         (BNCCNP_ID, BNCCNP_A, BNCCNP_R, BNCCNP_REQ, BNCCNP_AFS, BNCCNP_IDOP, BNCCNP_IDTP, BNCCNP_IDSBT, BNCCNP_STTS_ID, BNCCNP_IDTPCMB, BNCCNP_IVA, BNCCNP_CLASS, BNCCNP_DESC, BNCCNP_NOM, BNCCNP_CNTA, BNCCNP_FRMT_ID, BNCCNP_TPCMP, BNCCNP_RLCCMP, BNCCNP_STATUS, BNCCNP_RLCDIR, BNCCNP_FN, BNCCNP_USR_N)
                 VALUES (@BNCCNP_ID,@BNCCNP_A,@BNCCNP_R,@BNCCNP_REQ,@BNCCNP_AFS,@BNCCNP_IDOP,@BNCCNP_IDTP,@BNCCNP_IDSBT,@BNCCNP_STTS_ID,@BNCCNP_IDTPCMB,@BNCCNP_IVA,@BNCCNP_CLASS,@BNCCNP_DESC,@BNCCNP_NOM,@BNCCNP_CNTA,@BNCCNP_FRMT_ID,@BNCCNP_TPCMP,@BNCCNP_RLCCMP,@BNCCNP_STATUS,@BNCCNP_RLCDIR,@BNCCNP_FN,@BNCCNP_USR_N) RETURNING BNCCNP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@BNCCNP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_R", item.ReadOnly);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_REQ", item.RequiereComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_AFS", item.AfectaSaldoComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDOP", item.IdTipoOperacion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDTP", item.IdTipoEfecto);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDSBT", item.IdSubTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_STTS_ID", item.IdStatusComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDTPCMB", item.IdTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IVA", item.IVA);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_CLASS", item.Clasificacion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_NOM", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_CNTA", item.CuentaContable);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_FRMT_ID", item.IdFormatoImpreso);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_TPCMP", item.TipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_RLCCMP", item.RelacionComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_STATUS", item.StatusComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_RLCDIR", item.Directorio);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_USR_N", item.Creo);
            item.IdConcepto = this.ExecuteScalar(sqlCommand);
            if (item.IdConcepto > 0)
                return item.IdConcepto;
            return 0;
        }

        public int Update(MovimientoConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCCNP SET BNCCNP_A = @BNCCNP_A, BNCCNP_R = @BNCCNP_R, BNCCNP_REQ = @BNCCNP_REQ, BNCCNP_AFS = @BNCCNP_AFS, BNCCNP_IDOP = @BNCCNP_IDOP, BNCCNP_IDTP = @BNCCNP_IDTP, BNCCNP_IDSBT = @BNCCNP_IDSBT, BNCCNP_STTS_ID = @BNCCNP_STTS_ID, BNCCNP_IDTPCMB = @BNCCNP_IDTPCMB, BNCCNP_IVA = @BNCCNP_IVA, 
BNCCNP_CLASS = @BNCCNP_CLASS, BNCCNP_DESC = @BNCCNP_DESC, BNCCNP_NOM = @BNCCNP_NOM, BNCCNP_CNTA = @BNCCNP_CNTA, BNCCNP_FRMT_ID = @BNCCNP_FRMT_ID, BNCCNP_TPCMP = @BNCCNP_TPCMP, BNCCNP_RLCCMP = @BNCCNP_RLCCMP, BNCCNP_STATUS = @BNCCNP_STATUS, BNCCNP_RLCDIR = @BNCCNP_RLCDIR, BNCCNP_FM = @BNCCNP_FM, BNCCNP_USR_M = @BNCCNP_USR_M
WHERE BNCCNP_ID = @BNCCNP_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@BNCCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_R", item.ReadOnly);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_REQ", item.RequiereComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_AFS", item.AfectaSaldoComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDOP", item.IdTipoOperacion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDTP", item.IdTipoEfecto);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDSBT", item.IdSubTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_STTS_ID", item.IdStatusComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IDTPCMB", item.IdTipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_IVA", item.IVA);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_CLASS", item.Clasificacion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_NOM", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_CNTA", item.CuentaContable);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_FRMT_ID", item.IdFormatoImpreso);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_TPCMP", item.TipoComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_RLCCMP", item.RelacionComprobantes);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_STATUS", item.StatusComprobante);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_RLCDIR", item.Directorio);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@BNCCNP_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE BNCCNP SET BNCCNP_A = WHERE BNCCNP_ID = @BNCCNP_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@BNCCNP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public MovimientoConceptoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<MovimientoConceptoModel> GetList() {
            throw new NotImplementedException();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"select bnccnp.* from bnccnp @wcondiciones"
            };

            if (conditionals.Count() > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
                return this.GetMapper<T1>(sqlCommand);
            }
            return new List<T1>();
        }

        public MovimientoConceptoDetailModel Save(MovimientoConceptoDetailModel model) {
            var seleccionados = model.ObjetosDirectorio.Where(it => it.Selected == true).Select(it => it.Id).ToArray();
            model.Directorio = string.Join(",", seleccionados);
            model.RelacionComprobantes = string.Join(",", model.ObjetosRelacion.Where(it => it.Selected == true).Select(it => it.Id).ToArray());
            model.TipoComprobante = string.Join(",", model.ObjetosComprobante.Where(it => it.Selected == true).Select(it => it.Id).ToArray());
            if (model.IdConcepto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.IdConcepto = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }

        /// <summary>
        /// crear tabla
        /// </summary>
        public void Create() {
            //this.CreateTable();
        }
    }
}
