﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbAccionCorrectivaRepository : RepositoryMaster<AccionCorrectivaModel>, ISqlAccionCorrectivaRepository {
        public SqlFbAccionCorrectivaRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFAC WHERE NOCFAC_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public AccionCorrectivaModel GetById(int index) {
            return this.GetList<AccionCorrectivaModel>(new List<IConditional>() { new Conditional("NOCFAC_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<AccionCorrectivaModel> GetList() {
            return this.GetList<AccionCorrectivaModel>(new List<IConditional>());
        }

        public int Insert(AccionCorrectivaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFAC (NOCFAC_ID, NOCFAC_A, NOCFAC_NOCF_ID, NOCFAC_DESC, NOCFAC_RESP, NOCFAC_CTDEP_ID, NOCFAC_CTTIP_ID, NOCFAC_FEC, NOCFAC_FN)
                                            VALUES(@NOCFAC_ID,@NOCFAC_A,@NOCFAC_NOCF_ID,@NOCFAC_DESC,@NOCFAC_RESP,@NOCFAC_CTDEP_ID,@NOCFAC_CTTIP_ID,@NOCFAC_FEC,@NOCFAC_FN) RETURNING NOCFAC_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFAC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_CTTIP_ID", item.IdTipoAccion);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_FN", item.FechaNuevo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(AccionCorrectivaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFAC SET NOCFAC_A = @NOCFAC_A, NOCFAC_NOCF_ID = @NOCFAC_NOCF_ID, NOCFAC_DESC = @NOCFAC_DESC, NOCFAC_RESP = @NOCFAC_RESP, NOCFAC_CTDEP_ID = @NOCFAC_CTDEP_ID, NOCFAC_CTTIP_ID = @NOCFAC_CTTIP_ID, NOCFAC_FEC = @NOCFAC_FEC, NOCFAC_FN = @NOCFAC_FN WHERE NOCFAC_ID = @NOCFAC_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFAC_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_RESP", item.Responsable);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_CTDEP_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_CTTIP_ID", item.IdTipoAccion);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NOCFAC_FN", item.FechaNuevo);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFAC @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public AccionCorrectivaDetailModel Save(AccionCorrectivaDetailModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Id = this.Insert(model);
            } else {
                model.FechaNuevo = DateTime.Now;
                this.Update(model);
            }
            return model;
        }
    }
}
