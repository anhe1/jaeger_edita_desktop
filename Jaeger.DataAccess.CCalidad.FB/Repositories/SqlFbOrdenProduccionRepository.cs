﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbOrdenProduccionRepository : RepositoryMaster<OrdenProduccionModel>, ISqlOrdenProduccionRepository {
        public SqlFbOrdenProduccionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCFO WHERE NOCFO_ID = @ID "
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public OrdenProduccionModel GetById(int index) {
            return this.GetList<OrdenProduccionModel>(new List<IConditional>() { new Conditional("NOCFO_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<OrdenProduccionModel> GetList() {
            return this.GetList<OrdenProduccionModel>(new List<IConditional>());
        }

        public int Insert(OrdenProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCFO (NOCFO_ID, NOCFO_A, NOCFO_NOCF_ID, NOCFO_PEDPRD_ID, NOCFO_NOM, NOCFO_DESC, NOCFO_FN) VALUES (@NOCFO_ID, @NOCFO_A, @NOCFO_NOCF_ID, @NOCFO_PEDPRD_ID, @NOCFO_NOM, @NOCFO_DESC, @NOCFO_FN) RETURNING NOCFO_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFO_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCFO_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFO_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFO_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@NOCFO_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NOCFO_NOM", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@NOCFO_DESC", item.Descripcion);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(OrdenProduccionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCFO SET NOCFO_A = @NOCFO_A, NOCFO_NOCF_ID = @NOCFO_NOCF_ID, NOCFO_PEDPRD_ID = @NOCFO_PEDPRD_ID, NOCFO_NOM = @NOCFO_NOM, NOCFO_DESC = @NOCFO_DESC, NOCFO_FN = @NOCFO_FN WHERE NOCFO_ID = @NOCFO_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NOCFO_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NOCFO_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCFO_NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCFO_PEDPRD_ID", item.IdOrden);
            sqlCommand.Parameters.AddWithValue("@NOCFO_NOM", item.Cliente);
            sqlCommand.Parameters.AddWithValue("@NOCFO_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCFO_FN", item.FechaNuevo);

            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public OrdenProduccionDetailModel Save(OrdenProduccionDetailModel model) {
            model.FechaNuevo = DateTime.Now;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCFO @wcondiciones "
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
