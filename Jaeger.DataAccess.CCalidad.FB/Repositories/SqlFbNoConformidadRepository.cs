﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.DataBase.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.CCalidad.Repositories {
    public class SqlFbNoConformidadRepository : RepositoryMaster<NoConformidadModel>, ISqlNoConformidadRepository {
        protected internal ISqlNoConformidadStatusRepository statusRepository;

        public SqlFbNoConformidadRepository(IDataBaseConfiguracion configuracion, string user) : base(configuracion) { 
            this.User = user;
            this.statusRepository = new SqlFbNoConformidadStatusRepository(configuracion, user);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NOCF WHERE NOCF_ID = @id"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(NoConformidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NOCF (NOCF_ID, NOCF_A, NOCF_CTST_ID, NOCF_CTTIP_ID, NOCF_AUT_ID, NOCF_CTUSR_ID, NOCF_CTDPTO_ID, NOCF_DESC, NOCF_CANT, NOCF_COSTO, NOCF_NOM, NOCF_DRCTR_ID, NOCF_NOTA, NOCF_FN, NOCF_USU_N)
                                          VALUES(@NOCF_ID,@NOCF_A,@NOCF_CTST_ID,@NOCF_CTTIP_ID,@NOCF_AUT_ID,@NOCF_CTUSR_ID,@NOCF_CTDPTO_ID,@NOCF_DESC,@NOCF_CANT,@NOCF_COSTO,@NOCF_NOM,@NOCF_DRCTR_ID,@NOCF_NOTA,@NOCF_FN,@NOCF_USU_N) RETURNING NOCF_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCF_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NOCF_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTST_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTTIP_ID", item.NOCF_CTTIP_ID);
            sqlCommand.Parameters.AddWithValue("@NOCF_AUT_ID", item.IdAutoriza);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTUSR_ID", item.IdUsuario);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTDPTO_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCF_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCF_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@NOCF_COSTO", item.Costo);
            sqlCommand.Parameters.AddWithValue("@NOCF_NOM", item.Detector);
            sqlCommand.Parameters.AddWithValue("@NOCF_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NOCF_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NOCF_FN", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NOCF_USU_N", item.Creo);
            item.IdNoConformidad = this.ExecuteScalar(sqlCommand);

            if (item.IdNoConformidad > 0)
                return item.IdNoConformidad;
            return 0;
        }

        public int Update(NoConformidadModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NOCF SET NOCF_A = @NOCF_A, NOCF_CTST_ID = @NOCF_CTST_ID, NOCF_CTTIP_ID = @NOCF_CTTIP_ID, NOCF_AUT_ID = @NOCF_AUT_ID, NOCF_CTUSR_ID = @NOCF_CTUSR_ID, NOCF_CTDPTO_ID = @NOCF_CTDPTO_ID, NOCF_DESC = @NOCF_DESC, 
NOCF_CANT = @NOCF_CANT, NOCF_COSTO = @NOCF_COSTO, NOCF_NOM = @NOCF_NOM, NOCF_DRCTR_ID = @NOCF_DRCTR_ID, NOCF_NOTA = @NOCF_NOTA, NOCF_FM = @NOCF_FM, NOCF_USU_M = @NOCF_USU_M WHERE NOCF_ID = @NOCF_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NOCF_ID", item.IdNoConformidad);
            sqlCommand.Parameters.AddWithValue("@NOCF_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTST_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTTIP_ID", item.NOCF_CTTIP_ID);
            sqlCommand.Parameters.AddWithValue("@NOCF_AUT_ID", item.IdAutoriza);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTUSR_ID", item.IdUsuario);
            sqlCommand.Parameters.AddWithValue("@NOCF_CTDPTO_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@NOCF_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NOCF_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@NOCF_COSTO", item.Costo);
            sqlCommand.Parameters.AddWithValue("@NOCF_NOM", item.Detector);
            sqlCommand.Parameters.AddWithValue("@NOCF_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NOCF_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NOCF_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@NOCF_USU_M", item.Modifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<NoConformidadModel> GetList() {
            return this.GetList<NoConformidadModel>(new List<IConditional>());
        }

        NoConformidadModel IGenericRepository<NoConformidadModel>.GetById(int index) {
            throw new System.NotImplementedException();
        }
        #endregion

        public NoConformidadDetailModel Save(NoConformidadDetailModel model) {
            if (model.IdNoConformidad == 0) {
                model.FechaModifica = DateTime.Now;
                model.Creo = this.User;
                model.IdNoConformidad = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }

        public NoConformidadDetailModel GetById(int index) {
            return this.GetList<NoConformidadDetailModel>(new List<IConditional>() { new Conditional("NOCF_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NOCF @wcondiciones "
            };

            if (typeof(T1) == typeof(DepartamentoModel)) {
                sqlCommand.CommandText = @"SELECT * FROM EDP1 WHERE EDP1_A > 0 ORDER BY EDP1_NOM";
            } else if (typeof(T1) == typeof(NoConformidadPrinter)) {
                sqlCommand.CommandText = @"SELECT * FROM NOCF LEFT JOIN EDP1 ON NOCF.NOCF_CTDPTO_ID = EDP1.EDP1_ID @wcondiciones ";
            } else if (typeof(T1) == typeof(EmpleadoModel)) {
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT DIR.*, DIRCLA.*, DIRESP.*, TIPREM.*
                                    FROM DIR
                                    LEFT JOIN DIRCLA ON DIR_DIRCLA_ID = DIRCLA_ID 
                                    LEFT JOIN DIRESP ON DIR_DIRESP_ID = DIRESP_ID 
                                    LEFT JOIN TIPREM ON DIR_TIPREM_ID = TIPREM_ID
                                    WHERE DIRCLA_C <>'0' AND DIRCLA_C <> '0' @condiciones
                                    ORDER BY DIR_NOM "
                };
                var d1 = conditionals.Where(it => it.FieldName.ToLower() == "@search").FirstOrDefault();
                if (d1 != null) {
                    if (d1.FieldValue != string.Empty) {
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "AND LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DIR_NOM, 'á', 'a'), 'é', 'e'), 'í', 'i'), 'ó', 'o'), 'ú', 'u')) || ',' || LOWER(DIR_CLA) || ','|| LOWER(DIR_RFC) LIKE @SEARCH @condiciones ");
                        sqlCommand.CommandText = sqlCommand.CommandText.Replace("@SEARCH", "'%" + d1.FieldValue.ToLower() + "%'");
                    }
                    conditionals.Remove(d1);
                }
            } else if (typeof(T1) == typeof(NoConformidadPeriodoModel)) {
                sqlCommand.CommandText = @"SELECT COUNT(NOCF.NOCF_ID) AS TOTAL, SUM(IIF(NOCF.NOCF_CTST_ID > 1, 1, 0)) AS RESUELTO, SUM(NOCF.NOCF_COSTO) AS COSTO, NOCF.NOCF_ANIO AS EJERCICIO, NOCF.NOCF_MES AS PERIODO
                                            FROM NOCF 
                                            @wcondiciones
                                            GROUP BY NOCF.NOCF_ANIO, NOCF.NOCF_MES";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public static void WriteFileText(string archivo, string texto) {
            Encoding utf8WithoutBom = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);
            using (StreamWriter streamWriter = new StreamWriter(archivo, false, utf8WithoutBom)) {
                streamWriter.Write(texto);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }
    }
}
