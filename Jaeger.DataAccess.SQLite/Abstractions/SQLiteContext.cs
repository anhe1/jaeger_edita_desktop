﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SqlSugar;
using Jaeger.Domain.Contracts;
using Jaeger.DataAccess.Entities;
using Jaeger.Domain.DataBase.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.Abstractions {
    public class SQLiteContext<T> : IGenericRepository<T> where T : class, new() {

        private MessageError menssageField = new MessageError();
        private DataBaseConfiguracion configuracionField;
        private readonly SqlSugarClient dbase;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion"></param>
        public SQLiteContext(DataBaseConfiguracion configuracion) {
            this.configuracionField = configuracion;
            this.Message = new MessageError();
            this.dbase = new SqlSugarClient(new ConnectionConfig() {
                ConnectionString = "DataSource = " + configuracion.Database,
                DbType = DbType.Sqlite,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute,
                AopEvents = new AopEvents() {
                    OnLogExecuting = (sql, p) => {
                        Console.WriteLine(string.Concat("Executing SQL: ", sql));
                        Console.WriteLine(string.Join(",", p.Select(it => it.ParameterName + ":" + it.Value)));
                    }
                }
            });
        }

        /// <summary>
        /// obtener o establecer la configuracion para las conexiones a la base de datos
        /// </summary>
        public DataBaseConfiguracion Settings {
            get {
                return this.configuracionField;
            }
            set {
                this.configuracionField = value;
            }
        }

        public SqlSugarClient Db {
            get {
                return this.dbase;
            }
        }

        public SimpleClient<T> CurrentDb {
            get {
                return new SimpleClient<T>(Db);
            }
        }

        public MessageError Message {
            get {
                return this.menssageField;
            }
            set {
                this.menssageField = value;
            }
        }

        string IGenericRepository<T>.Message { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public virtual T GetById(int id) {
            return CurrentDb.GetById(id);
        }

        public virtual IEnumerable<T> GetList() {
            return CurrentDb.GetList();
        }

        public virtual bool Delete(int id) {
            return CurrentDb.DeleteById(id);
        }

        public virtual int Insert(T item) {
            return this.CurrentDb.InsertReturnIdentity(item);
        }

        public virtual bool Insert(List<T> items) {
            return this.CurrentDb.InsertRange(items);
        }

        public virtual int Update(T objeto) {
            return this.CurrentDb.AsUpdateable(objeto).ExecuteCommand();
        }

        /// <summary>
        /// Crear tabla del modelo
        /// </summary>
        /// <returns>mensaje</returns>
        public virtual bool InitTable() {
            try {
                this.Db.CodeFirst.InitTables<T>();
                this.Message = new MessageError() { DateTime = DateTime.Now, Value = "Exito!" };
                return true;
            } catch (SqlSugarException ex) {
                this.Message = new MessageError() { DateTime = DateTime.Now, Value = ex.Message };
                return false;
            }
        }

        public virtual bool CreateDataBase() {
            try {
                this.Db.DbMaintenance.CreateDatabase();
                return true;
            } catch (SqlSugarException ex) {
                Services.LogErrorService.LogWrite("[CreateDataBase]: " + ex.Message);
                Console.WriteLine(ex.Message); 
                return false;
            }
        }

        public string CreateGuid(string[] datos) {
            //use MD5 hash to get a 16-byte hash of the string:
            var provider = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(string.Join("", datos).Trim().ToUpper());

            byte[] hashBytes = provider.ComputeHash(inputBytes);

            //generate a guid from the hash:

            var hashGuid = new Guid(hashBytes);

            return hashGuid.ToString().ToUpper();
        }

        public virtual IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var _conditionalList = new List<IConditionalModel>();
            if (conditionals != null) {
                foreach (var item in conditionals) {
                    _conditionalList.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return this.Db.Queryable<T1>().Where(_conditionalList).ToList();
        }

        public List<IConditionalModel> Convierte(List<IConditional> conditional) {
            var _condiciones = new List<IConditionalModel>();
            if (conditional != null) {
                foreach (var item in conditional) {
                    _condiciones.Add(new ConditionalModel() { ConditionalType = (ConditionalType)item.ConditionalType, FieldName = item.FieldName, FieldValue = item.FieldValue });
                }
            }
            return _condiciones;
        }
    }
}
