﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jaeger.DataAccess.Service {
    public static class ValidateExtensions {
        public static bool IsNullOrEmpty(this IEnumerable<object> thisValue) {
            if (thisValue == null || thisValue.Count() == 0) return true;
            return false;
        }

        public static bool HasValue(this IEnumerable<object> thisValue) {
            if (thisValue == null || thisValue.Count() == 0) return false;
            return true;
        }

        public static bool HasValue(this object thisValue) {
            if (thisValue == null || thisValue == DBNull.Value) return false;
            return thisValue.ToString() != "";
        }

        public static bool IsContainsIn(this string thisValue, params string[] inValues) {
            return inValues.Any(it => thisValue.Contains(it));
        }
    }
}
