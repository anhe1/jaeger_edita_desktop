﻿using System;
using System.IO;

namespace Jaeger.DataAccess.Service {
    public class LogErrorService {
        public static string FileName;

        /// <summary>
        /// Constructor
        /// </summary>
        static LogErrorService() {
            LogErrorService.FileName = @"C:\Jaeger\Jaeger.Log\jaeger_data_access_sqlite.log";
        }

        static public bool LogDelete() {
            try {
                File.Delete(LogErrorService.FileName);
                return true;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public static void LogWrite(string mensaje) {
            try {
                if (!File.Exists(LogErrorService.FileName)) {
                    File.Create(LogErrorService.FileName).Close();
                }
                var streamWriter = File.AppendText(LogErrorService.FileName);
                object[] type = new object[] { mensaje, "|", DateTime.Now.ToString("s") };
                streamWriter.WriteLine(string.Concat(type));
                streamWriter.Close();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
