﻿namespace Jaeger.DataAccess.Service {
    public static class UtilConvert {
        public static bool EqualCase(this string thisValue, string equalValue) {
            if (thisValue != null && equalValue != null) {
                return thisValue.ToLower() == equalValue.ToLower();
            } else {
                return thisValue == equalValue;
            }
        }
    }
}
