﻿using System;

namespace Jaeger.DataAccess.Service {
    public static class UtilConstants {
        public static Type LongType = typeof(long);
        public static Type IntType = typeof(int);
    }
}
