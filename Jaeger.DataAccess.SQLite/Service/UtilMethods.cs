﻿using System;
using System.Reflection;

namespace Jaeger.DataAccess.Service {
    public static class UtilMethods {
        public static Type GetUnderType(PropertyInfo propertyInfo) {
            Type unType = Nullable.GetUnderlyingType(propertyInfo.PropertyType);
            unType = unType ?? propertyInfo.PropertyType;
            return unType;
        }
    }
}
