﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.CCalidad.Entities;

namespace Jaeger.Aplication.CCalidad.Contracts {
    /// <summary>
    /// interfaz para servicio de no conformidades
    /// </summary>
    public interface INoConformidadService {
        /// <summary>
        /// obtener no conformidad por su indice
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>NoConformidad</returns>
        NoConformidadDetailModel GetById(int index);

        /// <summary>
        /// almacenar no conformidad
        /// </summary>
        /// <param name="model">NoConformidad</param>
        /// <returns>NoConformidad</returns>
        NoConformidadDetailModel Save(NoConformidadDetailModel model);

        /// <summary>
        /// almacenar media relacionada a no conformidades
        /// </summary>
        /// <param name="media">Media</param>
        /// <returns>Media</returns>
        MediaModel Save(MediaModel media);

        /// <summary>
        /// obtener objeto de impresion de no conformidad
        /// </summary>
        /// <param name="index">indice</param>
        /// <returns>Printer</returns>
        NoConformidadPrinter GetPrinter(int index);

        /// <summary>
        /// lista de no conformidades por condicionales
        /// </summary>
        /// <typeparam name="T1">referencia de la entidad</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        /// <returns>List</returns>
        List<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
