﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Aplication.CCalidad.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CCalidad.Builder;
using Jaeger.Domain.CCalidad.Contracts;
using Jaeger.Domain.CCalidad.Entities;
using Jaeger.Domain.CCalidad.ValueObjects;

namespace Jaeger.Aplication.CCalidad.Services {
    public class NoConformidadService : INoConformidadService {
        #region declaraciones
        protected internal ISqlNoConformidadRepository _repository;
        protected internal ISqlCausaRaizRepository sqlCausaRaizRepository;
        protected internal ISqlAccionCorrectivaRepository sqlAccionCorrectivaRepository;
        protected internal ISqlAccionPreventivaRepository sqlAccionPreventivaRepository;
        protected internal ISqlOrdenProduccionRepository sqlOrdenProduccionRepository;
        protected internal ISqlCostoNoCalidadRepository sqlCostoNoCalidadRepository;
        protected internal ISqlMediaRepository sqlMediaRepository;
        protected Base.Contracts.IEditaBucketService s3;
        #endregion

        public NoConformidadService() { }

        public NoConformidadDetailModel GetById(int index) {
            var model = this._repository.GetById(index);
            if (model != null) {
                var causaRaiz = this.GetCausaRaiz(index).ToList();
                var accionPreventiva = this.GetAccionPreventiva(index).ToList();
                var accionCorrectiva = this.GetAccionCorrectiva(index).ToList();
                var ordenProduccion = this.GetOrdenProduccion(index).ToList();
                var costoNoCalidad = this.GetCostoNoCalidad(index).ToList();
                var media = this.GetMedia(index).ToList();

                if (causaRaiz != null) {
                    model.CausaRaiz = new BindingList<CausaRaizDetailModel>(causaRaiz);
                }

                if (accionPreventiva != null) {
                    model.AccionesPreventivas = new BindingList<AccionPreventivaDetailModel>(accionPreventiva);
                }

                if (accionCorrectiva != null) {
                    model.AccionesCorrectivas = new BindingList<AccionCorrectivaDetailModel>(accionCorrectiva);
                }

                if (ordenProduccion != null) {
                    model.OrdenProduccion = new BindingList<OrdenProduccionDetailModel>(ordenProduccion);
                }

                if (costoNoCalidad != null) {
                    model.CostosNoCalidad = new BindingList<CostoNoCalidadDetailModel>(costoNoCalidad);
                }

                if (media != null) {
                    model.Media = new BindingList<MediaModel>(media);
                }
            }
            return model;
        }

        public NoConformidadDetailModel Save(NoConformidadDetailModel model) {
            model = this._repository.Save(model);
            if (model.IdNoConformidad > 0) {
                if (model.CausaRaiz.Count > 0) {
                    for (int i = 0; i < model.CausaRaiz.Count; i++) {
                        model.CausaRaiz[i].IdNoConformidad = model.IdNoConformidad;
                        model.CausaRaiz[i] = this.sqlCausaRaizRepository.Save(model.CausaRaiz[i]);
                    }
                }

                if (model.AccionesCorrectivas.Count > 0) {
                    for (int i = 0; i < model.AccionesCorrectivas.Count; i++) {
                        model.AccionesCorrectivas[i].IdNoConformidad = model.IdNoConformidad;
                        model.AccionesCorrectivas[i] = this.sqlAccionCorrectivaRepository.Save(model.AccionesCorrectivas[i]);
                    }
                }

                if (model.AccionesPreventivas.Count > 0) {
                    for (int i = 0; i < model.AccionesPreventivas.Count; i++) {
                        model.AccionesPreventivas[i].IdNoConformidad = model.IdNoConformidad;
                        model.AccionesPreventivas[i] = this.sqlAccionPreventivaRepository.Save(model.AccionesPreventivas[i]);
                    }
                }

                if (model.OrdenProduccion.Count > 0) {
                    for (int i = 0; i < model.OrdenProduccion.Count; i++) {
                        model.OrdenProduccion[i].IdNoConformidad = model.IdNoConformidad;
                        model.OrdenProduccion[i] = this.sqlOrdenProduccionRepository.Save(model.OrdenProduccion[i]);
                    }
                }

                if (model.CostosNoCalidad.Count > 0) {
                    for (int i = 0; i < model.CostosNoCalidad.Count; i++) {
                        model.CostosNoCalidad[i].IdNoConformidad = model.IdNoConformidad;
                        model.CostosNoCalidad[i] = this.sqlCostoNoCalidadRepository.Save(model.CostosNoCalidad[i]);
                    }
                }

                if (model.Media.Count > 0) {
                    for (int i = 0; i < model.Media.Count; i++) {
                        if (model.Media[i].Tag != null) {
                            var md5 = StringExtensions.CreateMD5((string)model.Media[i].Tag);
                            var fileName = System.IO.Path.GetFileName(model.Media[i].FileName);
                            var extension = System.IO.Path.GetExtension(model.Media[i].FileName);
                            model.Media[i].URL = this.s3.Upload((string)model.Media[i].Tag, fileName, md5 + extension);
                        }
                        model.Media[i].IdNoConformidad = model.IdNoConformidad;
                        model.Media[i] = this.sqlMediaRepository.Save(model.Media[i]);
                    }
                }
            }
            return model;
        }

        public MediaModel Save(MediaModel model) {
            //using (var md5 = MD5.Create()) {
            //    using (var stream = System.IO.File.OpenRead(filename)) {
            //        md5.ComputeHash(stream);
            //    }
            //}
            //var infoFile = new System.IO.FileInfo(fileName);     
            return this.sqlMediaRepository.Save(model);
        }

        public NoConformidadPrinter GetPrinter(int index) {
            var model = this.GetList<NoConformidadPrinter>(new List<IConditional> { new Conditional("NOCF_ID", index.ToString()) }).FirstOrDefault();
            var causaRaiz = this.GetCausaRaiz(index).ToList();
            var accionPreventiva = this.GetAccionPreventiva(index).ToList();
            var accionCorrectiva = this.GetAccionCorrectiva(index).ToList();
            var ordenProduccion = this.GetOrdenProduccion(index).ToList();
            var costoNoCalidad = this.GetCostoNoCalidad(index).ToList();
            var media = this.GetMedia(index).ToList();
            var departamento = this.GetList<DepartamentoModel>(new List<Domain.Base.Builder.IConditional>()).ToList();

            if (causaRaiz != null) {
                model.CausaRaiz = new BindingList<CausaRaizDetailModel>(causaRaiz);
                for (int i = 0; i < model.CausaRaiz.Count; i++) {
                    try {
                        var depto = departamento.Where(it => it.IdDepartamento == model.CausaRaiz[i].IdDepartamento).FirstOrDefault();
                        if (depto != null) {
                            model.CausaRaiz[i].Departamento = depto.Nombre;
                        }
                    } catch (System.Exception) {

                    }
                }
            }

            if (accionPreventiva != null) {
                model.AccionesPreventivas = new BindingList<AccionPreventivaDetailModel>(accionPreventiva);
            }

            if (accionCorrectiva != null) {
                model.AccionesCorrectivas = new BindingList<AccionCorrectivaDetailModel>(accionCorrectiva);
            }

            if (ordenProduccion != null) {
                model.OrdenProduccion = new BindingList<OrdenProduccionDetailModel>(ordenProduccion);
            }

            if (costoNoCalidad != null) {
                model.CostosNoCalidad = new BindingList<CostoNoCalidadDetailModel>(costoNoCalidad);
            }
            return new NoConformidadPrinter(model);
        }

        #region CAUSA RAIZ 
        private IEnumerable<CausaRaizDetailModel> GetCausaRaiz(int index) {
            var causaRaiz = this.GetList<CausaRaizDetailModel>(new List<IConditional> {
                    new Conditional("NOCFCR_A", "1"),
                    new Conditional("NOCFCR_NOCF_ID", index.ToString())
                }).ToList();
            return causaRaiz;
        }

        private IEnumerable<AccionCorrectivaDetailModel> GetAccionCorrectiva(int index) {
            var accionCorrectiva = this.GetList<AccionCorrectivaDetailModel>(new List<IConditional> {
                    new Conditional("NOCFAC_A", "1"),
                    new Conditional("NOCFAC_NOCF_ID", index.ToString())
                }).ToList();
            return accionCorrectiva;
        }

        private IEnumerable<AccionPreventivaDetailModel> GetAccionPreventiva(int index) {
            var accionPreventiva = this.GetList<AccionPreventivaDetailModel>(new List<IConditional> {
                    new Conditional("NOCFAP_A", "1"),
                    new Conditional("NOCFAP_NOCF_ID", index.ToString())
                });
            return accionPreventiva;
        }

        private IEnumerable<OrdenProduccionDetailModel> GetOrdenProduccion(int index) {
            var ordenProduccion = this.GetList<OrdenProduccionDetailModel>(new List<IConditional> {
                    new Conditional("NOCFO_A", "1"),
                    new Conditional("NOCFO_NOCF_ID", index.ToString())
                }).ToList();
            return ordenProduccion;
        }

        private IEnumerable<CostoNoCalidadDetailModel> GetCostoNoCalidad(int index) {
            var costoNoCalidad = this.GetList<CostoNoCalidadDetailModel>(new List<IConditional> {
                    new Conditional("NOCFC_A", "1"),
                    new Conditional("NOCFC_NOCF_ID", index.ToString())
                }).ToList();
            return costoNoCalidad;
        }

        private IEnumerable<MediaModel> GetMedia(int index) {
            var media = this.GetList<MediaModel>(new List<IConditional> {
                    new Conditional("NOCFM_A", "1"),
                    new Conditional("NOCFM_NOCF_ID", index.ToString()) }).ToList();
            return media;
        }
        #endregion

        public virtual List<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(CausaRaizDetailModel)) {
                return this.sqlCausaRaizRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(AccionCorrectivaDetailModel)) {
                return this.sqlAccionCorrectivaRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(AccionPreventivaDetailModel)) {
                return this.sqlAccionPreventivaRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(OrdenProduccionDetailModel)) {
                return this.sqlOrdenProduccionRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(CostoNoCalidadDetailModel)) {
                return this.sqlCostoNoCalidadRepository.GetList<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(MediaModel)) {
                return this.sqlMediaRepository.GetList<T1>(conditionals).ToList();
            }
            return this._repository.GetList<T1>(conditionals).ToList();
        }

        #region metodos estaticos
        /// <summary>
        /// Consulta
        /// </summary>
        public static INoConformidadQueryBuilder Query() {
            return new NoConformidadQueryBuilder();
        }

        /// <summary>
        /// obtener lista de status
        /// </summary>
        public static List<StatusModel> GetStatus() {
            return EnumerationExtension.GetEnumToClass<StatusModel, NoConformidadStatusEnum>().ToList();
        }

        /// <summary>
        /// clasificacion 5 m's
        /// </summary>
        public static List<Clasificacion5Model> GetClasificacion() {
            return EnumerationExtension.GetEnumToClass<Clasificacion5Model, Clasificacion5Enum>().ToList();
        }

        /// <summary>
        /// efectos
        /// </summary>
        public static List<TipoEfectoModel> GetEfecto() {
            return EnumerationExtension.GetEnumToClass<TipoEfectoModel, TipoEfectoEnum>().ToList();
        }

        public static List<TipoAccionModel> GetTipoAccion() {
            return EnumerationExtension.GetEnumToClass<TipoAccionModel, TipoAccionEnum>().ToList();
        }

        public static List<MotivoCancelacionModel> GetMotivoCancelacion() {
            return EnumerationExtension.GetEnumToClass<MotivoCancelacionModel, MotivoCancelacionEnum>().ToList();
        }
        #endregion
    }
}
