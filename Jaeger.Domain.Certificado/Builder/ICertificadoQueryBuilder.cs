﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Certificado.Builder {
    public interface ICertificadoQueryBuilder : IConditionalBuilder {
        ICertificadoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);

        ICertificadoQueryLastBuilder Last();
    }

    public interface ICertificadoOnlyActiveQueryBuilder : IConditionalBuilder {

    }

    public interface ICertificadoQueryLastBuilder : IConditionalBuilder {

    }
}
