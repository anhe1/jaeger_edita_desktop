﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Certificado.Builder {
    public class CertificadoQueryBuilder : ConditionalBuilder, IConditionalBuilder, ICertificadoQueryBuilder, ICertificadoOnlyActiveQueryBuilder, ICertificadoQueryLastBuilder {
        public CertificadoQueryBuilder() : base() { }

        public ICertificadoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive)
                this._Conditionals.Add(new Conditional("_ctlcer_a", "1"));
            return this;
        }

        public ICertificadoQueryLastBuilder Last() {
            this.OnlyActive();
            return this;
        }
    }
}
