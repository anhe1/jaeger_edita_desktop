﻿using SqlSugar;
using System;

namespace Jaeger.Domain.Certificado.Abstractions {
    public abstract class Certificate : Base.Abstractions.BasePropertyChangeImplementation {
        /// <summary>
        /// obtener o establecer el RFC del contribuyente
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre razon social del receptor del certificado
        /// </summary>
        [SugarColumn(IsIgnore = true)] 
        public string RazonSocial { get; set; }

        /// <summary>
        /// obtener o establecer numero de serie del certificado
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Serie { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de certificado SELLO o FIEL,
        /// este dato no se almacena en la base de datos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Tipo { get; set; }

        [SugarColumn(IsIgnore = true)]
        public DateTime NotAfter {
            get; set;
        }

        [SugarColumn(IsIgnore = true)]
        public DateTime NotBefore {
            get; set;
        }
    }
}
