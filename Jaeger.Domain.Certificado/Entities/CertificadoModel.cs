﻿// develop: 010220180144
// purpose: certificado
using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Certificado.Entities {
    /// <summary>
    /// clase modelo de certificados
    /// </summary>
    [SugarTable("_ctlcer")]
    public class CertificadoModel : Abstractions.Certificate, Contracts.ICertificado {
        #region declaraciones
        private int index;
        private bool activo;
        private string emisor;
        private string creo;
        private string certificadoBase64;
        private string keyBase64;
        private string keyPasswordBase64;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CertificadoModel() { }

        #region propiedades

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_ctlcer_id")]
        [SugarColumn(ColumnName = "_ctlcer_id", ColumnDescription = "indice de la tabla", IsIdentity = false, IsPrimaryKey = false)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_ctlcer_a")]
        [SugarColumn(ColumnName = "_ctlcer_a", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del contribuyente
        /// </summary>
        [DataNames("_ctlcer_rfc")]
        [SugarColumn(ColumnName = "_ctlcer_rfc", ColumnDescription = "rfc del contribuyente", Length = 16, IsIdentity = true, IsPrimaryKey = true)]
        public new string RFC {
            get {
                return base.RFC;
            }
            set {
                base.RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de serie del certificado
        /// </summary>
        [DataNames("_ctlcer_serie")]
        [SugarColumn(ColumnName = "_ctlcer_serie", ColumnDescription = "numero de serie del certificado", Length = 21, IsIdentity = true, IsPrimaryKey = true)]
        public new string Serie {
            get {
                return base.Serie;
            }
            set {
                base.Serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// _ctlcer_subjectname
        /// </summary>
        [DataNames("_ctlcer_subjectname")]
        [SugarColumn(ColumnName = "_ctlcer_subjectname", ColumnDescription = "a nombre de", ColumnDataType = "MEDIUMTEXT")]
        public new string RazonSocial {
            get {
                return base.RazonSocial;
            }
            set {
                base.RazonSocial = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el nombre de la entidad emisora
        /// </summary>
        [DataNames("_ctlcer_issuerName")]
        [SugarColumn(ColumnName = "_ctlcer_issuerName", ColumnDescription = "nombre del emisor", ColumnDataType = "TEXT")]
        public string Emisor {
            get {
                return this.emisor;
            }
            set {
                this.emisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_cerb64")]
        [SugarColumn(ColumnName = "_ctlcer_cerb64")]
        public string CerB64 {
            get {
                return this.certificadoBase64;
            }
            set {
                this.certificadoBase64 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_keyb64")]
        [SugarColumn(ColumnName = "_ctlcer_keyb64")]
        public string KeyB64 {
            get {
                return this.keyBase64;
            }
            set {
                this.keyBase64 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_psw64")]
        [SugarColumn(ColumnName = "_ctlcer_psw64")]
        public string KeyPassB64 {
            get {
                return this.keyPasswordBase64;
            }
            set {
                this.keyPasswordBase64 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// vigencia del certificado, despues de
        /// </summary>
        [DataNames("_ctlcer_notafter")]
        [SugarColumn(ColumnName = "_ctlcer_notafter")]
        public new DateTime NotAfter {
            get { return base.NotAfter; }
            set { base.NotAfter = value; }
        }

        [DataNames("_ctlcer_notbefore")]
        [SugarColumn(ColumnName = "_ctlcer_notbefore")]
        public new DateTime NotBefore {
            get { return base.NotBefore; }
            set { base.NotBefore = value; }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("_ctlcer_usr_n")]
        [SugarColumn(ColumnName = "_ctlcer_usr_n", ColumnDescription = "", Length = 15)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_fn")]
        [SugarColumn(ColumnName = "_ctlcer_fn")]
        public DateTime FechaNuevo {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_filecer")]
        [SugarColumn(ColumnName = "_ctlcer_filecer")]
        public byte[] CerFile {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("_ctlcer_filekey")]
        [SugarColumn(ColumnName = "_ctlcer_filekey")]
        public byte[] KeyFile {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el tipo de certificado SELLO o FIEL,
        /// este dato no se almacena en la base de datos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public new string Tipo {
            get { return base.Tipo; }
            set {
                base.Tipo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}