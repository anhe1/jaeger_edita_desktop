﻿// develop: 010220180144
// purpose: certificado

namespace Jaeger.Domain.Certificado.Entities {
    public class Response {
        public Response() {
            this.IsOk = false;
        }

        public Response(bool isOk, string message, CertificadoModel certificado) {
            this.IsOk = isOk;
            this.Message = message;
            this.Certificado = certificado;
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public bool IsOk { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// obtener o establecer modelo del certificado
        /// </summary>
        public CertificadoModel Certificado { get; set; }
    }
}