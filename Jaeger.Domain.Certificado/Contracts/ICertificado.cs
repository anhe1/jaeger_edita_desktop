﻿using System;

namespace Jaeger.Domain.Certificado.Contracts {
    public interface ICertificado {
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el RFC del contribuyente
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// numero de serie del certificado
        /// </summary>
        string Serie { get; set; }

        string RazonSocial { get; set; }

        /// <summary>
        /// obtener o establcer el nombre de la entidad emisora
        /// </summary>
        string Emisor { get; set; }

        string CerB64 { get; set; }

        string KeyB64 { get; set; }

        string KeyPassB64 { get; set; }

        DateTime NotAfter { get; set; }

        DateTime NotBefore { get; set; }

        string Tipo { get; set; }

        string Creo { get; set; }

        DateTime FechaNuevo { get; set; }

        byte[] CerFile { get; set; }

        byte[] KeyFile { get; set; }
    }
}
