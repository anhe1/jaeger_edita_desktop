﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Certificado.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Certificado.Contracts {
    /// <summary>
    /// interface de repositorio para certificados
    /// </summary>
    public interface ISqlCertificadoRepository : IGenericRepository<CertificadoModel> {
        CertificadoModel Salveable(CertificadoModel model);

        /// <summary>
        /// obtener certificado activo en el sistema
        /// </summary>
        CertificadoModel GetCertificado();

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
