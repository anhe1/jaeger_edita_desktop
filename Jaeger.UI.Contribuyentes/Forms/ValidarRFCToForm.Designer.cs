﻿
namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ValidarRFCToForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidarRFCToForm));
            this.Nombre = new Telerik.WinControls.UI.RadTextBox();
            this.NombreLabel = new Telerik.WinControls.UI.RadLabel();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.RFCLabel = new Telerik.WinControls.UI.RadLabel();
            this.CodigoPostal = new Telerik.WinControls.UI.RadTextBox();
            this.CodigoPostalLabel = new Telerik.WinControls.UI.RadLabel();
            this.HeaderPic = new System.Windows.Forms.PictureBox();
            this.Agregar = new Telerik.WinControls.UI.RadButton();
            this.Cancelar = new Telerik.WinControls.UI.RadButton();
            this.GroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.ProviderError = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostalLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).BeginInit();
            this.GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Nombre
            // 
            this.Nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Nombre.Location = new System.Drawing.Point(12, 100);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(298, 20);
            this.Nombre.TabIndex = 5;
            // 
            // NombreLabel
            // 
            this.NombreLabel.Location = new System.Drawing.Point(12, 76);
            this.NombreLabel.Name = "NombreLabel";
            this.NombreLabel.Size = new System.Drawing.Size(155, 18);
            this.NombreLabel.TabIndex = 4;
            this.NombreLabel.Text = "Denominación o Razón Social";
            // 
            // RFC
            // 
            this.RFC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.RFC.Location = new System.Drawing.Point(12, 50);
            this.RFC.MaxLength = 14;
            this.RFC.Name = "RFC";
            this.RFC.NullText = "Registro Federal";
            this.RFC.Size = new System.Drawing.Size(137, 20);
            this.RFC.TabIndex = 1;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RFCLabel
            // 
            this.RFCLabel.Location = new System.Drawing.Point(12, 26);
            this.RFCLabel.Name = "RFCLabel";
            this.RFCLabel.Size = new System.Drawing.Size(28, 18);
            this.RFCLabel.TabIndex = 0;
            this.RFCLabel.Text = "RFC:";
            // 
            // CodigoPostal
            // 
            this.CodigoPostal.Location = new System.Drawing.Point(226, 50);
            this.CodigoPostal.MaxLength = 5;
            this.CodigoPostal.Name = "CodigoPostal";
            this.CodigoPostal.NullText = "CP";
            this.CodigoPostal.Size = new System.Drawing.Size(84, 20);
            this.CodigoPostal.TabIndex = 3;
            this.CodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CodigoPostalLabel
            // 
            this.CodigoPostalLabel.Location = new System.Drawing.Point(226, 26);
            this.CodigoPostalLabel.Name = "CodigoPostalLabel";
            this.CodigoPostalLabel.Size = new System.Drawing.Size(78, 18);
            this.CodigoPostalLabel.TabIndex = 2;
            this.CodigoPostalLabel.Text = "Código Postal:";
            // 
            // HeaderPic
            // 
            this.HeaderPic.BackColor = System.Drawing.Color.White;
            this.HeaderPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderPic.Location = new System.Drawing.Point(0, 0);
            this.HeaderPic.Name = "HeaderPic";
            this.HeaderPic.Size = new System.Drawing.Size(353, 30);
            this.HeaderPic.TabIndex = 32;
            this.HeaderPic.TabStop = false;
            // 
            // Agregar
            // 
            this.Agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Agregar.Location = new System.Drawing.Point(185, 201);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(75, 23);
            this.Agregar.TabIndex = 1;
            this.Agregar.Text = "Agregar";
            this.Agregar.Click += new System.EventHandler(this.Agregar_Click);
            // 
            // Cancelar
            // 
            this.Cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancelar.Location = new System.Drawing.Point(266, 201);
            this.Cancelar.Name = "Cancelar";
            this.Cancelar.Size = new System.Drawing.Size(75, 23);
            this.Cancelar.TabIndex = 2;
            this.Cancelar.Text = "Cancelar";
            this.Cancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // GroupBox
            // 
            this.GroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBox.Controls.Add(this.NombreLabel);
            this.GroupBox.Controls.Add(this.Nombre);
            this.GroupBox.Controls.Add(this.RFCLabel);
            this.GroupBox.Controls.Add(this.RFC);
            this.GroupBox.Controls.Add(this.CodigoPostalLabel);
            this.GroupBox.Controls.Add(this.CodigoPostal);
            this.GroupBox.HeaderText = "Validar";
            this.GroupBox.Location = new System.Drawing.Point(12, 36);
            this.GroupBox.Name = "GroupBox";
            this.GroupBox.Size = new System.Drawing.Size(328, 143);
            this.GroupBox.TabIndex = 0;
            this.GroupBox.TabStop = false;
            this.GroupBox.Text = "Validar";
            // 
            // ProviderError
            // 
            this.ProviderError.ContainerControl = this;
            // 
            // ValidarRFCToForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancelar;
            this.ClientSize = new System.Drawing.Size(353, 236);
            this.Controls.Add(this.GroupBox);
            this.Controls.Add(this.Cancelar);
            this.Controls.Add(this.Agregar);
            this.Controls.Add(this.HeaderPic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidarRFCToForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Agregar";
            this.Load += new System.EventHandler(this.ValidarRFCToForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NombreLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFCLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoPostalLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).EndInit();
            this.GroupBox.ResumeLayout(false);
            this.GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProviderError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Telerik.WinControls.UI.RadTextBox Nombre;
        public Telerik.WinControls.UI.RadLabel NombreLabel;
        public Telerik.WinControls.UI.RadTextBox RFC;
        public Telerik.WinControls.UI.RadLabel RFCLabel;
        public Telerik.WinControls.UI.RadTextBox CodigoPostal;
        public Telerik.WinControls.UI.RadLabel CodigoPostalLabel;
        private System.Windows.Forms.PictureBox HeaderPic;
        private Telerik.WinControls.UI.RadButton Agregar;
        private Telerik.WinControls.UI.RadButton Cancelar;
        private Telerik.WinControls.UI.RadGroupBox GroupBox;
        private System.Windows.Forms.ErrorProvider ProviderError;
    }
}