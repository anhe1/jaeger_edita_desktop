﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ContribuyenteSearchControl : UserControl {
        protected BackgroundWorker _Consultar = new BackgroundWorker();
        private List<ContribuyenteDetailModel> _SourceData;
        private string _Nombre;

        public event EventHandler<ContribuyenteDetailModel> Selected;
        public void OnSelected(ContribuyenteDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public ContribuyenteSearchControl() {
            InitializeComponent();
        }

        private void ContribuyenteSearchControl_Load(object sender, EventArgs e) {
            this.GResult.Columns.AddRange(Services.GridContribuyenteService.GetSearch());
            this.GResult.AllowRowResize = false;
            this.GResult.ShowRowHeaderColumn = false;
            this._Consultar = new BackgroundWorker();
            this._Consultar.DoWork += Consultar_DoWork;
            this._Consultar.RunWorkerCompleted += Consultar_RunWorkerCompleted;
        }

        public TipoRelacionComericalEnum Relacion { get; set; }

        public IDirectorioService Service { get; set; }

        public string Nombre {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public virtual void Search() {
            this.Espera.Visible = true;
            if (!this._Consultar.IsBusy) {
                this.Espera.StartWaiting();
                this._Consultar.RunWorkerAsync();
            }
        }

        public virtual void Search(string descripcion) {
            this.Nombre = descripcion;
            this.Search();
        }

        private void GridResult_DoubleClick(object sender, EventArgs e) {
            if (this.GResult.CurrentRow != null) {
                var seleccionado = this.GResult.CurrentRow.DataBoundItem as ContribuyenteDetailModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }

        private void GridResult_Resize(object sender, EventArgs e) {
            var w = this.GResult.Width / 2;
            this.Espera.Left = w - (this.Espera.Width / 2);
            var h = this.GResult.Height / 2;
            this.Espera.Top = h - (this.Espera.Height / 2);
        }

        public virtual ContribuyenteDetailModel GetCurrent() {
            if (this.GResult.CurrentRow != null) {
                var seleccionado = this.GResult.CurrentRow.DataBoundItem as ContribuyenteDetailModel;
                if (seleccionado != null) {
                    return seleccionado;
                }
            }
            return null;
        }

        private void Consultar_DoWork(object sender, DoWorkEventArgs e) {
            this.GResult.Enabled = false;
            this._SourceData = this.Service.GetList<ContribuyenteDetailModel>(true, this.Nombre);
            //var d0 = DirectorioService.Create().WithRelacionComercial(this.Relacion).RegistroActivo().WithRazonSocial(this.Nombre).Build();
            //this._SourceData = this.Service.GetList<ContribuyenteDetailModel>(d0).ToList();
        }

        private void Consultar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.GResult.DataSource = this._SourceData;
            this.GResult.Enabled = true;
        }
    }
}
