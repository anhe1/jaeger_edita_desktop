﻿namespace Jaeger.UI.Contribuyentes.Forms {
    partial class CedulaDatosUbicacionControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.grpDattosUbicacion = new Telerik.WinControls.UI.RadGroupBox();
            this.lblTipoVialidad = new Telerik.WinControls.UI.RadLabel();
            this.lblNumInterior = new Telerik.WinControls.UI.RadLabel();
            this.lblNumExterior = new Telerik.WinControls.UI.RadLabel();
            this.txtNumInterior = new Telerik.WinControls.UI.RadTextBox();
            this.txtNumExterior = new Telerik.WinControls.UI.RadTextBox();
            this.lblEntidad = new Telerik.WinControls.UI.RadLabel();
            this.lblNombreVialidad = new Telerik.WinControls.UI.RadLabel();
            this.txtCodigoPostal = new Telerik.WinControls.UI.RadTextBox();
            this.lblColonia = new Telerik.WinControls.UI.RadLabel();
            this.lblMunicipio = new Telerik.WinControls.UI.RadLabel();
            this.txtColonia = new Telerik.WinControls.UI.RadTextBox();
            this.lblCodigoPostal = new Telerik.WinControls.UI.RadLabel();
            this.txtNombreVialidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtEntidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtTipoVialidad = new Telerik.WinControls.UI.RadTextBox();
            this.txtMunicipio = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grpDattosUbicacion)).BeginInit();
            this.grpDattosUbicacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumInterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMunicipio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColonia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoPostal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDattosUbicacion
            // 
            this.grpDattosUbicacion.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grpDattosUbicacion.Controls.Add(this.lblTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.lblNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.lblNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.txtNumInterior);
            this.grpDattosUbicacion.Controls.Add(this.txtNumExterior);
            this.grpDattosUbicacion.Controls.Add(this.lblEntidad);
            this.grpDattosUbicacion.Controls.Add(this.lblNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.lblColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblMunicipio);
            this.grpDattosUbicacion.Controls.Add(this.txtColonia);
            this.grpDattosUbicacion.Controls.Add(this.lblCodigoPostal);
            this.grpDattosUbicacion.Controls.Add(this.txtNombreVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtEntidad);
            this.grpDattosUbicacion.Controls.Add(this.txtTipoVialidad);
            this.grpDattosUbicacion.Controls.Add(this.txtMunicipio);
            this.grpDattosUbicacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDattosUbicacion.HeaderText = "Datos de Ubicación (domicilio fiscal, vigente)";
            this.grpDattosUbicacion.Location = new System.Drawing.Point(0, 0);
            this.grpDattosUbicacion.Name = "grpDattosUbicacion";
            this.grpDattosUbicacion.Size = new System.Drawing.Size(552, 125);
            this.grpDattosUbicacion.TabIndex = 5;
            this.grpDattosUbicacion.TabStop = false;
            this.grpDattosUbicacion.Text = "Datos de Ubicación (domicilio fiscal, vigente)";
            // 
            // lblTipoVialidad
            // 
            this.lblTipoVialidad.Location = new System.Drawing.Point(8, 22);
            this.lblTipoVialidad.Name = "lblTipoVialidad";
            this.lblTipoVialidad.Size = new System.Drawing.Size(88, 18);
            this.lblTipoVialidad.TabIndex = 38;
            this.lblTipoVialidad.Text = "Tipo de vialidad:";
            // 
            // lblNumInterior
            // 
            this.lblNumInterior.Location = new System.Drawing.Point(190, 46);
            this.lblNumInterior.Name = "lblNumInterior";
            this.lblNumInterior.Size = new System.Drawing.Size(75, 18);
            this.lblNumInterior.TabIndex = 42;
            this.lblNumInterior.Text = "Núm. interior:";
            // 
            // lblNumExterior
            // 
            this.lblNumExterior.Location = new System.Drawing.Point(8, 46);
            this.lblNumExterior.Name = "lblNumExterior";
            this.lblNumExterior.Size = new System.Drawing.Size(77, 18);
            this.lblNumExterior.TabIndex = 40;
            this.lblNumExterior.Text = "Núm. exterior:";
            // 
            // txtNumInterior
            // 
            this.txtNumInterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumInterior.Location = new System.Drawing.Point(266, 45);
            this.txtNumInterior.Name = "txtNumInterior";
            this.txtNumInterior.ReadOnly = true;
            this.txtNumInterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumInterior.TabIndex = 41;
            // 
            // txtNumExterior
            // 
            this.txtNumExterior.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumExterior.Location = new System.Drawing.Point(91, 45);
            this.txtNumExterior.Name = "txtNumExterior";
            this.txtNumExterior.ReadOnly = true;
            this.txtNumExterior.Size = new System.Drawing.Size(93, 20);
            this.txtNumExterior.TabIndex = 39;
            // 
            // lblEntidad
            // 
            this.lblEntidad.Location = new System.Drawing.Point(314, 72);
            this.lblEntidad.Name = "lblEntidad";
            this.lblEntidad.Size = new System.Drawing.Size(101, 18);
            this.lblEntidad.TabIndex = 30;
            this.lblEntidad.Text = "Entidad Federativa:";
            // 
            // lblNombreVialidad
            // 
            this.lblNombreVialidad.Location = new System.Drawing.Point(211, 22);
            this.lblNombreVialidad.Name = "lblNombreVialidad";
            this.lblNombreVialidad.Size = new System.Drawing.Size(119, 18);
            this.lblNombreVialidad.TabIndex = 37;
            this.lblNombreVialidad.Text = "Nombre de la vialidad:";
            // 
            // txtCodigoPostal
            // 
            this.txtCodigoPostal.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodigoPostal.Location = new System.Drawing.Point(474, 45);
            this.txtCodigoPostal.Name = "txtCodigoPostal";
            this.txtCodigoPostal.ReadOnly = true;
            this.txtCodigoPostal.Size = new System.Drawing.Size(70, 20);
            this.txtCodigoPostal.TabIndex = 43;
            this.txtCodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblColonia
            // 
            this.lblColonia.Location = new System.Drawing.Point(8, 72);
            this.lblColonia.Name = "lblColonia";
            this.lblColonia.Size = new System.Drawing.Size(47, 18);
            this.lblColonia.TabIndex = 33;
            this.lblColonia.Text = "Colonia:";
            // 
            // lblMunicipio
            // 
            this.lblMunicipio.Location = new System.Drawing.Point(8, 98);
            this.lblMunicipio.Name = "lblMunicipio";
            this.lblMunicipio.Size = new System.Drawing.Size(126, 18);
            this.lblMunicipio.TabIndex = 34;
            this.lblMunicipio.Text = "Municipio o delegación:";
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.SystemColors.Window;
            this.txtColonia.Location = new System.Drawing.Point(58, 71);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.ReadOnly = true;
            this.txtColonia.Size = new System.Drawing.Size(253, 20);
            this.txtColonia.TabIndex = 31;
            // 
            // lblCodigoPostal
            // 
            this.lblCodigoPostal.Location = new System.Drawing.Point(393, 46);
            this.lblCodigoPostal.Name = "lblCodigoPostal";
            this.lblCodigoPostal.Size = new System.Drawing.Size(78, 18);
            this.lblCodigoPostal.TabIndex = 44;
            this.lblCodigoPostal.Text = "Codigo Postal:";
            // 
            // txtNombreVialidad
            // 
            this.txtNombreVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtNombreVialidad.Location = new System.Drawing.Point(340, 21);
            this.txtNombreVialidad.Name = "txtNombreVialidad";
            this.txtNombreVialidad.ReadOnly = true;
            this.txtNombreVialidad.Size = new System.Drawing.Size(204, 20);
            this.txtNombreVialidad.TabIndex = 35;
            // 
            // txtEntidad
            // 
            this.txtEntidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtEntidad.Location = new System.Drawing.Point(419, 71);
            this.txtEntidad.Name = "txtEntidad";
            this.txtEntidad.ReadOnly = true;
            this.txtEntidad.Size = new System.Drawing.Size(125, 20);
            this.txtEntidad.TabIndex = 29;
            // 
            // txtTipoVialidad
            // 
            this.txtTipoVialidad.BackColor = System.Drawing.SystemColors.Window;
            this.txtTipoVialidad.Location = new System.Drawing.Point(102, 21);
            this.txtTipoVialidad.Name = "txtTipoVialidad";
            this.txtTipoVialidad.ReadOnly = true;
            this.txtTipoVialidad.Size = new System.Drawing.Size(103, 20);
            this.txtTipoVialidad.TabIndex = 36;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.SystemColors.Window;
            this.txtMunicipio.Location = new System.Drawing.Point(134, 97);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.ReadOnly = true;
            this.txtMunicipio.Size = new System.Drawing.Size(177, 20);
            this.txtMunicipio.TabIndex = 32;
            // 
            // CedulaDatosUbicacionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpDattosUbicacion);
            this.Name = "CedulaDatosUbicacionControl";
            this.Size = new System.Drawing.Size(552, 125);
            this.Load += new System.EventHandler(this.DomicilioFiscalControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpDattosUbicacion)).EndInit();
            this.grpDattosUbicacion.ResumeLayout(false);
            this.grpDattosUbicacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTipoVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumInterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumExterior)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombreVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMunicipio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtColonia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCodigoPostal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombreVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoVialidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox grpDattosUbicacion;
        private Telerik.WinControls.UI.RadLabel lblTipoVialidad;
        private Telerik.WinControls.UI.RadLabel lblNumInterior;
        private Telerik.WinControls.UI.RadLabel lblNumExterior;
        private Telerik.WinControls.UI.RadTextBox txtNumInterior;
        private Telerik.WinControls.UI.RadTextBox txtNumExterior;
        private Telerik.WinControls.UI.RadLabel lblEntidad;
        private Telerik.WinControls.UI.RadLabel lblNombreVialidad;
        private Telerik.WinControls.UI.RadTextBox txtCodigoPostal;
        private Telerik.WinControls.UI.RadLabel lblColonia;
        private Telerik.WinControls.UI.RadLabel lblMunicipio;
        private Telerik.WinControls.UI.RadTextBox txtColonia;
        private Telerik.WinControls.UI.RadLabel lblCodigoPostal;
        private Telerik.WinControls.UI.RadTextBox txtNombreVialidad;
        private Telerik.WinControls.UI.RadTextBox txtEntidad;
        private Telerik.WinControls.UI.RadTextBox txtTipoVialidad;
        private Telerik.WinControls.UI.RadTextBox txtMunicipio;
    }
}
