﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Catalogos;
using Jaeger.Aplication.Contribuyentes;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ContribuyenteControl : UserControl {
        #region declaraciones
        protected internal BackgroundWorker Preparar = new BackgroundWorker();
        private readonly IUsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        private readonly IRegimenesFiscalesCatalogo regimenesfiscales = new RegimenesFiscalesCatalogo();
        #endregion

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public ContribuyenteControl() {
            InitializeComponent();
            this.Load += this.ContribuyenteControl_Load;
            this.Preparar.DoWork += this.Preparar_DoWork;
            this.Preparar.RunWorkerCompleted += this.Preparar_RunWorkerCompleted;
        }

        private void ContribuyenteControl_Load(object sender, EventArgs e) {
            this.BSplitNombre.Enabled = true;
            this.ButtonCIF.Enabled = true;
        }

        private void ContribuyenteLayoutControl_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.RFC.MaskedEditBoxElement.TextBoxItem.MaxLength = 14;
            this.Nombre.ShowItemToolTips = true;
            this.Nombre.TextBoxElement.ToolTipText = "Utilizado en facturación";
            this.DomicilioFiscal.TextBoxElement.ToolTipText = "Codigo postal de la dirección fiscal utilizado para facturación.";

            // relaciones comerciales
            this.RelacionComercial.CheckedMember = "Activo";
            this.RelacionComercial.DisplayMember = "Name";
            this.RelacionComercial.ValueMember = "IdTipoRelacion";
        }

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            // catalogos
            this.regimenesfiscales.Load();
            this.RegimenFiscal.Enabled = false;
            this.RegimenFiscal.DisplayMember = "Descriptor";
            this.RegimenFiscal.ValueMember = "Clave";
            this.RegimenFiscal.AutoSizeDropDownToBestFit = true;

            this.catalogoUsoCfdi.Load();
            this.UsoCFDI.DisplayMember = "Descriptor";
            this.UsoCFDI.ValueMember = "Clave";
            this.UsoCFDI.AutoSizeDropDownToBestFit = true;
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.RegimenFiscal.DataSource = this.regimenesfiscales.Items;
            this.UsoCFDI.DataSource = this.catalogoUsoCfdi.Items;
            this.RegimenFiscal.SelectedIndex = -1;
            this.RegimenFiscal.SelectedItem = null;

            this.UsoCFDI.SelectedIndex = -1;
            this.UsoCFDI.SelectedItem = null;
            this.RegimenFiscal.Enabled = true;
            this.OnBindingClompleted(e);
        }

        private void BSplitNombre_Click(object sender, EventArgs e) {
            var d0 = new SociedadMercantilBuilder();
            var n0 = this.Nombre.Text;
            d0.WithRazonSocial(n0);
            if (!string.IsNullOrEmpty(d0.Sociedad)) {
                this.Nombre.Text = d0.RazonSocial;
                this.NombreComercial.Text = n0;
            }
        }
    }
}
