﻿namespace Jaeger.UI.Contribuyentes.Forms
{
    partial class ContribuyentesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContribuyentesForm));
            this.GContribuyente = new Jaeger.UI.Contribuyentes.Forms.ContribuyentesGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // GContribuyente
            // 
            this.GContribuyente.Caption = "";
            this.GContribuyente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GContribuyente.Location = new System.Drawing.Point(0, 0);
            this.GContribuyente.Name = "GContribuyente";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.GContribuyente.Permisos = uiAction1;
            this.GContribuyente.ShowActualizar = true;
            this.GContribuyente.ShowAutorizar = false;
            this.GContribuyente.ShowAutosuma = false;
            this.GContribuyente.ShowCerrar = true;
            this.GContribuyente.ShowEditar = true;
            this.GContribuyente.ShowExportarExcel = false;
            this.GContribuyente.ShowFiltro = true;
            this.GContribuyente.ShowHerramientas = false;
            this.GContribuyente.ShowImagen = false;
            this.GContribuyente.ShowImprimir = false;
            this.GContribuyente.ShowNuevo = true;
            this.GContribuyente.ShowRemover = true;
            this.GContribuyente.ShowSeleccionMultiple = true;
            this.GContribuyente.Size = new System.Drawing.Size(832, 472);
            this.GContribuyente.TabIndex = 0;
            // 
            // ContribuyentesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 472);
            this.Controls.Add(this.GContribuyente);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ContribuyentesForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Directorio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ContribuyentesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public ContribuyentesGridControl GContribuyente;
    }
}
