﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Aplication.Contribuyentes.Services;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ContribuyenteNuevoForm : RadForm {
        #region declaraciones
        protected IContribuyenteService service;

        private BackgroundWorker preparar;
        private BackgroundWorker guardar;

        private IContribuyenteDetailModel currentPersona;

        private UIAction _permisos;
        #endregion

        public ContribuyenteNuevoForm() {
            InitializeComponent();
            this.service = new ContribuyenteService(TipoRelacionComericalEnum.Cliente);
            this._permisos = new UIAction();
        }

        public ContribuyenteNuevoForm(UIAction _permisos) {
            InitializeComponent();
            this.service = new ContribuyenteService(TipoRelacionComericalEnum.Cliente);
            this._permisos = _permisos;
        }

        private void ContribuyenteForm_Load(object sender, EventArgs e) {
            // reemplazar las imagenes standar
            this.TContribuyente.Nuevo.Image = Properties.Resources.add_user_male_16px;
            this.TContribuyente.Editar.Image = Properties.Resources.registration_16px;
            this.TContribuyente.Remover.Image = Properties.Resources.denied_16px;
            this.TContribuyente.Guardar.Visibility = ElementVisibility.Visible;

            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.Contribuyente.Preparar.RunWorkerAsync();
            this.preparar.RunWorkerAsync();

            this.guardar = new BackgroundWorker();
            this.guardar.DoWork += Guardar_DoWork;
            this.guardar.RunWorkerCompleted += Guardar_RunWorkerCompleted;
            this.Contribuyente.RFC.LostFocus += TextBoxRFC_TextChanged;
        }

        #region barra de herramientas
        protected virtual void TContribuyente_Guardar_Click(object sender, EventArgs e) {
            if (this.currentPersona.IdDirectorio <= 0) {
                if (ValidacionService.RFC(this.currentPersona.RFC) == false) {
                    if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RFC_NoValido, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                        return;
                }

                IContribuyenteDetailModel temp = this.service.GetByRFC(this.currentPersona.RFC);
                if (temp != null) {
                    RadMessageBox.Show(this, Properties.Resources.RFC_Exisente, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
            }

            var d = this.currentPersona.Domicilios.Where(it => it.Error.Length > 0).Count() > 0;
            if (d == true) {
                RadMessageBox.Show(this, Properties.Resources.Domicilio_Error, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            this.TContribuyente.Enabled = false;
            this.guardar.RunWorkerAsync();
        }

        private void TContacto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TextBoxRFC_TextChanged(object sender, EventArgs e) {
            if (this.currentPersona.IdDirectorio == 0 && this.Contribuyente.RFC.Text.Length >= 12) {
                if (this.VerificarRFC == null) {

                }
                if (!this.VerificarRFC.IsBusy) {
                    this.VerificarRFC.RunWorkerAsync();
                }
            }
        }
        #endregion

        #region preparar formulario
        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Contribuyente.RegimenFiscal.SelectedIndex = -1;
            this.Contribuyente.RegimenFiscal.SelectedItem = null;

            this.Contribuyente.UsoCFDI.SelectedIndex = -1;
            this.Contribuyente.UsoCFDI.SelectedItem = null;

            this.TContribuyente.Actualizar.PerformClick();
        }

        #endregion

        #region metodos privados
        private void CreateBinding() {
            this.Contribuyente.Clave.DataBindings.Clear();
            this.Contribuyente.Clave.DataBindings.Add("Text", this.currentPersona, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RFC.DataBindings.Clear();
            this.Contribuyente.RFC.DataBindings.Add("Text", this.currentPersona, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.CURP.DataBindings.Clear();
            this.Contribuyente.CURP.DataBindings.Add("Text", this.currentPersona, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Nombre.DataBindings.Clear();
            this.Contribuyente.Nombre.DataBindings.Add("Text", this.currentPersona, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.NombreComercial.DataBindings.Clear();
            this.Contribuyente.NombreComercial.DataBindings.Add("Text", this.currentPersona, "NombreComercial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Regimen.DataBindings.Clear();
            this.Contribuyente.Regimen.DataBindings.Add("Text", this.currentPersona, "Regimen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RegimenFiscal.DataBindings.Clear();
            this.Contribuyente.RegimenFiscal.DataBindings.Add("SelectedValue", this.currentPersona, "RegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.ResidenciaFiscal.DataBindings.Clear();
            this.Contribuyente.ResidenciaFiscal.DataBindings.Add("Text", this.currentPersona, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.NumRegIdTrib.DataBindings.Clear();
            this.Contribuyente.NumRegIdTrib.DataBindings.Add("Text", this.currentPersona, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.UsoCFDI.DataBindings.Clear();
            this.Contribuyente.UsoCFDI.DataBindings.Add("SelectedValue", this.currentPersona, "ClaveUsoCfdi", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.DomicilioFiscal.DataBindings.Clear();
            this.Contribuyente.DomicilioFiscal.DataBindings.Add("Text", this.currentPersona, "DomicilioFiscalF", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Telefono.DataBindings.Clear();
            this.Contribuyente.Telefono.DataBindings.Add("Text", this.currentPersona, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Sitio.DataBindings.Clear();
            this.Contribuyente.Sitio.DataBindings.Add("Text", this.currentPersona, "SitioWeb", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Extranjero.DataBindings.Clear();
            this.Contribuyente.Extranjero.DataBindings.Add("Checked", this.currentPersona, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Correo.DataBindings.Clear();
            this.Contribuyente.Correo.DataBindings.Add("Text", this.currentPersona, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Nota.DataBindings.Clear();
            this.Contribuyente.Nota.DataBindings.Add("Text", this.currentPersona, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RelacionComercial.DataSource = this.currentPersona.Relaciones;
        }

        private void Inabilita() {
            // informacion general
            this.TContribuyente.Nuevo.Enabled = this._permisos.Agregar;
            this.Contribuyente.Clave.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.RFC.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.CURP.ReadOnly = this.currentPersona.IdDirectorio > 0;
            //this.controlContribuyente.Nombre.ReadOnly = this.currentPersona.Id > 0;
            this.Contribuyente.NumRegIdTrib.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Telefono.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Sitio.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Correo.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Nota.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Clave.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.CURP.ReadOnly = this.currentPersona.IdDirectorio > 0;
        }

        private void Guardar_DoWork(object sender, DoWorkEventArgs e) {
            this.currentPersona = service.Save(currentPersona);
        }

        private void Guardar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CreateBinding();
            this.Inabilita();
        }

        private void VerificarRFC_DoWork(object sender, DoWorkEventArgs e) {
            Console.WriteLine("Consultando RFC");
            this.Tag = this.service.GetByRFC(this.currentPersona.RFC);
        }

        private void VerificarRFC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (this.Tag != null) {
                var _persona = this.Tag as ContribuyenteDetailModel;
                if (_persona != null) {
                    Console.WriteLine("se encontro RFC");
                    this.Text = _persona.RFC;
                    if (RadMessageBox.Show(this, "El RFC ya se encuetra registrado, ¿quieres agregar la relacion?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        this.currentPersona = _persona;
                        this.Tag = null;
                        this.TContribuyente.Actualizar.PerformClick();
                    }
                }
            }
            this.VerificarRFC.Dispose();
        }
        #endregion
    }
}
