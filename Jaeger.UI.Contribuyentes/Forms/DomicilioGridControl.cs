﻿using System;
using Jaeger.UI.Contribuyentes.Builder;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class DomicilioGridControl : UI.Common.Forms.GridStandarControl {
        public DomicilioGridControl() : base() {
            this.InitializeComponent();
            this.Load += DomicilioGridControl_Load;
        }

        private void DomicilioGridControl_Load(object sender, EventArgs e) {
            this.ShowEditar = false;
            this.ShowFiltro = false;
            this.ShowCerrar = false;
            this.ShowActualizar = false;
            this.ShowCerrar = false;
        }

        private void InitializeComponent() {
            IDomicilioGridBuilder view = new DomicilioGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
