﻿namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ValidaRFCForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidaRFCForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TValida = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.Status = new Telerik.WinControls.UI.RadStatusStrip();
            this.lblStatus = new Telerik.WinControls.UI.RadLabelElement();
            this.title = new Telerik.WinControls.UI.RadLabel();
            this.gridRegistros = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(571, 50);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // TValida
            // 
            this.TValida.Dock = System.Windows.Forms.DockStyle.Top;
            this.TValida.Etiqueta = "";
            this.TValida.Location = new System.Drawing.Point(0, 50);
            this.TValida.Name = "TValida";
            this.TValida.ReadOnly = false;
            this.TValida.ShowActualizar = true;
            this.TValida.ShowAutorizar = false;
            this.TValida.ShowCerrar = true;
            this.TValida.ShowEditar = false;
            this.TValida.ShowExportarExcel = false;
            this.TValida.ShowFiltro = false;
            this.TValida.ShowGuardar = false;
            this.TValida.ShowHerramientas = true;
            this.TValida.ShowImagen = false;
            this.TValida.ShowImprimir = false;
            this.TValida.ShowNuevo = true;
            this.TValida.ShowRemover = true;
            this.TValida.Size = new System.Drawing.Size(571, 30);
            this.TValida.TabIndex = 3;
            // 
            // Status
            // 
            this.Status.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.lblStatus});
            this.Status.Location = new System.Drawing.Point(0, 424);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(571, 26);
            this.Status.TabIndex = 4;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.Status.SetSpring(this.lblStatus, false);
            this.lblStatus.Text = "Listo.";
            this.lblStatus.TextWrap = true;
            // 
            // title
            // 
            this.title.BackColor = System.Drawing.Color.White;
            this.title.Location = new System.Drawing.Point(12, 16);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(163, 18);
            this.title.TabIndex = 5;
            this.title.Text = "Validación de la clave en el RFC";
            // 
            // gridRegistros
            // 
            this.gridRegistros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRegistros.Location = new System.Drawing.Point(0, 80);
            // 
            // 
            // 
            this.gridRegistros.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Registro";
            gridViewTextBoxColumn1.HeaderText = "Núm.";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Registro";
            gridViewTextBoxColumn2.FieldName = "RFC";
            gridViewTextBoxColumn2.HeaderText = "RFC";
            gridViewTextBoxColumn2.MaxLength = 14;
            gridViewTextBoxColumn2.Name = "RFC";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 110;
            gridViewTextBoxColumn3.FieldName = "NombreRazonSocial";
            gridViewTextBoxColumn3.HeaderText = "Nombre ó Razon Social";
            gridViewTextBoxColumn3.MaxLength = 300;
            gridViewTextBoxColumn3.Name = "NombreRazonSocial";
            gridViewTextBoxColumn3.Width = 250;
            gridViewTextBoxColumn4.FieldName = "CodigoPostal";
            gridViewTextBoxColumn4.HeaderText = "C. P.";
            gridViewTextBoxColumn4.MaxLength = 5;
            gridViewTextBoxColumn4.Name = "CodigoPostal";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "Resultado";
            gridViewTextBoxColumn5.HeaderText = "Resultado";
            gridViewTextBoxColumn5.Name = "Resultado";
            gridViewTextBoxColumn5.Width = 250;
            this.gridRegistros.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5});
            this.gridRegistros.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridRegistros.Name = "gridRegistros";
            this.gridRegistros.ShowGroupPanel = false;
            this.gridRegistros.Size = new System.Drawing.Size(571, 344);
            this.gridRegistros.TabIndex = 6;
            this.gridRegistros.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.GridRegistros_RowsChanged);
            // 
            // ValidaRFCForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 450);
            this.Controls.Add(this.gridRegistros);
            this.Controls.Add(this.title);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.TValida);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidaRFCForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Valida RFC";
            this.Load += new System.EventHandler(this.ValidaRFCForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRegistros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Common.Forms.ToolBarStandarControl TValida;
        private Telerik.WinControls.UI.RadStatusStrip Status;
        private Telerik.WinControls.UI.RadLabel title;
        internal Telerik.WinControls.UI.RadGridView gridRegistros;
        private Telerik.WinControls.UI.RadLabelElement lblStatus;
    }
}