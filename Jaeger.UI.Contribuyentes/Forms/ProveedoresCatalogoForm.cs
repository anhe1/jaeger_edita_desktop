﻿using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class ProveedoresCatalogoForm : ContribuyentesForm {
        public ProveedoresCatalogoForm(UIMenuElement menuElement) : base(TipoRelacionComericalEnum.Proveedor) {
            this.Text = "Proveedores: Expediente";
            this.GContribuyente.Permisos = new UIAction(menuElement.Permisos);
            this.Load += ProveedoresCatalogoForm_Load;
        }

        private void ProveedoresCatalogoForm_Load(object sender, System.EventArgs e) {
        }
    }
}
