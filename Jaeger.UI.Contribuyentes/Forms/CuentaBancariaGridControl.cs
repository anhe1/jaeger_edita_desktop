﻿using System;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Services;
using Jaeger.UI.Contribuyentes.Builder;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class CuentaBancariaGridControl : UI.Common.Forms.GridStandarControl {
        protected internal bool ready = false;
        public CuentaBancariaGridControl() : base() {
            this.InitializeComponent();
            this.Load += DomicilioGridControl_Load;
        }

        private void InitializeComponent() {
            ICuentaBancariaGridBuilder view = new CuentaBancariaGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            this.GridData.CellBeginEdit += GridCuentaBancaria_CellBeginEdit;
        }

        private void DomicilioGridControl_Load(object sender, EventArgs e) {
            this.ShowEditar = false;
            this.ShowFiltro = false;
            this.ShowCerrar = false;
            this.ShowActualizar = false;
            this.ShowCerrar = false;
        }

        private void GridCuentaBancaria_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (DbConvert.ConvertBool(e.Row.Cells["Verificado"].Value) == true) {
                e.Cancel = true;
                return;
            }

            if (this.GridData.CurrentColumn is GridViewMultiComboBoxColumn) {
                RadMultiColumnComboBoxElement editor = (RadMultiColumnComboBoxElement)this.GridData.ActiveEditor;
                if (this.ready) { return; }
                editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Clave", Name = "Clave", FieldName = "Clave" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Descripción", Name = "Descripcion", FieldName = "Descripcion" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Razon Social", Name = "RazonSocial", FieldName = "RazonSocial" });
                editor.AutoSizeDropDownToBestFit = true;
                this.ready = true;
            }
        }

        /// <summary>
        /// Listado de bancos
        /// </summary>
        public void Items<T>(List<T> items) {
            var cbancos = (GridViewMultiComboBoxColumn)this.GridData.Columns["Banco"];
            cbancos.DisplayMember = "RazonSocial";
            cbancos.ValueMember = "RazonSocial";
            cbancos.DataSource = items;
        }
    }
}
