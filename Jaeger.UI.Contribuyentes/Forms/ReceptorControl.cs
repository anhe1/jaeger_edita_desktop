﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Catalogos;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class ReceptorControl : Common.Forms.ReceptorControl {
        protected IDirectorioService service;
        protected IUsoCFDICatalogo catalogoUsoCfdi = new UsoCFDICatalogo();
        protected IRegimenesFiscalesCatalogo regimenesfiscales = new RegimenesFiscalesCatalogo();
        protected BackgroundWorker bkReceptores = new BackgroundWorker();
        protected internal List<ContribuyenteModel> dsDirectorio;

        public ReceptorControl() {
            this.Load += this.ReceptorControl_Load;
        }

        private void ReceptorControl_Load(object sender, EventArgs e) {
            this.bkReceptores.DoWork += BkReceptores_DoWork;
            this.bkReceptores.RunWorkerCompleted += BkReceptores_RunWorkerCompleted;
            this.NumRegIdTrib.TextBoxElement.ToolTipText = "Número de registro de identidad fiscal cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.";
        }

        public TipoRelacionComericalEnum RelacionComerical { get; set; }

        public virtual void OnLoad() {
            this.Nombre.Enabled = false;
            if (!this.bkReceptores.IsBusy) {
                this.bkReceptores.RunWorkerAsync();
            }
        }

        public virtual void Receptor_SelectedValueChanged(object sender, EventArgs e) {
            var rowInfo = this.Nombre.SelectedItem as GridViewRowInfo;
            if (!(rowInfo == null)) {
                var receptor = rowInfo.DataBoundItem as ContribuyenteModel;
                if (receptor != null) {
                    this.RFC.Text = receptor.RFC;
                    this.Nombre.Text = receptor.Nombre;
                    this.IdDirectorio.Value = receptor.IdDirectorio;

                    if (receptor.DomicilioFiscal != null) {
                        this.DomicilioFiscal.Text = receptor.DomicilioFiscal;
                    }

                    if (receptor.ClaveUsoCFDI == null) {
                        this.UsoCFDI.SelectedValue = "G01";
                    } else {
                        this.UsoCFDI.SelectedValue = receptor.ClaveUsoCFDI;
                    }

                    if (receptor.RegimenFiscal != null) {
                        this.RegimenFiscal.SelectedValue = receptor.RegimenFiscal;
                    }
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Objeto_NoValido, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void Receptor_DropDownOpened(object sender, EventArgs e) {
            this.Nombre.DataSource = this.dsDirectorio;
        }

        private void BkReceptores_DoWork(object sender, DoWorkEventArgs e) {
            this.Actualizar.Enabled = false;
            this.Nombre.Enabled = false;
            this.dsDirectorio = this.service.GetList<ContribuyenteModel>(true);
        }

        private void BkReceptores_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.SelectedIndex = -1;
            this.Nombre.SelectedValue = null;
            this.Nombre.Enabled = true;
            this.Actualizar.Enabled = true;
            this.Nombre.Enabled = true;
            this.Nombre.SelectedValueChanged += new EventHandler(this.Receptor_SelectedValueChanged);
            this.Nombre.DropDownOpened += new EventHandler(this.Receptor_DropDownOpened);
        }
    }
}
