﻿
namespace Jaeger.UI.Contribuyentes.Forms {
    partial class BuscarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuscarForm));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.TSearch = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblBuscar = new Telerik.WinControls.UI.CommandBarLabel();
            this.Descripcion = new Telerik.WinControls.UI.CommandBarTextBox();
            this.Buscar = new Telerik.WinControls.UI.CommandBarButton();
            this.Agregar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.searchControl = new Jaeger.UI.Contribuyentes.Forms.ContribuyenteSearchControl();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(574, 30);
            this.radCommandBar1.TabIndex = 3;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.TSearch});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.UseCompatibleTextRendering = false;
            // 
            // TSearch
            // 
            this.TSearch.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TSearch.DisplayName = "commandBarStripElement1";
            this.TSearch.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblBuscar,
            this.Descripcion,
            this.Buscar,
            this.Agregar,
            this.Cerrar});
            this.TSearch.Name = "TSearch";
            this.TSearch.StretchHorizontally = true;
            this.TSearch.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.TSearch.UseCompatibleTextRendering = false;
            // 
            // lblBuscar
            // 
            this.lblBuscar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblBuscar.DisplayName = "Etiqueta: Producto";
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Text = "Descripción";
            this.lblBuscar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.lblBuscar.UseCompatibleTextRendering = false;
            // 
            // Descripcion
            // 
            this.Descripcion.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Descripcion.DisplayName = "Descripción";
            this.Descripcion.MaxSize = new System.Drawing.Size(260, 22);
            this.Descripcion.MinSize = new System.Drawing.Size(260, 22);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.StretchHorizontally = false;
            this.Descripcion.StretchVertically = false;
            this.Descripcion.Text = "";
            this.Descripcion.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Descripcion.UseCompatibleTextRendering = false;
            // 
            // Buscar
            // 
            this.Buscar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Buscar.DisplayName = "Buscar";
            this.Buscar.DrawText = true;
            this.Buscar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.search_16px;
            this.Buscar.Name = "Buscar";
            this.Buscar.Text = "Buscar";
            this.Buscar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Buscar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Buscar.UseCompatibleTextRendering = false;
            // 
            // Agregar
            // 
            this.Agregar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Agregar.DisplayName = "commandBarButton1";
            this.Agregar.DrawText = true;
            this.Agregar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.add_16px;
            this.Agregar.Name = "Agregar";
            this.Agregar.Text = "Agregar";
            this.Agregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Agregar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Agregar.UseCompatibleTextRendering = false;
            // 
            // Cerrar
            // 
            this.Cerrar.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cerrar.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Cerrar.UseCompatibleTextRendering = false;
            // 
            // searchControl
            // 
            this.searchControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchControl.Location = new System.Drawing.Point(0, 30);
            this.searchControl.Name = "searchControl";
            this.searchControl.Nombre = null;
            this.searchControl.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.searchControl.Service = null;
            this.searchControl.Size = new System.Drawing.Size(574, 199);
            this.searchControl.TabIndex = 4;
            // 
            // BuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 229);
            this.Controls.Add(this.searchControl);
            this.Controls.Add(this.radCommandBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar";
            this.Load += new System.EventHandler(this.BuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement TSearch;
        private Telerik.WinControls.UI.CommandBarLabel lblBuscar;
        public Telerik.WinControls.UI.CommandBarTextBox Descripcion;
        public Telerik.WinControls.UI.CommandBarButton Buscar;
        public Telerik.WinControls.UI.CommandBarButton Agregar;
        public Telerik.WinControls.UI.CommandBarButton Cerrar;
        public ContribuyenteSearchControl searchControl;
    }
}