﻿
namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ContribuyenteSearchControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.GResult = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.GResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GResult.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            this.SuspendLayout();
            // 
            // GResult
            // 
            this.GResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GResult.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.GResult.MasterTemplate.AllowAddNewRow = false;
            this.GResult.MasterTemplate.AllowColumnChooser = false;
            this.GResult.MasterTemplate.AllowDeleteRow = false;
            this.GResult.MasterTemplate.AllowDragToGroup = false;
            this.GResult.MasterTemplate.AllowEditRow = false;
            this.GResult.MasterTemplate.AllowRowResize = false;
            this.GResult.MasterTemplate.ShowFilteringRow = false;
            this.GResult.MasterTemplate.ShowRowHeaderColumn = false;
            this.GResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GResult.Name = "GResult";
            this.GResult.ShowGroupPanel = false;
            this.GResult.Size = new System.Drawing.Size(736, 249);
            this.GResult.TabIndex = 16;
            this.GResult.DoubleClick += new System.EventHandler(this.GridResult_DoubleClick);
            this.GResult.Resize += new System.EventHandler(this.GridResult_Resize);
            // 
            // Espera
            // 
            this.Espera.Location = new System.Drawing.Point(301, 121);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(151, 24);
            this.Espera.TabIndex = 15;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.Visible = false;
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            this.dotsLineWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // ContribuyenteSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Espera);
            this.Controls.Add(this.GResult);
            this.Name = "ContribuyenteSearchControl";
            this.Size = new System.Drawing.Size(736, 249);
            this.Load += new System.EventHandler(this.ContribuyenteSearchControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Telerik.WinControls.UI.RadGridView GResult;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
    }
}
