﻿using System;
using Telerik.WinControls.UI;
using Jaeger.SAT.RFC.Valida;
using Jaeger.SAT.RFC.Valida.Interfaces;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ValidarRFCToForm : RadForm {
        public event EventHandler<IRegistro> Agregado;

        public void OnConsultaChanged(IRegistro e) {
            if (this.Agregado != null) 
                this.Agregado(this, e);
        }

        public ValidarRFCToForm() {
            InitializeComponent();
        }

        private void ValidarRFCToForm_Load(object sender, EventArgs e) {
        }
        
        private void Agregar_Click(object sender, EventArgs e) {
            if (this.Validar()) {
                var _Registro = new Registro {
                    CodigoPostal = this.CodigoPostal.Text,
                    NombreRazonSocial = this.Nombre.Text,
                    RFC = this.RFC.Text
                };
                this.OnConsultaChanged(_Registro);
                this.Close();
            }
        }

        private void Cancelar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private bool Validar() {
            this.ProviderError.Clear();
            if (string.IsNullOrEmpty(this.Nombre.Text)) {
                this.ProviderError.SetError(this.Nombre, "No puede estar vacío");
                return false;
            }

            if (string.IsNullOrEmpty(this.RFC.Text)) {
                this.ProviderError.SetError(this.RFC, "Registro Federal de causantes no válido");
                return false;
            }

            if (Domain.Base.Services.ValidacionService.RFC(this.RFC.Text) == false) {
                this.ProviderError.SetError(this.RFC, "Registro Federal de causantes no válido");
                return false;
            }

            if (string.IsNullOrEmpty(this.CodigoPostal.Text)) {
                this.ProviderError.SetError(this.CodigoPostal, "Código postal no válido");
                return false;
            }

            if (Domain.Base.Services.ValidacionService.CodigoPostal(this.CodigoPostal.Text) == false) {
                return false;
            }

            return true;
        }
    }
}
