﻿namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ContribuyenteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction2 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction3 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            Telerik.WinControls.UI.GridViewMultiComboBoxColumn gridViewMultiComboBoxColumn1 = new Telerik.WinControls.UI.GridViewMultiComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContribuyenteForm));
            this.lblIVA = new Telerik.WinControls.UI.RadLabel();
            this.lblDescuento = new Telerik.WinControls.UI.RadLabel();
            this.lblLimiteCredito = new Telerik.WinControls.UI.RadLabel();
            this.lblDiasEntrega = new Telerik.WinControls.UI.RadLabel();
            this.lblDiasCredito = new Telerik.WinControls.UI.RadLabel();
            this.PageView = new Telerik.WinControls.UI.RadPageView();
            this.PageDomicilio = new Telerik.WinControls.UI.RadPageViewPage();
            this.TDomicilio = new Jaeger.UI.Contribuyentes.Forms.DomicilioGridControl();
            this.PageCuentaBancaria = new Telerik.WinControls.UI.RadPageViewPage();
            this.TCuentaBancaria = new Jaeger.UI.Contribuyentes.Forms.CuentaBancariaGridControl();
            this.PageContactos = new Telerik.WinControls.UI.RadPageViewPage();
            this.TContacto = new Jaeger.UI.Contribuyentes.Forms.ContactosGridControl();
            this.PageVendedor = new Telerik.WinControls.UI.RadPageViewPage();
            this.GridVendedores = new Telerik.WinControls.UI.RadGridView();
            this.TVendedor = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.PageCredito = new Telerik.WinControls.UI.RadPageViewPage();
            this.Descuento = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.LimiteCredito = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.DiasEntrega = new Telerik.WinControls.UI.RadSpinEditor();
            this.DiasCredito = new Telerik.WinControls.UI.RadSpinEditor();
            this.PactadoIVA = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.Panel = new Telerik.WinControls.UI.RadPanel();
            this.Contribuyente = new Jaeger.UI.Contribuyentes.Forms.ContribuyenteControl();
            this.Foto = new System.Windows.Forms.PictureBox();
            this.Estado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEspera = new Telerik.WinControls.UI.RadWaitingBarElement();
            this.lblVerificar = new Telerik.WinControls.UI.RadLabelElement();
            this.LineWaitingIndicator = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.VerificarRFC = new System.ComponentModel.BackgroundWorker();
            this.TContribuyente = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLimiteCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiasEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiasCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).BeginInit();
            this.PageView.SuspendLayout();
            this.PageDomicilio.SuspendLayout();
            this.PageCuentaBancaria.SuspendLayout();
            this.PageContactos.SuspendLayout();
            this.PageVendedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridVendedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridVendedores.MasterTemplate)).BeginInit();
            this.PageCredito.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LimiteCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasCredito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PactadoIVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel)).BeginInit();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Foto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIVA
            // 
            this.lblIVA.Location = new System.Drawing.Point(15, 120);
            this.lblIVA.Name = "lblIVA";
            this.lblIVA.Size = new System.Drawing.Size(36, 18);
            this.lblIVA.TabIndex = 56;
            this.lblIVA.Text = "IVA %";
            // 
            // lblDescuento
            // 
            this.lblDescuento.Location = new System.Drawing.Point(15, 94);
            this.lblDescuento.Name = "lblDescuento";
            this.lblDescuento.Size = new System.Drawing.Size(72, 18);
            this.lblDescuento.TabIndex = 55;
            this.lblDescuento.Text = "Descuento %";
            // 
            // lblLimiteCredito
            // 
            this.lblLimiteCredito.Location = new System.Drawing.Point(15, 68);
            this.lblLimiteCredito.Name = "lblLimiteCredito";
            this.lblLimiteCredito.Size = new System.Drawing.Size(69, 18);
            this.lblLimiteCredito.TabIndex = 54;
            this.lblLimiteCredito.Text = "Lím. Crédito:";
            // 
            // lblDiasEntrega
            // 
            this.lblDiasEntrega.Location = new System.Drawing.Point(15, 42);
            this.lblDiasEntrega.Name = "lblDiasEntrega";
            this.lblDiasEntrega.Size = new System.Drawing.Size(71, 18);
            this.lblDiasEntrega.TabIndex = 52;
            this.lblDiasEntrega.Text = "Días Entrega:";
            // 
            // lblDiasCredito
            // 
            this.lblDiasCredito.Location = new System.Drawing.Point(15, 16);
            this.lblDiasCredito.Name = "lblDiasCredito";
            this.lblDiasCredito.Size = new System.Drawing.Size(70, 18);
            this.lblDiasCredito.TabIndex = 51;
            this.lblDiasCredito.Text = "Días Crédito:";
            // 
            // PageView
            // 
            this.PageView.Controls.Add(this.PageDomicilio);
            this.PageView.Controls.Add(this.PageCuentaBancaria);
            this.PageView.Controls.Add(this.PageContactos);
            this.PageView.Controls.Add(this.PageVendedor);
            this.PageView.Controls.Add(this.PageCredito);
            this.PageView.DefaultPage = this.PageDomicilio;
            this.PageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageView.Location = new System.Drawing.Point(0, 220);
            this.PageView.Name = "PageView";
            this.PageView.SelectedPage = this.PageDomicilio;
            this.PageView.Size = new System.Drawing.Size(878, 224);
            this.PageView.TabIndex = 65;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PageDomicilio
            // 
            this.PageDomicilio.Controls.Add(this.TDomicilio);
            this.PageDomicilio.ItemSize = new System.Drawing.SizeF(68F, 28F);
            this.PageDomicilio.Location = new System.Drawing.Point(10, 37);
            this.PageDomicilio.Name = "PageDomicilio";
            this.PageDomicilio.Size = new System.Drawing.Size(857, 176);
            this.PageDomicilio.Text = "Domicilios";
            // 
            // TDomicilio
            // 
            this.TDomicilio.Caption = "";
            this.TDomicilio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TDomicilio.Location = new System.Drawing.Point(0, 0);
            this.TDomicilio.Name = "TDomicilio";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TDomicilio.Permisos = uiAction1;
            this.TDomicilio.ShowActualizar = false;
            this.TDomicilio.ShowAutorizar = false;
            this.TDomicilio.ShowAutosuma = false;
            this.TDomicilio.ShowCerrar = false;
            this.TDomicilio.ShowEditar = false;
            this.TDomicilio.ShowExportarExcel = false;
            this.TDomicilio.ShowFiltro = false;
            this.TDomicilio.ShowHerramientas = false;
            this.TDomicilio.ShowImagen = false;
            this.TDomicilio.ShowImprimir = false;
            this.TDomicilio.ShowNuevo = true;
            this.TDomicilio.ShowRemover = true;
            this.TDomicilio.ShowSeleccionMultiple = false;
            this.TDomicilio.Size = new System.Drawing.Size(857, 176);
            this.TDomicilio.TabIndex = 3;
            // 
            // PageCuentaBancaria
            // 
            this.PageCuentaBancaria.Controls.Add(this.TCuentaBancaria);
            this.PageCuentaBancaria.ItemSize = new System.Drawing.SizeF(106F, 28F);
            this.PageCuentaBancaria.Location = new System.Drawing.Point(10, 37);
            this.PageCuentaBancaria.Name = "PageCuentaBancaria";
            this.PageCuentaBancaria.Size = new System.Drawing.Size(857, 176);
            this.PageCuentaBancaria.Text = "Cuentas de Banco";
            // 
            // TCuentaBancaria
            // 
            this.TCuentaBancaria.Caption = "";
            this.TCuentaBancaria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCuentaBancaria.Location = new System.Drawing.Point(0, 0);
            this.TCuentaBancaria.Name = "TCuentaBancaria";
            uiAction2.Agregar = false;
            uiAction2.Autorizar = false;
            uiAction2.Cancelar = false;
            uiAction2.Editar = false;
            uiAction2.Exportar = false;
            uiAction2.Importar = false;
            uiAction2.Imprimir = false;
            uiAction2.Remover = false;
            uiAction2.Reporte = false;
            uiAction2.Status = false;
            this.TCuentaBancaria.Permisos = uiAction2;
            this.TCuentaBancaria.ShowActualizar = true;
            this.TCuentaBancaria.ShowAutorizar = false;
            this.TCuentaBancaria.ShowAutosuma = false;
            this.TCuentaBancaria.ShowCerrar = true;
            this.TCuentaBancaria.ShowEditar = true;
            this.TCuentaBancaria.ShowExportarExcel = false;
            this.TCuentaBancaria.ShowFiltro = true;
            this.TCuentaBancaria.ShowHerramientas = false;
            this.TCuentaBancaria.ShowImagen = false;
            this.TCuentaBancaria.ShowImprimir = false;
            this.TCuentaBancaria.ShowNuevo = true;
            this.TCuentaBancaria.ShowRemover = true;
            this.TCuentaBancaria.ShowSeleccionMultiple = true;
            this.TCuentaBancaria.Size = new System.Drawing.Size(857, 176);
            this.TCuentaBancaria.TabIndex = 4;
            // 
            // PageContactos
            // 
            this.PageContactos.Controls.Add(this.TContacto);
            this.PageContactos.ItemSize = new System.Drawing.SizeF(66F, 28F);
            this.PageContactos.Location = new System.Drawing.Point(10, 37);
            this.PageContactos.Name = "PageContactos";
            this.PageContactos.Size = new System.Drawing.Size(857, 176);
            this.PageContactos.Text = "Contactos";
            // 
            // TContacto
            // 
            this.TContacto.Caption = "";
            this.TContacto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TContacto.Location = new System.Drawing.Point(0, 0);
            this.TContacto.Name = "TContacto";
            uiAction3.Agregar = false;
            uiAction3.Autorizar = false;
            uiAction3.Cancelar = false;
            uiAction3.Editar = false;
            uiAction3.Exportar = false;
            uiAction3.Importar = false;
            uiAction3.Imprimir = false;
            uiAction3.Remover = false;
            uiAction3.Reporte = false;
            uiAction3.Status = false;
            this.TContacto.Permisos = uiAction3;
            this.TContacto.ShowActualizar = true;
            this.TContacto.ShowAutorizar = false;
            this.TContacto.ShowAutosuma = false;
            this.TContacto.ShowCerrar = true;
            this.TContacto.ShowEditar = true;
            this.TContacto.ShowExportarExcel = false;
            this.TContacto.ShowFiltro = true;
            this.TContacto.ShowHerramientas = false;
            this.TContacto.ShowImagen = false;
            this.TContacto.ShowImprimir = false;
            this.TContacto.ShowNuevo = true;
            this.TContacto.ShowRemover = true;
            this.TContacto.ShowSeleccionMultiple = true;
            this.TContacto.Size = new System.Drawing.Size(857, 176);
            this.TContacto.TabIndex = 28;
            // 
            // PageVendedor
            // 
            this.PageVendedor.Controls.Add(this.GridVendedores);
            this.PageVendedor.Controls.Add(this.TVendedor);
            this.PageVendedor.ItemSize = new System.Drawing.SizeF(116F, 28F);
            this.PageVendedor.Location = new System.Drawing.Point(10, 37);
            this.PageVendedor.Name = "PageVendedor";
            this.PageVendedor.Size = new System.Drawing.Size(857, 176);
            this.PageVendedor.Text = "Vendedor Asignado";
            // 
            // GridVendedores
            // 
            this.GridVendedores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridVendedores.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridVendedores.MasterTemplate.AllowAddNewRow = false;
            gridViewMultiComboBoxColumn1.FieldName = "IdVendedor";
            gridViewMultiComboBoxColumn1.HeaderText = "Vendedor";
            gridViewMultiComboBoxColumn1.Name = "IdVendedor";
            gridViewMultiComboBoxColumn1.Width = 200;
            gridViewTextBoxColumn1.DataType = typeof(decimal);
            gridViewTextBoxColumn1.FieldName = "Comision";
            gridViewTextBoxColumn1.FormatString = "{0:n}";
            gridViewTextBoxColumn1.HeaderText = "Comisión";
            gridViewTextBoxColumn1.Name = "Comision";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn1.Width = 65;
            this.GridVendedores.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewMultiComboBoxColumn1,
            gridViewTextBoxColumn1});
            this.GridVendedores.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridVendedores.Name = "GridVendedores";
            this.GridVendedores.ShowGroupPanel = false;
            this.GridVendedores.Size = new System.Drawing.Size(857, 146);
            this.GridVendedores.TabIndex = 1;
            this.GridVendedores.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.GridVendedor_CellBeginEdit);
            this.GridVendedores.SelectionChanged += new System.EventHandler(this.GridVendedor_SelectionChanged);
            // 
            // TVendedor
            // 
            this.TVendedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.TVendedor.Etiqueta = "";
            this.TVendedor.Location = new System.Drawing.Point(0, 0);
            this.TVendedor.Name = "TVendedor";
            this.TVendedor.ReadOnly = false;
            this.TVendedor.ShowActualizar = false;
            this.TVendedor.ShowAutorizar = false;
            this.TVendedor.ShowCerrar = false;
            this.TVendedor.ShowEditar = false;
            this.TVendedor.ShowExportarExcel = false;
            this.TVendedor.ShowFiltro = false;
            this.TVendedor.ShowGuardar = false;
            this.TVendedor.ShowHerramientas = false;
            this.TVendedor.ShowImagen = false;
            this.TVendedor.ShowImprimir = false;
            this.TVendedor.ShowNuevo = true;
            this.TVendedor.ShowRemover = true;
            this.TVendedor.Size = new System.Drawing.Size(857, 30);
            this.TVendedor.TabIndex = 2;
            // 
            // PageCredito
            // 
            this.PageCredito.Controls.Add(this.Descuento);
            this.PageCredito.Controls.Add(this.LimiteCredito);
            this.PageCredito.Controls.Add(this.DiasEntrega);
            this.PageCredito.Controls.Add(this.DiasCredito);
            this.PageCredito.Controls.Add(this.PactadoIVA);
            this.PageCredito.Controls.Add(this.lblIVA);
            this.PageCredito.Controls.Add(this.lblDescuento);
            this.PageCredito.Controls.Add(this.lblDiasCredito);
            this.PageCredito.Controls.Add(this.lblLimiteCredito);
            this.PageCredito.Controls.Add(this.lblDiasEntrega);
            this.PageCredito.ItemSize = new System.Drawing.SizeF(53F, 28F);
            this.PageCredito.Location = new System.Drawing.Point(10, 37);
            this.PageCredito.Name = "PageCredito";
            this.PageCredito.Size = new System.Drawing.Size(857, 176);
            this.PageCredito.Text = "Crédito";
            // 
            // Descuento
            // 
            this.Descuento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Descuento.BackColor = System.Drawing.SystemColors.Window;
            this.Descuento.Location = new System.Drawing.Point(97, 93);
            this.Descuento.Mask = "N2";
            this.Descuento.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.Descuento.Name = "Descuento";
            this.Descuento.Size = new System.Drawing.Size(70, 20);
            this.Descuento.TabIndex = 102;
            this.Descuento.TabStop = false;
            this.Descuento.Text = "0.00";
            this.Descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LimiteCredito
            // 
            this.LimiteCredito.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LimiteCredito.BackColor = System.Drawing.SystemColors.Window;
            this.LimiteCredito.Location = new System.Drawing.Point(95, 67);
            this.LimiteCredito.Mask = "N2";
            this.LimiteCredito.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.LimiteCredito.Name = "LimiteCredito";
            this.LimiteCredito.Size = new System.Drawing.Size(70, 20);
            this.LimiteCredito.TabIndex = 101;
            this.LimiteCredito.TabStop = false;
            this.LimiteCredito.Text = "0.00";
            this.LimiteCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // DiasEntrega
            // 
            this.DiasEntrega.Location = new System.Drawing.Point(97, 41);
            this.DiasEntrega.Name = "DiasEntrega";
            this.DiasEntrega.Size = new System.Drawing.Size(70, 20);
            this.DiasEntrega.TabIndex = 100;
            this.DiasEntrega.TabStop = false;
            this.DiasEntrega.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DiasCredito
            // 
            this.DiasCredito.Location = new System.Drawing.Point(97, 15);
            this.DiasCredito.Name = "DiasCredito";
            this.DiasCredito.Size = new System.Drawing.Size(70, 20);
            this.DiasCredito.TabIndex = 99;
            this.DiasCredito.TabStop = false;
            this.DiasCredito.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PactadoIVA
            // 
            this.PactadoIVA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PactadoIVA.BackColor = System.Drawing.SystemColors.Window;
            this.PactadoIVA.Location = new System.Drawing.Point(97, 119);
            this.PactadoIVA.Mask = "N2";
            this.PactadoIVA.MaskType = Telerik.WinControls.UI.MaskType.Numeric;
            this.PactadoIVA.Name = "PactadoIVA";
            this.PactadoIVA.Size = new System.Drawing.Size(70, 20);
            this.PactadoIVA.TabIndex = 98;
            this.PactadoIVA.TabStop = false;
            this.PactadoIVA.Text = "0.00";
            this.PactadoIVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Panel
            // 
            this.Panel.Controls.Add(this.Contribuyente);
            this.Panel.Controls.Add(this.Foto);
            this.Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel.Location = new System.Drawing.Point(0, 29);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(878, 191);
            this.Panel.TabIndex = 66;
            // 
            // Contribuyente
            // 
            this.Contribuyente.Location = new System.Drawing.Point(4, 2);
            this.Contribuyente.MinimumSize = new System.Drawing.Size(680, 186);
            this.Contribuyente.Name = "Contribuyente";
            this.Contribuyente.Size = new System.Drawing.Size(683, 186);
            this.Contribuyente.TabIndex = 66;
            // 
            // Foto
            // 
            this.Foto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Foto.Location = new System.Drawing.Point(698, 8);
            this.Foto.Name = "Foto";
            this.Foto.Size = new System.Drawing.Size(168, 177);
            this.Foto.TabIndex = 65;
            this.Foto.TabStop = false;
            // 
            // Estado
            // 
            this.Estado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEspera,
            this.lblVerificar});
            this.Estado.Location = new System.Drawing.Point(0, 444);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(878, 26);
            this.Estado.SizingGrip = false;
            this.Estado.TabIndex = 67;
            // 
            // BarraEspera
            // 
            this.BarraEspera.Name = "BarraEspera";
            this.Estado.SetSpring(this.BarraEspera, false);
            this.BarraEspera.Text = "";
            // 
            // lblVerificar
            // 
            this.lblVerificar.Name = "lblVerificar";
            this.Estado.SetSpring(this.lblVerificar, false);
            this.lblVerificar.Text = "...";
            this.lblVerificar.TextWrap = true;
            // 
            // LineWaitingIndicator
            // 
            this.LineWaitingIndicator.Name = "LineWaitingIndicator";
            // 
            // VerificarRFC
            // 
            this.VerificarRFC.DoWork += new System.ComponentModel.DoWorkEventHandler(this.VerificarRFC_DoWork);
            this.VerificarRFC.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.VerificarRFC_RunWorkerCompleted);
            // 
            // TContribuyente
            // 
            this.TContribuyente.Dock = System.Windows.Forms.DockStyle.Top;
            this.TContribuyente.Etiqueta = "";
            this.TContribuyente.Location = new System.Drawing.Point(0, 0);
            this.TContribuyente.Name = "TContribuyente";
            this.TContribuyente.ReadOnly = false;
            this.TContribuyente.ShowActualizar = true;
            this.TContribuyente.ShowAutorizar = false;
            this.TContribuyente.ShowCerrar = true;
            this.TContribuyente.ShowEditar = false;
            this.TContribuyente.ShowExportarExcel = false;
            this.TContribuyente.ShowFiltro = false;
            this.TContribuyente.ShowGuardar = true;
            this.TContribuyente.ShowHerramientas = false;
            this.TContribuyente.ShowImagen = false;
            this.TContribuyente.ShowImprimir = false;
            this.TContribuyente.ShowNuevo = true;
            this.TContribuyente.ShowRemover = false;
            this.TContribuyente.Size = new System.Drawing.Size(878, 29);
            this.TContribuyente.TabIndex = 1;
            // 
            // ContribuyenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 470);
            this.Controls.Add(this.PageView);
            this.Controls.Add(this.Panel);
            this.Controls.Add(this.TContribuyente);
            this.Controls.Add(this.Estado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContribuyenteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contacto";
            this.Load += new System.EventHandler(this.ContribuyenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLimiteCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiasEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDiasCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageView)).EndInit();
            this.PageView.ResumeLayout(false);
            this.PageDomicilio.ResumeLayout(false);
            this.PageCuentaBancaria.ResumeLayout(false);
            this.PageContactos.ResumeLayout(false);
            this.PageVendedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridVendedores.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridVendedores)).EndInit();
            this.PageCredito.ResumeLayout(false);
            this.PageCredito.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Descuento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LimiteCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiasCredito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PactadoIVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Panel)).EndInit();
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Foto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Estado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblIVA;
        private Telerik.WinControls.UI.RadLabel lblDescuento;
        private Telerik.WinControls.UI.RadLabel lblLimiteCredito;
        private Telerik.WinControls.UI.RadLabel lblDiasEntrega;
        private Telerik.WinControls.UI.RadLabel lblDiasCredito;
        private Telerik.WinControls.UI.RadPageView PageView;
        private Telerik.WinControls.UI.RadPageViewPage PageDomicilio;
        private Telerik.WinControls.UI.RadPanel Panel;
        private Telerik.WinControls.UI.RadPageViewPage PageCuentaBancaria;
        private Telerik.WinControls.UI.RadPageViewPage PageContactos;
        private System.Windows.Forms.PictureBox Foto;
        private Telerik.WinControls.UI.RadStatusStrip Estado;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement LineWaitingIndicator;
        private Telerik.WinControls.UI.RadWaitingBarElement BarraEspera;
        private Telerik.WinControls.UI.RadPageViewPage PageVendedor;
        protected internal Telerik.WinControls.UI.RadGridView GridVendedores;
        protected internal ContribuyenteControl Contribuyente;
        protected internal Common.Forms.ToolBarStandarControl TContribuyente;
        protected internal Common.Forms.ToolBarStandarControl TVendedor;
        private System.ComponentModel.BackgroundWorker VerificarRFC;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox PactadoIVA;
        protected internal Telerik.WinControls.UI.RadSpinEditor DiasCredito;
        protected internal Telerik.WinControls.UI.RadSpinEditor DiasEntrega;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox LimiteCredito;
        protected internal Telerik.WinControls.UI.RadMaskedEditBox Descuento;
        private Telerik.WinControls.UI.RadLabelElement lblVerificar;
        protected internal Telerik.WinControls.UI.RadPageViewPage PageCredito;
        private DomicilioGridControl TDomicilio;
        private CuentaBancariaGridControl TCuentaBancaria;
        private ContactosGridControl TContacto;
    }
}
