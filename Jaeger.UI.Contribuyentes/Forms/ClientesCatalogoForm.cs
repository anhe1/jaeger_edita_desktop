﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class ClientesCatalogoForm : ContribuyentesForm {
        protected internal RadMenuItem TContribuyente_ValidarRFC = new RadMenuItem { Text = "Validación masiva de RFC" };
        protected internal RadMenuItem MContextual_ValidarRFC = new RadMenuItem { Text = "Validación de RFC" };

        public ClientesCatalogoForm() : base() { }

        public ClientesCatalogoForm(UIMenuElement menuElement) : base(TipoRelacionComericalEnum.Cliente) {
            this.Text = "Clientes: Expediente";
            this.GContribuyente.Permisos = new UIAction(menuElement.Permisos);
            this.Load += ClientesCatalogoForm_Load;
        }

        private void ClientesCatalogoForm_Load(object sender, EventArgs e) {
            this.GContribuyente.Herramientas.Items.Add(this.TContribuyente_ValidarRFC);
            this.GContribuyente.menuContextual.Items.AddRange(this.MContextual_ValidarRFC);

            this.MContextual_ValidarRFC.Click += this.TContribuyente_ValidarRFC_Click;
            this.TContribuyente_ValidarRFC.Click += this.MContextual_ValidarRFC_Click;
        }
    }
}
