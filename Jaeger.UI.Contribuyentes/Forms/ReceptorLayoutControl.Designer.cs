﻿
namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ReceptorLayoutControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Agregar = new Telerik.WinControls.UI.RadButton();
            this.Actualizar = new Telerik.WinControls.UI.RadButton();
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.Domicilio = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.RFC = new Telerik.WinControls.UI.RadTextBox();
            this.Nombre = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.lblNombre = new Telerik.WinControls.UI.RadLabel();
            this.lblRFC = new Telerik.WinControls.UI.RadLabel();
            this.lblDomicilio = new Telerik.WinControls.UI.RadLabel();
            this.IdDirectorio = new Telerik.WinControls.UI.RadSpinEditor();
            this.Buscar = new Telerik.WinControls.UI.RadButton();
            this.IdDomicilio = new Telerik.WinControls.UI.RadSpinEditor();
            this.IdTipoRemision = new Telerik.WinControls.UI.RadSpinEditor();
            this.panelRFC = new System.Windows.Forms.Panel();
            this.panelNombre = new System.Windows.Forms.Panel();
            this.Advertencia = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio)).BeginInit();
            this.IdDomicilio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdTipoRemision)).BeginInit();
            this.panelRFC.SuspendLayout();
            this.panelNombre.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).BeginInit();
            this.SuspendLayout();
            // 
            // Agregar
            // 
            this.Agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Agregar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Agregar.Enabled = false;
            this.Agregar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.add_user_male_16px;
            this.Agregar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Agregar.Location = new System.Drawing.Point(607, 0);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(20, 20);
            this.Agregar.TabIndex = 13;
            this.Agregar.Text = "radButton1";
            // 
            // Actualizar
            // 
            this.Actualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Actualizar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Actualizar.Enabled = false;
            this.Actualizar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.refresh_16;
            this.Actualizar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Actualizar.Location = new System.Drawing.Point(563, 0);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(20, 20);
            this.Actualizar.TabIndex = 12;
            this.Actualizar.Text = "radButton1";
            // 
            // Clave
            // 
            this.Clave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Clave.Location = new System.Drawing.Point(356, 0);
            this.Clave.MaxLength = 5;
            this.Clave.Name = "Clave";
            this.Clave.NullText = "Clave";
            this.Clave.ReadOnly = true;
            this.Clave.Size = new System.Drawing.Size(72, 20);
            this.Clave.TabIndex = 25;
            this.Clave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Domicilio
            // 
            this.Domicilio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Domicilio.AutoSizeDropDownHeight = true;
            this.Domicilio.AutoSizeDropDownToBestFit = true;
            // 
            // Domicilio.NestedRadGridView
            // 
            this.Domicilio.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Domicilio.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Domicilio.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Domicilio.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Domicilio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Domicilio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Domicilio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Domicilio.EditorControl.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Calle";
            gridViewTextBoxColumn1.HeaderText = "Calle";
            gridViewTextBoxColumn1.Name = "Calle";
            gridViewTextBoxColumn1.Width = 300;
            gridViewTextBoxColumn2.FieldName = "NoExterior";
            gridViewTextBoxColumn2.HeaderText = "Ním. Exterior";
            gridViewTextBoxColumn2.Name = "NoExterior";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn3.FieldName = "NoInterior";
            gridViewTextBoxColumn3.HeaderText = "Núm. Interior";
            gridViewTextBoxColumn3.Name = "NoInterior";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Colonia";
            gridViewTextBoxColumn4.HeaderText = "Colonia";
            gridViewTextBoxColumn4.Name = "Colonia";
            gridViewTextBoxColumn4.Width = 100;
            gridViewTextBoxColumn5.FieldName = "CodigoPostal";
            gridViewTextBoxColumn5.HeaderText = "C. P.";
            gridViewTextBoxColumn5.Name = "CodigoPostal";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 65;
            gridViewTextBoxColumn6.FieldName = "Municipio";
            gridViewTextBoxColumn6.HeaderText = "Delegación / Municipio";
            gridViewTextBoxColumn6.Name = "Municipio";
            gridViewTextBoxColumn6.Width = 150;
            gridViewTextBoxColumn7.FieldName = "Estado";
            gridViewTextBoxColumn7.HeaderText = "Estado";
            gridViewTextBoxColumn7.Name = "Estado";
            gridViewTextBoxColumn7.Width = 150;
            gridViewTextBoxColumn8.FieldName = "Localidad";
            gridViewTextBoxColumn8.HeaderText = "Localidad";
            gridViewTextBoxColumn8.Name = "Localidad";
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.FieldName = "Referencia";
            gridViewTextBoxColumn9.HeaderText = "Referencia";
            gridViewTextBoxColumn9.Name = "Referencia";
            gridViewTextBoxColumn9.Width = 80;
            gridViewTextBoxColumn10.FieldName = "Completo";
            gridViewTextBoxColumn10.HeaderText = "Completo";
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "Completo";
            gridViewTextBoxColumn10.VisibleInColumnChooser = false;
            gridViewTextBoxColumn11.DataType = typeof(int);
            gridViewTextBoxColumn11.FieldName = "IdDomicilio";
            gridViewTextBoxColumn11.HeaderText = "IdDomicilio";
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "IdDomicilio";
            gridViewTextBoxColumn11.VisibleInColumnChooser = false;
            this.Domicilio.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.Domicilio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Domicilio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Domicilio.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Domicilio.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Domicilio.EditorControl.Name = "NestedRadGridView";
            this.Domicilio.EditorControl.ReadOnly = true;
            this.Domicilio.EditorControl.ShowGroupPanel = false;
            this.Domicilio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Domicilio.EditorControl.TabIndex = 0;
            this.Domicilio.Location = new System.Drawing.Point(62, 24);
            this.Domicilio.Name = "Domicilio";
            this.Domicilio.NullText = "Domicilio";
            this.Domicilio.Size = new System.Drawing.Size(565, 20);
            this.Domicilio.TabIndex = 21;
            this.Domicilio.TabStop = false;
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(35, 0);
            this.RFC.MaxLength = 14;
            this.RFC.Name = "RFC";
            this.RFC.NullText = "Registro Federal";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(100, 20);
            this.RFC.TabIndex = 15;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Nombre
            // 
            this.Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nombre.AutoSizeDropDownHeight = true;
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.DisplayMember = "Nombre";
            // 
            // Nombre.NestedRadGridView
            // 
            this.Nombre.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nombre.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nombre.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nombre.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nombre.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nombre.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Nombre.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nombre.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Nombre.EditorControl.Name = "NestedRadGridView";
            this.Nombre.EditorControl.ReadOnly = true;
            this.Nombre.EditorControl.ShowGroupPanel = false;
            this.Nombre.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Nombre.EditorControl.TabIndex = 0;
            this.Nombre.Location = new System.Drawing.Point(59, 0);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(298, 20);
            this.Nombre.TabIndex = 11;
            this.Nombre.TabStop = false;
            this.Nombre.ValueMember = "Id";
            // 
            // lblNombre
            // 
            this.lblNombre.Location = new System.Drawing.Point(1, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(50, 18);
            this.lblNombre.TabIndex = 10;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblRFC
            // 
            this.lblRFC.Location = new System.Drawing.Point(4, 1);
            this.lblRFC.Name = "lblRFC";
            this.lblRFC.Size = new System.Drawing.Size(28, 18);
            this.lblRFC.TabIndex = 14;
            this.lblRFC.Text = "RFC:";
            // 
            // lblDomicilio
            // 
            this.lblDomicilio.Location = new System.Drawing.Point(0, 24);
            this.lblDomicilio.Name = "lblDomicilio";
            this.lblDomicilio.Size = new System.Drawing.Size(56, 18);
            this.lblDomicilio.TabIndex = 20;
            this.lblDomicilio.Text = "Domicilio:";
            // 
            // IdDirectorio
            // 
            this.IdDirectorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDirectorio.Location = new System.Drawing.Point(585, 235);
            this.IdDirectorio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDirectorio.Name = "IdDirectorio";
            this.IdDirectorio.ShowBorder = false;
            this.IdDirectorio.ShowUpDownButtons = false;
            this.IdDirectorio.Size = new System.Drawing.Size(37, 20);
            this.IdDirectorio.TabIndex = 29;
            this.IdDirectorio.TabStop = false;
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.Buscar.Enabled = false;
            this.Buscar.Image = global::Jaeger.UI.Contribuyentes.Properties.Resources.search_16px;
            this.Buscar.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Buscar.Location = new System.Drawing.Point(585, 0);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(20, 20);
            this.Buscar.TabIndex = 373;
            // 
            // IdDomicilio
            // 
            this.IdDomicilio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdDomicilio.Controls.Add(this.IdTipoRemision);
            this.IdDomicilio.Location = new System.Drawing.Point(585, 209);
            this.IdDomicilio.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdDomicilio.Name = "IdDomicilio";
            this.IdDomicilio.ShowBorder = false;
            this.IdDomicilio.ShowUpDownButtons = false;
            this.IdDomicilio.Size = new System.Drawing.Size(37, 20);
            this.IdDomicilio.TabIndex = 374;
            this.IdDomicilio.TabStop = false;
            // 
            // IdTipoRemision
            // 
            this.IdTipoRemision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IdTipoRemision.Location = new System.Drawing.Point(8, 8);
            this.IdTipoRemision.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.IdTipoRemision.Name = "IdTipoRemision";
            this.IdTipoRemision.ShowBorder = false;
            this.IdTipoRemision.ShowUpDownButtons = false;
            this.IdTipoRemision.Size = new System.Drawing.Size(37, 20);
            this.IdTipoRemision.TabIndex = 375;
            this.IdTipoRemision.TabStop = false;
            // 
            // panelRFC
            // 
            this.panelRFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRFC.Controls.Add(this.RFC);
            this.panelRFC.Controls.Add(this.lblRFC);
            this.panelRFC.Location = new System.Drawing.Point(428, 0);
            this.panelRFC.Name = "panelRFC";
            this.panelRFC.Size = new System.Drawing.Size(137, 20);
            this.panelRFC.TabIndex = 375;
            // 
            // panelNombre
            // 
            this.panelNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelNombre.Controls.Add(this.Clave);
            this.panelNombre.Controls.Add(this.Nombre);
            this.panelNombre.Controls.Add(this.lblNombre);
            this.panelNombre.Location = new System.Drawing.Point(0, 0);
            this.panelNombre.Name = "panelNombre";
            this.panelNombre.Size = new System.Drawing.Size(428, 20);
            this.panelNombre.TabIndex = 0;
            // 
            // Advertencia
            // 
            this.Advertencia.ContainerControl = this;
            // 
            // ReceptorLayoutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Actualizar);
            this.Controls.Add(this.panelNombre);
            this.Controls.Add(this.panelRFC);
            this.Controls.Add(this.IdDomicilio);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.IdDirectorio);
            this.Controls.Add(this.Agregar);
            this.Controls.Add(this.lblDomicilio);
            this.Controls.Add(this.Domicilio);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReceptorLayoutControl";
            this.Size = new System.Drawing.Size(630, 44);
            this.Load += new System.EventHandler(this.ReceptorLayoutControl_Load);
            this.Resize += new System.EventHandler(this.DirectorioControl_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.Agregar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Actualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Domicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDirectorio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Buscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDomicilio)).EndInit();
            this.IdDomicilio.ResumeLayout(false);
            this.IdDomicilio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdTipoRemision)).EndInit();
            this.panelRFC.ResumeLayout(false);
            this.panelRFC.PerformLayout();
            this.panelNombre.ResumeLayout(false);
            this.panelNombre.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Advertencia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Telerik.WinControls.UI.RadTextBox Clave;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Domicilio;
        public Telerik.WinControls.UI.RadTextBox RFC;
        public Telerik.WinControls.UI.RadLabel lblNombre;
        public Telerik.WinControls.UI.RadLabel lblRFC;
        public Telerik.WinControls.UI.RadLabel lblDomicilio;
        public Telerik.WinControls.UI.RadButton Actualizar;
        public Telerik.WinControls.UI.RadButton Agregar;
        public Telerik.WinControls.UI.RadSpinEditor IdDirectorio;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Nombre;
        public Telerik.WinControls.UI.RadButton Buscar;
        public Telerik.WinControls.UI.RadSpinEditor IdDomicilio;
        protected internal System.Windows.Forms.Panel panelRFC;
        protected internal System.Windows.Forms.Panel panelNombre;
        public Telerik.WinControls.UI.RadSpinEditor IdTipoRemision;
        public System.Windows.Forms.ErrorProvider Advertencia;
    }
}
