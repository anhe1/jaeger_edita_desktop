﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ContribuyenteLayoutControl : UserControl {
        private bool _ReadOnly = false;
        public ContribuyenteLayoutControl() {
            InitializeComponent();
        }

        private void ContribuyenteLayoutControl_Load(object sender, EventArgs e) {
            this.NumRegIdTrib.TextBoxElement.ToolTipText = "Número de registro de identidad fiscal cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.";
        }

        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value; }
        }
    }
}
