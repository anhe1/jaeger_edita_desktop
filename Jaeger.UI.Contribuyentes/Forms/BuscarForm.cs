﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class BuscarForm : RadForm {
        public event EventHandler<ContribuyenteDetailModel> Selected;

        public void OnSelected(ContribuyenteDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public BuscarForm() {
            InitializeComponent();
        }

        private void BuscarForm_Load(object sender, EventArgs e) {
            this.TSearch.OverflowButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.Descripcion.KeyDown += this.Descripcion_KeyDown;
            this.Buscar.Click += this.Buscar_Click;
            this.Agregar.Click += this.Agregar_Click;
            this.Cerrar.Click += this.Cerrar_Click;
            this.searchControl.Selected += this.SearchControl_Selected;
        }

        public virtual void Agregar_Click(object sender, EventArgs e) {
            var selecciondo = this.searchControl.GetCurrent();
            if (selecciondo != null) {
                this.OnSelected(selecciondo);
            }
        }

        public virtual void Buscar_Click(object sender, EventArgs e) {
            this.searchControl.Search(this.Descripcion.Text);
            this.searchControl.GResult.Focus();
        }

        private void Descripcion_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e) {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) {
                this.Buscar.PerformClick();
            }
        }

        public virtual void SearchControl_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.OnSelected(e);
                this.Close();
            }
        }

        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
