﻿using System;
using Jaeger.UI.Contribuyentes.Builder;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class ContactosGridControl : UI.Common.Forms.GridStandarControl {
        public ContactosGridControl() : base() {
            this.InitializeComponent();
            this.Load += DomicilioGridControl_Load;
        }

        private void InitializeComponent() {
            IContactosGridBuilder view = new ContactosGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }

        private void DomicilioGridControl_Load(object sender, EventArgs e) {
            this.ShowEditar = false;
            this.ShowFiltro = false;
            this.ShowCerrar = false;
            this.ShowActualizar = false;
            this.ShowCerrar = false;
        }
    }
}
