﻿using System.Linq;
using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Catalogos.Entities;
using Jaeger.Catalogos.Contracts;
using Jaeger.Catalogos.Repositories;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class ContribuyenteForm : RadForm {
        #region declaraciones
        protected internal IContribuyenteService service;
        protected internal IBancosCatalogo catalogoBancos = new BancosCatalogo();
        protected internal RadButton CancelarButton = new RadButton();
        private BackgroundWorker preparar;
        private BackgroundWorker consulta;
        private BackgroundWorker guardar;

        protected internal IContribuyenteDetailModel currentPersona;

        private UIAction _permisos;
        #endregion

        public ContribuyenteForm(IContribuyenteService service, UIAction _permisos) {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            this.service = service;
            this._permisos = _permisos;
        }

        public ContribuyenteForm(IContribuyenteService service, ContribuyenteDetailModel model, UIAction _permisos) {
            InitializeComponent();
            this.currentPersona = model;
            this.service = service;
            this._permisos = _permisos;
        }

        private void ContribuyenteForm_Load(object sender, EventArgs e) {
            this.CancelButton = this.CancelarButton;
            this.Controls.Add(this.CancelarButton);
            this.preparar = new BackgroundWorker();
            this.preparar.DoWork += Preparar_DoWork;
            this.preparar.RunWorkerCompleted += Preparar_RunWorkerCompleted;
            this.Contribuyente.Preparar.RunWorkerAsync();
            this.preparar.RunWorkerAsync();

            this.consulta = new BackgroundWorker();
            this.consulta.DoWork += Consulta_DoWork;
            this.consulta.RunWorkerCompleted += Consulta_RunWorkerCompleted;

            this.guardar = new BackgroundWorker();
            this.guardar.DoWork += Guardar_DoWork;
            this.guardar.RunWorkerCompleted += Guardar_RunWorkerCompleted;
            this.Contribuyente.RFC.LostFocus += TextBoxRFC_TextChanged;
            this.Contribuyente.ButtonCIF.Click += ButtonCSF_Click;
            this.Contribuyente.Search.Click += TextBoxRFC_TextChanged;

            this.TContribuyente.Nuevo.Click += this.TContribuyente_Nuevo_Click;
            this.TContribuyente.Guardar.Click += this.TContribuyente_Guardar_Click;
            this.TContribuyente.Actualizar.Click += this.TContacto_Actualizar_Click;
            this.TContribuyente.Cerrar.Click += this.TContacto_Cerrar_Click;
            this.CancelarButton.Click += this.TContacto_Cerrar_Click;
            this.TDomicilio.Permisos = this._permisos;
            this.TDomicilio.Nuevo.Click += this.TDomicilio_Agregar_Click;
            this.TDomicilio.Remover.Click += this.TDomicilio_Quitar_Click;
            this.TContacto.Permisos = this._permisos;
            this.TContacto.Nuevo.Click += this.TContacto_Agregar_Click;
            this.TContacto.Remover.Click += this.TContacto_Quitar_Click;
            this.TCuentaBancaria.Permisos = this._permisos;
            this.TCuentaBancaria.Nuevo.Click += TCuentaBancaria_Agregar_Click;
            this.TCuentaBancaria.Remover.Click += TCuentaBancaria_Remover_Click;

            this.TVendedor.Nuevo.Click += this.TVendedor_Agregar_Click;
            this.TVendedor.Remover.Click += this.TVendedor_Remover_Click;
            this.DiasCredito.Enabled = this._permisos.Autorizar;
            this.DiasEntrega.Enabled = this._permisos.Autorizar;
            this.Descuento.Enabled = this._permisos.Autorizar;
            this.PactadoIVA.Enabled = this._permisos.Autorizar;
            this.LimiteCredito.Enabled = this._permisos.Autorizar;
        }

        #region barra de herramientas
        protected virtual void TContribuyente_Nuevo_Click(object sender, EventArgs e) {
            this.currentPersona = new ContribuyenteDetailModel();
            this.TContacto_Actualizar_Click(sender, e);
        }

        protected virtual void TContribuyente_Guardar_Click(object sender, EventArgs e) {
            if (this.currentPersona.IdDirectorio <= 0) {
                if (ValidacionService.RFC(this.currentPersona.RFC) == false) {
                    if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RFC_NoValido, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.No)
                        return;
                }

                IContribuyenteModel temp = this.service.GetByRFC(this.currentPersona.RFC);
                if (temp != null) {
                    RadMessageBox.Show(this, Properties.Resources.RFC_Exisente, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    return;
                }
            }

            var isError = this.currentPersona.Domicilios.Where(it => it.Error.Length > 0).Count() > 0;
            if (isError == true) {
                RadMessageBox.Show(this, Properties.Resources.Domicilio_Error, "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            var itRelated = this.currentPersona.Relaciones.Where(it => it.Selected == true).ToList();
            if (itRelated.Count() == 0) {
                RadMessageBox.Show(this, "Selecciona al menos una relación comercial.", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            this.BarraEspera.StartWaiting();
            this.BarraEspera.Visibility = ElementVisibility.Visible;
            this.BarraEspera.Text = "Guardando ...";
            this.TContribuyente.Enabled = false;
            this.Panel.Enabled = false;
            this.PageView.Enabled = false;
            this.guardar.RunWorkerAsync();
        }

        protected virtual void TContacto_Actualizar_Click(object sender, EventArgs e) {
            this.BarraEspera.StartWaiting();
            this.BarraEspera.Visibility = ElementVisibility.Visible;
            this.BarraEspera.Text = "Cargando ...";
            this.TContribuyente.Enabled = false;
            this.Panel.Enabled = false;
            this.PageView.Enabled = false;
            this.consulta.RunWorkerAsync();
            this.TCuentaBancaria.Items(this.catalogoBancos.Items);
        }

        private void TContacto_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TextBoxRFC_TextChanged(object sender, EventArgs e) {
            if (this.currentPersona.IdDirectorio == 0 && this.Contribuyente.RFC.Text.Length >= 12) {
                if (this.VerificarRFC == null) {

                }
                if (!this.VerificarRFC.IsBusy) {
                    this.VerificarRFC.RunWorkerAsync();
                    this.lblVerificar.Text = "Verificando...";
                }
            }
        }
        #endregion

        /// <summary>
        /// Constancia de Situacion Fiscal
        /// </summary>
        public virtual void ButtonCSF_Click(object sender, EventArgs e) {
            var cedula = new CedulaForm();
            cedula.Copied += Cedula_Copied;
            cedula.ShowDialog(this);
        }

        public virtual void Cedula_Copied(object sender, SAT.CIF.Interfaces.ICedulaFiscal e) {
            if (e != null) {
                if (this.currentPersona.IdDirectorio > 0 && !e.RFC.Equals(this.Contribuyente.RFC.Text)) {
                    MessageBox.Show(this, "El registro federal de contribuyentes no coincide", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                SAT.CIF.Interfaces.IDomicilioFiscal domicilio;
                if (e.TipoPersona == SAT.CIF.Entities.CedulaFiscal.TipoPersonaEnum.Moral) {
                    this.currentPersona.RFC = e.Moral.RFC;
                    this.currentPersona.Nombre = e.Moral.Nombre;
                    this.currentPersona.NombreComercial = e.Moral.NombreComercial;
                    this.currentPersona.DomicilioFiscal = e.Moral.DomicilioFiscal.CodigoPostal;
                    this.currentPersona.Correo = e.Moral.DomicilioFiscal.Correo;
                    this.currentPersona.RegimenFiscal = e.Moral.RegimenFiscal.Clave;
                    domicilio = e.Moral.DomicilioFiscal;
                } else {
                    this.currentPersona.RFC = e.Fisica.RFC;
                    this.currentPersona.CURP = e.Fisica.CURP;
                    this.currentPersona.Nombre = e.Fisica.NombreCompleto;
                    this.currentPersona.NombreComercial = currentPersona.Nombre;
                    this.currentPersona.DomicilioFiscal = e.Fisica.DomicilioFiscal.CodigoPostal;
                    this.currentPersona.Correo = e.Fisica.DomicilioFiscal.Correo;
                    this.currentPersona.RegimenFiscal = e.Fisica.Regimenes.FirstOrDefault().Clave;
                    domicilio = e.Fisica.DomicilioFiscal;
                }

                if (currentPersona.Domicilios == null)
                    this.currentPersona.Domicilios = new BindingList<IDomicilioFiscalDetailModel>();

                if (currentPersona.Domicilios != null) {
                    try {
                        IDomicilioFiscalDetailModel d0 = this.currentPersona.Domicilios.Where(it => it.Tipo == TipoDomicilioEnum.Fiscal.ToString()).FirstOrDefault();
                        if (d0 == null) {
                            d0 = SetDomicilio(null, domicilio);
                            d0.TipoDomicilio = TipoDomicilioEnum.Fiscal;
                            this.currentPersona.Domicilios.Add(d0 as DomicilioFiscalDetailModel);
                        } else {
                            d0 = SetDomicilio(d0, domicilio);
                        }
                    } catch (Exception) {
                    }
                }
            }
        }

        #region cuentas bancarias   
        private void TCuentaBancaria_Agregar_Click(object sender, EventArgs e) {
            if (this.currentPersona.CuentasBancarias == null) {
                this.currentPersona.CuentasBancarias = new BindingList<ICuentaBancariaModel>();
            }

            this.currentPersona.CuentasBancarias.Add(new CuentaBancariaModel());
        }

        private void TCuentaBancaria_Remover_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(Properties.Resources.CuentaBancaria_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                if (this.TCuentaBancaria.GridData.CurrentRow != null) {
                    var seleccionado = this.TCuentaBancaria.GridData.CurrentRow.DataBoundItem as CuentaBancariaModel;
                    if (seleccionado != null) {
                        if (seleccionado.IdCuenta <= 0) {
                            this.TCuentaBancaria.GridData.Rows.Remove(this.TCuentaBancaria.GridData.CurrentRow);
                        } else {
                            seleccionado.Activo = false;
                        }
                    }
                    this.TCuentaBancaria.Refresh();
                }
            }
        }

        private void GridCuentaBancaria_SelectionChanged(object sender, EventArgs e) {
            if (this.TCuentaBancaria.GridData.CurrentRow != null) {
                var cuenta = this.TCuentaBancaria.GridData.CurrentRow.DataBoundItem as CuentaBancariaModel;
                if (cuenta != null) {
                    if (cuenta.IdCuenta == 0) {
                        this.TCuentaBancaria.Remover.Enabled = true;
                        this.TCuentaBancaria.GridData.AllowEditRow = true;
                    } else {
                        this.TCuentaBancaria.Remover.Enabled = true;
                        this.TCuentaBancaria.GridData.AllowEditRow = true;
                    }
                }
            }
        }

        private void GridCuentaBancaria_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.ActiveEditor is RadMultiColumnComboBoxElement) {
                RadMultiColumnComboBoxElement editor = e.ActiveEditor as RadMultiColumnComboBoxElement;
                if (!(editor == null)) {
                    var viewInfo = editor.SelectedItem as GridViewRowInfo;
                    if (!(viewInfo == null)) {
                        var item = viewInfo.DataBoundItem as ClaveBanco;
                        if (item != null) {
                            e.Row.Cells["Clave"].Value = item.Clave;
                            e.Row.Cells["InsitucionBancaria"].Value = item.Descripcion;
                            e.Row.Cells["Banco"].Value = item.RazonSocial;
                            e.Row.Cells["Beneficiario"].Value = this.currentPersona.Nombre;
                            e.Row.Cells["Rfc"].Value = this.currentPersona.RFC;
                        }
                    }
                }
            }
        }
        #endregion

        #region domicilios
        private void TDomicilio_Agregar_Click(object sender, EventArgs e) {
            if (this.currentPersona.Domicilios == null) {
                this.currentPersona.Domicilios = new BindingList<IDomicilioFiscalDetailModel>();
            }
            this.currentPersona.Domicilios.Add(new DomicilioFiscalDetailModel());
        }

        private void TDomicilio_Quitar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(Properties.Resources.Domicilio_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                if (this.TDomicilio.GridData.CurrentRow != null) {
                    if (DbConvert.ConvertInt32((this.TDomicilio.GridData.CurrentRow.Cells["Id"].Value)) <= 0) {
                        this.TDomicilio.GridData.Rows.Remove(this.TDomicilio.GridData.CurrentRow);
                    } else {
                        this.TDomicilio.GridData.CurrentRow.Cells["IsActive"].Value = false;
                    }
                    this.TDomicilio.Refresh();
                }
            }
        }

        private void GridDomicilio_SelectionChanged(object sender, EventArgs e) {
            if (this.TDomicilio.GridData.CurrentRow != null) {
                var dom = this.TDomicilio.GridData.CurrentRow.DataBoundItem as DomicilioFiscalDetailModel;
                if (dom != null) {
                    if (dom.IdDomicilio == 0) {
                        this.TDomicilio.Remover.Enabled = true;
                        this.TDomicilio.GridData.AllowEditRow = true;
                    } else {
                        this.TDomicilio.Remover.Enabled = true;
                        this.TDomicilio.GridData.AllowEditRow = true;
                    }
                }
            }
        }
        #endregion

        #region contactos
        private void TContacto_Agregar_Click(object sender, EventArgs e) {
            if (this.currentPersona.Contactos == null)
                this.currentPersona.Contactos = new BindingList<IContactoDetailModel>();
            this.currentPersona.Contactos.Add(new ContactoDetailModel());
        }

        private void TContacto_Quitar_Click(object sender, EventArgs e) {
            if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RemoveRegistro, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                if (this.TContacto.GridData.CurrentRow != null) {
                    var seleccionado = this.TContacto.GridData.CurrentRow.DataBoundItem as ContactoDetailModel;
                    if (seleccionado != null) {
                        if (seleccionado.IdContacto == 0) {
                            this.TContacto.GridData.Rows.Remove(this.TContacto.GridData.CurrentRow);
                        } else {
                            seleccionado.Activo = false;
                        }
                    }
                    this.TContacto.Refresh();
                }
            }
        }

        private void GridContacto_SelectionChanged(object sender, EventArgs e) {
            if (this.TContacto.GridData.CurrentRow != null) {
                var fila = this.TContacto.GridData.CurrentRow.DataBoundItem as ContactoModel;
                if (fila != null) {
                    if (fila.IdContacto == 0) {
                        this.TContribuyente.Remover.Enabled = true;
                        this.TContacto.GridData.AllowEditRow = true;
                    } else {
                        this.TContribuyente.Remover.Enabled = false;
                        this.TContacto.GridData.AllowEditRow = true;
                    }
                }
            }
        }
        #endregion

        #region vendedor
        public virtual void TVendedor_Agregar_Click(object sender, EventArgs e) {
            if (this.currentPersona.Vendedores == null) {
                this.currentPersona.Vendedores = new BindingList<IContribuyenteVendedorModel>();
            }
            this.currentPersona.Vendedores.Add(new ContribuyenteVendedorModel());
        }

        public virtual void TVendedor_Remover_Click(object sender, EventArgs e) {
            if (this.GridVendedores.CurrentRow != null) {
                if (RadMessageBox.Show("¿Esta seguro de remover al vendedor serleccionado?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    var row = this.GridVendedores.CurrentRow.DataBoundItem as ContribuyenteVendedorModel;
                    if (row != null) {
                        if (row.Activo == true) {
                            row.Activo = false;
                            this.GridVendedores.CurrentRow.IsVisible = false;
                        }
                    }
                }
            }
        }

        public virtual void GridVendedor_SelectionChanged(object sender, EventArgs e) {
            if (this.GridVendedores.CurrentRow != null) {
                //var fila = this.GridVendedores.CurrentRow.DataBoundItem as VendedorClienteModel;
                //if (fila != null) {
                //    this.ToolVendedor.Remover.Enabled = fila.Id == 0;
                //    this.GridVendedores.AllowEditRow = fila.Id == 0;
                //}
            }
        }

        public virtual void GridVendedor_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.GridVendedores.CurrentColumn is GridViewMultiComboBoxColumn) {
                var editor = (RadMultiColumnComboBoxElement)this.GridVendedores.ActiveEditor;
                if ((string)editor.Tag == "listo") { return; }
                editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                editor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "IdVendedor", Name = "IdVendedor", FieldName = "IdVendedor", IsVisible = false });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Clave", Name = "Clave", FieldName = "Clave" });
                editor.EditorControl.Columns.Add(new GridViewTextBoxColumn() { HeaderText = "Vendedor", Name = "Nombre", FieldName = "Nombre" });
                editor.AutoSizeDropDownToBestFit = true;
                editor.Tag = "listo";
            }
        }
        #endregion

        #region preparar formulario

        private void Preparar_DoWork(object sender, DoWorkEventArgs e) {
            this.catalogoBancos.Load();
            this.TCuentaBancaria.GridData.CellEndEdit += GridCuentaBancaria_CellEndEdit;
            this.TCuentaBancaria.GridData.SelectionChanged += GridCuentaBancaria_SelectionChanged;
            this.TDomicilio.GridData.SelectionChanged += GridDomicilio_SelectionChanged;
            this.TContacto.GridData.SelectionChanged += GridContacto_SelectionChanged;
        }

        private void Preparar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Contribuyente.RegimenFiscal.SelectedIndex = -1;
            this.Contribuyente.RegimenFiscal.SelectedItem = null;

            this.Contribuyente.UsoCFDI.SelectedIndex = -1;
            this.Contribuyente.UsoCFDI.SelectedItem = null;

            //var cbancos = (GridViewMultiComboBoxColumn)this.TCuentaBancaria.GridData.MasterTemplate.Columns["Banco"];
            //cbancos.DisplayMember = "RazonSocial";
            //cbancos.ValueMember = "RazonSocial";
            //cbancos.DataSource = this.catalogoBancos.Items;

            this.TContribuyente.Actualizar.PerformClick();
        }

        #endregion

        #region metodos privados
        public virtual void CreateBinding() {
            this.Contribuyente.Clave.DataBindings.Clear();
            this.Contribuyente.Clave.DataBindings.Add("Text", this.currentPersona, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RFC.DataBindings.Clear();
            this.Contribuyente.RFC.DataBindings.Add("Text", this.currentPersona, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.CURP.DataBindings.Clear();
            this.Contribuyente.CURP.DataBindings.Add("Text", this.currentPersona, "CURP", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Nombre.DataBindings.Clear();
            this.Contribuyente.Nombre.DataBindings.Add("Text", this.currentPersona, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.NombreComercial.DataBindings.Clear();
            this.Contribuyente.NombreComercial.DataBindings.Add("Text", this.currentPersona, "NombreComercial", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Regimen.DataBindings.Clear();
            this.Contribuyente.Regimen.DataBindings.Add("Text", this.currentPersona, "Regimen", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RegimenFiscal.DataBindings.Clear();
            this.Contribuyente.RegimenFiscal.DataBindings.Add("SelectedValue", this.currentPersona, "RegimenFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.ResidenciaFiscal.DataBindings.Clear();
            this.Contribuyente.ResidenciaFiscal.DataBindings.Add("Text", this.currentPersona, "ResidenciaFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.NumRegIdTrib.DataBindings.Clear();
            this.Contribuyente.NumRegIdTrib.DataBindings.Add("Text", this.currentPersona, "NumRegIdTrib", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.UsoCFDI.DataBindings.Clear();
            this.Contribuyente.UsoCFDI.DataBindings.Add("SelectedValue", this.currentPersona, "ClaveUsoCfdi", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.DomicilioFiscal.DataBindings.Clear();
            this.Contribuyente.DomicilioFiscal.DataBindings.Add("Text", this.currentPersona, "DomicilioFiscal", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Telefono.DataBindings.Clear();
            this.Contribuyente.Telefono.DataBindings.Add("Text", this.currentPersona, "Telefono", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Sitio.DataBindings.Clear();
            this.Contribuyente.Sitio.DataBindings.Add("Text", this.currentPersona, "SitioWeb", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Extranjero.DataBindings.Clear();
            this.Contribuyente.Extranjero.DataBindings.Add("Checked", this.currentPersona, "Extranjero", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Correo.DataBindings.Clear();
            this.Contribuyente.Correo.DataBindings.Add("Text", this.currentPersona, "Correo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.Nota.DataBindings.Clear();
            this.Contribuyente.Nota.DataBindings.Add("Text", this.currentPersona, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiasCredito.Enabled = false;
            this.DiasCredito.DataBindings.Clear();
            this.DiasCredito.DataBindings.Add("Value", this.currentPersona, "DiasCredito", true, DataSourceUpdateMode.OnPropertyChanged);

            this.DiasEntrega.DataBindings.Clear();
            this.DiasEntrega.DataBindings.Add("Value", this.currentPersona, "DiasEntrega", true, DataSourceUpdateMode.OnPropertyChanged);

            this.LimiteCredito.DataBindings.Clear();
            this.LimiteCredito.DataBindings.Add("Value", this.currentPersona, "Credito", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Descuento.DataBindings.Clear();
            this.Descuento.DataBindings.Add("Value", this.currentPersona, "FactorDescuento", true, DataSourceUpdateMode.OnPropertyChanged);

            this.PactadoIVA.DataBindings.Clear();
            this.PactadoIVA.DataBindings.Add("Value", this.currentPersona, "FactorIvaPactado", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TDomicilio.GridData.DataBindings.Clear();
            this.TDomicilio.GridData.DataBindings.Add("DataSource", this.currentPersona, "Domicilios", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TCuentaBancaria.GridData.DataBindings.Clear();
            this.TCuentaBancaria.GridData.DataBindings.Add("DataSource", this.currentPersona, "CuentasBancarias", true, DataSourceUpdateMode.OnPropertyChanged);

            this.TContacto.GridData.DataBindings.Clear();
            this.TContacto.GridData.DataBindings.Add("DataSource", this.currentPersona, "Contactos", true, DataSourceUpdateMode.OnPropertyChanged);

            this.GridVendedores.DataBindings.Clear();
            this.GridVendedores.DataBindings.Add("DataSource", this.currentPersona, "Vendedores", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Contribuyente.RelacionComercial.DataSource = this.currentPersona.Relaciones;
        }

        private void Inabilita() {
            // informacion general
            this.TContribuyente.Nuevo.Enabled = this._permisos.Agregar;
            this.TContribuyente.Guardar.Enabled = this._permisos.Editar | this.currentPersona.IdDirectorio == 0;
            this.Contribuyente.Clave.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.RFC.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.CURP.ReadOnly = this.currentPersona.IdDirectorio > 0;
            //this.controlContribuyente.Nombre.ReadOnly = this.currentPersona.Id > 0;
            this.Contribuyente.NumRegIdTrib.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Telefono.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Sitio.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Correo.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Nota.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Clave.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.CURP.ReadOnly = this.currentPersona.IdDirectorio > 0;
            this.Contribuyente.Search.Enabled = this.currentPersona.IdDirectorio == 0;
        }

        private void Guardar_DoWork(object sender, DoWorkEventArgs e) {
            this.currentPersona = service.Save(currentPersona);
        }

        private void Guardar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.CreateBinding();
            this.Inabilita();
            this.TContribuyente.Enabled = true;
            this.Panel.Enabled = true;
            this.PageView.Enabled = true;
            this.BarraEspera.StopWaiting();
            this.BarraEspera.Visibility = ElementVisibility.Collapsed;
        }

        private void Consulta_DoWork(object sender, DoWorkEventArgs e) {
            //if (this.currentPersona != null)
            //    if (this.currentPersona.IdDirectorio > 0)
            //        this.currentPersona = this.service.GetById(this.currentPersona.IdDirectorio);


        }

        private void Consulta_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (currentPersona != null) {
                if (this.currentPersona.IdDirectorio > 0) {
                    this.currentPersona.Modifica = ConfigService.Piloto.Clave;
                    this.currentPersona.FechaModifica = DateTime.Now;
                } else {
                    this.currentPersona.Creo = ConfigService.Piloto.Clave;
                    this.currentPersona.FechaNuevo = DateTime.Now;
                }
            } else {
                this.currentPersona = this.service.GetNew();
            }
            this.CreateBinding();
            this.Inabilita();
            this.TContribuyente.Enabled = true;
            this.Panel.Enabled = true;
            this.PageView.Enabled = true;

            this.BarraEspera.StopWaiting();
            this.BarraEspera.Visibility = ElementVisibility.Collapsed;
        }

        private void VerificarRFC_DoWork(object sender, DoWorkEventArgs e) {
            this.lblVerificar.Text = "Buscando RFC ...";
            var rfc = this.service.GetByRFC(this.currentPersona.RFC);
            if (rfc != null) {
                this.Enabled = false;
                this.Tag = this.service.GetById(rfc.IdDirectorio);
            }
        }

        private void VerificarRFC_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (this.Tag != null) {
                var _persona = this.Tag as ContribuyenteDetailModel;
                this.lblVerificar.Text = "Encontrado";
                if (_persona != null) {
                    Console.WriteLine("se encontro RFC");
                    this.Text = _persona.RFC;
                    if (RadMessageBox.Show(this, "El RFC ya se encuetra registrado, ¿quieres agregar la relación?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        this.currentPersona = _persona;
                        this.Tag = null;
                        this.TContribuyente.Actualizar.PerformClick();
                    }
                }
            }
            this.Enabled = true;
            this.VerificarRFC.Dispose();
        }

        private IDomicilioFiscalDetailModel SetDomicilio(IDomicilioFiscalDetailModel seleccionado, SAT.CIF.Interfaces.IDomicilioFiscal response) {
            if (seleccionado == null)
                seleccionado = new DomicilioFiscalDetailModel();
            seleccionado.Calle = response.NombreVialidad;
            seleccionado.NoExterior = response.NumExterior;
            seleccionado.NoInterior = response.NumInterior;
            seleccionado.Colonia = response.Colonia;
            seleccionado.Municipio = response.MunicipioDelegacion;
            seleccionado.CodigoPostal = response.CodigoPostal;
            seleccionado.Estado = response.EntidadFederativa;
            return seleccionado;
        }
        #endregion
    }
}
