﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;

namespace Jaeger.UI.Contribuyentes.Forms {
    /// <summary>
    /// control de la vista simple de un contribuyente del directorio
    /// </summary>
    public partial class ReceptorLayoutControl : UserControl {
        private bool _ReadOnly = false;
        private bool _AllowAddNew = false;

        public ReceptorLayoutControl() {
            InitializeComponent();
            this.lblNombre.Resize += this.PanelNombre_Resize;
        }

        private void ReceptorLayoutControl_Load(object sender, EventArgs e) {
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.AllowRowResize = false;
            this.Nombre.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.Nombre.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
            this.Nombre.EditorControl.TableElement.MinSize = new Size(300, 200);
            this.Nombre.EditorControl.TableElement.Size = new Size(300, 200);
            this.Nombre.AutoSizeDropDownToBestFit = true;

            this.Actualizar.ButtonElement.ToolTipText = "Actualizar lista del directorio.";
            this.Buscar.ButtonElement.ToolTipText = "Mostrar cuadro de diálogo para búsquedas.";
            this.Agregar.ButtonElement.ToolTipText = "Agregar nuevo registro al directorio.";

            this.RFC.TextBoxElement.ToolTipText = "Clave del Registro Federal de Contribuyentes";
            this.Nombre.MultiColumnComboBoxElement.ToolTipText = "Denominación o razón social registrado.";
            this.Clave.TextBoxElement.ToolTipText = "Clave de sistema";
        }

        #region propiedades del control
        /// <summary>
        /// obtener o establecer si el control es de solo lectura
        /// </summary>
        [Description("Indica sin el control es solo lectura"), Category("Control")]
        public bool ReadOnly {
            get { return this._ReadOnly; }
            set { this._ReadOnly = value;
                this.CreateReadOnly();
            }
        }

        [Description("Indica si el usuario puede agregar nuevos registros"), Category("Control")]
        public bool AllowAddNew {
            get { return this._AllowAddNew; }
            set { this._AllowAddNew = value;
                this.Agregar.Enabled = value;
            }
        }

        [Description("Mostrar o ocultar RFC del contribuyente seleccionado"), Category("Control")]
        public bool ShowRFC {
            get { return this.panelRFC.Visible; }
            set {
                this.panelRFC.Visible = value;
                this.DirectorioControl_Resize(new object(), new EventArgs());
            }
        }

        [Description("Mostrar o ocultar la direccion del contribuyente seleccionado"), Category("Control")]
        public bool ShowDomicilio {
            get { return this.Domicilio.Visible; }
            set {
                this.Domicilio.Visible = value;
                this.lblDomicilio.Visible = value;
                this.DirectorioControl_Resize(new object(), new EventArgs());
            }
        }

        [Description("Etiqueta utiliza para la descripcion del nombre"), Category("Control")]
        public string LNombre {
            get { return this.lblNombre.Text; }
            set { this.lblNombre.Text = value; }
        }

        [Description("Etiqueta utiliza para la descripcion del domicilio"), Category("Control")]
        public string LDomicilio {
            get { return this.lblDomicilio.Text; }
            set { this.lblDomicilio.Text = value; }
        }
        #endregion

        #region metodos privados
        private void CreateReadOnly() {
            if (this._ReadOnly) {
                this.Nombre.DropDownOpening += EditorControl_DropDownOpening;
                this.Nombre.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = true;
                this.Nombre.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Hidden;
                this.Buscar.Enabled = false;
                this.Actualizar.Enabled = false;
            } else {
                this.Nombre.DropDownOpening -= EditorControl_DropDownOpening;
                this.Nombre.MultiColumnComboBoxElement.TextBoxElement.TextBoxItem.ReadOnly = false;
                this.Nombre.MultiColumnComboBoxElement.ArrowButton.Visibility = ElementVisibility.Visible;
                this.Buscar.Enabled = true;
                this.Actualizar.Enabled = true;
            }
        }

        private void EditorControl_DropDownOpening(object sender, CancelEventArgs e) {
            e.Cancel = this._ReadOnly;
        }

        private void DirectorioControl_Resize(object sender, EventArgs e) {
            this.panelNombre.Width = this.Width - (this.Width - (this.panelRFC.Visible ? this.panelRFC.Left : this.Actualizar.Left-2));
            this.Height = 20 + (this.Domicilio.Visible ? 24 : 0);
        }

        private void PanelNombre_Resize(object sender, EventArgs e) {
            this.Nombre.Left = this.lblNombre.Left + this.lblNombre.Width + 4;
            this.Nombre.Width = (this.panelNombre.Width - (this.Clave.Width + this.Nombre.Left))-2;
            this.Nombre.Height = 20;
        }
        #endregion
    }
}
