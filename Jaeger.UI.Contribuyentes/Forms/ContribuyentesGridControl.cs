﻿using System;
using Jaeger.UI.Contribuyentes.Builder;

namespace Jaeger.UI.Contribuyentes.Forms {
    public class ContribuyentesGridControl : UI.Common.Forms.GridStandarControl {
        public ContribuyentesGridControl() : base() {
            this.Load += ContribuyentesGridControl_Load;
        }

        private void ContribuyentesGridControl_Load(object sender, EventArgs e) {
            IContribuyenteGridBuilder view = new ContribuyenteGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
