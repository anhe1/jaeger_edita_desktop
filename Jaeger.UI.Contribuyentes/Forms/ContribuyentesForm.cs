﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.SAT.RFC.Valida;
using Jaeger.SAT.RFC.Valida.Interfaces;
using Jaeger.Aplication.Contribuyentes.Helpers;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Contribuyentes.Forms {
    /// <summary>
    /// Formulario pdel directorio
    /// </summary>
    public partial class ContribuyentesForm : RadForm {
        #region declaraciones
        protected internal IDirectorioService service;
        protected internal IService service2sat = new Service();
        protected internal TipoRelacionComericalEnum relacionComericalEnum;
        protected BindingList<IContribuyenteDomicilioSingleModel> Data;
        protected internal RadMenuItem _RelacionComercial = new RadMenuItem { Text = "Actualizar relaciones", CheckOnClick = true };
        #endregion

        #region formulario
        public ContribuyentesForm() {
            InitializeComponent();
            this.relacionComericalEnum = TipoRelacionComericalEnum.None;
            this.Initialization();
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyentesForm(int relacionComericalEnum) {
            InitializeComponent();
            this.relacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
            this.Initialization();
        }

        /// <summary>
        /// tipo de relacion 1 = Cliente, 2 = Provedor
        /// </summary>
        public ContribuyentesForm(TipoRelacionComericalEnum relacionComericalEnum) {
            InitializeComponent();
            this.relacionComericalEnum = (TipoRelacionComericalEnum)relacionComericalEnum;
            this.Initialization();
        }

        private void ContribuyentesForm_Load(object sender, EventArgs e) {

        }

        private void RelacionComercial_Click(object sender, EventArgs e) {
            if (MessageBox.Show(this, "¿Esta seguro de crear las relaciones para este directorio? \r\nEsta acción no se puede revirtir.", "Atención", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                using (var proceso = new ProgressDialogForm()) {
                    var worker = new RelacionComercialWorker(this.service);
                    proceso.Start(this, worker);
                }
            }
        }

        #endregion

        #region barra de herramientas
        protected virtual void TContribuyente_Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ContribuyenteForm(this.service, this.GContribuyente.Permisos)) {
                nuevo.Text = "Nuevo";
                nuevo.ShowDialog(this);
            }
        }

        public virtual void TContribyente_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GContribuyente.GridData.DataSource = this.Data;
        }

        public virtual void TContribuyente_Editar_Click(object sender, EventArgs e) {
            var seleccionado1 = this.GContribuyente.GetCurrent<ContribuyenteDomicilioSingleModel>();
            if (seleccionado1 != null) {
                using (var espera = new Waiting1Form(this.Editar)) {
                    espera.Text = "Consultando información ...";
                    espera.ShowDialog(this);
                }

                if (seleccionado1.Tag != null) {
                    var seleccionado = seleccionado1.Tag as ContribuyenteDetailModel;
                    if (seleccionado != null) {
                        using (var editar = new ContribuyenteForm(this.service, seleccionado, this.GContribuyente.Permisos)) {
                            editar.Text = "Edición:";
                            editar.ShowDialog(this);
                        }
                    }
                }
            }
        }

        protected virtual void TContribuyente_Remover_Click(object sender, EventArgs e) {
            if (this.GContribuyente.GridData.CurrentRow != null) {
                if (RadMessageBox.Show(this, Properties.Resources.Pregunta_RemoveRegistro, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes) {
                    using (var espera = new Waiting2Form(this.Remover)) {
                        espera.Text = "Removiendo ...";
                        espera.ShowDialog(this);
                    }
                }
            }
        }

        protected virtual void TContribuyente_ValidarRFC_Click(object sender, EventArgs eventArgs) {
            if (this.GContribuyente.GridData.ChildRows.Count > 0) {
                using (var espera = new Waiting1Form(this.ValidarRFC)) {
                    espera.Text = "Esperando al servidor SAT";
                    espera.ShowDialog(this);
                }
                if (this.Tag != null) {
                    var mensaje = (string)this.Tag;
                    RadMessageBox.Show(this, mensaje, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        protected virtual void TContribuyente_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        public virtual void MContextual_ValidarRFC_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
        }
        #endregion

        #region metodos privados
        public virtual void Initialization() {
            // reemplazar las imagenes standar
            this.GContribuyente.Nuevo.Image = Properties.Resources.add_user_male_16px;
            this.GContribuyente.Editar.Image = Properties.Resources.registration_16px;
            this.GContribuyente.Remover.Image = Properties.Resources.denied_16px;
            // solo para el administrador
            if (ConfigService.Piloto.Clave.ToLower() == "anhe1") { this.GContribuyente.Herramientas.Items.Add(this._RelacionComercial); }

            this._RelacionComercial.Click += this.RelacionComercial_Click;
            this.GContribuyente.Nuevo.Click += this.TContribuyente_Nuevo_Click;
            this.GContribuyente.Actualizar.Click += this.TContribyente_Actualizar_Click;
            this.GContribuyente.Editar.Click += this.TContribuyente_Editar_Click;
            this.GContribuyente.Remover.Click += this.TContribuyente_Remover_Click;
            this.GContribuyente.Cerrar.Click += this.TContribuyente_Cerrar_Click;
            this.GContribuyente.Herramientas.Visibility = ElementVisibility.Visible;
        }

        public virtual void Consultar() {
            this.Data = new BindingList<IContribuyenteDomicilioSingleModel>(this.service.GetList<ContribuyenteDomicilioSingleModel>(!this.GContribuyente.RegistroInactivo.IsChecked).ToList<IContribuyenteDomicilioSingleModel>());
        }

        public virtual void Editar() {
            var seleccionado = this.GContribuyente.GetCurrent<ContribuyenteDomicilioSingleModel>();
            if (seleccionado != null)
                seleccionado.Tag = this.service.GetById(seleccionado.IdDirectorio);
        }

        public virtual void Remover() {
            var _seleccionado = this.GContribuyente.GetCurrent<ContribuyenteDomicilioSingleModel>();
            if (_seleccionado != null) {
                if (this.service.Remover(_seleccionado.IdDirectorio)) {
                    //this.GContribuyente.Actualizar.PerformClick();
                    this.GContribuyente.GridData.CurrentRow.IsVisible = false;
                }
            }
        }

        protected virtual void ValidarRFC() {
            var _seleccion = new System.Collections.Generic.List<ContribuyenteDomicilioSingleModel>();
            if (this.GContribuyente.GridData.MultiSelect == true) {
                _seleccion = this.GContribuyente.GridData.SelectedRows.Where(it => it.IsSelected == true).Select(it => it.DataBoundItem as ContribuyenteDomicilioSingleModel).ToList();
            } else {
                var d0 = this.GContribuyente.GridData.CurrentRow.DataBoundItem as ContribuyenteDomicilioSingleModel;
                _seleccion.Add(d0);
            }
            var registros = new System.Collections.Generic.List<IRegistro>();
            foreach (var item in _seleccion) {
                var _registro = new Registro { CodigoPostal = item.DomicilioFiscal, NombreRazonSocial = item.Nombre, RFC = item.RFC, Numero = registros.Count + 1, Tag = item.IdDirectorio };
                // en caso donde domiclio Fiscal sea nulo o este vacio entonces utilizamos el codigo postal de la direccion fiscal
                if (string.IsNullOrEmpty(item.DomicilioFiscal)) {
                    _registro.CodigoPostal = item.CodigoPostal;
                }
                if (!string.IsNullOrEmpty(item.RFC)) {
                    registros.Add(_registro);
                }
            }
            if (registros.Count > 0) {
                var validarfc = this.service2sat.Execute(Request.Create().WithRegistros(registros).Build());
                var salida = string.Empty;
                if (validarfc.Status == SAT.RFC.Valida.Enums.StatusCodeEnum.Error) {
                    this.Tag = validarfc.Message;
                    return;
                }

                foreach (var record in validarfc.Records) {
                    if (record.IsValid) {
                        try {
                            var s = _seleccion.Where(it => it.RFC == record.RFC).FirstOrDefault();
                            if (s != null) {
                                s.IsValidRFC = record.IsValid;
                                if (string.IsNullOrEmpty(s.DomicilioFiscal))
                                    s.DomicilioFiscal = record.CodigoPostal;
                                this.service.ValidaRFC(s.IdDirectorio, record.NombreRazonSocial, s.Nombre, record.CodigoPostal, record.IsValid);
                                s.Nombre = record.NombreRazonSocial;
                            }
                        } catch (Exception ex) {
                            Console.WriteLine("Contribuyentes: " + ex.Message);
                        }
                    }
                    salida = string.Concat(salida, record.ToString(), " Resultado: ", record.Respuesta, Environment.NewLine);
                }

                this.Tag = salida;
            } else {
                this.Tag = "Los registros seleccionados no tienen los requisitos mínimos (RFC, Nombre o Domicilio Fiscal)";
            }
        }
        #endregion
    }
}
