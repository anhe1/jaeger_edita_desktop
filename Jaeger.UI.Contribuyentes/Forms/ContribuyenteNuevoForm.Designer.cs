﻿namespace Jaeger.UI.Contribuyentes.Forms {
    partial class ContribuyenteNuevoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContribuyenteNuevoForm));
            this.Contribuyente = new Jaeger.UI.Contribuyentes.Forms.ContribuyenteControl();
            this.VerificarRFC = new System.ComponentModel.BackgroundWorker();
            this.TContribuyente = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEspera = new Telerik.WinControls.UI.RadWaitingBarElement();
            this.lblVerificar = new Telerik.WinControls.UI.RadLabelElement();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Contribuyente
            // 
            this.Contribuyente.Location = new System.Drawing.Point(0, 35);
            this.Contribuyente.MinimumSize = new System.Drawing.Size(680, 186);
            this.Contribuyente.Name = "Contribuyente";
            this.Contribuyente.Size = new System.Drawing.Size(683, 186);
            this.Contribuyente.TabIndex = 66;
            // 
            // VerificarRFC
            // 
            this.VerificarRFC.DoWork += new System.ComponentModel.DoWorkEventHandler(this.VerificarRFC_DoWork);
            this.VerificarRFC.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.VerificarRFC_RunWorkerCompleted);
            // 
            // TContribuyente
            // 
            this.TContribuyente.Dock = System.Windows.Forms.DockStyle.Top;
            this.TContribuyente.Etiqueta = "";
            this.TContribuyente.Location = new System.Drawing.Point(0, 0);
            this.TContribuyente.Name = "TContribuyente";
            this.TContribuyente.ReadOnly = false;
            this.TContribuyente.ShowActualizar = false;
            this.TContribuyente.ShowAutorizar = false;
            this.TContribuyente.ShowCerrar = true;
            this.TContribuyente.ShowEditar = false;
            this.TContribuyente.ShowExportarExcel = false;
            this.TContribuyente.ShowFiltro = false;
            this.TContribuyente.ShowGuardar = true;
            this.TContribuyente.ShowHerramientas = false;
            this.TContribuyente.ShowImagen = false;
            this.TContribuyente.ShowImprimir = false;
            this.TContribuyente.ShowNuevo = false;
            this.TContribuyente.ShowRemover = false;
            this.TContribuyente.Size = new System.Drawing.Size(682, 29);
            this.TContribuyente.TabIndex = 1;
            this.TContribuyente.Guardar.Click += this.TContribuyente_Guardar_Click;
            this.TContribuyente.Cerrar.Click += this.TContacto_Cerrar_Click;
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEspera,
            this.lblVerificar});
            this.BarraEstado.Location = new System.Drawing.Point(0, 228);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(682, 26);
            this.BarraEstado.SizingGrip = false;
            this.BarraEstado.TabIndex = 68;
            // 
            // BarraEspera
            // 
            this.BarraEspera.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarraEspera.Name = "BarraEspera";
            this.BarraEstado.SetSpring(this.BarraEspera, false);
            this.BarraEspera.Text = "";
            this.BarraEspera.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.BarraEspera.UseCompatibleTextRendering = false;
            // 
            // lblVerificar
            // 
            this.lblVerificar.Name = "lblVerificar";
            this.BarraEstado.SetSpring(this.lblVerificar, false);
            this.lblVerificar.Text = "...";
            this.lblVerificar.TextWrap = true;
            this.lblVerificar.UseCompatibleTextRendering = false;
            // 
            // ContribuyenteNuevoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 254);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.Contribuyente);
            this.Controls.Add(this.TContribuyente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContribuyenteNuevoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contacto";
            this.Load += new System.EventHandler(this.ContribuyenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ContribuyenteControl Contribuyente;
        private Common.Forms.ToolBarStandarControl TContribuyente;
        private System.ComponentModel.BackgroundWorker VerificarRFC;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadWaitingBarElement BarraEspera;
        private Telerik.WinControls.UI.RadLabelElement lblVerificar;
    }
}
