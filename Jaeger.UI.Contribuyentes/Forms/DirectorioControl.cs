﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.WinControls.Data;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.Contribuyentes.Forms {
    public partial class DirectorioControl : ReceptorLayoutControl {
        public BackgroundWorker bkWorker = new BackgroundWorker();
        protected internal List<ContribuyenteDetailModel> _DataSource;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public IDirectorioService Service;

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public TipoRelacionComericalEnum Relacion { get; set; }

        #region eventos
        public event EventHandler<ContribuyenteDetailModel> Seleccionado;
        public void OnSeleccionadoClompleted(ContribuyenteDetailModel e) {
            if (this.Seleccionado != null)
                this.Seleccionado(this, e);
        }
        #endregion

        #region propiedades
        public bool ShowSearch {
            get { return this.Nombre.EditorControl.AllowSearchRow; }
            set { this.Nombre.EditorControl.AllowSearchRow = value;
                this.AllowSearch();
            }
        }
        #endregion

        public DirectorioControl() {
            this.Load += this.DirectorioControl_Load;
        }

        private void DirectorioControl_Load(object sender, EventArgs e) {
            this.bkWorker.DoWork += this.BkWorker_DoWork;
            this.bkWorker.RunWorkerCompleted += this.BkWorker_RunWorkerCompleted;

            this.Nombre.Columns.AddRange(Services.GridContribuyenteService.GetSearch());
            this.Nombre.AutoSizeDropDownColumnMode = BestFitColumnMode.AllCells;
            this.Nombre.EditorControl.FilterDescriptors.Add(new FilterDescriptor(Services.GridContribuyenteService.ColNombre.Name, FilterOperator.StartsWith, ""));

            this.Actualizar.Click += this.Actualizar_Click;
            this.Buscar.Click += this.Buscar_Click;
            this.Agregar.Click += this.Agregar_Click;
        }

        public virtual void Init(TipoRelacionComericalEnum relacion = TipoRelacionComericalEnum.None) {
            if (relacion != TipoRelacionComericalEnum.None)
                if (relacion != this.Relacion)
                    this.Relacion = relacion;
            this.bkWorker.RunWorkerAsync();
        }

        public virtual void Actualizar_Click(object sender, EventArgs e) {
            if (this.bkWorker.IsBusy == false) {
                this.bkWorker.RunWorkerAsync();
            }
        }

        public virtual void Buscar_Click(object sender, EventArgs e) {
            var TCliente = new BuscarForm();
            TCliente.searchControl.Relacion = this.Relacion;
            TCliente.searchControl.Service = this.Service;
            TCliente.Selected += this.TCliente_Selected;
            TCliente.ShowDialog(this);
        }

        public virtual void TCliente_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.RFC.Text = e.RFC;
                this.Nombre.Text = e.Nombre;
                this.Clave.Text = e.Clave;
                this.IdDirectorio.Value = e.IdDirectorio;
                if (this.ShowDomicilio) {
                    this.Domicilio.DataSource = e.Domicilios;
                }
                this.OnSeleccionadoClompleted(e);
            }
        }

        public virtual void Agregar_Click(object sender, EventArgs e) {
            var dir = new ContribuyenteNuevoForm();
            dir.ShowDialog(this);
            RadMessageBox.Show(this, Properties.Resources.No_Implementado, "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
        }

        public virtual void Nombre_SelectedValueChanged(object sender, EventArgs e) {
            var temporal = this.Nombre.SelectedItem as GridViewRowInfo;
            if (!(temporal == null)) {
                var receptor = temporal.DataBoundItem as ContribuyenteDetailModel;
                if (receptor != null) {
                    this.RFC.Text = receptor.RFC;
                    this.Clave.Text = receptor.Clave;
                    this.Nombre.Text = receptor.Nombre;
                    this.IdDirectorio.Value = receptor.IdDirectorio;
                    if (this.ShowDomicilio) {
                        this.Domicilio.DataSource = receptor.Domicilios;
                    }
                    this.OnSeleccionadoClompleted(receptor);
                } else {
                    RadMessageBox.Show(this, Properties.Resources.Objeto_NoValido, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                }
            }
        }

        public virtual void Nombre_DropDownOpened(object sender, EventArgs e) {

        }

        public virtual void BkWorker_DoWork(object sender, DoWorkEventArgs e) {
            this.Actualizar.Enabled = false;
            this.Buscar.Enabled = false;
            this.Nombre.Enabled = false;
            this._DataSource = this.Service.GetList<ContribuyenteDetailModel>(true);
        }

        public virtual void BkWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Buscar.Enabled = true;
            this.Actualizar.Enabled = true;
            this.Nombre.DataSource = this._DataSource;
            this.Nombre.Enabled = true;
            this.Nombre.SelectedIndex = -1;
            this.Nombre.SelectedValue = null;
            this.Nombre.DropDownOpened += this.Nombre_DropDownOpened;
            this.Nombre.SelectedIndexChanged += this.Nombre_SelectedValueChanged;
        }

        private void AllowSearch() {
            this.Nombre.EditorControl.AllowRowResize = false;
            this.Nombre.EditorControl.AllowSearchRow = true;
            this.Nombre.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.Nombre.MultiColumnComboBoxElement.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.Nombre.MultiColumnComboBoxElement.DropDownAnimationEnabled = false;
        }
    }
}
