﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public class ContactosGridBuilder : GridViewBuilder, IGridViewBuilder, IContactosGridBuilder, IContactosTempletesGridBuilder, IContactosColumnsGridBuilder {
        public IContactosTempletesGridBuilder Templetes() {
            return this;
        }

        public IContactosTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.ColIdContacto().ColNombre().ColTratoSugerido().ColPuesto().ColDetalles().ColTipoTelefono().ColTelefono().ColTipoCorreo().ColCorreo().ColFechaEspecial();
            return this;
        }

        public IContactosColumnsGridBuilder ColIdContacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre de contacto",
                Name = "Nombre",
                Width = 250
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColTratoSugerido() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TratoSugerido",
                HeaderText = "Trato segerido",
                Name = "TratoSugerido",
                Width = 65,
                WrapText = true
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Puesto",
                HeaderText = "Puesto",
                Name = "Puesto",
                Width = 150
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColDetalles() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Detalles",
                HeaderText = "Detalles de contacto",
                Name = "Detalles",
                Width = 150
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColTipoTelefono() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TelefonoTipo",
                HeaderText = "Tipo Teléfono",
                Name = "TelefonoTipo",
                Width = 65
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 80
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColTipoCorreo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "CorreoTipo",
                HeaderText = "Tipo Correo",
                Name = "CorreoTipo",
                Width = 65
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColCorreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo",
                Name = "Correo",
                Width = 120
            });
            return this;
        }

        public IContactosColumnsGridBuilder ColFechaEspecial() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEspecial",
                HeaderText = "Fec. Especial",
                Name = "FechaEspecial",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 80,
                FormatString = "{0:dd MMM yy}",
            });
            return this;
        }
    }
}
