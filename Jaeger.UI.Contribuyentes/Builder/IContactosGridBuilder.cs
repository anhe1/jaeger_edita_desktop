﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public interface IContactosGridBuilder : IGridViewBuilder, IDisposable {
        IContactosTempletesGridBuilder Templetes();
    }

    public interface IContactosTempletesGridBuilder : IGridViewBuilder {
        IContactosTempletesGridBuilder Master();
    }

    public interface IContactosColumnsGridBuilder : IGridViewBuilder {
        IContactosColumnsGridBuilder ColIdContacto();
        IContactosColumnsGridBuilder ColNombre();
        IContactosColumnsGridBuilder ColTratoSugerido();
        IContactosColumnsGridBuilder ColPuesto();
        IContactosColumnsGridBuilder ColDetalles();
        IContactosColumnsGridBuilder ColTipoTelefono();
        IContactosColumnsGridBuilder ColTipoCorreo();
        IContactosColumnsGridBuilder ColTelefono();
        IContactosColumnsGridBuilder ColCorreo();
        IContactosColumnsGridBuilder ColFechaEspecial();
    }
}
