﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public class DomicilioGridBuilder : GridViewBuilder, IDomicilioGridBuilder, IGridViewBuilder, IDomicilioColumnsGridBuilder, IDomicilioTempletesGridBuilder {
        public DomicilioGridBuilder() : base() { }
        public IDomicilioTempletesGridBuilder Templetes() {
            return this;
        }

        public IDomicilioTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.ColTitulo().ColCboTipo().ColCalle().ColNoExterior().ColNoInterior().ColColonia().ColDelegacion().ColCodigoPostal().ColEstado().ColCiudad().ColPais().ColLocalidad().ColReferencia().ColTelefono();
            return this;
        }

        public IDomicilioColumnsGridBuilder ColTitulo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Titulo",
                HeaderText = "Título",
                Name = "Titulo",
                Width = 100
            });
            return this;
        }

        public IDomicilioColumnsGridBuilder ColCboTipo() {
            var combo = new GridViewComboBoxColumn {
                FieldName = "IdTipoDomicilio",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75
            };
            combo.DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio();
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "Id";
            this._Columns.Add(combo);
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColCalle() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Calle",
                HeaderText = "Calle",
                Name = "Calle",
                Width = 185
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColNoExterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoExterior",
                HeaderText = "No. Exterior",
                Name = "NoExterior",
                Width = 85
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColNoInterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoInterior",
                HeaderText = "No. Interior",
                Name = "NoInterior",
                Width = 85
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColColonia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Colonia",
                HeaderText = "Colonia",
                Name = "Colonia",
                Width = 150
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColDelegacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Municipio",
                HeaderText = "Delegación / Municipio",
                Name = "Municipio",
                Width = 150
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColCodigoPostal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CodigoPostal",
                HeaderText = "C. Postal",
                MaxLength = 5,
                Name = "CodigoPostal",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 150
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColCiudad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Ciudad",
                HeaderText = "Ciudad",
                Name = "Ciudad",
                Width = 95
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColPais() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Pais",
                HeaderText = "País",
                Name = "Pais",
                Width = 85
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColLocalidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Localidad",
                HeaderText = "Localidad",
                Name = "Localidad",
                Width = 85
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColReferencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 85
            });
            return this;
        }
        
        public IDomicilioColumnsGridBuilder ColTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 110
            });
            return this;
        }
    }
}
