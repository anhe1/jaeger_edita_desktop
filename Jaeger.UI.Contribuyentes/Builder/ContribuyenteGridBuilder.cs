﻿using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Contribuyentes.Services;
using System;

namespace Jaeger.UI.Contribuyentes.Builder {
    public class ContribuyenteGridBuilder : GridViewBuilder, IGridViewBuilder, IContribuyenteGridBuilder, IContribuyenteTempletesGridBuilder, IContribuyenteColumnsGridBuilder {
        public IContribuyenteTempletesGridBuilder Templetes() {
            return this;
        }

        public IContribuyenteColumnsGridBuilder Columns() {
            return this;
        }

        public IContribuyenteColumnsGridBuilder Master() {
            this._Columns.Clear();
            this.ColIdContribuyente().Activo().ColClave().ColValidaRFC().ColRFC().ColNombre().ColNombreComercial().ColRegimenFiscal().ColDomicilioF().ColCalle().ColNoExterior().ColNoInterior().ColColonia().ColDelegacion().ColCodigoPostal().ColEstado()
                .ColCiudad().ColPais().ColTelefono().ColCorreo().Creo().FechaNuevo().Modifica().FechaModifica();
            return this;
        }

        public IContribuyenteColumnsGridBuilder Domicilios() {
            this._Columns.Clear();
            this.ColTitulo().ColCboTipo().ColCalle().ColNoExterior().ColNoInterior().ColColonia().ColDelegacion().ColCodigoPostal().ColEstado().ColCiudad().ColPais().ColLocalidad().ColReferencia().ColTelefono();
            return this;
        }

        public IContribuyenteColumnsGridBuilder Contactos() {
            this._Columns.Clear();
            this.ColIdContacto().ColNombre().ColTratoSugerido().ColPuesto().ColDetalles().ColTipoTelefono().ColTelefono().ColTipoCorreo().ColCorreo().ColFechaEspecial();
            return this;
        }

        public IContribuyenteColumnsGridBuilder CuentasBancarias() {
            this._Columns.Clear();
            this.ColIdCuentaBancaria().ColVerificado().ColInstitucion().ColClave().ColNombreCorto().ColSucursal().ColNumeroCuenta().ColClabe().ColBeneficiario().ColPrimerApellido().ColSegundoApellido()
                .ColRFC().ColCargoMaximo().ColAlias().ColReferenciaNumerica().ColRefAlfanumerico().Creo();
            return this;
        }

        public IContribuyenteColumnsGridBuilder Search() {
            this._Columns.Clear();
            this.Columns().ColIdContribuyente().ColClave().ColNombre().ColNombreComercial();
            return this;
        }

        #region informacion del contribuyente
        /// <summary>
        /// indice de la tabla de contribuyentes
        /// </summary>
        public IContribuyenteColumnsGridBuilder ColIdContribuyente() {
            var column = this.GetColumnID();
            column.FieldName = "IdDirectorio";
            column.HeaderText = "Núm.";
            column.Name = "IdDirectorio";
            column.IsVisible = true;
            this._Columns.Add(column);
            return this;
        }

        public IContribuyenteColumnsGridBuilder Activo() {
            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo",
                IsVisible = false,
                VisibleInColumnChooser = false
            };
            var c0 = activo.ConditionalFormattingObjectList;
            c0.Add(this.ActivoConditionalFormat());
            this._Columns.Add(activo);
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColClave() {

            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColValidaRFC() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "IsValidRFC",
                HeaderText = "Val.",
                Name = "ValidaRFC",
                Width = 25
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage,
                Width = 90
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre o Razón Social",
                Name = "Nombre",
                Width = 280
            });
            return this;
        }

        /// <summary>
        /// obtener nombre comercial
        /// </summary>
        public IContribuyenteColumnsGridBuilder ColNombreComercial() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NombreComercial",
                HeaderText = "Nombre Comercial",
                Name = "NombreComercial",
                Width = 280,
                IsVisible = false
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColRegimenFiscal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RegimenFiscal",
                HeaderText = "Régimen\r\n Fiscal",
                Name = "RegimenFiscal",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColDomicilioF() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "DomicilioFiscal",
                HeaderText = "Dom. \r\nFiscal",
                Name = "DomicilioFiscalF",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 65,
                ReadOnly = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 110
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColCorreo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo",
                Name = "Correo",
                Width = 185
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder Modifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = false,
                ReadOnly = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder FechaModifica() {
            _Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Mod.",
                Name = "FechaModifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }
        #endregion

        #region columnas para la vista de domicilios
        /// <summary>
        /// columna de tipo de domicilio en modo texto
        /// </summary>
        public IContribuyenteColumnsGridBuilder ColCboTipo() {

            var combo = new GridViewComboBoxColumn {
                FieldName = "IdTipoDomicilio",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75,
                DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio(),
                DisplayMember = "Descripcion",
                ValueMember = "Id"
            };
            this._Columns.Add(combo);
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTitulo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Titulo",
                HeaderText = "Título",
                Name = "Titulo",
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// columna de tipo de domicilio el campo debe ser un indice
        /// </summary>
        public IContribuyenteColumnsGridBuilder ColCboIdTipo() {

            var combo = new GridViewComboBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75
            };
            combo.DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio();
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "Id";
            this._Columns.Add(combo);
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColCalle() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Calle",
                HeaderText = "Calle",
                Name = "Calle",
                Width = 185
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColNoExterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoExterior",
                HeaderText = "No. Exterior",
                Name = "NoExterior",
                Width = 85
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColNoInterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoInterior",
                HeaderText = "No. Interior",
                Name = "NoInterior",
                Width = 85
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColColonia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Colonia",
                HeaderText = "Colonia",
                Name = "Colonia",
                Width = 150
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColDelegacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Municipio",
                HeaderText = "Delegación / Municipio",
                Name = "Municipio",
                Width = 150
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColCodigoPostal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CodigoPostal",
                HeaderText = "C. Postal",
                MaxLength = 5,
                Name = "CodigoPostal",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 150
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColCiudad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Ciudad",
                HeaderText = "Ciudad",
                Name = "Ciudad",
                Width = 95
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColPais() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Pais",
                HeaderText = "País",
                Name = "Pais",
                Width = 85
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColLocalidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Localidad",
                HeaderText = "Localidad",
                Name = "Localidad",
                Width = 85
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColReferencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 85
            });
            return this;
        }
        #endregion

        #region contactos
        public IContribuyenteColumnsGridBuilder ColIdContacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTratoSugerido() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TratoSugerido",
                HeaderText = "Trato segerido",
                Name = "TratoSugerido",
                Width = 65,
                WrapText = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Puesto", HeaderText = "Puesto", Name = "Puesto", Width = 150 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColDetalles() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Detalles", HeaderText = "Detalles de contacto", Name = "Detalles", Width = 150 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTipoTelefono() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "TelefonoTipo", HeaderText = "Tipo Teléfono", Name = "TelefonoTipo", Width = 65 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTipoCorreo() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "CorreoTipo", HeaderText = "Tipo Correo", Name = "CorreoTipo", Width = 65 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColFechaEspecial() {
            this._Columns.Add(new GridViewDateTimeColumn { FieldName = "FechaEspecial", HeaderText = "Fec. Especial", Name = "FechaEspecial", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 80, FormatString = "{0:dd MMM yy}" });
            return this;
        }
        #endregion

        #region cuentas bancarias
        public IContribuyenteColumnsGridBuilder ColIdCuentaBancaria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Id",
                HeaderText = "Id",
                Width = 50,
                Name = "Id",
                IsVisible = false,
                VisibleInColumnChooser = false,
                ReadOnly = true
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColVerificado() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Verificado",
                HeaderText = "Verificado",
                Name = "Verificado"
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColInstitucion() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "Banco",
                HeaderText = "Institución Bancaria",
                Name = "Banco",
                Width = 120
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColNombreCorto() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "InsitucionBancaria", HeaderText = "Nombre Corto", Name = "InsitucionBancaria", Width = 150 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColSucursal() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Sucursal", HeaderText = "Sucursal", Name = "Sucursal", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColNumeroCuenta() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "NumeroDeCuenta", HeaderText = "Número de Cuenta", Name = "NumeroDeCuenta", Width = 120 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColClabe() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Clabe", HeaderText = "Cuenta Clabe", Name = "Clabe", Width = 110 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColBeneficiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre (s)",
                Name = "Beneficiario",
                Width = 150
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColPrimerApellido() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "PrimerApellido", HeaderText = "Apellido Paterno", Name = "PrimerApellido", Width = 110 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColSegundoApellido() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "SegundoApellido", HeaderText = "Apellido Materno", Name = "SegundoApellido", Width = 110 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColTipoCuenta() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "TipoCuenta", HeaderText = "Tipo de Cuenta", Name = "TipoCuenta", Width = 80 });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColCargoMaximo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CargoMaximo",
                HeaderText = "Cargo Máximo",
                Name = "CargoMaximo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColAlias() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Alias", HeaderText = "Alias de cuenta", Name = "Alias", Width = 75, WrapText = true });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColReferenciaNumerica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RefNumerica",
                HeaderText = "Ref. Númerica",
                MaxLength = 7,
                Name = "RefNumerica"
            });
            return this;
        }

        public IContribuyenteColumnsGridBuilder ColRefAlfanumerico() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "RefAlfanumerica", HeaderText = "Ref. Alfanúmerica", MaxLength = 40, Name = "RefAlfanumerica", Width = 80 });
            return this;
        }
        #endregion

        /// <summary>
        /// formato condicional para registros inactivos
        /// </summary>
        public ExpressionFormattingObject ActivoConditionalFormat() {
            return new ExpressionFormattingObject("Registro activo", "Activo = 0", true) {
                RowForeColor = Color.DarkGray
            };
        }
    }
}
