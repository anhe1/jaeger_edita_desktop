﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public interface ICuentaBancariaGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        ICuentaBancariaTempletesGridBuilder Templetes();
    }

    public interface ICuentaBancariaTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        ICuentaBancariaTempletesGridBuilder Master();
    }

    public interface ICuentaBancariaColumnsGridBuilder : IGridViewColumnsBuild {
        ICuentaBancariaColumnsGridBuilder Id();
        ICuentaBancariaColumnsGridBuilder Verificado();
        ICuentaBancariaColumnsGridBuilder Institucion();
        ICuentaBancariaColumnsGridBuilder Clave();
        ICuentaBancariaColumnsGridBuilder NombreCorto();
        ICuentaBancariaColumnsGridBuilder Sucursal();
        ICuentaBancariaColumnsGridBuilder ColNumeroCuenta();
        ICuentaBancariaColumnsGridBuilder Clabe();
        ICuentaBancariaColumnsGridBuilder Nombre();
        ICuentaBancariaColumnsGridBuilder PrimerApellido();
        ICuentaBancariaColumnsGridBuilder SegundoApellido();
        ICuentaBancariaColumnsGridBuilder RFC();
        ICuentaBancariaColumnsGridBuilder TipoCuenta();
        ICuentaBancariaColumnsGridBuilder CargoMaximo();
        ICuentaBancariaColumnsGridBuilder Alias();
        ICuentaBancariaColumnsGridBuilder ReferenciaNumerica();
        ICuentaBancariaColumnsGridBuilder RefAlfanumerico();
        ICuentaBancariaColumnsGridBuilder Creo();
    }
}
