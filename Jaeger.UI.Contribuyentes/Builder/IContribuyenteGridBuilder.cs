﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public interface IContribuyenteGridBuilder : IGridViewBuilder, IDisposable {
        IContribuyenteTempletesGridBuilder Templetes();
    }

    public interface IContribuyenteTempletesGridBuilder {
        IContribuyenteColumnsGridBuilder Master();

        IContribuyenteColumnsGridBuilder Domicilios();

        IContribuyenteColumnsGridBuilder Contactos();

        IContribuyenteColumnsGridBuilder CuentasBancarias();

        IContribuyenteColumnsGridBuilder Search();
    }

    public interface IContribuyenteColumnsGridBuilder : IGridViewBuilder {
        #region informacion del contribuyente
        IContribuyenteColumnsGridBuilder ColIdContribuyente();
        IContribuyenteColumnsGridBuilder Activo();
        IContribuyenteColumnsGridBuilder ColClave();
        IContribuyenteColumnsGridBuilder ColValidaRFC();

        IContribuyenteColumnsGridBuilder ColRFC();
        IContribuyenteColumnsGridBuilder ColNombre();

        /// <summary>
        /// obtener nombre comercial
        /// </summary>
        IContribuyenteColumnsGridBuilder ColNombreComercial();
        IContribuyenteColumnsGridBuilder ColRegimenFiscal();

        IContribuyenteColumnsGridBuilder ColDomicilioF();

        IContribuyenteColumnsGridBuilder ColTelefono();

        IContribuyenteColumnsGridBuilder ColCorreo();
        IContribuyenteColumnsGridBuilder Creo();
        IContribuyenteColumnsGridBuilder FechaNuevo();
        IContribuyenteColumnsGridBuilder Modifica();
        IContribuyenteColumnsGridBuilder FechaModifica();
        #endregion

        #region columnas para la vista de domicilios
        /// <summary>
        /// columna de tipo de domicilio en modo texto
        /// </summary>
        IContribuyenteColumnsGridBuilder ColCboTipo();

        IContribuyenteColumnsGridBuilder ColTitulo();

        /// <summary>
        /// columna de tipo de domicilio el campo debe ser un indice
        /// </summary>
        IContribuyenteColumnsGridBuilder ColCboIdTipo();

        IContribuyenteColumnsGridBuilder ColCalle();

        IContribuyenteColumnsGridBuilder ColNoExterior();

        IContribuyenteColumnsGridBuilder ColNoInterior();

        IContribuyenteColumnsGridBuilder ColColonia();

        IContribuyenteColumnsGridBuilder ColDelegacion();

        IContribuyenteColumnsGridBuilder ColCodigoPostal();

        IContribuyenteColumnsGridBuilder ColEstado();

        IContribuyenteColumnsGridBuilder ColCiudad();

        IContribuyenteColumnsGridBuilder ColPais();

        IContribuyenteColumnsGridBuilder ColLocalidad();

        IContribuyenteColumnsGridBuilder ColReferencia();
        #endregion

        #region contactos
        IContribuyenteColumnsGridBuilder ColIdContacto();

        IContribuyenteColumnsGridBuilder ColTratoSugerido();

        IContribuyenteColumnsGridBuilder ColPuesto();

        IContribuyenteColumnsGridBuilder ColDetalles();

        IContribuyenteColumnsGridBuilder ColTipoTelefono();

        IContribuyenteColumnsGridBuilder ColTipoCorreo();

        IContribuyenteColumnsGridBuilder ColFechaEspecial();
        #endregion

        #region cuentas bancarias
        IContribuyenteColumnsGridBuilder ColIdCuentaBancaria(); 

        IContribuyenteColumnsGridBuilder ColVerificado();

        IContribuyenteColumnsGridBuilder ColInstitucion();

        IContribuyenteColumnsGridBuilder ColNombreCorto();

        IContribuyenteColumnsGridBuilder ColSucursal();

        IContribuyenteColumnsGridBuilder ColNumeroCuenta();

        IContribuyenteColumnsGridBuilder ColClabe();

        IContribuyenteColumnsGridBuilder ColBeneficiario();

        IContribuyenteColumnsGridBuilder ColPrimerApellido();

        IContribuyenteColumnsGridBuilder ColSegundoApellido();

        IContribuyenteColumnsGridBuilder ColTipoCuenta();

        IContribuyenteColumnsGridBuilder ColCargoMaximo();

        IContribuyenteColumnsGridBuilder ColAlias();

        IContribuyenteColumnsGridBuilder ColReferenciaNumerica();

        IContribuyenteColumnsGridBuilder ColRefAlfanumerico();
        #endregion
    }
}
