﻿using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Builder {
    public interface IDomicilioGridBuilder : IGridViewBuilder, IGridViewTempleteBuild {
        IDomicilioTempletesGridBuilder Templetes();
    }

    public interface IDomicilioTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IDomicilioTempletesGridBuilder Master();
    }

    public interface IDomicilioColumnsGridBuilder : IGridViewColumnsBuild {
        IDomicilioColumnsGridBuilder ColTitulo();
        IDomicilioColumnsGridBuilder ColCboTipo(); 
        IDomicilioColumnsGridBuilder ColCalle();
        IDomicilioColumnsGridBuilder ColNoExterior();
        IDomicilioColumnsGridBuilder ColNoInterior();
        IDomicilioColumnsGridBuilder ColColonia();
        IDomicilioColumnsGridBuilder ColDelegacion();
        IDomicilioColumnsGridBuilder ColCodigoPostal();
        IDomicilioColumnsGridBuilder ColEstado();
        IDomicilioColumnsGridBuilder ColCiudad();
        IDomicilioColumnsGridBuilder ColPais();
        IDomicilioColumnsGridBuilder ColLocalidad();
        IDomicilioColumnsGridBuilder ColReferencia();
        IDomicilioColumnsGridBuilder ColTelefono();
    }
}
