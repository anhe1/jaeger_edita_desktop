﻿using Jaeger.UI.Common.Builder;
using System.Drawing;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Contribuyentes.Builder {
    public class CuentaBancariaGridBuilder : GridViewBuilder, IGridViewBuilder, ICuentaBancariaGridBuilder, ICuentaBancariaTempletesGridBuilder, ICuentaBancariaColumnsGridBuilder {
        public CuentaBancariaGridBuilder() : base() { }

        public ICuentaBancariaTempletesGridBuilder Templetes() {
            return this;
        }

        public ICuentaBancariaTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.Verificado().Institucion().Clave().NombreCorto().Sucursal().ColNumeroCuenta().Clabe().Nombre().PrimerApellido().SegundoApellido().RFC().CargoMaximo()
                .Alias().ReferenciaNumerica().RefAlfanumerico().Creo();
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Id",
                HeaderText = "Id",
                Width = 50,
                Name = "Id",
                IsVisible = false,
                VisibleInColumnChooser = false,
                ReadOnly = true
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Verificado() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Verificado",
                HeaderText = "Verificado",
                Name = "Verificado"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Institucion() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "Banco",
                HeaderText = "Institución Bancaria",
                Name = "Banco",
                Width = 120
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder NombreCorto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "InsitucionBancaria",
                HeaderText = "Nombre Corto",
                Name = "InsitucionBancaria",
                Width = 150
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Sucursal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Sucursal",
                HeaderText = "Sucursal",
                Name = "Sucursal",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder ColNumeroCuenta() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumeroDeCuenta",
                HeaderText = "Número de Cuenta",
                Name = "NumeroDeCuenta",
                Width = 120
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Clabe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clabe",
                HeaderText = "Cuenta Clabe",
                Name = "Clabe",
                Width = 110
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre (s)",
                Name = "Beneficiario",
                Width = 150
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder PrimerApellido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "PrimerApellido",
                HeaderText = "Apellido Paterno",
                Name = "PrimerApellido",
                Width = 110
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder SegundoApellido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "SegundoApellido",
                HeaderText = "Apellido Materno",
                Name = "SegundoApellido",
                Width = 110
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder RFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Rfc",
                HeaderText = "RFC",
                Name = "RFC",
                Width = 90
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder TipoCuenta() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TipoCuenta",
                HeaderText = "Tipo de Cuenta",
                Name = "TipoCuenta",
                Width = 80
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder CargoMaximo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CargoMaximo",
                HeaderText = "Cargo Máximo",
                Name = "CargoMaximo",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Alias() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Alias",
                HeaderText = "Alias de cuenta",
                Name = "Alias",
                Width = 75,
                WrapText = true
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder ReferenciaNumerica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RefNumerica",
                HeaderText = "Ref. Númerica",
                MaxLength = 7,
                Name = "RefNumerica"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder RefAlfanumerico() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RefAlfanumerica",
                HeaderText = "Ref. Alfanúmerica",
                MaxLength = 40,
                Name = "RefAlfanumerica",
                Width = 80
            });
            return this;
        }

        public ICuentaBancariaColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }
    }
}
