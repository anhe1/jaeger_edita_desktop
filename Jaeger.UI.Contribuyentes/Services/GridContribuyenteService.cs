﻿using Jaeger.UI.Common.Services;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Contribuyentes.Services {
    public static class GridContribuyenteService {

        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                ColIdContribuyente, ColClave, ColValidaRFC, ColRFC, ColNombre, ColNombreComercial, ColRegimenFiscal, ColDomicilioF, GridDomicilioService.ColCalle, GridDomicilioService.ColNoExterior,
                GridDomicilioService.ColNoInterior, GridDomicilioService.ColColonia, GridDomicilioService.ColDelegacion, GridDomicilioService.ColCodigoPostal, GridDomicilioService.ColEstado,
                GridDomicilioService.ColCiudad, GridDomicilioService.ColPais, ColTelefono, ColCorreo, GridTelerikCommon.ColCreo, GridTelerikCommon.ColFechaNuevo, GridTelerikCommon.ColModifica, GridTelerikCommon.ColFechaModifica
            };
        }

        public static GridViewDataColumn[] GetSearch() {
            return new GridViewDataColumn[] {
                ColIdContribuyente, ColClave, ColRFC, ColNombre, ColNombreComercial
            };
        }

        #region informacion del contribuyente
        public static GridViewTextBoxColumn ColIdContribuyente {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    VisibleInColumnChooser = false,
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clave",
                    HeaderText = "Clave",
                    Name = "Clave",
                    Width = 65,
                    IsVisible = false
                };
            }
        }

        public static GridViewCheckBoxColumn ColValidaRFC {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "IsValidRFC",
                    HeaderText = "Val.",
                    Name = "ValidaRFC",
                    Width = 25
                };
            }
        }

        public static GridViewTextBoxColumn ColRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RFC",
                    HeaderText = "RFC",
                    Name = "RFC",
                    TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage,
                    Width = 90
                };
            }
        }

        public static GridViewTextBoxColumn ColNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Nombre o Razón Social",
                    Name = "Nombre",
                    Width = 280,
                };
            }
        }

        /// <summary>
        /// obtener nombre comercial
        /// </summary>
        public static GridViewTextBoxColumn ColNombreComercial {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NombreComercial",
                    HeaderText = "Nombre Comercial",
                    Name = "NombreComercial",
                    Width = 280,
                    IsVisible = false
                };
            }
        }

        public static GridViewTextBoxColumn ColRegimenFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RegimenFiscal",
                    HeaderText = "Régimen\r\n Fiscal",
                    Name = "RegimenFiscal",
                    ReadOnly = true,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColDomicilioF {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DomicilioFiscal",
                    HeaderText = "Dom. \r\nFiscal",
                    Name = "DomicilioFiscalF",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 65,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 110
                };
            }
        }

        public static GridViewTextBoxColumn ColCorreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Correo",
                    HeaderText = "Correo",
                    Name = "Correo",
                    Width = 185
                };
            }
        }
        #endregion
    }
}
