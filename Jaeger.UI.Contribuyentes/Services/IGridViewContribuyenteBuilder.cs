﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Services {
    public interface IGridViewContribuyenteBuilder : IDisposable {
        IGridViewContribuyenteTempletesBuilder Templetes();
        IGridViewContribuyenteColumnsBuilder Columns();
    }

    public interface IGridViewContribuyenteTempletesBuilder {
        IGridViewContribuyenteColumnsBuilder GetColumns();

        IGridViewContribuyenteColumnsBuilder Domicilios();

        IGridViewContribuyenteColumnsBuilder Contactos();

        IGridViewContribuyenteColumnsBuilder CuentasBancarias();

        IGridViewContribuyenteColumnsBuilder Search();
    }

    public interface IGridViewContribuyenteColumnsBuilder : IGridViewBuilder {
        #region informacion del contribuyente
        IGridViewContribuyenteColumnsBuilder ColIdContribuyente();
        IGridViewContribuyenteColumnsBuilder ColClave();
        IGridViewContribuyenteColumnsBuilder ColValidaRFC();

        IGridViewContribuyenteColumnsBuilder ColRFC();
        IGridViewContribuyenteColumnsBuilder ColNombre();

        /// <summary>
        /// obtener nombre comercial
        /// </summary>
        IGridViewContribuyenteColumnsBuilder ColNombreComercial();
        IGridViewContribuyenteColumnsBuilder ColRegimenFiscal();

        IGridViewContribuyenteColumnsBuilder ColDomicilioF();

        IGridViewContribuyenteColumnsBuilder ColTelefono();

        IGridViewContribuyenteColumnsBuilder ColCorreo();
        IGridViewContribuyenteColumnsBuilder Creo();
        IGridViewContribuyenteColumnsBuilder FechaNuevo();
        IGridViewContribuyenteColumnsBuilder Modifica();
        IGridViewContribuyenteColumnsBuilder FechaModifica();
        #endregion

        #region columnas para la vista de domicilios
        /// <summary>
        /// columna de tipo de domicilio en modo texto
        /// </summary>
        IGridViewContribuyenteColumnsBuilder ColCboTipo();

        IGridViewContribuyenteColumnsBuilder ColTitulo();

        /// <summary>
        /// columna de tipo de domicilio el campo debe ser un indice
        /// </summary>
        IGridViewContribuyenteColumnsBuilder ColCboIdTipo();

        IGridViewContribuyenteColumnsBuilder ColCalle();

        IGridViewContribuyenteColumnsBuilder ColNoExterior();

        IGridViewContribuyenteColumnsBuilder ColNoInterior();

        IGridViewContribuyenteColumnsBuilder ColColonia();

        IGridViewContribuyenteColumnsBuilder ColDelegacion();

        IGridViewContribuyenteColumnsBuilder ColCodigoPostal();

        IGridViewContribuyenteColumnsBuilder ColEstado();

        IGridViewContribuyenteColumnsBuilder ColCiudad();

        IGridViewContribuyenteColumnsBuilder ColPais();

        IGridViewContribuyenteColumnsBuilder ColLocalidad();

        IGridViewContribuyenteColumnsBuilder ColReferencia();
        #endregion

        #region contactos
        IGridViewContribuyenteColumnsBuilder ColIdContacto();

        IGridViewContribuyenteColumnsBuilder ColTratoSugerido();

        IGridViewContribuyenteColumnsBuilder ColPuesto();

        IGridViewContribuyenteColumnsBuilder ColDetalles();

        IGridViewContribuyenteColumnsBuilder ColTipoTelefono();

        IGridViewContribuyenteColumnsBuilder ColTipoCorreo();

        IGridViewContribuyenteColumnsBuilder ColFechaEspecial();
        #endregion

        #region cuentas bancarias
        IGridViewContribuyenteColumnsBuilder ColIdCuentaBancaria(); 

        IGridViewContribuyenteColumnsBuilder ColVerificado();

        IGridViewContribuyenteColumnsBuilder ColInstitucion();

        IGridViewContribuyenteColumnsBuilder ColNombreCorto();

        IGridViewContribuyenteColumnsBuilder ColSucursal();

        IGridViewContribuyenteColumnsBuilder ColNumeroCuenta();

        IGridViewContribuyenteColumnsBuilder ColClabe();

        IGridViewContribuyenteColumnsBuilder ColBeneficiario();

        IGridViewContribuyenteColumnsBuilder ColPrimerApellido();

        IGridViewContribuyenteColumnsBuilder ColSegundoApellido();

        IGridViewContribuyenteColumnsBuilder ColTipoCuenta();

        IGridViewContribuyenteColumnsBuilder ColCargoMaximo();

        IGridViewContribuyenteColumnsBuilder ColAlias();

        IGridViewContribuyenteColumnsBuilder ColReferenciaNumerica();

        IGridViewContribuyenteColumnsBuilder ColRefAlfanumerico();
        #endregion
    }
}
