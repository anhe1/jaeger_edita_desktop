﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Contribuyentes.Services {
    public class GridViewContribuyenteBuilder : GridViewBuilder, IGridViewBuilder, IGridViewContribuyenteBuilder, IGridViewContribuyenteTempletesBuilder, IGridViewContribuyenteColumnsBuilder {
        public IGridViewContribuyenteTempletesBuilder Templetes() {
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Columns() {
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder GetColumns() {
            this._Columns.Clear();
            this.ColIdContribuyente().ColClave().ColValidaRFC().ColRFC().ColNombre().ColNombreComercial().ColRegimenFiscal().ColDomicilioF().ColCalle().ColNoExterior().ColNoInterior().ColColonia().ColDelegacion().ColCodigoPostal().ColEstado()
                .ColCiudad().ColPais().ColTelefono().ColCorreo().Creo().FechaNuevo().Modifica().FechaModifica();
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Domicilios() {
            this._Columns.Clear();
            this.ColTitulo().ColCboTipo().ColCalle().ColNoExterior().ColNoInterior().ColColonia().ColDelegacion().ColCodigoPostal().ColEstado().ColCiudad().ColPais().ColLocalidad().ColReferencia().ColTelefono();
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Contactos() {
            this._Columns.Clear();
            this.ColIdContacto().ColNombre().ColTratoSugerido().ColPuesto().ColDetalles().ColTipoTelefono().ColTelefono().ColTipoCorreo().ColCorreo().ColFechaEspecial();
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder CuentasBancarias() {
            this._Columns.Clear();
            this.ColIdCuentaBancaria().ColVerificado().ColInstitucion().ColClave().ColNombreCorto().ColSucursal().ColNumeroCuenta().ColClabe().ColBeneficiario().ColPrimerApellido().ColSegundoApellido()
                .ColRFC().ColCargoMaximo().ColAlias().ColReferenciaNumerica().ColRefAlfanumerico().Creo();
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Search() {
            this._Columns.Clear();
            this.Columns().ColIdContribuyente().ColClave().ColNombre().ColNombreComercial();
            return this;
        }

        #region informacion del contribuyente
        public IGridViewContribuyenteColumnsBuilder ColIdContribuyente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false,
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColClave() {

            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65,
                IsVisible = false
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColValidaRFC() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "IsValidRFC",
                HeaderText = "Val.",
                Name = "ValidaRFC",
                Width = 25
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColRFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "RFC",
                TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage,
                Width = 90
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColNombre() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Nombre", HeaderText = "Nombre o Razón Social", Name = "Nombre", Width = 280 });
            return this;
        }

        /// <summary>
        /// obtener nombre comercial
        /// </summary>
        public IGridViewContribuyenteColumnsBuilder ColNombreComercial() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "NombreComercial", HeaderText = "Nombre Comercial", Name = "NombreComercial", Width = 280, IsVisible = false });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColRegimenFiscal() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "RegimenFiscal", HeaderText = "Régimen\r\n Fiscal", Name = "RegimenFiscal", ReadOnly = true, TextAlignment = System.Drawing.ContentAlignment.MiddleCenter });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColDomicilioF() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "DomicilioFiscal", HeaderText = "Dom. \r\nFiscal", Name = "DomicilioFiscalF", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 65, ReadOnly = true });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTelefono() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Telefono", HeaderText = "Teléfono", Name = "Telefono", Width = 110 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColCorreo() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Correo", HeaderText = "Correo", Name = "Correo", Width = 185 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Sist.",
                Name = "FecNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder Modifica() {
            _Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Modifica",
                HeaderText = "Modifica",
                Name = "Modifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                IsVisible = false,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder FechaModifica() {
            _Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                FormatString = FormatStringDate,
                HeaderText = "Fec. Mod.",
                Name = "FechaModifica",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true,
                IsVisible = false
            });
            return this;
        }
        #endregion

        #region columnas para la vista de domicilios
        /// <summary>
        /// columna de tipo de domicilio en modo texto
        /// </summary>
        public IGridViewContribuyenteColumnsBuilder ColCboTipo() {

            var combo = new GridViewComboBoxColumn {
                FieldName = "IdTipoDomicilio",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75,
                DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio(),
                DisplayMember = "Descripcion",
                ValueMember = "Id"
            };
            this._Columns.Add(combo);
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTitulo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Titulo",
                HeaderText = "Título",
                Name = "Titulo",
                Width = 100
            });
            return this;
        }

        /// <summary>
        /// columna de tipo de domicilio el campo debe ser un indice
        /// </summary>
        public IGridViewContribuyenteColumnsBuilder ColCboIdTipo() {

            var combo = new GridViewComboBoxColumn {
                FieldName = "Tipo",
                HeaderText = "Tipo",
                Name = "Tipo",
                Width = 75
            };
            combo.DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio();
            combo.DisplayMember = "Descripcion";
            combo.ValueMember = "Id";
            this._Columns.Add(combo);
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColCalle() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Calle",
                HeaderText = "Calle",
                Name = "Calle",
                Width = 185
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColNoExterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoExterior",
                HeaderText = "No. Exterior",
                Name = "NoExterior",
                Width = 85
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColNoInterior() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NoInterior",
                HeaderText = "No. Interior",
                Name = "NoInterior",
                Width = 85
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColColonia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Colonia",
                HeaderText = "Colonia",
                Name = "Colonia",
                Width = 150
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColDelegacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Municipio",
                HeaderText = "Delegación / Municipio",
                Name = "Municipio",
                Width = 150
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColCodigoPostal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CodigoPostal",
                HeaderText = "C. Postal",
                MaxLength = 5,
                Name = "CodigoPostal",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColEstado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 150
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColCiudad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Ciudad",
                HeaderText = "Ciudad",
                Name = "Ciudad",
                Width = 95
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColPais() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Pais",
                HeaderText = "País",
                Name = "Pais",
                Width = 85
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColLocalidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Localidad",
                HeaderText = "Localidad",
                Name = "Localidad",
                Width = 85
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColReferencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 85
            });
            return this;
        }
        #endregion

        #region contactos
        public IGridViewContribuyenteColumnsBuilder ColIdContacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Id",
                HeaderText = "Id",
                IsVisible = false,
                Name = "Id",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTratoSugerido() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "TratoSugerido",
                HeaderText = "Trato segerido",
                Name = "TratoSugerido",
                Width = 65,
                WrapText = true
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Puesto", HeaderText = "Puesto", Name = "Puesto", Width = 150 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColDetalles() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Detalles", HeaderText = "Detalles de contacto", Name = "Detalles", Width = 150 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTipoTelefono() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "TelefonoTipo", HeaderText = "Tipo Teléfono", Name = "TelefonoTipo", Width = 65 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTipoCorreo() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "CorreoTipo", HeaderText = "Tipo Correo", Name = "CorreoTipo", Width = 65 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColFechaEspecial() {
            this._Columns.Add(new GridViewDateTimeColumn { FieldName = "FechaEspecial", HeaderText = "Fec. Especial", Name = "FechaEspecial", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter, Width = 80, FormatString = "{0:dd MMM yy}" });
            return this;
        }
        #endregion

        #region cuentas bancarias
        public IGridViewContribuyenteColumnsBuilder ColIdCuentaBancaria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Id",
                HeaderText = "Id",
                Width = 50,
                Name = "Id",
                IsVisible = false,
                VisibleInColumnChooser = false,
                ReadOnly = true
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColVerificado() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Verificado",
                HeaderText = "Verificado",
                Name = "Verificado"
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColInstitucion() {
            this._Columns.Add(new GridViewMultiComboBoxColumn {
                FieldName = "Banco",
                HeaderText = "Institución Bancaria",
                Name = "Banco",
                Width = 120
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColNombreCorto() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "InsitucionBancaria", HeaderText = "Nombre Corto", Name = "InsitucionBancaria", Width = 150 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColSucursal() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Sucursal", HeaderText = "Sucursal", Name = "Sucursal", TextAlignment = System.Drawing.ContentAlignment.MiddleCenter });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColNumeroCuenta() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "NumeroDeCuenta", HeaderText = "Número de Cuenta", Name = "NumeroDeCuenta", Width = 120 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColClabe() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Clabe", HeaderText = "Cuenta Clabe", Name = "Clabe", Width = 110 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColBeneficiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre (s)",
                Name = "Beneficiario",
                Width = 150
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColPrimerApellido() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "PrimerApellido", HeaderText = "Apellido Paterno", Name = "PrimerApellido", Width = 110 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColSegundoApellido() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "SegundoApellido", HeaderText = "Apellido Materno", Name = "SegundoApellido", Width = 110 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColTipoCuenta() {
            this._Columns.Add(new GridViewComboBoxColumn { FieldName = "TipoCuenta", HeaderText = "Tipo de Cuenta", Name = "TipoCuenta", Width = 80 });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColCargoMaximo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(double),
                FieldName = "CargoMaximo",
                HeaderText = "Cargo Máximo",
                Name = "CargoMaximo",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 80
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColAlias() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "Alias", HeaderText = "Alias de cuenta", Name = "Alias", Width = 75, WrapText = true });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColReferenciaNumerica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RefNumerica",
                HeaderText = "Ref. Númerica",
                MaxLength = 7,
                Name = "RefNumerica"
            });
            return this;
        }

        public IGridViewContribuyenteColumnsBuilder ColRefAlfanumerico() {
            this._Columns.Add(new GridViewTextBoxColumn { FieldName = "RefAlfanumerica", HeaderText = "Ref. Alfanúmerica", MaxLength = 40, Name = "RefAlfanumerica", Width = 80 });
            return this;
        }
        #endregion
    }
}
