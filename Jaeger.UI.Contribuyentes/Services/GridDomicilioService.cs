﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Contribuyentes.Services {
    public static class GridDomicilioService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                ColTitulo, ColCboTipo, ColCalle, ColNoExterior, ColNoInterior, ColColonia, ColDelegacion, ColCodigoPostal, ColEstado, ColCiudad, ColPais, ColLocalidad, ColReferencia, ColTelefono
            };
        }

        #region columnas para la vista de domicilios
        /// <summary>
        /// columna de tipo de domicilio en modo texto
        /// </summary>
        public static GridViewComboBoxColumn ColCboTipo {
            get {
                var combo = new GridViewComboBoxColumn {
                    FieldName = "IdTipoDomicilio",
                    HeaderText = "Tipo",
                    Name = "Tipo",
                    Width = 75
                };
                combo.DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio();
                combo.DisplayMember = "Descripcion";
                combo.ValueMember = "Id";
                return combo;
            }
        }

        public static GridViewTextBoxColumn ColTitulo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Titulo",
                    HeaderText = "Título",
                    Name = "Titulo",
                    Width = 100
                };
            }
        }

        /// <summary>
        /// columna de tipo de domicilio el campo debe ser un indice
        /// </summary>
        public static GridViewComboBoxColumn ColCboIdTipo {
            get {
                var combo = new GridViewComboBoxColumn {
                    FieldName = "Tipo",
                    HeaderText = "Tipo",
                    Name = "Tipo",
                    Width = 75
                };
                combo.DataSource = Aplication.Contribuyentes.Services.DomicilioService.GetTipoDomicilio();
                combo.DisplayMember = "Descripcion";
                combo.ValueMember = "Id";
                return combo;
            }
        }

        public static GridViewTextBoxColumn ColCalle {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Calle",
                    HeaderText = "Calle",
                    Name = "Calle",
                    Width = 185
                };
            }
        }

        public static GridViewTextBoxColumn ColNoExterior {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NoExterior",
                    HeaderText = "No. Exterior",
                    Name = "NoExterior",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColNoInterior {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NoInterior",
                    HeaderText = "No. Interior",
                    Name = "NoInterior",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColColonia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Colonia",
                    HeaderText = "Colonia",
                    Name = "Colonia",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColDelegacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Municipio",
                    HeaderText = "Delegación / Municipio",
                    Name = "Municipio",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCodigoPostal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CodigoPostal",
                    HeaderText = "C. Postal",
                    MaxLength = 5,
                    Name = "CodigoPostal",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColEstado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Estado",
                    HeaderText = "Estado",
                    Name = "Estado",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCiudad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Ciudad",
                    HeaderText = "Ciudad",
                    Name = "Ciudad",
                    Width = 95
                };
            }
        }

        public static GridViewTextBoxColumn ColPais {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Pais",
                    HeaderText = "País",
                    Name = "Pais",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColLocalidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Localidad",
                    HeaderText = "Localidad",
                    Name = "Localidad",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColReferencia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Referencia",
                    HeaderText = "Referencia",
                    Name = "Referencia",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 110
                };
            }
        }
        #endregion
    }
}
