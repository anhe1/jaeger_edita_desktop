﻿using Telerik.WinControls.UI;

namespace Jaeger.UI.Contribuyentes.Services {
    public static class GridContactoService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                ColIdContacto, ColNombre, ColTratoSugerido, ColPuesto, ColDetalles, ColTipoTelefono, ColTelefono, ColTipoCorreo, ColCorreo, ColFechaEspecial
            };
        }
        public static GridViewTextBoxColumn ColIdContacto {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Nombre de contacto",
                    Name = "Nombre",
                    Width = 250
                };
            }
        }

        public static GridViewComboBoxColumn ColTratoSugerido {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "TratoSugerido",
                    HeaderText = "Trato segerido",
                    Name = "TratoSugerido",
                    Width = 65,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColPuesto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Puesto",
                    HeaderText = "Puesto",
                    Name = "Puesto",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColDetalles {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Detalles",
                    HeaderText = "Detalles de contacto",
                    Name = "Detalles",
                    Width = 150
                };
            }
        }

        public static GridViewComboBoxColumn ColTipoTelefono {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "TelefonoTipo",
                    HeaderText = "Tipo Teléfono",
                    Name = "TelefonoTipo",
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 80
                };
            }
        }

        public static GridViewComboBoxColumn ColTipoCorreo {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "CorreoTipo",
                    HeaderText = "Tipo Correo",
                    Name = "CorreoTipo",
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColCorreo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Correo",
                    HeaderText = "Correo",
                    Name = "Correo",
                    Width = 120
                };
            }
        }

        public static GridViewDateTimeColumn ColFechaEspecial {
            get {
                return new GridViewDateTimeColumn {
                    FieldName = "FechaEspecial",
                    HeaderText = "Fec. Especial",
                    Name = "FechaEspecial",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 80,
                    FormatString = "{0:dd MMM yy}",
                };
            }
        }
    }
}
