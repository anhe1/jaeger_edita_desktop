﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Contribuyentes.Services {
    public static class GridCuentaBancariaService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                GridTelerikCommon.ColId, ColVerificado, ColInstitucion, ColClave, ColNombreCorto, ColSucursal, ColNumeroCuenta, ColClabe, ColNombre, ColPrimerApellido, ColSegundoApellido, ColRFC, ColCargoMaximo, ColAlias, ColReferenciaNumerica,
                ColRefAlfanumerico, GridTelerikCommon.ColCreo
            };
        }

        public static GridViewCheckBoxColumn ColVerificado {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "Verificado",
                    HeaderText = "Verificado",
                    Name = "Verificado"
                };
            }
        }

        public static GridViewMultiComboBoxColumn ColInstitucion {
            get {
                return new GridViewMultiComboBoxColumn {
                    FieldName = "Banco",
                    HeaderText = "Institución Bancaria",
                    Name = "Banco",
                    Width = 120
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clave",
                    HeaderText = "Clave",
                    Name = "Clave",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColNombreCorto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "InsitucionBancaria",
                    HeaderText = "Nombre Corto",
                    Name = "InsitucionBancaria",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColSucursal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Sucursal",
                    HeaderText = "Sucursal",
                    Name = "Sucursal",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColNumeroCuenta {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NumeroDeCuenta",
                    HeaderText = "Número de Cuenta",
                    Name = "NumeroDeCuenta",
                    Width = 120
                };
            }
        }

        public static GridViewTextBoxColumn ColClabe {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clabe",
                    HeaderText = "Cuenta Clabe",
                    Name = "Clabe",
                    Width = 110
                };
            }
        }

        public static GridViewTextBoxColumn ColNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Nombre (s)",
                    Name = "Beneficiario",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColPrimerApellido {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "PrimerApellido",
                    HeaderText = "Apellido Paterno",
                    Name = "PrimerApellido",
                    Width = 110
                };
            }
        }

        public static GridViewTextBoxColumn ColSegundoApellido {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "SegundoApellido",
                    HeaderText = "Apellido Materno",
                    Name = "SegundoApellido",
                    Width = 110
                };
            }
        }

        public static GridViewTextBoxColumn ColRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Rfc",
                    HeaderText = "RFC",
                    Name = "RFC",
                    Width = 90
                };
            }
        }

        public static GridViewComboBoxColumn ColTipoCuenta {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "TipoCuenta",
                    HeaderText = "Tipo de Cuenta",
                    Name = "TipoCuenta",
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColCargoMaximo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(double),
                    FieldName = "CargoMaximo",
                    HeaderText = "Cargo Máximo",
                    Name = "CargoMaximo",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColAlias {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Alias",
                    HeaderText = "Alias de cuenta",
                    Name = "Alias",
                    Width = 75,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColReferenciaNumerica {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RefNumerica",
                    HeaderText = "Ref. Númerica",
                    MaxLength = 7,
                    Name = "RefNumerica"
                };
            }
        }
         
        public static GridViewTextBoxColumn ColRefAlfanumerico {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RefAlfanumerica",
                    HeaderText = "Ref. Alfanúmerica",
                    MaxLength = 40,
                    Name = "RefAlfanumerica",
                    Width = 80
                };
            }
        }
    }
}
