﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class Base64Form : Form {
        public Base64Form() {
            InitializeComponent();
        }

        private void Base64Form_Load(object sender, EventArgs e) {

        }

        private void buttonCER_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.txt",
                DefaultExt = ".txt",
                Filter = "Archivo TXT (*.txt)|*.txt",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.txtArchivoCertificado.Text = openFileDialog.FileName;
                var _base64 = System.IO.File.ReadAllText(openFileDialog.FileName);
                var _bytes = Convert.FromBase64String(_base64);
                System.IO.File.WriteAllBytes(@"C:\Jaeger\Jaeger.Temporal\descarga.gzip", _bytes);
            }
        }
    }
}
