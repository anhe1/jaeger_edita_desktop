﻿namespace Jaeger.UI.Forms {
    partial class Comprobante1FiscalRetencionForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.textBoxEmisorNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxEmisorRFC = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEmisorCURP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxReceptorCURP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxReceptorRFC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxReceptorNombre = new System.Windows.Forms.TextBox();
            this.retencionesEmisorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ToolBarButtonAbrir = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonGuardar = new System.Windows.Forms.ToolStripButton();
            this.ToolBarButtonCerrar = new System.Windows.Forms.ToolStripButton();
            this.retencionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.treeViewControl1 = new Jaeger.UI.Forms.TreeViewControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retencionesEmisorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retencionesBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarButtonAbrir,
            this.ToolBarButtonGuardar,
            this.ToolBarButtonCerrar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // textBoxEmisorNombre
            // 
            this.textBoxEmisorNombre.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "NomDenRazSocE", true));
            this.textBoxEmisorNombre.Location = new System.Drawing.Point(62, 15);
            this.textBoxEmisorNombre.Name = "textBoxEmisorNombre";
            this.textBoxEmisorNombre.Size = new System.Drawing.Size(361, 20);
            this.textBoxEmisorNombre.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Emisor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "RFC:";
            // 
            // textBoxEmisorRFC
            // 
            this.textBoxEmisorRFC.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "RFCEmisor", true));
            this.textBoxEmisorRFC.Location = new System.Drawing.Point(62, 41);
            this.textBoxEmisorRFC.Name = "textBoxEmisorRFC";
            this.textBoxEmisorRFC.Size = new System.Drawing.Size(100, 20);
            this.textBoxEmisorRFC.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(177, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "CURP:";
            // 
            // textBoxEmisorCURP
            // 
            this.textBoxEmisorCURP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "CURPE", true));
            this.textBoxEmisorCURP.Location = new System.Drawing.Point(221, 42);
            this.textBoxEmisorCURP.Name = "textBoxEmisorCURP";
            this.textBoxEmisorCURP.Size = new System.Drawing.Size(100, 20);
            this.textBoxEmisorCURP.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(177, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "CURP:";
            // 
            // textBoxReceptorCURP
            // 
            this.textBoxReceptorCURP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "CURPE", true));
            this.textBoxReceptorCURP.Location = new System.Drawing.Point(221, 95);
            this.textBoxReceptorCURP.Name = "textBoxReceptorCURP";
            this.textBoxReceptorCURP.Size = new System.Drawing.Size(100, 20);
            this.textBoxReceptorCURP.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "RFC:";
            // 
            // textBoxReceptorRFC
            // 
            this.textBoxReceptorRFC.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "RFCEmisor", true));
            this.textBoxReceptorRFC.Location = new System.Drawing.Point(62, 94);
            this.textBoxReceptorRFC.Name = "textBoxReceptorRFC";
            this.textBoxReceptorRFC.Size = new System.Drawing.Size(100, 20);
            this.textBoxReceptorRFC.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Receptor:";
            // 
            // textBoxReceptorNombre
            // 
            this.textBoxReceptorNombre.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.retencionesEmisorBindingSource, "NomDenRazSocE", true));
            this.textBoxReceptorNombre.Location = new System.Drawing.Point(62, 68);
            this.textBoxReceptorNombre.Name = "textBoxReceptorNombre";
            this.textBoxReceptorNombre.Size = new System.Drawing.Size(361, 20);
            this.textBoxReceptorNombre.TabIndex = 7;
            // 
            // retencionesEmisorBindingSource
            // 
            this.retencionesEmisorBindingSource.DataSource = typeof(Jaeger.CFDRetencion.V10.RetencionesEmisor);
            // 
            // ToolBarButtonAbrir
            // 
            this.ToolBarButtonAbrir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonAbrir.Name = "ToolBarButtonAbrir";
            this.ToolBarButtonAbrir.Size = new System.Drawing.Size(53, 22);
            this.ToolBarButtonAbrir.Text = "Abrir";
            this.ToolBarButtonAbrir.Click += new System.EventHandler(this.ToolBarButtonAbrir_Click);
            // 
            // ToolBarButtonGuardar
            // 
            this.ToolBarButtonGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonGuardar.Name = "ToolBarButtonGuardar";
            this.ToolBarButtonGuardar.Size = new System.Drawing.Size(69, 22);
            this.ToolBarButtonGuardar.Text = "Guardar";
            this.ToolBarButtonGuardar.Click += new System.EventHandler(this.ToolBarButtonGuardar_Click);
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Size = new System.Drawing.Size(59, 22);
            this.ToolBarButtonCerrar.Text = "Cerrar";
            // 
            // retencionesBindingSource
            // 
            this.retencionesBindingSource.DataSource = typeof(Jaeger.CFDRetencion.V10.Retenciones);
            // 
            // treeViewControl1
            // 
            this.treeViewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewControl1.Location = new System.Drawing.Point(3, 3);
            this.treeViewControl1.Name = "treeViewControl1";
            this.treeViewControl1.Size = new System.Drawing.Size(786, 393);
            this.treeViewControl1.TabIndex = 13;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 425);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.textBoxEmisorNombre);
            this.tabPage1.Controls.Add(this.textBoxReceptorCURP);
            this.tabPage1.Controls.Add(this.textBoxEmisorRFC);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.textBoxReceptorRFC);
            this.tabPage1.Controls.Add(this.textBoxEmisorCURP);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.textBoxReceptorNombre);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.treeViewControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Comprobante1FiscalRetencionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Comprobante1FiscalRetencionForm";
            this.Text = "CFD Retención";
            this.Load += new System.EventHandler(this.Comprobante1FiscalRetencionForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retencionesEmisorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retencionesBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolBarButtonAbrir;
        private System.Windows.Forms.TextBox textBoxEmisorNombre;
        private System.Windows.Forms.ToolStripButton ToolBarButtonCerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxEmisorRFC;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEmisorCURP;
        private System.Windows.Forms.ToolStripButton ToolBarButtonGuardar;
        private System.Windows.Forms.BindingSource retencionesBindingSource;
        private System.Windows.Forms.BindingSource retencionesEmisorBindingSource;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxReceptorCURP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxReceptorRFC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxReceptorNombre;
        private TreeViewControl treeViewControl1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}