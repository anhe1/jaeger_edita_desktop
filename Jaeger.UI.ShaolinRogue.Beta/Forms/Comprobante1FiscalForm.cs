﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Jaeger.SAT.CFDI.V33;

namespace Jaeger.UI.Forms {
    public partial class Comprobante1FiscalForm : Form {
        private Comprobante comprobante;
        public Comprobante1FiscalForm() {
            InitializeComponent();
        }

        private void Comprobante1FiscalForm_Load(object sender, EventArgs e) {
            
        }

        private void ToolBarButtonAbrir_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                var pdf = new Aplication.Html.Services.HtmlToPDFService();
                this.comprobante = Comprobante.Load(openFile.FileName);
                if (this.comprobante != null) {
                    pdf.Procesar(this.comprobante, openFile.FileName + "_.pdf");
                        this.treeViewControl1.Crear(this.comprobante.OriginalXmlString);
                    //var d = new ReporteForm(this.comprobante);
                    //d.Show();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e) {
            
            var openFile = new OpenFileDialog { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                this.comprobante = Comprobante.Load(openFile.FileName);
                if (this.comprobante != null) {
                    var reporte = new ReporteForm(this.comprobante);
                    reporte.Show();
                }
            }
        }
    }
}
