﻿using System;
using System.Collections.Generic;

using Jaeger.Domain.Base.Services;
using Jaeger.Util.Services;

namespace Jaeger.UI.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.CFDI");
        
        public ReporteForm(string rfc, string razonSocial) : base(rfc, razonSocial) {
        }
        public ReporteForm(object sender) : base("", "") {
            this.CurrentObject = sender;
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            this.PathLogo = @"C:\Jaeger\Jaeger.Media\logo-ipo.png";
            if (this.CurrentObject.GetType() == typeof(SAT.CFDI.V33.Comprobante)) {
                this.CrearComprobanteV33();
            } else if (this.CurrentObject.GetType() == typeof(SAT.CFDI.V32.Comprobante)) {
                this.CrearComprobanteV32();
            }
        }

        private void CrearComprobanteV32() {
            throw new NotImplementedException();
        }

        private void CrearComprobanteV33() {
            var current = (SAT.CFDI.V33.Comprobante)this.CurrentObject;

            if (current.Complemento.Pagos == null) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.CFDI.Reports.ReportFactura_3.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.CFDI.Reports.Cfdiv33Pagos10.rdlc");
                this.SetDataSource("PagosPago", DbConvert.ConvertToDataTable(new List<SAT.CFDI.Complemento.Pagos.V10.PagosPago>() { current.Complemento.Pagos.Pago[0] }));
                this.SetDataSource("PagoDoctoRelacionado", DbConvert.ConvertToDataTable(current.Complemento.Pagos.Pago[0].DoctoRelacionado));
            }
            this.SetLogotipo();
            string[] qr = { "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?", current.Emisor.Rfc, current.Receptor.Rfc, current.Total.ToString(), current.Complemento.TimbreFiscalDigital.UUID, current.Complemento.TimbreFiscalDigital.SelloSAT };
            this.ImagenQR = QRCodeExtension.GetQRBase64(qr);
            this.SetDisplayName("Comprobante Fiscal V33");
            this.SetParameter("ImagenQR", this.ImagenQR);
            this.SetParameter("TotalEnLetra", NumeroALetras.Convertir(Double.Parse(current.Total.ToString()), 1));
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(new List<SAT.CFDI.V33.Comprobante>() { current }));
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.SetDataSource("TimbreFiscalV11", DbConvert.ConvertToDataTable(new List<SAT.CFDI.Complemento.TimbreFiscal.V11.TimbreFiscalDigital>() { current.Complemento.TimbreFiscalDigital }));
            this.SetDataSource("Emisor", DbConvert.ConvertToDataTable(new List<SAT.CFDI.V33.ComprobanteEmisor>() { current.Emisor }));
            this.SetDataSource("Receptor", DbConvert.ConvertToDataTable(new List<SAT.CFDI.V33.ComprobanteReceptor>() { current.Receptor }));
            this.SetDataSource("Impuestos", DbConvert.ConvertToDataTable(new List<SAT.CFDI.V33.ComprobanteImpuestos>() { current.Impuestos }));
            this.Finalizar();
        }
    }
}
