﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Html.Contracts;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.Forms {
    public partial class ConvertirHtmlToPDFForm : Form {
        private IHtmlToPDFService  pdf = new Aplication.Html.Services.HtmlToPDFService();
        private object comprobante;

        public ConvertirHtmlToPDFForm() {
            InitializeComponent();
        }

        private void ConvertirHtmlToPDFForm_Load(object sender, EventArgs e) {
            this.FileTempleteHtmlText.Text = Properties.Settings.Default.LastPathTemplete;
            this.FileLogotipoText.Text = Properties.Settings.Default.LastPathLogo;
            if (System.IO.File.Exists(this.FileLogotipoText.Text))
                this.LogotipoPicture.Load(this.FileLogotipoText.Text);
        }

        private void FileXmlButton_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() {
                Title = "Selecciona el comprobante CFDI ver. 3.3",
                Filter = "*.xml|*.XML"
            };

            if (openFile.ShowDialog(this) != DialogResult.OK)
                return;

            try {
                this.FileXmlText.Text = openFile.FileName;
                this.comprobante = SAT.CFDI.V33.Comprobante.Load(this.FileXmlText.Text);
                if (this.comprobante == null) {
                    this.comprobante = SAT.CFDI.V40.Comprobante.Load(this.FileXmlText.Text);
                }
                this.FilePDFText.Text = System.IO.Path.ChangeExtension(this.FileXmlText.Text, "pdf");
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
                this.comprobante = null;
            } 
        }

        private void FileTempleteHTMLButton_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() {
                Title = "Selecciona el plantilla HTML",
                Filter = "*.html|*.html"
            };

            if (openFile.ShowDialog(this) != DialogResult.OK)
                return;
            this.FileTempleteHtmlText.Text = openFile.FileName;
            Properties.Settings.Default.LastPathTemplete = openFile.FileName;
            Properties.Settings.Default.Save();
        }

        private void FileLogotipoButton_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog() {
                Title = "Selecciona el imagen PNG",
                Filter = "*.png|*.png"
            };

            if (openFile.ShowDialog(this) != DialogResult.OK)
                return;
            this.FileLogotipoText.Text = openFile.FileName;
            this.LogotipoPicture.Load(this.FileLogotipoText.Text);
            Properties.Settings.Default.LastPathLogo = openFile.FileName;
            Properties.Settings.Default.Save();
        }

        private void ProcessButton_Click(object sender, EventArgs e) {
            if (this.comprobante != null) {
                using (var espera = new WaitingForm(this.Procesar)) {
                    Text = "Procesando archivo ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void Procesar() {
            this.pdf.Logo = this.FileLogotipoText.Text;
            this.pdf.PathTemplete = this.FileTempleteHtmlText.Text;
            this.pdf.Procesar(this.comprobante, System.IO.Path.GetFileNameWithoutExtension(this.FileXmlText.Text));
            Properties.Settings.Default.Save();
        }

        private void FileLogotipoLabel_Click(object sender, EventArgs e) {

        }
    }
}
