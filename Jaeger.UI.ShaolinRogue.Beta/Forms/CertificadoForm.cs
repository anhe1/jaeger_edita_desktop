﻿using System;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Jaeger.Crypto.Services;

namespace Jaeger.UI.Forms {
    public partial class CertificadoForm : Form {
        private Certificate certificate;
        private PrivateKey privateKey;
        private Color colorInvalido;
        private Color colorValido;
        private Color colorEditable;
        private string _pathOpenSSL = @"D:\bitbucket\jaeger_edita_desktop\documentos";

        public CertificadoForm() {
            InitializeComponent();
        }

        private void CertificadoForm_Load(object sender, EventArgs e) {
            this.colorInvalido = Color.Bisque;
            this.colorValido = Color.MintCream;
            this.colorEditable = Color.LemonChiffon;
        }

        private void ButtonCER_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.cer",
                DefaultExt = ".cer",
                Filter = "Archivo CER (*.cer)|*.cer",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.txtArchivoCertificado.Text = openFileDialog.FileName;
                this.VerificacionDeArchivos();
                this.LeerCertificado();
            }
        }

        private void ButtonKEY_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo de la llave privada *.key",
                DefaultExt = ".key",
                Filter = "Archivo KEY (*.key)|*.key",
                FilterIndex = 1,
                FileName = ""
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.txtLlavePrivada.Text = openFileDialog.FileName;
            }
        }

        private void VerificacionDeArchivos() {
            if (!File.Exists(this.txtArchivoCertificado.Text)) {
                this.txtArchivoCertificado.BackColor = this.colorInvalido;
            } else {
                this.txtArchivoCertificado.BackColor = this.colorValido;
            }
            if (!File.Exists(this.txtLlavePrivada.Text)) {
                this.txtLlavePrivada.BackColor = this.colorInvalido;
            } else {
                this.txtLlavePrivada.BackColor = this.colorValido;
            }
            this.VerificarContrasenas();
        }

        private void VerificarContrasenas() {
            if (this.txtConfirmacion.Text == "") {
                this.txtContrasena.BackColor = this.colorEditable;
                this.txtConfirmacion.BackColor = this.colorEditable;
            } else if (this.txtContrasena.Text == this.txtConfirmacion.Text) {
                this.txtContrasena.BackColor = this.colorEditable;
                this.txtConfirmacion.BackColor = this.colorEditable;
            } else {
                this.txtContrasena.BackColor = this.colorInvalido;
                this.txtConfirmacion.BackColor = this.colorInvalido;
            }
        }

        private void LeerCertificado() {
            string text = this.txtArchivoCertificado.Text;
            if (!File.Exists(text)) {
                this.txtArchivoCertificado.BackColor = this.colorInvalido;
                this.txtCertificadoB64.Text = "";
            } else {

                if (LeerCertificado(text)) {
                    this.txtNumeroDeSerieCertificado.BackColor = this.colorValido;
                } else {
                    MessageBox.Show("no se puede leer certificado, intente nuevamente.", "Error", MessageBoxButtons.OK);
                    this.txtArchivoCertificado.BackColor = this.colorInvalido;
                    this.txtCertificadoB64.Text = "";
                }
            }
        }

        private bool LeerCertificado(string fileName) {
            //var informacion = new CryptoCertificateInfo(fileName);
            //if (informacion != null) {
            //    if (informacion.Valid) {
            //        txtValidoDesde.Text = informacion.Inicio.Value.ToString("yyyy/MM/dd HH:mm:ss");
            //        txtValidoHasta.Text = informacion.Fin.Value.ToString("yyyy/MM/dd HH:mm:ss");
            //        this.RFC.Text = informacion.RFC;
            //        this.TipoCertificado.Text = informacion.TipoCertificado;
            //        this.txtNumeroDeSerieCertificado.Text = informacion.NoSerie;
            //        this.txtCertificadoB64.Text = informacion.CerB64;
            //        this.txtRazonSocial.Text = informacion.UserName;
            //        return true;
            //    }
            //}
           // MessageBox.Show(string.Concat("Ocurrió un error al leer el certificado.\r\nCertificado no válido. ", informacion.Message), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e) {
            if (!this.checkBox1.Checked) {
                this.txtContrasena.PasswordChar = '*';
                this.txtConfirmacion.PasswordChar = '*';
            } else {
                this.txtContrasena.PasswordChar = '\0';
                this.txtConfirmacion.PasswordChar = (char)0;
            }
        }

        private void ButtonVerificar_Click(object sender, EventArgs e) {
            if (this.EsInformacionValida()) {
                this.certificate = new Certificate();
                this.certificate.Cargar(this.txtArchivoCertificado.Text);
                if (this.certificate.CodigoDeError == 0) {
                    this.privateKey = new PrivateKey();
                    this.privateKey.Cargar(this.txtLlavePrivada.Text, this.txtContrasena.Text);
                    if (this.privateKey.CodigoDeError == 0) {
                        this.certificate.CargarLlavePrivada(this.privateKey);
                        if (this.certificate.CodigoDeError != 0) {
                            MessageBox.Show(this, this.certificate.MensajeDeError, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        } else {
                            MessageBox.Show(this, "Todos los datos del certificado actual son correctos.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    } else {
                        MessageBox.Show(this, this.privateKey.MensajeDeError, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                } else {
                    MessageBox.Show(this, this.certificate.MensajeDeError, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool EsInformacionValida() {
            bool flag;
            Convert.ToDateTime(this.txtValidoHasta.Text.Substring(0, 10));
            if (this.txtArchivoCertificado.Text == "") {
                MessageBox.Show("No ha seleccionado el archivo del certificado", "Sin certificado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                flag = false;
            } else if (File.Exists(this.txtArchivoCertificado.Text)) {
                if (this.txtCertificadoB64.Text == "") {
                    MessageBox.Show("El certificado se encuentra vacío", "Certificado vacío", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                if (this.txtLlavePrivada.Text != "") {
                    if (!File.Exists(this.txtLlavePrivada.Text)) {
                        MessageBox.Show("El archivo de llave privada no existe", "Archivo inválido", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (this.txtContrasena.Text == "") {
                        MessageBox.Show("La contrseña no puede estar vacía", "Sin contraseña", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (this.txtContrasena.Text != this.txtConfirmacion.Text) {
                        MessageBox.Show("La contraseña y su confirmación son inconsistentes", "Confirmación erronea", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.txtValidoDesde.Text.Substring(0, 10))) < 0) {
                        MessageBox.Show("La vigencia del certificado no ha entrado en vigor", "Vigencia sin vigor", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    } else if (DateTime.Compare(DateTime.Now.Date, Convert.ToDateTime(this.txtValidoHasta.Text.Substring(0, 10))) <= 0) {
                        flag = true;
                    } else {
                        MessageBox.Show("La vigencia del certificado ha expirado", "Vigencia expirada", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        flag = false;
                    }
                } else {
                    MessageBox.Show("No ha seleccionado el archivo de la llave privada", "Sin llave privada", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    flag = false;
                }
            } else {
                MessageBox.Show("El archivo de certificado seleccionado no existe", "Archivo inválido", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                flag = false;
            }
            return flag;
        }

        private void GenerarArchivoPFX() {
            
            string _pathDestino = this.txtArchivoPFX.Text;
            Process paso1 = new Process();
            paso1.StartInfo.FileName = Path.Combine(this._pathOpenSSL, "openssl.exe");
            string[] text = new string[] { "x509 -inform DER -outform PEM -in \"", this.txtArchivoCertificado.Text, "\" -pubkey -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, "Cer.pem\"" };
            string str2 = string.Concat(text);
            paso1.StartInfo.Arguments = str2;
            paso1.StartInfo.WorkingDirectory = _pathOpenSSL;
            paso1.StartInfo.UseShellExecute = false;
            paso1.StartInfo.RedirectStandardOutput = true;
            paso1.Start();
            paso1.WaitForExit();

            Process paso2 = new Process();
            paso2.StartInfo.FileName = Path.Combine(_pathOpenSSL, "openssl.exe");
            text = new string[] { "pkcs8 -inform DER -in \"", this.txtLlavePrivada.Text, "\" -passin pass:\"", this.txtContrasena.Text, "\" -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, "key.pem\"" };
            string str3 = string.Concat(text);
            paso2.StartInfo.Arguments = str3;
            paso2.StartInfo.WorkingDirectory = _pathOpenSSL;
            paso2.StartInfo.UseShellExecute = false;
            paso2.StartInfo.RedirectStandardOutput = true;
            paso2.Start();
            paso2.WaitForExit();
            
            Process paso3 = new Process();
            paso3.StartInfo.FileName = Path.Combine(_pathOpenSSL, "openssl.exe");
            text = new string[] { "pkcs12 -export -inkey \"", _pathDestino.ToString(), "\\", this.RFC.Text, "key.pem\" -in \"", _pathDestino.ToString(), "\\", this.RFC.Text, "Cer.pem\" -out \"", _pathDestino.ToString(), "\\", this.RFC.Text, ".pfx\" -passout pass:\"", this.txtContrasena.Text, "\"" };
            string str4 = string.Concat(text);
            paso3.StartInfo.Arguments = str4;
            paso3.StartInfo.WorkingDirectory = this._pathOpenSSL;
            paso3.StartInfo.UseShellExecute = false;
            paso3.StartInfo.RedirectStandardOutput = true;
            paso3.Start();
            paso3.WaitForExit();
            paso3.Close();
            this.txtArchivoPFX.Text = string.Concat(_pathDestino, "\\", this.RFC.Text, ".pfx");
        }

        private void buttonGeneraPFX_Click(object sender, EventArgs e) {
            //GenerarArchivoPFX();
            //var _uno = new CryptoConvert();
            //string _pathDestino = this.txtArchivoPFX.Text;
            //var _salida = _uno.CerKey2Pfx(Jaeger.Util.Services.FileService.ReadFileB64(txtArchivoCertificado.Text), Util.Services.FileService.ReadFileB64(txtLlavePrivada.Text), this.txtContrasena.Text);
            //Jaeger.Util.Services.FileService.WriteFileByte(_salida, Path.Combine(_pathDestino, this.RFC.Text));
        }

        private void buttonOpenSSL_Click(object sender, EventArgs e) {
            var openFileDialog = new OpenFileDialog() {
                Title = "Buscar archivo del certificado *.exe",
                DefaultExt = ".exe",
                Filter = "Archivo CER (*.exe)|*.exe",
                FilterIndex = 1,
                FileName = "openssl.exe"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                this.label14.Text = openFileDialog.FileName;
            }
        }

        private void buttonPathPFX_Click(object sender, EventArgs e) {
            var openFolrderDialog = new FolderBrowserDialog();
            if (openFolrderDialog.ShowDialog(this) == DialogResult.OK) {
                this.txtArchivoPFX.Text = openFolrderDialog.SelectedPath;
            }
        }
    }
}
