﻿
namespace Jaeger.UI.Forms {
    partial class ConvertirHtmlToPDFForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ProcessButton = new System.Windows.Forms.Button();
            this.FileLogotipoLabel = new System.Windows.Forms.Label();
            this.FileLogotipoText = new System.Windows.Forms.TextBox();
            this.FileLogotipoButton = new System.Windows.Forms.Button();
            this.FileTempleteHtmlLabel = new System.Windows.Forms.Label();
            this.FileTempleteHtmlText = new System.Windows.Forms.TextBox();
            this.FileTempleteButton = new System.Windows.Forms.Button();
            this.FileXmlLabel = new System.Windows.Forms.Label();
            this.FileXmlText = new System.Windows.Forms.TextBox();
            this.FileXmlButton = new System.Windows.Forms.Button();
            this.LogotipoPicture = new System.Windows.Forms.PictureBox();
            this.HeaderPicture = new System.Windows.Forms.PictureBox();
            this.FilePdfLabel = new System.Windows.Forms.Label();
            this.FilePDFText = new System.Windows.Forms.TextBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.HeaderTitleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LogotipoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // ProcessButton
            // 
            this.ProcessButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ProcessButton.Location = new System.Drawing.Point(497, 236);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(90, 23);
            this.ProcessButton.TabIndex = 19;
            this.ProcessButton.Text = "Procesar";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // FileLogotipoLabel
            // 
            this.FileLogotipoLabel.AutoSize = true;
            this.FileLogotipoLabel.Location = new System.Drawing.Point(12, 136);
            this.FileLogotipoLabel.Name = "FileLogotipoLabel";
            this.FileLogotipoLabel.Size = new System.Drawing.Size(48, 13);
            this.FileLogotipoLabel.TabIndex = 18;
            this.FileLogotipoLabel.Text = "Logotipo";
            this.FileLogotipoLabel.Click += new System.EventHandler(this.FileLogotipoLabel_Click);
            // 
            // FileLogotipoText
            // 
            this.FileLogotipoText.Location = new System.Drawing.Point(15, 152);
            this.FileLogotipoText.Name = "FileLogotipoText";
            this.FileLogotipoText.Size = new System.Drawing.Size(348, 20);
            this.FileLogotipoText.TabIndex = 17;
            // 
            // FileLogotipoButton
            // 
            this.FileLogotipoButton.Location = new System.Drawing.Point(369, 150);
            this.FileLogotipoButton.Name = "FileLogotipoButton";
            this.FileLogotipoButton.Size = new System.Drawing.Size(75, 23);
            this.FileLogotipoButton.TabIndex = 16;
            this.FileLogotipoButton.Text = "Seleccionar";
            this.FileLogotipoButton.UseVisualStyleBackColor = true;
            this.FileLogotipoButton.Click += new System.EventHandler(this.FileLogotipoButton_Click);
            // 
            // FileTempleteHtmlLabel
            // 
            this.FileTempleteHtmlLabel.AutoSize = true;
            this.FileTempleteHtmlLabel.Location = new System.Drawing.Point(12, 95);
            this.FileTempleteHtmlLabel.Name = "FileTempleteHtmlLabel";
            this.FileTempleteHtmlLabel.Size = new System.Drawing.Size(76, 13);
            this.FileTempleteHtmlLabel.TabIndex = 15;
            this.FileTempleteHtmlLabel.Text = "Plantilla HTML";
            // 
            // FileTempleteHtmlText
            // 
            this.FileTempleteHtmlText.Location = new System.Drawing.Point(15, 111);
            this.FileTempleteHtmlText.Name = "FileTempleteHtmlText";
            this.FileTempleteHtmlText.Size = new System.Drawing.Size(348, 20);
            this.FileTempleteHtmlText.TabIndex = 14;
            // 
            // FileTempleteButton
            // 
            this.FileTempleteButton.Location = new System.Drawing.Point(369, 109);
            this.FileTempleteButton.Name = "FileTempleteButton";
            this.FileTempleteButton.Size = new System.Drawing.Size(75, 23);
            this.FileTempleteButton.TabIndex = 13;
            this.FileTempleteButton.Text = "Seleccionar";
            this.FileTempleteButton.UseVisualStyleBackColor = true;
            this.FileTempleteButton.Click += new System.EventHandler(this.FileTempleteHTMLButton_Click);
            // 
            // FileXmlLabel
            // 
            this.FileXmlLabel.AutoSize = true;
            this.FileXmlLabel.Location = new System.Drawing.Point(12, 55);
            this.FileXmlLabel.Name = "FileXmlLabel";
            this.FileXmlLabel.Size = new System.Drawing.Size(95, 13);
            this.FileXmlLabel.TabIndex = 12;
            this.FileXmlLabel.Text = "Comprobante XML";
            // 
            // FileXmlText
            // 
            this.FileXmlText.Location = new System.Drawing.Point(15, 71);
            this.FileXmlText.Name = "FileXmlText";
            this.FileXmlText.Size = new System.Drawing.Size(348, 20);
            this.FileXmlText.TabIndex = 11;
            // 
            // FileXmlButton
            // 
            this.FileXmlButton.Location = new System.Drawing.Point(369, 69);
            this.FileXmlButton.Name = "FileXmlButton";
            this.FileXmlButton.Size = new System.Drawing.Size(75, 23);
            this.FileXmlButton.TabIndex = 10;
            this.FileXmlButton.Text = "Seleccionar";
            this.FileXmlButton.UseVisualStyleBackColor = true;
            this.FileXmlButton.Click += new System.EventHandler(this.FileXmlButton_Click);
            // 
            // LogotipoPicture
            // 
            this.LogotipoPicture.Location = new System.Drawing.Point(467, 50);
            this.LogotipoPicture.Name = "LogotipoPicture";
            this.LogotipoPicture.Size = new System.Drawing.Size(120, 124);
            this.LogotipoPicture.TabIndex = 20;
            this.LogotipoPicture.TabStop = false;
            // 
            // HeaderPicture
            // 
            this.HeaderPicture.BackColor = System.Drawing.Color.White;
            this.HeaderPicture.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderPicture.Location = new System.Drawing.Point(0, 0);
            this.HeaderPicture.Name = "HeaderPicture";
            this.HeaderPicture.Size = new System.Drawing.Size(601, 40);
            this.HeaderPicture.TabIndex = 21;
            this.HeaderPicture.TabStop = false;
            // 
            // FilePdfLabel
            // 
            this.FilePdfLabel.AutoSize = true;
            this.FilePdfLabel.Location = new System.Drawing.Point(12, 183);
            this.FilePdfLabel.Name = "FilePdfLabel";
            this.FilePdfLabel.Size = new System.Drawing.Size(88, 13);
            this.FilePdfLabel.TabIndex = 23;
            this.FilePdfLabel.Text = "Archivo de salida";
            // 
            // FilePDFText
            // 
            this.FilePDFText.Location = new System.Drawing.Point(15, 199);
            this.FilePDFText.Name = "FilePDFText";
            this.FilePDFText.Size = new System.Drawing.Size(429, 20);
            this.FilePDFText.TabIndex = 22;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(403, 236);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(90, 23);
            this.CancelButton.TabIndex = 24;
            this.CancelButton.Text = "Cancelar";
            this.CancelButton.UseVisualStyleBackColor = true;
            // 
            // HeaderTitleLabel
            // 
            this.HeaderTitleLabel.AutoSize = true;
            this.HeaderTitleLabel.BackColor = System.Drawing.Color.White;
            this.HeaderTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderTitleLabel.Location = new System.Drawing.Point(12, 12);
            this.HeaderTitleLabel.Name = "HeaderTitleLabel";
            this.HeaderTitleLabel.Size = new System.Drawing.Size(356, 13);
            this.HeaderTitleLabel.TabIndex = 25;
            this.HeaderTitleLabel.Text = "Crear representación impresa de un comprobante fiscal (XML)";
            // 
            // ConvertirHtmlToPDFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 271);
            this.Controls.Add(this.HeaderTitleLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.FilePdfLabel);
            this.Controls.Add(this.FilePDFText);
            this.Controls.Add(this.HeaderPicture);
            this.Controls.Add(this.LogotipoPicture);
            this.Controls.Add(this.ProcessButton);
            this.Controls.Add(this.FileLogotipoLabel);
            this.Controls.Add(this.FileLogotipoText);
            this.Controls.Add(this.FileLogotipoButton);
            this.Controls.Add(this.FileTempleteHtmlLabel);
            this.Controls.Add(this.FileTempleteHtmlText);
            this.Controls.Add(this.FileTempleteButton);
            this.Controls.Add(this.FileXmlLabel);
            this.Controls.Add(this.FileXmlText);
            this.Controls.Add(this.FileXmlButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConvertirHtmlToPDFForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Convertir XML a PDF";
            this.Load += new System.EventHandler(this.ConvertirHtmlToPDFForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LogotipoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.Label FileLogotipoLabel;
        private System.Windows.Forms.TextBox FileLogotipoText;
        private System.Windows.Forms.Button FileLogotipoButton;
        private System.Windows.Forms.Label FileTempleteHtmlLabel;
        private System.Windows.Forms.TextBox FileTempleteHtmlText;
        private System.Windows.Forms.Button FileTempleteButton;
        private System.Windows.Forms.Label FileXmlLabel;
        private System.Windows.Forms.TextBox FileXmlText;
        private System.Windows.Forms.Button FileXmlButton;
        private System.Windows.Forms.PictureBox LogotipoPicture;
        private System.Windows.Forms.PictureBox HeaderPicture;
        private System.Windows.Forms.Label FilePdfLabel;
        private System.Windows.Forms.TextBox FilePDFText;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label HeaderTitleLabel;
    }
}