﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class ClasificaForm : Form {
        public ClasificaForm() {
            InitializeComponent();
        }

        private void ClasificaForm_Load(object sender, EventArgs e) {

        }

        private void button1_Click(object sender, EventArgs e) {
            var _orgien = new FolderBrowserDialog();
            if (_orgien.ShowDialog(this) == DialogResult.OK) {
                this.textBox1.Text = _orgien.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            var _destino = new FolderBrowserDialog();
            if (_destino.ShowDialog(this) == DialogResult.OK) {
                this.textBox2.Text = _destino.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            List<string> archivos = Directory.GetFiles(this.textBox1.Text, "*.xml", SearchOption.AllDirectories).ToList<string>();
            foreach (string item in archivos) {
                DateTime? _emision = null;
                var _cfdi = CFDI.V33.Comprobante.Load(item);
                if (_cfdi != null) {
                    _emision = _cfdi.Fecha;
                }
                else {
                    var _cfdi32 = CFDI.V32.Comprobante.Load(item);
                    if (_cfdi32 != null) { 
                        _emision = _cfdi32.fecha;
                    }
                }

                if (_emision != null) {
                    var _carpeta = Path.Combine(this.textBox2.Text, _emision.Value.Year.ToString());
                    if (Directory.Exists(_carpeta) == false) {
                        Directory.CreateDirectory(_carpeta);
                    }
                    
                        var _destino = Path.Combine(_carpeta, Path.GetFileName(item));
                        File.Copy(item, _destino);
                    
                }
            }
        }
    }
}
