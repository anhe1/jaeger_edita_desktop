﻿namespace Jaeger.UI.Forms {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu_Archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Archivo_Salir = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas_Comprobante = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Herramientas_ComprobanteRetencion = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.m_Certificado = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo,
            this.menu_Herramientas});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu_Archivo
            // 
            this.menu_Archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Archivo_Salir});
            this.menu_Archivo.Name = "menu_Archivo";
            this.menu_Archivo.Size = new System.Drawing.Size(96, 20);
            this.menu_Archivo.Text = "menu_Archivo";
            // 
            // menu_Archivo_Salir
            // 
            this.menu_Archivo_Salir.Name = "menu_Archivo_Salir";
            this.menu_Archivo_Salir.Size = new System.Drawing.Size(96, 22);
            this.menu_Archivo_Salir.Text = "Salir";
            this.menu_Archivo_Salir.Click += new System.EventHandler(this.menu_Archivo_Salir_Click);
            // 
            // menu_Herramientas
            // 
            this.menu_Herramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Herramientas_Comprobante,
            this.menu_Herramientas_ComprobanteRetencion,
            this.m_Certificado});
            this.menu_Herramientas.Name = "menu_Herramientas";
            this.menu_Herramientas.Size = new System.Drawing.Size(90, 20);
            this.menu_Herramientas.Text = "Herramientas";
            // 
            // menu_Herramientas_Comprobante
            // 
            this.menu_Herramientas_Comprobante.Name = "menu_Herramientas_Comprobante";
            this.menu_Herramientas_Comprobante.Size = new System.Drawing.Size(193, 22);
            this.menu_Herramientas_Comprobante.Text = "menu_CFDI";
            this.menu_Herramientas_Comprobante.Click += new System.EventHandler(this.menu_Herramientas_Comprobante_Click);
            // 
            // menu_Herramientas_ComprobanteRetencion
            // 
            this.menu_Herramientas_ComprobanteRetencion.Name = "menu_Herramientas_ComprobanteRetencion";
            this.menu_Herramientas_ComprobanteRetencion.Size = new System.Drawing.Size(193, 22);
            this.menu_Herramientas_ComprobanteRetencion.Text = "menu_CFDI_Retencion";
            this.menu_Herramientas_ComprobanteRetencion.Click += new System.EventHandler(this.menu_Herramientas_ComprobanteRetencion_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // m_Certificado
            // 
            this.m_Certificado.Name = "m_Certificado";
            this.m_Certificado.Size = new System.Drawing.Size(193, 22);
            this.m_Certificado.Text = "Certificado";
            this.m_Certificado.Click += new System.EventHandler(this.m_Certificado_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Testing";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo;
        private System.Windows.Forms.ToolStripMenuItem menu_Archivo_Salir;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas_Comprobante;
        private System.Windows.Forms.ToolStripMenuItem menu_Herramientas_ComprobanteRetencion;
        private System.Windows.Forms.ToolStripMenuItem m_Certificado;
    }
}