﻿using System;
using System.Windows.Forms;
using Jaeger.CFDRetencion.V10;

namespace Jaeger.UI.Forms {
    public partial class Comprobante1FiscalRetencionForm : Form {
        private Retenciones comprobante;
        public Comprobante1FiscalRetencionForm() {
            InitializeComponent();
        }

        private void Comprobante1FiscalRetencionForm_Load(object sender, EventArgs e) {
        }

        private void ToolBarButtonAbrir_Click(object sender, EventArgs e) {
            var openFile = new OpenFileDialog { Filter = "*.xml|*.XML" };
            if (openFile.ShowDialog(this) == DialogResult.OK) {
                this.comprobante = Retenciones.Load(openFile.FileName);
            }
            //this.CreateBinding();
            this.retencionesBindingSource.DataSource = this.comprobante;
            this.retencionesEmisorBindingSource.DataSource = this.comprobante.Emisor;
            this.treeViewControl1.Crear(this.comprobante.OriginalXmlString);
        }

        private void CreateBinding() {
            this.textBoxEmisorNombre.DataBindings.Clear();
            this.textBoxEmisorNombre.DataBindings.Add("Text", this.comprobante.Emisor, "NomDenRazSocE", true, DataSourceUpdateMode.OnPropertyChanged);
            this.textBoxEmisorRFC.DataBindings.Add("Text", this.comprobante.Emisor, "RFCEmisor", true, DataSourceUpdateMode.OnPropertyChanged);
            this.textBoxEmisorRFC.DataBindings.Add("Text", this.comprobante.Emisor, "CURPE", true);
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            if (this.comprobante != null) {
                var saveFile = new SaveFileDialog { AddExtension = true, DefaultExt = "xml", Filter = "*.xml|*.XML" };
                if (this.comprobante.Complemento != null) {
                    if (this.comprobante.Complemento.TimbreFiscalDigital != null){
                        saveFile.FileName = this.comprobante.Complemento.TimbreFiscalDigital.UUID;
                    }
                }
                if (saveFile.ShowDialog(this) == DialogResult.OK) {
                    this.comprobante.Save(saveFile.FileName);
                }
            }
        }
    }
}
