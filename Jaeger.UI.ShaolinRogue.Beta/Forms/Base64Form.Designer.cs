﻿namespace Jaeger.UI.Forms {
    partial class Base64Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonCER = new System.Windows.Forms.Button();
            this.txtArchivoCertificado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCER
            // 
            this.buttonCER.Location = new System.Drawing.Point(354, 32);
            this.buttonCER.Name = "buttonCER";
            this.buttonCER.Size = new System.Drawing.Size(29, 23);
            this.buttonCER.TabIndex = 6;
            this.buttonCER.Text = "...";
            this.buttonCER.UseVisualStyleBackColor = true;
            this.buttonCER.Click += new System.EventHandler(this.buttonCER_Click);
            // 
            // txtArchivoCertificado
            // 
            this.txtArchivoCertificado.Location = new System.Drawing.Point(12, 34);
            this.txtArchivoCertificado.Name = "txtArchivoCertificado";
            this.txtArchivoCertificado.Size = new System.Drawing.Size(336, 20);
            this.txtArchivoCertificado.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Certificado (.cer):";
            // 
            // Base64Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonCER);
            this.Controls.Add(this.txtArchivoCertificado);
            this.Controls.Add(this.label1);
            this.Name = "Base64Form";
            this.Text = "Base64Form";
            this.Load += new System.EventHandler(this.Base64Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCER;
        private System.Windows.Forms.TextBox txtArchivoCertificado;
        private System.Windows.Forms.Label label1;
    }
}