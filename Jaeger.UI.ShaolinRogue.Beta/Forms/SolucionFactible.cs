﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class SolucionFactible : Form {
        private readonly string usuarioTesting = "testing@solucionfactible.com";
        private readonly string passwordTesting = "timbrado.SF.16672";
        private readonly string testEndpoint = "TimbradoEndpoint_TESTING";
        public SolucionFactible() {
            InitializeComponent();
        }

        private void SolucionFactible_Load(object sender, EventArgs e) {
            var nuevo = new SolucionFactibleService();
            nuevo.Cancelar1("Impresores21.",
                File.ReadAllBytes(@"C:\Users\anhed\Desktop\CSD_IPR981125PN9_20210125202659\00001000000506252062.cer"), 
                File.ReadAllBytes(@"C:\Users\anhed\Desktop\CSD_IPR981125PN9_20210125202659\CSD_CERTIFICADO_DIGITAL_IPR981125PN9_20210125_195307.key"), 
                new string[] { "0348E567-AA12-435B-B396-EA515C0C50B6|02|" });
        }
    }
}
