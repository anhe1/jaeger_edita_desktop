﻿namespace Jaeger.UI.Forms {
    partial class CertificadoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtArchivoCertificado = new System.Windows.Forms.TextBox();
            this.txtLlavePrivada = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtContrasena = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonVerificar = new System.Windows.Forms.Button();
            this.txtConfirmacion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.buttonKEY = new System.Windows.Forms.Button();
            this.buttonCER = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.TipoCertificado = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtValidoHasta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtValidoDesde = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumeroDeSerieCertificado = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCertificadoB64 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonOpenSSL = new System.Windows.Forms.Button();
            this.buttonGeneraPFX = new System.Windows.Forms.Button();
            this.txtArchivoPFX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonPathPFX = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Certificado (.cer):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave privada (.key):";
            // 
            // txtArchivoCertificado
            // 
            this.txtArchivoCertificado.Location = new System.Drawing.Point(6, 45);
            this.txtArchivoCertificado.Name = "txtArchivoCertificado";
            this.txtArchivoCertificado.Size = new System.Drawing.Size(336, 20);
            this.txtArchivoCertificado.TabIndex = 0;
            // 
            // txtLlavePrivada
            // 
            this.txtLlavePrivada.Location = new System.Drawing.Point(6, 84);
            this.txtLlavePrivada.Name = "txtLlavePrivada";
            this.txtLlavePrivada.Size = new System.Drawing.Size(336, 20);
            this.txtLlavePrivada.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Contraseña de clave privada:*";
            // 
            // txtContrasena
            // 
            this.txtContrasena.Location = new System.Drawing.Point(6, 123);
            this.txtContrasena.Name = "txtContrasena";
            this.txtContrasena.PasswordChar = '*';
            this.txtContrasena.Size = new System.Drawing.Size(220, 20);
            this.txtContrasena.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonVerificar);
            this.groupBox1.Controls.Add(this.txtConfirmacion);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.buttonKEY);
            this.groupBox1.Controls.Add(this.buttonCER);
            this.groupBox1.Controls.Add(this.txtArchivoCertificado);
            this.groupBox1.Controls.Add(this.txtContrasena);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtLlavePrivada);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(0, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 222);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del certificado";
            // 
            // buttonVerificar
            // 
            this.buttonVerificar.Location = new System.Drawing.Point(291, 190);
            this.buttonVerificar.Name = "buttonVerificar";
            this.buttonVerificar.Size = new System.Drawing.Size(100, 23);
            this.buttonVerificar.TabIndex = 7;
            this.buttonVerificar.Text = "Verificar";
            this.buttonVerificar.UseVisualStyleBackColor = true;
            this.buttonVerificar.Click += new System.EventHandler(this.ButtonVerificar_Click);
            // 
            // txtConfirmacion
            // 
            this.txtConfirmacion.Location = new System.Drawing.Point(6, 162);
            this.txtConfirmacion.Name = "txtConfirmacion";
            this.txtConfirmacion.PasswordChar = '*';
            this.txtConfirmacion.Size = new System.Drawing.Size(220, 20);
            this.txtConfirmacion.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(228, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Confirmación de contraseña de clave privada:*";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(232, 125);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(94, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "ver caracteres";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged);
            // 
            // buttonKEY
            // 
            this.buttonKEY.Location = new System.Drawing.Point(348, 82);
            this.buttonKEY.Name = "buttonKEY";
            this.buttonKEY.Size = new System.Drawing.Size(29, 23);
            this.buttonKEY.TabIndex = 3;
            this.buttonKEY.Text = "...";
            this.buttonKEY.UseVisualStyleBackColor = true;
            this.buttonKEY.Click += new System.EventHandler(this.ButtonKEY_Click);
            // 
            // buttonCER
            // 
            this.buttonCER.Location = new System.Drawing.Point(348, 43);
            this.buttonCER.Name = "buttonCER";
            this.buttonCER.Size = new System.Drawing.Size(29, 23);
            this.buttonCER.TabIndex = 1;
            this.buttonCER.Text = "...";
            this.buttonCER.UseVisualStyleBackColor = true;
            this.buttonCER.Click += new System.EventHandler(this.ButtonCER_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtRazonSocial);
            this.groupBox3.Controls.Add(this.TipoCertificado);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.RFC);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtValidoHasta);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtValidoDesde);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtNumeroDeSerieCertificado);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtCertificadoB64);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(404, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 305);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Información del certificado";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.BackColor = System.Drawing.SystemColors.Window;
            this.txtRazonSocial.Location = new System.Drawing.Point(6, 32);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.ReadOnly = true;
            this.txtRazonSocial.Size = new System.Drawing.Size(295, 20);
            this.txtRazonSocial.TabIndex = 5;
            // 
            // TipoCertificado
            // 
            this.TipoCertificado.BackColor = System.Drawing.SystemColors.Window;
            this.TipoCertificado.Location = new System.Drawing.Point(6, 156);
            this.TipoCertificado.Name = "TipoCertificado";
            this.TipoCertificado.ReadOnly = true;
            this.TipoCertificado.Size = new System.Drawing.Size(145, 20);
            this.TipoCertificado.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Razón Social";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Tipo de certificado";
            // 
            // RFC
            // 
            this.RFC.BackColor = System.Drawing.SystemColors.Window;
            this.RFC.Location = new System.Drawing.Point(156, 78);
            this.RFC.Name = "RFC";
            this.RFC.ReadOnly = true;
            this.RFC.Size = new System.Drawing.Size(145, 20);
            this.RFC.TabIndex = 2;
            this.RFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(156, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "RFC";
            // 
            // txtValidoHasta
            // 
            this.txtValidoHasta.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoHasta.Location = new System.Drawing.Point(156, 117);
            this.txtValidoHasta.Name = "txtValidoHasta";
            this.txtValidoHasta.ReadOnly = true;
            this.txtValidoHasta.Size = new System.Drawing.Size(145, 20);
            this.txtValidoHasta.TabIndex = 4;
            this.txtValidoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(156, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Válido hasta";
            // 
            // txtValidoDesde
            // 
            this.txtValidoDesde.BackColor = System.Drawing.SystemColors.Window;
            this.txtValidoDesde.Location = new System.Drawing.Point(6, 117);
            this.txtValidoDesde.Name = "txtValidoDesde";
            this.txtValidoDesde.ReadOnly = true;
            this.txtValidoDesde.Size = new System.Drawing.Size(145, 20);
            this.txtValidoDesde.TabIndex = 3;
            this.txtValidoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Válido desde:";
            // 
            // txtNumeroDeSerieCertificado
            // 
            this.txtNumeroDeSerieCertificado.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumeroDeSerieCertificado.Location = new System.Drawing.Point(6, 78);
            this.txtNumeroDeSerieCertificado.Name = "txtNumeroDeSerieCertificado";
            this.txtNumeroDeSerieCertificado.ReadOnly = true;
            this.txtNumeroDeSerieCertificado.Size = new System.Drawing.Size(145, 20);
            this.txtNumeroDeSerieCertificado.TabIndex = 1;
            this.txtNumeroDeSerieCertificado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Número de serie";
            // 
            // txtCertificadoB64
            // 
            this.txtCertificadoB64.BackColor = System.Drawing.SystemColors.Window;
            this.txtCertificadoB64.Location = new System.Drawing.Point(6, 195);
            this.txtCertificadoB64.Multiline = true;
            this.txtCertificadoB64.Name = "txtCertificadoB64";
            this.txtCertificadoB64.ReadOnly = true;
            this.txtCertificadoB64.Size = new System.Drawing.Size(295, 104);
            this.txtCertificadoB64.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Certificado Base 64";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonPathPFX);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.buttonOpenSSL);
            this.groupBox2.Controls.Add(this.buttonGeneraPFX);
            this.groupBox2.Controls.Add(this.txtArchivoPFX);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(0, 272);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 89);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opciones";
            // 
            // label14
            // 
            this.label14.AutoEllipsis = true;
            this.label14.Location = new System.Drawing.Point(6, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(130, 18);
            this.label14.TabIndex = 6;
            this.label14.Text = "Archivo PFX (.pfx)";
            // 
            // buttonOpenSSL
            // 
            this.buttonOpenSSL.Location = new System.Drawing.Point(142, 55);
            this.buttonOpenSSL.Name = "buttonOpenSSL";
            this.buttonOpenSSL.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenSSL.TabIndex = 5;
            this.buttonOpenSSL.Text = "OpenSSL";
            this.buttonOpenSSL.UseVisualStyleBackColor = true;
            this.buttonOpenSSL.Click += new System.EventHandler(this.buttonOpenSSL_Click);
            // 
            // buttonGeneraPFX
            // 
            this.buttonGeneraPFX.Location = new System.Drawing.Point(223, 55);
            this.buttonGeneraPFX.Name = "buttonGeneraPFX";
            this.buttonGeneraPFX.Size = new System.Drawing.Size(154, 23);
            this.buttonGeneraPFX.TabIndex = 1;
            this.buttonGeneraPFX.Text = "Generar archivo PFX";
            this.buttonGeneraPFX.UseVisualStyleBackColor = true;
            this.buttonGeneraPFX.Click += new System.EventHandler(this.buttonGeneraPFX_Click);
            // 
            // txtArchivoPFX
            // 
            this.txtArchivoPFX.Location = new System.Drawing.Point(6, 32);
            this.txtArchivoPFX.Name = "txtArchivoPFX";
            this.txtArchivoPFX.Size = new System.Drawing.Size(336, 20);
            this.txtArchivoPFX.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Archivo PFX (.pfx)";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.White;
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(721, 50);
            this.header.TabIndex = 5;
            this.header.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Validación de Certificados";
            // 
            // buttonPathPFX
            // 
            this.buttonPathPFX.Location = new System.Drawing.Point(348, 32);
            this.buttonPathPFX.Name = "buttonPathPFX";
            this.buttonPathPFX.Size = new System.Drawing.Size(29, 20);
            this.buttonPathPFX.TabIndex = 7;
            this.buttonPathPFX.Text = "...";
            this.buttonPathPFX.UseVisualStyleBackColor = true;
            this.buttonPathPFX.Click += new System.EventHandler(this.buttonPathPFX_Click);
            // 
            // CertificadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 370);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CertificadoForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Certificado";
            this.Load += new System.EventHandler(this.CertificadoForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.header)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtArchivoCertificado;
        private System.Windows.Forms.TextBox txtLlavePrivada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtContrasena;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button buttonKEY;
        private System.Windows.Forms.Button buttonCER;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonGeneraPFX;
        private System.Windows.Forms.TextBox txtArchivoPFX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox header;
        private System.Windows.Forms.TextBox txtConfirmacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCertificadoB64;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNumeroDeSerieCertificado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValidoDesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtValidoHasta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox RFC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TipoCertificado;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonVerificar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button buttonOpenSSL;
        private System.Windows.Forms.Button buttonPathPFX;
    }
}