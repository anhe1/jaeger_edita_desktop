﻿using System;
using System.Windows.Forms;

namespace Jaeger.UI.Forms {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            
        }

        private void menu_Archivo_Salir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void menu_Herramientas_ComprobanteRetencion_Click(object sender, EventArgs e) {
            this.Form_Active(typeof(Base64Form));
        }

        private void menu_Herramientas_Comprobante_Click(object sender, EventArgs e) {
            this.Form_Active(typeof(SolucionFactible));
        }

        private void Form_Active(Type type) {
            var _certificado = new ConvertirHtmlToPDFForm();
            _certificado.ShowDialog(this);
        }

        private void m_Certificado_Click(object sender, EventArgs e) {
            var _certificado = new ConvertirHtmlToPDFForm();
            _certificado.ShowDialog(this);
        }
    }
}
