﻿namespace Jaeger.UI {
    public interface IProveedorCertificacion {
        string User { get; set; }
        string Password { get; set; }
        int Status { get; set; }
        string Mensaje { get; set; }
        bool Produccion { get; set; }

        string PathTemporal { get; set; }

        string Timbrar(string cfdi);
    }
}
