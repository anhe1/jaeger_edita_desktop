﻿using System;
using System.ServiceModel;
using System.Text;

namespace Jaeger.UI {
    public abstract class CertificacionService {
        private string _user;
        private string _password;
        private int _status;
        private string _mensaje;
        private bool _testing;
        private string pathTemporal;

        public string User {
            get { return _user; }
            set { _user = value; }
        }

        public string Password {
            get { return _password; }
            set { _password = value; }
        }

        public int Status {
            get { return _status; }
            set { _status = value; }
        }

        public string Mensaje {
            get { return _mensaje; }
            set { _mensaje = value; }
        }

        public bool Produccion {
            get { return _testing; }
            set { _testing = value; }
        }

        public string PathTemporal {
            get { return pathTemporal; }
            set { pathTemporal = value; }
        }

        public CertificacionService() {
            this.Produccion = true;
        }

        public CertificacionService(bool testing) {
            this.Produccion = testing;
        }

        protected virtual BasicHttpBinding GetBinding(string url) {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            var _responseBinding = new BasicHttpBinding {
                CloseTimeout = new TimeSpan(0, 1, 0),
                OpenTimeout = new TimeSpan(0, 1, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                SendTimeout = new TimeSpan(0, 1, 0),
                AllowCookies = false,
                BypassProxyOnLocal = false,
                HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                MaxBufferSize = 65536,
                MaxBufferPoolSize = 524288,
                MaxReceivedMessageSize = 65536,
                MessageEncoding = WSMessageEncoding.Text,
                TextEncoding = Encoding.UTF8,
                TransferMode = TransferMode.Buffered,
                UseDefaultWebProxy = true
            };
            _responseBinding.Security.Mode = url.StartsWith("https") ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None;
            return _responseBinding;
        }
    }
}
