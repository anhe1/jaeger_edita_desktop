﻿using System;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Jaeger.Certifica.Helpers;
using Jaeger.SAT.CFDI.Cancel;

namespace Jaeger.UI {
    public partial class SolucionFactibleService : CertificacionService, IProveedorCertificacion {
        
        private readonly string usuarioTesting = "testing@solucionfactible.com";
        private readonly string passwordTesting = "timbrado.SF.16672";
        
        private string urlTimbre = "http://testing.solucionfactible.com/ws/services/Timbrado.TimbradoHttpsSoap11Endpoint/";
        
        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="produccion">true = modo de prueba</param>
        public SolucionFactibleService(bool produccion = true) {
            this.Produccion = produccion;
            this.Create();
        }

        private void Create() {
            if (this.Produccion) {
                this.User = this.usuarioTesting;
                this.Password = this.passwordTesting;
                this.urlTimbre = "http://testing.solucionfactible.com/ws/services/Timbrado.TimbradoHttpsSoap11Endpoint/";
            }
        }

        public virtual string Timbrar(string cfdi) {
            /// Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            Certifica.com.SF.Timbre.Testing.TimbradoPortType portCliente = new Certifica.com.SF.Timbre.Testing.TimbradoPortTypeClient(this.GetBinding(this.urlTimbre), new EndpointAddress(this.urlTimbre));

            try {
                byte[] _bytes = Encoding.UTF8.GetBytes(@"");
                var _timbrarRequest = new Certifica.com.SF.Timbre.Testing.timbrarRequest {
                    cfdi = _bytes,
                    usuario = this.User,
                    password = this.Password
                };

                var _response = portCliente.timbrar(_timbrarRequest);

                if (_response.@return.status == 200) {
                    var resultado = _response.@return;
                    if (resultado.resultados != null) {
                        if (resultado.resultados[0].cfdiTimbrado != null) {
                            var _xmlResult = new XmlDocument();
                            _xmlResult.LoadXml(Encoding.UTF8.GetString(resultado.resultados[0].cfdiTimbrado));
                            _xmlResult.Save(System.IO.Path.Combine(this.PathTemporal, string.Concat(resultado.resultados[0].uuid, "_temporal.xml")));
                        } else {
                            this.Status = _response.@return.status;
                            this.Mensaje = _response.@return.mensaje;
                        }
                    }
                } else {
                    this.Status = _response.@return.status;
                    this.Mensaje = _response.@return.mensaje;
                }
            } catch (Exception ex) {
                this.Status = -1;
                this.Mensaje = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return string.Empty;
        }

        public virtual void Cancelar(string contrasenaCSD, byte[] derCertCSD, byte[] derKeyCSD, string[] uuids) {
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            //El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado
            System.Net.ServicePointManager.Expect100Continue = false;
            Certifica.com.SF.Timbre.Testing.TimbradoPortType portCliente = new Certifica.com.SF.Timbre.Testing.TimbradoPortTypeClient(this.GetBinding(this.urlTimbre), new EndpointAddress(this.urlTimbre));
            var _cancelarRequest = new Certifica.com.SF.Timbre.Testing.cancelarRequest {
                contrasenaCSD = contrasenaCSD,
                derCertCSD = derCertCSD,
                derKeyCSD = derKeyCSD,
                password = this.Password,
                usuario = this.User,
                uuids = uuids
            };

            try {
                var _response = portCliente.cancelar(_cancelarRequest);
                if (_response != null) {
                    if (_response.@return.status == 200) {
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }

        public virtual CancelaCFDResponse Cancelar1(string contrasenaCSD, byte[] derCertCSD, byte[] derKeyCSD, string[] uuids) {
            //Si recibe error 417 deberá descomentar la linea a continuación
            System.Net.ServicePointManager.Expect100Continue = false;
            //El paquete o namespace en el que se encuentran las clases será el que se define al agregar la referencia al WebService,
            //en este ejemplo es: com.sf.ws.Timbrado

            Certifica.com.SF.Cancelacion.Produccion.CancelacionPortTypeClient objCancelacion = null;
            CancelaCFDResponse objAcuse = new CancelaCFDResponse();
            objCancelacion = new Certifica.com.SF.Cancelacion.Produccion.CancelacionPortTypeClient(this.GetBinding("https://testing.solucionfactible.com/ws/services/Cancelacion.CancelacionHttpsSoap11Endpoint/"), new EndpointAddress("https://testing.solucionfactible.com/ws/services/Cancelacion.CancelacionHttpsSoap11Endpoint/"));

            try {
                Certifica.com.SF.Cancelacion.Produccion.KeyValue[] objProperties = new Certifica.com.SF.Cancelacion.Produccion.KeyValue[0];
                Certifica.com.SF.Cancelacion.Produccion.StatusCancelacionResponse objResponse = new Certifica.com.SF.Cancelacion.Produccion.StatusCancelacionResponse();
                Envelope objEnvelope = new Envelope();
                string stringXml;
                

                objResponse = objCancelacion.cancelar(this.User,
                                                      this.Password,
                                                      "IPR981125PN9",
                                                      uuids,
                                                      derCertCSD,
                                                      derKeyCSD,
                                                      contrasenaCSD,
                                                      objProperties);
                if (objResponse.status == 200) {
                    stringXml = Encoding.UTF8.GetString(objResponse.acuseSat);
                    FileExtensions.WriteFileByte(objResponse.acuseSat, RouteManager.JaegerPath(Certifica.ValueObjects.PathsEnum.Accuse, string.Concat("Acuse-", "uuid", ".xml")));
                    objEnvelope = HelperXmlSerializer.XmlDeserializarStringXml<Envelope>(stringXml);
                    objAcuse = objEnvelope.Body.CancelaCFDResponse;
                    return objAcuse;
                } else if (objResponse.status == 201) {
                    this.Status = 201;
                    this.Mensaje = objResponse.mensaje;
                } else if (objResponse.status == 211) {
                    // La cancelación está en proceso
                    this.Status = 211;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                } else if (objResponse.status == 601) {
                    this.Status = 601;
                    this.Mensaje = objResponse.mensaje;
                    return null;
                } else {
                    if (objResponse != null) {
                        this.Status = objResponse.status;
                        this.Mensaje = objResponse.mensaje;
                        FileExtensions.WriteFileText(RouteManager.JaegerPath(Certifica.ValueObjects.PathsEnum.Accuse, string.Concat("Acuse-error-", "uuid", ".xml")), objResponse.ToString());
                    }
                    return null;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            this.Status = 201;
            this.Mensaje = "Este procedimiento no se encuentra en modo productivo";
            return null;
        }
    }
}
