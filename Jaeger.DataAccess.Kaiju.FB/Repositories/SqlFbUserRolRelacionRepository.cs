﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Kaiju.Entities;

namespace Jaeger.DataAccess.FB.Kaiju.Repositories {
    /// <summary>
    /// clase para relaciones de usuario y rol
    /// </summary>
    public class SqlFbUserRolRelacionRepository : Abstractions.RepositoryMaster<UIMenuRolRelacionModel>, Domain.Kaiju.Contracts.ISqlUserRolRelacionRepository {
        public SqlFbUserRolRelacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM RLUSRL WHERE RLUSRL_ID = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(UIMenuRolRelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RLUSRL (RLUSRL_ID, RLUSRL_A, RLUSRL_USR_ID, RLUSRL_RLSUSR_ID) VALUES (@RLUSRL_ID,@RLUSRL_A,@RLUSRL_USR_ID,@RLUSRL_RLSUSR_ID) RETURNING RLUSRL_ID"
            };
            sqlCommand.Parameters.AddWithValue("@RLUSRL_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_USR_ID", item.IdUser);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_RLSUSR_ID", item.IdUserRol);
            return this.ExecuteScalar(sqlCommand);
        }

        public int Update(UIMenuRolRelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RLUSRL SET RLUSRL_A=@RLUSRL_A, RLUSRL_USR_ID=@RLUSRL_USR_ID, RLUSRL_RLSUSR_ID=@RLUSRL_RLSUSR_ID WHERE RLUSRL_ID=@RLUSRL_ID"
            };
            sqlCommand.Parameters.AddWithValue("@RLUSRL_ID", item.IdRelacion);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_USR_ID", item.IdUser);
            sqlCommand.Parameters.AddWithValue("@RLUSRL_RLSUSR_ID", item.IdUserRol);
            return this.ExecuteScalar(sqlCommand);
        }

        public UIMenuRolRelacionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 * FROM RLUSRL WHERE RLUSRL_ID = @index;"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<UIMenuRolRelacionModel> GetList() {
            return this.GetList<UIMenuRolRelacionModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM RLUSRL @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public UIMenuRolRelacionModel Save(UIMenuRolRelacionModel item) {
            if (item.IdRelacion == 0) {
                item.IdRelacion = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}
