﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Kaiju.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Kaiju.Repositories {
    /// <summary>
    /// clase para perfiles de usuario
    /// </summary>
    public class SqlFbUIProfileRepository : Abstractions.RepositoryMaster<UIMenuProfileModel>, ISqlUIProfileRepository {
        protected ISqlUserRolRepository userRolRepository;
        protected ISqlUserRolRelacionRepository relacionRepository;

        public SqlFbUIProfileRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.userRolRepository = new SqlFbUserRolRepository(configuracion, user);
            this.relacionRepository = new SqlFbUserRolRelacionRepository(configuracion, user);
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE UIPERFIL SET UIPERFIL_A=0, UIPERFIL_FM=@UIPERFIL_FM, UIPERFIL_USR_M=@UIPERFIL_USR_M WHERE UIPERFIL_ID=@UIPERFIL_ID"
            };

            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ID", index);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_FM", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_USR_M", this.User);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(UIMenuProfileModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO UIPERFIL 
                                          ( UIPERFIL_ID, UIPERFIL_A, UIPERFIL_UIMENU_ID, UIPERFIL_RLSUSR_ID, UIPERFIL_KEY, UIPERFIL_ACTION, UIPERFIL_FN, UIPERFIL_USR_N) 
                                    VALUES(@UIPERFIL_ID,@UIPERFIL_A,@UIPERFIL_UIMENU_ID,@UIPERFIL_RLSUSR_ID,@UIPERFIL_KEY,@UIPERFIL_ACTION,@UIPERFIL_FN,@UIPERFIL_USR_N) RETURNING UIPERFIL_ID"
            };
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_UIMENU_ID", item.IdPerfilMenu);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_RLSUSR_ID", item.IdRol);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_KEY", item.Key);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ACTION", item.Action2);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_FN", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_USR_N", this.User);
            item.IdPerfil = this.ExecuteScalar(sqlCommand);
            if (item.IdPerfil > 0)
                return item.IdPerfil;
            return 0;
        }

        public int Update(UIMenuProfileModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE UIPERFIL SET UIPERFIL_A=@UIPERFIL_A, UIPERFIL_UIMENU_ID=@UIPERFIL_UIMENU_ID, UIPERFIL_RLSUSR_ID=@UIPERFIL_RLSUSR_ID, UIPERFIL_KEY=@UIPERFIL_KEY,UIPERFIL_ACTION=@UIPERFIL_ACTION, UIPERFIL_FM=@UIPERFIL_FM, UIPERFIL_USR_M=@UIPERFIL_USR_M WHERE UIPERFIL_ID=@UIPERFIL_ID"
            };

            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ID", item.IdPerfil);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_UIMENU_ID", item.IdPerfilMenu);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_RLSUSR_ID", item.IdRol);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_KEY", item.Key);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ACTION", item.Action2);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_FM", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_USR_M", this.User);
            return this.ExecuteTransaction(sqlCommand);
        }

        public UIMenuProfileModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 * FROM UIPERFIL WHERE UIPERFIL_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<UIMenuProfileModel> GetList() {
            return this.GetList<UIMenuProfileModel>(new List<IConditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT UIPERFIL.* FROM UIPERFIL @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
        #endregion

        public int Salveable(UIMenuProfileModel menu) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO UIPERFIL (UIPERFIL_A, UIPERFIL_RLSUSR_ID, UIPERFIL_ACTION, UIPERFIL_KEY, UIPERFIL_FN, UIPERFIL_USR_N) 
                                                       VALUES (@UIPERFIL_A,@UIPERFIL_RLSUSR_ID,@UIPERFIL_ACTION,@UIPERFIL_KEY,@UIPERFIL_FN,@UIPERFIL_USR_N) 
                                                     MATCHING (UIPERFIL_RLSUSR_ID, UIPERFIL_KEY) RETURNING UIPERFIL_A, UIPERFIL_RLSUSR_ID, UIPERFIL_KEY"
            };

            sqlCommand.Parameters.AddWithValue("@UIPERFIL_A", menu.Activo);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_RLSUSR_ID", menu.IdPerfil);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_ACTION", menu.Action2);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_KEY", menu.Key);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_FN", menu.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@UIPERFIL_USR_N", menu.Creo);

            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<UserRolDetailModel> GetProfile(bool onlyActive) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM RLSUSR"
            };
            var result = this.GetMapper<UserRolDetailModel>(sqlCommand).ToList();
            var _perfiles = this.GetList<UIMenuProfileDetailModel>(new List<IConditional>());
            var _relaciones = this.relacionRepository.GetList<UIMenuRolRelacionModel>(new List<IConditional>());

            if (_perfiles != null) {
                for (int i = 0; i < result.Count(); i++) {
                    result[i].Perfil = new System.ComponentModel.BindingList<UIMenuProfileDetailModel>(_perfiles.Where(it => it.IdRol == result[i].IdUserRol).ToList());
                    result[i].Relacion = new System.ComponentModel.BindingList<UIMenuRolRelacionModel>(_relaciones.Where(it => it.IdUserRol == result[i].IdUserRol).ToList());
                }
            }

            return result;
        }

        public UIMenuProfileDetailModel Save(UIMenuProfileDetailModel model2) {
            if (model2.IdPerfil == 0 && model2.IdPerfilMenu > 0) {
                model2.FechaNuevo = DateTime.Now;
                model2.Creo = this.User;
                model2.IdPerfil = this.Insert(model2);
            } else if (model2.IdPerfil > 0 && model2.IdPerfilMenu > 0) {
                model2.FechaModifica = DateTime.Now;
                model2.Modifica = this.User;
                this.Update(model2);
            }
            return model2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUser">indice del usuario</param>
        public IEnumerable<UIMenuProfileDetailModel> GetBy(int idUser) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT RLSUSR.*, RLUSRL.*, UIPERFIL.* FROM RLUSRL 
LEFT JOIN RLSUSR ON RLUSRL.RLUSRL_RLSUSR_ID = RLSUSR.RLSUSR_ID
LEFT JOIN UIPERFIL ON RLSUSR.RLSUSR_ID = UIPERFIL.UIPERFIL_RLSUSR_ID AND UIPERFIL_A > 0
WHERE RLUSRL.RLUSRL_USR_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", idUser);
            return this.GetMapper<UIMenuProfileDetailModel>(sqlCommand);
        }
    }
}
