﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Kaiju.Repositories {
    /// <summary>
    /// repositorio rol de usuario (RLSUSR)
    /// </summary>
    public class SqlFbUserRolRepository : Abstractions.RepositoryMaster<UserRolModel>, Domain.Kaiju.Contracts.ISqlUserRolRepository {
        public SqlFbUserRolRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RLSUSR SET RLSUSR_A = 0, RLSUSR_FM = @RLSUSR_FM, RLSUSR_USR_M = @RLSUSR_USR_M WHERE RLSUSR_ID = @RLSUSR_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@RLSUSR_ID", index);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_FM", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_USR_M", this.User);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(UserRolModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO RLSUSR (RLSUSR_ID, RLSUSR_A, RLSUSR_M, RLSUSR_CLV, RLSUSR_NMBR, RLSUSR_FN, RLSUSR_USR_N) VALUES (@RLSUSR_ID,@RLSUSR_A,@RLSUSR_M,@RLSUSR_CLV,@RLSUSR_NMBR,@RLSUSR_FN,@RLSUSR_USR_N) returning RLSUSR_ID"
            };
            sqlCommand.Parameters.AddWithValue("@RLSUSR_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_M", item.IdMaster);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_NMBR", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_USR_N", item.Creo);
            item.IdUserRol = this.ExecuteScalar(sqlCommand);
            if (item.IdUserRol > 0)
                return item.IdUserRol;
            return 0;
        }

        public int Update(UserRolModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE RLSUSR SET RLSUSR_A = @RLSUSR_A, RLSUSR_M = @RLSUSR_M, RLSUSR_CLV = @RLSUSR_CLV, RLSUSR_NMBR = @RLSUSR_NMBR, RLSUSR_FM = @RLSUSR_FM, RLSUSR_USR_M = @RLSUSR_USR_M WHERE RLSUSR_ID = @RLSUSR_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@RLSUSR_ID", item.IdUserRol);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_M", item.IdMaster);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_NMBR", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@RLSUSR_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public UserRolModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 RLSUSR.* FROM RLSUSR WHERE RLSUSR_ID=@RLSUSR_ID "
            };
            sqlCommand.Parameters.AddWithValue("@RLSUSR_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<UserRolModel> GetList() {
            return this.GetList<UserRolModel>(new List<IConditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM RLSUSR @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
        #endregion

        /// <summary>
        /// obtener informacion del rol asignado al usuario
        /// </summary>
        /// <param name="idUser">indice del usuario</param>
        public UserRolModel GetByIdUser(int idUser) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 RLUSRL.*, RLSUSR.* FROM RLUSRL LEFT JOIN RLSUSR ON RLUSRL.RLUSRL_RLSUSR_ID = RLSUSR.RLSUSR_ID WHERE RLUSRL.RLUSRL_USR_ID = @index"
            };
            sqlCommand.Parameters.AddWithValue("@index", idUser);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public UserRolModel Save(UserRolModel item) {
            if (item.IdUserRol > 0) {
                item.Creo = this.User;
                item.FechaModifica = DateTime.Now;
                this.Update(item);
            } else {
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
                item.IdUserRol = this.Insert(item);
            }
            return item;
        }
    }
}
