﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Kaiju.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Kaiju.Repositories {
    /// <summary>
    /// clase para perfiles de usuario
    /// </summary>
    public partial class SqlFbUserRepository : Abstractions.RepositoryMaster<UserModel>, Domain.Kaiju.Contracts.ISqlUserRepository {
        #region CRUD 178256416
        public int Insert(UserModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO \""USER\"" ( USER_ID, USER_A, USER_IDROLL, USER_IDDIR, USER_CLV, USER_APIKEY, USER_PSW, USER_MAIL, USER_NOM, USER_FN, USER_USR_N) 
                                                VALUES (@USER_ID,@USER_A,@USER_IDROLL,@USER_IDDIR,@USER_CLV,@USER_APIKEY,@USER_PSW,@USER_MAIL,@USER_NOM,@USER_FN,@USER_USR_N);"
            };
            sqlCommand.Parameters.AddWithValue("@USER_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@USER_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@USER_IDROLL", item.IdUserRol);
            sqlCommand.Parameters.AddWithValue("@USER_IDDIR", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@USER_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@USER_APIKEY", item.ApiKey);
            sqlCommand.Parameters.AddWithValue("@USER_PSW", item.Password);
            sqlCommand.Parameters.AddWithValue("@USER_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@USER_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@USER_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@USER_USR_N", item.Creo);

            item.IdUser = this.ExecuteScalar(sqlCommand);
            if (item.IdUser > 0)
                return item.IdUser;
            return 0;
        }

        public int Update(UserModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE \""USER\"" SET USER_A = @USER_A, USER_IDROLL = @USER_IDROLL, USER_IDDIR = @USER_IDDIR, USER_CLV = @USER_CLV, USER_APIKEY = @USER_APIKEY, USER_PSW = @USER_PSW,
                USER_MAIL = @USER_MAIL, USER_NOM = @USER_NOM, USER_FM = @USER_FM, USER_USR_M = @USER_USR_M WHERE USER_ID = @USER_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@USER_ID", item.IdUser);
            sqlCommand.Parameters.AddWithValue("@USER_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@USER_IDROLL", item.IdUserRol);
            sqlCommand.Parameters.AddWithValue("@USER_IDDIR", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@USER_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@USER_APIKEY", item.ApiKey);
            sqlCommand.Parameters.AddWithValue("@USER_PSW", item.Password);
            sqlCommand.Parameters.AddWithValue("@USER_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@USER_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@USER_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@USER_USR_N", item.Creo);

            return this.ExecuteTransaction(sqlCommand);
        }


        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE \""USER\"" SET USER_A = 0 WHERE USER_ID = @USER_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@id", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public UserModel GetById(int index) {
            return this.GetList<UserModel>(new List<IConditional> { new Conditional("USER_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<UserModel> GetList() {
            return this.GetList<UserModel>(new List<IConditional>());
        }
        #endregion 

        public SqlFbUserRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        public int Search(string clave) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT USER.USER_ID FROM \""USER\"" WHERE UPPER(\""USER\"".USER_CLV) LIKE @clave"
            };
            sqlCommand.Parameters.AddWithValue("@clave", clave.ToUpper());
            var _indice = this.ExecuteScalar(sqlCommand);
            return _indice;
        }

        public UserModel Save(UserModel model) {
            if (model.IdUser == 0) {
                model.IdUser = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public UserModel GetUserBy(string clave, bool correo) {
            if (correo)
                return this.GetList<UserModel>(new List<IConditional> { new Conditional("UPPER(@USER_MAIL)", clave) }).FirstOrDefault();
            return this.GetList<UserModel>(new List<IConditional> { new Conditional("UPPER(@USER_CLV)", clave) }).FirstOrDefault();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand() {
                CommandText = @"SELECT * FROM ""USER"" wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }
    }
}
