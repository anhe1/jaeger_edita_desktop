﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Builder {
    /// <summary>
    /// Consulta
    /// </summary>
    public interface IRemisionQueryBuilder {
        /// <summary>
        /// por ejercicio
        /// </summary>
        IRemisionYearQueryBuilder WithYear(int year);
        /// <summary>
        /// por indice de receptor
        /// </summary>
        IRemisionadoQueryBuild WithIdReceptor(int idReceptor);
        /// <summary>
        /// por conceptos 
        /// </summary>
        IRemisionadoQueryBuild WithConceptos(int idReceptor);
        /// <summary>
        /// listado de remisiones por indice del pedido RMSN_PDD_ID
        /// </summary>
        IRemisionadoQueryBuild IdPedido(int idPedido);
        /// <summary>
        /// por folio
        /// </summary>
        IRemisionadoQueryBuild WithFolio(int folio);

        IRemisionStatusQueryBuilder WithIdStatus(ValueObjects.RemisionStatusEnum status);

        IRemisionYearQueryBuilder IdVendedor(int idVendedor);

        IRemisionConceptosQueryBuilder GetConceptos(int idRemision);
        /// <summary>
        /// condicionales para actualizar la tabla de existencias
        /// </summary>
        IRemisionStockQueryBuilder SetStock(int idRemision);
    }

    public interface IRemisionYearQueryBuilder {
        IRemisionMonthQueryBuilder WithMonth(int month);
        IRemisionadoQueryBuild WithIdReceptor(int idReceptor);
        IRemisionStatusQueryBuilder WithIdStatus(ValueObjects.RemisionStatusEnum status);
        IRemisionStatusQueryBuilder WithIdStatus(int idStatus);
        IRemisionStatusQueryBuilder WithIdStatus(int[] idStatus);
        IRemisionYearQueryBuilder IdVendedor(int idVendedor);
        List<IConditional> Build();
    }

    public interface IRemisionMonthQueryBuilder {
        IRemisionStatusQueryBuilder WithIdStatus(ValueObjects.RemisionStatusEnum status);
        IRemisionStatusQueryBuilder WithIdStatus(int idStatus);
        IRemisionStatusQueryBuilder WithIdStatus(int[] idStatus);
        List<IConditional> Build();
    }

    public interface IRemisionStatusQueryBuilder {
        List<IConditional> Build();
    }

    public interface IRemisionadoQueryBuild {
        IRemisionadoQueryBuild OnlyActive(bool onlyActive);
        IRemisionYearQueryBuilder WithYear(int year);
        IRemisionStatusQueryBuilder WithIdStatus(ValueObjects.RemisionStatusEnum status);
        IRemisionStatusQueryBuilder WithIdStatus(int idStatus);
        IRemisionStatusQueryBuilder WithIdStatus(int[] idStatus);
        List<IConditional> Build();
    }

    public interface IRemisionReceptorQueryBuilder {
        IRemisionadoQueryBuild WithIdReceptor(int idReceptor);
    }   

    public interface IRemisionByIdBuilder {
        IRemisionDetailModel ById(int index);
    }

    public interface IRemisionConceptosQueryBuilder : IConditionalBuilder {

    }

    /// <summary>
    /// condicionales para actualizar la tabla de existencias
    /// </summary>
    public interface IRemisionStockQueryBuilder : IConditionalBuilder {

    }
}
