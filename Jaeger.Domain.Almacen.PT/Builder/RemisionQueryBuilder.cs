﻿using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Builder {
    public class RemisionQueryBuilder : ConditionalBuilder, IConditionalBuilder, IRemisionQueryBuilder, IRemisionYearQueryBuilder, IRemisionMonthQueryBuilder, IRemisionStatusQueryBuilder, IRemisionadoQueryBuild, IRemisionReceptorQueryBuilder,
        IRemisionConceptosQueryBuilder, IRemisionStockQueryBuilder {

        /// <summary>
        /// Constructor
        /// </summary>
        public RemisionQueryBuilder() : base() {
            this._Conditionals = new List<IConditional>();
        }

        public IRemisionStatusQueryBuilder WithIdStatus(RemisionStatusEnum status) {
            var idStatus = (int)status;
            this._Conditionals.Add(new Conditional("RMSN_STTS_ID", idStatus.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// con id de status
        /// </summary>
        /// <param name="idStatus">indice del status</param>
        public IRemisionStatusQueryBuilder WithIdStatus(int idStatus) {
            this._Conditionals.Add(new Conditional("RMSN_STTS_ID", idStatus.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// con array de status
        /// </summary>
        /// <param name="idStatus">array de status</param>
        public IRemisionStatusQueryBuilder WithIdStatus(int[] idStatus) {
            this._Conditionals.Add(new Conditional("RMSN_STTS_ID", string.Join(",", idStatus), ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// por mes, en caso de ser cero se omite la condicion
        /// </summary>
        /// <param name="month">numero del mes</param>
        public IRemisionMonthQueryBuilder WithMonth(int month) {
            // solo si el mes es mayor a cero
            if (month > 0)
                this._Conditionals.Add(new Conditional("RMSN_MES", month.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// del ejercicio
        /// </summary>
        /// <param name="year">ejercicio</param>
        public IRemisionYearQueryBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional("RMSN_ANIO", year.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// conceptos por id de orden de produccion
        /// </summary>
        /// <param name="idPedido">orden de produccion</param>
        public IRemisionadoQueryBuild WithConceptos(int idPedido) {
            this._Conditionals.Add(new Conditional("MVAPT_PDCLN_ID", idPedido.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// listado de remisiones por indice del pedido RMSN_PDD_ID
        /// </summary>
        public IRemisionadoQueryBuild IdPedido(int idPedido) {
            this._Conditionals.Add(new Conditional("RMSN_PDD_ID", idPedido.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// por indice del receptor
        /// </summary>
        /// <param name="idReceptor">indice del receptor</param>
        public IRemisionadoQueryBuild WithIdReceptor(int idReceptor) {
            this._Conditionals.Add(new Conditional("RMSN_DRCTR_ID", idReceptor.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">verdadero solo registros activos</param>
        public IRemisionadoQueryBuild OnlyActive(bool onlyActive) {
            var activo = (onlyActive ? "1" : "0");
            this._Conditionals.Add(new Conditional("RMSN_A", activo, ConditionalTypeEnum.Equal));
            return this;
        }

        /// <summary>
        /// Por folio de control interno
        /// </summary>
        /// <param name="folio">folio</param>
        public IRemisionadoQueryBuild WithFolio(int folio) {
            this._Conditionals.Add(new Conditional("RMSN_FOLIO", folio.ToString()));
            return this;
        }

        public IRemisionYearQueryBuilder IdVendedor(int idVendedor) {
            this._Conditionals.Add(new Conditional("RMSN_VNDDR_ID", idVendedor.ToString(), ConditionalTypeEnum.Equal));
            return this;
        }

        public IRemisionConceptosQueryBuilder GetConceptos(int idRemision) {
            this._Conditionals.Add(new Conditional("MVAPT_RMSN_ID", idRemision.ToString()));
            this._Conditionals.Add(new Conditional("MVAPT_A", "1"));
            this._Conditionals.Add(new Conditional("MVAPT_DOC_ID", "26"));
            return this;
        }

        /// <summary>
        /// condicionales para actualizar la tabla de existencias
        /// </summary>
        public IRemisionStockQueryBuilder SetStock(int idRemision) {
            this._Conditionals = new List<IConditional> {
                    new Conditional("MVAPT_RMSN_ID", idRemision.ToString()),
                    new Conditional("MVAPT_DOC_ID", "26"),
                    new Conditional("MVAPT_A", "1")
            };
            return this;
        }
    }
}
