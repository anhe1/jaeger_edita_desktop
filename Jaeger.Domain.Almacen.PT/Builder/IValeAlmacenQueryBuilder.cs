﻿using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Builder {
    public interface IValeAlmacenQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IConditionalBuilder ByFolio(int folio);
        IValeAlmacenIdDirectorioQueryBuilder ById(int idComprobante);
        IValeAlmacenIdDirectorioQueryBuilder ByIdReceptor(int idComprobante);
        IValeAlmacenIdAlmacenQueryBuilder ByIdAlmacen(int idAlmacen);
        IValeAlmacenIdDirectorioQueryBuilder ByType(TipoComprobanteEnum movimientoTipo);
        IMovimientoAlmacenQueryBuilder SetStock();
    }

    public interface IValeAlmacenIdAlmacenQueryBuilder {
        IValeAlmacenIdDirectorioQueryBuilder ByTipoAlmacen(AlmacenEnum idAlmacen);
    }

    public interface IValeAlmacenIdDirectorioQueryBuilder : IConditionalBuilder {
        IConditionalBuilder ByFolio(int folio);
        IValeAlmacenQueryYearBuilder WithYear(int year);
        IValeAlmacenIdDirectorioQueryBuilder ByType(TipoComprobanteEnum movimientoTipo);
    }

    public interface IValeAlmacenQueryYearBuilder {
        IValeAlmacenQueryMonthBuilder WithMonth(int month);
    }

    public interface IValeAlmacenQueryMonthBuilder : IConditionalBuilder {
        IConditionalBuild ByStatus(int idAlmacen);
    }

    /// <summary>
    /// condicionales para actualizar la tabla de existencias
    /// </summary>
    public interface IValeAlmacenStockQueryBuilder : IConditionalBuilder {

    }
}
