﻿using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Builder {
    public class ValeAlmacenQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IValeAlmacenQueryBuilder, IValeAlmacenIdDirectorioQueryBuilder, IValeAlmacenQueryYearBuilder, IValeAlmacenQueryMonthBuilder,
        IValeAlmacenIdAlmacenQueryBuilder, IValeAlmacenStockQueryBuilder {
        public ValeAlmacenQueryBuilder() : base() {

        }

        public IValeAlmacenIdDirectorioQueryBuilder ById(int idComprobante) {
            this._Conditionals.Add(new Conditional("ALMPT_ID", idComprobante.ToString()));
            return this;
        }

        /// <summary>
        /// tipo de comprobante
        /// </summary>
        public IValeAlmacenIdDirectorioQueryBuilder TipoComprobante(TipoComprobanteEnum tipo) {
            var d0 = (int)tipo;
            this._Conditionals.Add(new Conditional("ALMPT_DOC_ID", d0.ToString()));
            return this;
        }

        /// <summary>
        /// indice del catalogo de almacenes
        /// </summary>
        public IValeAlmacenIdAlmacenQueryBuilder ByIdAlmacen(int idAlmacen) {
            this._Conditionals.Add(new Conditional("ALMPT_CTALM_ID", idAlmacen.ToString()));
            return this;
        }

        /// <summary>
        /// incide de tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        public IValeAlmacenIdDirectorioQueryBuilder ByTipoAlmacen(AlmacenEnum idAlmacen) {
            var d0 = (int)idAlmacen;
            this._Conditionals.Add(new Conditional("ALMPT_TPALM_ID", d0.ToString()));
            return this;
        }

        public IConditionalBuilder ByFolio(int folio) {
            this._Conditionals.Clear();
            this._Conditionals.Add(new Conditional("ALMPT_FOLIO", folio.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder IdDepartamento(int folio) {
            this._Conditionals.Add(new Conditional("ALMPT_CTDEP_ID", folio.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByIdReceptor(int idDirectorio) {
            this._Conditionals.Add(new Conditional("ALMPT_DRCTR_ID", idDirectorio.ToString()));
            return this;
        }

        public IConditionalBuild ByStatus(int idStatus) {
            this._Conditionals.Add(new Conditional("ALMPT_STTS_ID", idStatus.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByType(TipoMovimientoEnum movimientoTipo) {
            this._Conditionals.Add(new Conditional("ALMPT_CTEFC_ID", movimientoTipo.ToString()));
            return this;
        }

        public IValeAlmacenIdDirectorioQueryBuilder ByType(TipoComprobanteEnum tipo) {
            var d0 = (int)tipo;
            this._Conditionals.Add(new Conditional("ALMPT_DOC_ID", d0.ToString()));
            return this;
        }

        public IValeAlmacenQueryMonthBuilder WithMonth(int month) {
            if (month > 0) this._Conditionals.Add(new Conditional("ALMPT_MES", month.ToString()));
            return this;
        }

        public IValeAlmacenQueryYearBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional("ALMPT_ANIO", year.ToString()));
            return this;
        }

        /// <summary>
        /// condicionales para actualizar la tabla de existencias
        /// </summary>
        public IMovimientoAlmacenQueryBuilder SetStock() {
            return new MovimientoAlmacenQueryBuilder();
        }
    }
}
