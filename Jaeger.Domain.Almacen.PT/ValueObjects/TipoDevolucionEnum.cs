﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    public enum TipoDevolucionEnum {
        [Description("No definido")]
        None = 0,
        [Description("Por Cliente")]
        PorCliente = 1,
        [Description("Producto con defecto")]
        PorDefecto = 2,
        [Description("No se llevó a cabo la operación.")]
        SinOperacion = 3,
        [Description("Solicitud de vendedor")]
        PorVendedor = 4,
        [Description("Mercancía no pedida")]
        Devolucion = 5,
        [Description("Mercancía pedida por error")]
        SinSolicitud = 6,
        [Description("Cambio de la mercancía")]
        Cambio = 7,
        [Description("Solicita Administrador")]
        Administrador = 99
    }
}
