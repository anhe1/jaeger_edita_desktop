﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    /// <summary>
    /// status de remisiones
    /// </summary>
    public enum RemisionStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Pendiente")]
        Pendiente = 1,
        [Description("En Ruta...")]
        En_Ruta = 2,
        [Description("Entregado")]
        Entregado = 3,
        [Description("Por Cobrar")]
        Por_Cobrar = 4,
        [Description("Cobrado")]
        Cobrado = 5,
        [Description("Facturado")]
        Facturado = 6
    }
}
