﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    public enum RemisionCondicionesPagoEnum {
        [Description("Pago contra entrega")]
        PagoContraEntrega = 1,
        [Description("Cliente con crédito")]
        ConCredito = 2,
    }
}
