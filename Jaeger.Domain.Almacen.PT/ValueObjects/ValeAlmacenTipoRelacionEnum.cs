﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    public enum ValeAlmacenTipoRelacionEnum {
        [Description("Sin relación")]
        NoDefinido = 0,
        [Description("Comprobante emitido con errores")]
        PorError = 1,
        [Description("Cambia por documento nuevo")]
        Cambia = 2,
        [Description("No se llevó a cabo la operación.")]
        SinOperacion = 3,
        [Description("Sustitución de un documento previo")]
        Sustitucion = 4,
        [Description("Devolución de mercancías")]
        Devolucion = 5,
        [Description("Rechazado por C. Calidad")]
        Rechazado = 6,
        [Description("Solicita Administrador")]
        Administrador = 99
    }
}
