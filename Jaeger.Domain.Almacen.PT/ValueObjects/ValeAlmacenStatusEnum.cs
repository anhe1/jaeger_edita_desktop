﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    /// <summary>
    /// enumeracion de status
    /// </summary>
    public enum ValeAlmacenStatusEnum {
        [Description("Cancelado")]
        Cancelado = 0,
        [Description("Emitido")]
        Emitido = 1,
        [Description("Entregado")]
        Entregado = 2,
        [Description("Recibido")]
        Recibido = 3
    }
}
