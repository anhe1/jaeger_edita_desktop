﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    /// <summary>
    /// tipos de relacion con remisiones
    /// </summary>
    public enum RemisionTipoRelacionEnum {
        [Description("Sin relación")]
        NoDefinido = 0,
        [Description("Comprobante emitido con errores")]
        PorError = 1,
        [Description("Cambia por remisión")]
        Cambia = 2,
        [Description("No se llevó a cabo la operación.")]
        SinOperacion = 3,
        [Description("Sustitución de remisión previa")]
        Sustitucion = 4,
        [Description("Devolución de mercancías")]
        Devolucion = 5,
        [Description("Aplicación de descuentos")]
        Descuento = 6,
        [Description("Solicita Administrador")]
        Administrador = 99
    }
}
