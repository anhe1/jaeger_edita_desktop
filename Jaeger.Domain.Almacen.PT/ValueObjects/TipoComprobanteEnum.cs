﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    /// <summary>
    /// Enumeracion de tipo de documento dentro del sistema
    /// </summary>
    public enum TipoComprobanteEnum {
        [Description("Ninguno")]
        None = 0,
        /// <summary>
        /// Remision = 26
        /// </summary>
        [Description("Remisión")]
        Remision = 26,
        [Description("Salida de Producto")]
        SalidaProducto = 28,
        /// <summary>
        /// Entradas de Producto = 29
        /// </summary>
        [Description("Entrada de Producto")]
        EntradaProducto = 29,
        /// <summary>
        /// Devoluciones de Producto = 30
        /// </summary>
        [Description("Devolución de Producto")]
        Devolucion = 30,
        /// <summary>
        /// Pedido del cliente = 33
        /// </summary>
        [Description("Pedido")]
        Pedido = 33,
        /// <summary>
        /// Remision Interna = 34 en produccion
        /// </summary>
        [Description("Remisión Interna")]
        RemisionInterna = 34,
        /// <summary>
        /// Nota de Descuento = 35
        /// </summary>
        [Description("Nota de descuento")]
        NotaDescuento = 35,
    }
}
