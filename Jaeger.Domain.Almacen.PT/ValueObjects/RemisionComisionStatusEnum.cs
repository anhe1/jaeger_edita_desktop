﻿// banderas utilizadas para el status de la comision
// RMSN_CTDIS_ID = 0 < --no esta agregada a ningun recibo
// RMSN_CTDIS_ID = 1 < --la comsion autorizada y esta agregada a un recibo
// RMSN_CTDIS_ID = 2 < --comision pagada y ya esta autorizada en un recibo
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    public enum RemisionComisionStatusEnum {
        /// <summary>
        /// no esta agregada a ningun recibo
        /// </summary>
        [Description("Sin relación")]
        SinRelacion = 0,
        /// <summary>
        /// la comsion autorizada y esta agregada a un recibo
        /// </summary>
        [Description("Autorizada")]
        Autorizada = 1,
        /// <summary>
        /// comision pagada y ya esta autorizada en un recibo
        /// </summary>
        [Description("Pagada")]
        Pagada = 2
    }
}
