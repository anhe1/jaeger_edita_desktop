﻿using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.ValueObjects {
    /// <summary>
    /// tipos de remision
    /// </summary>
    public enum RemisionTipoEnum {
        [Description("Ingreso")]
        Ingreso = 1,
        [Description("Egreso")]
        Egreso = 2,
        [Description("Traslado")]
        Traslado = 3
    }
}
