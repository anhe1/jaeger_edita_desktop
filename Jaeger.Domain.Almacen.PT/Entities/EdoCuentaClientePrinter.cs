﻿using System.Collections.Generic;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class EdoCuentaClientePrinter {
        public EdoCuentaClientePrinter() {
        }

        public string Cliente { get; set; }

        public string Vendedor { get; set; }

        public List<RemisionSingleModel> Conceptos { get; set; }

        public string[] QrText {
            get {
                return new string[] { "||Vendedores||" };
            }
        }
    }
}
