﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class ValeAlmacenTipo : BaseSingleModel {
        public ValeAlmacenTipo() {

        }

        public ValeAlmacenTipo(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
