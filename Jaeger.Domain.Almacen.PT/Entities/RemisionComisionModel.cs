﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    /// <summary>
    /// registro de calculo de pago de comisiones a vendedores
    /// </summary>
    [SugarTable("RMSNC", "Remisionado PT: Comisiones a vendedores")]
    public class RemisionComisionModel : BasePropertyChangeImplementation, IRemisionComisionModel {
        #region declaraciones
        private bool activo;
        private int idCatalogo;
        private int idremision;
        private decimal _FactorComisionActualizda;
        private decimal apagar;
        private decimal pagado;
        private string modifica;
        private DateTime fechaModifica;
        private string descripcion;
        private decimal _FactorComisionPacatada;
        private string _Vendedor;
        private int _IdVendedor;
        private int _IdCliente;
        private int _IsDisponible;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public RemisionComisionModel() {
            this.activo = true;
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_rmsnc_a", "RMSNC_A")]
        [SugarColumn(ColumnName = "RMSNC_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la remision
        /// </summary>
        [DataNames("_rmsnc_rmsn_id", "RMSNC_RMSN_ID")]
        [SugarColumn(ColumnName = "RMSNC_RMSN_ID", ColumnDescription = "indice de relacion con la tabla de remisiones")]
        public int IdRemision {
            get {
                return this.idremision;
            }
            set {
                this.idremision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de comisiones
        /// </summary>
        [DataNames("_rmsnc_ctcms_id", "RMSNC_CTCMS_ID")]
        [SugarColumn(ColumnName = "RMSNC_CTCMS_ID", ColumnDescription = "indice del catalogo de comisiones")]
        public int IdComision {
            get {
                return this.idCatalogo;
            }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el indice del vendedor
        /// </summary>
        [DataNames("_rmsnc_vndr_id", "RMSNC_VNDR_ID")]
        [SugarColumn(ColumnName = "RMSNC_VNDR_ID", ColumnDescription = "indice del catalogo de vendedores")]
        public int IdVendedor {
            get { return this._IdVendedor; }
            set {
                this._IdVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del cliente
        /// </summary>
        [DataNames("_rmsnc_drctr_id", "RMSNC_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSNC_DRCTR_ID", ColumnDescription = "indice de tabla del directorio")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la bandera para indicar si la remision no se incluyo en algun recibo de comision
        /// </summary>
        [DataNames("_rmsnc_ctdis_id", "RMSNC_CTDIS_ID")]
        [SugarColumn(ColumnName = "RMSNC_CTDIS_ID", ColumnDescription = "bandera para indicar si la remision no se incluyo en algun recibo de comision")]
        public int IsDisponible {
            get { return this._IsDisponible; }
            set {
                this._IsDisponible = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de comision
        /// </summary>
        [DataNames("_rmsnc_desc", "RMSNC_DESC")]
        [SugarColumn(ColumnName = "RMSNC_DESC", ColumnDescription = "descripcion de la comisiones")]
        public string Comision {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer factor pactado de comision
        /// </summary>
        [DataNames("_rmsnc_fcpac", "RMSNC_FCPAC")]
        [SugarColumn(ColumnName = "RMSNC_FCPAC", ColumnDescription = "factor pactado de la comision", Length = 14, DecimalDigits = 4)]
        public decimal ComisionFactor {
            get {
                return this._FactorComisionPacatada;
            }
            set {
                this._FactorComisionPacatada = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del descuento aplicable
        /// </summary>
        [DataNames("_rmsnc_fcact", "RMSNC_FCACT")]
        [SugarColumn(ColumnName = "RMSNC_FCACT", ColumnDescription = "factor del descuento aplicable", Length = 14, DecimalDigits = 4)]
        public decimal ComisionFactorActualizado {
            get {
                return this._FactorComisionActualizda;
            }
            set {
                this._FactorComisionActualizda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe de la comision pactada
        /// </summary>
        [DataNames("_rmsnc_impr", "RMSNC_IMPR")]
        [SugarColumn(ColumnName = "RMSNC_IMPR", ColumnDescription = "importe de la comision pactada", Length = 14, DecimalDigits = 4)]
        public decimal ComisionPorPagar {
            get {
                return this.apagar;
            }
            set {
                this.apagar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe realmente pagado al vendedor
        /// </summary>
        [DataNames("_rmsnc_acuml", "RMSNC_ACUML")]
        [SugarColumn(ColumnName = "RMSNC_ACUML", ColumnDescription = "importe realmente pagado al vendedor", Length = 14, DecimalDigits = 4)]
        public decimal ComisionPagada {
            get {
                return this.pagado;
            }
            set {
                this.pagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del vendedor
        /// </summary>
        [DataNames("_rmsnc_vndr", "RMSNC_VNDR")]
        [SugarColumn(ColumnName = "RMSNC_VNDR", ColumnDescription = "clave del vendedor", Length = 10)]
        public string Vendedor {
            get {
                return this._Vendedor;
            }
            set {
                this._Vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_rmsnc_fn", "RMSNC_FN")]
        [SugarColumn(ColumnName = "RMSNC_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaModifica {
            get {
                return this.fechaModifica;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("_rmsnc_usr_n", "RMSNC_USR_N")]
        [SugarColumn(ColumnName = "RMSNC_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }
    }
}
