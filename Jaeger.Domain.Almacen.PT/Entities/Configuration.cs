﻿using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class Configuration : IConfiguration {
        public Configuration() {
            this.ShowPagare = true;
            this.ShowTotalConLetra = true;
            this.ShowDomicilio = true;
            this.Folder = "remision";
        }

        public bool ShowDomicilio { get; set; }
     
        public bool ShowTotalConLetra { get; set; }

        public bool ShowPagare { get; set; }

        public string Pagare { get; set; }

        /// <summary>
        /// obtener o establecer folder de almacenamiento de archivos
        /// </summary>
        public string Folder { get; set; }

        public string Leyenda1 { get; set; }

        public string Leyenda2 { get; set; }

        public string Templete { get; set; }
    }
}
