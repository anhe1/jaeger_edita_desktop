﻿using System.Reflection;
using System;
using SqlSugar;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class DocumentoAlmacenDetailModel : DocumentoAlmacenModel, IDocumentoAlmacen {
        private bool _ready;
        private BindingList<ItemSelectedModel> _relacionDirectorio;

        public DocumentoAlmacenDetailModel() : base() {
            this._relacionDirectorio = new BindingList<ItemSelectedModel>(((TipoRelacionComericalEnum[])Enum.GetValues(typeof(TipoRelacionComericalEnum))).Where(it => it.ToString() != "None")
                .Select(c => new ItemSelectedModel((int)c, false, c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description))
                .ToList());
        }

        public new TipoMovimientoEnum TipoMovimiento {
            get { return (TipoMovimientoEnum)this.IdTipo; }
            set { this.IdTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        public TipoComprobanteEnum TipoComprobante {
            get { return (TipoComprobanteEnum)this.IdDocumento; }
            set { this.IdDocumento = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objetos del directorio
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ItemSelectedModel> ReceptorTipos {
            get {
                if (this._ready == false) {
                    if (this.Receptores != null) {
                        if (this.Receptores.Length > 0) {
                            var d = this.Receptores.Split(',');
                            this._relacionDirectorio.ToList().ForEach(it => { it.Selected = d.Contains(it.Id.ToString()); });
                        }
                    }
                    this._ready = true;
                }
                return this._relacionDirectorio;
            }
            set {
                this._relacionDirectorio = value;
                this.OnPropertyChanged();
            }
        }
    }
}
