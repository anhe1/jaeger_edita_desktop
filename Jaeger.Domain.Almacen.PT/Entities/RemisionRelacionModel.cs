﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionRelacionModel : BaseSingleModel {
        public RemisionRelacionModel() {

        }

        public RemisionRelacionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
     