﻿using SqlSugar;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public partial class ValeAlmacenPrinter : ValeAlmacenDetailModel, IValeAlmacenDetailModel {
        #region declaraciones
        private DocumentoAlmacenDetailModel _FormatoImpreso;
        #endregion

        public ValeAlmacenPrinter() {
            this._FormatoImpreso = null;
        }

        public ValeAlmacenPrinter(ValeAlmacenDetailModel source) {
            if (source != null) {
                MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenPrinter>(source, this);
                this.Conceptos = source.Conceptos;
            }
        }

        /// <summary>
        /// obtener o establecer el formato de impresion del documento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public DocumentoAlmacenDetailModel Documento {
            get { return this._FormatoImpreso; }
            set {
                this._FormatoImpreso = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string EfectoText {
            get { return EnumerationExtension.GetEnumDescription((TipoMovimientoEnum)this.IdTipoMovimiento); }
        }

        public string TipoText {
            get { return base.TipoComprobante.Description(); }
        }

        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdDocumento,
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&rfce=&rfcr=", this.ReceptorRFC, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Total.ToString("0.00") };
            }
        }
    }
}
