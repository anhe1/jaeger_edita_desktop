﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("RMSNR")]
    public class RemisionRelacionadaModel : BasePropertyChangeImplementation, IRemisionRelacionadaModel {
        #region declaraciones
        
        private int idRemision;
        private string idDocumento;
        private string serie;
        private int folio;
        private int idDirectorio;
        private DateTime fechaEmision;
        
        private string creo;
        
        private string receptorNombre;
        private decimal total;
        private int _IdTipoRelacion;
        private string _Relacion;
        private DateTime fechaNuevo;
        #endregion

        public RemisionRelacionadaModel() {
            this.fechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        //[DataNames("RMSNR_ID")]
        //[SugarColumn(IsIgnore = true, ColumnName = "RMSNR_ID", IsPrimaryKey = true, IsIdentity = true)]
        //public int IdRelacion {
        //    get {
        //        return this.index;
        //    }
        //    set {
        //        this.index = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer el indice de relacion de la remision
        /// </summary>
        [DataNames("RMSNR_RMSN_ID", "RMSN_ID")]
        [SugarColumn(ColumnName = "RMSNR_RMSN_ID", ColumnDescription = "indice de relacion con la tabla de remisiones")]
        public int IdRemision {
            get {
                return this.idRemision;
            }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("RMSNR_DRCTR_ID", "RMSN_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSNR_DRCTR_ID", ColumnDescription = "indice de relacion del directorio")]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de tipos de relacion
        /// </summary>
        [DataNames("RMSNR_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSNR_CTREL_ID", ColumnDescription = "indice de la tabla de tipos de relacion")]
        public int IdTipoRelacion {
            get { return this._IdTipoRelacion; }
            set { this._IdTipoRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer descripcion o motivo de la relacion
        /// </summary>
        [DataNames("RMSNR_RELN")]
        [SugarColumn(ColumnName = "RMSNR_RELN", ColumnDescription = "descripcion de la relacion del comprobante")]
        public string Relacion {
            get { return this._Relacion; }
            set { this._Relacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("RMSNR_UUID", "RMSN_UUID")]
        [SugarColumn(ColumnName = "RMSNR_UUID", ColumnDescription = "", Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSNR_SERIE", "RMSN_SERIE")]
        [SugarColumn(ColumnName = "RMSNR_SERIE", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSNR_FOLIO", "RMSN_FOLIO")]
        [SugarColumn(ColumnName = "RMSNR_FOLIO", ColumnDescription = "para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres")]
        public int Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSNR_NOMR", "RMSN_NOMR")]
        [SugarColumn(ColumnName = "RMSNR_NOMR", ColumnDescription = "nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el RFC, del receptor del comprobante")]
        public string ReceptorNombre {
            get {
                return this.receptorNombre;
            }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("RMSNR_FECEMS", "RMSN_FECEMS")]
        [SugarColumn(ColumnName = "RMSNR_FECEMS", ColumnDescription = "fecha y hora de expedicion del comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("RMSNR_TOTAL", "RMSN_TOTAL")]
        [SugarColumn(ColumnName = "RMSNR_TOTAL", ColumnDescription = "importe total del comprobante")]
        public decimal GTotal {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("RMSNR_FN")]
        [SugarColumn(ColumnName = "RMSNR_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("RMSNR_USR_N")]
        [SugarColumn(ColumnName = "RMSNR_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        //[DataNames("RMSNR_FM")]
        //[SugarColumn(IsIgnore = true, ColumnName = "RMSNR_FM", ColumnDescription = "ultima fecha de modificaion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        //public DateTime? FechaModifica {
        //    get {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaModifica >= firstGoodDate)
        //            return this.fechaModifica;
        //        return null;
        //    }
        //    set {
        //        this.fechaModifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        //[DataNames("RMSNR_USR_M")]
        //[SugarColumn(IsIgnore = true, ColumnName = "RMSNR_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        //public string Modifica {
        //    get {
        //        return this.modifica;
        //    }
        //    set {
        //        this.modifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}
    }
}
     