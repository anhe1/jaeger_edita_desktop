﻿using Jaeger.Domain.Almacen.PT.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionStatusDetailModel : RemisionStatusModel, IRemisionStatusDetailModel {
        private BindingList<IRemisionRelacionadaModel> _Relaciones;

        public RemisionStatusDetailModel() : base() {
            this._Relaciones = new BindingList<IRemisionRelacionadaModel>();
        }

        public BindingList<IRemisionRelacionadaModel> Relaciones {
            get { return this._Relaciones; }
            set { this._Relaciones = value;
                this.OnPropertyChanged();
            }
        }
    }
}
     