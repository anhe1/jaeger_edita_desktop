﻿using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("ALMPT", "ALMPT: movimientos")]
    public class DevolucionDetailModel : ValeAlmacenModel, IValeAlmacenModel, IDevolucionDetailModel {
        private BindingList<ValeAlmacenConceptoModel> _Conceptos;

        public DevolucionDetailModel() : base() {
            this.PropertyChanged += this.ValeAlmacenDetailModel_PropertyChanged;
            this._Conceptos = new BindingList<ValeAlmacenConceptoModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
        }

        public DevolucionDetailModel(ValeAlmacenModel source) : base() {
            MapperClassExtensions.MatchAndMap<ValeAlmacenModel, DevolucionDetailModel>(source, this);
        }

        [SugarColumn(IsIgnore = true)]
        public DevolucionPTStatusEnum Status {
            get { return (DevolucionPTStatusEnum)this.IdStatus; }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum TipoComprobante {
            get { return (TipoComprobanteEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((DevolucionPTStatusEnum)this.IdStatus); }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get { return string.IsNullOrEmpty(this.IdDocumento); }
        }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string KeyName {
            get { return string.Format("DEV-{0}-{1}-{2}-{3}-{4}", this.Creo, this.ReceptorRFC, this.IdDocumento, this.Folio.ToString("000000"), this.FechaEmision.ToString("yyyyMMddHHmmss")); }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get {
                var d1 = string.Join("", this.Conceptos.Select(it => it.GetOriginalString));
                var d2 = string.Join("|", new string[] { this.Version, this.Folio.ToString(), this.Serie, this.Creo, this.ReceptorRFC, this.Receptor, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("0.00") });
                return "||" + string.Join("|", d2 + d1) + "||";
            }
        }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenConceptoModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        public void SetValues(DevolucionDetailModel source) {
            MapperClassExtensions.MatchAndMap<DevolucionDetailModel, DevolucionDetailModel>(source, this);
        }

        private void ValeAlmacenDetailModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "IdEfecto") {
                this._Conceptos.Select(it => {
                    it.IdTipoMovimiento = this.IdTipoMovimiento;
                    it.Cantidad = (this.IdComprobante == 0 ? 0 : it.Cantidad);
                    return it;
                }).ToList();
            }
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.Descuento);
            this.TotalTrasladoIVA = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.TrasladoIVA);
            this.Total = (this.SubTotal - this.TotalDescuento) + (this.TotalTrasladoIVA);
        }
    }
}
