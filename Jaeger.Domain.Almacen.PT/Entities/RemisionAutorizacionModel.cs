﻿using System;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionAutorizacionModel : BasePropertyChangeImplementation, IRemisionAutorizacionModel {
        #region declaraciones
        private int _Index;
        private int _IdRemision;
        private int _IdStatus;
        private string _ClaveCancelacion;
        private string _Nota;
        private string _Usuario;
        private DateTime _Fecha;
        #endregion

        public RemisionAutorizacionModel() {
            this.Fecha = DateTime.Now;
        }

        public int Id {
            get { return _Index; }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        public int IdRemision {
            get { return this._IdRemision; }
            set {
                this._IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        public string CvCancela {
            get { return _ClaveCancelacion; }
            set {
                this._ClaveCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        public string Usuario {
            get { return this._Usuario; }
            set {
                this._Usuario = value;
                this.OnPropertyChanged();
            }
        }

        public DateTime Fecha {
            get { return this._Fecha; }
            set {
                this._Fecha = value;
                this.OnPropertyChanged();
            }
        }
    }
}
