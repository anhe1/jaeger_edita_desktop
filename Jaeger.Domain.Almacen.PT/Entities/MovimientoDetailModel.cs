﻿using SqlSugar;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class MovimientoDetailModel : MovimientoModel, IMovimientoModel, IMovimientoDetailModel {
        public MovimientoDetailModel() : base() {

        }
    }
}
