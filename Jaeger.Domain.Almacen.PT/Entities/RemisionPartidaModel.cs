﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionPartidaModel : RemisionConceptoDetailModel {
        #region declaraciones
        private bool activo;
        private int precisionDecimal;
        private int idCliente;
        private int folio;
        private int idVendedor;
        private int idStatus;
        private int idTipoDocumento;
        private DateTime fechaEmision;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltPago;
        private DateTime? fechaCobranza;
        private DateTime? fechaVence;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string receptorRFC;
        private string receptorNombre;
        private decimal tipoCambio;
        private decimal trasladoIVA;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private string contacto;
        private int idMetodoEnvio;
        private int idDomicilio;
        private int idNumeroGuia;
        private int idSerie;
        private int idCatalogo;
        private string _urlPDF;
        private int idRelacion;
        private string _ReceptorClave;
        private decimal _TotalRetencionISR;
        private decimal _TotalRetencionIVA;
        private decimal _TotalRetencionIEPS;
        private decimal _TotalTrasladoIEPS;
        private string tipoComprobanteText;
        #endregion

        public RemisionPartidaModel() : base() {
        }

        #region datos generales del comprobante
        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("_rmsn_a", "RMSN_A")]
        [SugarColumn(ColumnName = "RMSN_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public new bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        [DataNames("_rmsn_ctdoc_id", "RMSN_CTDOC_ID")]
        [SugarColumn(ColumnName = "RMSN_CTDOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoDocumento {
            get {
                return this.idTipoDocumento;
            }
            set {
                this.idTipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_rmsn_efecto", "RMSN_EFECTO")]
        [SugarColumn(ColumnName = "RMSN_EFECTO", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobante {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("_rmsn_ver", "RMSN_VER")]
        [SugarColumn(ColumnName = "RMSN_VER", ColumnDescription = "version del comprobante", Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// precision decimal
        /// </summary>
        [DataNames("_rmsn_deci", "RMSN_DECI")]
        [SugarColumn(ColumnName = "RMSN_DECI", ColumnDescription = "decimales", Length = 4)]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("_rmsn_stts_id", "RMSN_STTS_ID")]
        [SugarColumn(ColumnName = "RMSN_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("_rmsn_folio", "RMSN_FOLIO")]
        [SugarColumn(ColumnName = "RMSN_FOLIO", ColumnDescription = "folio de control interno")]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        [DataNames("rmsn_ctsr_id", "RMSN_CTSR_ID")]
        [SugarColumn(ColumnName = "RMSN_CTSR_ID", ColumnDescription = "indice de la tabla de series")]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("_rmsn_serie", "RMSN_SERIE")]
        [SugarColumn(ColumnName = "RMSN_SERIE", ColumnDescription = "serie de control interno del documento en modo texto", Length = 10, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        ///// <summary>
        ///// obtener o establecer el indice o numero de pedido de control interno
        ///// </summary>
        //[DataNames("_rmsn_pdd_id", "RMSN_PDD_ID")]
        //[SugarColumn(ColumnName = "_rmsn_pdd_id", ColumnDescription = "indice o numero de pedido del cliente, control interno")]
        //public new int IdPedido {
        //    get { return this.idPedido; }
        //    set {
        //        this.idPedido = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("_rmsn_ctcls_id", "RMSN_CTCLS_ID")]
        [SugarColumn(ColumnName = "RMSN_CTCLS_ID", ColumnDescription = "indice del catalogo de productos")]
        public int IdCatalogo {
            get { return this.idCatalogo; }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del metodo de envio (RMSN_MTDENV_ID)
        /// </summary>
        [DataNames("_rmsn_ctenv_id", "RMSN_CTENV_ID")]
        [SugarColumn(ColumnName = "RMSN_CTENV_ID", ColumnDescription = "indice de la tabla del metodo de envio")]
        public int IdMetodoEnvio {
            get { return this.idMetodoEnvio; }
            set {
                this.idMetodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("_rmsn_drctr_id", "RMSN_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSN_DRCTR_ID", ColumnDescription = "indice del direcotorio")]
        public new int IdCliente {
            get { return this.idCliente; }
            set {
                this.idCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        [DataNames("_rmsn_drccn_id", "RMSN_DRCCN_ID")]
        [SugarColumn(ColumnName = "RMSN_DRCCN_ID", ColumnDescription = "indice del domicilio del directorio")]
        public int IdDomicilio {
            get { return this.idDomicilio; }
            set {
                this.idDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("_rmsn_vnddr_id", "RMSN_VNDDR_ID")]
        [SugarColumn(ColumnName = "RMSN_VNDDR_ID", ColumnDescription = "indice de la tabla de vendedores")]
        public int IdVendedor {
            get { return this.idVendedor; }
            set {
                this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        [DataNames("_rmsn_guia_id", "RMSN_GUIA_ID")]
        [SugarColumn(ColumnName = "RMSN_GUIA_ID", ColumnDescription = "numero de guia o referencia del metodo de envio")]
        public int NoGuia {
            get { return this.idNumeroGuia; }
            set {
                this.idNumeroGuia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("_rmsn_ctrel_id", "RMSN_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSN_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        [DataNames("_rmsn_rfce", "RMSN_RFCE")]
        [SugarColumn(ColumnName = "RMSN_RFCE", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("_rmsn_clvr", "RMSN_CLVR")]
        [SugarColumn(ColumnName = "RMSN_CLVR", ColumnDescription = "clave de control interno de cliente", Length = 10)]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("_rmsn_rfcr", "RMSN_RFCR")]
        [SugarColumn(ColumnName = "RMSN_RFCR", ColumnDescription = "registro federal de contribuyentes del cliente o receptor", Length = 14)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("_rmsn_nomr", "RMSN_NOMR")]
        [SugarColumn(ColumnName = "RMSN_NOMR", ColumnDescription = "nombre o razon social del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("_rmsn_cntct", "RMSN_CNTCT")]
        [SugarColumn(ColumnName = "RMSN_CNTCT", ColumnDescription = "nombre de la persona que recibe", Length = 255)]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("_rmsn_uuid", "RMSN_UUID")]
        [SugarColumn(ColumnName = "RMSN_UUID", ColumnDescription = "id de documento", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("_rmsn_fecems", "RMSN_FECEMS")]
        [SugarColumn(ColumnName = "RMSN_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega (RMSN_FECENT)
        /// </summary>
        [DataNames("_rmsn_fecent", "RMSN_FECENT")]
        [SugarColumn(ColumnName = "RMSN_FECENT", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de recepcion de cobranza (RMSN_FECCBR)
        /// </summary>
        [DataNames("_rmsn_feccbr", "RMSN_FECCBR")]
        [SugarColumn(ColumnName = "RMSN_FECCBR", ColumnDescription = "fecha de recepcion de cobranza", IsNullable = true)]
        public DateTime? FechaCobranza {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCobranza >= firstGoodDate)
                    return this.fechaCobranza;
                return null;
            }
            set {
                this.fechaCobranza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        [DataNames("_rmsn_fecupc", "RMSN_FECUPC")]
        [SugarColumn(ColumnName = "RMSN_FECUPC", ColumnDescription = "fecha del ultimo pago o cobro del comprobante", IsNullable = true)]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPago >= firstGoodDate)
                    return this.fechaUltPago;
                return null;
            }
            set {
                this.fechaUltPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento del pagare
        /// </summary>
        [DataNames("_rmsn_fecvnc", "RMSN_FECVNC")]
        [SugarColumn(ColumnName = "RMSN_FECVNC", ColumnDescription = "fecha de vencimiento del pagare", IsNullable = true)]
        public DateTime? FechaVence {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaVence >= firstGoodDate)
                    return this.fechaVence;
                return null;
            }
            set {
                this.fechaVence = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        [DataNames("_rmsn_tpcmb", "RMSN_TPCMB")]
        [SugarColumn(ColumnName = "RMSN_TPCMB", ColumnDescription = "tipo de cambio", DefaultValue = "1", IsNullable = true)]
        public decimal TipoCambio {
            get { return this.tipoCambio; }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        [DataNames("_rmsn_sbttl", "RMSN_SBTTL")]
        [SugarColumn(ColumnName = "RMSN_SBTTL", ColumnDescription = "subtotal", DefaultValue = "0")]
        public decimal GSubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento (suma de los descuentos aplicados en las partidas de la remision)
        /// </summary>
        [DataNames("_rmsn_dscnt", "RMSN_DSCNT")]
        [SugarColumn(ColumnName = "RMSN_DSCNT", ColumnDescription = "importe del descuento (suma de los descuentos aplicados en las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalDescuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establer el importe de la remision antes de impuestos (SubToal - TotalDescuentos)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal TotalImporte {
            get { return (this.SubTotal - this.TotalDescuento); }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA (suma del iva de las partidas de la remision)
        /// </summary>
        [DataNames("_rmsn_trsiva", "RMSN_TRSIVA")]
        [SugarColumn(ColumnName = "RMSN_TRSIVA", ColumnDescription = "total del impuesto trasladado IVA (suma del iva de las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de retencion del impuesto ISR
        /// </summary>
        [DataNames("_rmsn_retisr", "RMSN_RETISR")]
        [SugarColumn(ColumnName = "RMSN_RETISR", ColumnDescription = "total de retencion del impuesto ISR", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionISR {
            get { return this._TotalRetencionISR; }
            set {
                this._TotalRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IVA
        /// </summary>
        [DataNames("_rmsn_retiva", "RMSN_RETIVA")]
        [SugarColumn(ColumnName = "RMSN_RETIVA", ColumnDescription = "total del impuesto retenido IVA", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionIVA {
            get { return this._TotalRetencionIVA; }
            set {
                this._TotalRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IEPS
        /// </summary>
        [DataNames("_rmsn_retieps", "RMSN_RETIEPS")]
        [SugarColumn(ColumnName = "RMSN_RETIEPS", ColumnDescription = "total del impuesto retenido IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalRetencionIEPS {
            get { return this._TotalRetencionIEPS; }
            set {
                this._TotalRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto traslado IEPS
        /// </summary>
        [DataNames("_rmsn_trsieps", "RMSN_TRSIEPS")]
        [SugarColumn(ColumnName = "RMSN_TRSIEPS", ColumnDescription = "total del impuesto traslado IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIEPS {
            get { return this._TotalTrasladoIEPS; }
            set {
                _TotalTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("_rmsn_gtotal", "RMSN_GTOTAL")]
        [SugarColumn(ColumnName = "RMSN_GTOTAL", ColumnDescription = "total del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GTotal {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_rmsn_url_pdf", "RMSN_URL_PDF")]
        [SugarColumn(ColumnName = "RMSN_URL_PDF", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
