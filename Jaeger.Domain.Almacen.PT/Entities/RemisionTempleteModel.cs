﻿using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionTempleteModel : IRemisionTempleteModel {
        private string _Name;
        private string _Description;
        private string _Templete;

        public RemisionTempleteModel() {
        }

        public RemisionTempleteModel(string name, string description, string templete) {
            _Name = name;
            _Description = description;
            _Templete = templete;
        }

        public string Name {
            get { return this._Name; }
            set { this._Name = value; }
        }

        public string Description {
            get { return this._Description; }
            set { this._Description = value; }
        }

        public string Templete {
            get { return this._Templete; }
            set { this._Templete = value; }
        }
    }
}
