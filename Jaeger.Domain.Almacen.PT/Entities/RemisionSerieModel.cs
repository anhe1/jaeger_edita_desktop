﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionSerieModel : BaseSingleModel {
        public RemisionSerieModel() {
        }

        public RemisionSerieModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
