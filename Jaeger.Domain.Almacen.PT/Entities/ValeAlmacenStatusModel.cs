﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    /// <summary>
    /// vale del almacen de materia prima
    /// </summary>
    [SugarTable("ALMPTS", "ALMPT: cambios de status")]
    public class ValeAlmacenStatusModel : AlmacenStatus, IValeAlmacenStatusModel {

        public ValeAlmacenStatusModel() {

        }

        /// <summary>
        /// obtener o establecer indice (ALMPT_ID)
        /// </summary>
        [DataNames("ALMPTS_ALMPT_ID")]
        [SugarColumn(ColumnName = "ALMPTS_ALMPT_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public new int IdComprobante {
            get { return base.IdComprobante; }
            set {
                base.IdComprobante = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMPTS_DRCTR_ID")]
        [SugarColumn(ColumnName = "ALMPTS_DRCTR_ID", ColumnDescription = "indice del directorio de clientes", IsNullable = true)]
        public new int IdDirectorio {
            get { return base.IdDirectorio; }
            set {
                base.IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        [DataNames("ALMPTS_STTS_ID")]
        [SugarColumn(ColumnName = "ALMPTS_STTS_ID", ColumnDescription = "indice del catalogo de status")]
        public new int IdStatus {
            get { return base.IdStatus; }
            set {
                base.IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        [DataNames("ALMPTS_STTSB_ID")]
        [SugarColumn(ColumnName = "ALMPTS_STTSB_ID", ColumnDescription = "indice del catalogo de status")]
        public new int IdStatusB {
            get { return base.IdStatusB; }
            set {
                base.IdStatusB = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMPTS_CTMTV_ID")]
        [SugarColumn(ColumnName = "ALMPTS_CTMTV_ID", ColumnDescription = "indice de la tabla de motivos de modificacion del status", IsNullable = true)]
        public new int IdCveMotivo {
            get { return base.IdCveMotivo; }
            set {
                base.IdCveMotivo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMPTS_CTMTV")]
        [SugarColumn(ColumnName = "ALMPTS_CTMTV", ColumnDescription = "clave del motivo del cambio de status")]
        public new string CveMotivo {
            get { return base.CveMotivo; }
            set {
                base.CveMotivo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("ALMPTS_NOTA")]
        [SugarColumn(ColumnName = "ALMPTS_NOTA", ColumnDescription = "observaciones", Length = 50, IsNullable = true)]
        public new string Nota {
            get { return base.Nota; }
            set {
                base.Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        [DataNames("ALMPTS_FN")]
        [SugarColumn(ColumnName = "ALMPTS_FN", ColumnDescription = "fecha de creacion del registro")]
        public new DateTime FechaNuevo {
            get { return base.FechaNuevo; }
            set {
                base.FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ALMPTS_USR_N")]
        [SugarColumn(ColumnName = "ALMPTS_USR_N", ColumnDescription = "clave del usuario creador del registro")]
        public new string Creo {
            get { return base.Creo; }
            set {
                base.Creo = value;
                this.OnPropertyChanged();
            }
        }
    }
}
