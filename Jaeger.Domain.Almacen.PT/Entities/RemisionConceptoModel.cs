﻿using System;
using SqlSugar;

namespace Jaeger.Domain.Almacen.PT.Entities {
    /// <summary>
    /// partidas o conceptos del comprobante de remision
    /// </summary>
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class RemisionConceptoModel : MovimientoDetailModel, ICloneable {
        #region declaraciones
        #endregion

        public RemisionConceptoModel() : base() {
            this.IdTipoComprobante = 26;
            this.IdTipoMovimiento = 2;
        }

        public new object Clone() {
            return this.MemberwiseClone();
        }
    }
}
     