﻿namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionMetodoEnvioModel : Base.Abstractions.BaseSingleModel {
        private decimal _Costo;
        public RemisionMetodoEnvioModel(int id, string descripcion) : base(id, descripcion) {
            this._Costo = 0;
        }

        public RemisionMetodoEnvioModel(int id, string descripcion, decimal costo) : base(id, descripcion) {
            this._Costo = costo;
        }

        public decimal Costo {
            get { return _Costo; }
            set { this._Costo = value; }
        }
    }
}
