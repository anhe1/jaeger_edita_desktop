﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    /// <summary>
    /// movimiento bancario
    /// </summary>
    [SugarTable("_bncmov")]
    public class RemisionMovimientoBancarioModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int _IdStatus;
        private int idCuentaBancaria;
        private string concepto;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private string beneficiario;
        private string claveMoneda;
        private string numDocto;
        private string referencia;
        private string numAutorizacion;
        private string notas;
        private string numOperacion;
        private string referenciaNumerica;
        private string autoriza;
        private string cancela;
        private string formaPago;
        private string claveFormaPago;
        private decimal tipoCambio;
        private decimal cargo;
        private decimal abono;
        private string beneficiarioRFC;
        private string numeroCuenta;
        private string cuentaCLABE;
        private string sucursal;
        private string claveBanco;
        private DateTime? fechaVence;
        private DateTime? fechaDocto;
        private DateTime fechaEmision;
        private DateTime? fechaPago;
        private DateTime? fechaBoveda;
        private DateTime? fechaCancela;
        private string numeroDeCuenta;
        private int idCuenta2;
        private string claveBancoP;
        private string identificador;
        private int idConcepto;
        private int _IdTipo;
        private int tipoOperacionInt;
        private string json;
        private string bancoT;
        private int idDirectorio;
        private int idFormato;
        private bool porJustificar;
        private bool afectarSaldoComprobantes;
        private bool afectarSaldoCuenta;
        private string version;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public RemisionMovimientoBancarioModel() {
            this.version = "3.0";
            this.activo = true;
            this.fechaNuevo = DateTime.Now;
            this.fechaEmision = DateTime.Now;
            this.tipoCambio = 1;
            this.claveMoneda = "MXN";
            this.porJustificar = false;
        }

        /// <summary>
        /// obtener o establecer el indice del movimiento
        /// </summary>
        [DataNames("_bncmov_id", "BNCMOV_ID")]
        [SugarColumn(ColumnName = "bncmov_id", ColumnDescription = "", IsPrimaryKey = true, IsIdentity = true, IsNullable = false)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_bncmov_a", "BNCMOV_A")]
        [SugarColumn(ColumnName = "BNCMOV_A", ColumnDescription = "", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_bncmov_ver", "BNCMOV_VER")]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del estado del movimiento
        /// </summary>
        [DataNames("_bncmov_stts_id", "BNCMOV_STTS_ID")]
        public int IdStatus {
            get {
                return this._IdStatus;
            }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de movimiento o efecto del movimiento 1 = ingreso o 2 = Egreso
        /// </summary>
        [DataNames("_bncmov_tipo_id", "BNCMOV_TIPO_ID")]
        public int IdTipo {
            get {
                return this._IdTipo;
            }
            set {
                this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de operacion
        /// </summary>
        [DataNames("_bncmov_oper_id", "BNCMOV_OPER_ID")]
        public int IdTipoOperacion {
            get {
                return this.tipoOperacionInt;
            }
            set {
                this.tipoOperacionInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del concepto de movimiento
        /// </summary>
        [DataNames("_bncmov_cncp_id", "BNCMOV_CNCP_ID")]
        public int IdConcepto {
            get {
                return this.idConcepto;
            }
            set {
                this.idConcepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obteneer o establecer la clave del formato de impresion
        /// </summary>
        [DataNames("_bncmov_frmt_id", "BNCMOV_FRMT_ID")]
        public int IdFormato {
            get { return this.idFormato; }
            set {
                this.idFormato = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si debe afectar saldo de la cunta
        /// </summary>
        [DataNames("_bncmov_ascta", "BNCMOV_ASCTA")]
        public bool AfectaSaldoCuenta {
            get { return this.afectarSaldoCuenta; }
            set {
                this.afectarSaldoCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la afectactacion de saldos de comprobantes
        /// </summary>
        [DataNames("_bncmov_ascom", "BNCMOV_ASCOM")]
        public bool AfectaSaldoComprobantes {
            get {
                return this.afectarSaldoComprobantes;
            }
            set {
                this.afectarSaldoComprobantes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("_bncmov_drctr_id", "BNCMOV_DRCTR_ID")]
        public int IdDirectorio {
            get {
                return this.idDirectorio;
            }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de la cuenta de banco a utilizar
        /// </summary>
        [DataNames("_bncmov_ctae_id", "BNCMOV_CTAE_ID")]
        public int IdCuentaP {
            get {
                return this.idCuentaBancaria;
            }
            set {
                this.idCuentaBancaria = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la cuenta del receptor o beneficiario
        /// </summary>
        [DataNames("_bncmov_ctar_id", "BNCMOV_CTAR_ID")]
        public int IdCuentaT {
            get {
                return this.idCuenta2;
            }
            set {
                this.idCuenta2 = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:bandera solo para indicar si el gasto es por justificar
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [DataNames("_bncmov_por", "BNCMOV_POR")]
        public bool PorComprobar {
            get {
                return this.porJustificar;
            }
            set {
                this.porJustificar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el identificador de la prepoliza poliza
        /// </summary>           
        [DataNames("_bncmov_noiden", "BNCMOV_NOIDEN")]
        public string Identificador {
            get {
                return this.identificador;
            }
            set {
                this.identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de cuenta bancaria
        /// </summary>
        [DataNames("_bncmov_numctap", "BNCMOV_NUMCTAP")]
        public string NumeroCuentaP {
            get {
                return this.numeroDeCuenta;
            }
            set {
                this.numeroDeCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("_bncmov_clvbp", "BNCMOV_CLVBP")]
        public string ClaveBancoP {
            get {
                return this.claveBancoP;
            }
            set {
                this.claveBancoP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>           
        [DataNames("_bncmov_fecems", "BNCMOV_FECEMS")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha del documento
        /// </summary>           
        [DataNames("_bncmov_fecdoc", "BNCMOV_FECDOC")]
        public DateTime? FechaDocto {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaDocto >= firstGoodDate)
                    return this.fechaDocto;
                else
                    return null;
            }
            set {
                this.fechaDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento 
        /// </summary>           
        [DataNames("_bncmov_fcvnc", "BNCMOV_FCVNC")]
        public DateTime? FechaVence {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaVence >= firstGooDate)
                    return this.fechaVence;
                else
                    return null;
            }
            set {
                this.fechaVence = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cobro o pago
        /// </summary>           
        [DataNames("_bncmov_fccbr", "BNCMOV_FCCBR")]
        public DateTime? FechaAplicacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPago >= firstGoodDate)
                    return this.fechaPago;
                else
                    return null;
            }
            set {
                this.fechaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de liberación (transmición al banco, por default es null) 
        /// </summary>           
        [DataNames("_bncmov_fclib", "BNCMOV_FCLIB")]
        public DateTime? FechaBoveda {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaBoveda >= firstGooDate)
                    return this.fechaBoveda;
                return null;
            }
            set {
                this.fechaBoveda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion (default null)
        /// </summary>           
        [DataNames("_bncmov_fccncl", "BNCMOV_FCCNCL")]
        public DateTime? FechaCancela {
            get {
                DateTime firstGooDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGooDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del beneficiario
        /// </summary>
        [DataNames("_bncmov_benf", "BNCMOV_BENF")]
        public string BeneficiarioT {
            get {
                return this.beneficiario;
            }
            set {
                this.beneficiario = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal de contribuyentes del receptor del documento
        /// </summary>           
        [DataNames("_bncmov_rfcr", "BNCMOV_RFCR")]
        public string BeneficiarioRFCT {
            get {
                return this.beneficiarioRFC;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    if (value.Length > 15) {
                        this.beneficiarioRFC = value.Substring(0, 15);
                    } else {
                        this.beneficiarioRFC = value;
                    }
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// codigo de banco según catalogo del SAT
        /// </summary>
        [DataNames("_bncmov_clvbt", "BNCMOV_CLVBT")]
        public string ClaveBancoT {
            get {
                return this.claveBanco;
            }
            set {
                this.claveBanco = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_bncmov_bancot", "BNCMOV_BANCOT")]
        public string BancoT {
            get {
                return this.bancoT;
            }
            set {
                this.bancoT = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el numero de cuenta del beneficiario
        /// </summary>
        [DataNames("_bncmov_numctat", "BNCMOV_NUMCTAT")]
        public string NumeroCuentaT {
            get {
                return this.numeroCuenta;
            }
            set {
                this.numeroCuenta = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer cuenta CLABE del beneficiario
        /// </summary>
        [DataNames("_bncmov_clabet", "BNCMOV_CLABET")]
        public string CuentaCLABET {
            get {
                return this.cuentaCLABE;
            }
            set {
                this.cuentaCLABE = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sucursal del banco receptor
        /// </summary>
        [DataNames("_bncmov_scrslt", "BNCMOV_SCRSLT")]
        public string SucursalT {
            get {
                return this.sucursal;
            }
            set {
                this.sucursal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de forma de pago SAT
        /// </summary>
        [DataNames("_bncmov_clvfrm", "BNCMOV_CLVFRM")]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion de la forma de pago o cobro
        /// </summary>           
        [DataNames("_bncmov_frmpg", "BNCMOV_FRMPG")]
        public string FormaPagoDescripcion {
            get {
                return this.formaPago;
            }
            set {
                this.formaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero del documento
        /// </summary>
        [DataNames("_bncmov_nodocto", "BNCMOV_NODOCTO")]
        public string NumDocto {
            get {
                return this.numDocto;
            }
            set {
                this.numDocto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer CVE_CONCEP
        /// </summary>
        [DataNames("_bncmov_desc", "BNCMOV_DESC")]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia alfanumerica
        /// </summary>           
        [DataNames("_bncmov_ref", "bncmov_ref")]
        public string Referencia {
            get {
                return this.referencia;
            }
            set {
                this.referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de autorizacion bancaria
        /// </summary>           
        [DataNames("_bncmov_numauto", "BNCMOV_NUMAUTO")]
        public string NumAutorizacion {
            get {
                return this.numAutorizacion;
            }
            set {
                this.numAutorizacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de operacion (json)
        /// </summary>
        [DataNames("_bncmov_numope", "BNCMOV_NUMOPE")]
        public string NumOperacion {
            get {
                return this.numOperacion;
            }
            set {
                this.numOperacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// referencia numerica, este dato pertenece al json
        /// </summary>
        [DataNames("_bncmov_refnum", "BNCMOV_REFNUM")]
        public string ReferenciaNumerica {
            get {
                return this.referenciaNumerica;
            }
            set {
                this.referenciaNumerica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio utilizado
        /// </summary>
        [DataNames("_bncmov_tpcmb", "BNCMOV_TPCMB")]
        public decimal TipoCambio {
            get {
                return this.tipoCambio;
            }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el cargo al documento (default 0)
        /// </summary>           
        [DataNames("_bncmov_cargo", "bncmov_cargo")]
        public decimal Cargo {
            get {
                return this.cargo;
            }
            set {
                this.cargo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el abono al documento (default 0)
        /// </summary>           
        [DataNames("_bncmov_abono", "BNCMOV_ABONO")]
        public decimal Abono {
            get {
                return this.abono;
            }
            set {
                this.abono = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de moneda SAT
        /// </summary>
        [DataNames("_bncmov_clvmnd", "BNCMOV_CLVMND")]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nota para este documento
        /// </summary>           
        [DataNames("_bncmov_nota", "BNCMOV_NOTA")]
        public string Nota {
            get {
                return this.notas;
            }
            set {
                this.notas = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario cancela el documento
        /// </summary>           
        [DataNames("_bncmov_usr_c", "bncmov_usr_c")]
        public string Cancela {
            get {
                return this.cancela;
            }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblecer la clave del usuario que autoriza el documento
        /// </summary>           
        [DataNames("_bncmov_usr_a", "BNCMOV_USR_A")]
        public string Autoriza {
            get {
                return this.autoriza;
            }
            set {
                this.autoriza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_bncmov_fn", "BNCMOV_FN")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de la ultima modificacion del registro
        /// </summary>
        [DataNames("_bncmov_fm", "BNCMOV_FM")]
        public DateTime? FechaModifica {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que creo el registro
        /// </summary>
        [DataNames("_bncmov_usr_n", "BNCMOV_USR_N")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_bncmov_usr_m", "BNCMOV_USR_M")]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_bncmov_json", "BNCMOV_JSON")]
        public string InfoAuxiliar {
            get {
                return this.json;
            }
            set {
                this.json = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_bnccmp_idcom", "BNCCMP_IDCOM")]
        public int IdComprobante { get; set; }

        [DataNames("_bnccmp_cargo", "bnccmp_cargo")]
        public decimal Cargo1 { get; set; }

        [DataNames("_bnccmp_abono", "BNCCMP_ABONO")]
        public decimal Abono1 { get; set; }

        [DataNames("_bnccmp_cbrd", "BNCCMP_CBRD")]
        public decimal Acumulado { get; set; }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
