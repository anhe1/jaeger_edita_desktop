﻿using System.ComponentModel;
using System.Linq;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("RMSN", "Almacen PT: Remisionado")]
    public class RemisionDetailModel : RemisionModel, IRemisionModel, IRemisionDetailModel {
        #region declaraciones
        private BindingList<RemisionRelacionadaModel> remisionRelacionadaModels;
        private BindingList<RemisionConceptoDetailModel> _Conceptos;
        private BindingList<RemisionStatusModel> autorizaciones;
        #endregion

        public RemisionDetailModel() : base() {
            this.autorizaciones = new BindingList<RemisionStatusModel>();
            this._Conceptos = new BindingList<RemisionConceptoDetailModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
        }

        /// <summary>
        /// obtener o establecer status de la remision (RemisionStatusEnum)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public RemisionStatusEnum Status {
            get { return (RemisionStatusEnum)this.IdStatus; }
            set { this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener status en modo texto
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((RemisionStatusEnum)this.IdStatus); }
        }

        /// <summary>
        /// obtener estado del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Estado {
            get { if (this.IdStatus > 0)
                    return "Vigente";
                return "Cancelado";
            }
        }

        [SugarColumn(IsIgnore = true)]
        public RemisionFormatoEnum Formato { get; set; }

        [SugarColumn(IsIgnore = true)]
        public new decimal Saldo {
            get { return this.PorCobrar - this.Acumulado; }
        }

        /// <summary>
        /// obtener si el comprobante es editable
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get { return string.IsNullOrEmpty(this.IdDocumento); }
        }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string KeyName {
            get { return string.Format("REM-{0}-{1}-{2}-{3}-{4}", this.EmisorRFC, this.ReceptorRFC, this.IdDocumento, this.Folio.ToString("000000"), this.FechaEmision.ToString("yyyyMMddHHmmss")); }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get {
                var d1 = string.Join("", this.Conceptos.Select(it => it.GetOriginalString));
                var d2 = string.Join("|", new string[] { this.Version, this.Folio.ToString(), this.Serie, this.EmisorRFC, this.ReceptorRFC, this.ReceptorNombre, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("0.00") });
                return "||" + string.Join("|", d2 + d1) + "||"; 
            }
        }

        /// <summary>
        /// lista de remisiones relacionadas
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionRelacionadaModel> RemisionRelacionada {
            get { return this.remisionRelacionadaModels; }
            set { this.remisionRelacionadaModels = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoDetailModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer listado de autorizaciones
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionStatusModel> Autorizaciones {
            get { return this.autorizaciones; }
            set {
                this.autorizaciones = value;
                //this.OnPropertyChanged();
            }
        }

        public void SetValues(RemisionDetailModel source) {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionDetailModel>(source, this);
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.Descuento);
            this.TotalTrasladoIVA = this.Conceptos.Where((RemisionConceptoDetailModel p) => p.Activo == true).Sum((RemisionConceptoDetailModel p) => p.TrasladoIVA);
            this.Total = (this.SubTotal - this.TotalDescuento) + (this.TotalTrasladoIVA + this.TotalTrasladoIEPS) - (this.TotalRetencionIVA + this.TotalRetencionISR);
            this.TotalIVAPactado = (this.SubTotal - this.TotalDescuento) * this.TasaIVAPactado;
            this.GTotal = (this.SubTotal - this.TotalDescuento) + this.TotalIVAPactado;
            // si el factor pactado es cero entonces debemos multiplicar por 1 para que sea el total a cobrar
            this.PorCobrarPactado = this.GTotal * (this.FactorPactado <= 0 ? 1 : this.FactorPactado);
        }
    }
}
     