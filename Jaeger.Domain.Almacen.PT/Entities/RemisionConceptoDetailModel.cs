﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class RemisionConceptoDetailModel : RemisionConceptoModel, IRemisionConceptoDetailModel {
        #region declaraciones
        private BindingList<RemisionConceptoParte> remisionConceptoPartes;
        #endregion

        public RemisionConceptoDetailModel() : base() {
            this.remisionConceptoPartes = new BindingList<RemisionConceptoParte>();
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<RemisionConceptoParte> Partes {
            get { return this.remisionConceptoPartes; }
            set {
                this.remisionConceptoPartes = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum Tipo {
            get { return (TipoComprobanteEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum Efecto {
            get { return (TipoMovimientoEnum)this.IdTipoMovimiento; }
            set {
                this.IdTipoMovimiento = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get { return string.Join("|", new string[] { this.Cantidad.ToString("#0.00"), this.Unidad, this.Descripcion, this.Identificador, this.Unitario.ToString("0.00"), this.Importe.ToString("0.00") }); }
        }

        public new RemisionConceptoDetailModel Clone() {
            var duplicado = (RemisionConceptoDetailModel)base.Clone();
            // concepto parte sin referencia anterior
            return duplicado;
        }
    }
}
     