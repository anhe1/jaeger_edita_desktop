﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class TipoDevolucionModel : BaseSingleModel {
        public TipoDevolucionModel() { }

        public TipoDevolucionModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
