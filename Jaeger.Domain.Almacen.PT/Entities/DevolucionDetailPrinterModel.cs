﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class DevolucionDetailPrinterModel : DevolucionDetailModel {
        public DevolucionDetailPrinterModel() : base() { }

        public DevolucionDetailPrinterModel(DevolucionDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<DevolucionDetailModel, DevolucionDetailPrinterModel>(source, this);
            this.Conceptos = source.Conceptos;
        }

        public string TipoText {
            get { return base.TipoComprobante.Description(); }
        }

        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdDocumento,
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&rfce=&rfcr=", this.ReceptorRFC, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Total.ToString("0.00") };
            }
        }
    }
}
