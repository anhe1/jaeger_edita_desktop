﻿using Jaeger.Domain.Almacen.Abstractions;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class DocumentoAlmacenModel : ComprobanteAlmacen, IComprobanteAlmacen {
        #region declaraciones
        private bool _ReasonRequired = false;
        private int _IdTipo;
        private bool _ConValores = false;
        private bool _RequerierePedido = false;
        #endregion

        public DocumentoAlmacenModel() { }

        /// <summary>
        /// obtener o establecer indice del documento
        /// </summary>
        public new int IdDocumento {
            get { return base.IdDocumento; }
            set {
                base.IdDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el objeto es de solo lectura
        /// </summary>
        public new bool IsReadOnly {
            get { return base.IsReadOnly; }
            set {
                base.IsReadOnly = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el documento debe afectar los saldos
        /// </summary>
        public new bool IsSum {
            get { return base.IsSum; }
            set {
                base.IsSum = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si es necesario clave de motivo requerido
        /// </summary>
        public bool ClaveMotivo {
            get { return this._ReasonRequired; }
            set { this._ReasonRequired = value;
                this.OnPropertyChanged();
            }
        }

        public bool ConValores {
            get { return this._ConValores; }
            set { this._ConValores = value;
                this.OnPropertyChanged();
            }
        }

        public bool RequerierePedido {
            get { return this._RequerierePedido; }
            set {
                this._RequerierePedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de efecto 1 = Ingreso, 2 = Egreso
        /// </summary>
        public int IdTipo {
            get { return this._IdTipo; }
            set {
                this._IdTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripcion del tipo de operacion
        /// </summary>
        public new string Descripcion {
            get { return base.Descripcion; }
            set {
                base.Descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el formato de impresion del documento
        /// </summary>
        public new string FormatoImpreso {
            get { return base.FormatoImpreso; }
            set {
                base.FormatoImpreso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la secuencia de tipos de objetos utilizados como receptores del tipo de movimiento
        /// </summary>
        public new string Receptores {
            get { return base.Receptores; }
            set {
                base.Receptores = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener el descriptor del tipo de documento {Clave:Descripcion}
        /// </summary>
        public override string Descriptor {
            get { return string.Format("{0}: {1}", this.IdDocumento.ToString("00"), this.Descripcion); }
        }
    }
}
