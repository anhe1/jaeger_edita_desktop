﻿using System.Collections.Generic;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionDetailReportePrinter {
        public List<RemisionDetailModel> Conceptos { get; set; }
    }
}
