﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    /// <summary>
    /// modelo de status de remision
    /// </summary>
    [SugarTable("RMSNS")]
    public class RemisionStatusModel : Base.Abstractions.BasePropertyChangeImplementation, IRemisionStatusModel {
        #region declaraciones
        private int _Index;
        private bool _Activo;
        private int _IdRemision;
        private int _IdCliente;
        private int _IdStatusA;
        private int _IdStatusB;
        private string _CvMotivo;
        private string _Nota;
        private DateTime _FechaNuevo;
        private string _User;
        #endregion

        public RemisionStatusModel() : base() {
            this._Activo = true;
        }

        /// <summary>
        /// obtener o estableecr indice 
        /// </summary>
        [DataNames("RMSNS_ID")]
        [SugarColumn(ColumnName = "RMSNS_ID", ColumnDescription = "", IsNullable = false, IsIdentity = true, IsPrimaryKey = true)]
        public int IdAuto {
            get { return this._Index; }
            set {
                this._Index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("RMSNS_A")]
        [SugarColumn(ColumnName = "RMSNS_A", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con remisiones
        /// </summary>
        [DataNames("RMSNS_RMSN_ID")]
        [SugarColumn(ColumnName = "RMSNS_RMSN_ID", DefaultValue = "1")]
        public int IdRemision {
            get { return this._IdRemision; }
            set {
                this._IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de clientes
        /// </summary>
        [DataNames("RMSNS_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSNS_DRCTR_ID", ColumnDescription = "indice de la tabla de clientes")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSNS_RMSN_STTS_ID")]
        [SugarColumn(ColumnName = "RMSNS_RMSN_STTS_ID", ColumnDescription = "status anterior")]
        public int IdStatusA {
            get { return this._IdStatusA; }
            set {
                this._IdStatusA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSNS_RMSN_STTSA_ID")]
        [SugarColumn(ColumnName = "RMSNS_RMSN_STTSA_ID", ColumnDescription = "status autorizado")]
        public int IdStatusB {
            get { return this._IdStatusB; }
            set {
                this._IdStatusB = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del motivo del cambio de status
        /// </summary>
        [DataNames("RMSNS_CVMTV")]
        [SugarColumn(ColumnName = "RMSNS_CVMTV", ColumnDescription = "clave del motivo del cambio de status")]
        public string CvMotivo {
            get { return this._CvMotivo; }
            set {
                this._CvMotivo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones del registro
        /// </summary>
        [DataNames("RMSNS_NOTA")]
        [SugarColumn(ColumnName = "RMSNS_NOTA", ColumnDescription = "observaciones")]
        public string Nota {
            get { return this._Nota; }
            set {
                this._Nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estalecer fecha de creacion
        /// </summary>
        [DataNames("RMSNS_FN")]
        [SugarColumn(ColumnName = "RMSNS_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer usuario
        /// </summary>
        [DataNames("RMSNS_USR_N")]
        [SugarColumn(ColumnName = "RMSNS_USR_N", ColumnDescription = "clave del usuario que autoriza el cambio")]
        public string User {
            get { return this._User; }
            set {
                this._User = value;
                this.OnPropertyChanged();
            }
        }
    }
}
