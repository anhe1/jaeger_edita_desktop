﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("RMSN", "Almacen PT: Remisionado")]
    public class RemisionModel : BasePropertyChangeImplementation, IRemisionModel, IUploadPDF, Base.Contracts.ICancelacion {
        #region declaraciones
        private int idRemision;
        private bool activo;
        private int precisionDecimal;
        private int idCliente;
        private int idPedido;
        private int folio;
        private int idVendedor;
        private int idStatus;
        private int idTipoDocumento;
        private DateTime fechaEmision;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltPago;
        private DateTime? fechaModifica;
        private DateTime? fechaCobranza;
        private DateTime fechaNuevo;
        private DateTime? fechaVence;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string receptorRFC;
        private string receptorNombre;
        private decimal tipoCambio;
        private decimal trasladoIVA;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal _porCobrar;
        private decimal acumulado;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveMoneda;
        private string vendedor;
        private string creo;
        private string modifica;
        private string nota;
        private string contacto;
        private int idMetodoEnvio;
        private int idDomicilio;
        private int idNumeroGuia;
        private int idSerie;
        private int idCatalogo;
        private decimal factorPactado;
        private int idRelacion;
        private string cancela;
        private decimal _TasaIVA;
        private string _ReceptorClave;
        private decimal _Saldo;
        private decimal _TotalIVAPactado;
        private decimal _GTotal;
        private decimal _PorCobrarPactado;
        private string _DescuentoTipo;
        private decimal _DescuentoFactor;
        private decimal _DescuentoTotal;
        private string _CondicionPago;
        private string _DomicilioEntrega;
        private decimal _TotalRetencionISR;
        private decimal _TotalRetencionIVA;
        private decimal _TotalRetencionIEPS;
        private decimal _TotalTrasladoIEPS;
        private string tipoComprobanteText;
        private string _urlPDF;
        private int _ClaveCancela;
        private string _NotaCancelacion;
        private string _MotivoCancelacion;
        private int _Periodo;
        private int _Ejercicio;
        #endregion

        public RemisionModel() {
            this.Version = "2.0";
            this.Activo = true;
            this.IdStatus = 1;
            this.PrecisionDecimal = 2;
            this.idTipoDocumento = 26;
            this.FechaNuevo = DateTime.Now;
            this.FechaEmision = DateTime.Now;
            this.claveMoneda = "MXN";
            this.IdMetodoEnvio = 1;
        }

        #region datos generales del comprobante
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        [DataNames("_rmsn_id", "RMSN_ID")]
        [SugarColumn(ColumnName = "RMSN_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRemision {
            get { return this.idRemision; }
            set {
                this.idRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro esta activo
        /// </summary>
        [DataNames("_rmsn_a", "RMSN_A")]
        [SugarColumn(ColumnName = "RMSN_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        [DataNames("_rmsn_ctdoc_id", "RMSN_CTDOC_ID")]
        [SugarColumn(ColumnName = "RMSN_CTDOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoDocumento {
            get {
                return this.idTipoDocumento;
            }
            set {
                this.idTipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_rmsn_efecto", "RMSN_EFECTO")]
        [SugarColumn(ColumnName = "RMSN_EFECTO", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true, IsIgnore = true)]
        public string TipoComprobante {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("_rmsn_ver", "RMSN_VER")]
        [SugarColumn(ColumnName = "RMSN_VER", ColumnDescription = "version del comprobante", Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// precision decimal
        /// </summary>
        [DataNames("_rmsn_deci", "RMSN_DECI")]
        [SugarColumn(ColumnName = "RMSN_DECI", ColumnDescription = "decimales", Length = 4)]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("_rmsn_stts_id", "RMSN_STTS_ID")]
        [SugarColumn(ColumnName = "RMSN_STTS_ID", ColumnDescription = "indice del status del comprobante")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("_rmsn_folio", "RMSN_FOLIO")]
        [SugarColumn(ColumnName = "RMSN_FOLIO", ColumnDescription = "folio de control interno", IsNullable = false)]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        [DataNames("rmsn_ctsr_id", "RMSN_CTSR_ID")]
        [SugarColumn(ColumnName = "RMSN_CTSR_ID", ColumnDescription = "indice de la tabla de series", IsNullable = true)]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("_rmsn_serie", "RMSN_SERIE")]
        [SugarColumn(ColumnName = "RMSN_SERIE", ColumnDescription = "serie de control interno del documento en modo texto", Length = 10, IsNullable = true)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o numero de pedido de control interno
        /// </summary>
        [DataNames("_rmsn_pdd_id", "RMSN_PDD_ID")]
        [SugarColumn(ColumnName = "RMSN_PDD_ID", ColumnDescription = "indice o numero de pedido del cliente, control interno", IsNullable = true)]
        public int IdPedido {
            get { return this.idPedido; }
            set {
                this.idPedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("_rmsn_ctcls_id", "RMSN_CTCLS_ID")]
        [SugarColumn(ColumnName = "RMSN_CTCLS_ID", ColumnDescription = "indice del catalogo de productos", IsNullable = true)]
        public int IdCatalogo {
            get { return this.idCatalogo; }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del metodo de envio (RMSN_MTDENV_ID)
        /// </summary>
        [DataNames("_rmsn_ctenv_id", "RMSN_CTENV_ID")]
        [SugarColumn(ColumnName = "RMSN_CTENV_ID", ColumnDescription = "indice de la tabla del metodo de envio", IsNullable = true)]
        public int IdMetodoEnvio {
            get { return this.idMetodoEnvio; }
            set {
                this.idMetodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("_rmsn_drctr_id", "RMSN_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSN_DRCTR_ID", ColumnDescription = "indice del direcotorio", IsNullable = true)]
        public int IdCliente {
            get { return this.idCliente; }
            set {
                this.idCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        [DataNames("_rmsn_drccn_id", "RMSN_DRCCN_ID")]
        [SugarColumn(ColumnName = "RMSN_DRCCN_ID", ColumnDescription = "indice del domicilio del directorio")]
        public int IdDomicilio {
            get { return this.idDomicilio; }
            set {
                this.idDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("_rmsn_vnddr_id", "RMSN_VNDDR_ID")]
        [SugarColumn(ColumnName = "RMSN_VNDDR_ID", ColumnDescription = "indice de la tabla de vendedores")]
        public int IdVendedor {
            get { return this.idVendedor; }
            set {
                this.idVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        [DataNames("_rmsn_guia_id", "RMSN_GUIA_ID")]
        [SugarColumn(ColumnName = "RMSN_GUIA_ID", ColumnDescription = "numero de guia o referencia del metodo de envio", IsNullable = true)]
        public int NoGuia {
            get { return this.idNumeroGuia; }
            set {
                this.idNumeroGuia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("_rmsn_ctrel_id", "RMSN_CTREL_ID")]
        [SugarColumn(ColumnName = "RMSN_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        [DataNames("_rmsn_rfce", "RMSN_RFCE")]
        [SugarColumn(ColumnName = "RMSN_RFCE", ColumnDescription = "registro federal de contribuyentes del emisor del comprobante", Length = 14)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("_rmsn_clvr", "RMSN_CLVR")]
        [SugarColumn(ColumnName = "RMSN_CLVR", ColumnDescription = "clave de control interno de cliente", Length = 10)]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("_rmsn_rfcr", "RMSN_RFCR")]
        [SugarColumn(ColumnName = "RMSN_RFCR", ColumnDescription = "registro federal de contribuyentes del cliente o receptor", Length = 14)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("_rmsn_nomr", "RMSN_NOMR")]
        [SugarColumn(ColumnName = "RMSN_NOMR", ColumnDescription = "nombre o razon social del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("_rmsn_cntct", "RMSN_CNTCT")]
        [SugarColumn(ColumnName = "RMSN_CNTCT", ColumnDescription = "nombre de la persona que recibe", Length = 255)]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("_rmsn_uuid", "RMSN_UUID")]
        [SugarColumn(ColumnName = "RMSN_UUID", ColumnDescription = "id de documento", Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("_rmsn_fecems", "RMSN_FECEMS")]
        [SugarColumn(ColumnName = "RMSN_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega (RMSN_FECENT)
        /// </summary>
        [DataNames("_rmsn_fecent", "RMSN_FECENT")]
        [SugarColumn(ColumnName = "RMSN_FECENT", ColumnDescription = "fecha de entrega del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de recepcion de cobranza (RMSN_FECCBR)
        /// </summary>
        [DataNames("_rmsn_feccbr", "RMSN_FECCBR")]
        [SugarColumn(ColumnName = "RMSN_FECCBR", ColumnDescription = "fecha de recepcion de cobranza", IsNullable = true)]
        public DateTime? FechaCobranza {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCobranza >= firstGoodDate)
                    return this.fechaCobranza;
                return null;
            }
            set {
                this.fechaCobranza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        [DataNames("_rmsn_fecupc", "RMSN_FECUPC")]
        [SugarColumn(ColumnName = "RMSN_FECUPC", ColumnDescription = "fecha del ultimo pago o cobro del comprobante", IsNullable = true)]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPago >= firstGoodDate)
                    return this.fechaUltPago;
                return null;
            }
            set {
                this.fechaUltPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento del pagare
        /// </summary>
        [DataNames("_rmsn_fecvnc", "RMSN_FECVNC")]
        [SugarColumn(ColumnName = "RMSN_FECVNC", ColumnDescription = "fecha de vencimiento del pagare", IsNullable = true)]
        public DateTime? FechaVence {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaVence >= firstGoodDate)
                    return this.fechaVence;
                return null;
            }
            set {
                this.fechaVence = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        [DataNames("_rmsn_tpcmb", "RMSN_TPCMB")]
        [SugarColumn(ColumnName = "RMSN_TPCMB", ColumnDescription = "tipo de cambio", DefaultValue = "1", IsNullable = true)]
        public decimal TipoCambio {
            get { return this.tipoCambio; }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        [DataNames("_rmsn_sbttl", "RMSN_SBTTL")]
        [SugarColumn(ColumnName = "RMSN_SBTTL", ColumnDescription = "subtotal", DefaultValue = "0")]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento (suma de los descuentos aplicados en las partidas de la remision)
        /// </summary>
        [DataNames("_rmsn_dscnt", "RMSN_DSCNT")]
        [SugarColumn(ColumnName = "RMSN_DSCNT", ColumnDescription = "importe del descuento (suma de los descuentos aplicados en las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalDescuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establer el importe de la remision antes de impuestos (SubToal - TotalDescuentos)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal Importe {
            get { return (this.SubTotal - this.TotalDescuento); }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA (suma del iva de las partidas de la remision)
        /// </summary>
        [DataNames("_rmsn_trsiva", "RMSN_TRSIVA")]
        [SugarColumn(ColumnName = "RMSN_TRSIVA", ColumnDescription = "total del impuesto trasladado IVA (suma del iva de las partidas de la remision)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de retencion del impuesto ISR
        /// </summary>
        [DataNames("_rmsn_retisr", "RMSN_RETISR")]
        [SugarColumn(ColumnName = "RMSN_RETISR", ColumnDescription = "total de retencion del impuesto ISR", Length = 14, DecimalDigits = 4, DefaultValue = "0", IsIgnore = true)]
        public decimal TotalRetencionISR {
            get { return this._TotalRetencionISR; }
            set {
                this._TotalRetencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IVA
        /// </summary>
        [DataNames("_rmsn_retiva", "RMSN_RETIVA")]
        [SugarColumn(ColumnName = "RMSN_RETIVA", ColumnDescription = "total del impuesto retenido IVA", Length = 14, DecimalDigits = 4, DefaultValue = "0", IsIgnore = true)]
        public decimal TotalRetencionIVA {
            get { return this._TotalRetencionIVA; }
            set {
                this._TotalRetencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto retenido IEPS
        /// </summary>
        [DataNames("_rmsn_retieps", "RMSN_RETIEPS")]
        [SugarColumn(ColumnName = "RMSN_RETIEPS", ColumnDescription = "total del impuesto retenido IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0", IsIgnore = true)]
        public decimal TotalRetencionIEPS {
            get { return this._TotalRetencionIEPS; }
            set {
                this._TotalRetencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto traslado IEPS
        /// </summary>
        [DataNames("_rmsn_trsieps", "RMSN_TRSIEPS")]
        [SugarColumn(ColumnName = "RMSN_TRSIEPS", ColumnDescription = "total del impuesto traslado IEPS", Length = 14, DecimalDigits = 4, DefaultValue = "0", IsIgnore = true)]
        public decimal TotalTrasladoIEPS {
            get { return this._TotalTrasladoIEPS; }
            set {
                _TotalTrasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("_rmsn_gtotal", "RMSN_GTOTAL")]
        [SugarColumn(ColumnName = "RMSN_GTOTAL", ColumnDescription = "total del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Total {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cobranza
        /// <summary>
        /// obtener o establecer la tasa del iva pactado con el cliente
        /// </summary>
        [DataNames("_rmsn_faciva", "RMSN_FACIVA")]
        [SugarColumn(ColumnName = "RMSN_FACIVA", ColumnDescription = "tasa del iva pactado con el cliente", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TasaIVAPactado {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del iva pactado con el cliente (RMSN_IVAPAC)
        /// </summary>
        [DataNames("_rmsn_ivapac", "RMSN_IVAPAC")]
        [SugarColumn(ColumnName = "RMSN_IVAPAC", ColumnDescription = "total del iva pactado con el cliente (RMSN_IVAPAC)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalIVAPactado {
            get { return this._TotalIVAPactado; }
            set {
                this._TotalIVAPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total despues del iva pactado (SubTotal + TotalIVAPactado)
        /// </summary>
        [DataNames("_rmsn_total", "RMSN_TOTAL")]
        [SugarColumn(ColumnName = "RMSN_TOTAL", ColumnDescription = "total despues del iva pactado (SubTotal + TotalIVAPactado)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal GTotal {
            get { return this._GTotal; }
            set {
                this._GTotal = value;
                this.OnPropertyChanged();
            }
        }
        /// <summary>
        /// obtener o establecer el valor del factor pactado para el descuento
        /// </summary>
        [DataNames("_rmsn_facpac", "RMSN_FACPAC")]
        [SugarColumn(ColumnName = "RMSN_FACPAC", ColumnDescription = "valor del factor pactado para el descuento", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal FactorPactado {
            get { return this.factorPactado; }
            set {
                this.factorPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del importe por cobrar pactado (RMSN_XCBPAC)
        /// </summary>
        [DataNames("_rmsn_xcbpac", "RMSN_XCBPAC")]
        [SugarColumn(ColumnName = "RMSN_XCBPAC", ColumnDescription = "total del importe por cobrar pactado (RMSN_XCBPAC)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal PorCobrarPactado {
            get { return this._PorCobrarPactado; }
            set {
                this._PorCobrarPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de descuento aplicado
        /// </summary>
        [DataNames("_rmsn_desc", "RMSN_DESC")]
        [SugarColumn(ColumnName = "RMSN_DESC", ColumnDescription = "tipo de descuento aplicado", Length = 100)]
        public string DescuentoTipo {
            get { return this._DescuentoTipo; }
            set {
                this._DescuentoTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del descuento aplicado
        /// </summary>
        [DataNames("_rmsn_facdes", "RMSN_FACDES")]
        [SugarColumn(ColumnName = "RMSN_FACDES", ColumnDescription = "factor del descuento aplicado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal DescuentoFactor {
            get { return this._DescuentoFactor; }
            set {
                this._DescuentoFactor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total del descuento aplicado (GTotal * DescuentoFactor)
        /// </summary>
        [DataNames("_rmsn_desct", "RMSN_DESCT")]
        [SugarColumn(ColumnName = "RMSN_DESCT", ColumnDescription = "importe total del descuento aplicado (GTotal * DescuentoFactor)", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal DescuentoTotal {
            get { return this._DescuentoTotal; }
            set {
                this._DescuentoTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe por cobrar del comprobante
        /// </summary>
        [DataNames("_rmsn_xcbrr", "RMSN_XCBRR")]
        [SugarColumn(ColumnName = "RMSN_XCBRR", ColumnDescription = " importe por cobrar del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal PorCobrar {
            get { return this._porCobrar; }
            set {
                this._porCobrar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acumulado de la cobranza del comprobante
        /// </summary>
        [DataNames("_rmsn_cbrd", "RMSN_CBRD")]
        [SugarColumn(ColumnName = "RMSN_CBRD", ColumnDescription = "acumulado de la cobranza del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get { return this.acumulado; }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el saldo por cobrar del comprobante
        /// </summary>
        [DataNames("_rmsn_saldo", "RMSN_SALDO")]
        [SugarColumn(ColumnName = "RMSN_SALDO", ColumnDescription = "saldo por cobrar del comprobante", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Saldo {
            get { return this._Saldo; }
            set {
                this._Saldo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region cancelacion
        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_rmsn_fccncl", "RMSN_FCCNCL")]
        [SugarColumn(ColumnName = "RMSN_FCCNCL", ColumnDescription = "fecha de cancelacion del comprobante", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        [DataNames("_rmsn_usr_c", "RMSN_USR_C")]
        [SugarColumn(ColumnName = "RMSN_USR_C", ColumnDescription = "clave del usuario que cancela el comprobante", Length = 10, IsNullable = true)]
        public string Cancela {
            get { return this.cancela; }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSN_CVCAN")]
        [SugarColumn(ColumnName = "RMSN_CVCAN", ColumnDescription = "clave de cancelacion", IsIgnore = true)]
        public int ClaveCancela {
            get { return this._ClaveCancela; }
            set {
                this._ClaveCancela = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSN_CLMTV")]
        [SugarColumn(ColumnName = "RMSDP_CLMTV", ColumnDescription = "clave del tipo de cancelacion", Length = 100, IsOnlyIgnoreInsert = true, IsNullable = true, IsIgnore = true)]
        public string ClaveCancelacion {
            get => this._MotivoCancelacion;
            set {
                this._MotivoCancelacion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSN_CLNTA")]
        [SugarColumn(ColumnName = "RMSN_CLNTA", ColumnDescription = "nota para la cancelacion", IsIgnore = true)]
        public string NotaCancelacion {
            get {
                return this._NotaCancelacion;
            }
            set {
                this._NotaCancelacion = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region condiciones y datos 
        /// <summary>
        /// obtener o establecer clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_rmsn_moneda", "RMSN_MONEDA")]
        [SugarColumn(ColumnName = "RMSN_MONEDA", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_rmsn_mtdpg", "RMSN_MTDPG")]
        [SugarColumn(ColumnName = "RMSN_MTDPG", ColumnDescription = "clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_rmsn_frmpg", "RMSN_FRMPG")]
        [SugarColumn(ColumnName = "RMSN_FRMPG", ColumnDescription = "clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer las condiciones de pago (RMSN_CNDNS)
        /// </summary>
        [DataNames("_rmsn_cndns", "RMSN_CNDNS")]
        [SugarColumn(ColumnName = "RMSN_CNDNS", ColumnDescription = "condicion de pago", Length = 30)]
        public string CondicionPago {
            get { return this._CondicionPago; }
            set {
                this._CondicionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la dirección de entrega en modo texto (RMSN_DRCCN)
        /// </summary>
        [DataNames("_rmsn_drccn", "RMSN_DRCCN")]
        [SugarColumn(ColumnName = "RMSN_DRCCN", ColumnDescription = "dirección de entrega en modo texto", Length = 400)]
        public string DomicilioEntrega {
            get { return this._DomicilioEntrega; }
            set {
                this._DomicilioEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer una nota
        /// </summary>
        [DataNames("_rmsn_nota", "RMSN_NOTA")]
        [SugarColumn(ColumnName = "RMSN_NOTA", ColumnDescription = "observaciones", Length = 255, IsNullable = true)]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        [DataNames("_rmsn_vnddr", "RMSN_VNDDR")]
        [SugarColumn(ColumnName = "RMSN_VNDDR", ColumnDescription = "clave del vendedor asociado", Length = 10, IsNullable = true)]
        public string Vendedor {
            get { return this.vendedor; }
            set {
                this.vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_rmsn_url_pdf", "RMSN_URL_PDF")]
        [SugarColumn(ColumnName = "RMSN_URL_PDF", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("_rmsn_fn", "RMSN_FN")]
        [SugarColumn(ColumnName = "RMSN_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario creador del registro
        /// </summary>
        [DataNames("_rmsn_usr_n", "RMSN_USR_N")]
        [SugarColumn(ColumnName = "RMSN_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_rmsn_fm", "RMSN_FM")]
        [SugarColumn(ColumnName = "RMSN_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreUpdate = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica;
                return null;
            }
            set {
                this.fechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        [DataNames("_rmsn_usr_m", "RMSN_USR_M")]
        [SugarColumn(ColumnName = "RMSN_USR_M", ColumnDescription = "clave del ultimo usuario que modifica", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_rmsn_anio", "RMSN_ANIO")]
        [SugarColumn(ColumnName = "RMSN_ANIO", ColumnDescription = "ejercicio al que pertenece el comprobante", IsNullable = true, IsOnlyIgnoreInsert = true, IsOnlyIgnoreUpdate = true)]
        public int Ejercicio {
            get { return this._Ejercicio; }
            set {
                this._Ejercicio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_rmsn_mes", "RMSN_MES")]
        [SugarColumn(ColumnName = "RMSN_MES", ColumnDescription = "periodo al que pertenece el comprobante", IsNullable = true, IsOnlyIgnoreInsert = true, IsOnlyIgnoreUpdate = true)]
        public int Periodo {
            get { return this._Periodo; }
            set {
                this._Periodo = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region dias para la cobranza
        /// <summary>
        /// obtener o establecer dias transcurridos desde el ultimo cobro (CTLRMS_DSTRS solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRS")]
        [SugarColumn(ColumnName = "", ColumnDescription = "", IsIgnore = true)]
        public int DiasUltCobro {
            get {
                if (!(this.FechaUltPago is null) && !(this.FechaEntrega is null))
                    return (this.FechaUltPago - this.FechaEntrega).Value.Days;
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer dias transcurridos desde la fecha de entrega (CTLRMS_DSTRSA solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRSA")]
        [SugarColumn(ColumnName = "", ColumnDescription = "", IsIgnore = true)]
        public int DiasTranscurridos {
            get {
                if (this.FechaEntrega != null)
                    if (this.IdStatus == 4)
                        return DateTime.Now.Subtract(this.FechaEntrega.Value).Days + 1;
                    else if (this.IdStatus > 4)
                        if (this.FechaUltPago != null)
                            return this.FechaUltPago.Value.Subtract(this.FechaEntrega.Value).Days + 1;
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer los dias transcurridos desde la fecha de cobranza cuando el status es 4, cuando es mayor entonces se utiliza la fecha del ultimo pago
        /// </summary>
        [SugarColumn(ColumnName = "", ColumnDescription = "", IsIgnore = true)]
        public int DiasCobranza {
            get {
                if (this.FechaCobranza != null) {
                    if (this.IdStatus == 4)
                        return DateTime.Now.Subtract(this.FechaCobranza.Value).Days + 1;
                    else if (this.IdStatus > 4) {
                        if (this.FechaUltPago != null)
                            return this.FechaUltPago.Value.Subtract(this.FechaCobranza.Value).Days + 1;
                    }
                }
                return 0;
            }
        }

        [SugarColumn(ColumnName = "", ColumnDescription = "", IsIgnore = true)]
        public object Tag {
            get; set;
        }
        #endregion
    }
}
