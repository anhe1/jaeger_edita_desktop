﻿using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionDetailPrinter : RemisionDetailModel, Base.Contracts.IPrinter {
        #region declaraciones
        private bool _ShowPagare = false;
        private string _Pagare;
        private string _Leyenda;
        #endregion

        public RemisionDetailPrinter() : base() {
            this.IsValuesVisible = false;
        }

        public RemisionDetailPrinter(RemisionDetailModel source) : base() {
            this.IsValuesVisible = false;
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionDetailPrinter>(source, this);
        }

        public bool ShowPagare {
            get { return this._ShowPagare; }
            set { this._ShowPagare = value;
                this.OnPropertyChanged();
            }
        }

        public string TotalEnLetra {
            get { return NumeroALetras.Convertir((double)this.Total, 1); }
        }

        public string Leyenda1 {
            get { return this._Leyenda; }
            set { this._Leyenda = value; }
        }

        public string Leyenda2 {
            get { return "Favor de revisar su material al recibirlo, porque no se aceptan devoluciones de material maltratado despúes de 8 días. (Revisar empaque, material en buenas condiciones, checar que los choferes que no azonten los paquetes y los entreguen en buen estado)."; }
        }

        public string Leyenda3 {
            get { return "Realice su pago en una de nuestras cuentas Alejandro Reyes Liñan Santander Cta. 18131402 Suc. 015 Clabe: 014180606204545338. Anotando en la referencia alfanumérica su clave de cliente «Expr» y su número (s) de remisión. Comuníquese con nosotros a los teléfonos 5115-5320 al 27 ext. 114 o mándenos un correo adm.cobranza@ipo.com.mx "; }
        }

        public string Leyenda4 {
            get { return "El Cliente firma de conformidad con los materiales descritos en esta remisión, cuenta con cinco días para cualquier aclaración. En mercancía de temporada no se aceptan devoluciones."; }
        }

        public string Pagare {
            get { return this._Pagare; }
            set { this._Pagare = value; }
        }

        public bool IsValues​Visible { get; set; }
        public string ReportName {
            get { return ""; }
        }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "&identificador=", this.IdDocumento,
                    (this.Folio.ToString("000000") == null ? "" : "&folio=" + this.Folio.ToString()),
                    "&rfce=", this.EmisorRFC, "&rfcr=", this.ReceptorRFC, "&fecha=", this.FechaEmision.ToShortDateString(), "&total=", this.Importe.ToString("0.00") };
            }
        }
    }
}
