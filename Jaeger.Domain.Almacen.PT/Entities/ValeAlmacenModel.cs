﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("ALMPT", "ALMPT: movimientos")]
    public class ValeAlmacenModel : BasePropertyChangeImplementation, IValeAlmacenModel {
        #region declaraciones
        private int _IdVale;
        private int _IdTipoComprobante;
        private int _IdAlmacen;
        private int _IdStatus;
        private int _IdDirectorio;
        private int _IdEmisor;
        private string _Version;
        private DateTime _FechaEmision;
        private DateTime? _FechaIngreso;
        private int _Folio;
        private string _Serie;
        private decimal _TotalDescuento;
        private decimal _TotalTrasladoIVA;
        private decimal _SubTotal;
        private decimal _Total;
        private string _Entrega;
        private string _Observaciones;
        private DateTime _FechaNuevo;
        private string _Creo;
        private DateTime? _FechaModifica;
        private string _Modifica;
        private string _Receptor;
        private int idRelacion;
        private string _Referencia;
        private int _IdVendedor;
        private string _Vendedor;
        private string idDocumento;
        private int _IdTipoMovmiento;
        private string _urlPDF;
        private string _ReceptorRFC;
        private int _IdTipoAlmacen;
        private int _IdCveDevolucion;
        #endregion

        public ValeAlmacenModel() {
            this._FechaNuevo = DateTime.Now;
            this._FechaEmision = DateTime.Now;
            this._IdAlmacen = 2;
            this._IdStatus = 1;
            this._Version = "2.0";
        }

        /// <summary>
        /// obtener o establecer indice (ALMPT_ID)
        /// </summary>
        [DataNames("ALMPT_ID")]
        [SugarColumn(ColumnName = "ALMPT_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdComprobante {
            get { return this._IdVale; }
            set {
                this._IdVale = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del almacen
        /// </summary>
        [DataNames("ALMPT_CTALM_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTALM_ID", ColumnDescription = "identificador del almacen")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer incide de tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        [DataNames("ALMPT_TPALM_ID")]
        [SugarColumn(ColumnName = "ALMPT_TPALM_ID", ColumnDescription = "incide de tipo de almacen (MP, PT, DP, PP)")]
        public int IdTipoAlmacen {
            get { return this._IdTipoAlmacen; }
            set {
                this._IdTipoAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del documento
        /// </summary>
        [DataNames("ALMPT_VER")]
        [SugarColumn(ColumnName = "ALMPT_VER", ColumnDescription = "version del comprobante", Length = 3, DefaultValue = "2.0")]
        public string Version {
            get { return this._Version; }
            set {
                this._Version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del tipo de documento
        /// </summary>
        [DataNames("ALMPT_DOC_ID")]
        [SugarColumn(ColumnName = "ALMPT_DOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoComprobante {
            get { return this._IdTipoComprobante; }
            set {
                this._IdTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer efecto del comprobante
        /// </summary>
        [DataNames("ALMPT_CTEFC_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTEFC_ID", ColumnDescription = "Efecto del comprobante")]
        public int IdTipoMovimiento {
            get { return this._IdTipoMovmiento; }
            set {
                this._IdTipoMovmiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtenero o establcer indice del tipo de devolucion
        /// </summary>
        [DataNames("ALMPT_CTDEV_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTDEV_ID", ColumnDescription = "indice del tipo de devolucion")]
        public int IdCveDevolucion {
            get { return this._IdCveDevolucion; }
            set {
                this._IdCveDevolucion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        [DataNames("ALMPT_STTS_ID")]
        [SugarColumn(ColumnName = "ALMPT_STTS_ID", ColumnDescription = "indice del catalogo de status")]
        public int IdStatus {
            get { return this._IdStatus; }
            set {
                this._IdStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("ALMPT_UUID")]
        [SugarColumn(ColumnName = "ALMPT_UUID", ColumnDescription = "id de documento", Length = 36, IsNullable = true)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("ALMPT_CTREL_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTREL_ID", ColumnDescription = "indice o clave de relacion con otros comprobantes")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("ALMPT_DRCTR_ID")]
        [SugarColumn(ColumnName = "ALMPT_DRCTR_ID", ColumnDescription = "indice del directorio de clientes", IsNullable = true)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set {
                this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento
        /// </summary>
        [DataNames("ALMPT_CTDEP_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTDEP_ID", ColumnDescription = "indice del departamento", IsNullable = true)]
        public int IdDepartamento {
            get { return this._IdEmisor; }
            set {
                this._IdEmisor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        [DataNames("ALMPT_FOLIO")]
        [SugarColumn(ColumnName = "ALMPT_FOLIO", ColumnDescription = "folio de control interno", IsNullable = false)]
        public int Folio {
            get { return this._Folio; }
            set {
                this._Folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de serie
        /// </summary>
        [DataNames("ALMPT_CTSR_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTSR_ID", ColumnDescription = "serie de control interno", IsNullable = true)]
        public int IdSerie { get; set; }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>
        [DataNames("ALMPT_SERIE")]
        [SugarColumn(ColumnName = "ALMPT_SERIE", ColumnDescription = "serie de control interno", IsNullable = true)]
        public string Serie {
            get { return this._Serie; }
            set {
                this._Serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>
        [DataNames("ALMPT_FECEMS")]
        [SugarColumn(ColumnName = "ALMPT_FECEMS", ColumnDescription = "fecha de emision del comprobante")]
        public DateTime FechaEmision {
            get { return this._FechaEmision; }
            set {
                this._FechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del receptor
        /// </summary>
        [DataNames("ALMPT_RFCR")]
        [SugarColumn(ColumnName = "ALMPT_RFCR", ColumnDescription = "nombre o razon social del receptor", Length = 255)]
        public string ReceptorRFC {
            get { return this._ReceptorRFC; }
            set {
                this._ReceptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o denominacion del receptor
        /// </summary>
        [DataNames("ALMPT_NOM")]
        [SugarColumn(ColumnName = "ALMPT_NOM", ColumnDescription = "nombre o razon social del receptor", Length = 255)]
        public string Receptor {
            get { return this._Receptor; }
            set {
                this._Receptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("ALMPT_CNTCT")]
        [SugarColumn(ColumnName = "ALMPT_CNTCT", ColumnDescription = "nombre del contacto", Length = 100, IsNullable = true)]
        public string Contacto {
            get { return this._Entrega; }
            set {
                this._Entrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer referencia
        /// </summary>
        [DataNames("ALMPT_RFRNC")]
        [SugarColumn(ColumnName = "ALMPT_RFRNC", ColumnDescription = "referencia", Length = 100, IsNullable = true)]
        public string Referencia {
            get { return this._Referencia; }
            set {
                this._Referencia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de ingreso al almacen
        /// </summary>
        [DataNames("ALMPT_FCING")]
        [SugarColumn(ColumnName = "ALMPT_FCING", ColumnDescription = "fecha de ingreso al almacen", IsNullable = true)]
        public DateTime? FechaIngreso {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this._FechaIngreso >= firstGoodDate)
                    return this._FechaIngreso;
                return null;
            }
            set {
                this._FechaIngreso = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total
        /// </summary>
        [DataNames("ALMPT_SBTTL")]
        [SugarColumn(ColumnName = "ALMPT_SBTTL", ColumnDescription = "sub total", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del descuento
        /// </summary>
        [DataNames("ALMPT_DSCT")]
        [SugarColumn(ColumnName = "ALMPT_DSCT", ColumnDescription = "total del descuento", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalDescuento {
            get { return this._TotalDescuento; }
            set {
                this._TotalDescuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("ALMPT_TRIVA")]
        [SugarColumn(ColumnName = "ALMPT_TRIVA", ColumnDescription = "total traslado IVA", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal TotalTrasladoIVA {
            get { return this._TotalTrasladoIVA; }
            set {
                this._TotalTrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Total = SubTotal + TotalTrasladoIVA
        /// </summary>
        [DataNames("ALMPT_GTOTAL")]
        [SugarColumn(ColumnName = "ALMPT_GTOTAL", ColumnDescription = "gran total", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        [DataNames("ALMPT_OBSRV")]
        [SugarColumn(ColumnName = "ALMPT_OBSRV", ColumnDescription = "observaciones", Length = 100, IsNullable = true)]
        public string Nota {
            get { return this._Observaciones; }
            set {
                this._Observaciones = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("ALMPT_URL_PDF")]
        [SugarColumn(ColumnName = "ALMPT_URL_PDF", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text", IsNullable = true)]
        public string UrlFilePDF {
            get { return this._urlPDF; }
            set {
                this._urlPDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio del catalogo de vendedores
        /// </summary>
        [DataNames("ALMPT_CTVND_ID")]
        [SugarColumn(ColumnName = "ALMPT_CTVND_ID", ColumnDescription = "indice del directorio del catalogo de vendedores", IsNullable = true)]
        public int IdVendedor {
            get { return this._IdVendedor; }
            set {
                this._IdVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        [DataNames("ALMPT_VNDR")]
        [SugarColumn(ColumnName = "ALMPT_VNDR", ColumnDescription = "clave del vendedor asociado", Length = 10, IsNullable = true)]
        public string Vendedor {
            get { return this._Vendedor; }
            set {
                this._Vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        [DataNames("ALMPT_FN")]
        [SugarColumn(ColumnName = "ALMPT_FN", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this._FechaNuevo; }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        [DataNames("ALMPT_USU_N")]
        [SugarColumn(ColumnName = "ALMPT_USU_N", ColumnDescription = "clave del usuario creador del registro")]
        public string Creo {
            get { return this._Creo; }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("ALMPT_FM")]
        [SugarColumn(ColumnName = "ALMPT_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this._FechaModifica >= firstGoodDate)
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("ALMPT_USU_M")]
        [SugarColumn(ColumnName = "ALMPT_USU_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get { return this._Modifica; }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ALMPT_ANIO")]
        [SugarColumn(ColumnName = "ALMPT_ANIO", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public int Ejercicio { get; set; }

        [DataNames("ALMPT_MES")]
        [SugarColumn(ColumnName = "ALMPT_MES", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsNullable = true)]
        public int Periodo { get; set; }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
