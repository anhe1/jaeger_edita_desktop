﻿using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionConceptoPrinter {
        [DataNames("movapt_id")]
        public int Id {
            get; set;
        }

        [DataNames("VWMDLS_PRDC_ID")]
        public int IdProducto {
            get;set;
        }

        [DataNames("VWMDLS_ID")]
        public int IdModelo {
            get;set;
        }

        [DataNames("VWTAM_TAM_ID")]
        public int IdTamanio {
            get;set;
        }

        [DataNames("movapt_cntdd_sld")]
        public decimal Cantidad {
            get; set;
        }
        
        [DataNames("lstsys_nom")]
        public string Unidad {
            get; set;
        }
        
        [DataNames("movapt_undd")]
        public decimal Unidad2 {
            get; set;
        }
        
        [DataNames("vwMdls_catalogo")]
        public string Catalogo {
            get; set;
        }
        
        [DataNames("vwmdls_prdc")]
        public string Producto {
            get; set;
        }
        [DataNames("vwmdls_clave")]
        public string Clave {
            get; set;
        }
        [DataNames("vwmdls_marca")]
        public string Marca {
            get; set;
        }
        [DataNames("vwmdls_dscrp")]
        public string Descripcion {
            get; set;
        }
        [DataNames("vwmdls_espcf")]
        public string Especificacion {
            get; set;
        }
        [DataNames("vwtam_nom")]
        public string Tamanio {
            get; set;
        }
        [DataNames("vwtam_unt")]
        public decimal Unitario {
            get; set;
        }
        [DataNames("movapt_untr$")]
        public decimal ValorUnitario {
            get; set;
        }
        [DataNames("movapt_undd_untr$")]
        public decimal Importe {
            get; set;
        }
        [DataNames("movapt_sbttIVA$")]
        public decimal SubTotal {
            get; set;
        }
        [DataNames("ctlrms_impst")]
        public decimal Impuesto {
            get; set;
        }

        public string NoIdentificacion {
            get {
                return string.Format("LT{0:00000}-{1:00000}-{2:00}", this.IdProducto, this.IdModelo, this.IdTamanio);
            }
        }
    }
}
