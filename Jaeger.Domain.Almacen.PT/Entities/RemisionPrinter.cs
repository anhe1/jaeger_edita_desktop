﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionPrinter {
        public RemisionPrinter(object source) {

        }

        [DataNames("id")]
        public int Id {
            get; set;
        }

        [DataNames("IdStatus")]
        public int IdStatus {
            get;set;
        }

        [DataNames("folio")]
        public int Folio {
            get; set;
        }
        
        [DataNames("pedido")]
        public int NoPedido {
            get; set;
        }
        
        [DataNames("vendedor")]
        public string Vendedor {
            get; set;
        }
        
        [DataNames("clave")]
        public string Clave {
            get; set;
        }
        
        [DataNames("cliente")]
        public string Cliente {
            get; set;
        }
        
        [DataNames("rfc")]
        public string RFC {
            get; set;
        }
        
        [DataNames("credito")]
        public int Credito {
            get; set;
        }
        
        [DataNames("subtotal")]
        public decimal SubTotal {
            get; set;
        }
        
        [DataNames("total")]
        public decimal Total {
            get; set;
        }
        
        [DataNames("creo")]
        public string Creo {
            get; set;
        }
        
        [DataNames("leyenda")]
        public string Leyenda {
            get; set;
        }
        
        [DataNames("vence")]
        public DateTime FechaVence {
            get; set;
        }
        
        [DataNames("emision")]
        public DateTime FechaEmision {
            get; set;
        }
        
        [DataNames("recibe")]
        public string Recibe {
            get; set;
        }
        
        [DataNames("distribucion")]
        public string Distribucion {
            get; set;
        }
        
        [DataNames("observaciones")]
        public string Nota {
            get; set;
        }
        [DataNames("envio")]
        public string Embarque {
            get; set;
        }
        [DataNames("fiscal")]
        public string DomicilioFiscal {
            get; set;
        }

        public List<RemisionConceptoPrinter> Conceptos {
            get;set;
        }

        /// <summary>
        /// obtener la cantidad en letra
        /// </summary>
        public string TotalEnLetra {
            get {
                return NumeroALetras.Convertir((double)this.Total, 1);
            }
        }

        /// <summary>
        /// informacion para QR
        /// </summary>
        public string[] QrText {
            get {
                return new string[] { "||Folio=", this.Folio.ToString("#000000"), "|Fecha=", this.FechaEmision.ToString("dd-mm-yyyy"), "|Cliente=", this.Cliente, "|Vendedor=", this.Vendedor.ToString(), "|Articulos=", this.Conceptos.Count.ToString(),"|Total=", this.Total.ToString(), "||" };
            }
        }
    }
}
