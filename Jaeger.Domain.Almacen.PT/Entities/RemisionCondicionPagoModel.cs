﻿namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionCondicionPagoModel : Base.Abstractions.BaseSingleModel {
        public RemisionCondicionPagoModel() {
        }

        public RemisionCondicionPagoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
