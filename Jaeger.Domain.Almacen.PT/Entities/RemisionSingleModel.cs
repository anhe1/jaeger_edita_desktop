﻿using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionSingleModel : RemisionDetailModel, IRemisionDetailModel, IRemisionSingleModel, IRemisionModel {
        public RemisionSingleModel() : base() {

        }

        public RemisionSingleModel(RemisionDetailModel source) : base() {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionSingleModel>(source, this);
        }

        public new void SetValues(RemisionDetailModel source) {
            MapperClassExtensions.MatchAndMap<RemisionDetailModel, RemisionSingleModel>(source, this);
        }
    }
}
     