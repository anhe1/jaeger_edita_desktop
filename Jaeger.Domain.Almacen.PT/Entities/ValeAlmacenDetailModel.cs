﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("ALMPT", "ALMPT: movimientos")]
    public class ValeAlmacenDetailModel : ValeAlmacenModel, IValeAlmacenDetailModel, IValeAlmacenModel {
        private BindingList<ValeAlmacenConceptoModel> _Conceptos;
        private BindingList<ValeAlmacenStatusModel> _status;
        private BindingList<ValeAlmacenRelacionModel> _Relacion;

        public ValeAlmacenDetailModel() : base() {
            this.PropertyChanged += this.ValeAlmacenDetailModel_PropertyChanged;
            this._Conceptos = new BindingList<ValeAlmacenConceptoModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
            this._status = new BindingList<ValeAlmacenStatusModel>();
            this._Relacion = new BindingList<ValeAlmacenRelacionModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public AlmacenEnum TipoAlmacen {
            get { return (AlmacenEnum)Enum.Parse(typeof(AlmacenEnum), this.IdTipoAlmacen.ToString()); }
            set {
                this.IdTipoAlmacen = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum TipoComprobante {
            get { return (TipoComprobanteEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum TipoMovimiento {
            get { return (TipoMovimientoEnum)this.IdTipoMovimiento; }
            set {
                this.IdTipoMovimiento = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ValeAlmacenStatusEnum Status {
            get { return (ValeAlmacenStatusEnum)this.IdStatus; }
            set {
                this.IdStatus = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string StatusText {
            get { return EnumerationExtension.GetEnumDescription((ValeAlmacenStatusEnum)this.IdStatus); }
        }

        [SugarColumn(IsIgnore = true)]
        public string TipoComprobanteText {
            get { return EnumerationExtension.GetEnumDescription((TipoComprobanteEnum)this.IdTipoComprobante); }
        }

        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get { return string.IsNullOrEmpty(this.IdDocumento); }
        }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenConceptoModel> Conceptos {
            get { return this._Conceptos; }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenStatusModel> Autorizacion {
            get {
                return this._status;
            }
            set {
                this._status = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ValeAlmacenRelacionModel> Relaciones {
            get { return this._Relacion; }
            set {
                this._Relacion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get {
                var d1 = string.Join("", this.Conceptos.Select(it => it.GetOriginalString));
                var d2 = string.Join("|", new string[] { this.Version, this.Folio.ToString("000000"), this.Serie, this.Creo, this.ReceptorRFC, this.Receptor, this.FechaEmision.ToString("yyyyMMddHHmmss"), this.Total.ToString("0.00") });
                return "||" + string.Join("|", d2 + d1) + "||";
            }
        }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string KeyName {
            get { return string.Format("VAL-{0}-{1}-{2}-{3}-{4}", this.Creo, this.ReceptorRFC, this.IdDocumento, this.Folio.ToString("000000"), this.FechaEmision.ToString("yyyyMMddHHmmss")); }
        }
        
        private void ValeAlmacenDetailModel_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            //if (e.PropertyName == "IdEfecto") {
            //    this._Conceptos.Select(it => {
            //        it.IdEfecto = this.IdTipoMovimiento;
            //        it.Cantidad = (this.IdComprobante == 0 ? 0 : it.Cantidad);
            //        return it;
            //    }).ToList();
            //}
        }

        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            this.SubTotal = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.SubTotal);
            this.TotalDescuento = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.Descuento);
            this.TotalTrasladoIVA = this.Conceptos.Where((ValeAlmacenConceptoModel p) => p.Activo == true).Sum((ValeAlmacenConceptoModel p) => p.TrasladoIVA);
            this.Total = (this.SubTotal - this.TotalDescuento) + (this.TotalTrasladoIVA);
        }

        public void SetValues(ValeAlmacenDetailModel source) {
            MapperClassExtensions.MatchAndMap<ValeAlmacenDetailModel, ValeAlmacenDetailModel>(source, this);
        }

        public object Clone() {
            return this.MemberwiseClone();
        }

        public bool HasConcepts() {
            if (this._Conceptos == null) {
                this.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
            }
            return true;
        }

        public bool AddConcepto(ValeAlmacenConceptoModel nuevo) {
            try {
                var existe = Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                if (existe.Count() == 0) {
                    nuevo.IdTipoComprobante = this.IdTipoComprobante;
                    nuevo.IdTipoMovimiento = this.IdTipoMovimiento;
                    this.Conceptos.Add(nuevo);
                    return true;
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
