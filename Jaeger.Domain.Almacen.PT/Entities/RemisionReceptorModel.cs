﻿using SqlSugar;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionReceptorModel : BasePropertyChangeImplementation, IRemisionReceptorModel {
        #region declaraciones
        private int idCliente;
        private string _ReceptorClave;
        private string receptorRFC;
        private string receptorNombre;
        #endregion

        public RemisionReceptorModel() { }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("_rmsn_drctr_id", "RMSN_DRCTR_ID")]
        [SugarColumn(ColumnName = "RMSN_DRCTR_ID", ColumnDescription = "indice del direcotorio", IsNullable = true)]
        public int IdCliente {
            get { return this.idCliente; }
            set {
                this.idCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("_rmsn_clvr", "RMSN_CLVR")]
        [SugarColumn(ColumnName = "RMSN_CLVR", ColumnDescription = "clave de control interno de cliente", Length = 10)]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("_rmsn_rfcr", "RMSN_RFCR")]
        [SugarColumn(ColumnName = "RMSN_RFCR", ColumnDescription = "registro federal de contribuyentes del cliente o receptor", Length = 14)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("_rmsn_nomr", "RMSN_NOMR")]
        [SugarColumn(ColumnName = "RMSN_NOMR", ColumnDescription = "nombre o razon social del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }
    }
}
