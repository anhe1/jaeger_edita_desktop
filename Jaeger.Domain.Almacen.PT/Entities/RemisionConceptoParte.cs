﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Domain.Base.Abstractions;
using Newtonsoft.Json;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionConceptoParte : BasePropertyChangeImplementation, IDataErrorInfo {
        #region declaraciones
        private string claveProdServField;
        private string noIdentificacionField;
        private decimal cantidadField;
        private string unidadField;
        private string descripcionField;
        private decimal valorUnitarioField;
        private decimal importeField;
        #endregion

        public RemisionConceptoParte() {
        }

        #region propiedadees

        /// <summary>
        /// Atributo requerido para expresar la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [JsonProperty("claveProdServ")]
        public string ClaveProdServ {
            get {
                return this.claveProdServField;
            }
            set {
                this.claveProdServField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el número de serie, número de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves del estándar GTIN.
        /// </summary>
        [JsonProperty("noIdent")]
        public string NoIdentificacion {
            get {
                return this.noIdentificacionField;
            }
            set {
                this.noIdentificacionField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para precisar la cantidad de bienes o servicios del tipo particular definido por la presente parte.
        /// </summary>
        [JsonProperty("cantidad")]
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en la parte. La unidad debe corresponder con la descripción de la parte.
        /// </summary>
        [JsonProperty("unidad")]
        public string Unidad {
            get {
                return this.unidadField;
            }
            set {
                this.unidadField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para precisar la descripción del bien o servicio cubierto por la presente parte.
        /// </summary>
        [JsonProperty("descripcion")]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar el valor o precio unitario del bien o servicio cubierto por la presente parte. No se permiten valores negativos.
        /// </summary>
        [JsonProperty("valorUnitario")]
        public decimal ValorUnitario {
            get {
                return this.valorUnitarioField;
            }
            set {
                this.valorUnitarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar el importe total de los bienes o servicios de la presente parte. Debe ser equivalente al resultado de multiplicar la cantidad por el valor unitario expresado en la parte. No se permiten valores negativos.
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region metodos
        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static RemisionConceptoParte Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<RemisionConceptoParte>(inputJson);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        #endregion

        [JsonIgnore]
        public string Error {
            get {
                if (!this.RegexValido(this.ClaveProdServ, "[0-9]{8}") || !(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false)) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        [JsonIgnore]
        public string this[string columnName] {
            get {
                if (columnName == "ClaveProdServ" && !this.RegexValido(this.ClaveProdServ, "[0-9]{8}")) {
                    return "Formato de clave de producto ó servicio no válida!";
                }

                if (columnName == "Descripcion" && !(this.Descripcion == null)) {
                    return "Debes ingresar un concepto.";
                }
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron) {
            if (!(valor == null)) {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
}
     