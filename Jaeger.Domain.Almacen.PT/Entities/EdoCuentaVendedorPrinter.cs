﻿using System.Collections.Generic;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class EdoCuentaVendedorPrinter {

        public EdoCuentaVendedorPrinter() {
        }

        public string Vendedor { get; set; }

        public List<RemisionSingleModel> Conceptos { get; set; }

        public string[] QrText {
            get {
                return new string[] { "||Vendedores||" };
            }
        }
    }
}
