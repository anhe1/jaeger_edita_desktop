﻿using SqlSugar;
using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class ValeAlmacenConceptoModel : MovimientoModel, IMovimientoModel, IDataErrorInfo {
        public ValeAlmacenConceptoModel() : base() {

        }

        [SugarColumn(IsIgnore = true)]
        public TipoComprobanteEnum TipoComprobante {
            get { return (TipoComprobanteEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum TipoMovimiento {
            get { return (TipoMovimientoEnum)this.IdTipoMovimiento; }
            set {
                this.IdTipoMovimiento = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string GetOriginalString {
            get { return string.Join("|", new string[] { this.Cantidad.ToString("#0.00"), this.Unidad, this.Descripcion, this.Identificador, this.Unitario.ToString("0.00"), this.Importe.ToString("0.00") }); }
        }

        public new ValeAlmacenConceptoModel Clone() {
            var duplicado = (ValeAlmacenConceptoModel)base.Clone();
            return duplicado;
        }

        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                //if ((this.Efecto == MovimientoTipoEnum.Entrada && this.cantidadEntrada <= 0) | this.Efecto == MovimientoTipoEnum.Salida && this.cantidadSalida <= 0)
                //    return "Por favor ingrese datos válidos en esta fila!";
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                //if (columnName == "Entrada" && this.Efecto == MovimientoTipoEnum.Entrada && this.Entrada <= 0)
                //    return "Es necesario ingresar una cantidad";
                return string.Empty;
            }
        }
    }
}
