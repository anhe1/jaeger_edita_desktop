﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class DocumentoAlmacenRelacionadoModel : BaseSingleModel {
        public DocumentoAlmacenRelacionadoModel() {
        }

        public DocumentoAlmacenRelacionadoModel(int id, string descripcion) : base(id, descripcion) {
        }
    }
}
