﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class RemisionComisionDetailModel : RemisionComisionModel, IRemisionComisionDetailModel, IRemisionComisionModel {
        #region declaraciones
        private int precisionDecimal;
        private int idPedido;
        private int folio;
        private int idStatus;
        private int idTipoDocumento;
        private DateTime fechaEmision;
        private DateTime? fechaCancela;
        private DateTime? fechaEntrega;
        private DateTime? fechaUltPago;
        private DateTime? fechaCobranza;
        private DateTime fechaNuevo;
        private string version;
        private string serie;
        private string idDocumento;
        private string emisorRFC;
        private string receptorRFC;
        private string receptorNombre;
        private decimal tipoCambio;
        private decimal trasladoIVA;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal _porCobrar;
        private decimal acumulado;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveMoneda;
        private string creo;
        private string nota;
        private string contacto;
        private int idMetodoEnvio;
        private int idDomicilio;
        private int idNumeroGuia;
        private int idSerie;
        private int idCatalogo;
        private decimal factorPactado;
        private int idRelacion;
        private string cancela;
        private decimal _TasaIVA;
        private string _ReceptorClave;
        private decimal _Saldo;
        private decimal _TotalIVAPactado;
        private decimal _GTotal;
        private decimal _PorCobrarPactado;
        private string _DescuentoTipo;
        private decimal _DescuentoFactor;
        private decimal _DescuentoTotal;
        private BindingList<RemisionMovimientoBancarioModel> movimientos;
        #endregion

        public RemisionComisionDetailModel() : base() {
            this.movimientos = new BindingList<RemisionMovimientoBancarioModel>();
        }

        #region propiedades de la remision

        [DataNames("RMSN_ID")]
        public new int IdRemision {
            get {
                return base.IdRemision;
            }
            set {
                base.IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        [DataNames("RMSN_CTDOC_ID")]
        public int IdTipoDocumento {
            get {
                return this.idTipoDocumento;
            }
            set {
                this.idTipoDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// precision decimal
        /// </summary>
        [DataNames("RMSN_DECI")]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set {
                this.precisionDecimal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        [DataNames("RMSN_VER")]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        [DataNames("RMSN_FOLIO")]
        public int Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        [DataNames("RMSN_CTSR_ID")]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        [DataNames("RMSN_SERIE")]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        [DataNames("RMSN_STTS_ID")]
        public int IdStatus {
            get {
                return this.idStatus;
            }
            set {
                this.idStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataNames("RMSN_PDD_ID")]
        public int IdPedido {
            get { return this.idPedido; }
            set {
                this.idPedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("RMSN_CTCLS_ID")]
        public int IdCatalogo {
            get { return this.idCatalogo; }
            set {
                this.idCatalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del metodo de envio
        /// </summary>
        [DataNames("RMSN_MTDENV_ID")]
        public int IdMetodoEnvio {
            get { return this.idMetodoEnvio; }
            set {
                this.idMetodoEnvio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        [DataNames("RMSN_DRCTR_ID")]
        public new int IdCliente {
            get { return base.IdCliente; }
            set {
                base.IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        [DataNames("RMSN_DRCCN_ID")]
        public int IdDomicilio {
            get { return this.idDomicilio; }
            set {
                this.idDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el id del vendedor
        /// </summary>
        [DataNames("RMSN_VNDDR_ID")]
        public new int IdVendedor {
            get { return base.IdVendedor; }
            set {
                base.IdVendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        [DataNames("RMSN_GUIA_ID")]
        public int NoGuia {
            get { return this.idNumeroGuia; }
            set {
                this.idNumeroGuia = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        [DataNames("RMSN_CTREL_ID")]
        public int IdRelacion {
            get { return this.idRelacion; }
            set {
                this.idRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        [DataNames("RMSN_RFCE")]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        [DataNames("RMSN_CLVR")]
        public string ReceptorClave {
            get { return this._ReceptorClave; }
            set {
                this._ReceptorClave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        [DataNames("RMSN_RFCR")]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                if (value != null) {
                    this.receptorRFC = value.Trim().Replace("-", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        [DataNames("RMSN_NOMR")]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        [DataNames("RMSN_CNTCT")]
        public string Contacto {
            get { return this.contacto; }
            set {
                this.contacto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        [DataNames("RMSN_UUID")]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        [DataNames("RMSN_FECEMS")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de entrega
        /// </summary>
        [DataNames("RMSN_FECENT")]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate)
                    return this.fechaEntrega;
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de recepcion de cobranza
        /// </summary>
        [DataNames("RMSN_FECCBR")]
        public DateTime? FechaCobranza {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCobranza >= firstGoodDate)
                    return this.fechaCobranza;
                return null;
            }
            set {
                this.fechaCobranza = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        [DataNames("RMSN_FECUPC")]
        public DateTime? FechaUltPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPago >= firstGoodDate)
                    return this.fechaUltPago;
                return null;
            }
            set {
                this.fechaUltPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de vencimiento del pagare
        /// </summary>
        [DataNames("RMSN_FECVNC")]
        public DateTime? FechaVence {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltPago >= firstGoodDate)
                    return this.fechaUltPago;
                return null;
            }
            set {
                this.fechaUltPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("RMSN_FCCNCL")]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        [DataNames("RMSN_TPCMB")]
        public decimal TipoCambio {
            get { return this.tipoCambio; }
            set {
                this.tipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        [DataNames("RMSN_SBTTL")]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del descuento (suma de los descuentos aplicados en las partidas de la remision)
        /// </summary>
        [DataNames("RMSN_DSCNT")]
        public decimal TotalDescuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establer el importe de la remision antes de impuestos (SubToal - TotalDescuentos)
        /// </summary>
        public decimal Importe {
            get { return (this.SubTotal - this.TotalDescuento); }
        }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA (suma del iva de las partidas de la remision)
        /// </summary>
        [DataNames("RMSN_TRSIVA")]
        public decimal TotalTrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        [DataNames("RMSN_GTOTAL")]
        public decimal Total {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la tasa del iva pactado con el cliente
        /// </summary>
        [DataNames("RMSN_FACIVA")]
        public decimal TasaIVAPactado {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total del iva pactado con el cliente (RMSN_IVAPAC)
        /// </summary>
        [DataNames("RMSN_IVAPAC")]
        public decimal TotalIVAPactado {
            get { return this._TotalIVAPactado; }
            set {
                this._TotalIVAPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total despues del iva pactado (SubTotal + TotalIVAPactado)
        /// </summary>
        [DataNames("RMSN_TOTAL")]
        public decimal GTotal {
            get { return this._GTotal; }
            set {
                this._GTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del factor pactado para el descuento
        /// </summary>
        [DataNames("RMSN_FACPAC")]
        public decimal FactorPactado {
            get { return this.factorPactado; }
            set {
                this.factorPactado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSN_XCBPAC")]
        public decimal PorCobrarPactado {
            get { return this._PorCobrarPactado; }
            set {
                this._PorCobrarPactado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de descuento aplicado
        /// </summary>
        [DataNames("RMSN_DESC")]
        public string DescuentoTipo {
            get { return this._DescuentoTipo; }
            set {
                this._DescuentoTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor del descuento aplicado
        /// </summary>
        [DataNames("RMSN_FACDES")]
        public decimal DescuentoFactor {
            get { return this._DescuentoFactor; }
            set {
                this._DescuentoFactor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total del descuento aplicado (GTotal * DescuentoFactor)
        /// </summary>
        [DataNames("RMSN_DESCT")]
        public decimal DescuentoTotal {
            get { return this._DescuentoTotal; }
            set {
                this._DescuentoTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe por cobrar del comprobante
        /// </summary>
        [DataNames("RMSN_XCBRR")]
        public decimal PorCobrar {
            get { return this._porCobrar; }
            set {
                this._porCobrar = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acumulado de la cobranza del comprobante
        /// </summary>
        [DataNames("RMSN_CBRD")]
        public decimal Acumulado {
            get { return this.acumulado; }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("RMSN_SALDO")]
        public decimal Saldo {
            get { return this._Saldo; }
            set {
                this._Saldo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("RMSN_MONEDA")]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("RMSN_MTDPG")]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("RMSN_FRMPG")]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer una nota
        /// </summary>
        [DataNames("RMSN_NOTA")]
        public string Nota {
            get { return this.nota; }
            set {
                this.nota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        [DataNames("RMSN_VNDDR")]
        public new string Vendedor {
            get { return base.Vendedor; }
            set {
                base.Vendedor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        [DataNames("RMSN_USR_C")]
        public string Cancela {
            get { return this.cancela; }
            set {
                this.cancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("RMSN_FN")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        [DataNames("RMSN_USR_N")]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer dias transcurridos desde el ultimo cobro (CTLRMS_DSTRS solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRS")]
        public int DiasUltCobro {
            get {
                if (!(this.FechaUltPago is null) && !(this.FechaEntrega is null))
                    return (this.FechaUltPago - this.FechaEntrega).Value.Days;
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer dias transcurridos desde la fecha de entrega (CTLRMS_DSTRSA solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRSA")]
        public int DiasTranscurridos {
            get {
                if (this.FechaEntrega != null)
                    if (this.IdStatus == 4)
                        return DateTime.Now.Subtract(this.FechaEntrega.Value).Days + 1;
                    else if (this.IdStatus > 4)
                        if (this.FechaUltPago != null)
                            return this.FechaUltPago.Value.Subtract(this.FechaEntrega.Value).Days + 1;
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer los dias transcurridos desde la fecha de cobranza cuando el status es 4, cuando es mayor entonces se utiliza la fecha del ultimo pago
        /// </summary>
        public int DiasCobranza {
            get {
                if (this.FechaCobranza != null) {
                    if (this.IdStatus == 4)
                        return DateTime.Now.Subtract(this.FechaCobranza.Value).Days + 1;
                    else if (this.IdStatus > 4) {
                        if (this.FechaUltPago != null)
                            return this.FechaUltPago.Value.Subtract(this.FechaCobranza.Value).Days + 1;
                    }
                }
                return 0;
            }
        }
        #endregion

        /// <summary>
        /// ALTER TABLE CTLRMS ADD CTLRMS_DIVRR COMPUTED BY (((cast(case when ( ctlrms_cbrd$ > 0 and ctlrms_sbttl$ > 0 ) then ctlrms_cbrd$ / (ctlrms_sbttl$+ctlrms_impst$) else -0.00001 end as dom_moneda))))
        /// </summary>
        public decimal FactorRealCobrado {
            get {
                if (this.TotalCobrado > 0 && this.SubTotal > 0) {
                    return this.TotalCobrado / (this.SubTotal + this.TotalIVAPactado);
                }
                return 0;
            }
        }

        public decimal TotalNotaDescuento {
            get { return this.movimientos.Where(it => it.ClaveFormaPago == "12").Sum(it => it.Cargo1); }
        }

        /// <summary>
        /// obtener el importe cobrado sin notas de descuento
        /// </summary>
        public decimal TotalCobrado {
            get { return this.Acumulado - this.TotalNotaDescuento; }
        }

        public BindingList<RemisionMovimientoBancarioModel> Movimientos {
            get { return this.movimientos; }
            set {
                this.movimientos = value;
                this.OnPropertyChanged();
            }
        }
    }
}
