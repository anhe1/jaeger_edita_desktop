﻿using System;
using System.Text.RegularExpressions;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class MovimientoModel : BasePropertyChangeImplementation, IMovimientoModel, Almacen.Contracts.IMovimientoModel {
        #region declaraciones
        private int _IdMovimiento;
        private bool _Activo;
        private int _IdComprobante;
        private int _IdAlmacen;
        private int _IdProducto;
        private int _IdModelo;
        private int _IdRemision;
        private int _IdDepartamento;
        private int _IdEspecificacion;
        private int _IdUnidad;
        private decimal _CantidadEntrante;
        private decimal _CantidadSalida;
        private decimal _CostoUnitario;
        private decimal _SubTotal;
        private string _Creo;
        private DateTime _FechaNuevo;
        private string _Modifica;
        private DateTime? _FechaModifica;
        private decimal _TasaIVA;
        private decimal _TrasladoIVA;
        private int _IdPrecio;
        private decimal _UnidadFactor;
        private string _Unidad;
        private string _Catalogo;
        private string _Producto;
        private string _Modelo;
        private string _Marca;
        private string _Especificacion;
        private string _Tamanio;
        private decimal _ValorUnitario;
        private decimal _CostoUnidad;
        private decimal _Importe;
        private string _Identificador;
        private int _idPedido;
        private string claveUnidad;
        private string claveProdServ;
        private int _IdCliente;
        private decimal _Unitario;
        private decimal _Descuento;
        private decimal _Total;
        private int _IdTipoComprobante;
        private int _IdTipoMovimiento;
        private int _OrdenProduccion;
        private int _OrdenCompra;
        #endregion

        public MovimientoModel() {
            this._FechaNuevo = DateTime.Now;
            this._Activo = true;
        }

        /// <summary>
        /// obtener o establecer indice de la tabla (MVAPT_ID)
        /// </summary>
        [DataNames("MVAPT_ID")]
        [SugarColumn(ColumnName = "MVAPT_ID", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdMovimiento {
            get { return _IdMovimiento; }
            set {
                _IdMovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo (MVAPT_A)
        /// </summary>
        [DataNames("MVAPT_A")]
        [SugarColumn(ColumnName = "MVAPT_A", ColumnDescription = "registro activo", DefaultValue = "1", IsNullable = false)]
        public bool Activo {
            get { return this._Activo; }
            set {
                this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento (26= Remision
        /// </summary>
        [DataNames("MVAPT_DOC_ID")]
        [SugarColumn(ColumnName = "MVAPT_DOC_ID", ColumnDescription = "tipo de documento")]
        public int IdTipoComprobante {
            get { return this._IdTipoComprobante; }
            set {
                this._IdTipoComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el efecto del movimiento (MVAPT_CTEFC_ID)
        /// </summary>
        [DataNames("MVAPT_CTEFC_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTEFC_ID", ColumnDescription = "Efecto del comprobante")]
        public int IdTipoMovimiento {
            get { return this._IdTipoMovimiento; }
            set {
                this._IdTipoMovimiento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de Almacen de Producto Terminado (ALMPT)
        /// </summary>
        [DataNames("MVAPT_ALMPT_ID")]
        [SugarColumn(ColumnName = "MVAPT_ALMPT_ID", ColumnDescription = "indice de relacion con la tabla de Almacen de Producto Terminado (ALMPT)")]
        public int IdComprobante {
            get { return this._IdComprobante; }
            set {
                this._IdComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de remisiones
        /// </summary>
        [DataNames("MVAPT_RMSN_ID")]
        [SugarColumn(ColumnName = "MVAPT_RMSN_ID", ColumnDescription = "indice de relacion con la tabla de remisiones")]
        public int IdRemision {
            get { return this._IdRemision; }
            set {
                this._IdRemision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        [DataNames("MVAPT_DRCTR_ID")]
        [SugarColumn(ColumnName = "MVAPT_DRCTR_ID", ColumnDescription = "indice del directorio")]
        public int IdCliente {
            get { return this._IdCliente; }
            set {
                this._IdCliente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de departamentos
        /// </summary>
        [DataNames("MVAPT_CTDPT_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTDPT_ID", ColumnDescription = "indice del catalogo de departamentos")]
        public int IdDepartamento {
            get { return this._IdDepartamento; }
            set {
                this._IdDepartamento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del departamento (MVAPT_CTALM_ID)
        /// </summary>
        [DataNames("MVAPT_CTALM_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTALM_ID", ColumnDescription = "indice del departamento")]
        public int IdAlmacen {
            get { return this._IdAlmacen; }
            set {
                this._IdAlmacen = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        [DataNames("MVAPT_CTPRD_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTPRD_ID", ColumnDescription = "indice del catalogo de productos")]
        public int IdProducto {
            get { return this._IdProducto; }
            set {
                this._IdProducto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de modelos
        /// </summary>
        [DataNames("MVAPT_CTMDL_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTMDL_ID", ColumnDescription = "indice del catalogo de modelos")]
        public int IdModelo {
            get { return this._IdModelo; }
            set {
                this._IdModelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de precios
        /// </summary>
        [DataNames("MVAPT_CTPRC_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTPRC_ID", ColumnDescription = "indice del catalogo de precios")]
        public int IdPrecio {
            get { return this._IdPrecio; }
            set {
                this._IdPrecio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice del catalogo de especificaciones
        /// </summary>
        [DataNames("MVAPT_CTESPC_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTESPC_ID", ColumnDescription = "indice del catalogo de especificaciones")]
        public int IdEspecificacion {
            get { return this._IdEspecificacion; }
            set {
                this._IdEspecificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del catalogo de unidad
        /// </summary>
        [DataNames("MVAPT_CTUND_ID")]
        [SugarColumn(ColumnName = "MVAPT_CTUND_ID", ColumnDescription = "el indice del catalogo de unidad")]
        public int IdUnidad {
            get { return this._IdUnidad; }
            set {
                this._IdUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el factor de la unidad
        /// </summary>
        [DataNames("MVAPT_UNDF")]
        [SugarColumn(ColumnName = "MVAPT_UNDF", ColumnDescription = "factor de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal UnidadFactor {
            get { return this._UnidadFactor; }
            set {
                this._UnidadFactor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden de compra relacionada
        /// </summary>
        [DataNames("MVAPT_OCMP")]
        [SugarColumn(ColumnName = "MVAPT_OCMP", ColumnDescription = "obtener o establecer el numero de orden de compra relacionada", IsNullable = true)]
        public int OrdenCompra {
            get {
                return this._OrdenCompra;
            }
            set {
                this._OrdenCompra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre de la unidad
        /// </summary>
        [DataNames("MVAPT_UNDN")]
        [SugarColumn(ColumnName = "MVAPT_UNDN", ColumnDescription = "nombre de la unidad", Length = 50)]
        public string Unidad {
            get { return this._Unidad; }
            set {
                this._Unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de orden de produccion relacionada
        /// </summary>
        [DataNames("MVAPT_OPRD")]
        [SugarColumn(ColumnName = "MVAPT_OPRD", ColumnDescription = "obtener o establecer el numero de orden de produccion relacionada", IsNullable = true)]
        public int OrdenProduccion {
            get {
                return this._OrdenProduccion;
            }
            set {
                this._OrdenProduccion = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Cantidad {
            get {
                if (this.IdTipoMovimiento == (int)TipoMovimientoEnum.Ingreso)
                    return this.CantidadE;
                else return this.CantidadS;
            }
            set {
                if (this.IdTipoMovimiento == (int)TipoMovimientoEnum.Ingreso)
                    this.CantidadE = value;
                else
                    this.CantidadS = value;
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad entrante
        /// </summary>
        [DataNames("MVAPT_CANTE")]
        [SugarColumn(ColumnName = "MVAPT_CANTE", ColumnDescription = "cantidad entrante", Length = 14, DecimalDigits = 4)]
        public decimal CantidadE {
            get { return this._CantidadEntrante; }
            set {
                this._CantidadEntrante = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad saliente
        /// </summary>
        [DataNames("MVAPT_CANTS")]
        [SugarColumn(ColumnName = "MVAPT_CANTS", ColumnDescription = "cantidad saliente", Length = 14, DecimalDigits = 4)]
        public decimal CantidadS {
            get { return this._CantidadSalida; }
            set {
                this._CantidadSalida = value;
                this.Unitario = this.Unitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de pedido del cliente hasta 21 caracteres
        /// </summary>
        [DataNames("MVAPT_PDCLN_ID")]
        [SugarColumn(ColumnName = "MVAPT_PDCLN_ID", ColumnDescription = "numero de pedido del cliente hasta 21 caracteres")]
        public int IdPedido {
            get { return this._idPedido; }
            set {
                this._idPedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del catalogo de productos
        /// </summary>
        [DataNames("MVAPT_CTCLS")]
        [SugarColumn(ColumnName = "MVAPT_CTCLS", ColumnDescription = "nombre o descripcion del catalogo de productos", Length = 128)]
        public string Catalogo {
            get { return this._Catalogo; }
            set {
                this._Catalogo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del producto
        /// </summary>
        [DataNames("MVAPT_PRDN")]
        [SugarColumn(ColumnName = "MVAPT_PRDN", ColumnDescription = "nombre o descripcion del producto", Length = 128)]
        public string Producto {
            get { return this._Producto; }
            set {
                this._Producto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del modelo
        /// </summary>
        [DataNames("MVAPT_MDLN")]
        [SugarColumn(ColumnName = "MVAPT_MDLN", ColumnDescription = "nombre o descripcion del modelo", Length = 128)]
        public string Modelo {
            get { return this._Modelo; }
            set {
                this._Modelo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la marca
        /// </summary>
        [DataNames("MVAPT_MRC")]
        [SugarColumn(ColumnName = "MVAPT_MRC", ColumnDescription = "nombre o descripcion de la marca", Length = 128)]
        public string Marca {
            get { return this._Marca; }
            set {
                this._Marca = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion de la especificacion
        /// </summary>
        [DataNames("MVAPT_ESPC")]
        [SugarColumn(ColumnName = "MVAPT_ESPC", ColumnDescription = "nombre o descripcion de la especificacion", Length = 128)]
        public string Especificacion {
            get { return this._Especificacion; }
            set {
                this._Especificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre o descripcion del tamanio
        /// </summary>
        [DataNames("MVAPT_ESPN")]
        [SugarColumn(ColumnName = "MVAPT_ESPN", ColumnDescription = "nombre o descripcion del tamanio", Length = 50)]
        public string Tamanio {
            get { return this._Tamanio; }
            set {
                this._Tamanio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo unitario (MVAPT_UNTC)
        /// </summary>
        [DataNames("MVAPT_UNTC")]
        [SugarColumn(ColumnName = "MVAPT_UNTC", ColumnDescription = "costo unitario", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnitario {
            get { return this._CostoUnitario; }
            set {
                this._CostoUnitario = value;
                this._CostoUnidad = value * this._UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer costo de la unidad (MVAPT_UNDC)
        /// </summary>
        [DataNames("MVAPT_UNDC")]
        [SugarColumn(ColumnName = "MVAPT_UNDC", ColumnDescription = "costo de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal CostoUnidad {
            get { return this._CostoUnidad; }
            set {
                this._CostoUnidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario por pieza (MVAPT_UNTR)
        /// </summary>
        [DataNames("MVAPT_UNTR")]
        [SugarColumn(ColumnName = "MVAPT_UNTR", ColumnDescription = "unitario por pieza", Length = 14, DecimalDigits = 4)]
        public decimal ValorUnitario {
            get { return this._ValorUnitario; }
            set {
                this._ValorUnitario = value;
                this.Unitario = this._ValorUnitario * this.UnidadFactor;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer valor unitario de la unidad (MVAPT_UNTR2)
        /// </summary>
        [DataNames("MVAPT_UNTR2")]
        [SugarColumn(ColumnName = "MVAPT_UNTR2", ColumnDescription = "unitario de la unidad", Length = 14, DecimalDigits = 4)]
        public decimal Unitario {
            get { return this._Unitario; }
            set {
                this._Unitario = value;
                this.SubTotal = this.Cantidad * this.Unitario;
                this.Importe = this.SubTotal - this.Descuento;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer sub total de la partida
        /// </summary>
        [DataNames("MVAPT_SBTTL")]
        [SugarColumn(ColumnName = "MVAPT_SBTTL", ColumnDescription = "sub total de la partida", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this._SubTotal; }
            set {
                this._SubTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estsablecer el importe del descuento aplicable al producto o sevicio 
        /// </summary>
        [DataNames("MVAPT_DESC")]
        [SugarColumn(ColumnName = "MVAPT_DESC", ColumnDescription = "importe del descuento aplicable al producto o sevicio ", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this._Descuento; }
            set {
                this._Descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del importe de la partida (SubTotal - Descuento)
        /// </summary>
        [DataNames("MVAPT_IMPRT")]
        [SugarColumn(ColumnName = "MVAPT_IMPRT", ColumnDescription = "valor del importe de la partida (SubTotal - Descuento)", Length = 14, DecimalDigits = 4)]
        public decimal Importe {
            get { return this._Importe; }
            set {
                this._Importe = value;
                this.TrasladoIVA = this._Importe * this.TasaIVA;
                this.Total = this._Importe + this.TrasladoIVA;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tasa del IVA
        /// </summary>
        [DataNames("MVAPT_TSIVA")]
        [SugarColumn(ColumnName = "MVAPT_TSIVA", ColumnDescription = "tasa del IVA", Length = 14, DecimalDigits = 4)]
        public decimal TasaIVA {
            get { return this._TasaIVA; }
            set {
                this._TasaIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe trasladado del impuesto IVA
        /// </summary>
        [DataNames("MVAPT_TRIVA")]
        [SugarColumn(ColumnName = "MVAPT_TRIVA", ColumnDescription = "importe trasladado del impuesto IVA", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this._TrasladoIVA; }
            set {
                this._TrasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de la partida (Importe + TrasladoIVA)
        /// </summary>
        [DataNames("MVAPT_TOTAL")]
        [SugarColumn(ColumnName = "MVAPT_TOTAL", ColumnDescription = "total de la partida (Importe + TrasladoIVA)", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this._Total; }
            set {
                this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DataNames("MVAPT_CLVUND")]
        [SugarColumn(IsIgnore = true, ColumnName = "MVAPT_CLVUND", ColumnDescription = "clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.", IsNullable = true)]
        public string ClaveUnidad {
            get {
                return this.claveUnidad;
            }
            set {
                this.claveUnidad = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por el presente concepto. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [DataNames("MVAPT_CLVPRDS")]
        [SugarColumn(IsIgnore = true, ColumnName = "MVAPT_CLVPRDS", ColumnDescription = "clave de producto catalogo SAT", Length = 6, IsNullable = true)]
        public string ClaveProdServ {
            get {
                return this.claveProdServ;
            }
            set {
                if (value != null) {
                    this.claveProdServ = Regex.Replace(value.ToString(), "[^\\d]", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVAPT_SKU")]
        [SugarColumn(ColumnName = "MVAPT_SKU", ColumnDescription = "identificador: IdProducto + IdModelo + IdEspecificacion (Tamanio)", Length = 20)]
        public string Identificador {
            get { return this._Identificador; }
            set {
                this._Identificador = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVAPT_USR_N")]
        [SugarColumn(ColumnName = "MVAPT_USR_N", ColumnDescription = "clave del usuario creador del registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this._Creo;
            }
            set {
                this._Creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        [DataNames("MVAPT_FN")]
        [SugarColumn(ColumnName = "MVAPT_FN", ColumnDescription = "fecha de creacion del registro", IsOnlyIgnoreUpdate = true)]
        public DateTime FechaNuevo {
            get {
                return this._FechaNuevo;
            }
            set {
                this._FechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVAPT_USR_M")]
        [SugarColumn(ColumnName = "MVAPT_USR_M", ColumnDescription = "clave del ultimo usuario que modifica el registro", Length = 10, IsOnlyIgnoreInsert = true)]
        public string Modifica {
            get {
                return this._Modifica;
            }
            set {
                this._Modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        [DataNames("MVAPT_FM")]
        [SugarColumn(ColumnName = "MVAPT_FM", ColumnDescription = "ultima fecha de modificacion del registro", IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                if (this._FechaModifica >= new DateTime(1900, 1, 1))
                    return this._FechaModifica;
                return null;
            }
            set {
                this._FechaModifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener la concatenacion de producto + modelo + tamanio
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string Descripcion {
            get { return string.Format("{0} {1} {2}", this.Producto, this.Modelo, this.Tamanio); }
        }

        public object Clone() {
            return this.MemberwiseClone();
        }
    }
}
