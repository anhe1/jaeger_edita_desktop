﻿using Jaeger.Domain.Almacen.PT.Contracts;
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.Entities {
    public class ValeAlmacenStatusDetailModel : ValeAlmacenStatusModel, IValeAlmacenStatusModel, IValeAlmacenStatusDetailModel {
        public ValeAlmacenStatusDetailModel() : base() {

        }

        public BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
    }
}
