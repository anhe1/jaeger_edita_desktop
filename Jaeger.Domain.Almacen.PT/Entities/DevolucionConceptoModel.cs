﻿using SqlSugar;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Entities {
    [SugarTable("MVAPT", "ALMPT: movimientos")]
    public class DevolucionConceptoModel : MovimientoModel {
        public DevolucionConceptoModel() : base() {

        }

        [SugarColumn(IsIgnore = true)]
        public TipoMovimientoEnum Tipo {
            get { return (TipoMovimientoEnum)this.IdTipoComprobante; }
            set {
                this.IdTipoComprobante = (int)value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public new decimal Cantidad {
            get {
                if (this.Tipo == TipoMovimientoEnum.Ingreso)
                    return base.CantidadE;
                else return base.CantidadS;
            }
        }
    }
}
