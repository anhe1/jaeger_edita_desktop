﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface ISqlRemisionadoPTRepository : IGenericRepository<RemisionModel> {
        new RemisionDetailModel GetById(int index);

        /// <summary>
        /// obtener listado por condiciones
        /// </summary>
        /// <typeparam name="T1">objetos aceptados Remision y Remision Partida</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado por receptores
        /// </summary>
        /// <typeparam name="T1">objetos aceptados RemisionReceptor</typeparam>
        /// <param name="conditionals">lista de condicionales</param>
        IEnumerable<T1> GetListR<T1>(List<IConditional> conditionals) where T1 : class, new();

        RemisionDetailModel Save(RemisionDetailModel remision);

        bool Update(string fieldName, object fieldValue, int indexPK);

        bool UploadPDF(IUploadPDF model);
        bool Update(IRemisionStatusDetailModel model);

        string CreateGuid(string[] datos);
    }
}
