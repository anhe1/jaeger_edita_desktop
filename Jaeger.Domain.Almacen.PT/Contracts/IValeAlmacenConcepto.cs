﻿/// purpose: interface para describir un concepto del vale de almacén
/// develop: anhe 151020191722
using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    /// <summary>
    /// interface para describir un concepto del vale de almacén
    /// </summary>
    public interface IValeAlmacenConcepto {
        int IdConcepto { get; set; }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el vale del almacen
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el numero de orden de compra relacionada
        /// </summary>
        int OrdenCompra { get; set; }

        /// <summary>
        /// obtener o establecer el numero de orden de produccion relacionada
        /// </summary>
        int OrdenProduccion { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de prdoductos
        /// </summary>
        int IdProducto { get; set; }

        /// <summary>
        /// obtener o establecer el indice del modelo
        /// </summary>
        int IdModelo { get; set; }

        /// <summary>
        /// obtener o establecer la unidad de almacen
        /// </summary>
        int IdUnidad { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad entrada del movimiento
        /// </summary>
        decimal Entrada { get; set; }

        /// <summary>
        /// obtener o establecer la cantidad de salida del movimiento
        /// </summary>
        decimal Salida { get; set; }

        /// <summary>
        /// obtener o establecer la unidad personalizada de la unidad
        /// </summary>
        string Unidad { get; set; }

        /// <summary>
        /// obtener o establecer la descripcion del modelo
        /// </summary>
        string Descripcion { get; set; }

        /// <summary>
        /// obtener o establecer identificador del modelo SKU
        /// </summary>
        string NoIdentificacion { get; set; }

        /// <summary>
        /// obtener o establecer especificacones del modelo
        /// </summary>
        string Especificacion { get; set; }

        /// <summary>
        /// obtener o establecer la marca del modelo
        /// </summary>
        string Marca { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }
    }
}
