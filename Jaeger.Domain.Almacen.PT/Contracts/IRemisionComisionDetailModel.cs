﻿using Jaeger.Domain.Almacen.PT.Entities;
using System;
using System.ComponentModel;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionComisionDetailModel : IRemisionComisionModel {
        #region propiedades de la remision
        /// <summary>
        /// obtener o establecer tipo de documento
        /// </summary>
        int IdTipoDocumento { get; set; }

        /// <summary>
        /// precision decimal
        /// </summary>
        int PrecisionDecimal { get; set; }

        /// <summary>
        /// obtener o establecer la version del comprobante
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer folio de control interno del comprobante
        /// </summary>
        int Folio { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla de series
        /// </summary>
        int IdSerie { get; set; }

        /// <summary>
        /// obtener o establecer la serie de control interno del comprobante
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer status de control interno
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int IdPedido { get; set; }

        /// <summary>
        /// obtener o establecer indice del catalogo de productos
        /// </summary>
        int IdCatalogo { get; set; }

        /// <summary>
        /// obtener o establecer indice del metodo de envio
        /// </summary>
        int IdMetodoEnvio { get; set; }

        /// <summary>
        /// obtener o establecer el indice del domicilio del directorio
        /// </summary>
        int IdDomicilio { get; set; }

        /// <summary>
        /// obtener o establecer numero de guia o referencia del metodo de envio
        /// </summary>
        int NoGuia { get; set; }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        int IdRelacion { get; set; }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del emisor
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer clave de control interno del receptor
        /// </summary>
        string ReceptorClave { get; set; }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer nombre del contacto
        /// </summary>
        string Contacto { get; set; }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de entrega
        /// </summary>
        DateTime? FechaEntrega { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de recepcion de cobranza
        /// </summary>
        DateTime? FechaCobranza { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de pago del comprobante
        /// </summary>
        DateTime? FechaUltPago { get; set; }

        /// <summary>
        /// obtener o establecer fecha de vencimiento del pagare
        /// </summary>
        DateTime? FechaVence { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio
        /// </summary>
        decimal TipoCambio { get; set; }

        /// <summary>
        /// obtener o establecer el subtotal del comprobante
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el importe del descuento (suma de los descuentos aplicados en las partidas de la remision)
        /// </summary>
        decimal TotalDescuento { get; set; }

        /// <summary>
        /// obtener o establer el importe de la remision antes de impuestos (SubToal - TotalDescuentos)
        /// </summary>
        decimal Importe { get; }

        /// <summary>
        /// obtener o establecer el total del impuesto trasladado IVA (suma del iva de las partidas de la remision)
        /// </summary>
        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer la tasa del iva pactado con el cliente
        /// </summary>
        decimal TasaIVAPactado { get; set; }

        /// <summary>
        /// obtener o establecer el total del iva pactado con el cliente (RMSN_IVAPAC)
        /// </summary>
        decimal TotalIVAPactado { get; set; }

        /// <summary>
        /// obtener o establecer el total despues del iva pactado (SubTotal + TotalIVAPactado)
        /// </summary>
        decimal GTotal { get; set; }

        /// <summary>
        /// obtener o establecer el valor del factor pactado para el descuento
        /// </summary>
        decimal FactorPactado { get; set; }

        decimal PorCobrarPactado { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de descuento aplicado
        /// </summary>
        string DescuentoTipo { get; set; }

        /// <summary>
        /// obtener o establecer el factor del descuento aplicado
        /// </summary>
        decimal DescuentoFactor { get; set; }

        /// <summary>
        /// obtener o establecer el importe total del descuento aplicado (GTotal * DescuentoFactor)
        /// </summary>
        decimal DescuentoTotal { get; set; }

        /// <summary>
        /// obtener o establecer el importe por cobrar del comprobante
        /// </summary>
        decimal PorCobrar { get; set; }

        /// <summary>
        /// obtener o establecer el acumulado de la cobranza del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        decimal Saldo { get; set; }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        string ClaveMoneda { get; set; }

        /// <summary>
        /// obtener o establecer la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        string ClaveMetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        string ClaveFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer una nota
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario que cancela el comprobante
        /// </summary>
        string Cancela { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer dias transcurridos desde el ultimo cobro (CTLRMS_DSTRS solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRS")]
        int DiasUltCobro { get; }

        /// <summary>
        /// obtener o establecer dias transcurridos desde la fecha de entrega (CTLRMS_DSTRSA solo lectura)
        /// </summary>
        ///[DataNames("CTLRMS_DSTRSA")]
        int DiasTranscurridos { get; }

        /// <summary>
        /// obtener o establecer los dias transcurridos desde la fecha de cobranza cuando el status es 4, cuando es mayor entonces se utiliza la fecha del ultimo pago
        /// </summary>
        int DiasCobranza { get; }
        #endregion

        /// <summary>
        /// ALTER TABLE CTLRMS ADD CTLRMS_DIVRR COMPUTED BY (((cast(case when ( ctlrms_cbrd$ > 0 and ctlrms_sbttl$ > 0 ) then ctlrms_cbrd$ / (ctlrms_sbttl$+ctlrms_impst$) else -0.00001 end as dom_moneda))))
        /// </summary>
        decimal FactorRealCobrado { get; }

        decimal TotalNotaDescuento { get; }

        /// <summary>
        /// obtener el importe cobrado sin notas de descuento
        /// </summary>
        decimal TotalCobrado { get; }

        BindingList<RemisionMovimientoBancarioModel> Movimientos { get; set; }
    }
}