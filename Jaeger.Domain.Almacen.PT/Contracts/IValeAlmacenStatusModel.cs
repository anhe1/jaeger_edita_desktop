﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IValeAlmacenStatusModel {
        /// <summary>
        /// obtener o establecer indice (ALMMP_ID)
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        int IdStatusB { get; set; }

        /// <summary>
        /// obtener o establecer indice de la clave del motivo de modificacion del status
        /// </summary>
        int IdCveMotivo { get; set; }

        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer la clave del motivo de modificacion del status
        /// </summary>
        string CveMotivo { get; set; }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del usuario creador del registro
        /// </summary>
        string Creo { get; set; }
    }
}
