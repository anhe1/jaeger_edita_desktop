﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    /// <summary>
    /// movimientos del almacen de producto terminado
    /// </summary>
    public interface ISqlMovimientoAlmacenPT : IGenericRepository<MovimientoModel> {
        MovimientoDetailModel Save(MovimientoDetailModel model);

        ValeAlmacenConceptoModel Save(ValeAlmacenConceptoModel vale);

        IEnumerable<MovimientoDetailModel> GetList(List<IConditional> condidional);


        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        bool Existencia(List<IConditional> conditionals);
    }
}
