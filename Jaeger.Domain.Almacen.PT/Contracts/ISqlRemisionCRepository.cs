﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    /// <summary>
    /// comisiones a vendedor
    /// </summary>
    public interface ISqlRemisionCRepository : IGenericRepository<RemisionComisionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> condidional) where T1 : class, new();

        /// <summary>
        /// lista de remisiones-movimientos bancarios
        /// </summary>
        IEnumerable<T1> GetToList<T1>(List<IConditional> condidional) where T1 : class, new();

        RemisionComisionModel Save(RemisionComisionModel item);

        int Saveable(RemisionComisionModel item);

        /// <summary>
        /// actualizar bandera de comisiones
        /// </summary>
        /// <param name="idRemision"></param>
        /// <param name="value">0 <-no esta agregada a ningun recibo, 1 <- comsion autorizada y esta agregada a un recibo, 2 <-comision pagada y ya esta autorizada en un recibo</param>
        bool Update(int[] idRemision, int value);
    }
}
