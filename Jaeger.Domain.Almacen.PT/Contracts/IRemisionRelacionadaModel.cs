﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionRelacionadaModel {
        /// <summary>
        /// obtener o establecer indice de la tabla
        /// </summary>
        //int IdRelacion { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion de la remision
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer el indice del catalogo de tipos de relacion
        /// </summary>
        int IdTipoRelacion { get; set; }

        /// <summary>
        /// obtener o establecer descripcion o motivo de la relacion
        /// </summary>
        string Relacion { get; set; }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        string IdDocumento { get; set; }

        string Serie { get; set; }

        int Folio { get; set; }

        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de emision del comprobante
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer el total del comprobante
        /// </summary>
        decimal GTotal { get; set; }

        /// <summary>
        /// obtener o establecer fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer la clave del usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la ultima fecha de modificacion del registro
        /// </summary>
        //DateTime? FechaModifica { get; set; }

        /// <summary>
        /// obtener o establecer la clave del ultimo usuario que modifica
        /// </summary>
        //string Modifica { get; set; }
    }
}