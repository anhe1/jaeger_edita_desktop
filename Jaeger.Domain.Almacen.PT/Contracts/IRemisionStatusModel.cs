﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionStatusModel {
        /// <summary>
        /// obtener o establecer indice incremental
        /// </summary>
        int IdAuto { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obte
        /// </summary>
        int IdRemision { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla de clientes
        /// </summary>
        int IdCliente { get; set; }

        int IdStatusA { get; set; }
        
        int IdStatusB { get; set; }

        /// <summary>
        /// obtener o establecer la clave del motivo del cambio de status
        /// </summary>
        string CvMotivo { get; set; }

        /// obtener o establecer observaciones del registro
        /// </summary>
        /// <summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o estalecer fecha de creacion
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer usuario
        /// </summary>
        string User { get; set; }
    }
}