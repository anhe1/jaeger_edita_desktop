﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionComisionModel {
        bool Activo { get; set; }
        string Comision { get; set; }
        decimal ComisionFactor { get; set; }
        decimal ComisionFactorActualizado { get; set; }
        decimal ComisionPagada { get; set; }
        decimal ComisionPorPagar { get; set; }
        DateTime FechaModifica { get; set; }
        int IdCliente { get; set; }
        int IdComision { get; set; }
        int IdRemision { get; set; }
        int IdVendedor { get; set; }
        int IsDisponible { get; set; }
        string Modifica { get; set; }
        string Vendedor { get; set; }
    }
}