﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IValeAlmacenModel : Base.Contracts.IEntityBase {
        /// <summary>
        /// obtener o establecer indice (ALMPT_ID)
        /// </summary>
        int IdComprobante { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int IdAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer incide de tipo de almacen (MP, PT, DP, PP)
        /// </summary>
        int IdTipoAlmacen { get; set; }

        /// <summary>
        /// obtener o establecer la version del documento
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer indice del tipo de documento
        /// </summary>
        int IdTipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer efecto del comprobante
        /// </summary>
        int IdTipoMovimiento { get; set; }

        /// <summary>
        /// obtenero o establcer indice del tipo de devolucion
        /// </summary>
        int IdCveDevolucion { get; set; }

        /// <summary>
        /// obtener o establecer el indice del status del documento
        /// </summary>
        int IdStatus { get; set; }

        /// <summary>
        /// obtener o establecer el Id de documento
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer el indice o clave de relacion con otros comprobantes
        /// </summary>
        int IdRelacion { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer indice del emisor
        /// </summary>
        int IdDepartamento { get; set; }

        /// <summary>
        /// obtener o establecer folio de control interno
        /// </summary>
        int Folio { get; set; }

        /// <summary>
        /// obtener o establecer indice de serie
        /// </summary>
        int IdSerie { get; set; }

        /// <summary>
        /// obtener o establecer serie de control interno
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer fecha de emision
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer RFC del receptor
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o denominacion del receptor
        /// </summary>
        string Receptor { get; set; }   

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Contacto { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        string Referencia { get; set; }

        /// <summary>
        /// obtener o establecer fecha de ingreso al almacen
        /// </summary>
        DateTime? FechaIngreso { get; set; }

        /// <summary>
        /// obtener o establecer sub total
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el total del descuento
        /// </summary>
        decimal TotalDescuento { get; set; }

        /// <summary>
        /// obtener o establecer
        /// </summary>
        decimal TotalTrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer Total = SubTotal + TotalTrasladoIVA
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer observaciones
        /// </summary>
        string Nota { get; set; }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        string UrlFilePDF { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio del catalogo de vendedores
        /// </summary>
        int IdVendedor { get; set; }

        /// <summary>
        /// obtener o establecer la clave del vendedor asociada
        /// </summary>
        string Vendedor { get; set; }

        int Ejercicio { get; set; }

        int Periodo { get; set; }
    }
}
