﻿namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionSingleModel : IRemisionModel, IRemisionDetailModel {
        
    }
}
