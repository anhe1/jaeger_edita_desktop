﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IDevolucionDetailModel : IValeAlmacenModel {
        DevolucionPTStatusEnum Status { get; set; }
        TipoComprobanteEnum TipoComprobante { get; set; }
        string StatusText { get; }
        bool IsEditable { get; }
        string KeyName {  get; }

        BindingList<ValeAlmacenConceptoModel> Conceptos { get; set; }

        void SetValues(DevolucionDetailModel source);
    }
}
