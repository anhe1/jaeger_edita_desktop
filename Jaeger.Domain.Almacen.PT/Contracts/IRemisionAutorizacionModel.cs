﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionAutorizacionModel {
        string CvCancela { get; set; }
        DateTime Fecha { get; set; }
        int Id { get; set; }
        int IdRemision { get; set; }
        int IdStatus { get; set; }
        string Nota { get; set; }
        string Usuario { get; set; }
    }
}