﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    /// <summary>
    /// repositorio de relaciones entre remisiones
    /// </summary>
    public interface ISqlRemisionadoRRepository : IGenericRepository<RemisionRelacionadaModel> {
        IEnumerable<RemisionRelacionadaModel> GetList(List<IConditional> condidional);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        //RemisionRelacionadaModel Save(RemisionRelacionadaModel model);
        int Saveable(RemisionRelacionadaModel item);
    }
}
