﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface ISqlRemisionStatusRepository : IGenericRepository<RemisionStatusModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> condidional) where T1 : class, new();

        RemisionStatusModel Save(RemisionStatusModel item);

        int Saveable(RemisionStatusModel item);
    }
}
