﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionConceptoDetailModel : IMovimientoModel {
        TipoMovimientoEnum Efecto { get; set; }
        string GetOriginalString { get; }
        BindingList<RemisionConceptoParte> Partes { get; set; }
        TipoComprobanteEnum Tipo { get; set; }

        new RemisionConceptoDetailModel Clone();
    }
}