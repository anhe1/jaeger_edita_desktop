﻿namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionReceptorModel {
        /// <summary>
        /// obtener o establecer el indice del directorio
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// obtener o establecer registro federal de contribuyentes del receptor
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o razon social del receptor
        /// </summary>
        string ReceptorNombre { get; set; }
    }
}
