﻿using System;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IValeAlmacenRelacionModel {
        int IdComprobante { get; set; }

        int IdTipoComprobante { get; set; }

        int IdClaveRelacion { get; set; }

        string ClaveRelacion { get; set; }

        string IdDocumento { get; set; }

        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>

        int IdDirectorio { get; set; }

        int Folio { get; set; }

        string Serie { get; set; }

        DateTime FechaEmision { get; set; }

        string Clave { get; set; }

        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre o denominacion del receptor
        /// </summary>

        string Receptor { get; set; }

        string Creo { get; set; }

        DateTime FechaNuevo { get; set; }
    }
}
