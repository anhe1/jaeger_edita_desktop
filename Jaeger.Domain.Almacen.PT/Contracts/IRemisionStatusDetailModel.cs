﻿namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionStatusDetailModel : IRemisionStatusModel {
        /// <summary>
        /// obtener o establecer relaciones con otras remisiones
        /// </summary>
        System.ComponentModel.BindingList<IRemisionRelacionadaModel> Relaciones { get; set; }
    }
}