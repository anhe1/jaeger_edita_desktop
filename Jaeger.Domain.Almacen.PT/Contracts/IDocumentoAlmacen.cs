﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IDocumentoAlmacen : IComprobanteAlmacen {
        TipoComprobanteEnum TipoComprobante { get; set; }
        BindingList<ItemSelectedModel> ReceptorTipos { get; set; }
    }
}
