﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Almacen.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IValeAlmacenDetailModel : IValeAlmacenModel {
        ValeAlmacenStatusEnum Status { get; set; }
        TipoMovimientoEnum TipoMovimiento { get; set; }
        TipoComprobanteEnum TipoComprobante { get; set; }
        BindingList<ValeAlmacenConceptoModel> Conceptos { get; set; }
        BindingList<ValeAlmacenStatusModel> Autorizacion { get; set; }
        BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
        bool IsEditable { get; }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        string KeyName { get; }

        string GetOriginalString { get; }

        bool HasConcepts();

        bool AddConcepto(ValeAlmacenConceptoModel nuevo);
    }
}
