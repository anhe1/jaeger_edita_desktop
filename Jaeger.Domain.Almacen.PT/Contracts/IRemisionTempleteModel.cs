﻿namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionTempleteModel {
        string Name { get; set; }
        string Description { get; set; }
        string Templete { get; set; }
    }
}
