﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IValeAlmacenStatusDetailModel : IValeAlmacenStatusModel {
        BindingList<ValeAlmacenRelacionModel> Relaciones { get; set; }
    }
}
