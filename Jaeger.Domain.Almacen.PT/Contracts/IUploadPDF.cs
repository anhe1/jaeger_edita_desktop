﻿namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IUploadPDF {
        int IdRemision { get; set; }
        string UrlFilePDF { get; set; }
    }
}
