﻿using Jaeger.Domain.Almacen.PT.Entities;
using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface ISqlValeRelacionRepository : IGenericRepository<ValeAlmacenRelacionModel> {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        int Save(IValeAlmacenRelacionModel item);
    }
}
