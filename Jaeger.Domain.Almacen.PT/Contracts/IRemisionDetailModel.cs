﻿using System.ComponentModel;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface IRemisionDetailModel : IRemisionModel {
        /// <summary>
        /// obtener o establecer status de la remision
        /// </summary>
        RemisionStatusEnum Status { get; set; }

        /// <summary>
        /// obtener status en modo texto
        /// </summary>
        string StatusText { get; }

        /// <summary>
        /// obtener estado del comprobante
        /// </summary>
        string Estado { get; }

        RemisionFormatoEnum Formato { get; set; }

        decimal Saldo { get; }

        /// <summary>
        /// obtener si el comprobante es editable
        /// </summary>
        bool IsEditable { get; }

        /// <summary>
        /// obtener keyname del documento
        /// </summary>
        string KeyName { get; }

        string GetOriginalString { get; }

        /// <summary>
        /// lista de remisiones relacionadas
        /// </summary>
        BindingList<RemisionRelacionadaModel> RemisionRelacionada { get; set; }

        /// <summary>
        /// obtener o establecer la lista de conceptos de la remision
        /// </summary>
        BindingList<RemisionConceptoDetailModel> Conceptos { get; set; }

        /// <summary>
        /// obtener o establecer listado de autorizaciones
        /// </summary>
        BindingList<RemisionStatusModel> Autorizaciones { get; set; }

        void SetValues(RemisionDetailModel source);
    }
}
