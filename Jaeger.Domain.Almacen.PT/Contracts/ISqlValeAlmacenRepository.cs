﻿using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Contracts;
using System.Collections.Generic;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface ISqlValeAlmacenRepository : IGenericRepository<ValeAlmacenModel> {
        /// <summary>
        /// obtener un comprobante de almacen (vale del almacen)
        /// </summary>
        /// <param name="index">indice de la llave primaria</param>
        /// <returns>devuelve un objeto ViewModelValeAlmacen</returns>
        new ValeAlmacenDetailModel GetById(int index);

        /// <summary>
        /// almacenar un comprobante del almacen (vale de almacen)
        /// </summary>
        /// <param name="item"></param>
        /// <returns>devuelve el mismo objeto enviado con los valores actualizados</returns>
        IValeAlmacenDetailModel Save(IValeAlmacenDetailModel item);

        /// <summary>
        /// crear tablas del sistema de almacen
        /// </summary>
        /// <returns>verdadero si la tarea se completa correctamente.</returns>
        bool CrearTablas();

        /// <summary>
        /// calcular el numero consecutivo del recibo segun el tipo de comprobante
        /// </summary>
        /// <param name="item">objteo ViewModelValeAlmacen</param>
        /// <returns>devuelve un numero entero que representa el nuevo folio consecutivo para el comprobante.</returns>
        int Folio(IValeAlmacenModel item);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
