﻿using System.Collections.Generic;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    /// <summary>
    /// Configuracion
    /// </summary>
    public interface IConfiguration {
        bool ShowDomicilio { get; set; }

        bool ShowTotalConLetra { get; set; }
        
        bool ShowPagare { get; set; }

        /// <summary>
        /// obtener o establecer folder de almacenamiento de archivos
        /// </summary>
        string Folder { get; set; }

        string Templete { get; set; }

        string Pagare { get; set; }

        string Leyenda1 { get; set; }

        string Leyenda2 { get; set; }

        //List<IRemisionTempleteModel> Templetes { get; set;}
    }
}
