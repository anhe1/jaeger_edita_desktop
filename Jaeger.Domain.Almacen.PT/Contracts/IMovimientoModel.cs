﻿namespace Jaeger.Domain.Almacen.PT.Contracts { 
    public interface IMovimientoModel : Almacen.Contracts.IMovimientoModel {
        /// <summary>
        /// obtener o establecer indice del directorio
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// obtener o establecer el numero de orden de compra relacionada
        /// </summary>
        int OrdenCompra { get; set; }

        /// <summary>
        /// obtener o establecer el numero de orden de produccion relacionada
        /// </summary>
        int OrdenProduccion { get; set; }

        decimal Cantidad { get; set; }

        /// <summary>
        /// obtener o establecer el nombre o descripcion del catalogo de productos
        /// </summary>
        string Catalogo { get; set; }

        /// <summary>
        /// obtener o establecer nombre o descripcion del tamanio
        /// </summary>
        string Tamanio { get; set; }

        /// <summary>
        /// obtener la concatenacion de producto + modelo + tamanio
        /// </summary>
        string Descripcion { get; }

        object Clone();
    }
}