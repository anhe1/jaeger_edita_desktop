﻿using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Almacen.PT.Contracts {
    public interface ISqlValeStatusRepository : IGenericRepository<ValeAlmacenStatusModel> {
        IValeAlmacenStatusModel Save(IValeAlmacenStatusModel item);
    }
}
