﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.DP.Builder {
    /// <summary>
    /// Clase builder para creacion de grid de remisionado
    /// </summary>
    public class RemisionGridBuilder : GridViewBuilder, IGridViewBuilder, IDisposable, IRemisionGridBuilder, IRemisionTempletesGridBuilder, IRemisionColumnsGridBuilder {
        public RemisionGridBuilder() : base() { }

        #region templetes
        public IRemisionTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IRemisionTempletesGridBuilder Master() {
            this.Version().Serie().Folio().IdStatus().ReceptorNombre().Contacto().FechaEmision().FechaEntrega().Cancela().FechaCancela().FechaCalidad().Auditor().UsoRemision().IdDocumento().Creo();
            return this;
        }

        public IRemisionTempletesGridBuilder Conceptos() {
            this._Columns.AddRange(new RemisionConceptoGridBuilder().Templetes().Master().Build());
            return this;
        }

        public IRemisionTempletesGridBuilder GetPartidas() {
            this.Version().Serie().Folio().IdStatus().ReceptorNombre().Contacto().FechaEmision().FechaEntrega().Cancela().FechaCancela().FechaCalidad().Auditor().UsoRemision();
            this._Columns.AddRange(new RemisionConceptoGridBuilder().Cantidad().Unidad().IdPedido().Cliente().Producto().Identificador().Build());
            this.IdDocumento().Creo();
            return this;
        }

        /// <summary>
        /// combinacion de informacion de remsion y conceptos
        /// </summary>
        /// <returns></returns>
        public IRemisionTempletesGridBuilder RemisionConceptos() {
            this.Folio().IdStatus().FechaEmision();
            var conceptos = new RemisionConceptoGridBuilder().Cantidad().Unidad().IdPedido().Cliente().Producto().Identificador().Build();
            this._Columns.AddRange(conceptos);
            this.IdDocumento().Creo();
            return this;
        }
        #endregion

        #region columnas
        public IRemisionColumnsGridBuilder Version() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Version",
                HeaderText = "Ver.",
                Name = "Version",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Serie() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Serie",
                HeaderText = "Serie",
                Name = "Serie",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                FormatString = FormarStringFolio,
                ReadOnly = true
            });
            return this;
        }

        /// <summary>
        /// combobox con los status
        /// </summary>
        public IRemisionColumnsGridBuilder IdStatus() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(string),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75,
                DisplayMember = "Descriptor",
                ValueMember = "Id",
                ReadOnly = true,
                DataSource = Aplication.Almacen.DP.Services.RemisionService.GetStatus()
            });
            return this;
        }

        public IRemisionColumnsGridBuilder ReceptorNombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ReceptorNombre",
                HeaderText = "Cliente",
                IsPinned = true,
                Name = "ReceptorNombre",
                Width = 280,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Contacto() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "Contacto",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Recibe",
                Name = "contacto",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 140
            });
            return this;
        }

        public IRemisionColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEmision",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IRemisionColumnsGridBuilder FechaEntrega() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaEntrega",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Entrega",
                Name = "FechaEntrega",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Cancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cancela",
                HeaderText = "Usu. Cancela",
                IsVisible = false,
                Name = "Cancela",
                Width = 75
            });
            return this;
        }

        public IRemisionColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaCancela",
                FormatString = GridTelerikCommon.FormatStringDate,
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IRemisionColumnsGridBuilder FechaCalidad() {
            this._Columns.Add(new GridViewDateTimeColumn {
                Name = "FechaCalidad",
                FieldName = "FechaCalidad",
                HeaderText = "Fec. Calidad",
                Width = 75,
                FormatString = GridTelerikCommon.FormatStringDate,
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Auditor() {
            this._Columns.Add(new GridViewTextBoxColumn {
                Name = "Auditor",
                FieldName = "Auditor",
                HeaderText = "Auditor",
                Width = 75
            });
            return this;
        }

        public IRemisionColumnsGridBuilder UsoRemision() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdTipoDocumento",
                HeaderText = "Tipo",
                Name = "IdTipoDocumento",
                Width = 140
            });
            return this;
        }

        public IRemisionColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                Name = "IdDocumento",
                ReadOnly = true,
                Width = 180
            });
            return this;
        }

        public IRemisionColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                ReadOnly = true
            });
            return this;
        }
        #endregion

        /// <summary>
        /// Item Crear Remision Interna
        /// </summary>
        /// <returns>RadMenuItem</returns>
        public static RadMenuItem MenuItemRemision() {
            return new RadMenuItem { 
                Text = "Crear Remisión (Interna)", 
                Name = "cppro_brem_emitido" 
            };
        }
    }
}
