﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.Almacen.DP.Builder {
    public class RemisionConceptoGridBuilder : GridViewBuilder, IGridViewBuilder, IDisposable, IRemisionConceptoGridBuilder , IRemisionConceptoTempletesGridBuilder, IRemisionConceptoColumnsGridBuilder {
        public RemisionConceptoGridBuilder() : base() { }

        public IRemisionConceptoTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public IRemisionConceptoTempletesGridBuilder Master() {
            this.Cantidad().Unidad().ValorUnitario().IdPedido().Cliente().Producto().Componente().SubTotal().TasaIVA().TrasladoIVA().Importe().Identificador();
            return this;
        }

        #region columnas
        public IRemisionConceptoColumnsGridBuilder Cantidad() {
            this._Columns.Add(new GridViewDecimalColumn {
                DataType = typeof(decimal),
                FieldName = "Cantidad",
                FormatString = "{0:N0}",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 65
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Unidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Unidad",
                HeaderText = "Unidad",
                Name = "Unidad",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Unitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Unitario",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Unitario",
                Name = "Unitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder ValorUnitario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "ValorUnitario",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Unitario",
                Name = "ValorUnitario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder IdPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdPedido",
                FormatString = "{0:N0}",
                HeaderText = "#Orden",
                Name = "IdPedido",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Cliente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cliente",
                HeaderText = "Cliente",
                IsPinned = true,
                Name = "Cliente",
                Width = 280,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Producto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                HeaderText = "Producto",
                FieldName = "Producto",
                Name = "Producto",
                Width = 340
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Componente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                HeaderText = "Componente",
                FieldName = "Componente",
                Name = "Componente",
                Width = 240,
                ReadOnly = true
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder SubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder TasaIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaIVA",
                FormatString = GridTelerikCommon.FormatStringP,
                HeaderText = "% IVA",
                Name = "TasaIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder TrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Tras. IVA",
                Name = "TrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = GridTelerikCommon.FormatStringMoney,
                HeaderText = "Importe",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IRemisionConceptoColumnsGridBuilder Identificador() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Identificador",
                HeaderText = "Identificador",
                Name = "Identificador",
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 115
            });
            return this;
        }
        #endregion
    }
}
