﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.DP.Builder {
    /// <summary>
    /// interface builder para creacion de grid de remisionado
    /// </summary>
    public interface IRemisionGridBuilder : IGridViewBuilder, IDisposable {
        IRemisionTempletesGridBuilder Templetes();
    }

    public interface IRemisionTempletesGridBuilder : IGridViewBuilder {
        IRemisionTempletesGridBuilder Master();
        IRemisionTempletesGridBuilder Conceptos();
        IRemisionTempletesGridBuilder GetPartidas();
        IRemisionTempletesGridBuilder RemisionConceptos();
    }

    public interface IRemisionColumnsGridBuilder : IGridViewBuilder  {
        IRemisionColumnsGridBuilder Version();
        IRemisionColumnsGridBuilder Serie();
        IRemisionColumnsGridBuilder Folio();

        /// <summary>
        /// combobox con los status
        /// </summary>
        IRemisionColumnsGridBuilder IdStatus();

        IRemisionColumnsGridBuilder ReceptorNombre();
        IRemisionColumnsGridBuilder Contacto();
        IRemisionColumnsGridBuilder FechaEmision();
        IRemisionColumnsGridBuilder FechaEntrega();
        IRemisionColumnsGridBuilder Cancela();
        IRemisionColumnsGridBuilder FechaCancela();
        IRemisionColumnsGridBuilder FechaCalidad();
        IRemisionColumnsGridBuilder Auditor();
        IRemisionColumnsGridBuilder UsoRemision();
        IRemisionColumnsGridBuilder IdDocumento();
        IRemisionColumnsGridBuilder Creo();
    }
}
