﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Almacen.DP.Builder {
    public interface IRemisionConceptoGridBuilder : IGridViewBuilder, IDisposable {
        IRemisionConceptoTempletesGridBuilder Templetes();
    }
    public interface IRemisionConceptoTempletesGridBuilder : IGridViewBuilder {
        IRemisionConceptoTempletesGridBuilder Master();
    }

    public interface IRemisionConceptoColumnsGridBuilder : IGridViewBuilder {
        IRemisionConceptoColumnsGridBuilder Cantidad();
        IRemisionConceptoColumnsGridBuilder Unidad();
        IRemisionConceptoColumnsGridBuilder Unitario();
        IRemisionConceptoColumnsGridBuilder ValorUnitario();
        IRemisionConceptoColumnsGridBuilder IdPedido();
        IRemisionConceptoColumnsGridBuilder Cliente();
        IRemisionConceptoColumnsGridBuilder Producto();
        IRemisionConceptoColumnsGridBuilder Componente();
        IRemisionConceptoColumnsGridBuilder SubTotal();
        IRemisionConceptoColumnsGridBuilder TasaIVA();
        IRemisionConceptoColumnsGridBuilder TrasladoIVA();
        IRemisionConceptoColumnsGridBuilder Importe();
        IRemisionConceptoColumnsGridBuilder Identificador();
    }
}
