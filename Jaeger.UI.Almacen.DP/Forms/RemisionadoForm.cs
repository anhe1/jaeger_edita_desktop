﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Almacen.DP.Builder;
using Jaeger.Aplication.Almacen.DP.Services;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Almacen.DP.ValueObjects;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.DP.Forms {
    /// <summary>
    /// Remisionado de Control Interno (Departamento)
    /// </summary>
    public partial class RemisionadoForm : RadForm {
        protected internal BindingList<RemisionDetailModel> _DataSource;
        protected internal Aplication.Almacen.DP.Contracts.IRemisionadoService Service;
        protected internal GridViewTemplate GridConceptos = new GridViewTemplate() { Caption = "Conceptos" };
        protected internal RadMenuItem ImprimirComprobante = new RadMenuItem { Text = "Comprobante" };
        protected internal RadMenuItem ImprimirListado = new RadMenuItem { Text = "Listado" };
        protected internal RadMenuItem PorPartida = new RadMenuItem { Text = "Por Partida" };

        public RemisionadoForm() {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public RemisionadoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this.CreateView();
            this.OnLoad();

            this.TRemision.Herramientas.Items.Add(this.PorPartida);
            this.TRemision.Editar.Visibility = ElementVisibility.Collapsed;
            this.TRemision.Imprimir.Items.Add(this.ImprimirComprobante);
            this.TRemision.Imprimir.Items.Add(this.ImprimirListado);

            this.TRemision.Nuevo.Click += this.TRemision_Nuevo_Click;
            this.TRemision.Editar.Click += this.TRemision_Editar_Click;
            this.TRemision.Cancelar.Click += this.TRemision_Cancelar_Click;
            this.TRemision.Actualizar.Click += this.TRemision_Actualizar_Click;
            this.TRemision.Cerrar.Click += this.TRemision_Cerrar_Click;
            this.TRemision.ContextImprimir.Click += this.TRemision_Imprimir_Click;
            this.TRemision.ContextCancelar.Click += this.TRemision_Cancelar_Click;

            this.ImprimirComprobante.Click += this.TRemision_Imprimir_Click;
            this.ImprimirListado.Click += this.TRemision_ImprimirListado_Click;
            this.PorPartida.Click += this.PorPartida_Click;
        }

        public virtual void OnLoad() {

        }

        public virtual void CreateView() {
            IRemisionGridBuilder gridBuilder = new RemisionGridBuilder();
            this.TRemision.GridData.Columns.AddRange(gridBuilder.Templetes().Master().Build());
            this.GridConceptos.Columns.AddRange(gridBuilder.Templetes().Conceptos().Build());
            this.GridConceptos.Standard();

            this.TRemision.GridData.MasterTemplate.Templates.Add(this.GridConceptos);
            
            var status = this.TRemision.GridData.Columns[GridTelerikCommon.ColStatus.Name] as GridViewComboBoxColumn;
            status.DataSource = RemisionService.GetStatus();

            var usoRemision = this.TRemision.GridData.Columns["IdTipoDocumento"] as GridViewComboBoxColumn;
            usoRemision.DisplayMember = "Descriptor";
            usoRemision.ValueMember = "Id";
            usoRemision.DataSource = RemisionService.GetUsos();

            this.TRemision.GridData.CellBeginEdit += this.CellBeginEdit;
            this.TRemision.GridData.CellEndEdit += this.CellEndEdit;
            this.TRemision.GridData.AllowEditRow = true;

            this.TRemision.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
            this.GridConceptos.HierarchyDataProvider = new GridViewEventDataProvider(this.GridConceptos);
        }

        #region barra de herramientas
        public virtual void TRemision_Nuevo_Click(object sender, EventArgs e) { }

        public virtual void TRemision_Editar_Click(object sender, EventArgs e) { }

        public virtual void TRemision_Cancelar_Click(object sender, EventArgs e) {
            if (this.TRemision.GridData.CurrentRow != null) {
                var seleccionado = this.TRemision.GridData.CurrentRow.DataBoundItem as RemisionDetailModel;
                if (seleccionado != null) {
                    if (seleccionado.IdStatus == 0) {
                        RadMessageBox.Show(this, "¡El comprobante seleccionado ya se encuentra cancelado!", "Atención", MessageBoxButtons.OK, RadMessageIcon.Error);
                    } else {
                        if (RadMessageBox.Show(this, "Esta seguro de cancelar? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                            var cancela = new RemisionStatusForm(seleccionado);
                            cancela.Selected += this.TRemision_Cancela_Selected;
                            cancela.ShowDialog(this);
                        }
                    }
                }
            }
        }

        public virtual void TRemision_Cancela_Selected(object sender, RemisionCancelacionModel e) {
            if (e != null) {
                this.Tag = e;
                using (var espera = new Waiting1Form(this.Cancelar)) {
                    espera.Text = "Solicitando cancelación ...";
                    espera.ShowDialog(this);
                }
            }
        }

        public virtual void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.TRemision.GridData.DataSource = this._DataSource;
        }

        public virtual void TRemision_Imprimir_Click(object sender, EventArgs e) {
            if (this.TRemision.GridData.CurrentRow != null) {
                var _remision = this.TRemision.GridData.CurrentRow.DataBoundItem as RemisionDetailModel;
                if (_remision != null) {
                    var _d1 = this.Service.GetPrinter(_remision.IdRemision);
                    if (_d1 != null) {
                        var _reporte = new ReporteForm(_d1);
                        _reporte.Show();
                    }
                }
            }
        }

        public virtual void TRemision_ImprimirListado_Click(object sender, EventArgs e) {
            if (this.TRemision.GridData.ChildRows.Count > 0) {
                var seleccion = new List<RemisionDetailModel>();
                var printers = new List<RemisionDetailPrinter>();
                seleccion.AddRange(this.TRemision.GridData.ChildRows.Select(it => it.DataBoundItem as RemisionDetailModel));
                if (seleccion.Count > 0) {
                    foreach (var item in seleccion) {
                        printers.Add(new RemisionDetailPrinter(item));
                    }

                    var reporte = new ReporteForm(printers);
                    reporte.Show();
                }
            }
        }

        

        public virtual void PorPartida_Click(object sender, EventArgs e) {

        }

        public virtual void TRemision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var rowview = e.ParentRow.DataBoundItem as RemisionDetailModel;
            if (e.Template.Caption == this.GridConceptos.Caption) {
                if (rowview != null) {
                    using (var espera = new Waiting1Form(this.GetConceptos)) {
                        espera.Text = "Consultando conceptos ...";
                        espera.ShowDialog(this);
                    }

                    var tabla = rowview.Conceptos;
                    if (tabla != null) {
                        foreach (var item in tabla) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Cantidad"].Value = item.Cantidad;
                            row.Cells["Unidad"].Value = item.Unidad;
                            row.Cells["IdPedido"].Value = item.IdPedido;
                            row.Cells["Cliente"].Value = item.Cliente;
                            row.Cells["Producto"].Value = item.Producto;
                            row.Cells["Identificador"].Value = item.Identificador;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        private void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (e.Row is GridViewFilteringRowInfo) {
                return;
            }
            var seleccionado = e.Row.DataBoundItem as RemisionDetailModel;
            if (e.Column.Name == "FechaCalidad") {
                if (seleccionado.FechaCalidad != null) { return; }
                var d = e.ActiveEditor as RadDateTimeEditor;
                d.MinValue = seleccionado.FechaEmision;
            } else {
                e.Cancel = true;
            }
        }

        private void CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (e.Row is GridViewFilteringRowInfo) {
                return;
            }
            if (e.Column.Name == "FechaCalidad") {
                var seleccionado = e.Row.DataBoundItem as RemisionDetailModel;
                if (seleccionado.FechaCalidad == null) { return; }
                using (var espera = new Waiting1Form(this.FechaCalidad)) {
                    espera.Text = "Actualizando, espere un momento ...";
                    espera.ShowDialog(this);
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void Consultar() {
            this._DataSource = new BindingList<RemisionDetailModel>(this.Service.GetList<RemisionDetailModel>(
                RemisionService.Query().Year(this.TRemision.GetEjercicio()).Month(this.TRemision.GetPeriodo()).Build())
                );
        }

        public virtual void GetConceptos() {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                seleccionado.Conceptos = this.Service.GetPartidas(seleccionado.IdRemision);
            }
        }

        public virtual void Cancelar() {
            var cancelacion = (RemisionCancelacionModel)this.Tag;
            if (this.Service.Cancelar(cancelacion)) {
                var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
                seleccionado.Cancelacion = cancelacion;
            }
        }

        public virtual void FechaCalidad() {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (seleccionado != null) {
                var status = new RemisionStatusModel {
                    IdRemision = seleccionado.IdRemision,
                    IdStatus = (int)RemisionStatusEnum.Recibido,
                };

                if (this.Service.SetStatus(status)) {
                    seleccionado.IdStatus = status.IdStatus;
                    seleccionado.FechaModifica = DateTime.Now;
                    seleccionado.Modifica = ConfigService.Piloto.Clave;
                }
            }
        }
        #endregion
    }
}
