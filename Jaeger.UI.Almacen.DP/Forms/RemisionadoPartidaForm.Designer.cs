﻿
namespace Jaeger.UI.Almacen.DP.Forms {
    partial class RemisionadoPartidaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TRemision = new Jaeger.UI.Common.Forms.GridCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TRemision
            // 
            this.TRemision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TRemision.Location = new System.Drawing.Point(0, 0);
            this.TRemision.Name = "TRemision";
            this.TRemision.ShowActualizar = true;
            this.TRemision.ShowAutosuma = true;
            this.TRemision.ShowCancelar = false;
            this.TRemision.ShowCerrar = true;
            this.TRemision.ShowEditar = true;
            this.TRemision.ShowEjercicio = true;
            this.TRemision.ShowExportarExcel = false;
            this.TRemision.ShowFiltro = true;
            this.TRemision.ShowHerramientas = false;
            this.TRemision.ShowImprimir = false;
            this.TRemision.ShowNuevo = true;
            this.TRemision.ShowPeriodo = true;
            this.TRemision.ShowSeleccionMultiple = true;
            this.TRemision.Size = new System.Drawing.Size(800, 450);
            this.TRemision.TabIndex = 0;
            // 
            // RemisionadoPartidaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TRemision);
            this.Name = "RemisionadoPartidaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DP: Remisión Partida";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RemisionadoPartidaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.GridCommonControl TRemision;
    }
}