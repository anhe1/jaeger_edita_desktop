﻿using System;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.DP.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Almacen.DP.Forms {
    public partial class RemisionStatusForm : RadForm {
        protected RemisionDetailModel _current;
        public RemisionStatusForm() {
            InitializeComponent();
        }

        public RemisionStatusForm(RemisionDetailModel model) {
            InitializeComponent();
            this._current = model;
        }

        private void RemisionInternaStatusForm_Load(object sender, EventArgs e) {
            this.MotivoCancelacion.DataSource = RemisionService.GetMotivosCancelacion();
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.SetEditable(false);
            this.IdDocumento.Text = this._current.Folio.ToString();
            this.Cliente.Text = this._current.ReceptorNombre;
            this.IdStatus.DataSource = RemisionService.GetStatus();
            this.TCancelar.Autorizar.Text = "Cancelar";
            this.TCancelar.Autorizar.Click += this.TCancelar_Autorizar_Click;
            this.TCancelar.Cerrar.Click += this.TCancelar_Cerrar_Click;
        }

        public event EventHandler<RemisionCancelacionModel> Selected;
        public void OnSelected(RemisionCancelacionModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        #region barra de herramientas
        private void TCancelar_Autorizar_Click(object sender, EventArgs e) {
            var _status = this.IdStatus.SelectedItem.DataBoundItem as StatusModel;
            var _seleccionado = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as RemisionMotivoCancelacionModel;
            if (_seleccionado != null) {
                var _response = new RemisionCancelacionModel {
                    IdRemision = _current.IdRemision,
                    ClaveCancelacion = _seleccionado.Descriptor,
                    ClaveCancela = _seleccionado.Id,
                    Cancela = ConfigService.Piloto.Clave,
                    FechaCancela = DateTime.Now,
                    IdStatus = _status.Id,
                    NotaCancelacion = this.Nota.Text
                };
                this.OnSelected(_response);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void TCancelar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion
    }
}
