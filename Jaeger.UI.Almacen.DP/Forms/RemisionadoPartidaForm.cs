﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Aplication.Almacen.DP.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.DP.Services;
using Jaeger.UI.Almacen.DP.Builder;

namespace Jaeger.UI.Almacen.DP.Forms {
    public partial class RemisionadoPartidaForm : RadForm {
        protected internal IRemisionadoService Service;
        protected internal BindingList<RemisionPartidaModel> remisiones;

        public RemisionadoPartidaForm() {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public RemisionadoPartidaForm(UIMenuElement menuElement) {
            InitializeComponent();
            this.TRemision.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void RemisionadoPartidaForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            this.CreateView();
            this.OnLoad();

            this.TRemision.Nuevo.Click += this.Nuevo_Click;
            this.TRemision.Editar.Click += this.Editar_Click;
            this.TRemision.Actualizar.Click += this.Actualizar_Click;
            this.TRemision.Cerrar.Click += this.Cerrar_Click;

            this.TRemision.GridData.CellBeginEdit += CellBeginEdit;
            this.TRemision.GridData.CellEndEdit += CellEndEdit;
            this.TRemision.GridData.CellFormatting += CellFormatting;

            this.TRemision.GridData.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.RowSourceNeeded);
        }

        public virtual void CreateView() {
            var gridView = new RemisionGridBuilder();
            this.TRemision.GridData.Columns.AddRange(gridView.Templetes().GetPartidas().Build());
            var status = this.TRemision.GridData.Columns[GridTelerikCommon.ColStatus.Name] as GridViewComboBoxColumn;
            status.DataSource = RemisionService.GetStatus();

            var usoRemision = this.TRemision.GridData.Columns["IdTipoDocumento"] as GridViewComboBoxColumn;
            usoRemision.DisplayMember = "Descriptor";
            usoRemision.ValueMember = "Id";
            usoRemision.DataSource = RemisionService.GetUsos();
        }

        public virtual void OnLoad() {

        }

        #region barra de herramientas
        /// <summary>
        /// nuevo comprobante
        /// </summary>
        public virtual void Nuevo_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// editar comprobante seleccionado
        /// </summary>
        public virtual void Editar_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }
        
        /// <summary>
        /// actualizar vista
        /// </summary>
        public virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TRemision.GridData.DataSource = this.remisiones;
        }

        /// <summary>
        /// imprimir listado
        /// </summary>
        public virtual void TImprimir_Listado_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// imprimir comprobante seleccionado
        /// </summary>
        public virtual void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            RadMessageBox.Show(this, "No implementado");
        }

        /// <summary>
        /// cerrar ventana 
        /// </summary>
        public virtual void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        /// <summary>
        /// inicio de la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {

        }

        /// <summary>
        /// evento al terminar la edicion de la celda seleccionada
        /// </summary>
        public virtual void CellEndEdit(object sender, GridViewCellEventArgs e) {

        }

        /// <summary>
        /// formato de celdas
        /// </summary>
        public virtual void CellFormatting(object sender, CellFormattingEventArgs e) {
            
        }

        public virtual void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {

        }

        #endregion

        public virtual void Consultar() {
            this.remisiones = this.Service.GetList<RemisionPartidaModel>(this.TRemision.GetEjercicio(), this.TRemision.GetPeriodo());
        }
    }
}
