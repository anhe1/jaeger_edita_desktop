﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.DP.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Util;
using Jaeger.QRCode.Helpers;

namespace Jaeger.UI.Almacen.DP.Forms {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private readonly EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Almacen.DP");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(RemisionDetailPrinter)) {
                this.CrearRemision();
            } else if (this.CurrentObject.GetType() == typeof(List<RemisionDetailPrinter>)) {
                this.CrearRemisionL();
            }
        }

        private void CrearRemision() {
            var current = (RemisionDetailPrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.DP.Reports.RemisionInternav20.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(new List<RemisionDetailPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearRemisionL() {
            var current = (IEnumerable<RemisionDetailPrinter>)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.DP.Reports.RemisionInternav20L.rdlc");

            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter>(current.ToList());
            this.SetDataSource("Comprobante", d);
            this.Finalizar();
        }
    }
}
