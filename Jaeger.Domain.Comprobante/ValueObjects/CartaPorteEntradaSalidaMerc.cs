﻿namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    public enum CartaPorteEntradaSalidaMerc {

        /// <remarks/>
        Entrada,

        /// <remarks/>
        Salida,
    }
}
