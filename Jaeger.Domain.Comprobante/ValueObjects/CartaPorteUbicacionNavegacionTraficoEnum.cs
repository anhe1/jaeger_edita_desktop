﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// Altura - Cabotaje
    /// </summary>
    public enum CartaPorteUbicacionNavegacionTraficoEnum {

        /// <remarks/>
        Altura,

        /// <remarks/>
        Cabotaje,
    }
}
