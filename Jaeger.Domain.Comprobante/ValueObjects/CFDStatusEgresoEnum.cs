﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum CFDStatusEgresoEnum {

        [Description("Cancelado")]
        Cancelado,
        [Description("Importado")]
        Importado,
        [Description("Entregado")]
        Entregado,
        [Description("Por Pagar")]
        PorPagar,
        [Description("Pagado")]
        Pagado,
        [Description("Rechazado")]
        Rechazado
    }
}