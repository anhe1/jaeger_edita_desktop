﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum TipoImpuestoEnum {
        [Description("Traslado")]
        Traslado,
        [Description("Retención")]
        Retencion
    }
}