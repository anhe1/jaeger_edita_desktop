﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum TipoNominaEnum {
        [Description("Ordinaria")]
        Ordinaria = 1,
        [Description("Extraordinaria")]
        Extraordinaria = 2
    }
}