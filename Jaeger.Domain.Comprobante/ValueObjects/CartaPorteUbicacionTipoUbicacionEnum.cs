﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// Tipos de ubicacion Origen - Destino
    /// </summary>
    public enum CartaPorteUbicacionTipoUbicacionEnum {

        [Description("Origen")]
        Origen,

        [Description("Destino")]
        Destino,
    }
}
