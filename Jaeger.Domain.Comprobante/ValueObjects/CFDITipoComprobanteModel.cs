﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// Clase para tipos de comprobantes fiscales
    /// </summary>
    public class CFDITipoComprobanteModel : BaseSingleModel {
        public CFDITipoComprobanteModel(int id, string descripcion) : base(id, descripcion) { }

        /// <summary>
        /// obtener enumeracion de tipo
        /// </summary>
        public CFDITipoComprobanteEnum Tipo {
            get { return (CFDITipoComprobanteEnum)this.Id; }
        }
    }
}
