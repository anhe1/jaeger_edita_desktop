﻿using System.Xml.Serialization;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// Tipo de comprobante Ingreso, Egreso, Traslado, Nomina, Pagos
    /// </summary>
    public enum CFDITipoComprobanteEnum {
        [XmlEnum("I")]
        Ingreso,
        [XmlEnum("E")]
        Egreso,
        [XmlEnum("T")]
        Traslado,
        [XmlEnum("N")]
        Nomina,
        [XmlEnum("P")]
        Pagos
    }
}