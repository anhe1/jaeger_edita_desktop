﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum EdoPagoDoctoRelEnum {
        [Description("Sin Comprobar")]
        SinComprobar,
        [Description("Relacionado")]
        Relacionado,
        [Description("No Encontrado")]
        NoEncontrado
    }
}