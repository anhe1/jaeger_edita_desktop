﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// clase estatica para catalogos simples para comprobantes fiscales
    /// </summary>
    public static class CFDICatalogos {
        /// <summary>
        /// obtener listado de tipos de comprobantee
        /// </summary>
        public static BindingList<CFDITipoComprobanteModel> GetTipoComprobantes() {
            return new BindingList<CFDITipoComprobanteModel>(((CFDITipoComprobanteEnum[])Enum.GetValues(typeof(CFDITipoComprobanteEnum))).Select(c => new CFDITipoComprobanteModel((int)c, c.ToString())).ToList());
        }

        /// <summary>
        /// Catalogo de Metodos de Pago
        /// </summary>
        public static List<MetodoPagoModel> GetMetodoPagos() {
            return new List<MetodoPagoModel> {
                new MetodoPagoModel("PUE", "Pago en una sola exhibición"),
                new MetodoPagoModel("PPD", "Pago en parcialidades o diferido")
            };
        }

        /// <summary>
        /// Catalogo de exportacion para version CFDI 4.0
        /// </summary>
        public static List<ExportacionModel> GetExportacion() {
            return new List<ExportacionModel> {
                new ExportacionModel("01", "No Aplica"),
                new ExportacionModel("02", "Definitiva"),
                new ExportacionModel("03", "Temporal")
            };
        }

        /// <summary>
        /// listado de objetos de impuesto para el concepto
        /// </summary>
        public static List<ConceptoObjetoImpuestoModel> GetObjetoImpuesto() {
            return new List<ConceptoObjetoImpuestoModel> {
                new ConceptoObjetoImpuestoModel("01", "No objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("02", "Sí objeto de impuesto"),
                new ConceptoObjetoImpuestoModel("03", "Sí objeto del impuesto y no obligado al desglose."),
                new ConceptoObjetoImpuestoModel("04", "Sí objeto del impuesto y no causa impuesto.")
            };
        }

        /// <summary>
        /// obtener catalogo de motivos de cancelacion permitidos.
        /// </summary>
        public static BindingList<MotivoCancelacionModel> GetMotivoCancelacion() {
            return new BindingList<MotivoCancelacionModel> {
                new MotivoCancelacionModel("01", "Comprobante emitido con errores con relación."),
                new MotivoCancelacionModel("02", "Comprobante emitido con errores sin relación."),
                new MotivoCancelacionModel("03", "No se llevó a cabo la operación."),
                new MotivoCancelacionModel("04", "Operación nominativa relacionada en una factura global.")
            };
        }

        #region catalogos para complemento de carta porte
        /// <summary>
        /// Catalogo simple de tipo de tranposte para carta porte
        /// </summary>
        public static List<CartaPorteTranspInternacModel> GetTipoTranporte() {
            return new List<CartaPorteTranspInternacModel> { 
                new CartaPorteTranspInternacModel(0, "Nacional"),
                new CartaPorteTranspInternacModel(1, "Internacional") };
        }

        public static List<CartaPorteEntradaSalidaMercModel> GetCartaPorteEntradaSalidaMercs() {
            return new List<CartaPorteEntradaSalidaMercModel> { 
                new CartaPorteEntradaSalidaMercModel("Entrada", "Entrada"),
                new CartaPorteEntradaSalidaMercModel("Salida", "Salida")};
        }
        #endregion

        #region catalogos complemento de nomina
        public static List<Jaeger.Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel> GetTipoNominas() {
            return new List<Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel>() {
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("O", "Ordinaria"),
                new Domain.Comprobante.Entities.Complemento.Nomina.TipoNominaModel("E", "Extraordinaria")
            };
        }
        #endregion
    }
}
