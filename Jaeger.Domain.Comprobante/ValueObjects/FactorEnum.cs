﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum FactorEnum {
        [Description("No Aplica")]
        NoAplica = 3,
        [Description("Tasa")]
        Tasa = 0,
        [Description("Cuota")]
        Cuota = 1,
        [Description("Exento")]
        Exento = 2
    }
}