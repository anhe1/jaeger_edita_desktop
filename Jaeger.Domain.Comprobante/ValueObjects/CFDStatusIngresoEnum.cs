﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum CFDStatusIngresoEnum {
        [Description("Cancelado")]
        Cancelado,
        [Description("Pendiente")]
        Pendiente,
        [Description("Importado")]
        Importado,
        [Description("Entregado")]
        Entregado,
        [Description("Por Cobrar")]
        PorCobrar,
        [Description("Cobrado")]
        Cobrado,
        [Description("Rechazado")]
        Rechazado
    }
}