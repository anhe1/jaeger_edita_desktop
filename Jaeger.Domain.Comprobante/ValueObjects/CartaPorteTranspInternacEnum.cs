﻿namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// Trasporte Internacional Si - No
    /// </summary>
    public enum CartaPorteTranspInternacEnum {

        /// <remarks/>
        Sí,

        /// <remarks/>
        No,
    }

    public class CartaPorteTranspInternacModel : Base.Abstractions.BaseSingleModel {
        public CartaPorteTranspInternacModel(int id, string descripcion) : base(id, descripcion) {
        }
    }

    public class CartaPorteEntradaSalidaMercModel : Base.Abstractions.BaseSingleTipoModel {
        public CartaPorteEntradaSalidaMercModel(string id, string descripcion) : base(id, descripcion) {
        }
    }
}
