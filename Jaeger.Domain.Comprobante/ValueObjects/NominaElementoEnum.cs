﻿//' develop: 071120162357
//' purpose: Nomina tipo de elemento guardado en la tabla de percepciones y deducciones
//' elemento1	                    tipo1	            clave1	            concepto1	        gravado1	        exento1
//' 1=Percepcion	                TipoPercepcion	    Clave	            Concepto	        ImporteGravado	    ImporteExento
//' 2=Deduccion	                    TipoDeduccion	    Clave	            Concepto		                        Importe
//' 3=HorasExtra		            Dias	            TipoHoras	        HorasExtra	                            ImportePagado
//' 4=Incapacidades			                                            DiasIncapacidad	    TipoIncapacidad	    ImporteMonetario
//' 5=AccionesOTitulos			                                                            ValorMercado	    PrecioAlOtorgarse
//' 6=JubilacionPensionRetiro	    TotalUnaExhibicion	TotalParcialidad    MontoDiario	        IngresoAcumulable	IngresoNoAcumulable
//' 7=SeparacionIndemnizacion	    TotalPagado	        NumAñosServicio	    UltimoSueldoMensOrd	IngresoAcumulable	IngresoNoAcumulable
//' 8=OtrosPagos	                TipoOtroPago	    Clave	            Concepto		                        Importe
//' 9=CompensacionSaldosAFavor    			                            Año	                SaldoAFavor	        RemanenteSalFav

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum NominaElementoEnum {
        None = 0,
        Percepcion = 1,
        Deduccion = 2,
        HorasExtra = 3,
        Incapacidad = 4,
        AccionesOTitulos = 5,
        JubilacionPensionRetiro = 6,
        SeparacionIndemnizacion = 7,
        OtrosPagos = 8,
        CompensacionSaldosAFavor = 9
    }
}
