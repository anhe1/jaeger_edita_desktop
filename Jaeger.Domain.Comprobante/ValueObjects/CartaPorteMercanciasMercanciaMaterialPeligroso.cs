﻿namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Si - No
    /// </summary>
    public enum CartaPorteMercanciasMercanciaMaterialPeligroso {

        /// <remarks/>
        Sí,

        /// <remarks/>
        No,
    }
}
