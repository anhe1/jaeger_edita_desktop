﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    public enum CFDIEstadoEnum {
        [Description("Todos")]
        Todos,
        [Description("Vigente")]
        Vigente,
        [Description("Cancelado")]
        Cancelado,
        [Description("No Encontrado")]
        NoEncontado
    }
}