﻿using System.ComponentModel;

namespace Jaeger.Domain.Comprobante.ValueObjects {
    /// <summary>
    /// enumeracion de version de comprobantes
    /// </summary>
    public enum CFDVersionEnum {
        [Description("No definido")]
        None,
        [Description("1.0")]
        V10,
        [Description("1.3")]
        V13,
        [Description("2.0")]
        V20,
        [Description("2.2")]
        V22,
        [Description("3.0")]
        V30,
        [Description("3.2")]
        V32,
        [Description("3.3")]
        V33,
        [Description("4.0")]
        V40
    }
}