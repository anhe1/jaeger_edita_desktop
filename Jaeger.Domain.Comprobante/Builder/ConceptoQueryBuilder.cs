﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Comprobante.Builder {
    /// <summary>
    /// builder de consulta para conceptos del comprobante fiscal
    /// </summary>
    public class ConceptoQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IConceptoQueryBuilder, IConceptoComprobanteQueryBuilder {
        public ConceptoQueryBuilder() : base() { }

        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">default true</param>
        public IConditionalBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("_cfdcnp_a", "1"));
            return this;
        }

        /// <summary>
        /// conceptos por indice del comprobante
        /// </summary>
        /// <param name="idComprobante">indice</param>
        public IConceptoComprobanteQueryBuilder IdComprobante(int idComprobante) {
            this._Conditionals.Add(new Conditional("_cfdcnp_cfds_id", idComprobante.ToString()));
            return this;
        }

        /// <summary>
        /// conceptos por indice de control de orden de produccion
        /// </summary>
        /// <param name="index">numero de orden de produccion</param>
        public IConceptoComprobanteQueryBuilder ByOrdenProduccion(int index) {
            this._Conditionals.Add(new Conditional("_cfdcnp_pdd_id", index.ToString()));
            return this;
        }
    }
}
