﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.Builder {
    public interface IConceptoBuilder {
        IConceptoUnidadBuilder Cantidad(decimal cantidad);
    }

    public interface IConceptoNumPedidoBuilder {
        IConceptoUnidadBuilder NumPedido(int numPedido);
    }

    public interface IConceptoUnidadBuilder {
        IConceptoClaveUnidadBuilder NumPedido(int numPedido);
        IConceptoClaveUnidadBuilder Unidad(string unidad);
        IConceptoClaveProductoBuilder ClaveUnidad(string claveUnidad);
    }

    public interface IConceptoClaveUnidadBuilder {
        IConceptoClaveProductoBuilder ClaveUnidad(string claveUnidad);
    }

    public interface IConceptoClaveProductoBuilder {
        IConceptoDescripcionBuilder ClaveProducto(string claveProducto);
    }

    public interface IConceptoDescripcionBuilder {
        IConceptoUnitarioBuilder Descripcion(string descripcion);
    }

    public interface IConceptoUnitarioBuilder {
        IConceptoObjImpuestoBuilder ValorUnitario(decimal valorDecimal);
    }

    public interface IConceptoObjImpuestoBuilder {
        IConceptoImpuestoBuilder ObjetoDeImpuestos(CFDIObjetoImpuestoEnum ObjetoDeImpuesto);
    }

    public interface IConceptoImpuestoBuilder {
        IConceptoBuild AddTrasladoIVA(double tasaOCuota = .16);
        IConceptoBuild AddImpuesto(ComprobanteConceptoImpuesto impuesto);
    }

    public interface IConceptoBuild {
        IComprobanteConceptoDetailModel Build();
    }
}
