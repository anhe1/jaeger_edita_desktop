﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Builder {
    public interface IComprobanteQueryBuilder : IConditionalBuilder, IConditionalBuild {
        IComprobanteIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
        IComprobanteQueryBuilder Tipo(Base.ValueObjects.CFDISubTipoEnum subTipo);
        IComprobanteQueryEmisorBuilder Emitido();
        IComprobanteQueryRecibidoBuilder Recibido();
        IComprobanteQueryNominaBuilder Nomina();
        IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento);
        IComprobanteQueryRFCBuilder WithIdDocumento(string[] idDocumento);

        /// <summary>
        /// conceptos por indice de control de orden de produccion (solo aplica a impresores)
        /// </summary>
        /// <param name="index">numero de orden de produccion</param>
        IComprobanteOrdenProduccionQueryBuilder ByOrdenProduccion(int index);
        /// <summary>
        /// clase builder para consulta del complemento de pago
        /// </summary>
        IComplementoPagoQueryBuilder ComplementoPagoBuilder();
    }

    public interface IComprobanteQueryEmisorBuilder {
        IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum status);
        IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum[] status);
        IComprobanteQueryYearBuilder WithStatus(string[] status);
        IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento);
        IComprobanteQueryRFCBuilder WithFolio(string folio);
        IComprobanteQueryRFCBuilder ForSerie(string serie);
        IComprobanteQueryMonthBuilder WithYear(int year);
        IComprobanteQueryBuild Clientes();
    }

    public interface IComprobanteQueryRecibidoBuilder {
        IComprobanteQueryYearBuilder WithStatus(CFDStatusEgresoEnum status);
        IComprobanteQueryYearBuilder WithStatus(CFDStatusEgresoEnum[] status);
        IComprobanteQueryYearBuilder WithStatus(string[] status);
        IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento);
        IComprobanteQueryRFCBuilder WithFolio(string folio);
        IComprobanteQueryRFCBuilder ForSerie(string serie);
        IComprobanteQueryMonthBuilder WithYear(int year);
        IComprobanteQueryBuild Proveedores();
        IComprobanteIdDirectorioQueryBuilder IdDirectorio(int idDirectorio);
    }

    public interface IComprobanteQueryNominaBuilder : IComprobanteQueryEmisorBuilder {
    }

    public interface IComprobanteQueryYearBuilder {
        IComprobanteQueryMonthBuilder WithYear(int year);
        IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento);
        IComprobanteQueryRFCBuilder WithFolio(string folio);
        IComprobanteQueryRFCBuilder ForSerie(string serie);
        IComprobanteQueryBuild WithRFC(string rfc);
    }

    public interface IComprobanteQueryMonthBuilder {
        IComprobanteQueryRFCBuilder WithMonth(int month);
        IComprobanteQueryBuild WithRFC(string rfc);
        IComprobanteQueryBuild WithTipo(CFDITipoComprobanteEnum tipo);
        IComprobanteQueryBuild WithTipo(CFDITipoComprobanteEnum[] tipo);
        IComprobanteQueryBuild WithIdDirectorio(int idDirectorio);
        List<IConditional> Build();
    }

    public interface IComprobanteQueryRFCBuilder {
        IComprobanteQueryBuild WithRFC(string rfc);
        IComprobanteQueryBuild WithOrderBy(IOrderBy type);
        List<IConditional> Build();
        IConceptoQueryBuilder Conceptos();
        IComprobanteQueryBuild WithTipo(CFDITipoComprobanteEnum tipo);
    }

    public interface IComprobanteQueryBuild {
        List<IConditional> Build();
        IComprobanteQueryBuild WithOrderBy(IOrderBy type);
    }

    public interface IComprobanteIdDirectorioQueryBuilder {
        IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum status);
        IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum[] status);
        IComprobanteQueryYearBuilder WithStatus(string[] status);
        IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento);
        IComprobanteQueryRFCBuilder WithFolio(string folio);
        IComprobanteQueryRFCBuilder ForSerie(string serie);
        IComprobanteQueryMonthBuilder WithYear(int year);
    }

    public interface IComprobanteOrdenProduccionQueryBuilder {
        IComprobanteQueryBuild OnlyActive(bool onlyActive = true);
    }
}