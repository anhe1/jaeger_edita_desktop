﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.Domain.Comprobante.Builder {
    public class RecepcionPagoQueryBuilder : IRecepcionPagoQueryBuilder, IRecepcionPagoQueryBuild {
        private readonly List<IConditional> _Conditionals;

        public RecepcionPagoQueryBuilder() {
            this._Conditionals = new List<IConditional>();
        }

        public List<IConditional> Build() {
            return this._Conditionals;
        }

        public IRecepcionPagoQueryBuild WithIdComprobante(int idComprobante) {
            this._Conditionals.Add(new Conditional("_cmppgd_cfdi_id", idComprobante.ToString()));
            return this;
        }

        public IRecepcionPagoQueryBuild WithIdDocumento(string idDocumento) {
            this._Conditionals.Add(new Conditional("_cmppgd_uuid", idDocumento));
            return this;
        }
    }
}