﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Builder {
    /// <summary>
    /// builder de consulta para conceptos del comprobante fiscal
    /// </summary>
    public interface IConceptoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// conceptos por indice del comprobante
        /// </summary>
        /// <param name="idComprobante">indice</param>
        IConceptoComprobanteQueryBuilder IdComprobante(int idComprobante);
        /// <summary>
        /// conceptos por indice de control de orden de produccion
        /// </summary>
        /// <param name="index">numero de orden de produccion</param>
        IConceptoComprobanteQueryBuilder ByOrdenProduccion(int index);
    }

    public interface IConceptoComprobanteQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registros activos
        /// </summary>
        /// <param name="onlyActive">default true</param>
        IConditionalBuilder OnlyActive(bool onlyActive = true);
    }
}
