﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;

namespace Jaeger.Domain.Comprobante.Builder {
    /// <summary>
    /// clase query para consulta de complemento de pagos
    /// </summary>
    public class ComplementoPagoQueryBuilder : ConditionalBuilder, IComplementoPagoQueryBuilder, IConditionalBuilder, IConditionalBuild, IComplementoPagoIdComprobanteQueryBuilder, IComplementoPagoOnlyActiveQueryBuilder,
        IComplementoPagoMergerQueryBuilder, IComplementoPagoMergerOnlyActiveQueryBuilder {

        public ComplementoPagoQueryBuilder() : base() { }

        /// <summary>
        /// por indice del complemento de pago
        /// </summary>
        public IComplementoPagoIdComprobanteQueryBuilder WithIdComprobante(int idComprobante) {
            this._Conditionals.Add(new Conditional("_cmppg_cfd_id", idComprobante.ToString()));
            return this;
        }

        /// <summary>
        /// por array de indices de complemento de pago
        /// </summary>
        public IComplementoPagoIdComprobanteQueryBuilder WithIdComprobante(int[] idComprobantes) {
            this._Conditionals.Add(new Conditional("_cmppg_cfd_id", string.Join(",", idComprobantes), Base.ValueObjects.ConditionalTypeEnum.In));
            return this;
        }

        /// <summary>
        /// solo registro activos
        /// </summary>
        public IComplementoPagoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true) {
            if (onlyActive)
                this._Conditionals.Add(new Conditional("_cmppg_a", "1"));
            return this;
        }

        /// <summary>
        /// vista combinada de complmento de pago con sus partidas
        /// </summary>
        public IComplementoPagoMergerQueryBuilder Vista() {
            return this;
        }

        /// <summary>
        /// vista de complemento de pago con documentos relacionados
        /// </summary>
        /// <param name="idComprobante">indice del comprobante</param>
        /// <param name="idDocumento">uuid del comprobante</param>
        /// <returns>en caso de no ser un uuid validado tomamos en cuenta el indice del comprobante</returns>
        public IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(int idComprobante, string idDocumento) {
            this._Conditionals.Clear();
            if (ValidacionService.UUID(idDocumento)) {
                this._Conditionals.Add(new Conditional("_cmppgd_uuid", idDocumento));
            } else {
                this._Conditionals.Add(new Conditional("_cmppgd_cfdi_id", idComprobante.ToString()));
            }
            return this;
        }

        /// <summary>
        /// IdDocumento relacionado en la lista del complemento de pago
        /// </summary>
        public IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(string idDocumento) {
            this._Conditionals.Add(new Conditional("_cmppgd_uuid", idDocumento));
            return this;
        }

        /// <summary>
        /// array de IdDocumento relacionado en la lista del complemento de pago
        /// </summary>
        public IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(string[] idDocumento) {
            this._Conditionals.Add(new Conditional("_cmppgd_uuid", string.Join(",", idDocumento), Base.ValueObjects.ConditionalTypeEnum.In));
            return this;
        }
    }
}
