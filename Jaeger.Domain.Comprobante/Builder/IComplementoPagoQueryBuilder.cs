﻿using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Builder {
    /// <summary>
    /// clase builder para consulta del complemento de pago
    /// </summary>
    public interface IComplementoPagoQueryBuilder : IConditionalBuilder, IConditionalBuild {
        /// <summary>
        /// por indice del complemento de pago
        /// </summary>
        IComplementoPagoIdComprobanteQueryBuilder WithIdComprobante(int idComprobante);

        /// <summary>
        /// por array de indices de complemento de pago
        /// </summary>
        IComplementoPagoIdComprobanteQueryBuilder WithIdComprobante(int[] idComprobantes);

        /// <summary>
        /// vista combinada de complmento de pago con sus partidas
        /// </summary>
        IComplementoPagoMergerQueryBuilder Vista();
    }

    public interface IComplementoPagoIdComprobanteQueryBuilder : IConditionalBuilder {
        /// <summary>
        /// solo registro activos
        /// </summary>
        IComplementoPagoOnlyActiveQueryBuilder OnlyActive(bool onlyActive = true);
    }

    public interface IComplementoPagoMergerQueryBuilder {
        /// <summary>
        /// vista de complemento de pago con documentos relacionados
        /// </summary>
        /// <param name="idComprobante">indice del comprobante</param>
        /// <param name="idDocumento">uuid del comprobante</param>
        /// <returns>en caso de no ser un uuid validado tomamos en cuenta el indice del comprobante</returns>
        IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(int idComprobante, string idDocumento);

        /// <summary>
        /// IdDocumento relacionado en la lista del complemento de pago
        /// </summary>
        IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(string idDocumento);

        /// <summary>
        /// array de IdDocumento relacionado en la lista del complemento de pago
        /// </summary>
        IComplementoPagoMergerOnlyActiveQueryBuilder GetComplementoPagoD(string[] idDocumento);
    }

    public interface IComplementoPagoOnlyActiveQueryBuilder : IConditionalBuilder {

    }

    public interface IComplementoPagoMergerOnlyActiveQueryBuilder : IConditionalBuilder {

    }
}
