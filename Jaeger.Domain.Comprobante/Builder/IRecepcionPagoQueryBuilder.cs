﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Builder {
    public interface IRecepcionPagoQueryBuilder {
        IRecepcionPagoQueryBuild WithIdDocumento(string idDocumento);
        IRecepcionPagoQueryBuild WithIdComprobante(int idComprobante);
    }

    public interface IRecepcionPagoQueryBuild {
        List<IConditional> Build();
    }
}