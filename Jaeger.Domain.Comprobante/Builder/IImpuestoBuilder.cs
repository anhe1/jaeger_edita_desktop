﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Abstractions;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Builder {
    public interface IImpuestoBuilder {
        IImpuestoImpuestoBuilder Tipo(TipoImpuestoEnum tipo);
    }

    public interface IImpuestoImpuestoBuilder {
        IImpuestoTasaOCuotaBuilder Impuesto(ImpuestoEnum impuesto);
    }

    public interface IImpuestoTasaOCuotaBuilder {
        IImpuestoBaseBuilder TasaOCuota(double tasaCuota);
    }

    public interface IImpuestoBaseBuilder {
        ComprobanteConceptoImpuesto Build();
    }
}
