﻿using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Builder {
    /// <summary>
    /// Builder para consulta de comprobantes fiscales
    /// </summary>
    public class ComprobanteQueryBuilder : ConditionalBuilder, IConditionalBuilder, IConditionalBuild, IComprobanteQueryBuilder, IComprobanteQueryEmisorBuilder, IComprobanteQueryRecibidoBuilder, 
        IComprobanteQueryNominaBuilder, IComprobanteQueryYearBuilder, IComprobanteQueryMonthBuilder, IComprobanteQueryRFCBuilder, IComprobanteQueryBuild, IComprobanteOrdenProduccionQueryBuilder,
        IComprobanteIdDirectorioQueryBuilder {
        #region declaraciones
        private bool _IsEmitido = false;
        #endregion

        public ComprobanteQueryBuilder() : base() {
            this._IsEmitido = false;
        }

        public IComprobanteQueryBuilder Tipo(CFDISubTipoEnum subTipo) {
            if (subTipo == CFDISubTipoEnum.Emitido | subTipo == CFDISubTipoEnum.Nomina) {
                this.Emitido();
                return this;
            }
            this.Recibido();
            return this;
        }

        public IComprobanteQueryEmisorBuilder Emitido() {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_doc_id", "1"));
            this._IsEmitido = true;
            return this;
        }

        public IComprobanteQueryNominaBuilder Nomina() {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_doc_id", "3"));
            this._IsEmitido = true;
            return this;
        }

        public IComprobanteQueryRecibidoBuilder Recibido() {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_doc_id", "2"));
            this._IsEmitido = false;
            return this;
        }

        /// <summary>
        /// por folio fiscal
        /// </summary>
        public IComprobanteQueryRFCBuilder WithIdDocumento(string idDocumento) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_uuid", idDocumento));
            return this;
        }

        /// <summary>
        /// por array de folio fiscal
        /// </summary>
        public IComprobanteQueryRFCBuilder WithIdDocumento(string[] idDocumento) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_uuid", string.Join(",", idDocumento), ConditionalTypeEnum.In));
            return this;
        }

        public IComprobanteQueryBuild WithRFC(string rfc) {
            if (this._IsEmitido) {
                this._Conditionals.Add(new Conditional("_cfdi._cfdi_rfcr", rfc));
            } else {
                this._Conditionals.Add(new Conditional("_cfdi._cfdi_rfce", rfc));
            }
            return this;
        }

        public IComprobanteQueryBuild WithIdDirectorio(int idDirectorio) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_drctr_id", idDirectorio.ToString()));
            return this;
        }

        public IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum status) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_status", status.ToString()));
            return this;
        }

        public IComprobanteQueryYearBuilder WithStatus(CFDStatusIngresoEnum[] status) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_status", string.Join(",", status), ConditionalTypeEnum.In));
            return this;
        }

        public IComprobanteQueryYearBuilder WithStatus(string[] status) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_status", string.Join(",", status), ConditionalTypeEnum.In));
            return this;
        }

        public IComprobanteQueryYearBuilder WithStatus(CFDStatusEgresoEnum status) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_status", status.ToString()));
            return this;
        }

        public IComprobanteQueryYearBuilder WithStatus(CFDStatusEgresoEnum[] status) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_status", string.Join(",", status), ConditionalTypeEnum.In));
            return this;
        }

        public IComprobanteQueryBuild WithTipo(CFDITipoComprobanteEnum tipo) {
            var inicial = tipo.ToString()[0] + "%";
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_efecto", inicial, ConditionalTypeEnum.Like));
            return this;
        }

        public IComprobanteQueryBuild WithTipo(CFDITipoComprobanteEnum[] tipo) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_efecto", tipo.ToString()));
            return this;
        }

        public IComprobanteQueryMonthBuilder WithYear(int year) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_anio", year.ToString()));
            return this;
        }

        public IComprobanteQueryRFCBuilder WithMonth(int month) {
            // en caso de que el mes es cero entonces omitimos la condicional
            if (month > 0) this._Conditionals.Add(new Conditional("_cfdi._cfdi_mes", month.ToString()));
            return this;
        }

        public IComprobanteQueryRFCBuilder WithFolio(string folio) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_folio", folio));
            return this;
        }

        public IComprobanteQueryRFCBuilder ForSerie(string serie) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_serie", serie));
            return this;
        }

        /// <summary>
        /// conceptos por indice de control de orden de produccion
        /// </summary>
        /// <param name="index">numero de orden de produccion</param>
        public IComprobanteOrdenProduccionQueryBuilder ByOrdenProduccion(int index) {
            this._Conditionals.Add(new Conditional("_cfdcnp_pdd_id", index.ToString()));
            return this;
        }

        public IComprobanteQueryBuild OnlyActive(bool onlyActive = true) {
            if (onlyActive) this._Conditionals.Add(new Conditional("_cfdcnp_a", "1"));
            return this;
        }

        /// <summary>
        /// query para conceptos del comprobante fiscal
        /// </summary>
        public IConceptoQueryBuilder Conceptos() {
            return new ConceptoQueryBuilder();
        }

        public IComprobanteIdDirectorioQueryBuilder IdDirectorio(int idDirectorio) {
            this._Conditionals.Add(new Conditional("_cfdi._cfdi_drctr_id", idDirectorio.ToString()));
            return this;
        }

        public IComprobanteQueryBuild Clientes() {
            this._Conditionals.Add(new Conditional("", "Cliente"));
            return this;
        }

        public IComprobanteQueryBuild Proveedores() {
            this._Conditionals.Add(new Conditional("", "Proveedor"));
            return this;
        }

        public IComprobanteQueryBuild WithOrderBy(IOrderBy type) {
            this._OrderBy.Add(type);
            return this;
        }

        /// <summary>
        /// clase builder para consulta del complemento de pago
        /// </summary>
        public IComplementoPagoQueryBuilder ComplementoPagoBuilder() {
            return new ComplementoPagoQueryBuilder(); 
        }
    }
}
