﻿using System;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Builder {
    public class ConceptoBuilder : IConceptoBuilder, IConceptoUnidadBuilder, IConceptoClaveUnidadBuilder, IConceptoDescripcionBuilder, IConceptoClaveProductoBuilder, IConceptoUnitarioBuilder, IConceptoObjImpuestoBuilder, IConceptoImpuestoBuilder, IConceptoBuild {
        private IComprobanteConceptoDetailModel _Concepto;

        public ConceptoBuilder() {
            this._Concepto = new ComprobanteConceptoDetailModel();
        }

        public IComprobanteConceptoDetailModel Build() {
            return this._Concepto;
        }

        public IConceptoUnidadBuilder Cantidad(decimal cantidad) {
            this._Concepto.Cantidad = cantidad;
            return this;
        }

        public IConceptoClaveUnidadBuilder NumPedido(int numPedido) {
            this._Concepto.NumPedido = numPedido;
            return this;
        }

        public IConceptoClaveUnidadBuilder Unidad(string unidad) {
            this._Concepto.Unidad = unidad;
            return this;
        }

        public IConceptoClaveProductoBuilder ClaveUnidad(string claveUnidad) {
            this._Concepto.ClaveUnidad = claveUnidad;
            return this;
        }

        public IConceptoDescripcionBuilder ClaveProducto(string claveProducto) {
            this._Concepto.ClaveProdServ = claveProducto;
            return this;
        }

        public IConceptoUnitarioBuilder Descripcion(string descripcion) {
            this._Concepto.Descripcion = descripcion;
            return this;
        }

        public IConceptoImpuestoBuilder ObjetoDeImpuestos(CFDIObjetoImpuestoEnum ObjetoDeImpuesto) {
            this._Concepto.ObjetoDeImpuesto = ObjetoDeImpuesto;
            return this;
        }

        public IConceptoObjImpuestoBuilder ValorUnitario(decimal valorDecimal) {
            this._Concepto.ValorUnitario = valorDecimal;
            return this;
        }

        public IConceptoBuild AddTrasladoIVA(double tasaOCuota = .16) {
            this._Concepto.Impuestos.Add(
                new ComprobanteConceptoImpuesto {
                    Impuesto = ImpuestoEnum.IVA, TasaOCuota = Convert.ToDecimal(tasaOCuota), Tipo = TipoImpuestoEnum.Traslado, Base = this._Concepto.Importe
                });
            return this;
        }

        public IConceptoBuild AddImpuesto(ComprobanteConceptoImpuesto impuesto) {
            this._Concepto.Impuestos.Add(impuesto);
            return this;
        }

        public static IImpuestoBuilder Impuestos() {
            return new ImpuestoBuilder();
        }
    }
}
