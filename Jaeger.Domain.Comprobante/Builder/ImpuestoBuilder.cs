﻿using System;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Builder {
    public class ImpuestoBuilder : IImpuestoBuilder, IImpuestoImpuestoBuilder, IImpuestoTasaOCuotaBuilder, IImpuestoBaseBuilder {
        private ComprobanteConceptoImpuesto _TipoImpuesto;

        public ImpuestoBuilder() {
            this._TipoImpuesto = new ComprobanteConceptoImpuesto();
        }

        public ComprobanteConceptoImpuesto Build() {
            return this._TipoImpuesto;
        }

        public IImpuestoTasaOCuotaBuilder Impuesto(ImpuestoEnum impuesto) {
            this._TipoImpuesto.Impuesto = impuesto;
            return this;
        }

        public IImpuestoBaseBuilder TasaOCuota(double tasaCuota) {
            this._TipoImpuesto.TasaOCuota = Convert.ToDecimal(tasaCuota);
            return this;
        }

        public IImpuestoImpuestoBuilder Tipo(TipoImpuestoEnum tipo) {
            this._TipoImpuesto.Tipo = tipo;
            return this;
        }
    }
}
