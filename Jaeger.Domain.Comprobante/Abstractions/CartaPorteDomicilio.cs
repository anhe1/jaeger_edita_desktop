﻿namespace Jaeger.Domain.Comprobante.Abstractions {
    public abstract class CartaPorteDomicilio : Domain.Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private string calleField;
        private string numeroExteriorField;
        private string numeroInteriorField;
        private string coloniaField;
        private string localidadField;
        private string referenciaField;
        private string municipioField;
        private string estadoField;
        private string paisField; //c_Pais
        private string codigoPostalField;
        #endregion

        public CartaPorteDomicilio() { }

        protected CartaPorteDomicilio(string calle, string numeroExterior, string numeroInterior, string colonia, string localidad, string referencia, string municipio, string estado, string pais, string codigoPostal) {
            this.Calle = calle;
            this.NumeroExterior = numeroExterior;
            this.NumeroInterior = numeroInterior;
            this.Colonia = colonia;
            this.Localidad = localidad;
            this.Referencia = referencia;
            this.Municipio = municipio;
            this.Estado = estado;
            this.Pais = pais;
            this.CodigoPostal = codigoPostal;
        }

        /// <summary>
        /// Atributo opcional que sirve para registrar la calle en la que está ubicado el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string Calle {
            get {
                return this.calleField;
            }
            set {
                this.calleField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional que sirve para registrar el número exterior en donde se ubica el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string NumeroExterior {
            get {
                return this.numeroExteriorField;
            }
            set {
                this.numeroExteriorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional que sirve para registrar el número interior, en caso de existir, en donde se ubica el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string NumeroInterior {
            get {
                return this.numeroInteriorField;
            }
            set {
                this.numeroInteriorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional que sirve para expresar la clave de la colonia o dato análogo en donde se ubica el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string Colonia {
            get {
                return this.coloniaField;
            }
            set {
                this.coloniaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar la clave de la ciudad, población, distrito o dato análogo de donde se encuentra ubicado el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string Localidad {
            get {
                return this.localidadField;
            }
            set {
                this.localidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar una referencia geográfica adicional que permita una fácil o precisa ubicación del domicilio del(los) tipo(s) de figura transporte; por ejemplo, las coordenadas del GPS
        /// </summary>
        public string Referencia {
            get {
                return this.referenciaField;
            }
            set {
                this.referenciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar la clave del municipio, delegación o alcaldía, condado o dato análogo en donde se encuentra ubicado el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string Municipio {
            get {
                return this.municipioField;
            }
            set {
                this.municipioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el estado, entidad, región, comunidad, o dato análogo en donde se encuentra ubicado el domicilio del(los) tipo(s) de figura transporte.
        /// </summary>
        public string Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido que sirve para registrar la clave del país en donde se encuentra ubicado el domicilio del(los) tipo(s) de figura transporte, conforme al catálogo c_Pais del CFDI publicado en el portal del SAT en Internet de acuerdo a la especificación ISO 3166-1.
        /// </summary>
        public string Pais {
            get {
                return this.paisField;
            }
            set {
                this.paisField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el código postal en donde se encuentra ubicado el domicilio del(los) tipo(s) de figura transporte
        /// </summary>
        public string CodigoPostal {
            get {
                return this.codigoPostalField;
            }
            set {
                this.codigoPostalField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            string direccion = string.Empty;

            if (!(string.IsNullOrEmpty(this.Calle)))
                direccion = string.Concat("Calle ", this.Calle);

            if (!(string.IsNullOrEmpty(this.NumeroExterior)))
                direccion = string.Concat(direccion, " No. Ext. ", this.NumeroExterior);

            if (!(string.IsNullOrEmpty(this.NumeroInterior)))
                direccion = string.Concat(direccion, " No. Int. ", this.NumeroInterior);

            if (!(string.IsNullOrEmpty(this.Colonia)))
                direccion = string.Concat(direccion, " Col. ", this.Colonia);

            if (!(string.IsNullOrEmpty(this.CodigoPostal)))
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);

            if (!(string.IsNullOrEmpty(this.Municipio)))
                direccion = string.Concat(direccion, " ", this.Municipio);

            if (!(string.IsNullOrEmpty(this.Estado)))
                direccion = string.Concat(direccion, ", ", this.Estado);

            if (!(string.IsNullOrEmpty(this.Referencia)))
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);

            if (!(string.IsNullOrEmpty(this.Localidad)))
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);

            return direccion;
        }
    }
}
