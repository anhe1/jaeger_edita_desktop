﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Abstractions {
    [JsonObject]
    public abstract class TipoImpuestoModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private TipoImpuestoEnum tipoField;
        private decimal baseDRField;
        private ImpuestoEnum impuestoDRField;
        private FactorEnum tipoFactorDRField;
        private decimal tasaOCuotaDRField;
        private decimal importeDRField;
        #endregion

        [JsonProperty("tipo")]
        public TipoImpuestoEnum Tipo {
            get {
                return this.tipoField;
            }
            set {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer base para el calculo del impuesto conforme al monto del pago
        /// </summary>
        [JsonProperty("base")]
        public decimal Base {
            get {
                return this.baseDRField;
            }
            set {
                this.baseDRField = value;
                this.Importe = this.TasaOCuota * this.baseDRField;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del tipo de impuesto trasladado conforme al monto del pago
        /// </summary>
        [JsonProperty("impuesto")]
        public ImpuestoEnum Impuesto {
            get {
                return this.impuestoDRField;
            }
            set {
                this.impuestoDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del tipo de factor que se aplica a la base del impuesto
        /// </summary>
        [JsonProperty("tipoFactor")]
        public FactorEnum TipoFactor {
            get {
                return this.tipoFactorDRField;
            }
            set {
                this.tipoFactorDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor de la tasa o cuota del impuesto que se traslada. Es requerido cuando el atributo TipoFactorDR contenga una clave que corresponda a Tasa o Cuota.
        /// </summary>
        [JsonProperty("tasaOCuota")]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaDRField;
            }
            set {
                this.tasaOCuotaDRField = value;
                this.Base = this.baseDRField;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del impuesto trasladado conforme al monto del pago, aplicable al documento relacionado. No se permiten valores negativos. Es requerido cuando el tipo factor sea Tasa o Cuota.
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return Math.Round(this.importeDRField, 4);
            }
            set {
                this.importeDRField = value;
                this.OnPropertyChanged();
            }
        }

        public void CalcularImporte() {
            this.Importe = this.Base * this.TasaOCuota;
        }

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }
    }
}
