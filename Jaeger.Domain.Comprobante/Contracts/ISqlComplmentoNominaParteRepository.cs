﻿using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComplmentoNominaParteRepository : IGenericRepository<ComplementoNominaParteModel> {
    }
}
