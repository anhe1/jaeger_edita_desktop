﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComplementoCartaPorteRepository : IGenericRepository<CartaPorteModel> {
        new CartaPorteDetailModel GetById(int index);

        CartaPorteDetailModel Save(CartaPorteDetailModel model);

        bool CreateTable();
    }
}
