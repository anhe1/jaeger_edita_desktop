﻿namespace Jaeger.Domain.Comprobante.Contracts {
    /// <summary>
    /// Tipo de relacion entre CFDI
    /// </summary>
    public interface IComprobanteTipoRelacionCFDI {
        string Id { get; set; }
        string Descripcion { get; set; }
        string Descriptor { get; }
    }
}
