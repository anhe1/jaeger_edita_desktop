﻿using System;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoTimbreFiscal {
        #region propiedades

        /// <summary>
        /// versión del estándar del Timbre Fiscal Digital
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtiene o establece los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string UUID { get; set; }

        /// <summary>
        /// fecha y hora, de la generación del timbre por la certificación digital del SAT. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora de la Zona Centro del Sistema de Horario en México.
        /// </summary>
        DateTime? FechaTimbrado { get; set; }

        string SelloCFD { get; set; }

        /// <summary>
        /// número de serie del certificado del SAT usado para generar el sello digital del Timbre Fiscal Digital.
        /// </summary>
        string NoCertificadoSAT { get; set; }

        /// <summary>
        /// obtiene o establace sello digital del Timbre Fiscal Digital, al que hacen referencia las reglas de la Resolución Miscelánea vigente. El sello debe ser expresado como una cadena de texto en formato Base 64.
        /// </summary>
        string SelloSAT { get; set; }

        /// <summary>
        /// opcional para registrar información que el SAT comunique a los usuarios del CFDI.
        /// </summary>
        string Leyenda { get; set; }

        /// <summary>
        /// RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        string RFCProvCertif { get; set; }

        object Tag { get; set; }
        #endregion
    }
}
