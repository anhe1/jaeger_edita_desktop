﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlContribuyenteRepository : IGenericRepository<ComprobanteContribuyenteModel> {
        /// <summary>
        /// obtener indice del del contribuyente por el registro federal de contribuyentes
        /// </summary>
        int ReturnId(string rfc);

        /// <summary>
        /// obtener listado de receeptores 
        /// </summary>
        /// <param name="relacion">relacion con el directorio</param>
        IEnumerable<ComprobanteContribuyenteModel> GetList(string relacion);
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        ComprobanteContribuyenteModel Save(ComprobanteContribuyenteModel model);
    }
}
