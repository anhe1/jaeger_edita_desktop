﻿using System;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoPagoModel {
        /// <summary>
        /// obtener o establecer indice de la tabla de pagos
        /// </summary>
        int IdPago { get; set; }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer la versión del complemento para recepción de pagos.
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// obtener o establecer indice de relacion con el comprobante de pagos (cfdi)
        /// </summary>
        int IdComprobanteP { get; set; }

        /// <summary>
        /// obtener o establecer la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la 
        /// especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        DateTime FechaPagoP { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        string FormaDePagoP { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para realizar el pago conforme a la especificación ISO 4217. Cuando se usa moneda nacional se registra MXN. 
        /// El atributo Pagos:Pago:Monto debe ser expresado en la moneda registrada en este atributo.
        /// </summary>
        string MonedaP { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una 
        /// unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        decimal TipoCambioP { get; set; }

        /// <summary>
        /// obtener o establecer el importe del pago.
        /// </summary>
        decimal Monto { get; set; }

        /// <summary>
        /// obtener o establecer numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia 
        /// análogo que identifique la operación que ampara el pago efectuado.
        /// </summary>
        string NumOperacion { get; set; }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc.,
        /// en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string RfcEmisorCtaOrd { get; set; }

        /// <summary>
        /// obtener o establecer nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de 
        /// acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string NomBancoOrdExt { get; set; }

        /// <summary>
        /// obtener o establecer el numero de la cuenta con la que se realizo el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con 
        /// el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string CtaOrdenante { get; set; }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. 
        /// Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string RfcEmisorCtaBen { get; set; }

        /// <summary>
        /// obtener o establecer clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para 
        /// éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string TipoCadPago { get; set; }

        /// <summary>
        /// obtener o establecer el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo 
        /// con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        string CtaBeneficiario { get; set; }

        /// <summary>
        /// obtener o establecer la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago 
        /// contenga información.
        /// </summary>
        string CadPago { get; set; }

        /// <summary>
        /// obtener o establecer certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        string CertPago { get; set; }

        /// <summary>
        /// obtener o establecer el sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de 
        /// dicho comprobante, este sello digital es el que se debe registrar en este atributo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso 
        /// de que el atributo TipoCadPago contenga información.
        /// </summary>
        string SelloPago { get; set; }

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        string Creo { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        string Modifica { get; set; }

        /// <summary>
        /// obtener o establecer el ultima fecha de modificacion del registro
        /// </summary>
        DateTime? FechaModifica { get; set; }
    }
}
