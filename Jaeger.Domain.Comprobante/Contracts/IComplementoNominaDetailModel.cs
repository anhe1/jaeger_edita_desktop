﻿using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoNominaDetailModel : IComplementoNominaModel {
        BindingList<ComplementoNominaParteDetailModel> Partes { get; set; }
    }
}
