﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteFiscalPrinter : IComprobanteFiscalModel {
        CFDISubTipoEnum SubTipo { get; set; }
        CFDITipoComprobanteEnum TipoComprobante { get; set; }
        string EmisorRegimenFiscal { get; set; }
        string ReceptorDomicilioFiscal { get; set; }
        string Confirmacion { get; set; }
        decimal TotalRetenciones { get; }
        decimal TotalTraslados { get; }
        CartaPorteDetailModel CartaPorte { get; set; }
        BindingList<ComprobanteConceptoDetailModel> Conceptos { get; set; }
        BindingList<ComprobanteTotalPrinter> Impuestos { get; set; }
        BindingList<ComplementoPagoDetailModel> RecepcionPago { get; set; }
        ComplementoTimbreFiscal TimbreFiscal { get; set; }
        string QR { get; set; }
        string CadenaOriginalOff { get; set; }
        string CantidadEnLetra { get; set; }
    }
}