﻿using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlSugarConceptoRepository : IGenericRepository<ComprobanteConceptoModel> {
    }
}
