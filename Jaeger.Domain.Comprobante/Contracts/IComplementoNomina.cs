﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoNomina {
        #region propiedades
        int IdComplemento { get; set; }

        /// <summary>
        /// Nodo condicional para expresar la información del contribuyente emisor del comprobante de nómina.
        /// </summary>
        ComplementoNominaEmisor Emisor { get; set; }

        /// <summary>
        /// Nodo requerido para precisar la información del contribuyente receptor del comprobante de nómina.
        /// </summary>
        ComplementoNominaReceptor Receptor { get; set; }

        /// <summary>
        /// Nodo condicional para expresar las percepciones aplicables.
        /// </summary>
        ComplementoNominaPercepciones Percepciones { get; set; }

        /// <summary>
        /// Nodo opcional para expresar las deducciones aplicables.
        /// </summary>
        ComplementoNominaDeducciones Deducciones { get; set; }

        /// <summary>
        /// Nodo condicional para expresar otros pagos aplicables.
        /// </summary>
        BindingList<ComplementoNominaOtroPago> OtrosPagos { get; set; }

        /// <summary>
        /// Nodo condicional para expresar información de las incapacidades.
        /// </summary>
        BindingList<ComplementoNominaIncapacidad> Incapacidades { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión de la versión del complemento.
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// Atributo requerido para indicar el tipo de nómina, puede ser O= Nómina ordinaria o E= Nómina extraordinaria.
        /// </summary>
        string TipoNomina { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        DateTime? FechaPago { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha inicial del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        DateTime? FechaInicialPago { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha final del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        DateTime? FechaFinalPago { get; set; }

        /// <summary>
        /// Atributo requerido para la expresión del número o la fracción de días pagados.
        /// </summary>
        decimal NumDiasPagados { get; set; }

        /// <summary>
        /// Atributo condicional para representar la suma de las percepciones.
        /// </summary>
        decimal TotalPercepciones { get; set; }

        /// <summary>
        /// Atributo condicional para representar la suma de las deducciones aplicables.
        /// </summary>
        decimal TotalDeducciones { get; set; }

        /// <summary>
        /// Atributo condicional para re-presentar la suma de otros pagos.
        /// </summary>
        decimal TotalOtrosPagos { get; set; }

        string IdDocumento { get; set; }

        decimal Descuento { get; set; }
        #endregion
    }
}
