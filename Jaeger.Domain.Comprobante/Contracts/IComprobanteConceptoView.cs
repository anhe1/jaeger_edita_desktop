﻿using System;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteConceptoView {
        string Status { get; set; }

        string Folio { get; set; }

        string Serie { get; set; }

        string IdDocumento { get; set; }

        DateTime? FechaEmision { get; set; }

        int IdConcepto { get; set; }

        int Activo { get; set; }

        int SubId { get; set; }

        int NumPedido { get; set; }

        string ClaveProdServ { get; set; }

        string NoIdentificacion { get; set; }

        decimal Cantidad { get; set; }

        string ClaveUnidad { get; set; }

        string Unidad { get; set; }

        string Descripcion { get; set; }

        decimal ValorUnitario { get; set; }

        decimal Importe { get; set; }

        decimal Descuento { get; set; }

        decimal SubTotal { get; set; }

        string CtaPredial { get; set; }

        DateTime? FechaNuevo { get; set; }
    }
}
