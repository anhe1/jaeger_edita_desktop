﻿using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComplementoPagoRepository : IGenericRepository<ComplementoPagoModel> {
        new ComplementoPagoDetailModel GetById(int index);

        IEnumerable<ComplementoPagoDetailModel> GetComplementoById(int index);

        BindingList<ComplementoPagoDetailModel> Save(BindingList<ComplementoPagoDetailModel> model);

        /// <summary>
        /// guardar o actualizar comprobante relacionado al pago
        /// </summary>
        PagosPagoDoctoRelacionadoDetailModel Save(PagosPagoDoctoRelacionadoDetailModel model);

        //IEnumerable<ComplementoPagoDetailModel> GetList(List<Conditional> conditionals);

        //IEnumerable<ComplementoPagoMergeDoctoModel> Get3List(List<IConditional> conditionals);

        bool AplicarComprobantePagos(ComplementoPagoDetailModel pagos);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        bool CreateTable();
    }
}
