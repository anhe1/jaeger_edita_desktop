﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Contracts {
    /// <summary>
    /// repositorio de comprobantes fiscales
    /// </summary>
    public interface ISqlComprobanteFiscalRepository : IGenericRepository<ComprobanteFiscalModel> {
        /// <summary>
        /// obtener comprobante fiscal detalle
        /// </summary>
        /// <param name="index">indice</param>
        ComprobanteFiscalDetailModel GetComprobante(int index);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();

        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        IEnumerable<ComprobanteConceptoDetailModel> GetConceptos(int index);

        /// <summary>
        /// Realizar busqueda de un comprobante por el folio de control interno
        /// </summary>
        /// <param name="subTipo">subtipo del comprobante fiscal (Emitido, Recibido, Nomina)</param>
        /// <param name="rfc">RFC del emisor o receptor</param>
        /// <param name="status">status a buscar</param>
        //IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, string[] status);

        IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, string rfc, List<Conditional> keyValues, int year = 0, int month = 0);

        IEnumerable<ComprobanteFiscalDetailSingleModel> GetSearch(CFDISubTipoEnum subTipo, List<Conditional> condicionValues, int year = 0, int month = 0);

        /// <summary>
        /// obtener resumen de comprobantes fiscales para tabla dinamica
        /// </summary>
        /// <param name="subtipo">sub tipo de comprobante Emitido, Recibido, Nomina</param>
        /// <param name="anio">año</param>
        /// <param name="mes">mes</param>
        IEnumerable<EstadoCuentaSingleView> GetResumen(CFDISubTipoEnum subtipo, int year, int month = 0);

        System.Data.DataTable GetResumen(CFDISubTipoEnum subtipo, int year, string[] estado, string rfc = "");

        /// <summary>
        /// almacenar comprobante fiscal
        /// </summary>
        ComprobanteFiscalDetailModel Save(ComprobanteFiscalDetailModel comprobante);

        IEnumerable<ComprobanteBackup> GetInfoBackup(CFDISubTipoEnum subTipo);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlXml(int index, string url);

        /// <summary>
        /// actualizar url del archivo XML del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">direccion URL</param>
        /// <returns>verdadero si la acción se realiza con exito</returns>
        bool UpdateUrlPdf(int index, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlXmlAcuse(int index, string url);

        /// <summary>
        /// actualizar url del acuse de cancelacion del comprobante fiscal
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="url">url</param>
        /// <returns>verdadero si la accion se realiza con exito</returns>
        bool UpdateUrlPdfAcuse(int index, string url);

        /// <summary>
        /// actualizar estado del comprobante
        /// </summary>
        /// <param name="idDocumento">folio fiscal del comprobante</param>
        /// <param name="estado">estado del comprobante</param>
        /// <param name="usuario">clave del usuario</param>
        /// <returns>true al actualizar base de datos</returns>
        bool UpdateEstado(string idDocumento, string estado, string usuario);

        /// <summary>
        /// metodo para actualizar el status del comprobante fiscal
        /// </summary>
        /// <param name="indice">indice del comprobante</param>
        /// <param name="status">nuevo status</param>
        /// <param name="usuario">clave del usuario</param>
        bool Update(int indice, string status, string usuario);
    }
}
