﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteConceptoDetailModel : IComprobanteConceptoModel {
        CFDIObjetoImpuestoEnum ObjetoDeImpuesto { get; set; }
    }
}
