﻿using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComplementoNominaRepository : IGenericRepository<ComplementoNominaModel> {
        /// <summary>
        /// almacenar complemento de nomina
        /// </summary>
        //ComplementoNominaModel Save(ComplementoNominaModel model);

        IComplementoNominaDetailModel Save(IComplementoNominaDetailModel model);

        IComprobanteFiscalNominaModel Save(IComprobanteFiscalNominaModel model);
    }
}
