﻿using System;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoNominaModel {
        #region propiedades

        /// <summary>
        /// Desc:id principal de la tabla
        /// </summary>           
        int Id { get; set; }

        /// <summary>
        /// Desc:registro activo
        /// </summary>           
        bool Activo { get; set; }

        /// <summary>
        /// Desc:id de relación con la tabla del directorio
        /// </summary>           
        int ReceptorId { get; set; }

        /// <summary>
        /// Desc:Id de control de nominas
        /// </summary>           
        int ControlId { get; set; }

        /// <summary>
        /// Desc:id de relación con la tabla de comprobantes
        /// </summary>           
        int SubId { get; set; }

        /// <summary>
        /// Desc:Numero de Quincena
        /// </summary>           
        int? Quincena { get; set; }

        /// <summary>
        /// Desc:Año de poliza
        /// </summary>           
        int? Anio { get; set; }

        /// <summary>
        /// Desc:NumDiasPagados: Atributo requerido para la expresión del número de días pagados
        /// </summary>           
        decimal NumDiasPagados { get; set; }

        /// <summary>
        /// Desc:SalarioBaseCotApor: Retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o pre
        /// </summary>           
        decimal SalarioBaseCotApor { get; set; }

        /// <summary>
        /// Desc:SalarioDiarioIntegrado: El salario se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador p
        /// </summary>           
        decimal SalarioDiarioIntegrado { get; set; }

        /// <summary>
        /// Desc:Percepciones TotalGravado: requerido para expresar el total de percepciones gravadas que se relacionan en el comprobante
        /// </summary>           
        decimal PercepcionTotalGravado { get; set; }

        /// <summary>
        /// Desc:Percepciones TotalExento: requerido para expresar el total de percepciones exentas que se relacionan en el comprobante
        /// </summary>           
        decimal PercepcionTotalExento { get; set; }

        /// <summary>
        /// Desc:Deducciones TotalGravado: requerido para expresar el total de deducciones gravadas que se relacionan en el comprobante
        /// </summary>           
        decimal DeduccionTotalGravado { get; set; }

        /// <summary>
        /// Desc:Deducciones TotalExento: requerido para expresar el total de deducciones exentas que se relacionan en el comprobante
        /// </summary>           
        decimal DeduccionTotalExento { get; set; }

        /// <summary>
        /// Desc:Descuento: Monto del descuento por la incapacidad
        /// </summary>           
        decimal Descuento { get; set; }

        /// <summary>
        /// Desc:ImportePagado: Importe pagado por las horas extra
        /// </summary>           
        decimal ImportePagado { get; set; }

        /// <summary>
        /// Desc:TipoRegimen: requerido para la expresión de la clave del régimen por el cual se tiene contratado al trabajador, conforme al catálogo publicado en el portal del SAT en internet
        /// </summary>           
        string TipoRegimen { get; set; }

        /// <summary>
        /// Desc:RiesgoPuesto:Clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo a las actividades que desempeñan sus trabajadores, según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación
        /// </summary>           
        string RiesgoPuesto { get; set; }

        /// <summary>
        /// Desc:Antigüedad: Número de semanas que el empleado ha mantenido relación laboral con el empleador
        /// </summary>           
        string Antiguedad { get; set; }

        /// <summary>
        /// Desc:Atributo requerido para expresar la clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// </summary>           
        string EntidadFederativa { get; set; }

        /// <summary>
        /// Desc:Atributo opcional para indicar si el trabajador está asociado a un sindicato. Si se omite se asume que no está asociado a algún sindicato. (Si,No)
        /// </summary>           
        string Sindicalizado { get; set; }

        /// <summary>
        /// Desc:versión del documento
        /// </summary>           
        string Version { get; set; }

        /// <summary>
        /// Desc:Banco: opcional para la expresión del Banco conforme al catálogo, donde se realiza un depósito de nómina
        /// </summary>           
        string Banco { get; set; }

        /// <summary>
        /// Desc:Reg. Fed. De Contribuyentes (RFC)
        /// </summary>           
        string RFC { get; set; }

        /// <summary>
        /// Desc:RegistroPatronal: opcional para expresar el registro patronal a 20 posiciones máximo
        /// </summary>           
        string RegistroPatronal { get; set; }

        /// <summary>
        /// Desc:Atributo condicional para la expresión de la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones o la CLABE a 18 posiciones o número de monedero electrónico,
        /// </summary>           
        string CtaBanco { get; set; }

        /// <summary>
        /// Desc:NumEmpleado: requerido para expresar el número de empleado de 1 a 15 posiciones
        /// </summary>           
        string NumEmpleado { get; set; }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// </summary>           
        string CURP { get; set; }

        /// <summary>
        /// Desc:NumSeguridadSocial: opcional para la expresión del número de seguridad social aplicable al trabajador
        /// </summary>           
        string NumSeguridadSocial { get; set; }

        /// <summary>
        /// Desc:CLABE: opcional para la expresión de la CLABE
        /// </summary>           
        string CLABE { get; set; }

        /// <summary>
        /// Desc:tipo de nomina
        /// </summary>           
        string TipoNomina { get; set; }

        /// <summary>
        /// Desc:folio fiscal
        /// </summary>           
        string IdDocumento { get; set; }

        /// <summary>
        /// Desc:Departamento: opcional para la expresión del departamento o área a la que pertenece el trabajador
        /// </summary>           
        string Departamento { get; set; }

        /// <summary>
        /// Desc:Puesto: Puesto asignado al empleado o actividad que realiza
        /// </summary>           
        string Puesto { get; set; }

        /// <summary>
        /// Desc:TipoContrato: Tipo de contrato que tiene el trabajador: Base, Eventual, Confianza, Sindicalizado, a prueba, etc.
        /// </summary>           
        string TipoContrato { get; set; }

        /// <summary>
        /// Desc:TipoJornada: Tipo de jornada que cubre el trabajador: Diurna, nocturna, mixta, por hora, reducida, continuada, partida, por turnos, etc.
        /// </summary>           
        string TipoJornada { get; set; }

        /// <summary>
        /// Desc:PeriodicidadPago: Forma en que se establece el pago del salario: diario, semanal, quincenal, catorcenal mensual, bimestral, unidad de obra, comisión, precio alzado, etc.
        /// </summary>           
        string PeriodicidadPago { get; set; }

        /// <summary>
        /// Desc:FechaPago: requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>           
        DateTime? FechaPago { get; set; }

        /// <summary>
        /// Desc:FechaInicialPago: requerido para la expresión de la fecha inicial del pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>           
        DateTime? FechaInicialPago { get; set; }

        /// <summary>
        /// Desc:FechaFinalPago: requerido para la expresión de la fecha final del pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// </summary>           
        DateTime? FechaFinalPago { get; set; }

        /// <summary>
        /// Desc:FechaInicioRelLaboral: opcional para expresar la fecha de inicio de la relación laboral entre el empleador y el empleado
        /// </summary>           
        DateTime? FechaInicioRelLaboral { get; set; }

        /// <summary>
        /// Default:CURRENT_TIMESTAMP
        /// </summary>           
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// Desc:fecha de envio de correo de nomina
        /// </summary>           
        DateTime? FecEnvio { get; set; }

        /// <summary>
        /// Desc:fecha de modificacion
        /// </summary>           
        DateTime? FecModificacion { get; set; }

        /// <summary>
        /// Desc:usuario creo
        /// </summary>           
        string Creo { get; set; }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// </summary>           
        string Modifica { get; set; }
        #endregion
    }
}
