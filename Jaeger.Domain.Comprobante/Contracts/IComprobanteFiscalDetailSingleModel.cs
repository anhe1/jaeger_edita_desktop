﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteFiscalDetailSingleModel : IComprobanteFiscalSingleModel {
        CFDISubTipoEnum SubTipo { get; }

        CFDITipoComprobanteEnum TipoComprobante { get; }

        decimal Total1 { get; set; }
    }
}
