﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IPagosPagoDoctoRelacionadoDetailModel {
        CFDIObjetoImpuestoEnum ObjetoDeImpuesto { get; set; }
    }
}
