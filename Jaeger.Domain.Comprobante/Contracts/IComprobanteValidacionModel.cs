﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteValidacionModel {
        /// <summary>
        /// obtener o establecer el resultado de la validación
        /// </summary>
        CFDIValidacionEnum Valido {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el proveedor del servicio
        /// </summary>
        string Provider {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la versión del estándar bajo el que se encuentra expresado el comprobante.
        /// </summary>
        string Version {
            get; set;
        }

        /// <summary>
        /// para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado
        /// </summary>
        string TipoComprobante {
            get; set;
        }

        /// <summary>
        /// obtener o establecer folio de control interno del contribuyente.
        /// </summary>
        string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. 
        /// </summary>
        string Serie {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante fiscal
        /// </summary>
        DateTime FechaEmision {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de validación
        /// </summary>
        DateTime FechaValidacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer folio discal del comprobante (UUID)
        /// </summary>
        string IdDocumento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante
        /// </summary>
        string EmisorNombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante
        /// </summary>
        string EmisorRFC {
            get; set;
        }

        string ReceptorNombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el monto total del comprobante
        /// </summary>
        decimal Total {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el estado SAT del comprobante
        /// </summary>
        string Estado {
            get; set;
        }

        string TimeElapsed {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el listado de validaciones del comprobante
        /// </summary>
        List<PropertyValidate> Resultados {
            get; set;
        }

        List<PropertyValidateSingle> Errores {
            get; set;
        }
    }
}
