﻿namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IEstadoCuenta {
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        decimal RetencionISR { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        decimal RetencionIVA { get; set; }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        decimal TrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        decimal RetencionIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        decimal TrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        decimal Saldo { get; }
    }
}
