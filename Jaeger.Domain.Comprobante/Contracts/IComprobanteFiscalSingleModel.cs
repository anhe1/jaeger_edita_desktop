﻿using System;

namespace Jaeger.Domain.Comprobante.Contracts {
    /// <summary>
    /// interface de vista para comprobant fiscal
    /// </summary>
    public interface IComprobanteFiscalSingleModel {
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// version del comprobante
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        string TipoComprobanteText { get; set; }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        int IdSubTipo { get; set; }

        /// <summary>
        /// obtener la clasificacion del comprobante si es emitido, recibido o es un comprobante de nomina
        /// </summary>
        string SubTipoText { get; }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string Serie { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string Folio { get; set; }

        string Status { get; set; }

        int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string Estado { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        string IdDocumento { get; set; }

        /// <summary>
        /// numero de parcialidad
        /// </summary>
        int NumParcialidad { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        DateTime FechaEmision { get; set; }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        DateTime? FechaTimbre { get; set; }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        DateTime? FechaEstado { get; set; }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        DateTime? FechaCancela { get; set; }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        DateTime? FechaEntrega { get; set; }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        DateTime? FechaUltimoPago { get; set; }

        /// <summary>
        /// fecha del comprobante fiscal de repecion de pago
        /// </summary>
        DateTime? FechaRecepcionPago { get; set; }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        DateTime? FechaValidacion { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        string ClaveMoneda { get; set; }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        string ClaveMetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        string ClaveFormaPago { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        string ClaveUsoCFDI { get; set; }

        /// <summary>
        /// obtener o establecer atributo condicional para expresar las condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"/>
        /// </summary>
        string CondicionPago { get; set; }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        decimal RetencionISR { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        decimal RetencionIVA { get; set; }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        decimal TrasladoIVA { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        decimal RetencionIEPS { get; set; }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        decimal TrasladoIEPS { get; set; }

        /// <summary>
        /// obtener o estblacer el valor total de las percepciones en el caso de nomina
        /// </summary>
        decimal TotalPecepcion { get; set; }

        decimal TotalDeduccion { get; set; }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        decimal SubTotal { get; set; }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        decimal Descuento { get; set; }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        decimal Acumulado { get; set; }

        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        decimal ImportePagado { get; set; }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        string UrlFileXML { get; set; }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        string UrlFilePDF { get; set; }

        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        string FileAccuse { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        string FileAccuseXML { get; set; }

        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        string FileAccusePDF { get; set; }

        string Creo { get; set; }

        string Modifica { get; set; }

        DateTime FechaNuevo { get; set; }

        DateTime? FechaModifica { get; set; }
    }
}
