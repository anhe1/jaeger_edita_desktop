﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteConceptoModel {
        #region propiedades

        int Id {
            get; set;
        }

        int SubId {
            get; set;
        }

        bool Activo {
            get; set;
        }

        /// <summary>
        /// asignar numero de pedido 
        /// </summary>
        int NumPedido {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        string ClaveProdServ {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el número de serie, número de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves 
        /// del estándar GTIN.
        /// pattern value="[^|]{1,100}", Longitud: 100
        /// </summary>
        string NoIdentificacion {
            get; set;
        }

        decimal Cantidad {
            get; set;
        }

        string ClaveUnidad {
            get; set;
        }

        string Unidad {
            get; set;
        }

        string Descripcion {
            get; set;
        }

        decimal ValorUnitario {
            get; set;
        }

        decimal Importe {
            get; set;
        }

        decimal SubTotal {
            get; set;
        }

        decimal Descuento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado 
        /// de participación inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{1,150}", Longitud: 150
        /// </summary>
        string CtaPredial {
            get; set;
        }

        decimal TrasladoIVA {
            get; set;
        }

        decimal TrasladoIEPS {
            get; set;
        }

        decimal RetencionIVA {
            get; set;
        }

        decimal RetencionISR {
            get; set;
        }

        decimal RetencionIEPS {
            get; set;
        }

        string Creo {
            get; set;
        }

        string Modifica {
            get; set;
        }

        DateTime? FechaNuevo {
            get; set;
        }

        DateTime? FechaModifica {
            get; set;
        }

        /// <summary>
        /// obtiene objeto json o establece el valor del objeto de impuestos
        /// </summary>
        string JImpuestos {
            get; set;
        }

        /// <summary>
        /// obtener o establecer objeto json que representa las partes del concepto
        /// </summary>
        string JParte {
            get; set;
        }

        int PresionDecimal {
            get; set;
        }

        BindingList<ComprobanteInformacionAduanera> InformacionAduanera {
            get; set;
        }

        /// <summary>
        /// obtener o establacer lista de parte del conepto
        /// </summary>
        BindingList<ConceptoParte> Parte {
            get; set;
        }

        /// <summary>
        /// obtener o establecer lista de impuestos aplicables al concepto.
        /// </summary>
        BindingList<ComprobanteConceptoImpuesto> Impuestos {
            get; set;
        }

        #endregion
    }
}
