﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.Domain.Comprobante.Contracts {
public interface IComplementoNominaParteModel {
        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        int Id { get; set; }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>       
        bool Activo { get; set; }

        /// <summary>
        /// Desc:relacion con catalogo de nominas
        /// Default:
        /// Nullable:True
        /// </summary>           
        int SubId { get; set; }

        /// <summary>
        /// obtener o establecer el tipo de elemento: 1=Percepcion, 2=Deduccion, 3=HorasExtra, 4=Incapacidad, 5=AccionesOTitulos, 6=JubilacionPensionRetiro, 7=SeparacionIndemnizacion, 8=OtroPago, 9=CompensacionSaldosAFavor
        /// Default:
        /// Nullable:True
        /// </summary>           
        int IdDoc { get; set; }

        /// <summary>
        /// obtener o establecer clave agrupadora. Clasifica la percepción o deducción, ó Razón de la incapacidad: Catálogo publicado en el portal del SAT en internet
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Tipo { get; set; }

        /// <summary>
        /// Desc:Numero de Quincena
        /// Default:0
        /// Nullable:True
        /// </summary>           
        int Quincena { get; set; }

        /// <summary>
        /// Desc:Año de poliza
        /// Default:0
        /// Nullable:True
        /// </summary>           
        int Anio { get; set; }

        /// <summary>
        /// Desc:Número de días en que el trabajador realizó horas extra en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        int Dias { get; set; }

        /// <summary>
        /// Desc:HorasExtra: Número de horas extra trabajadas en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        int HorasExtra { get; set; }

        /// <summary>
        /// Desc:DiasIncapacidad: Número de días que el trabajador se incapacitó en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        int DiasIncapacidad { get; set; }

        /// <summary>
        /// Desc:ImporteGravado: representa el importe gravado de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        decimal ImporteGravado { get; set; }

        /// <summary>
        /// Desc:ImporteExento: representa el importe exento de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        decimal ImporteExento { get; set; }

        /// <summary>
        /// Desc:Monto del descuento por la incapacidad
        /// Default:
        /// Nullable:True
        /// </summary>           
        decimal DescuentoIncapcidad { get; set; }

        /// <summary>
        /// Desc:Importe pagado por las horas extra
        /// Default:
        /// Nullable:True
        /// </summary>           
        decimal ImportePagadoHorasExtra { get; set; }

        /// <summary>
        /// Desc:TipoHoras: tipo de pago de las horas extra: dobles o triples
        /// Default:
        /// Nullable:True
        /// </summary>           
        string TipoHoras { get; set; }

        /// <summary>
        /// Desc:clave de percepción ó deducción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres pattern value="[^|]{3,15}"
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Clave { get; set; }

        /// <summary>
        /// Desc:descripción del concepto de percepción o deducción pattern value="[^|]{1,100}
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Concepto { get; set; }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        DateTime FechaNuevo { get; set; }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
         DateTime? FechaMod { get; set; }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
         string Creo { get; set; }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:
        /// Nullable:True
        /// </summary>           
        string Modifica { get; set; }

        /// <summary>
        /// Desc:NumEmpleado: requerido para expresar el número de empleado de 1 a 15 posiciones pattern value="[^|]{1,15}
        /// Default:
        /// Nullable:True
        /// </summary>           
        string NumEmpleado { get; set; }

        /// <summary>
        /// Desc: importe de otros pagos
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        decimal Importe { get; set; }
    }
}
