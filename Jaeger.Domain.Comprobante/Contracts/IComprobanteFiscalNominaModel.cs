﻿using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteFiscalNominaModel : IComprobanteFiscalModel {
        IComplementoNominaDetailModel Complemento { get; set; }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        IComplementoTimbreFiscal TimbreFiscal { get; set; }

        /// <summary>
        /// obtener o establecer tipo de comprobante
        /// </summary>
        CFDITipoComprobanteEnum TipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar si el comprobante ampara una operación de exportación.
        /// </summary>
        CFDIExportacionEnum Exportacion { get; set; }

        /// <summary>
        /// obtener o establecer si el comprobante es editable
        /// </summary>
        bool IsEditable { get; }

        object Tag { get; set; }

        string KeyName();
    }
}
