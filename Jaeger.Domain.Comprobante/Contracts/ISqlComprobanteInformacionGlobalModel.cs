﻿using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Contracts;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComprobanteInformacionGlobalModel : IGenericRepository<ComprobanteInformacionGlobalModel> {

    }
}
