﻿using System;

namespace Jaeger.Domain.Comprobante.Contracts {
    /// <summary>
    /// clase para la vista de complemento de pago y documentos relacionados
    /// </summary>
    public interface IComplementoPagoMergeDoctoModel {
        #region informacion del comprobante fiscal de recepcion de pagos
        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        string SerieP { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        string FolioP { get; set; }

        string IdDocumentoP { get; set; }

        string StatusP { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        DateTime FechaEmisionP { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        string EmisorRFCP { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        string EmisorNombreP { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        string ReceptorRFCP { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        string ReceptorNombreP { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        string EstadoP { get; set; }
        #endregion

        #region informacion del comprobante relacionado al complemento
        /// <summary>
        /// obtener o establecer el indice del comprobante relacionado al complemento del pago (CFDI)
        /// </summary>
        int IdComprobanteR { get; set; }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        string IdDocumentoDR { get; set; }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        int NumParcialidad { get; set; }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        decimal ImpSaldoAnt { get; set; }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        decimal ImpPagado { get; set; }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        decimal ImpSaldoInsoluto { get; set; }
        #endregion

        #region validaciones
        string this[string columnName] { get; }

        string Error { get; }
        #endregion
    }
}
