﻿using System.Collections.Generic;
using Jaeger.Domain.Contracts;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlComprobanteFiscalConceptoRepository : IGenericRepository<ComprobanteConceptoModel> {
        /// <summary>
        /// almacenar un concepto de un comprobante fiscal
        /// </summary>
        /// <param name="concepto">objeto detail ComprobanteConceptoDetailModel</param>
        ComprobanteConceptoDetailModel Save(ComprobanteConceptoDetailModel concepto);
        /// <summary>
        /// obtener listado de conceptos por indice del comprobante fiscal
        /// </summary>
        /// <param name="index">indic</param>
        IEnumerable<ComprobanteConceptoDetailModel> GetList(int index);

        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
