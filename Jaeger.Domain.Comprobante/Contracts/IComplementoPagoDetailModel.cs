﻿using System.ComponentModel;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComplementoPagoDetailModel {
        PagosTotales Totales { get; set; }
        BindingList<PagosPagoDoctoRelacionadoDetailModel> DoctoRelacionados { get; set; }
        decimal Total { get; set; }
        PagosTotales Totales1 { get; }

        /// <summary>
        /// buscar mediante uuid un objeto relacionado
        /// </summary>
        /// <param name="uuid">Id Documento (uuid)</param>
        PagosPagoDoctoRelacionadoDetailModel Search(string uuid);

        /// <summary>
        /// Agregar un documento relacionado siempre y cuando no exista
        /// </summary>
        /// <param name="documento"></param>
        bool AgregarDocto(PagosPagoDoctoRelacionadoDetailModel documento);
    }
}
