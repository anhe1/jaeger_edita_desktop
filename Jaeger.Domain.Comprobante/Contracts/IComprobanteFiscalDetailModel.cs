﻿using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IComprobanteFiscalDetailModel : IComprobanteFiscalModel {
        #region propiedades
        /// <summary>
        /// obtener o establecer el subtipo de comprobante (emitido, recibo o nomina)
        /// </summary>
        CFDISubTipoEnum SubTipo { get; set; }

        /// <summary>
        /// obtener o establecer tipo de comprobante
        /// </summary>
        CFDITipoComprobanteEnum TipoComprobante { get; set; }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar si el comprobante ampara una operación de exportación.
        /// </summary>
        CFDIExportacionEnum Exportacion { get; set; }

        ComprobanteCfdiRelacionadosModel CfdiRelacionados { get; set; }

        /// <summary>
        /// objeto emisor
        /// </summary>
        ComprobanteContribuyenteModel Emisor { get; set; }

        /// <summary>
        /// objeto receptor
        /// </summary>
        ComprobanteContribuyenteModel Receptor { get; set; }

        /// <summary>
        /// lista de conceptos del comprobante
        /// </summary>
        BindingList<ComprobanteConceptoDetailModel> Conceptos { get; set; }

        /// <summary>
        /// obtener o establecer complemento de recepcion de pagos
        /// </summary>
        BindingList<ComplementoPagoDetailModel> RecepcionPago { get; set; }

        /// <summary>
        /// obtener o establecer complemento de carta porte
        /// </summary>
        CartaPorteDetailModel CartaPorte { get; set; }

        ComplementoNominaDetailModel Nomina12 { get; set; }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        ComplementoTimbreFiscal TimbreFiscal { get; set; }

        /// <summary>
        /// obtener o establecer objeto de complemento nomina
        /// </summary>
        ComplementoNomina Nomina { get; set; }

        /// <summary>
        /// obtener o establecer si el comprobante es editable
        /// </summary>
        bool IsEditable { get; }

        object Tag { get; set; }
        #endregion
        void Default();
    }
}
