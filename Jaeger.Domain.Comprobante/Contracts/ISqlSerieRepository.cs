﻿using Jaeger.Domain.Comprobante.Entities;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface ISqlSerieRepository : Domain.Contracts.IGenericRepository<SerieFolioModel> {
    }
}
