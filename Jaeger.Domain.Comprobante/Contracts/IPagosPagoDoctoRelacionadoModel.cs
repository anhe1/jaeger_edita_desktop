﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;

namespace Jaeger.Domain.Comprobante.Contracts {
    public interface IPagosPagoDoctoRelacionadoModel {
        /// <summary>
        /// obtener o establecer el indice de relacion de la tabla
        /// </summary>
        int IdRelacion { get; set; }

        /// <summary>
        /// obener o establecer registro activo
        /// </summary>
        bool Activo { get; set; }

        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de pagos
        /// </summary>
        int IdPago { get; set; }

        /// <summary>
        /// obtener o establecer el indice del comprobante fiscal de pagos
        /// </summary>
        int IdComprobanteP { get; set; }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        int IdSubTipo { get; set; }

        /// <summary>
        /// obtener o establecer el indice del comprobante relacionado al complemento del pago (CFDI) (_cmppgd_cfdi_id)
        /// </summary>
        int IdComprobanteR { get; set; }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        string IdDocumentoDR { get; set; }

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        string SerieDR { get; set; }

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        string Folio { get; set; }

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        string RFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        string Nombre { get; set; }

        DateTime FechaEmision { get; set; }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        string FormaDePagoP { get; set; }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        string MetodoPago { get; set; }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento 
        /// relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto”
        /// de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        string MonedaDR { get; set; }

        /// <summary>
        /// obtener o establecer tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento 
        /// relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que 
        /// equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD. El pago se realiza por 100 EUR. Este 
        /// atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD.
        /// </summary>
        decimal EquivalenciaDR { get; set; }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        int NumParcialidad { get; set; }

        /// <summary>
        /// obtener o establecer el importe total del comprobante relacionado
        /// </summary>
        decimal Total { get; set; }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        decimal ImpSaldoAnt { get; set; }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        decimal ImpPagado { get; set; }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        decimal ImpSaldoInsoluto { get; set; }

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        string ObjetoImpDR { get; set; }

        /// <summary>
        /// obtener o establecer objeto json con información de los impuestos retenciones aplicables
        /// </summary>
        string JRetenciones { get; set; }

        /// <summary>
        /// obtener o establecer objeto json con información de los impuestos traslados aplicables
        /// </summary>
        string JTraslados { get; set; }

        #region propiedades ignoradas
        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        CFDISubTipoEnum SubTipo { get; }

        decimal SaldoInsoluto { get; }

        BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR> Retenciones { get; set; }

        BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR> Traslados { get; set; }
        #endregion
    }
}
