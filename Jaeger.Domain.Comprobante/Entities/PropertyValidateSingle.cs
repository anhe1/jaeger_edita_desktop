﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class PropertyValidateSingle {
        public PropertyValidateSingle() {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PropertyValidateSingle(PropertyTypeEnum tipo, string codigo, string nombre, string valor, bool isValido = true) {
        }

        [JsonProperty("Tipo")]
        public PropertyTypeEnum Type {
            get;set;
        }

        [JsonProperty("Codigo")]
        public string Code {
            get;set;
        }

        [JsonProperty("Propiedad")]
        public string PropertyName {
            get;set;
        }

        [JsonProperty("Valor")]
        public string Value {
            get;set;
        }
    }
}
