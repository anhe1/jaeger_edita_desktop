﻿using SqlSugar;
using System;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteReciboEletronicoPagoSingle : Complemento.Pagos.ComplementoPagoDetailModel, Contracts.IComprobanteSingle {
        private DateTime? fechaEstado;
        private DateTime? fechaCancela;
        private DateTime? fechaTimbrado;
        private string fileXML;
        private string filePDF;
        private DateTime fechaEmision;
        private string claveMoneda;
        private ComprobanteCfdiRelacionadosModel _ComprobantesRelacionados;

        public ComprobanteReciboEletronicoPagoSingle() : base() { }

        /// <summary>
        /// obtener ó establecer el indice de la tabla. (cfdi_id)
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id { get; set; }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina (_cfdi_doc_id)
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdSubTipo { get; set; }

        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
        }

        /// <summary>
        /// documento (factura, nota de credito,nota de cargo)
        /// </summary>
        [DataNames("_cfdi_doc")]
        [SugarColumn(ColumnName = "_cfdi_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36)]
        public string Documento { get; set; }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText { get; set; }

        /// <summary>
        /// obtener o establecer tipo de comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDITipoComprobanteEnum TipoComprobante {
            get {
                if (this.TipoComprobanteText == "Ingreso" || this.TipoComprobanteText == "I")
                    return CFDITipoComprobanteEnum.Ingreso;
                else if (this.TipoComprobanteText == "Egreso" || this.TipoComprobanteText == "E")
                    return CFDITipoComprobanteEnum.Egreso;
                else if (this.TipoComprobanteText == "Traslado" || this.TipoComprobanteText == "T")
                    return CFDITipoComprobanteEnum.Traslado;
                else if (this.TipoComprobanteText == "Nomina" || this.TipoComprobanteText == "N")
                    return CFDITipoComprobanteEnum.Nomina;
                else if (this.TipoComprobanteText == "Pagos" || this.TipoComprobanteText == "P")
                    return CFDITipoComprobanteEnum.Pagos;
                else
                    return CFDITipoComprobanteEnum.Ingreso;
            }
            set {
                this.TipoComprobanteText = Enum.GetName(typeof(CFDITipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie { get; set; }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio { get; set; }

        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status { get; set; }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado { get; set; }

        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio { get; set; }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC { get; set; }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre { get; set; }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC { get; set; }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre { get; set; }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento { get; set; }


        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdi_fecedo")]
        [SugarColumn(ColumnName = "_cfdi_fecedo", ColumnDescription = "última fecha en que fué consultado el estado del comprobante", IsNullable = true)]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstado >= firstGoodDate)
                    return this.fechaEstado;
                return null;
            }
            set {
                this.fechaEstado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnName = "_cfdi_url_xml", ColumnDescription = "url de descarga para el archivo xml", ColumnDataType = "Text")]
        public string UrlFileXML {
            get {
                return this.fileXML;
            }
            set {
                this.fileXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnName = "_cfdi_url_pdf", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get {
                return this.filePDF;
            }
            set {
                this.filePDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel", ColumnDescription = "comprobantes relacionados (json)", ColumnDataType = "Text")]
        public string JCfdiRelacionados {
            get {
                if (this._ComprobantesRelacionados != null)
                    return this._ComprobantesRelacionados.Json();
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this._ComprobantesRelacionados = ComprobanteCfdiRelacionadosModel.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer objeto de CFDI relacionados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionadosModel CfdiRelacionados {
            get {
                return this._ComprobantesRelacionados;
            }
            set {
                this._ComprobantesRelacionados = value;
                this.OnPropertyChanged();
            }
        }

        public string KeyName() {
            string nombre;
            if (!string.IsNullOrEmpty(this.IdDocumento)) {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaTimbre.Value.ToString("yyyyMMddHHmmss"));
            } else {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
