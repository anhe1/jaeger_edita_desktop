﻿using System;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Comprobante.Entities {
    
    [SugarTable("_cfdrl", "tabla de cfdi relacionados")]
    public class ComprobanteRelacionadoModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool activo;
        private int idComprobante;
        private string idDocumentoField;
        private string serieField;
        private string folioField;
        private string monedaField;
        private decimal tipoCambioDRField;
        private string metodoPagoField;
        private string formaPagoField;
        private decimal impTotalField;
        private string rfcField;
        private string nombreField;
        private DateTime? fechaEmisionField;
        private int subTipoInt;
        #endregion

        public ComprobanteRelacionadoModel() {

        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdrl_id")]
        [SugarColumn(ColumnName = "_cfdrl_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdrl_a")]
        [SugarColumn(ColumnName = "_cfdrl_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdrl_doc_id")]
        [SugarColumn(ColumnName = "_cfdrl_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdSubTipo {
            get { return this.subTipoInt; }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con la tabla de comprobantes fiscales (_cfdrl)
        /// </summary>
        [DataNames("_cfdrl_sbid")]
        [SugarColumn(ColumnName = "_cfdrl_sbid", ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi)")]
        public int IdComprobante {
            get { return this.idComprobante; }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el folio fiscal (UUID) de un CFDI relacionado con el presente comprobante, por ejemplo: Si el CFDI relacionado es un comprobante de traslado 
        /// que sirve para registrar el movimiento de la mercancía. Si este comprobante se usa como nota de crédito o nota de débito del comprobante relacionado. Si este comprobante es 
        /// una devolución sobre el comprobante relacionado. Si éste sustituye a una factura cancelada.
        /// </summary>
        [DataNames("_cfdrl_uuid")]
        [SugarColumn(ColumnName = "_cfdrl_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                if (value != null) {
                    this.idDocumentoField = value.ToUpper();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [JsonProperty("serie", Order = 2)]
        [DataNames("_cfdrl_serie")]
        [SugarColumn(ColumnName = "_cfdrl_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [JsonProperty("folio", Order = 3)]
        [DataNames("_cfdrl_folio")]
        [SugarColumn(ColumnName = "_cfdrl_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("rfc", Order = 11)]
        [DataNames("_cfdrl_rfce")]
        [SugarColumn(ColumnName = "_cfdrl_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("nombre", Order = 12)]
        [DataNames("_cfdrl_nome")]
        [SugarColumn(ColumnName = "_cfdrl_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de emision del comprobante fiscal, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("fechaEmision", Order = 13)]
        [DataNames("_cfdrl_fecems")]
        [SugarColumn(ColumnName = "_cfdrl_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime? FechaEmision {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmisionField >= firstGoodDate) {
                    return this.fechaEmisionField;
                }
                return null;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("moneda", Order = 4)]
        [DataNames("_cfdrl_moneda")]
        [SugarColumn(ColumnName = "_cfdrl_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get {
                return this.monedaField;
            }
            set {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD El pago se realiza por 100 EUR. Este atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD
        /// </summary>
        [JsonProperty("tipoCambio", Order = 5)]
        [DataNames("_cfdrl_tcam")]
        [SugarColumn(ColumnName = "_cfdrl_tcam", ColumnDescription = "tipo de cambio FIX conforme con la moneda usada", Length = 14, DecimalDigits = 6, IsNullable = false)]
        public decimal TipoCambio {
            get {
                return this.tipoCambioDRField;
            }
            set {
                this.tipoCambioDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("metodoPago", Order = 6)]
        [DataNames("_cfdrl_mtdpg")]
        [SugarColumn(ColumnName = "_cfdrl_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.metodoPagoField;
            }
            set {
                this.metodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la clave de la forma de pago del comprobante
        /// </summary>
        [JsonProperty("formaPago", Order = 7)]
        [DataNames("_cfdrl_frmpg")]
        [SugarColumn(ColumnName = "_cfdrl_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.formaPagoField;
            }
            set {
                this.formaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [JsonProperty("total", Order = 8)]
        [DataNames("_cfdrl_total")]
        [SugarColumn(ColumnName = "_cfdrl_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.impTotalField;
            }
            set {
                this.impTotalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
            set {
                this.IdSubTipo = (int)value;
                this.OnPropertyChanged();
            }
        }
    }
}
