﻿using System;

namespace Jaeger.Domain.Comprobante.Entities {
    public class EstadoCuentaSingleView {
        private decimal total;

        public string RFC {
            get; set;
        }
        public string Tipo {
            get; set;
        }
        public string Receptor {
            get; set;
        }
        public string Status {
            get; set;
        }
        public string IdDocumento {
            get; set;
        }
        public string Folio {
            get; set;
        }
        public string Serie {
            get; set;
        }
        public DateTime FechaEmision {
            get; set;
        }
        public DateTime? FechaCobro {
            get; set;
        }

        public decimal Total {
            get {
                if (this.Tipo.ToLower() == "egreso")
                    return 1 - this.total;
                return this.total;
            }
            set {
                this.total = value;
            }
        }

        public decimal Descuento {
            get; set;
        }
        public decimal Acumulado {
            get; set;
        }

        public decimal Saldo {
            get {
                return this.Total - this.Acumulado;
            }
        }

        public decimal IVATraslado { get; set; }
        public decimal IEPSTraslado { get; set; }
        public decimal IVARetencion { get; set; }
        public decimal ISRRetenido { get; set; }
        public decimal IEPSRetenido { get; set; }

        public int DiasCobranza {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.FechaCobro >= firstGoodDate)
                    return ((TimeSpan)(this.FechaCobro - this.FechaEmision)).Days;
                return ((DateTime.Now - this.FechaEmision)).Days;
            }
        }
    }
}
