﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// clase vista para Comprobante y Conceptos
    /// </summary>
    public class ComprobanteFiscalConceptoView : ComprobanteFiscalModel, IComprobanteFiscalModel {
        #region declaraciones
        private int index;
        private int subId;
        private bool activo;
        private int numPedido;
        private string claveProdServ;
        private string noIdentificacion;
        private decimal cantidad;
        private string claveUnidad;
        private string unidad;
        private string descripcion;
        private decimal valorUnitario;
        private string ctaPredial;
        private decimal trasladoIVA;
        private decimal trasladoIEPS;
        private decimal retencionIVA;
        private decimal retencionISR;
        private string _claveObjetoImpuesto;
        private decimal retencionIEPS;
        private bool _AcuentaTerceraSpecified = false;
        #endregion

        public ComprobanteFiscalConceptoView() : base() {
            this.DecimalsChange += this.ComprobanteFiscalDetailModel_DecimalsChange;
        }

        private void ComprobanteFiscalDetailModel_DecimalsChange(object sender, int e) {
            //throw new NotImplementedException();
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer el indice de la tabla de conceptos
        /// </summary>
        [DataNames("_cfdcnp_id")]
        [SugarColumn(ColumnName = "_cfdcnp_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int IdConcepto {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el comprobante
        /// </summary>
        [DataNames("_cfdcnp_cfds_id")]
        [SugarColumn(ColumnName = "_cfdcnp_cfds_id", ColumnDescription = "indice de relacion con la tabla de comprobantes")]
        public int SubId {
            get {
                return this.subId;
            }
            set {
                this.subId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el registro se encuentra activo
        /// </summary>
        [DataNames("_cfdcnp_a")]
        [SugarColumn(ColumnName = "_cfdcnp_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1")]
        public new bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// asignar numero de pedido 
        /// </summary>
        [DataNames("_cfdcnp_pdd_id")]
        [SugarColumn(ColumnName = "_cfdcnp_pdd_id", ColumnDescription = "numero de pedido")]
        public int NumPedido {
            get {
                return this.numPedido;
            }
            set {
                this.numPedido = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, 
        /// cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.
        /// </summary>
        [Description("Atributo requerido para expresar la clave del producto o del servicio amparado por la presente parte. Es requerido y deben utilizar las claves del catálogo de productos y servicios, cuando los conceptos que registren por sus actividades correspondan con dichos conceptos.")]
        [DisplayName("Clave de Producto / Servicio")]
        [DataNames("_cfdcnp_clvprds")]
        [SugarColumn(ColumnName = "_cfdcnp_clvprds", ColumnDescription = "clave del producto o servicio", Length = 8)]
        public string ClaveProdServ {
            get {
                return this.claveProdServ;
            }
            set {
                if (value != null) {
                    this.claveProdServ = Regex.Replace(value.ToString(), "[^\\d]", "");
                    //else
                    //    this.ClaveProdServ = Regex.Replace(value.ToString(), "[^\\d]", "");
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el número de serie, número de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves 
        /// del estándar GTIN.
        /// pattern value="[^|]{1,100}", Longitud: 100
        /// </summary>
        [DisplayName("No. de Identificación")]
        [DataNames("_cfdcnp_noidnt")]
        [SugarColumn(ColumnName = "_cfdcnp_noidnt", ColumnDescription = "numero de serie, numero de parte del bien o identificador del producto o del servicio amparado por la presente parte. Opcionalmente se puede utilizar claves", Length = 64)]
        public string NoIdentificacion {
            get {
                return this.noIdentificacion;
            }
            set {
                this.noIdentificacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la cantidad de bienes o servicios del tipo particular definido por el presente concepto
        /// </summary>
        [DisplayName("Cantidad")]
        [DataNames("_cfdcnp_cntdd")]
        [SugarColumn(ColumnName = "_cfdcnp_cntdd", ColumnDescription = "cantidad de bienes o servicios del tipo particular definido", Length = 14, DecimalDigits = 4)]
        public decimal Cantidad {
            get {
                return this.cantidad;
            }
            set {
                this.cantidad = value;
                this.ValorUnitario = this.ValorUnitario;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DisplayName("Clave Unidad")]
        [DataNames("_cfdcnp_clvund")]
        [SugarColumn(ColumnName = "_cfdcnp_clvund", ColumnDescription = "clave de unidad de medida estandarizada aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.", Length = 4)]
        public string ClaveUnidad {
            get {
                return this.claveUnidad;
            }
            set {
                this.claveUnidad = (value != null ? value.ToUpper().Trim() : value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la unidad de medida propia de la operación del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripción del concepto.
        /// </summary>
        [DisplayName("Unidad")]
        [DataNames("_cfdcnp_undd")]
        [SugarColumn(ColumnName = "_cfdcnp_undd", ColumnDescription = "unidad de medida propia de la operacion del emisor, aplicable para la cantidad expresada en el concepto. La unidad debe corresponder con la descripcion del concepto.", Length = 25)]
        public string Unidad {
            get {
                return this.unidad;
            }
            set {
                this.unidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del bien o servicio cubierto por el presente concepto
        /// </summary>
        [DisplayName("Descripción")]
        [DataNames("_cfdcnp_cncpt")]
        [SugarColumn(ColumnName = "_cfdcnp_cncpt", ColumnDescription = "descripcion del bien o servicio cubierto por el presente concepto", Length = 1000)]
        public string Descripcion {
            get {
                return this.descripcion;
            }
            set {
                this.descripcion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor o precio unitario del bien o servicio cubierto por el presente concepto
        /// </summary>
        [DisplayName("Unitario")]
        [DataNames("_cfdcnp_untr")]
        [SugarColumn(ColumnName = "_cfdcnp_untr", ColumnDescription = "valor o precio unitario del bien o servicio cubierto por el presente concepto", Length = 14, DecimalDigits = 4)]
        public decimal ValorUnitario {
            get {
                //return Math.Round(this.valorUnitario, this.PresionDecimal);
                return this.valorUnitario;
            }
            set {
                this.valorUnitario = value;
                this.Importe = (this.Cantidad * this.ValorUnitario);
                this.SubTotal = (this.Importe - this.Descuento);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establcer el importe total de los bienes o servicios del presente concepto. Debe ser equivalente al resultado de multiplicar la cantidad por el valor unitario expresado en el concepto. No se permiten valores negativos.
        /// </summary>
        [DisplayName("Importe")]
        [DataNames("_cfdcnp_sbttl")]
        [SugarColumn(ColumnName = "_cfdcnp_sbttl", Length = 14, DecimalDigits = 4)]
        public decimal Importe { get; set; }

        [DisplayName("SubTotal")]
        [SugarColumn(IsIgnore = true)]
        public decimal SubTotal { get; set; }

        /// <summary>
        /// obtener o establecer el importe de los descuentos aplicables al concepto. No se permiten valores negativos.
        /// </summary>
        [DisplayName("Descuento")]
        [DataNames("_cfdcnp_dscnt")]
        [SugarColumn(ColumnName = "_cfdcnp_dscnt", Length = 14, DecimalDigits = 4)]
        public decimal Descuento { get; set; }

        /// <summary>
        /// obtener o establecer si la operación comercial es objeto o no de impuesto.
        /// </summary>
        [DisplayName("Obj. Impuesto")]
        [DataNames("_cfdcnp_clvobj")]
        [SugarColumn(ColumnName = "_cfdcnp_clvobj", ColumnDescription = "establecer si la operacion es objeto o no de impuesto", Length = 2)]
        public string ObjetoImp {
            get { return this._claveObjetoImpuesto; }
            set {
                this._claveObjetoImpuesto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de la cuenta predial del inmueble cubierto por el presente concepto, o bien para incorporar los datos de identificación del certificado 
        /// de participación inmobiliaria no amortizable, tratándose de arrendamiento.
        /// pattern value="[0-9]{1,150}", Longitud: 150
        /// </summary>
        [DisplayName("Cuenta Predial")]
        [DataNames("_cfdcnp_ctapre")]
        [SugarColumn(ColumnName = "_cfdcnp_ctapre", Length = 150)]
        public string CtaPredial {
            get {
                return this.ctaPredial;
            }
            set {
                this.ctaPredial = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdcnp_imtvl1")]
        [SugarColumn(ColumnName = "_cfdcnp_imtvl1", ColumnDescription = "", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get {
                return this.trasladoIVA;
            }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdcnp_imtvl2")]
        [SugarColumn(ColumnName = "_cfdcnp_imtvl2", ColumnDescription = "", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get {
                return this.trasladoIEPS;
            }
            set {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdcnp_retvl2")]
        [SugarColumn(ColumnName = "_cfdcnp_retvl2", ColumnDescription = "", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get {
                return this.retencionIVA;
            }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdcnp_retvl1")]
        [SugarColumn(ColumnName = "_cfdcnp_retvl1", ColumnDescription = "", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get {
                return this.retencionISR;
            }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(ColumnName = "_cfdcnp_retvl3", ColumnDescription = "", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        //[DataNames("_cfdcnp_usr_n")]
        //[SugarColumn(ColumnName = "_cfdcnp_usr_n", Length = 10)]
        //public string Creo {
        //    get {
        //        return this.creo;
        //    }
        //    set {
        //        this.creo = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_cfdcnp_usr_m")]
        //[SugarColumn(ColumnName = "_cfdcnp_usr_m", Length = 10)]
        //public string Modifica {
        //    get {
        //        return this.modifica;
        //    }
        //    set {
        //        this.modifica = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataNames("_cfdcnp_fn")]
        //[SugarColumn(ColumnName = "_cfdcnp_fn")]
        //public DateTime? FechaNuevo {
        //    get {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaNuevo >= firstGoodDate)
        //            return this.fechaNuevo;
        //        return null;
        //    }
        //    set {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (value >= firstGoodDate)
        //            this.fechaNuevo = value;
        //    }
        //}

        //[DataNames("_cfdcnp_fm")]
        //[SugarColumn(ColumnName = "_cfdcnp_fm")]
        //public DateTime? FechaModifica {
        //    get {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (this.fechaModifica >= firstGoodDate)
        //            return this.fechaModifica;
        //        return null;
        //    }
        //    set {
        //        DateTime firstGoodDate = new DateTime(1900, 1, 1);
        //        if (value >= firstGoodDate)
        //            this.fechaModifica = value;
        //    }
        //}

        
        [SugarColumn(IsIgnore = true)]
        public bool AcuentaTerceraSpecified {
            get { return this._AcuentaTerceraSpecified; }
            set {
                this._AcuentaTerceraSpecified = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
