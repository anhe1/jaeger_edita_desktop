﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Abstractions;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComplementoDoctoRelacionadoBase : BasePropertyChangeImplementation {
        private CFDISubTipoEnum subTipoDeComprobanteField;
        private string estadoSATField;
        private string idDocumentoField;
        private string serieField;
        private string folioField;
        private string monedaField;
        private decimal tipoCambioDRField;
        private string metodoPagoField;
        private string formaPagoField;
        private decimal impTotalField;
        private EdoPagoDoctoRelEnum estadoField;
        private string rfcField;
        private string nombreField;
        private DateTime? fechaEmisionField;

        public ComplementoDoctoRelacionadoBase() {
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [JsonIgnore]
        public CFDISubTipoEnum SubTipo {
            get {
                return this.subTipoDeComprobanteField;
            }
            set {
                this.subTipoDeComprobanteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string SubTipoText {
            get {
                return Enum.GetName(typeof(CFDISubTipoEnum), this.subTipoDeComprobanteField);
            }
            set {
                this.subTipoDeComprobanteField = (CFDISubTipoEnum)Enum.Parse(typeof(CFDISubTipoEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electrónica o bien el número de operación de un documento digital.
        /// </summary>
        [JsonProperty("uuid", Order = 1)]
        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                if (value != null) {
                    this.idDocumentoField = value.ToUpper();
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Atributo opcional para precisar la serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("serie", Order = 2)]
        public string Serie {
            get {
                return this.serieField;
            }
            set {
                this.serieField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para precisar el folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [JsonProperty("folio", Order = 3)]
        public string Folio {
            get {
                return this.folioField;
            }
            set {
                this.folioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("moneda", Order = 4)]
        public string Moneda {
            get {
                return this.monedaField;
            }
            set {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD El pago se realiza por 100 EUR. Este atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD
        /// </summary>
        [JsonProperty("tipoCambio", Order = 5)]
        public decimal TipoCambio {
            get {
                return this.tipoCambioDRField;
            }
            set {
                this.tipoCambioDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [JsonProperty("metodoPago", Order = 6)]
        public string MetodoPago {
            get {
                return this.metodoPagoField;
            }
            set {
                this.metodoPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer la clave de la forma de pago del comprobante
        /// </summary>
        [JsonProperty("formaPago", Order = 7)]
        public string FormaPago {
            get {
                return this.formaPagoField;
            }
            set {
                this.formaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("total", Order = 8)]
        public decimal Total {
            get {
                return this.impTotalField;
            }
            set {
                this.impTotalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("estadoSAT", Order = 9)]
        public string EstadoSAT {
            get {
                return this.estadoSATField;
            }
            set {
                this.estadoSATField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante esta registrado en el sistema
        /// </summary>
        [JsonIgnore]
        public EdoPagoDoctoRelEnum Estado {
            get {
                return this.estadoField;
            }
            set {
                this.estadoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("estado", Order = 10)]
        public string EstadoText {
            get {
                return Enum.GetName(typeof(EdoPagoDoctoRelEnum), this.estadoField);
            }
            set {
                this.estadoField = (EdoPagoDoctoRelEnum)Enum.Parse(typeof(EdoPagoDoctoRelEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("rfc", Order = 11)]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("nombre", Order = 12)]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de emision del comprobante fiscal, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [JsonProperty("fechaEmision", Order = 13)]
        public DateTime? FechaEmision {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEmisionField >= firstGoodDate) {
                    return this.fechaEmisionField;
                }
                return null;
            }
            set {
                this.fechaEmisionField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        }

        public static ComplementoDoctoRelacionado Json(string inputJson) {
            if (!string.IsNullOrEmpty(inputJson)) { 
                try {
                    return JsonConvert.DeserializeObject<ComplementoDoctoRelacionado>(inputJson);
                } catch {
                    return null;
                }
            }
            return null;
        }
    }
}
