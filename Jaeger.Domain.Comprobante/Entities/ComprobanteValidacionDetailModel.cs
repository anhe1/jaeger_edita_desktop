﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteValidacionDetailModel : ComprobanteValidacionModel {
        public ComprobanteValidacionDetailModel() : base() {

        }
        [JsonIgnore]
        public string ValidoText {
            get {
                return Enum.GetName(typeof(CFDIValidacionEnum), this.Valido);
            }
            set {
                this.Valido = (CFDIValidacionEnum)(Enum.Parse(typeof(CFDIValidacionEnum), value));
            }
        }

        public new string Json() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, 0, conf);
        }

        public new static ComprobanteValidacionDetailModel Json(string inputJson) {
            return JsonConvert.DeserializeObject<ComprobanteValidacionDetailModel>(inputJson);
        }
    }
}
