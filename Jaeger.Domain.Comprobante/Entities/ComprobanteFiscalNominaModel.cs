﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalNominaModel : ComprobanteFiscalModel, IComprobanteFiscalNominaModel, IComprobanteFiscalModel {
        private IComplementoNominaDetailModel _Complemento;
        //private BindingList<ComprobanteConceptoDetailModel> _Conceptos;

        public ComprobanteFiscalNominaModel() {
            this.Exportacion = CFDIExportacionEnum.No_Aplica;
            this.TipoComprobante = CFDITipoComprobanteEnum.Nomina;
            this._Complemento = new ComplementoNominaDetailModel();
        }

        [SugarColumn(IsIgnore = true)]
        public IComplementoNominaDetailModel Complemento {
            get { return this._Complemento; }
            set { this._Complemento = value; }
        }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public IComplementoTimbreFiscal TimbreFiscal {
            get {
                if (ValidacionService.UUID(this.IdDocumento))
                    return new ComplementoTimbreFiscal {
                        UUID = this.IdDocumento,
                        NoCertificadoSAT = this.NoCertificado,
                        FechaTimbrado = this.FechaTimbre,
                        RFCProvCertif = this.RFCProvCertif
                    };
                return null;
            }
            set {
                if (value != null) {
                    this.IdDocumento = value.UUID;
                    this.RFCProvCertif = value.RFCProvCertif;
                    this.NoCertificado = value.NoCertificadoSAT;
                    this.FechaTimbre = value.FechaTimbrado;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer tipo de comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDITipoComprobanteEnum TipoComprobante {
            get {
                if (base.TipoComprobanteText == "Ingreso" || base.TipoComprobanteText == "I")
                    return CFDITipoComprobanteEnum.Ingreso;
                else if (base.TipoComprobanteText == "Egreso" || base.TipoComprobanteText == "E")
                    return CFDITipoComprobanteEnum.Egreso;
                else if (base.TipoComprobanteText == "Traslado" || base.TipoComprobanteText == "T")
                    return CFDITipoComprobanteEnum.Traslado;
                else if (base.TipoComprobanteText == "Nomina" || base.TipoComprobanteText == "N")
                    return CFDITipoComprobanteEnum.Nomina;
                else if (base.TipoComprobanteText == "Pagos" || base.TipoComprobanteText == "P")
                    return CFDITipoComprobanteEnum.Pagos;
                else
                    return CFDITipoComprobanteEnum.Ingreso;
            }
            set {
                base.TipoComprobanteText = Enum.GetName(typeof(CFDITipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar si el comprobante ampara una operación de exportación.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDIExportacionEnum Exportacion {
            get {
                if (base.ClaveExportacion == "02")
                    return CFDIExportacionEnum.Definitiva;
                else if (base.ClaveExportacion == "03")
                    return CFDIExportacionEnum.Temporal;
                return CFDIExportacionEnum.No_Aplica;
            }
            set {
                if (value == CFDIExportacionEnum.No_Aplica)
                    base.ClaveExportacion = "01";
                else if (value == CFDIExportacionEnum.Definitiva)
                    base.ClaveExportacion = "02";
                else if (value == CFDIExportacionEnum.Temporal)
                    base.ClaveExportacion = "03";
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante es editable
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get {
                return !ValidacionService.UUID(this.IdDocumento);
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }

        public string KeyName() {
            string nombre;
            if (this.TimbreFiscal != null) {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.TimbreFiscal.UUID, "-", this.TimbreFiscal.FechaTimbrado.Value.ToString("yyyyMMddHHmmss"));
            } else {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }
    }
}
