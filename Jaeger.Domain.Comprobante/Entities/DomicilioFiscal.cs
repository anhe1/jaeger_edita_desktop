﻿/// develop:
/// purpose:
using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    [JsonObject("domicilioFiscal")]
    public class DomicilioFiscal {
        [JsonProperty("_drccn_cll")]
        public string Calle {
            get; set;
        }

        [JsonProperty("_drccn_cdd")]
        public string Ciudad {
            get; set;
        }

        [JsonProperty("_drccn_cpa")]
        public string CodigoDePais {
            get; set;
        }

        [JsonProperty("_drccn_cp")]
        public string CodigoPostal {
            get; set;
        }

        [JsonProperty("_drccn_cln")]
        public string Colonia {
            get; set;
        }

        [JsonProperty("_drccn_dlg")]
        public string Delegacion {
            get; set;
        }

        [JsonProperty("_drccn_std")]
        public string Estado {
            get; set;
        }

        [JsonProperty("_drccn_lcldd")]
        public string Localidad {
            get; set;
        }

        [JsonProperty("_drccn_extr")]
        public string NoExterior {
            get; set;
        }

        [JsonProperty("_drccn_intr")]
        public string NoInterior {
            get; set;
        }

        [JsonProperty("_drccn_ps")]
        public string Pais {
            get; set;
        }

        [JsonProperty("_drccn_rfrnc")]
        public string Referencia {
            get; set;
        }

        public override string ToString() {
            string direccion = string.Empty;

            if (!(string.IsNullOrEmpty(this.Calle))) {
                direccion = string.Concat("Calle ", this.Calle);
            }

            if (!(string.IsNullOrEmpty(this.NoExterior))) {
                direccion = string.Concat(direccion, " No. Ext. ", this.NoExterior);
            }

            if (!(string.IsNullOrEmpty(this.NoInterior))) {
                direccion = string.Concat(direccion, " No. Int. ", this.NoInterior);
            }

            if (!(string.IsNullOrEmpty(this.Colonia))) {
                direccion = string.Concat(direccion, " Col. ", this.Colonia);
            }

            if (!(string.IsNullOrEmpty(this.CodigoPostal))) {
                direccion = string.Concat(direccion, " C.P. ", this.CodigoPostal);
            }

            if (!(string.IsNullOrEmpty(this.Delegacion))) {
                direccion = string.Concat(direccion, " ", this.Delegacion);
            }

            if (!(string.IsNullOrEmpty(this.Estado))) {
                direccion = string.Concat(direccion, ", ", this.Estado);
            }

            if (!(string.IsNullOrEmpty(this.Referencia))) {
                direccion = string.Concat(direccion, ", Ref: ", this.Referencia);
            }

            if (!(string.IsNullOrEmpty(this.Localidad))) {
                direccion = string.Concat(direccion, ", Loc: ", this.Localidad);
            }
            return direccion;
        }
    }
}
