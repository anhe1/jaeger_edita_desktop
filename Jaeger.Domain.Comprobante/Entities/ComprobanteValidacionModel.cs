﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// reporte de validacion de comprobante
    /// </summary>
    public class ComprobanteValidacionModel : IComprobanteValidacionModel {
        private readonly JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

        public ComprobanteValidacionModel() {
            this.Resultados = new List<PropertyValidate>();
        }

        /// <summary>
        /// obtener o establecer el resultado de la validación
        /// </summary>
        [JsonProperty("valido")]
        public CFDIValidacionEnum Valido {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el proveedor del servicio
        /// </summary>
        [JsonProperty("provider")]
        public string Provider {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la versión del estándar bajo el que se encuentra expresado el comprobante.
        /// </summary>
        [JsonProperty("version")]
        public string Version {
            get; set;
        }

        /// <summary>
        /// para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado
        /// </summary>
        [JsonProperty("efecto")]
        public string TipoComprobante {
            get; set;
        }

        /// <summary>
        /// obtener o establecer folio de control interno del contribuyente.
        /// </summary>
        [JsonProperty("folio")]
        public string Folio {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. 
        /// </summary>
        [JsonProperty("serie")]
        public string Serie {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de emisión del comprobante fiscal
        /// </summary>
        [JsonProperty("fecha")]
        public DateTime FechaEmision {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la fecha de validación
        /// </summary>
        [JsonProperty("fechaValidacion")]
        public DateTime FechaValidacion {
            get; set;
        }

        /// <summary>
        /// obtener o establecer folio discal del comprobante (UUID)
        /// </summary>
        [JsonProperty("uuid")]
        public string IdDocumento {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el nombre, denominación o razón social del contribuyente emisor del comprobante
        /// </summary>
        [JsonProperty("emisor")]
        public string EmisorNombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante
        /// </summary>
        [JsonProperty("emisorRFC")]
        public string EmisorRFC {
            get; set;
        }

        [JsonProperty("receptor")]
        public string ReceptorNombre {
            get; set;
        }

        /// <summary>
        /// obtener o establecer la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.
        /// </summary>
        [JsonProperty("receptorRFC")]
        public string ReceptorRFC {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el monto total del comprobante
        /// </summary>
        [JsonProperty("total")]
        public decimal Total {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el estado SAT del comprobante
        /// </summary>
        [JsonProperty("status")]
        public string Estado {
            get; set;
        }

        [JsonProperty("timeElapsed")]
        public string TimeElapsed {
            get; set;
        }

        /// <summary>
        /// obtener o establecer el listado de validaciones del comprobante
        /// </summary>
        [JsonProperty("validation")]
        public List<PropertyValidate> Resultados {
            get;set;
        }

        [JsonProperty("errors")]
        public List<PropertyValidateSingle> Errores {
            get;set;
        }

        public string Json() {
            Formatting objFormat = 0;
            return JsonConvert.SerializeObject(this, objFormat, conf);
        }

        public static ComprobanteValidacionModel Json(string inputJson) {
            return JsonConvert.DeserializeObject<ComprobanteValidacionModel>(inputJson);
        }
    }
}
