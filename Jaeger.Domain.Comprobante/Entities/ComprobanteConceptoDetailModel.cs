﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdcnp", "")]
    public class ComprobanteConceptoDetailModel : ComprobanteConceptoModel, IDataErrorInfo, IComprobanteConceptoModel, IComprobanteConceptoDetailModel {
        
        public ComprobanteConceptoDetailModel() : base() {
            this.ObjetoDeImpuesto = CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto;
        }

        public ComprobanteConceptoDetailModel(decimal cantidad, decimal valorUnitario, string claveProdServ, string claveUnidad, string descripcion, CFDIObjetoImpuestoEnum objetoImpuestoEnum) : base() {
            this.Cantidad = cantidad;
            this.ValorUnitario = valorUnitario;
            this.ClaveProdServ = claveProdServ;
            this.ClaveUnidad = claveUnidad;
            this.Descripcion = descripcion;
            this.ObjetoDeImpuesto = objetoImpuestoEnum;
            this.PresionDecimal = 2;
        }

        [JsonIgnore]
        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public CFDIObjetoImpuestoEnum ObjetoDeImpuesto {
            get { if (base.ObjetoImp == "01")
                    return CFDIObjetoImpuestoEnum.No_objeto_de_impuesto;
                else if (base.ObjetoImp == "02")
                    return CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto;
                return CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto_y;
            }
            set { if (value == CFDIObjetoImpuestoEnum.No_objeto_de_impuesto)
                    this.ObjetoImp = "01";
                else if (value == CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto)
                    this.ObjetoImp = "02";
                else if (value == CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto_y)
                    this.ObjetoImp = "03";
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                //if (!this.RegexValido(this.ClaveProdServ, "[0-9]{8}") || !this.RegexValido(this.ClaveUnidad, "[0-9,A-Z]{3}|[0-9,A-Z]{2}") || !this.RegexValido(this.Cantidad.ToString(), "[0-9]{1,18}(.[0-9]{1,2})?") || !(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false)) {
                //    return "Por favor ingrese datos válidos en esta fila!";
                //}
                if (!this.RegexValido(this.ClaveUnidad, "[0-9,A-Z]{3}|[0-9,A-Z]{2}")) {
                    return "Formato de clave unidad no valida!";
                }

                if (!this.RegexValido(this.ClaveProdServ, "[0-9]{8}")) {
                    return "Formato de clave de producto ó servicio no válida!";
                }

                if (!(this.Descripcion != null)) {
                    return "Debes ingresar un concepto.";
                }

                if (!(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false)) {
                    return "Debe ingresar un concepto.";
                }
                return string.Empty;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (columnName == "ClaveUnidad" && !this.RegexValido(this.ClaveUnidad, "[0-9,A-Z]{3}|[0-9,A-Z]{2}")) {
                    return "Formato de clave unidad no valida!";
                }

                if (columnName == "ClaveProdServ" && !this.RegexValido(this.ClaveProdServ, "[0-9]{8}")) {
                    return "Formato de clave de producto ó servicio no válida!";
                }

                if (columnName == "Descripcion" && !(this.Descripcion != null)) {
                    return "Debes ingresar un concepto.";
                }

                if (columnName == "Descripcion" && !(this.Descripcion != null ? !(this.Descripcion.Trim() == "") : false)) {
                    return "Debe ingresar un concepto.";
                }

                return string.Empty;
            }
        }

        public virtual new ComprobanteConceptoDetailModel Clone() {
            var duplicado = (ComprobanteConceptoDetailModel)base.Clone();
            duplicado.Id = 0;
            // impuestos sin referencia
            var impuestos1 = new List<ComprobanteConceptoImpuesto>(duplicado.Impuestos);
            duplicado.Impuestos = new BindingList<ComprobanteConceptoImpuesto>();
            impuestos1.ForEach((item) => duplicado.Impuestos.Add(item));
            // concepto parte sin referencia anterior
            var partes = new List<ConceptoParte>(duplicado.Parte);
            duplicado.Parte = new BindingList<ConceptoParte>();
            partes.ForEach((item) => duplicado.Parte.Add(item));
            // informacion aduanera
            var infos = new List<ComprobanteInformacionAduanera>(duplicado.InformacionAduanera);
            duplicado.InformacionAduanera = new BindingList<ComprobanteInformacionAduanera>();
            infos.ForEach((item) => duplicado.InformacionAduanera.Add(item));
            return duplicado;
        }
    }
}
