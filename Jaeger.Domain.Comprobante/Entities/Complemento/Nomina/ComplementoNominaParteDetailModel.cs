﻿using SqlSugar;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    ///<summary>
    /// clase simple que representa la tabla donde se almacen los datos de percepciones y deducciones de las nominas de empleados
    ///</summary>
    [SugarTable("_nmnprt", "Nomina: conceptos de percepciones o deducciones")]
    public class ComplementoNominaParteDetailModel : ComplementoNominaParteModel {
        public ComplementoNominaParteDetailModel() { }

        [SugarColumn(IsIgnore = true)]
        public NominaElementoEnum TipoElemento {
            get {
                switch (this.IdDoc) {
                    case 1:
                        return NominaElementoEnum.Percepcion;
                    case 2:
                        return NominaElementoEnum.Deduccion;
                    case 3:
                        return NominaElementoEnum.HorasExtra;
                    case 4:
                        return NominaElementoEnum.Incapacidad;
                    case 5:
                        return NominaElementoEnum.AccionesOTitulos;
                    case 6:
                        return NominaElementoEnum.JubilacionPensionRetiro;
                    case 7:
                        return NominaElementoEnum.SeparacionIndemnizacion;
                    case 8:
                        return NominaElementoEnum.OtrosPagos;
                    case 9:
                        return NominaElementoEnum.CompensacionSaldosAFavor;
                    default:
                        return NominaElementoEnum.None;
                }
            }
            set {
                this.IdDoc = (int)value;
            }
        }
    }
}
