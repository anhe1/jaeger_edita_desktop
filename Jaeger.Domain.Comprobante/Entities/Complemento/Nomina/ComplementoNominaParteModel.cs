﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    ///<summary>
    /// clase simple que representa la tabla donde se almacen los datos de percepciones y deducciones de las nominas de empleados
    ///</summary>
    [SugarTable("_nmnprt", "Nomina: conceptos de percepciones o deducciones")]
    public class ComplementoNominaParteModel : BasePropertyChangeImplementation, IComplementoNominaParteModel {
        #region declaraciones
        private int index;
        private bool activo;
        private int subIndex;
        private int docId;
        private string tipo;
        private int quincena;
        private int anio;
        private int dias;
        private int horasExtra;
        private int diasIncapacidad;
        private decimal importeGravado;
        private decimal importeExento;
        private decimal descuentoIncapacidad;
        private decimal importePagado;
        private string tipoHorasExtra;
        private string clave;
        private string concepto;
        private string numEmpleado;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica;
        private string creo;
        private string modifica;
        private decimal importe;
        #endregion

        public ComplementoNominaParteModel() {
            this.Activo = true;
            this.FechaNuevo = DateTime.Now;
        }

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_nmnprt_id")]
        [SugarColumn(ColumnName = "_nmnprt_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>       
        [DataNames("_nmnprt_a")]
        [SugarColumn(ColumnName = "_nmnprt_a", IsNullable = false)]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:relacion con catalogo de nominas
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_cfdnmn_id")]
        [SugarColumn(ColumnName = "_nmnprt_cfdnmn_id", IsNullable = true)]
        public int SubId {
            get {
                return this.subIndex;
            }
            set {
                this.subIndex = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de elemento: 1=Percepcion, 2=Deduccion, 3=HorasExtra, 4=Incapacidad, 5=AccionesOTitulos, 6=JubilacionPensionRetiro, 7=SeparacionIndemnizacion, 8=OtroPago, 9=CompensacionSaldosAFavor
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_doc")]
        [SugarColumn(ColumnName = "_nmnprt_doc", IsNullable = true)]
        public int IdDoc {
            get {
                return this.docId;
            }
            set {
                this.docId = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave agrupadora. Clasifica la percepción o deducción, ó Razón de la incapacidad: Catálogo publicado en el portal del SAT en internet
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_tipo")]
        [SugarColumn(ColumnName = "_nmnprt_tipo", IsNullable = true)]
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Numero de Quincena
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_qnc")]
        [SugarColumn(ColumnName = "_nmnprt_qnc", IsNullable = true)]
        public int Quincena {
            get {
                return this.quincena;
            }
            set {
                this.quincena = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Año de poliza
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_anio", IsNullable = true)]
        [DataNames("_nmnprt_anio")]
        public int Anio {
            get {
                return this.anio;
            }
            set {
                this.anio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Número de días en que el trabajador realizó horas extra en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_dias")]
        [SugarColumn(ColumnName = "_nmnprt_dias", IsNullable = true)]
        public int Dias {
            get {
                return this.dias;
            }
            set {
                this.dias = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:HorasExtra: Número de horas extra trabajadas en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_hrsxtr")]
        [SugarColumn(ColumnName = "_nmnprt_hrsxtr", IsNullable = true)]
        public int HorasExtra {
            get {
                return this.horasExtra;
            }
            set {
                this.horasExtra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:DiasIncapacidad: Número de días que el trabajador se incapacitó en el periodo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_dsincp", IsNullable = true)]
        [DataNames("_nmnprt_dsincp")]
        public int DiasIncapacidad {
            get {
                return this.diasIncapacidad;
            }
            set {
                this.diasIncapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ImporteGravado: representa el importe gravado de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_impgrv")]
        [SugarColumn(ColumnName = "_nmnprt_impgrv", IsNullable = true)]
        public decimal ImporteGravado {
            get {
                return this.importeGravado;
            }
            set {
                this.importeGravado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ImporteExento: representa el importe exento de un concepto de percepción o deducción
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_impext", IsNullable = true)]
        [DataNames("_nmnprt_impext")]
        public decimal ImporteExento {
            get {
                return this.importeExento;
            }
            set {
                this.importeExento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Monto del descuento por la incapacidad
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_dscnt")]
        [SugarColumn(ColumnName = "_nmnprt_dscnt", IsNullable = true)]
        public decimal DescuentoIncapcidad {
            get {
                return this.descuentoIncapacidad;
            }
            set {
                this.descuentoIncapacidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Importe pagado por las horas extra
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_imppgd")]
        [SugarColumn(ColumnName = "_nmnprt_imppgd", IsNullable = true)]
        public decimal ImportePagadoHorasExtra {
            get {
                return this.importePagado;
            }
            set {
                this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:TipoHoras: tipo de pago de las horas extra: dobles o triples
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_tphrs")]
        [SugarColumn(ColumnName = "_nmnprt_tphrs", IsNullable = true)]
        public string TipoHoras {
            get {
                return this.tipoHorasExtra;
            }
            set {
                this.tipoHorasExtra = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:clave de percepción ó deducción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres pattern value="[^|]{3,15}"
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_clv")]
        [SugarColumn(ColumnName = "_nmnprt_clv", IsNullable = true)]
        public string Clave {
            get {
                return this.clave;
            }
            set {
                this.clave = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:descripción del concepto de percepción o deducción pattern value="[^|]{1,100}
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_cncpt")]
        [SugarColumn(ColumnName = "_nmnprt_cncpt", IsNullable = true)]
        public string Concepto {
            get {
                return this.concepto;
            }
            set {
                this.concepto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [DataNames("_nmnprt_fn")]
        [SugarColumn(ColumnName = "_nmnprt_fn")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_fm")]
        [SugarColumn(ColumnName = "_nmnprt_fm", IsNullable = true)]
        public DateTime? FechaMod {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                this.fechaModifica = value;
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_usr_n", IsNullable = true)]
        [DataNames("_nmnprt_usr_n")]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                if (this.creo != value) {
                    this.creo = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_usr_m")]
        [SugarColumn(ColumnName = "_nmnprt_usr_m", IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                if (this.modifica != value) {
                    this.modifica = value;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Desc:NumEmpleado: requerido para expresar el número de empleado de 1 a 15 posiciones pattern value="[^|]{1,15}
        /// Default:
        /// Nullable:True
        /// </summary>           
        [DataNames("_nmnprt_numem")]
        [SugarColumn(ColumnName = "_nmnprt_numem", Length = 15, IsNullable = true)]
        public string NumEmpleado {
            get {
                return this.numEmpleado;
            }
            set {
                this.numEmpleado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc: importe de otros pagos
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_nmnprt_imprt", IsNullable = true)]
        [DataNames("_nmnprt_imprt")]
        public decimal Importe {
            get {
                return this.importe;
            }
            set {
                this.importe = value;
                this.OnPropertyChanged();
            }
        }
    }
}
