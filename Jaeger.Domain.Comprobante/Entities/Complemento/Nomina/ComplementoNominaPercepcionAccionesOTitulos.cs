﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
    /// </summary>
    [JsonObject("accionesOTitulos")]
    public partial class ComplementoNominaPercepcionAccionesOTitulos : BasePropertyChangeImplementation {
        #region declarciones
        private decimal valorMercadoField;
        private decimal precioAlOtorgarseField;
        #endregion

        public ComplementoNominaPercepcionAccionesOTitulos() { }

        /// <summary>
        /// obtener o establecer el valor de mercado de las Acciones o Títulos valor al ejercer la opción.
        /// </summary>
        [JsonProperty("valorMercado")]
        public decimal ValorMercado {
            get {
                return this.valorMercadoField;
            }
            set {
                this.valorMercadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el precio establecido al otorgarse la opción de ingresos en acciones o títulos valor.
        /// </summary>
        [JsonProperty("precioAlOtorgarse")]
        public decimal PrecioAlOtorgarse {
            get {
                return this.precioAlOtorgarseField;
            }
            set {
                this.precioAlOtorgarseField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
