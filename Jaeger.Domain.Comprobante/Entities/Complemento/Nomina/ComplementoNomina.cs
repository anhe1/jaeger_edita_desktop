﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("nomina")]
    public class ComplementoNomina : BasePropertyChangeImplementation, IDataErrorInfo, IComplementoNomina {
        #region declaraciones
        private int index;
        private string uuidField;
        private ComplementoNominaEmisor emisorField;
        private ComplementoNominaReceptor receptorField;
        private ComplementoNominaPercepciones percepcionesField;
        private ComplementoNominaDeducciones deduccionesField;
        private BindingList<ComplementoNominaOtroPago> otrosPagosField;
        private BindingList<ComplementoNominaIncapacidad> incapacidadesField;
        private string versionField;
        private string tipoNominaField; //c_TipoNomina
        private DateTime? fechaPagoField;
        private DateTime? fechaInicialPagoField;
        private DateTime? fechaFinalPagoField;
        private decimal numDiasPagadosField;
        private decimal totalPercepcionesField;
        private decimal totalDeduccionesField;
        private decimal totalOtrosPagosField;
        private decimal descuentoField;
        #endregion

        public ComplementoNomina() {
            this.Version = "1.2";
            this.Receptor = new ComplementoNominaReceptor();
            this.Percepciones = new ComplementoNominaPercepciones();
            this.Deducciones = new ComplementoNominaDeducciones();
            this.OtrosPagos = new BindingList<ComplementoNominaOtroPago>();
            this.Incapacidades = new BindingList<ComplementoNominaIncapacidad>();
        }

        #region propiedades
        public int IdComplemento {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la información del contribuyente emisor del comprobante de nómina.
        /// </summary>
        [JsonProperty("emisor")]
        public ComplementoNominaEmisor Emisor {
            get {
                return this.emisorField;
            }
            set {
                this.emisorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para precisar la información del contribuyente receptor del comprobante de nómina.
        /// </summary>
        [JsonProperty("receptor")]
        public ComplementoNominaReceptor Receptor {
            get {
                return this.receptorField;
            }
            set {
                this.receptorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar las percepciones aplicables.
        /// </summary>
        [JsonProperty("percepciones")]
        public ComplementoNominaPercepciones Percepciones {
            get {
                return this.percepcionesField;
            }
            set {
                this.percepcionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para expresar las deducciones aplicables.
        /// </summary>
        [JsonProperty("deducciones")]
        public ComplementoNominaDeducciones Deducciones {
            get {
                return this.deduccionesField;
            }
            set {
                this.deduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar otros pagos aplicables.
        /// </summary>
        [JsonProperty("otrosPagos")]
        public BindingList<ComplementoNominaOtroPago> OtrosPagos {
            get {
                return this.otrosPagosField;
            }
            set {
                this.otrosPagosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar información de las incapacidades.
        /// </summary>
        [JsonProperty("incapacidades")]
        public BindingList<ComplementoNominaIncapacidad> Incapacidades {
            get {
                return this.incapacidadesField;
            }
            set {
                this.incapacidadesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la versión del complemento.
        /// </summary>
        [JsonProperty("version")]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para indicar el tipo de nómina, puede ser O= Nómina ordinaria o E= Nómina extraordinaria.
        /// </summary>
        [JsonProperty("tipo")]
        public string TipoNomina {
            get {
                return this.tipoNominaField;
            }
            set {
                this.tipoNominaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty("fechaPago")]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate)
                    return this.fechaPagoField;
                return null;
            }
            set {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha inicial del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty("fechaInicialPago")]
        public DateTime? FechaInicialPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicialPagoField >= firstGoodDate)
                    return this.fechaInicialPagoField;
                return null;
            }
            set {
                this.fechaInicialPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la fecha final del período de pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601.
        /// </summary>
        [JsonProperty("fechaFinalPago")]
        public DateTime? FechaFinalPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinalPagoField >= firstGoodDate)
                    return this.fechaFinalPagoField;
                return null;
            }
            set {
                this.fechaFinalPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión del número o la fracción de días pagados.
        /// </summary>
        [JsonProperty("numDiasPagados")]
        public decimal NumDiasPagados {
            get {
                return this.numDiasPagadosField;
            }
            set {
                this.numDiasPagadosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de las percepciones.
        /// </summary>
        [JsonProperty("totalPercepciones")]
        public decimal TotalPercepciones {
            get {
                return this.totalPercepcionesField;
            }
            set {
                this.totalPercepcionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de las deducciones aplicables.
        /// </summary>
        [JsonProperty("totalDeducciones")]
        public decimal TotalDeducciones {
            get {
                return this.totalDeduccionesField;
            }
            set {
                this.totalDeduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para representar la suma de otros pagos.
        /// </summary>
        [JsonProperty("totalOtrosPagos")]
        public decimal TotalOtrosPagos {
            get {
                return this.totalOtrosPagosField;
            }
            set {
                this.totalOtrosPagosField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("idDocumento")]
        public string IdDocumento {
            get {
                return this.uuidField;
            }
            set {
                if (value != null)
                    this.uuidField = value.ToUpper();
                else
                    this.uuidField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("descuento")]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region metodos publicos
        [JsonIgnore]
        public int Anio {
            get {
                return this.FechaPago.Value.Year;
            }
        }

        [JsonIgnore]
        public int Quincena {
            get {
                if (!(this.FechaPago == null))
                    return this.FechaPago.Value.Day > 15 ? 2 : 1;
                return 0;
            }
        }

        [JsonIgnore]
        public string this[string columnName] {
            get {
                if (columnName == "") {
                }
                return string.Empty;
            }
        }

        [JsonIgnore]
        public string Error {
            get {
                return string.Empty;
            }
        }
        #endregion

        #region serializacion JSON
        public string Json(int objFormat = 0) {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, (Formatting)objFormat, conf);
        }

        public static ComplementoNomina Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoNomina>(inputJson);
            } catch {
                return null;
            }
        }
        #endregion
    }
}
