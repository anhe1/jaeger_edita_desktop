﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar la información detallada de otros pagos por separación.
    /// </summary>
    [JsonObject("SeparacionIndemnizacion")]
    public partial class ComplementoNominaPercepcionesSeparacionIndemnizacion : BasePropertyChangeImplementation {
        private decimal totalPagadoField;
        private int numAñosServicioField;
        private decimal ultimoSueldoMensOrdField;
        private decimal ingresoAcumulableField;
        private decimal ingresoNoAcumulableField;

        public ComplementoNominaPercepcionesSeparacionIndemnizacion() {

        }

        /// <summary>
        /// obtener o establecer el monto total del pago.
        /// </summary>
        [JsonProperty("totalPagado")]
        public decimal TotalPagado {
            get {
                return this.totalPagadoField;
            }
            set {
                this.totalPagadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de años de servicio del trabajador. Se redondea al entero superior si la cifra contiene años y meses y hay más de 6 meses.
        /// </summary>
        [JsonProperty("numaniosServicio")]
        public int NumaniosServicio {
            get {
                return this.numAñosServicioField;
            }
            set {
                this.numAñosServicioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el último sueldo mensual ordinario.
        /// </summary>
        [JsonProperty("ultimoSueldoMensOrd")]
        public decimal UltimoSueldoMensOrd {
            get {
                return this.ultimoSueldoMensOrdField;
            }
            set {
                this.ultimoSueldoMensOrdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos acumulables.
        /// </summary>
        [JsonProperty("ingresoAcumulable")]
        public decimal IngresoAcumulable {
            get {
                return this.ingresoAcumulableField;
            }
            set {
                this.ingresoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos no acumulables.
        /// </summary>
        [JsonProperty("ingresoNoAcumulable")]
        public decimal IngresoNoAcumulable {
            get {
                return this.ingresoNoAcumulableField;
            }
            set {
                this.ingresoNoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
