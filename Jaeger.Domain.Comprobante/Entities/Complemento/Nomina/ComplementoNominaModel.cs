﻿using System;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [SugarTable("_cfdnmn", "Nomina: complemento nominas")]
    public class ComplementoNominaModel : BasePropertyChangeImplementation, IComplementoNominaModel {
        #region declaraciones
        private int indiceField;
        private bool activoField;
        private int receptorIdField;
        private int controlIdField;
        private int comprobanteIdField;
        private int? quincenaField;
        private int? anioField;
        private decimal numDiasPagadosField;
        private decimal salarioBaseCotAporField;
        private decimal salarioDiarioIntegradoField;
        private decimal percepcionTotalGravadoField;
        private decimal percepcionTotalExentoField;
        private decimal deduccionTotalGravadoField;
        private decimal deduccionTotalExentoField;
        private decimal descuentoField;
        private decimal importePagadoField;
        private string tipoRegimenField;
        private string riesgoPuestoField;
        private string antiguedadField;
        private string entidadFederativaField;
        private string sindicalizadoField;
        private string versionField;
        private string bancoField;
        private string rfcField;
        private string registroPatronalField;
        private string ctaBancoField;
        private string numEmpleadoField;
        private string curpField;
        private string numSeguridadSocialField;
        private string clabeField;
        private string tipoField;
        private string idDocumentoField;
        private string departamentoField;
        private string puestoField;
        private string tipoContratoField;
        private string tipoJornadaField;
        private string periodicidadPagoField;
        private DateTime? fechaPagoField;
        private DateTime? fechaInicialPagoField;
        private DateTime? fechaFinalPagoField;
        private DateTime? fechaInicioRelLaboralField;
        private DateTime fechaNuevoField;
        private DateTime? fechaModificacionField;
        private DateTime? fechaEnvioField;
        private string creoField;
        private string modificaField;
        #endregion

        #region constructores
        public ComplementoNominaModel() {
            this.Activo = true;
        }
        #endregion

        #region propiedades

        /// <summary>
        /// Desc:id principal de la tabla
        /// Default:
        /// Nullable:False
        /// </summary>           
        [DataNames("_cfdnmn_id")]
        [SugarColumn(ColumnName = "_cfdnmn_id", IsPrimaryKey = true, IsIdentity = true)]
        public int Id {
            get {
                return this.indiceField;
            }
            set {
                this.indiceField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:registro activo
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_a", IsNullable = false)]
        [DataNames("_cfdnmn_a")]
        public bool Activo {
            get {
                return this.activoField;
            }
            set {
                this.activoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id de relación con la tabla del directorio
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_drctr_id", IsNullable = false)]
        [DataNames("_cfdnmn_drctr_id")]
        public int ReceptorId {
            get {
                return this.receptorIdField;
            }
            set {
                this.receptorIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Id de control de nominas
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_nmnctrl_id", IsNullable = false)]
        [DataNames("_cfdnmn_nmnctrl_id")]
        public int ControlId {
            get {
                return this.controlIdField;
            }
            set {
                this.controlIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:id de relación con la tabla de comprobantes
        /// Default:0
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_cfdi_id", IsNullable = false)]
        [DataNames("_cfdnmn_cfdi_id")]
        public int SubId {
            get {
                return this.comprobanteIdField;
            }
            set {
                this.comprobanteIdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Numero de Quincena
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [DataNames("_cfdnmn_qnc")]
        [SugarColumn(ColumnName = "_cfdnmn_qnc", IsNullable = true)]
        public int? Quincena {
            get {
                if (this.quincenaField != null)
                    return this.quincenaField;
                return this.quincenaField;
            }
            set {
                this.quincenaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Año de poliza
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_anio", IsNullable = true)]
        [DataNames("_cfdnmn_anio")]
        public int? Anio {
            get {
                if (this.anioField != null)
                    return this.anioField;
                return null;
            }
            set {
                this.anioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:NumDiasPagados: Atributo requerido para la expresión del número de días pagados
        /// Default:0.000
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_dspgds", IsNullable = true)]
        [DataNames("_cfdnmn_dspgds")]
        public decimal NumDiasPagados {
            get {
                return this.numDiasPagadosField;
            }
            set {
                this.numDiasPagadosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:SalarioBaseCotApor: Retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o pre
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_bsctap", IsNullable = true)]
        [DataNames("_cfdnmn_bsctap")]
        public decimal SalarioBaseCotApor {
            get {
                return this.salarioBaseCotAporField;
            }
            set {
                this.salarioBaseCotAporField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:SalarioDiarioIntegrado: El salario se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador p
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_drintg", IsNullable = true)]
        [DataNames("_cfdnmn_drintg")]
        public decimal SalarioDiarioIntegrado {
            get {
                return this.salarioDiarioIntegradoField;
            }
            set {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Percepciones TotalGravado: requerido para expresar el total de percepciones gravadas que se relacionan en el comprobante
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_pttlgrv", IsNullable = true)]
        [DataNames("_cfdnmn_pttlgrv")]
        public decimal PercepcionTotalGravado {
            get {
                return this.percepcionTotalGravadoField;
            }
            set {
                this.percepcionTotalGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Percepciones TotalExento: requerido para expresar el total de percepciones exentas que se relacionan en el comprobante
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_pttlexn", IsNullable = true)]
        [DataNames("_cfdnmn_pttlexn")]
        public decimal PercepcionTotalExento {
            get {
                return this.percepcionTotalExentoField;
            }
            set {
                this.percepcionTotalExentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Deducciones TotalGravado: requerido para expresar el total de deducciones gravadas que se relacionan en el comprobante
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_dttlgrv", IsNullable = true)]
        [DataNames("_cfdnmn_dttlgrv")]
        public decimal DeduccionTotalGravado {
            get {
                return this.deduccionTotalGravadoField;
            }
            set {
                this.deduccionTotalGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Deducciones TotalExento: requerido para expresar el total de deducciones exentas que se relacionan en el comprobante
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_dttlexn", IsNullable = true)]
        [DataNames("_cfdnmn_dttlexn")]
        public decimal DeduccionTotalExento {
            get {
                return this.deduccionTotalExentoField;
            }
            set {
                this.deduccionTotalExentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Descuento: Monto del descuento por la incapacidad
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_dscnt", IsNullable = true)]
        [DataNames("_cfdnmn_dscnt")]
        public decimal Descuento {
            get {
                return this.descuentoField;
            }
            set {
                this.descuentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ImportePagado: Importe pagado por las horas extra
        /// Default:0.00
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_hrsxtr", IsNullable = true)]
        [DataNames("_cfdnmn_hrsxtr")]
        public decimal ImportePagado {
            get {
                return this.importePagadoField;
            }
            set {
                this.importePagadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:TipoRegimen: requerido para la expresión de la clave del régimen por el cual se tiene contratado al trabajador, conforme al catálogo publicado en el portal del SAT en internet
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_tpreg", IsNullable = true)]
        [DataNames("_cfdnmn_tpreg")]
        public string TipoRegimen {
            get {
                return this.tipoRegimenField;
            }
            set {
                this.tipoRegimenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:RiesgoPuesto:Clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo a las actividades que desempeñan sus trabajadores, según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_rsgpst", IsNullable = true)]
        [DataNames("_cfdnmn_rsgpst")]
        public string RiesgoPuesto {
            get {
                return this.riesgoPuestoField;
            }
            set {
                this.riesgoPuestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Antigüedad: Número de semanas que el empleado ha mantenido relación laboral con el empleador
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_antgdd", IsNullable = true)]
        [DataNames("_cfdnmn_antgdd")]
        public string Antiguedad {
            get {
                return this.antiguedadField;
            }
            set {
                this.antiguedadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Atributo requerido para expresar la clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_clvent", IsNullable = true)]
        [DataNames("_cfdnmn_clvent")]
        public string EntidadFederativa {
            get {
                return this.entidadFederativaField;
            }
            set {
                this.entidadFederativaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Atributo opcional para indicar si el trabajador está asociado a un sindicato. Si se omite se asume que no está asociado a algún sindicato. (Si,No)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_sndct", IsNullable = true)]
        [DataNames("_cfdnmn_sndct")]
        public string Sindicalizado {
            get {
                return this.sindicalizadoField;
            }
            set {
                this.sindicalizadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:versión del documento
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_ver", IsNullable = true)]
        [DataNames("_cfdnmn_ver")]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Banco: opcional para la expresión del Banco conforme al catálogo, donde se realiza un depósito de nómina
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_banco", IsNullable = true)]
        [DataNames("_cfdnmn_banco")]
        public string Banco {
            get {
                return this.bancoField;
            }
            set {
                this.bancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Reg. Fed. De Contribuyentes (RFC)
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_rfc", IsNullable = true)]
        [DataNames("_cfdnmn_rfc")]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:RegistroPatronal: opcional para expresar el registro patronal a 20 posiciones máximo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_rgp", IsNullable = true)]
        [DataNames("_cfdnmn_rgp")]
        public string RegistroPatronal {
            get {
                return this.registroPatronalField;
            }
            set {
                this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Atributo condicional para la expresión de la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones o la CLABE a 18 posiciones o número de monedero electrónico,
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_ctabnc", IsNullable = true)]
        [DataNames("_cfdnmn_ctabnc")]
        public string CtaBanco {
            get {
                return this.ctaBancoField;
            }
            set {
                this.ctaBancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:NumEmpleado: requerido para expresar el número de empleado de 1 a 15 posiciones
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_numem", IsNullable = true)]
        [DataNames("_cfdnmn_numem")]
        public string NumEmpleado {
            get {
                return this.numEmpleadoField;
            }
            set {
                this.numEmpleadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CURP: requerido para la expresión de la CURP del trabajador
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_curp", IsNullable = true)]
        [DataNames("_cfdnmn_curp")]
        public string CURP {
            get {
                return this.curpField;
            }
            set {
                this.curpField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:NumSeguridadSocial: opcional para la expresión del número de seguridad social aplicable al trabajador
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_numsgr", IsNullable = true)]
        [DataNames("_cfdnmn_numsgr")]
        public string NumSeguridadSocial {
            get {
                return this.numSeguridadSocialField;
            }
            set {
                this.numSeguridadSocialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:CLABE: opcional para la expresión de la CLABE
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_clabe", IsNullable = true)]
        [DataNames("_cfdnmn_clabe")]
        public string CLABE {
            get {
                return this.clabeField;
            }
            set {
                this.clabeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:tipo de nomina
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_tipo", IsNullable = true)]
        [DataNames("_cfdnmn_tipo")]
        public string TipoNomina {
            get {
                return this.tipoField;
            }
            set {
                this.tipoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:folio fiscal
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_uuid", IsNullable = true)]
        [DataNames("_cfdnmn_uuid")]
        public string IdDocumento {
            get {
                return this.idDocumentoField;
            }
            set {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Departamento: opcional para la expresión del departamento o área a la que pertenece el trabajador
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_depto", IsNullable = true)]
        [DataNames("_cfdnmn_depto")]
        public string Departamento {
            get {
                return this.departamentoField;
            }
            set {
                this.departamentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Puesto: Puesto asignado al empleado o actividad que realiza
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_puesto", IsNullable = true)]
        [DataNames("_cfdnmn_puesto")]
        public string Puesto {
            get {
                return this.puestoField;
            }
            set {
                this.puestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:TipoContrato: Tipo de contrato que tiene el trabajador: Base, Eventual, Confianza, Sindicalizado, a prueba, etc.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_tpcntr", IsNullable = true)]
        [DataNames("_cfdnmn_tpcntr")]
        public string TipoContrato {
            get {
                return this.tipoContratoField;
            }
            set {
                this.tipoContratoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:TipoJornada: Tipo de jornada que cubre el trabajador: Diurna, nocturna, mixta, por hora, reducida, continuada, partida, por turnos, etc.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_tpjrn", IsNullable = true)]
        [DataNames("_cfdnmn_tpjrn")]
        public string TipoJornada {
            get {
                return this.tipoJornadaField;
            }
            set {
                this.tipoJornadaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:PeriodicidadPago: Forma en que se establece el pago del salario: diario, semanal, quincenal, catorcenal mensual, bimestral, unidad de obra, comisión, precio alzado, etc.
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_prddpg", IsNullable = true)]
        [DataNames("_cfdnmn_prddpg")]
        public string PeriodicidadPago {
            get {
                return this.periodicidadPagoField;
            }
            set {
                this.periodicidadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:FechaPago: requerido para la expresión de la fecha efectiva de erogación del gasto. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fchpgo", IsNullable = true)]
        [DataNames("_cfdnmn_fchpgo")]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate) {
                    return this.fechaPagoField;
                }
                return null;
            }
            set {
                this.fechaPagoField = value;
            }
        }

        /// <summary>
        /// Desc:FechaInicialPago: requerido para la expresión de la fecha inicial del pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fchini", IsNullable = true)]
        [DataNames("_cfdnmn_fchini")]
        public DateTime? FechaInicialPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicialPagoField >= firstGoodDate) {
                    return this.fechaInicialPagoField;
                }
                return null;
            }
            set {
                this.fechaInicialPagoField = value;
            }
        }

        /// <summary>
        /// Desc:FechaFinalPago: requerido para la expresión de la fecha final del pago. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fchfnl", IsNullable = true)]
        [DataNames("_cfdnmn_fchfnl")]
        public DateTime? FechaFinalPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaFinalPagoField >= firstGoodDate) {
                    return this.fechaFinalPagoField;
                }
                return null;
            }
            set {
                this.fechaFinalPagoField = value;
            }
        }

        /// <summary>
        /// Desc:FechaInicioRelLaboral: opcional para expresar la fecha de inicio de la relación laboral entre el empleador y el empleado
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fchrel", IsNullable = true)]
        [DataNames("_cfdnmn_fchrel")]
        public DateTime? FechaInicioRelLaboral {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaInicioRelLaboralField >= firstGoodDate) {
                    return this.fechaInicioRelLaboralField;
                }
                return null;
            }
            set {
                this.fechaInicioRelLaboralField = value;
            }
        }

        /// <summary>
        /// Desc:fec. sist.
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fn")]
        [DataNames("_cfdnmn_fn")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevoField;
            }
            set {
                this.fechaNuevoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de envio de correo de nomina
        /// Default:CURRENT_TIMESTAMP
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fenvi")]
        [DataNames("_cfdnmn_fenvi")]
        public DateTime? FecEnvio {
            get {
                return this.fechaEnvioField;
            }
            set {
                this.fechaEnvioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:fecha de modificacion
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_fm", IsNullable = true)]
        [DataNames("_cfdnmn_fm")]
        public DateTime? FecModificacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModificacionField >= firstGoodDate) {
                    return this.fechaModificacionField;
                }
                return null;
            }
            set {
                this.fechaModificacionField = value;
            }
        }

        /// <summary>
        /// Desc:usuario creo
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_usr_n", IsNullable = true)]
        [DataNames("_cfdnmn_usr_n")]
        public string Creo {
            get {
                return this.creoField;
            }
            set {
                this.creoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:ultimo usuario que modifico
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_cfdnmn_usr_m", IsNullable = true)]
        [DataNames("_cfdnmn_usr_m")]
        public string Modifica {
            get {
                return this.modificaField;
            }
            set {
                this.modificaField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
