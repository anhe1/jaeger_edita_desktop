﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar información de las incapacidades.
    /// </summary>
    [JsonObject("incapacidad")]
    public class ComplementoNominaIncapacidad : BasePropertyChangeImplementation {
        #region declaraciones
        private int diasIncapacidadField;
        private string tipoIncapacidadField; //c_TipoIncapacidad
        private decimal importeMonetarioField;
        #endregion

        public ComplementoNominaIncapacidad() { }

        /// <summary>
        /// obtener o establecer el número de días enteros que el trabajador se incapacitó en el periodo.
        /// </summary>
        [JsonProperty("diasIncapacidad")]
        public int DiasIncapacidad {
            get {
                return this.diasIncapacidadField;
            }
            set {
                this.diasIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la razón de la incapacidad. (catNomina:c_TipoIncapacidad)
        /// </summary>
        [JsonProperty("tipoIncapacidad")]
        public string TipoIncapacidad {
            get {
                return this.tipoIncapacidadField;
            }
            set {
                this.tipoIncapacidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto del importe monetario de la incapacidad.
        /// </summary>
        [JsonProperty("importeMonetario")]
        public decimal ImporteMonetario {
            get {
                return this.importeMonetarioField;
            }
            set {
                this.importeMonetarioField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
