﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar la información referente a la compensación de saldos a favor de un trabajador.
    /// </summary>
    [JsonObject("OtroPagoCompensacionSaldosAFavor")]
    public class ComplementoNominaOtroPagoCompensacionSaldosAFavor : BasePropertyChangeImplementation {
        #region declaraciones
        private decimal saldoAFavorField;
        private short anioField;
        private decimal remanenteSalFavField;
        #endregion

        public ComplementoNominaOtroPagoCompensacionSaldosAFavor() { }

        /// <summary>
        /// obtener o establecer el saldo a favor determinado por el patrón al trabajador en periodos o ejercicios anteriores.
        /// </summary>
        [JsonProperty("saldoAFavor")]
        public decimal SaldoAFavor {
            get {
                return this.saldoAFavorField;
            }
            set {
                this.saldoAFavorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer expresar el año en que se determinó el saldo a favor del trabajador por el patrón que se incluye en el campo “RemanenteSalFav”.
        /// </summary>
        [JsonProperty("anio")]
        public short Anio {
            get {
                return this.anioField;
            }
            set {
                this.anioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el remanente del saldo a favor del trabajador
        /// </summary>
        [JsonProperty("remanenteSalFav")]
        public decimal RemanenteSalFav {
            get {
                return this.remanenteSalFavField;
            }
            set {
                this.remanenteSalFavField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
