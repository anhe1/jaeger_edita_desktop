﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar la información detallada de pagos por jubilación, pensiones o haberes de retiro.
    /// </summary>
    [JsonObject("JubilacionPensionRetiro")]
    public partial class ComplementoNominaPercepcionesJubilacionPensionRetiro : BasePropertyChangeImplementation {
        #region declaraciones
        private decimal totalUnaExhibicionField;
        private decimal totalParcialidadField;
        private decimal montoDiarioField;
        private decimal ingresoAcumulableField;
        private decimal ingresoNoAcumulableField;
        #endregion

        public ComplementoNominaPercepcionesJubilacionPensionRetiro() { }

        /// <summary>
        /// obtener o establecer el monto total del pago cuando se realiza en una sola exhibición.
        /// </summary>
        [JsonProperty("totalUnaExhibicion")]
        public decimal TotalUnaExhibicion {
            get {
                return this.totalUnaExhibicionField;
            }
            set {
                this.totalUnaExhibicionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos totales por pago cuando se hace en parcialidades.
        /// </summary>
        [JsonProperty("totalParcialidad")]
        public decimal TotalParcialidad {
            get {
                return this.totalParcialidadField;
            }
            set {
                this.totalParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el monto diario percibido por jubilación, pensiones o haberes de retiro cuando se realiza en parcialidades.
        /// </summary>
        [JsonProperty("montoDiario")]
        public decimal MontoDiario {
            get {
                return this.montoDiarioField;
            }
            set {
                this.montoDiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos acumulables.
        /// </summary>
        [JsonProperty("ingresoAcumulable")]
        public decimal IngresoAcumulable {
            get {
                return this.ingresoAcumulableField;
            }
            set {
                this.ingresoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer los ingresos no acumulables
        /// </summary>
        [JsonProperty("ingresoNoAcumulable")]
        public decimal IngresoNoAcumulable {
            get {
                return this.ingresoNoAcumulableField;
            }
            set {
                this.ingresoNoAcumulableField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
