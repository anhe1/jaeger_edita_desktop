﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo requerido para expresar la información referente al subsidio al empleo del trabajador.
    /// </summary>
    [JsonObject("OtroPagoSubsidioAlEmpleo")]
    public partial class ComplementoNominaOtroPagoSubsidioAlEmpleo : BasePropertyChangeImplementation {
        #region declaraciones
        private decimal subsidioCausadoField;
        #endregion

        /// <summary>
        /// obtener o establecer el subsidio causado conforme a la tabla del subsidio para el empleo publicada en el Anexo 8 de la RMF vigente.
        /// </summary>
        [JsonProperty("subsidioCausado")]
        public decimal SubsidioCausado {
            get {
                return this.subsidioCausadoField;
            }
            set {
                this.subsidioCausadoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
