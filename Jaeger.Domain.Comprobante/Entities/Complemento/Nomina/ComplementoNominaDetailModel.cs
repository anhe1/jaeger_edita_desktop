﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [SugarTable("_cfdnmn", "Nomina: complemento nominas")]
    public class ComplementoNominaDetailModel : ComplementoNominaModel, IComplementoNominaDetailModel, IComplementoNominaModel {
        #region declaraciones
        private BindingList<ComplementoNominaParteDetailModel> _Partes;
        #endregion

        public ComplementoNominaDetailModel() : base() {
            this._Partes = new BindingList<ComplementoNominaParteDetailModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public BindingList<ComplementoNominaParteDetailModel> Partes {
            get { return this._Partes; }
            set { this._Partes = value; }
        }
    }
}
