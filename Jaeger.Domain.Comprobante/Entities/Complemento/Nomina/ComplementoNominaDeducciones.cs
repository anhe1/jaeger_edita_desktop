﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo opcional para expresar las deducciones aplicables.
    /// </summary>
    [JsonObject("deducciones")]
    public partial class ComplementoNominaDeducciones : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<ComplementoNominaDeduccion> deduccionField;
        private decimal totalOtrasDeduccionesField;
        private decimal totalImpuestosRetenidosField;
        private decimal totalGravadoField; // campo para la version 1.1
        private decimal totalExentoField; // campo para la version 1.1
        #endregion

        public ComplementoNominaDeducciones() {
            this.Deduccion = new BindingList<ComplementoNominaDeduccion>();
        }


        [JsonProperty("deduccion")]
        public BindingList<ComplementoNominaDeduccion> Deduccion {
            get {
                return this.deduccionField;
            }
            set {
                this.deduccionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de deducciones que se relacionan en el comprobante, donde la clave de tipo de deducción sea distinta a la 002 correspondiente a ISR
        /// </summary>
        [JsonProperty("totalOtrasDeducciones")]
        public decimal TotalOtrasDeducciones {
            get {
                return this.totalOtrasDeduccionesField;
            }
            set {
                this.totalOtrasDeduccionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el total de los impuestos federales retenidos, es decir, donde la clave de tipo de deducción sea 002 correspondiente a ISR.
        /// </summary>
        [JsonProperty("totalImpuestosRetenidos")]
        public decimal TotalImpuestosRetenidos {
            get {
                return this.totalImpuestosRetenidosField;
            }
            set {
                this.totalImpuestosRetenidosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// campo para la version 1.1
        /// </summary>
        [JsonProperty("totalGravado")]
        public decimal TotalGravado {
            get {
                return this.totalGravadoField;
            }
            set {
                this.totalGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// campo para la version 1.1
        /// </summary>
        [JsonProperty("totalExento")]
        public decimal TotalExento {
            get {
                return this.totalExentoField;
            }
            set {
                this.totalExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
