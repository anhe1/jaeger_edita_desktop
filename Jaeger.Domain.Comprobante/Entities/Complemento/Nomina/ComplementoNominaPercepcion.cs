﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// >Nodo requerido para expresar la información detallada de una percepción
    /// </summary>
    [JsonObject("percepcion")]
    public partial class ComplementoNominaPercepcion : BasePropertyChangeImplementation {
        #region declaraciones
        private ComplementoNominaPercepcionAccionesOTitulos accionesOTitulosField;
        private BindingList<ComplementoNominaPercepcionHorasExtra> horasExtraField;
        private string tipoPercepcionField; //c_TipoPercepcion
        private string claveField;
        private string conceptoField;
        private decimal importeGravadoField;
        private decimal importeExentoField;
        #endregion

        public ComplementoNominaPercepcion() { }

        /// <summary>
        /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
        /// </summary>
        [JsonProperty("accionesOTitulos", NullValueHandling = NullValueHandling.Ignore)]
        public ComplementoNominaPercepcionAccionesOTitulos AccionesOTitulos {
            get {
                return this.accionesOTitulosField;
            }
            set {
                this.accionesOTitulosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar ingresos por acciones o títulos valor que representan bienes. Se vuelve requerido cuando existan ingresos por sueldos derivados de adquisición de acciones o títulos (Art. 94, fracción VII LISR).
        /// </summary>
        [JsonProperty("horasExtra", NullValueHandling = NullValueHandling.Ignore)]
        public BindingList<ComplementoNominaPercepcionHorasExtra> HorasExtra {
            get {
                return this.horasExtraField;
            }
            set {
                this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer Clave agrupadora bajo la cual se clasifica la percepción. (catNomina:c_TipoPercepcion)
        /// </summary>
        [JsonProperty("tipoPercepcion")]
        public string TipoPercepcion {
            get {
                return this.tipoPercepcionField;
            }
            set {
                this.tipoPercepcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de percepción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de percepción.
        /// pattern value="[^|]{1,100}"
        /// </summary>
        [JsonProperty("concepto")]
        public string Concepto {
            get {
                return this.conceptoField;
            }
            set {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe gravado de un concepto de percepción.
        /// </summary>
        [JsonProperty("importeGravado")]
        public decimal ImporteGravado {
            get {
                return this.importeGravadoField;
            }
            set {
                this.importeGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe exento de un concepto de percepción.
        /// </summary>
        [JsonProperty("importeExento")]
        public decimal ImporteExento {
            get {
                return this.importeExentoField;
            }
            set {
                this.importeExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
