﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo opcional para expresar las deducciones aplicables.
    /// </summary>
    [JsonObject("deduccion")]
    public partial class ComplementoNominaDeduccion : BasePropertyChangeImplementation {
        #region declaraciones
        private string tipoDeduccionField; //c_TipoDeduccion
        private string claveField;
        private string conceptoField;
        private decimal importeField;
        #endregion

        public ComplementoNominaDeduccion() { }

        /// <summary>
        /// obtener o establecer la clave agrupadora que clasifica la deducción.
        /// </summary>
        [JsonProperty("tipoDeduccion")]
        public string TipoDeduccion {
            get {
                return this.tipoDeduccionField;
            }
            set {
                this.tipoDeduccionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de deducción de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de deducción.
        /// pattern value="[^|]{1,100}"
        /// </summary>
        [JsonProperty("concepto")]
        public string Concepto {
            get {
                return this.conceptoField;
            }
            set {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del concepto de deducción.
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
