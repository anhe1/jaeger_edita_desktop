﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar otros pagos aplicables.
    /// </summary>
    [JsonObject("otroPago")]
    public partial class ComplementoNominaOtroPago : BasePropertyChangeImplementation {
        #region declaraciones
        private ComplementoNominaOtroPagoSubsidioAlEmpleo subsidioAlEmpleoField;
        private ComplementoNominaOtroPagoCompensacionSaldosAFavor compensacionSaldosAFavorField;
        private string tipoOtroPagoField; //c_TipoOtroPago
        private string claveField;
        private string conceptoField;
        private decimal importeField;
        #endregion

        public ComplementoNominaOtroPago() { }

        /// <summary>
        /// Nodo requerido para expresar la información referente al subsidio al empleo del trabajador.
        /// </summary>
        [JsonProperty("subsidioAlEmpleo")]
        public ComplementoNominaOtroPagoSubsidioAlEmpleo SubsidioAlEmpleo {
            get {
                return this.subsidioAlEmpleoField;
            }
            set {
                this.subsidioAlEmpleoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la información referente a la compensación de saldos a favor de un trabajador.
        /// </summary>
        [JsonProperty("compensacionSaldosAFavor")]
        public ComplementoNominaOtroPagoCompensacionSaldosAFavor CompensacionSaldosAFavor {
            get {
                return this.compensacionSaldosAFavorField;
            }
            set {
                this.compensacionSaldosAFavorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave agrupadora bajo la cual se clasifica el otro pago. (catNomina:c_TipoOtroPago)
        /// </summary>
        [JsonProperty("tipoOtroPago")]
        public string TipoOtroPago {
            get {
                return this.tipoOtroPagoField;
            }
            set {
                this.tipoOtroPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de otro pago de nómina propia de la contabilidad de cada patrón, puede conformarse desde 3 hasta 15 caracteres.
        /// pattern value="[^|]{3,15}"
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la descripción del concepto de otro pago.
        /// </summary>
        [JsonProperty("concepto")]
        public string Concepto {
            get {
                return this.conceptoField;
            }
            set {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del concepto de otro pago.
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
