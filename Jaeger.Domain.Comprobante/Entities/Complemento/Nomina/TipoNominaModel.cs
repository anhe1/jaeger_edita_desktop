﻿namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    public class TipoNominaModel : Base.Abstractions.BaseSingleTipoModel {
        public TipoNominaModel() { }

        public TipoNominaModel(string id, string descripcion) : base(id, descripcion) { }
    }
}
