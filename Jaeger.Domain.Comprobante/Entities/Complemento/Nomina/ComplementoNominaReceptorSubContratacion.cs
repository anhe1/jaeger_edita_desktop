﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("subContratacion")]
    public class ComplementoNominaReceptorSubContratacion : BasePropertyChangeImplementation {
        #region declaraciones
        private string rfcLaboraField;
        private decimal porcentajeTiempoField;
        #endregion

        public ComplementoNominaReceptorSubContratacion() : base() { }

        /// <summary>
        /// Atributo requerido para expresar el RFC de la persona que subcontrata.
        /// </summary>
        [JsonProperty("rfcLabora")]
        public string RFCLabora {
            get {
                return this.rfcLaboraField;
            }
            set {
                this.rfcLaboraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el porcentaje del tiempo que prestó sus servicios con el RFC que lo subcontrata.
        /// </summary>
        [JsonProperty("porcentajeTiempo")]
        public decimal PorcentajeTiempo {
            get {
                return this.porcentajeTiempoField;
            }
            set {
                this.porcentajeTiempoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
