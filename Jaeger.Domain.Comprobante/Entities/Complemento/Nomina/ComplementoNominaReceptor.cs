﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("receptor")]
    public class ComplementoNominaReceptor : BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private BindingList<ComplementoNominaReceptorSubContratacion> subContratacionField;
        private string rfcField;
        private string curpField;
        private string numSeguridadSocialField;
        private DateTime? fechaInicioRelLaboralField;
        private string antiguedadField;
        private string tipoContratoField; //c_TipoContrato
        private bool sindicalizadoField; // NominaReceptorSindicalizado
        private string tipoJornadaField; //c_TipoJornada
        private string tipoRegimenField; //c_TipoRegimen
        private string numEmpleadoField;
        private string departamentoField;
        private string puestoField;
        private string riesgoPuestoField; //c_RiesgoPuesto
        private string periodicidadPagoField; //c_PeriodicidadPago
        private string bancoField; //c_Banco
        private string cuentaBancariaField;
        private decimal salarioBaseCotAporField;
        private decimal salarioDiarioIntegradoField;
        private string claveEntFedField; //c_Estado
        #endregion

        [JsonProperty("id")]
        public int IdEmpleado {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la lista de las personas que los subcontrataron.
        /// </summary>
        [JsonProperty("subContratacion", NullValueHandling = NullValueHandling.Ignore)]
        public BindingList<ComplementoNominaReceptorSubContratacion> SubContratacion {
            get {
                return this.subContratacionField;
            }
            set {
                this.subContratacionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la CURP del receptor del comprobante de nómina.
        /// </summary>
        [JsonProperty("curp")]
        public string CURP {
            get {
                return this.curpField;
            }
            set {
                this.curpField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de seguridad social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("numSeguridadSocial")]
        public string NumSeguridadSocial {
            get {
                return this.numSeguridadSocialField;
            }
            set {
                this.numSeguridadSocialField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la fecha de inicio de la relación laboral entre el empleador y el empleado. Se expresa en la forma aaaa-mm-dd, de acuerdo con la especificación ISO 8601. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("fechaInicioRelLaboral")]
        public DateTime? FechaInicioRelLaboral {
            get {
                return this.fechaInicioRelLaboralField;
            }
            set {
                this.fechaInicioRelLaboralField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de semanas o el periodo de años, meses y días que el empleado ha mantenido relación laboral con el empleador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("antiguedad")]
        public string Antiguedad {
            get {
                return this.antiguedadField;
            }
            set {
                this.antiguedadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el tipo de contrato que tiene el trabajador.
        /// </summary>
        [JsonProperty("tipoContrato")]
        public string TipoContrato {
            get {
                return this.tipoContratoField;
            }
            set {
                this.tipoContratoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para indicar si el trabajador está asociado a un sindicato. Si se omite se asume que no está asociado a algún sindicato.
        /// </summary>
        [JsonProperty("sindicalizado", NullValueHandling = NullValueHandling.Ignore)]
        public bool Sindicalizado {
            get {
                return this.sindicalizadoField;
            }
            set {
                this.sindicalizadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el tipo de jornada que cubre el trabajador. Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("tipoJornada")]
        public string TipoJornada {
            get {
                return this.tipoJornadaField;
            }
            set {
                this.tipoJornadaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión de la clave del régimen por el cual se tiene contratado al trabajador.
        /// </summary>
        [JsonProperty("tipoRegimen")]
        public string TipoRegimen {
            get {
                return this.tipoRegimenField;
            }
            set {
                this.tipoRegimenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el número de empleado de 1 a 15 posiciones.
        /// </summary>
        [JsonProperty("numEmpleado")]
        public string NumEmpleado {
            get {
                return this.numEmpleadoField;
            }
            set {
                this.numEmpleadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para la expresión del departamento o área a la que pertenece el trabajador.
        /// </summary>
        [JsonProperty("depto")]
        public string Departamento {
            get {
                return this.departamentoField;
            }
            set {
                this.departamentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para la expresión del puesto asignado al empleado o actividad que realiza.
        /// </summary>
        [JsonProperty("puesto")]
        public string Puesto {
            get {
                return this.puestoField;
            }
            set {
                this.puestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la clave conforme a la Clase en que deben inscribirse los patrones, de acuerdo con las actividades que desempeñan sus trabajadores, según lo previsto en el artículo 196 del Reglamento en Materia de Afiliación Clasificación de Empresas, Recaudación y Fiscalización, o conforme con la normatividad del Instituto de Seguridad Social del trabajador. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("riesgoPuesto")]
        public string RiesgoPuesto {
            get {
                return this.riesgoPuestoField;
            }
            set {
                this.riesgoPuestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la forma en que se establece el pago del salario.
        /// </summary>
        [JsonProperty("periodicidadPago")]
        public string PeriodicidadPago {
            get {
                return this.periodicidadPagoField;
            }
            set {
                this.periodicidadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para la expresión de la clave del Banco conforme al catálogo, donde se realiza el depósito de nómina.
        /// </summary>
        [JsonProperty("banco")]
        public string Banco {
            get {
                return this.bancoField;
            }
            set {
                this.bancoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para la expresión de la cuenta bancaria a 11 posiciones o número de teléfono celular a 10 posiciones o número de tarjeta de crédito, débito o servicios a 15 ó 16 posiciones o la CLABE a 18 posiciones o número de monedero electrónico, donde se realiza el depósito de nómina.
        /// </summary>
        [JsonProperty("cuentaBancaria")]
        public string CuentaBancaria {
            get {
                return this.cuentaBancariaField;
            }
            set {
                this.cuentaBancariaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la retribución otorgada al trabajador, que se integra por los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo, sin considerar los conceptos que se excluyen de conformidad con el Artículo 27 de la Ley del Seguro Social, o la integración de los pagos conforme la normatividad del Instituto de Seguridad Social del trabajador. (Se emplea para pagar las cuotas y aportaciones de Seguridad Social). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("salarioBaseCotApor")]
        public decimal SalarioBaseCotApor {
            get {
                return this.salarioBaseCotAporField;
            }
            set {
                this.salarioBaseCotAporField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el salario que se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquier otra cantidad o prestación que se entregue al trabajador por su trabajo, de conformidad con el Art. 84 de la Ley Federal del Trabajo. (Se utiliza para el cálculo de las indemnizaciones). Se debe ingresar cuando se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("salarioDiarioIntegrado")]
        public decimal SalarioDiarioIntegrado {
            get {
                return this.salarioDiarioIntegradoField;
            }
            set {
                this.salarioDiarioIntegradoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave de la entidad federativa en donde el receptor del recibo prestó el servicio.
        /// </summary>
        [JsonProperty("claveEntFed")]
        public string ClaveEntFed {
            get {
                return this.claveEntFedField;
            }
            set {
                this.claveEntFedField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
