﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("percepciones")]
    public partial class ComplementoNominaPercepciones : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<ComplementoNominaPercepcion> percepcionField;
        private ComplementoNominaPercepcionesJubilacionPensionRetiro jubilacionPensionRetiroField;
        private ComplementoNominaPercepcionesSeparacionIndemnizacion separacionIndemnizacionField;
        private decimal totalSueldosField;
        private decimal totalSeparacionIndemnizacionField;
        private decimal totalJubilacionPensionRetiroField;
        private decimal totalGravadoField;
        private decimal totalExentoField;
        #endregion

        public ComplementoNominaPercepciones() {
            this.Percepcion = new BindingList<ComplementoNominaPercepcion>();
        }

        /// <summary>
        /// Nodo requerido para expresar la información detallada de una percepción
        /// </summary>
        [JsonProperty("percepcion")]
        public BindingList<ComplementoNominaPercepcion> Percepcion {
            get {
                return this.percepcionField;
            }
            set {
                this.percepcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la información detallada de pagos por jubilación, pensiones o haberes de retiro.
        /// </summary>
        [JsonProperty("jubilacionPensionRetiro")]
        public ComplementoNominaPercepcionesJubilacionPensionRetiro JubilacionPensionRetiro {
            get {
                return this.jubilacionPensionRetiroField;
            }
            set {
                this.jubilacionPensionRetiroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la información detallada de otros pagos por separación.
        /// </summary>
        [JsonProperty("separacionIndemnizacion")]
        public ComplementoNominaPercepcionesSeparacionIndemnizacion SeparacionIndemnizacion {
            get {
                return this.separacionIndemnizacionField;
            }
            set {
                this.separacionIndemnizacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el total de percepciones brutas (gravadas y exentas) por sueldos y salarios y conceptos asimilados a salarios.
        /// </summary>
        [JsonProperty("totalSueldos")]
        public decimal TotalSueldos {
            get {
                return this.totalSueldosField;
            }
            set {
                this.totalSueldosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el importe exento y gravado de las claves tipo percepción 022 Prima por Antigüedad, 023 Pagos por separación y 025 Indemnizaciones.
        /// </summary>
        [JsonProperty("totalSeparacionIndemnizacion")]
        public decimal TotalSeparacionIndemnizacion {
            get {
                return this.totalSeparacionIndemnizacionField;
            }
            set {
                this.totalSeparacionIndemnizacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el importe exento y gravado de las claves tipo percepción 039 Jubilaciones, pensiones o haberes de retiro en una exhibición y 044 Jubilaciones, pensiones o haberes de retiro en parcialidades.
        /// </summary>
        [JsonProperty("totalJubilacionPensionRetiro")]
        public decimal TotalJubilacionPensionRetiro {
            get {
                return this.totalJubilacionPensionRetiroField;
            }
            set {
                this.totalJubilacionPensionRetiroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el total de percepciones gravadas que se relacionan en el comprobante.
        /// </summary>
        [JsonProperty("totalGravado")]
        public decimal TotalGravado {
            get {
                return this.totalGravadoField;
            }
            set {
                this.totalGravadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el total de percepciones exentas que se relacionan en el comprobante.
        /// </summary>
        [JsonProperty("totalExento")]
        public decimal TotalExento {
            get {
                return this.totalExentoField;
            }
            set {
                this.totalExentoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
