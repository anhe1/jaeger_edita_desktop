﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("entidadSncf")]
    public class ComplementoNominaEmisorEntidadSncf : BasePropertyChangeImplementation {
        #region declaraciones
        private string origenRecursoField; //c_OrigenRecurso
        private decimal montoRecursoPropioField;
        #endregion

        public ComplementoNominaEmisorEntidadSncf() { }

        /// <summary>
        /// Atributo requerido para identificar el origen del recurso utilizado para el pago de nómina del personal que presta o desempeña un servicio personal subordinado o asimilado a salarios en las dependencias.
        /// </summary>
        [JsonProperty("origenRecurso")]
        public string OrigenRecurso {
            get {
                return this.origenRecursoField;
            }
            set {
                this.origenRecursoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el monto del recurso pagado con cargo a sus participaciones u otros ingresos locales (importe bruto de los ingresos propios, es decir total de gravados y exentos), cuando el origen es mixto.
        /// </summary>
        [JsonProperty("montoRecursoPropio")]
        public decimal MontoRecursoPropio {
            get {
                return this.montoRecursoPropioField;
            }
            set {
                this.montoRecursoPropioField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
