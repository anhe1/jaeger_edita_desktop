﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    /// <summary>
    /// Nodo condicional para expresar las horas extra aplicables.
    /// </summary>
    [JsonObject("HorasExtra")]
    public partial class ComplementoNominaPercepcionHorasExtra : BasePropertyChangeImplementation {
        private int diasField;
        private string tipoHorasField; //c_TipoHoras
        private int horasExtraField;
        private decimal importePagadoField;

        public ComplementoNominaPercepcionHorasExtra() {

        }

        /// <summary>
        /// obtener o establecer el número de días en que el trabajador realizó horas extra en el periodo.
        /// </summary>
        [JsonProperty("dias")]
        public int Dias {
            get {
                return this.diasField;
            }
            set {
                this.diasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de pago de las horas extra.
        /// </summary>
        [JsonProperty("tipoHoras")]
        public string TipoHoras {
            get {
                return this.tipoHorasField;
            }
            set {
                this.tipoHorasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de horas extra trabajadas en el periodo.
        /// </summary>
        [JsonProperty("horasExtra")]
        public int HorasExtra {
            get {
                return this.horasExtraField;
            }
            set {
                this.horasExtraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe pagado por las horas extra.
        /// </summary>
        [JsonProperty("importePagado")]
        public decimal ImportePagado {
            get {
                return this.importePagadoField;
            }
            set {
                this.importePagadoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
