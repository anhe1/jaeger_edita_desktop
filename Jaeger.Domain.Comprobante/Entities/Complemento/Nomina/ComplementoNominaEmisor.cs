﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Nomina {
    [JsonObject("emisor")]
    public class ComplementoNominaEmisor : BasePropertyChangeImplementation {
        #region declaraciones
        private ComplementoNominaEmisorEntidadSncf entidadSncfField;
        private string rfcField;
        private string curpField;
        private string registroPatronalField;
        private string rfcPatronOrigenField;
        #endregion

        public ComplementoNominaEmisor() { }

        /// <summary>
        /// Nodo condicional para que las entidades adheridas al Sistema Nacional de Coordinación Fiscal realicen la identificación del origen de los recursos utilizados en el pago de nómina del personal que presta o desempeña un servicio personal subordinado en las dependencias de la entidad federativa, del municipio o demarcación territorial de la Ciudad de México, así como en sus respectivos organismos autónomos y entidades paraestatales y paramunicipales
        /// </summary>
        [JsonProperty("EntidadSncf")]
        public ComplementoNominaEmisorEntidadSncf EntidadSncf {
            get {
                return this.entidadSncfField;
            }
            set {
                this.entidadSncfField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rfcField;
            }
            set {
                this.rfcField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la CURP del emisor del comprobante de nómina cuando es una persona física.
        /// </summary>
        [JsonProperty("curp")]
        public string CURP {
            get {
                return this.curpField;
            }
            set {
                this.curpField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el registro patronal, clave de ramo - pagaduría o la que le asigne la institución de seguridad social al patrón, a 20 posiciones máximo. Se debe ingresar cuando se cuente con él, o se esté obligado conforme a otras disposiciones distintas a las fiscales.
        /// </summary>
        [JsonProperty("registroPatronal")]
        public string RegistroPatronal {
            get {
                return this.registroPatronalField;
            }
            set {
                this.registroPatronalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el RFC de la persona que fungió como patrón cuando el pago al trabajador se realice a través de un tercero como vehículo o herramienta de pago.
        /// </summary>
        [JsonProperty("rfcPatronOrigen")]
        public string RFCPatronOrigen {
            get {
                return this.rfcPatronOrigenField;
            }
            set {
                this.rfcPatronOrigenField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
