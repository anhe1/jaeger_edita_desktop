﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    public class ComplementoTimbreFiscal : BasePropertyChangeImplementation, IComplementoTimbreFiscal {
        #region declaraciones
        private string versionField;
        private string uuidField;
        private DateTime? fechaTimbradoField = null;
        private string selloCfdField;
        private string noCertificadoSatField;
        private string selloSatField;
        private string leyendaField;
        private string rfcProvCertifField;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ComplementoTimbreFiscal() {
            this.fechaTimbradoField = null;
        }

        #region propiedades

        /// <summary>
        /// versión del estándar del Timbre Fiscal Digital
        /// </summary>
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene o establece los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        public string UUID {
            get {
                return this.uuidField;
            }
            set {
                this.uuidField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha y hora, de la generación del timbre por la certificación digital del SAT. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora de la Zona Centro del Sistema de Horario en México.
        /// </summary>
        public DateTime? FechaTimbrado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbradoField >= firstGoodDate)
                    return this.fechaTimbradoField.Value;
                return null;
            }
            set {
                this.fechaTimbradoField = value;
                this.OnPropertyChanged();
            }
        }

        public string SelloCFD {
            get {
                return this.selloCfdField;
            }
            set {
                this.selloCfdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// número de serie del certificado del SAT usado para generar el sello digital del Timbre Fiscal Digital.
        /// </summary>
        public string NoCertificadoSAT {
            get {
                return this.noCertificadoSatField;
            }
            set {
                this.noCertificadoSatField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene o establace sello digital del Timbre Fiscal Digital, al que hacen referencia las reglas de la Resolución Miscelánea vigente. El sello debe ser expresado como una cadena de texto en formato Base 64.
        /// </summary>
        public string SelloSAT {
            get {
                return this.selloSatField;
            }
            set {
                this.selloSatField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// opcional para registrar información que el SAT comunique a los usuarios del CFDI.
        /// </summary>
        public string Leyenda {
            get {
                return this.leyendaField;
            }
            set {
                this.leyendaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        public string RFCProvCertif {
            get {
                return this.rfcProvCertifField;
            }
            set {
                this.rfcProvCertifField = value;
                this.OnPropertyChanged();
            }
        }

        public object Tag {
            get; set;
        }
        #endregion
    }
}
