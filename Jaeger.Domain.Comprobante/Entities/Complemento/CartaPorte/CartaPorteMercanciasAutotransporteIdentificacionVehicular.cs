﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar los datos de identificación del autotransporte en el que se trasladan los bienes y/o mercancías.
    /// </summary>
    [JsonObject("mercanciasAutotransporteIdentificacionVehicular")]
    public class CartaPorteMercanciasAutotransporteIdentificacionVehicular : BasePropertyChangeImplementation {
        #region declaraciones
        private string configVehicularField; //c_ConfigAutotransporte
        private string placaVMField;
        private int anioModeloVMField;
        #endregion

        public CartaPorteMercanciasAutotransporteIdentificacionVehicular() {
            this.anioModeloVMField = System.DateTime.Now.Year;
        }

        /// <summary>
        /// Atributo requerido para expresar la clave de nomenclatura del autotransporte que es utilizado para transportar los bienes y/o mercancías.
        /// </summary>
        [JsonProperty]
        public string ConfigVehicular {
            get {
                return this.configVehicularField;
            }
            set {
                this.configVehicularField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar solo los caracteres alfanuméricos, sin guiones ni espacios de la placa vehicular del autotransporte que es utilizado para transportar los bienes y/o mercancías.
        /// <xs:pattern value="[^(?!.*\s)-]{5,7}"/>
        /// </summary>
        [JsonProperty]
        public string PlacaVM {
            get {
                return this.placaVMField;
            }
            set {
                this.placaVMField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el año del autotransporte que es utilizado para transportar los bienes y/o mercancías.
        /// <xs:pattern value="(19[0-9]{2}|20[0-9]{2})"/>
        /// </summary>
        [JsonProperty]
        public int AnioModeloVM {
            get {
                return this.anioModeloVMField;
            }
            set {
                this.anioModeloVMField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Configuración: {0} Placa: {1} Modelo: {2}", this.ConfigVehicular, this.PlacaVM, this.AnioModeloVM);
        }
    }
}