﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo opcional para registrar información del domicilio del(los) tipo(s) de figura transporte que intervenga(n) en el traslado de los bienes y/o mercancías.
    /// </summary>
    [JsonObject("tiposFiguraDomicilio")]
    public class CartaPorteTiposFiguraDomicilio : Abstractions.CartaPorteDomicilio {
        public CartaPorteTiposFiguraDomicilio() : base() { }
    }
}