﻿using Jaeger.Domain.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Atributo requerido para registrar información de la parte del transporte de la cual el emisor del comprobante es distinto al dueño de la misma, por ejemplo: vehículos, máquinas, contenedores, plataformas, etc; que se utilicen para el traslado de los bienes y/o mercancías.
    /// </summary>
    public class CartaPorteTiposFiguraPartesTransporte : BasePropertyChangeImplementation {
        private string parteTransporteField; //c_ParteTransporte

        public CartaPorteTiposFiguraPartesTransporte() { }

        public string ParteTransporte {
            get {
                return this.parteTransporteField;
            }
            set {
                this.parteTransporteField = value;
                this.OnPropertyChanged();
            }
        }
    }
}