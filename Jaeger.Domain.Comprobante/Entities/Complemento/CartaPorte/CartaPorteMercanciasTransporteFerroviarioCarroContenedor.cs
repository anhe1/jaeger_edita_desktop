﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para especificar el tipo de contenedor o vagón en el que se trasladan los bienes y/o mercancías por vía férrea.
    /// </summary>
    [JsonObject("mercanciasTransporteFerroviarioCarroContenedor")]
    public partial class CartaPorteMercanciasTransporteFerroviarioCarroContenedor : BasePropertyChangeImplementation {
        #region declaraciones
        private string tipoContenedorField; // c_Contenedor
        private decimal pesoContenedorVacioField;
        private decimal pesoNetoMercanciaField;
        #endregion

        /// <summary>
        /// Atributo requerido para registrar la clave con la que se identifica al tipo de contenedor o el vagón en el que se realiza el traslado de los bienes y/o mercancías.
        /// </summary>
        public string TipoContenedor {
            get {
                return this.tipoContenedorField;
            }
            set {
                this.tipoContenedorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar en kilogramos, el peso del contenedor vacío en el que se trasladan los bienes y/o mercancías.
        /// </summary>
        public decimal PesoContenedorVacio {
            get {
                return this.pesoContenedorVacioField;
            }
            set {
                this.pesoContenedorVacioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar en kilogramos el peso neto de los bienes y/o mercancías que son trasladados en el contenedor.
        /// </summary>
        public decimal PesoNetoMercancia {
            get {
                return this.pesoNetoMercanciaField;
            }
            set {
                this.pesoNetoMercanciaField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Tipo contenedor: {0} Peso: {1} Peso Neto: {2}", this.TipoContenedor, this.PesoContenedorVacio.ToString("N2"), this.PesoNetoMercancia.ToString("N2"));
        }
    }
}