﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar detalladamente la información de los bienes y/o mercancías que se trasladan en los distintos medios de transporte
    /// </summary>
    [JsonObject("mercanciasMercancia")]
    public partial class CartaPorteMercanciasMercancia : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<string> pedimentosField;
        private BindingList<CartaPorteMercanciasMercanciaGuiasIdentificacion> guiasIdentificacionField;
        private BindingList<CartaPorteMercanciasMercanciaCantidadTransporta> cantidadTransportaField;
        private CartaPorteMercanciasMercanciaDetalleMercancia detalleMercanciaField;
        private string bienesTranspField; //c_ClaveProdServCP
        private string claveSTCCField;
        private string descripcionField;
        private decimal cantidadField;
        private string claveUnidadField; //c_ClaveUnidad
        private string unidadField;
        private string dimensionesField;
        private bool materialPeligrosoField; //CartaPorteMercanciasMercanciaMaterialPeligroso
        private string cveMaterialPeligrosoField; //c_MaterialPeligroso
        private string embalajeField; //c_TipoEmbalaje
        private string descripEmbalajeField;
        private decimal pesoEnKgField;
        private decimal valorMercanciaField;
        private string monedaField; //c_Moneda
        private string fraccionArancelariaField; //c_FraccionArancelaria
        private string uUIDComercioExtField;
        
        #endregion

        public CartaPorteMercanciasMercancia() {
            this.pedimentosField = new BindingList<string>();
            this.guiasIdentificacionField = new BindingList<CartaPorteMercanciasMercanciaGuiasIdentificacion>();
            this.cantidadTransportaField = new BindingList<CartaPorteMercanciasMercanciaCantidadTransporta>();
            this.materialPeligrosoField = false;
            //this.detalleMercanciaField = new CartaPorteMercanciasMercanciaDetalleMercancia();
        }

        /// <summary>
        /// Nodo condicional para registrar la información del(los) número(s) de pedimento(s) de importación que se encuentra(n) asociado(s) al traslado de los bienes y/o mercancías de procedencia extranjera para acreditar 
        /// la legal estancia o tenencia durante su traslado en territorio nacional.
        /// </summary>
        public BindingList<string> Pedimentos {
            get {
                return this.pedimentosField;
            }
            set {
                this.pedimentosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar la información del(los) número(s) de guía(s) que se encuentre(n) asociado(s) al(los) paquete(s) que se traslada(n) dentro del territorio nacional.
        /// </summary>
        public BindingList<CartaPorteMercanciasMercanciaGuiasIdentificacion> GuiasIdentificacion {
            get {
                return this.guiasIdentificacionField;
            }
            set {
                this.guiasIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo opcional para registrar la cantidad de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte, que será captada o distribuida en distintos puntos, a fin de identificar el punto de origen y destino correspondiente
        /// </summary>
        public BindingList<CartaPorteMercanciasMercanciaCantidadTransporta> CantidadTransporta {
            get {
                return this.cantidadTransportaField;
            }
            set {
                this.cantidadTransportaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar especificaciones de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public CartaPorteMercanciasMercanciaDetalleMercancia DetalleMercancia {
            get {
                return this.detalleMercanciaField;
            }
            set {
                this.detalleMercanciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave de producto de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public string BienesTransp {
            get {
                return this.bienesTranspField;
            }
            set {
                this.bienesTranspField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la clave de producto de la STCC (por sus siglas en inglés, Standard Transportation Commodity Code), cuando el medio de transporte utilizado para el traslado de los bienes y/o mercancías sea ferroviario.
        /// </summary>
        public string ClaveSTCC {
            get {
                return this.claveSTCCField;
            }
            set {
                this.claveSTCCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para detallar las características de los bienes y/o mercancías que se trasladan en los distintos medios de transporte
        /// </summary>
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la cantidad total de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave de la unidad de medida estandarizada aplicable para la cantidad de los bienes y/o mercancías que se trasladan en los distintos medios de transporte. La unidad debe corresponder con la descripción de los bienes y/o mercancías registrados.
        /// </summary>
        public string ClaveUnidad {
            get {
                return this.claveUnidadField;
            }
            set {
                this.claveUnidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar la unidad de medida propia para la cantidad de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte. La unidad debe corresponder con la descripción de los bienes y/o mercancías.
        /// </summary>
        public string Unidad {
            get {
                return this.unidadField;
            }
            set {
                this.unidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar las medidas del empaque de los bienes y/o mercancías que se trasladan en los distintos medios de transporte. Se debe registrar la longitud, 
        /// la altura y la anchura en centímetros o en pulgadas, separados dichos valores con una diagonal, i.e. 30/40/30cm.
        /// </summary>
        public string Dimensiones {
            get {
                return this.dimensionesField;
            }
            set {
                this.dimensionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para precisar que los bienes y/o mercancías que se trasladan son considerados o clasificados como material peligroso.
        /// </summary>
        public bool MaterialPeligroso {
            get {
                return this.materialPeligrosoField;
            }
            set {
                this.materialPeligrosoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para indicar la clave del tipo de material peligroso que se transporta de acuerdo a la NOM-002-SCT/2011.
        /// </summary>
        public string CveMaterialPeligroso {
            get {
                return this.cveMaterialPeligrosoField;
            }
            set {
                this.cveMaterialPeligrosoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para precisar la clave del tipo de embalaje que se requiere para transportar el material o residuo peligroso.
        /// </summary>
        public string Embalaje {
            get {
                return this.embalajeField;
            }
            set {
                this.embalajeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar la descripción del embalaje de los bienes y/o mercancías que se trasladan y que se consideran material o residuo peligroso.
        /// </summary>
        public string DescripEmbalaje {
            get {
                return this.descripEmbalajeField;
            }
            set {
                this.descripEmbalajeField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para indicar en kilogramos el peso estimado de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public decimal PesoEnKg {
            get {
                return this.pesoEnKgField;
            }
            set {
                this.pesoEnKgField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el monto del valor de los bienes y/o mercancías que se trasladan en los distintos medios de transporte, de acuerdo al valor mercado, al valor pactado en la contraprestación o bien al valor estimado que determine el contribuyente.
        /// </summary>
        public decimal ValorMercancia {
            get {
                return this.valorMercanciaField;
            }
            set {
                this.valorMercanciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para identificar la clave de la moneda utilizada para expresar el valor de los bienes y/o mercancías que se trasladan en los distintos medios de transporte. Cuando se usa moneda nacional se registra MXN, de acuerdo a la especificación ISO 4217.
        /// </summary>
        public string Moneda {
            get {
                return this.monedaField;
            }
            set {
                this.monedaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional que sirve para expresar la clave de la fracción arancelaria que corresponde con la descripción de los bienes y/o mercancías que se trasladan en los distintos medios de transporte
        /// </summary>
        public string FraccionArancelaria {
            get {
                return this.fraccionArancelariaField;
            }
            set {
                this.fraccionArancelariaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el folio fiscal (UUID) del comprobante de comercio exterior que se relaciona.
        /// </summary>
        public string UUIDComercioExt {
            get {
                return this.uUIDComercioExtField;
            }
            set {
                this.uUIDComercioExtField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string DetallesToString {
            get { if (this.DetalleMercancia != null)
                    return this.DetalleMercancia.ToString();
                return string.Empty;
            }
        }

        [JsonIgnore]
        public string PedimentosToString {
            get { return string.Join(", ", this.Pedimentos); }
        }

        [JsonIgnore]
        public object Tag { get; set; }

        public virtual CartaPorteMercanciasMercancia Clone() {
            var duplicado = (CartaPorteMercanciasMercancia)this.MemberwiseClone();
            return duplicado;
        }
    }
}