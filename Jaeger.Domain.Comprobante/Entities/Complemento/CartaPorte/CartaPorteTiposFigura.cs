﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para indicar los datos del(los) tipo(s) de figura(s) que participan en el traslado de los bienes y/o mercancías en los distintos medios de transporte.
    /// </summary>
    [JsonObject("TiposFigura")]
    public class CartaPorteTiposFigura : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<string> partesTransporteField; //CartaPorteTiposFiguraPartesTransporte
        private CartaPorteTiposFiguraDomicilio domicilioField;
        private string tipoFiguraField; //c_FiguraTransporte
        private string rFCFiguraField;
        private string numLicenciaField;
        private string nombreFiguraField;
        private string numRegIdTribFiguraField;
        private string residenciaFiscalFiguraField; //c_Pais
        #endregion

        public CartaPorteTiposFigura() {
            this.domicilioField = new CartaPorteTiposFiguraDomicilio();
            this.partesTransporteField = new BindingList<string>();
        }

        /// <summary>
        /// Nodo condicional para indicar los datos de las partes del transporte de las cuales el emisor del comprobante es distinto al dueño de las mismas, por ejemplo: vehículos, máquinas, contenedores, 
        /// plataformas, etc; mismos que son utilizados para el traslado de los bienes y/o mercancías.
        /// </summary>
        public BindingList<string> PartesTransporte {
            get {
                return this.partesTransporteField;
            }
            set {
                this.partesTransporteField = value;
                this.OnPropertyChanged();
            }
        }

        public CartaPorteTiposFiguraDomicilio Domicilio {
            get {
                return this.domicilioField;
            }
            set {
                this.domicilioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave de la figura de transporte que interviene en el traslado de los bienes y/o mercancías.
        /// </summary>
        public string TipoFigura {
            get {
                return this.tipoFiguraField;
            }
            set {
                this.tipoFiguraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el RFC de la figura de transporte que interviene en el traslado de los bienes y/o mercancías.
        /// </summary>
        public string RFCFigura {
            get {
                return this.rFCFiguraField;
            }
            set {
                this.rFCFiguraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de la licencia o el permiso otorgado al operador del autotransporte de carga en el que realiza el traslado de los bienes y/o mercancías.
        /// </summary>
        public string NumLicencia {
            get {
                return this.numLicenciaField;
            }
            set {
                this.numLicenciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el nombre de la figura de transporte que interviene en el traslado de los bienes y/o mercancías.
        /// </summary>
        public string NombreFigura {
            get {
                return this.nombreFiguraField;
            }
            set {
                this.nombreFiguraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el número de identificación o registro fiscal del país de residencia de la figura de transporte que interviene en el traslado de los bienes y/o mercancías, cuando se trate de residentes en el extranjero para los efectos fiscales correspondientes.
        /// </summary>
        public string NumRegIdTribFigura {
            get {
                return this.numRegIdTribFiguraField;
            }
            set {
                this.numRegIdTribFiguraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la clave del país de residencia de la figura de transporte que interviene en el traslado de los bienes y/o mercancías para los efectos fiscales correspondientes
        /// </summary>
        public string ResidenciaFiscalFigura {
            get {
                return this.residenciaFiscalFiguraField;
            }
            set {
                this.residenciaFiscalFiguraField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string PartesTransporteToString {
            get { return string.Join(",", this.PartesTransporte); }
        }

        [JsonIgnore]
        public string DomicilioToString {
            get { return this.Domicilio.ToString(); }
        }
    }
}