﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar la información que permite identificar el (los) carro(s) en el (los) que se trasladan los bienes y/o mercancías por vía férrea.
    /// </summary>
    [JsonObject("mercanciasTransporteFerroviarioCarro")]
    public partial class CartaPorteMercanciasTransporteFerroviarioCarro : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<CartaPorteMercanciasTransporteFerroviarioCarroContenedor> contenedorField;
        private string tipoCarroField; //c_TipoCarro
        private string matriculaCarroField;
        private string guiaCarroField;
        private decimal toneladasNetasCarroField;
        #endregion

        /// <summary>
        /// Nodo condicional para especificar el tipo de contenedor o vagón en el que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public BindingList<CartaPorteMercanciasTransporteFerroviarioCarroContenedor> Contenedor {
            get {
                return this.contenedorField;
            }
            set {
                this.contenedorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave del tipo de carro utilizado para el traslado de los bienes y/o mercancías por vía férrea.
        /// </summary>
        public string TipoCarro {
            get {
                return this.tipoCarroField;
            }
            set {
                this.tipoCarroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el número de contenedor, carro de ferrocarril o número económico del vehículo en el que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public string MatriculaCarro {
            get {
                return this.matriculaCarroField;
            }
            set {
                this.matriculaCarroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el número de guía asignado al contenedor, carro de ferrocarril o vehículo, en el que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public string GuiaCarro {
            get {
                return this.guiaCarroField;
            }
            set {
                this.guiaCarroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la cantidad de las toneladas netas depositadas en el contenedor, carro de ferrocarril o vehículo en el que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public decimal ToneladasNetasCarro {
            get {
                return this.toneladasNetasCarroField;
            }
            set {
                this.toneladasNetasCarroField = value;
                this.OnPropertyChanged();
            }
        }
    }
}