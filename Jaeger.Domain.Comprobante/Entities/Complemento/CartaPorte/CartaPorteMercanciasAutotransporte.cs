﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar la información que permita la identificación del autotransporte de carga, por medio del cual se trasladan los bienes y/o mercancías, que transitan a través de las carreteras del territorio nacional.
    /// </summary>
    [JsonObject("mercanciasAutotransporte")]
    public partial class CartaPorteMercanciasAutotransporte : BasePropertyChangeImplementation {
        private CartaPorteMercanciasAutotransporteIdentificacionVehicular identificacionVehicularField;
        private CartaPorteMercanciasAutotransporteSeguros segurosField;
        private BindingList<CartaPorteMercanciasAutotransporteRemolque> remolquesField;
        private string permSCTField; //c_TipoPermiso
        private string numPermisoSCTField;

        public CartaPorteMercanciasAutotransporte() {
            this.Remolques = new BindingList<CartaPorteMercanciasAutotransporteRemolque>();
            this.identificacionVehicularField = new CartaPorteMercanciasAutotransporteIdentificacionVehicular();
            this.segurosField = new CartaPorteMercanciasAutotransporteSeguros();
            this.remolquesField = new BindingList<CartaPorteMercanciasAutotransporteRemolque>();
        }

        /// <summary>
        /// Nodo requerido para registrar los datos de identificación del autotransporte en el que se trasladan los bienes y/o mercancías.
        /// </summary>
        public CartaPorteMercanciasAutotransporteIdentificacionVehicular IdentificacionVehicular {
            get {
                return this.identificacionVehicularField;
            }
            set {
                this.identificacionVehicularField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para registrar los datos de las pólizas de seguro que cubren los riesgos en el traslado de los bienes y/o mercancías.
        /// </summary>
        public CartaPorteMercanciasAutotransporteSeguros Seguros {
            get {
                return this.segurosField;
            }
            set {
                this.segurosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar los datos del(los) remolque(s) o semirremolque(s) que se adaptan al autotransporte para realizar el traslado de los bienes y/o mercancías.
        /// </summary>
        public BindingList<CartaPorteMercanciasAutotransporteRemolque> Remolques {
            get {
                return this.remolquesField;
            }
            set {
                this.remolquesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave del tipo de permiso proporcionado por la SCT o la autoridad análoga, el cual debe corresponder con el tipo de autotransporte utilizado para el traslado de los bienes y/o mercancías de acuerdo al catálogo correspondiente.
        /// </summary>
        public string PermSCT {
            get {
                return this.permSCTField;
            }
            set {
                this.permSCTField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el número del permiso otorgado por la SCT o la autoridad correspondiente, al autotransporte utilizado para el traslado de los bienes y/o mercancías. 
        /// </summary>
        public string NumPermisoSCT {
            get {
                return this.numPermisoSCTField;
            }
            set {
                this.numPermisoSCTField = value;
                this.OnPropertyChanged();
            }
        }
    }
}