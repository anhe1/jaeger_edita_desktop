﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar los datos del(los) remolque(s) o semirremolque(s) que se adaptan al autotransporte para realizar el traslado de los bienes y/o mercancías.
    /// </summary>
    [JsonObject("mercanciasAutotransporteRemolque")]
    public partial class CartaPorteMercanciasAutotransporteRemolque : BasePropertyChangeImplementation {
        private string subTipoRemField; //c_SubTipoRem
        private string placaField;

        /// <summary>
        /// Atributo requerido para expresar la clave del subtipo de remolque o semirremolques que se emplean con el autotransporte para el traslado de los bienes y/o mercancías.
        /// </summary>
        public string SubTipoRem {
            get {
                return this.subTipoRemField;
            }
            set {
                this.subTipoRemField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar los caracteres alfanuméricos, sin guiones ni espacios de la placa vehicular del remolque o semirremolque que es utilizado para transportar los bienes y/o mercancías.
        /// <xs:whiteSpace value="collapse"/> 
        /// <xs:pattern value = "[^(?!.*\s)-]{5,7}" />
        /// </summary>
        public string Placa {
            get {
                return this.placaField;
            }
            set {
                this.placaField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Sub Tipo de Remolque: {0} Placa: {1}", this.SubTipoRem, this.Placa);
        }
    }
}