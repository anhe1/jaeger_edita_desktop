﻿using SqlSugar;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Complemento para incorporar al Comprobante Fiscal Digital por Internet (CFDI), la información relacionada a los bienes y/o mercancías, ubicaciones de origen, puntos intermedios y destinos, así como lo 
    /// referente al medio por el que se transportan; ya sea por vía terrestre (autotransporte y férrea), marítima y/o aérea; además de incluir el traslado de hidrocarburos y petrolíferos.
    /// </summary>
    [SugarTable("_cmpcp", "complemento carta porte")]
    public class CartaPorteDetailModel : CartaPorteModel {
        public CartaPorteDetailModel() : base() {
        }
    }
}