﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar la información de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
    /// </summary>
    [JsonObject("mercancias")]
    public class CartaPorteMercancias : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<CartaPorteMercanciasMercancia> mercanciaField;
        private CartaPorteMercanciasAutotransporte autotransporteField;
        private CartaPorteMercanciasTransporteMaritimo transporteMaritimoField;
        private CartaPorteMercanciasTransporteAereo transporteAereoField;
        private CartaPorteMercanciasTransporteFerroviario transporteFerroviarioField;
        private decimal pesoBrutoTotalField;
        private string unidadPesoField; //c_ClaveUnidadPeso
        private decimal pesoNetoTotalField;
        private int numTotalMercanciasField;
        private decimal cargoPorTasacionField;
        #endregion

        public CartaPorteMercancias() {
            this.mercanciaField = new BindingList<CartaPorteMercanciasMercancia>() { RaiseListChangedEvents = true };
            this.mercanciaField.ListChanged += MercanciaField_ListChanged;
            //this.autotransporteField = new CartaPorteMercanciasAutotransporte();
            //this.transporteAereoField = new CartaPorteMercanciasTransporteAereo();
            //this.transporteMaritimoField = new CartaPorteMercanciasTransporteMaritimo();
            //this.transporteFerroviarioField = new CartaPorteMercanciasTransporteFerroviario();
        }

        /// <summary>
        /// Nodo requerido para registrar detalladamente la información de los bienes y/o mercancías que se trasladan en los distintos medios de transporte
        /// </summary>
        public BindingList<CartaPorteMercanciasMercancia> Mercancia {
            get {
                return this.mercanciaField;
            }
            set {
                this.mercanciaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar la información que permita la identificación del autotransporte de carga, por medio del cual se trasladan los bienes y/o mercancías, que transitan a través de las carreteras del territorio nacional.
        /// </summary>
        public CartaPorteMercanciasAutotransporte Autotransporte {
            get {
                return this.autotransporteField;
            }
            set {
                this.autotransporteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar la información que permita la identificación de la embarcación a través de la cual se trasladan los bienes y/o mercancías por vía marítima.
        /// </summary>
        public CartaPorteMercanciasTransporteMaritimo TransporteMaritimo {
            get {
                return this.transporteMaritimoField;
            }
            set {
                this.transporteMaritimoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar la información que permita la identificación del transporte aéreo por medio del cual se trasladan los bienes y/o mercancías.
        /// </summary>
        public CartaPorteMercanciasTransporteAereo TransporteAereo {
            get {
                return this.transporteAereoField;
            }
            set {
                this.transporteAereoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para registrar la información que permita la identificación del carro o contenedor en el que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public CartaPorteMercanciasTransporteFerroviario TransporteFerroviario {
            get {
                return this.transporteFerroviarioField;
            }
            set {
                this.transporteFerroviarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la suma del peso bruto total estimado de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public decimal PesoBrutoTotal {
            get {
                return this.pesoBrutoTotalField;
            }
            set {
                this.pesoBrutoTotalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave de la unidad de medida estandarizada del peso de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public string UnidadPeso {
            get {
                return this.unidadPesoField;
            }
            set {
                this.unidadPesoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la suma de los valores indicados en el atributo “PesoNeto” del nodo “DetalleMercancia”.
        /// </summary>
        public decimal PesoNetoTotal {
            get {
                return this.pesoNetoTotalField;
            }
            set {
                this.pesoNetoTotalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el número total de los bienes y/o mercancías que se trasladan en los distintos medios de transporte, identificándose por cada nodo "Mercancia" registrado en el complemento.</xs:documentation>
        /// </summary>
        public int NumTotalMercancias {
            get { 
                return this.numTotalMercanciasField;
            }
            set {
                this.numTotalMercanciasField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el monto del importe pagado por la tasación de los bienes y/o mercancías que se trasladan vía aérea.
        /// </summary>
        public decimal CargoPorTasacion {
            get {
                return this.cargoPorTasacionField;
            }
            set {
                this.cargoPorTasacionField = value;
                this.OnPropertyChanged();
            }
        }

        private void MercanciaField_ListChanged(object sender, ListChangedEventArgs e) {
            if (mercanciaField != null) {
                this.NumTotalMercancias = this.mercanciaField.Count;
            }
        }

        public string Json(int oFormat = 0) {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, (Formatting)oFormat, conf);
        }

        public static CartaPorteMercancias Json(string inputString) {
            try {
                return JsonConvert.DeserializeObject<CartaPorteMercancias>(inputString);
            } catch (System.Exception ex) {
                System.Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}