﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar la información que permita la identificación del carro o contenedor en el que se trasladan los bienes y/o mercancías por vía férrea.
    /// </summary>
    [JsonObject("mercanciasTransporteFerroviario")]
    public partial class CartaPorteMercanciasTransporteFerroviario : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<CartaPorteMercanciasTransporteFerroviarioDerechosDePaso> derechosDePasoField;
        private BindingList<CartaPorteMercanciasTransporteFerroviarioCarro> carroField;
        private string tipoDeServicioField; //c_TipoDeServicio
        private string tipoDeTraficoField; // c_TipoDeTrafico
        private string nombreAsegField;
        private string numPolizaSeguroField;
        #endregion

        /// <summary>
        /// Nodo opcional para registrar los tipos de derechos de paso cubiertos por el transportista en las vías férreas de las cuales no es concesionario o asignatario, así como la distancia establecida en kilómetros.
        /// </summary>
        public BindingList<CartaPorteMercanciasTransporteFerroviarioDerechosDePaso> DerechosDePaso {
            get {
                return this.derechosDePasoField;
            }
            set {
                this.derechosDePasoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para registrar la información que permite identificar el (los) carro(s) en el (los) que se trasladan los bienes y/o mercancías por vía férrea.
        /// </summary>
        public BindingList<CartaPorteMercanciasTransporteFerroviarioCarro> Carro {
            get {
                return this.carroField;
            }
            set {
                this.carroField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave del tipo de servicio utilizado para el traslado de los bienes y/o mercancías por vía férrea.
        /// </summary>
        public string TipoDeServicio {
            get {
                return this.tipoDeServicioField;
            }
            set {
                this.tipoDeServicioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave del tipo de tráfico (interrelación entre concesionarios) para realizar el traslado de los bienes y/o mercancías por vía férrea dentro del territorio nacional. (catCartaPorte:c_TipoDeTrafico)
        /// </summary>
        public string TipoDeTrafico {
            get {
                return this.tipoDeTraficoField;
            }
            set {
                this.tipoDeTraficoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el nombre de la aseguradora que cubre los riesgos para el traslado de los bienes y/o mercancías por vía férrea.
        /// </summary>
        public string NombreAseg {
            get {
                return this.nombreAsegField;
            }
            set {
                this.nombreAsegField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el número de póliza asignada por la aseguradora para la protección e indemnización por responsabilidad civil en el traslado de los bienes y/o mercancías que se realiza por vía férrea.
        /// </summary>
        public string NumPolizaSeguro {
            get {
                return this.numPolizaSeguroField;
            }
            set {
                this.numPolizaSeguroField = value;
                this.OnPropertyChanged();
            }
        }
    }
}