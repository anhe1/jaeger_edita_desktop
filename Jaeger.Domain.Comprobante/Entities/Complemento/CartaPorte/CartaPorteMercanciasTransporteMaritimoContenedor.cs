﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    [JsonObject("mercanciasTransporteMaritimoContenedor")]
    public partial class CartaPorteMercanciasTransporteMaritimoContenedor : BasePropertyChangeImplementation {
        #region declaraciones
        private string matriculaContenedorField;
        private string tipoContenedorField; //c_ContenedorMaritimo
        private string numPrecintoField;
        #endregion

        public string MatriculaContenedor {
            get {
                return this.matriculaContenedorField;
            }
            set {
                this.matriculaContenedorField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoContenedor {
            get {
                return this.tipoContenedorField;
            }
            set {
                this.tipoContenedorField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumPrecinto {
            get {
                return this.numPrecintoField;
            }
            set {
                this.numPrecintoField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Matricula: {0} Tipo: {1} Núm. Precinto: {2}", this.MatriculaContenedor, this.TipoContenedor, this.NumPrecinto);
        }
    }
}