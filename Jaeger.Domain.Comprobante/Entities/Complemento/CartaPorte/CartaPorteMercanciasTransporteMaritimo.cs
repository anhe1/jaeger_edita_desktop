﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    [JsonObject("mercanciasTransporteMaritimo")]
    public class CartaPorteMercanciasTransporteMaritimo : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<CartaPorteMercanciasTransporteMaritimoContenedor> contenedorField;
        private string permSCTField; //c_TipoPermiso
        private string numPermisoSCTField;
        private string nombreAsegField;
        private string numPolizaSeguroField;
        private string tipoEmbarcacionField; //c_ConfigMaritima
        private string matriculaField;
        private string numeroOMIField;
        private int anioEmbarcacionField;
        private string nombreEmbarcField;
        private string nacionalidadEmbarcField; //c_Pais
        private decimal unidadesDeArqBrutoField;
        private string tipoCargaField; //c_ClaveTipoCarga
        private string numCertITCField;
        private decimal esloraField;
        private decimal mangaField;
        private decimal caladoField;
        private string lineaNavieraField;
        private string nombreAgenteNavieroField;
        private string numAutorizacionNavieroField; //c_NumAutorizacionNaviero
        private string numViajeField;
        private string numConocEmbarcField;
        #endregion

        public CartaPorteMercanciasTransporteMaritimo() { }

        /// <remarks/>
        public BindingList<CartaPorteMercanciasTransporteMaritimoContenedor> Contenedor {
            get {
                return this.contenedorField;
            }
            set {
                this.contenedorField = value;
                this.OnPropertyChanged();
            }
        }

        public string PermSCT {
            get {
                return this.permSCTField;
            }
            set {
                this.permSCTField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumPermisoSCT {
            get {
                return this.numPermisoSCTField;
            }
            set {
                this.numPermisoSCTField = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreAseg {
            get {
                return this.nombreAsegField;
            }
            set {
                this.nombreAsegField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumPolizaSeguro {
            get {
                return this.numPolizaSeguroField;
            }
            set {
                this.numPolizaSeguroField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoEmbarcacion {
            get {
                return this.tipoEmbarcacionField;
            }
            set {
                this.tipoEmbarcacionField = value;
                this.OnPropertyChanged();
            }
        }

        public string Matricula {
            get {
                return this.matriculaField;
            }
            set {
                this.matriculaField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumeroOMI {
            get {
                return this.numeroOMIField;
            }
            set {
                this.numeroOMIField = value;
                this.OnPropertyChanged();
            }
        }

        public int AnioEmbarcacion {
            get {
                return this.anioEmbarcacionField;
            }
            set {
                this.anioEmbarcacionField = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreEmbarc {
            get {
                return this.nombreEmbarcField;
            }
            set {
                this.nombreEmbarcField = value;
                this.OnPropertyChanged();
            }
        }

        public string NacionalidadEmbarc {
            get {
                return this.nacionalidadEmbarcField;
            }
            set {
                this.nacionalidadEmbarcField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal UnidadesDeArqBruto {
            get {
                return this.unidadesDeArqBrutoField;
            }
            set {
                this.unidadesDeArqBrutoField = value;
                this.OnPropertyChanged();
            }
        }

        public string TipoCarga {
            get {
                return this.tipoCargaField;
            }
            set {
                this.tipoCargaField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumCertITC {
            get {
                return this.numCertITCField;
            }
            set {
                this.numCertITCField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Eslora {
            get {
                return this.esloraField;
            }
            set {
                this.esloraField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Manga {
            get {
                return this.mangaField;
            }
            set {
                this.mangaField = value;
                this.OnPropertyChanged();
            }
        }

        public decimal Calado {
            get {
                return this.caladoField;
            }
            set {
                this.caladoField = value;
                this.OnPropertyChanged();
            }
        }

        public string LineaNaviera {
            get {
                return this.lineaNavieraField;
            }
            set {
                this.lineaNavieraField = value;
                this.OnPropertyChanged();
            }
        }

        public string NombreAgenteNaviero {
            get {
                return this.nombreAgenteNavieroField;
            }
            set {
                this.nombreAgenteNavieroField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumAutorizacionNaviero {
            get {
                return this.numAutorizacionNavieroField;
            }
            set {
                this.numAutorizacionNavieroField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumViaje {
            get {
                return this.numViajeField;
            }
            set {
                this.numViajeField = value;
                this.OnPropertyChanged();
            }
        }

        public string NumConocEmbarc {
            get {
                return this.numConocEmbarcField;
            }
            set {
                this.numConocEmbarcField = value;
                this.OnPropertyChanged();
            }
        }
    }
}