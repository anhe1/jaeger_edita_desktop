﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo opcional para registrar los tipos de derechos de paso cubiertos por el transportista en las vías férreas de las cuales no es concesionario o asignatario, así como la distancia establecida en kilómetros.
    /// </summary>
    [JsonObject("mercanciasTransporteFerroviarioDerechosDePaso")]
    public partial class CartaPorteMercanciasTransporteFerroviarioDerechosDePaso : BasePropertyChangeImplementation {
        #region declaraciones
        private string tipoDerechoDePasoField; // c_DerechosDePaso
        private decimal kilometrajePagadoField;
        #endregion

        /// <summary>
        /// Atributo requerido para registrar la clave del derecho de paso pagado por el transportista en las vías férreas de las cuales no es concesionario o asignatario.
        /// </summary>
        public string TipoDerechoDePaso {
            get {
                return this.tipoDerechoDePasoField;
            }
            set {
                this.tipoDerechoDePasoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el total de kilómetros pagados por el transportista en las vías férreas de las cuales no es concesionario o asignatario con el derecho de paso.
        /// </summary>
        public decimal KilometrajePagado {
            get {
                return this.kilometrajePagadoField;
            }
            set {
                this.kilometrajePagadoField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Tipo Derecha de Pago: {0} Kilometraja Pagado: {1}", this.TipoDerechoDePaso, this.KilometrajePagado.ToString("N2"));
        }
    }
}