﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Complemento para incorporar al Comprobante Fiscal Digital por Internet (CFDI), la información relacionada a los bienes y/o mercancías, ubicaciones de origen, puntos intermedios y destinos, así como lo 
    /// referente al medio por el que se transportan; ya sea por vía terrestre (autotransporte y férrea), marítima y/o aérea; además de incluir el traslado de hidrocarburos y petrolíferos.
    /// </summary>
    [SugarTable("_cmpcp", "complemento carta porte")]
    public class CartaPorteModel : BasePropertyChangeImplementation {
        #region declaraciones
        private int _index;
        private bool _activo;
        private BindingList<CartaPorteUbicacion> ubicacionesField;
        private CartaPorteMercancias mercanciasField;
        private BindingList<CartaPorteTiposFigura> figuraTransporteField;
        private string versionField;
        private int transpInternacField; //CartaPorteTranspInternacEnum
        private string entradaSalidaMercField; //CartaPorteEntradaSalidaMerc
        private string paisOrigenDestinoField; //c_Pais
        private string viaEntradaSalidaField; //c_CveTransporte
        private decimal totalDistRecField;
        private string creo;
        private DateTime fechaNuevo;
        private string modifica;
        private DateTime? fechaModifica;
        private int idComprobante;
        #endregion

        public CartaPorteModel() {
            this._activo = true;
            this.versionField = "2.0";
            this.TranspInternac = 0;
            this.ubicacionesField = new BindingList<CartaPorteUbicacion>();
            this.mercanciasField = new CartaPorteMercancias();
            this.figuraTransporteField = new BindingList<CartaPorteTiposFigura>();
        }

        [DataNames("_cmpcp_id")]
        [SugarColumn(ColumnName = "_cmpcp_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdCartaPorte {
            get { return this._index; }
            set { this._index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmpcp_a")]
        [SugarColumn(ColumnName = "_cmpcp_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this._activo; }
            set { this._activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido con valor prefijado en el cual se indica la versión del complemento Carta Porte.
        /// </summary>
        [DataNames("_cmpcp_ver")]
        [SugarColumn(ColumnName = "_cmpcp_ver", ColumnDescription = "version del complemento", Length = 3)]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmpcp_cfd_id")]
        [SugarColumn(ColumnName = "_cmpcp_cfd_id", ColumnDescription = "indice de relacion del comprobante fiscal")]
        public int IdComprobante {
            get { return this.idComprobante; }
            set {
                this.idComprobante = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar si los bienes y/o mercancías que son transportadas ingresan o salen del territorio nacional.
        /// </summary>
        [DataNames("_cmpcp_inter")]
        [SugarColumn(ColumnName = "_cmpcp_inter", ColumnDescription = "para expresar si los bienes y/o mercancías que son transportadas ingresan o salen del territorio nacional.")]
        public int TranspInternac {
            get {
                return this.transpInternacField;
            }
            set {
                this.transpInternacField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para precisar si los bienes y/o mercancías ingresan o salen del territorio nacional.
        /// </summary>
        [DataNames("_cmpcp_etmer")]
        [SugarColumn(ColumnName = "_cmpcp_etmer", ColumnDescription = "condicional para precisar si los bienes y/o mercancías ingresan o salen del territorio nacional.", Length = 10, IsNullable = true)]
        public string EntradaSalidaMerc {
            get {
                return this.entradaSalidaMercField;
            }
            set {
                this.entradaSalidaMercField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la clave del país de origen o destino de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        [DataNames("_cmpcp_pais")]
        [SugarColumn(ColumnName = "_cmpcp_pais", ColumnDescription = "clave de pais de origen o destino", Length = 10, IsNullable = true)]
        public string PaisOrigenDestino {
            get {
                return this.paisOrigenDestinoField;
            }
            set {
                this.paisOrigenDestinoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la vía de ingreso o salida de los bienes y/o mercancías en territorio nacional.
        /// </summary>
        [DataNames("_cmpcp_via")]
        [SugarColumn(ColumnName = "_cmpcp_via", ColumnDescription = "via de ingreso o salida de mercancia", Length = 2, IsNullable = true)]
        public string ViaEntradaSalida {
            get {
                return this.viaEntradaSalidaField;
            }
            set {
                this.viaEntradaSalidaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para indicar en kilómetros, la suma de las distancias recorridas, registradas en el atributo “DistanciaRecorrida”, para el traslado de los bienes y/o mercancías.
        /// </summary>
        [DataNames("_cmpcp_total")]
        [SugarColumn(ColumnName = "_cmpcp_total", ColumnDescription = "indicar en kilómetros, la suma de las distancias recorridas, registradas en el atributo DistanciaRecorrida, para el traslado de los bienes y/o mercancías", Length = 16, DecimalDigits = 4, DefaultValue = "0", IsNullable = true)]
        public decimal TotalDistRec {
            get {
                return this.totalDistRecField;
            }
            set {
                this.totalDistRecField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para registrar las distintas ubicaciones que sirven para indicar el domicilio del origen y/o destino que tienen los bienes y/o mercancías que se trasladan 
        /// a través de los distintos medios de transporte.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<CartaPorteUbicacion> Ubicaciones {
            get {
                return this.ubicacionesField;
            }
            set {
                this.ubicacionesField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmpcp_ubica")]
        [SugarColumn(ColumnName = "_cmpcp_ubica", ColumnDataType = "Text", ColumnDescription = "listado de ubicaciones en formato json", IsNullable = true)]
        public string JUbicaciones {
            get {
                if (this.Ubicaciones != null) {
                    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                    return JsonConvert.SerializeObject(this.Ubicaciones, conf);
                }
                return null;
            }
            set { if (!string.IsNullOrEmpty(value)) {
                    this.Ubicaciones = JsonConvert.DeserializeObject<BindingList<CartaPorteUbicacion>>(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Nodo requerido para registrar la información de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CartaPorteMercancias Mercancias {
            get {
                return this.mercanciasField;
            }
            set {
                this.mercanciasField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmpcp_merca")]
        [SugarColumn(ColumnName = "_cmpcp_merca", ColumnDataType = "Text", ColumnDescription = "informacion de los bienes y/o servicios que se trasladan", IsNullable = true)]
        public string JMercancias {
            get {
                if (this.Mercancias != null) {
                    return this.Mercancias.Json();
                }
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.Mercancias = CartaPorteMercancias.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Nodo condicional para indicar los datos de la(s) figura(s) del transporte que interviene(n) en el traslado de los bienes y/o mercancías realizado a través de los distintos 
        /// medios de transporte dentro del territorio nacional, cuando el dueño de dicho medio sea diferente del emisor del comprobante con el complemento Carta Porte.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<CartaPorteTiposFigura> FiguraTransporte {
            get {
                return this.figuraTransporteField;
            }
            set {
                this.figuraTransporteField = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmpcp_figt")]
        [SugarColumn(ColumnName = "_cmpcp_figt", ColumnDataType = "Text", ColumnDescription = "datos de la(s) figura(s) del transporte que interviene", IsNullable = true)]
        public string JFiguraTransporte {
            get {
                if (this.FiguraTransporte != null) {
                    var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
                    return JsonConvert.SerializeObject(this.FiguraTransporte, conf);
                }
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.FiguraTransporte = JsonConvert.DeserializeObject<BindingList<CartaPorteTiposFigura>>(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        [DataNames("_cmpcp_usr_n")]
        [SugarColumn(ColumnName = "_cmpcp_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this.creo; }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_cmpcp_fn")]
        [SugarColumn(ColumnName = "_cmpcp_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_cmpcp_usr_m")]
        [SugarColumn(ColumnName = "_cmpcp_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get { return this.modifica; }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_cmpcp_fm")]
        [SugarColumn(ColumnName = "_cmpcp_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaModifica = value;
            }
        }
    }
}