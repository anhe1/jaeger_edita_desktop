﻿using Newtonsoft.Json;
using Jaeger.Domain.Comprobante.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar información del domicilio de origen y/o destino de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
    /// </summary>
    [JsonObject("ubicacionDomicilio")]
    public partial class CartaPorteUbicacionDomicilio : CartaPorteDomicilio {
        

        public CartaPorteUbicacionDomicilio() : base() { }

        public CartaPorteUbicacionDomicilio(CartaPorteUbicacionDomicilio domicilio) {
            this.Calle = domicilio.Calle;
            this.NumeroExterior = domicilio.NumeroExterior;
            this.NumeroInterior = domicilio.NumeroInterior;
            this.Colonia = domicilio.Colonia;
            this.Localidad = domicilio.Localidad;
            this.Referencia = domicilio.Referencia;
            this.Municipio = domicilio.Municipio;
            this.Estado = domicilio.Estado;
            this.Pais = domicilio.Pais;
            this.CodigoPostal = domicilio.CodigoPostal;
        }

        public CartaPorteUbicacionDomicilio(string calle, string numeroExterior, string numeroInterior, string colonia, string localidad, string referencia, string municipio, string estado, string pais, string codigoPostal) : base(calle, numeroExterior, numeroInterior, colonia, localidad, referencia, municipio, estado, pais, codigoPostal) {
        }
    }
}