﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo opcional para registrar la cantidad de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte, que será captada o distribuida en distintos puntos, a fin de identificar el punto de origen y destino correspondiente.
    /// </summary>
    [JsonObject("mercanciasMercanciaCantidadTransporta")]
    public class CartaPorteMercanciasMercanciaCantidadTransporta : BasePropertyChangeImplementationDataError, IDataErrorInfo {
        #region declaraciones
        private decimal cantidadField;
        private string iDOrigenField;
        private string iDDestinoField;
        private string cvesTransporteField; //c_CveTransporte
        #endregion

        public CartaPorteMercanciasMercanciaCantidadTransporta() { }

        /// <summary>
        /// Atributo requerido para expresar el número de bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public decimal Cantidad {
            get {
                return this.cantidadField;
            }
            set {
                this.cantidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del identificador del origen de los bienes y/o mercancías que se trasladan por los distintos medios de transporte, de acuerdo al valor registrado en el atributo 
        /// “IDUbicacion”, del nodo “Ubicacion”.
        /// </summary>
        public string IDOrigen {
            get {
                return this.iDOrigenField;
            }
            set {
                this.iDOrigenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la clave del identificador del destino de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte, de acuerdo al valor registrado en el atributo “IDUbicacion”, del nodo “Ubicacion”.
        /// </summary>
        public string IDDestino {
            get {
                return this.iDDestinoField;
            }
            set {
                this.iDDestinoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para indicar la clave a través de la cual se identifica el medio por el que se transportan los bienes y/o mercancías.
        /// </summary>
        public string CvesTransporte {
            get {
                return this.cvesTransporteField;
            }
            set {
                this.cvesTransporteField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string this[string columnName] {
            get {
                if (columnName == "Cantidad") {
                    if (this.Cantidad <= 0) {
                        return "Número de bienes y/o mercancias es un dato no válido.";
                    }
                }

                if (columnName == "IDOrigen") {
                    if (!this.RegexValido(this.IDOrigen, "OR[0-9]{6}"))
                        return "La clave del identificador del origen de los bienes y/o mercancías no es válido";
                }

                if (columnName == "IDDestino") {
                    if (!this.RegexValido(this.IDDestino, "DE[0-9]{6}"))
                        return "La clave del identificador del destino de los bienes y/o mercancías no es válido";
                }

                return string.Empty;
            }
        }

        [JsonIgnore]
        public string Error {
            get {
                if (this.Cantidad <= 0) {
                    return "Número de bienes y/o mercancias es un dato no válido.";
                }

                if (!this.RegexValido(this.IDOrigen, "OR[0-9]{6}"))
                    return "La clave del identificador del origen de los bienes y/o mercancías no es válido";

                if (!this.RegexValido(this.IDDestino, "DE[0-9]{6}"))
                    return "La clave del identificador del destino de los bienes y/o mercancías no es válido";
                return string.Empty;
            }
        }
    }
}