﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar los datos de las pólizas de seguro que cubren los riesgos en el traslado de los bienes y/o mercancías.
    /// </summary>
    [JsonObject("mercanciasAutotransporteSeguros")]
    public class CartaPorteMercanciasAutotransporteSeguros : BasePropertyChangeImplementation {
        #region declaraciones
        private string aseguraRespCivilField;
        private string polizaRespCivilField;
        private string aseguraMedAmbienteField;
        private string polizaMedAmbienteField;
        private string aseguraCargaField;
        private string polizaCargaField;
        private decimal primaSeguroField;
        #endregion

        /// <summary>
        /// Atributo requerido para registrar el nombre de la aseguradora que cubre los riesgos por responsabilidad civil del autotransporte utilizado para el traslado de los bienes y/o mercancías.
        /// <xs:minLength value="3"/> <xs:maxLength value = "50" /> < xs:pattern value = "[^|]{3,50}" />
        /// </summary>
        public string AseguraRespCivil {
            get {
                return this.aseguraRespCivilField;
            }
            set {
                this.aseguraRespCivilField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el número de póliza asignado por la aseguradora, que cubre los riesgos por responsabilidad civil del autotransporte utilizado para el traslado de los bienes y/o mercancías.
        /// <xs:minLength value="3"/> 
        /// <xs:maxLength value = "30" /> 
        /// <xs:whiteSpace value = "collapse" /> 
        /// <xs:pattern value = "[^|]{3,30}" />
        /// </summary>
        public string PolizaRespCivil {
            get {
                return this.polizaRespCivilField;
            }
            set {
                this.polizaRespCivilField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el nombre de la aseguradora, que cubre los posibles daños al medio ambiente (aplicable para los transportistas de materiales, residuos o remanentes y desechos peligrosos).
        /// <xs:minLength value="3"/> <xs:maxLength value = "50" /> < xs:whiteSpace value = "collapse" /> <xs:pattern value = "[^|]{3,50}" />
        /// </summary>
        public string AseguraMedAmbiente {
            get {
                return this.aseguraMedAmbienteField;
            }
            set {
                this.aseguraMedAmbienteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el número de póliza asignado por la aseguradora, que cubre los posibles daños al medio ambiente (aplicable para los transportistas de materiales, residuos o remanentes y desechos peligrosos).
        /// <xs:minLength value="3"/> <xs:maxLength value = "30" /> <xs:whiteSpace value = "collapse" /> <xs:pattern value = "[^|]{3,30}" />
        /// </summary>
        public string PolizaMedAmbiente {
            get {
                return this.polizaMedAmbienteField;
            }
            set {
                this.polizaMedAmbienteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el nombre de la aseguradora que cubre los riesgos de la carga (bienes y/o mercancías) del autotransporte utilizado para el traslado.
        /// <xs:minLength value="3"/> <xs:maxLength value = "50" /> <xs:whiteSpace value = "collapse" /> <xs:pattern value = "[^|]{3,50}" />
        /// </summary>
        public string AseguraCarga {
            get {
                return this.aseguraCargaField;
            }
            set {
                this.aseguraCargaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para expresar el número de póliza asignado por la aseguradora que cubre los riesgos de la carga (bienes y/o mercancías) del autotransporte utilizado para el traslado.
        /// <xs:minLength value="3"/> <xs:maxLength value = "30" /> <xs:whiteSpace value = "collapse" /> <xs:pattern value = "[^|]{3,30}" />
        /// </summary>
        public string PolizaCarga {
            get {
                return this.polizaCargaField;
            }
            set {
                this.polizaCargaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el valor del importe por el cargo adicional convenido entre el transportista y el cliente, el cual será igual al valor de la prima del seguro contratado, conforme a lo establecido en la cláusula novena del Acuerdo por el que se homologa la Carta de Porte regulada por la Ley de Caminos, Puentes y Autotransporte Federal, con el complemento Carta Porte que debe acompañar al Comprobante Fiscal Digital por Internet (CFDI).
        /// </summary>
        public decimal PrimaSeguro {
            get {
                return this.primaSeguroField;
            }
            set {
                this.primaSeguroField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Aseguradora Responsabilidad Civil: {0} Poliza: {1} /n/r Aseguradora Medio Ambiente: {2} Poliza: {3} Aseguradora de Carga: {4} Poliza: {5} Prima de Seguro: {6}",
                this.AseguraRespCivil, this.PolizaRespCivil, this.AseguraMedAmbiente, this.PolizaMedAmbiente, this.AseguraCarga, this.PolizaCarga, this.PrimaSeguro.ToString("N2"));
        }
    }
}