﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar especificaciones de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
    /// </summary>
    [JsonObject("mercanciasMercanciaDetalleMercancia")]
    public class CartaPorteMercanciasMercanciaDetalleMercancia : BasePropertyChangeImplementation {
        #region declaraciones
        private string unidadPesoMercField; //c_ClaveUnidadPeso
        private decimal pesoBrutoField;
        private decimal pesoNetoField;
        private decimal pesoTaraField;
        private int numPiezasField;
        #endregion

        public CartaPorteMercanciasMercanciaDetalleMercancia() { }

        /// <summary>
        /// Atributo requerido para registrar la clave de la unidad de medida estandarizada del peso de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public string UnidadPesoMerc {
            get {
                return this.unidadPesoMercField;
            }
            set {
                this.unidadPesoMercField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el peso bruto total de los bienes y/o mercancías que se trasladan a través de los diferentes medios de transporte.
        /// </summary>
        public decimal PesoBruto {
            get {
                return this.pesoBrutoField;
            }
            set {
                this.pesoBrutoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el peso neto total de los bienes y/o mercancías que se trasladan en los distintos  medios de transporte.
        /// </summary>
        public decimal PesoNeto {
            get {
                return this.pesoNetoField;
            }
            set {
                this.pesoNetoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el peso bruto, menos el peso neto de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public decimal PesoTara {
            get {
                return this.pesoTaraField;
            }
            set {
                this.pesoTaraField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el número de piezas de los bienes y/o mercancías que se trasladan en los distintos medios de transporte.
        /// </summary>
        public int NumPiezas {
            get {
                return this.numPiezasField;
            }
            set {
                this.numPiezasField = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString() {
            return string.Format("Unidad: {0} Peso Bruto: {2} Peso Neto: {2} Peso Tara: {3} Núm. Piezas: {4}", this.UnidadPesoMerc, PesoBruto.ToString("N2"), this.PesoNeto.ToString("N2"), this.PesoTara.ToString("N2"), this.NumPiezas);
        }
    }
}