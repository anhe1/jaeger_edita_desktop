﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo condicional para registrar la información del(los) número(s) de guía(s) que se encuentre(n) asociado(s) al(los) paquete(s) que se traslada(n) dentro del territorio nacional.
    /// </summary>
    [JsonObject("mercanciasMercanciaGuiasIdentificacion")]
    public class CartaPorteMercanciasMercanciaGuiasIdentificacion : BasePropertyChangeImplementationDataError, IDataErrorInfo {
        #region declaraciones
        private string numeroGuiaIdentificacionField;
        private string descripGuiaIdentificacionField;
        private decimal pesoGuiaIdentificacionField;
        #endregion

        public CartaPorteMercanciasMercanciaGuiasIdentificacion() { }

        /// <summary>
        /// Atributo requerido para expresar el número de guía de cada paquete que se encuentra asociado con el traslado de los bienes y/o mercancías en territorio nacional.
        /// </summary>
        public string NumeroGuiaIdentificacion {
            get {
                return this.numeroGuiaIdentificacionField;
            }
            set {
                this.numeroGuiaIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la descripción del contenido del paquete o carga registrada en la guía, o en el número de identificación, que se encuentra asociado con el
        /// traslado de los bienes y/o mercancías dentro del territorio nacional.
        /// </summary>
        public string DescripGuiaIdentificacion {
            get {
                return this.descripGuiaIdentificacionField;
            }
            set {
                this.descripGuiaIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para indicar en kilogramos, el peso del paquete o carga que se está trasladando en territorio nacional  y que se encuentra registrado en la guía o el número de identificación correspondiente.
        /// </summary>
        public decimal PesoGuiaIdentificacion {
            get {
                return this.pesoGuiaIdentificacionField;
            }
            set {
                this.pesoGuiaIdentificacionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string this[string columnName] {
            get {
                if (columnName == "NumeroGuiaIdentificacion" && !this.RegexValido(this.NumeroGuiaIdentificacion, "[^|]{10,30}")) {
                    return "Formato de número de guía no válido.";
                }

                if (columnName == "DescripGuiaIdentificacion" && !this.RegexValido(this.DescripGuiaIdentificacion, "[^|]{1,1000}")) {
                    return "La descripción no es dato válido.";
                }

                if (!(this.PesoGuiaIdentificacion > new decimal(0.001))) {
                    return "El peso no es dato válido.";
                }
                return string.Empty;
            }
        }

        [JsonIgnore]
        public string Error {
            get {
                if (!this.RegexValido(this.NumeroGuiaIdentificacion, "[^|]{10,30}")) {

                }

                if (!this.RegexValido(this.DescripGuiaIdentificacion, "[^|]{1,1000}")) {
                    return "";
                }

                return string.Empty;
            }
        }

        public override string ToString() {
            return string.Format("Núm. Guia: {0} Descripción: {1} Peso: {2}", this.NumeroGuiaIdentificacion, this.DescripGuiaIdentificacion, this.PesoGuiaIdentificacion.ToString("N2"));
        }
    }
}