﻿using System;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte {
    /// <summary>
    /// Nodo requerido para registrar la ubicación que sirve para indicar el domicilio del origen y/o destino parcial o final, que tienen los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
    /// </summary>
    [JsonObject("ubicacion")]
    public class CartaPorteUbicacion : BasePropertyChangeImplementation {
        #region declaraciones
        private CartaPorteUbicacionDomicilio domicilioField;
        private string tipoUbicacionField;
        private string iDUbicacionField;
        private string rFCRemitenteDestinatarioField;
        private string nombreRemitenteDestinatarioField;
        private string numRegIdTribField;
        private string residenciaFiscalField; //c_Pais
        private string numEstacionField; //c_Estaciones
        private string nombreEstacionField;
        private string navegacionTraficoField; //CartaPorteUbicacionNavegacionTraficoEnum
        private DateTime fechaHoraSalidaLlegadaField;
        private string tipoEstacionField; //c_TipoEstacion
        private decimal distanciaRecorridaField;
        #endregion

        public CartaPorteUbicacion() {
            this.Domicilio = new CartaPorteUbicacionDomicilio();
        }

        

        /// <summary>
        /// Nodo condicional para registrar información del domicilio de origen y/o destino de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public CartaPorteUbicacionDomicilio Domicilio {
            get {
                return this.domicilioField;
            }
            set {
                this.domicilioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para precisar si el tipo de ubicación corresponde al origen o destino de las ubicaciones para el traslado de los bienes y/o mercancías en los distintos medios de transporte.
        /// </summary>
        public string TipoUbicacion {
            get {
                return this.tipoUbicacionField;
            }
            set {
                this.tipoUbicacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar una clave que sirva para identificar el punto de salida o entrada de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte, la cual estará 
        /// integrada de la siguiente forma: para origen el acrónimo “OR” o para destino el acrónimo “DE” seguido de 6 dígitos numéricos asignados por el contribuyente que emite el comprobante para su identificación.
        /// </summary>
        public string IDUbicacion {
            get {
                return this.iDUbicacionField;
            }
            set {
                this.iDUbicacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar el RFC del remitente o destinatario de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        public string RFCRemitenteDestinatario {
            get {
                return this.rFCRemitenteDestinatarioField;
            }
            set {
                this.rFCRemitenteDestinatarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo opcional para registrar el nombre del remitente o destinatario de los bienes y/o mercancías que se trasladan a través de los distintos medios de transporte.
        /// </summary>
        public string NombreRemitenteDestinatario {
            get {
                return this.nombreRemitenteDestinatarioField;
            }
            set {
                this.nombreRemitenteDestinatarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el número de identificación o registro fiscal del país de residencia, para los efectos fiscales del remitente o destinatario de los bienes y/o mercancías que se trasladan cuando 
        /// se trate de residentes en el extranjero.
        /// </summary>
        public string NumRegIdTrib {
            get {
                return this.numRegIdTribField;
            }
            set {
                this.numRegIdTribField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la clave del país de residencia para efectos fiscales del remitente o destinatario de los bienes y/o mercancías, conforme el catálogo de CFDI c_Pais publicado en el portal del SAT en Internet de acuerdo a la especificación ISO 3166-1.
        /// </summary>
        public string ResidenciaFiscal {
            get {
                return this.residenciaFiscalField;
            }
            set {
                this.residenciaFiscalField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar la clave de la estación de origen o destino para el traslado de los bienes y/o mercancías que se realiza a través de los distintos medios de transporte, esto de acuerdo al valor de la columna
        /// “Clave identificación” del catálogo c_Estaciones del complemento Carta Porte que permita asociarla al tipo de transporte.
        /// </summary>
        public string NumEstacion {
            get {
                return this.numEstacionField;
            }
            set {
                this.numEstacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el nombre de la estación de origen o destino por la que se pasa para efectuar el traslado de los bienes y/o mercancías a través de los distintos medios de transporte, conforme al catálogo c_Estaciones del complemento Carta Porte.
        /// </summary>
        public string NombreEstacion {
            get {
                return this.nombreEstacionField;
            }
            set {
                this.nombreEstacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el tipo de puerto de origen o destino en el cual se documentan los bienes y/o mercancías que se trasladan vía marítima.
        /// </summary>
        public string NavegacionTrafico {
            get {
                return this.navegacionTraficoField;
            }
            set {
                this.navegacionTraficoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para registrar la fecha y hora estimada en la que salen o llegan los bienes y/o mercancías de origen o al destino, respectivamente. Se expresa en la forma AAAA-MM-DDThh:mm:ss.
        /// </summary>
        public DateTime FechaHoraSalidaLlegada {
            get {
                return this.fechaHoraSalidaLlegadaField;
            }
            set {
                this.fechaHoraSalidaLlegadaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar el tipo de estación por el que pasan los bienes y/o mercancías durante su traslado a través de los distintos medios de transporte.
        /// </summary>
        public string TipoEstacion {
            get {
                return this.tipoEstacionField;
            }
            set {
                this.tipoEstacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para registrar en kilómetros la distancia recorrida entre la ubicación de origen y la de destino parcial o final, por los distintos medios de transporte que trasladan los bienes y/o mercancías.
        /// </summary>
        public decimal DistanciaRecorrida {
            get {
                return this.distanciaRecorridaField;
            }
            set {
                this.distanciaRecorridaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string DomicilioToString {
            get {
                string direccion = string.Empty;

                if (!(string.IsNullOrEmpty(this.Domicilio.Calle)))
                    direccion = string.Concat("Calle ", this.Domicilio.Calle);

                if (!(string.IsNullOrEmpty(this.Domicilio.NumeroExterior)))
                    direccion = string.Concat(direccion, " No. Ext. ", this.Domicilio.NumeroExterior);

                if (!(string.IsNullOrEmpty(this.Domicilio.NumeroInterior)))
                    direccion = string.Concat(direccion, " No. Int. ", this.Domicilio.NumeroInterior);

                if (!(string.IsNullOrEmpty(this.Domicilio.Colonia)))
                    direccion = string.Concat(direccion, " Col. ", this.Domicilio.Colonia);

                if (!(string.IsNullOrEmpty(this.Domicilio.CodigoPostal)))
                    direccion = string.Concat(direccion, " C.P. ", this.Domicilio.CodigoPostal);

                if (!(string.IsNullOrEmpty(this.Domicilio.Municipio)))
                    direccion = string.Concat(direccion, " ", this.Domicilio.Municipio);

                if (!(string.IsNullOrEmpty(this.Domicilio.Estado)))
                    direccion = string.Concat(direccion, ", ", this.Domicilio.Estado);

                if (!(string.IsNullOrEmpty(this.Domicilio.Referencia)))
                    direccion = string.Concat(direccion, ", Ref: ", this.Domicilio.Referencia);

                if (!(string.IsNullOrEmpty(this.Domicilio.Localidad)))
                    direccion = string.Concat(direccion, ", Loc: ", this.Domicilio.Localidad);

                return direccion;
            }
        }

        public CartaPorteUbicacion DeepCopy() {
            CartaPorteUbicacion other = (CartaPorteUbicacion)this.MemberwiseClone();
            other.Domicilio = new CartaPorteUbicacionDomicilio(this.Domicilio);
            return other;
        }
    }
}