﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    /// <summary>
    /// Complemento: Pago
    /// </summary>
    [SugarTable("_cmppg", "CFDI: complemento de pago")]
    public class ComplementoPagoDetailModel : ComplementoPagoModel, IComplementoPagoDetailModel, IComplementoPagoModel {
        #region declaraciones
        private BindingList<PagosPagoDoctoRelacionadoDetailModel> _DoctosRelacionados;
        private PagosTotales _PagosTotales;
        private decimal _Total;
        #endregion

        public ComplementoPagoDetailModel() : base() {
            this._DoctosRelacionados = new BindingList<PagosPagoDoctoRelacionadoDetailModel>() { RaiseListChangedEvents = true };
            this._DoctosRelacionados.ListChanged += new ListChangedEventHandler(this.DoctosRelacionados_ListChanged);
        }

        [SugarColumn(IsIgnore = true)]
        public PagosTotales Totales {
            get { return this._PagosTotales; }
            set { this._PagosTotales = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer lista de documentos relacionados con los pagos. Por cada documento que se relacione se debe generar un nodo DoctoRelacionado.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<PagosPagoDoctoRelacionadoDetailModel> DoctoRelacionados {
            get { return this._DoctosRelacionados; }
            set { if (this._DoctosRelacionados != null) {
                    this._DoctosRelacionados.ListChanged -= new ListChangedEventHandler(this.DoctosRelacionados_ListChanged);
                }
                this._DoctosRelacionados = value;
                if (this._DoctosRelacionados != null) {
                    this._DoctosRelacionados.ListChanged += new ListChangedEventHandler(this.DoctosRelacionados_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Total {
            get {
                return this._Total;
            }
            set { this._Total = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public PagosTotales Totales1 {
            get {
                if (this._DoctosRelacionados != null) {
                    if (this._DoctosRelacionados.Count > 0) {
                        var _totales = new PagosTotales();
                        foreach (var item in this._DoctosRelacionados) {
                            if (item.Traslados != null) {
                                _totales.TotalTrasladosBaseIVA16 =+ item.Traslados.Where(it => it.Impuesto == Base.ValueObjects.ImpuestoEnum.IVA && it.TipoFactor == ValueObjects.FactorEnum.Tasa && it.TasaOCuota == new decimal(.16)).Sum(it => it.Base);
                            }
                        }
                        return _totales;
                    }
                }
                return null;
            }
        }

        private void DoctosRelacionados_ListChanged(object sender, ListChangedEventArgs e) {
            if (this._DoctosRelacionados != null) {
                if (this._DoctosRelacionados.Count > 0) {
                    this.Total = this._DoctosRelacionados.Where(it => it.Activo == true).Sum(it => it.ImpPagado);
                }
            }
        }

        /// <summary>
        /// buscar mediante uuid un objeto relacionado
        /// </summary>
        /// <param name="uuid">Id Documento (uuid)</param>
        public PagosPagoDoctoRelacionadoDetailModel Search(string uuid) {
            return this._DoctosRelacionados.FirstOrDefault<PagosPagoDoctoRelacionadoDetailModel>((PagosPagoDoctoRelacionadoDetailModel p) => p.IdDocumentoDR == uuid);
        }

        /// <summary>
        /// Agregar un documento relacionado siempre y cuando no exista
        /// </summary>
        /// <param name="documento"></param>
        public bool AgregarDocto(PagosPagoDoctoRelacionadoDetailModel documento) {
            if (documento != null) {
                try {
                    var exists = this._DoctosRelacionados.FirstOrDefault<PagosPagoDoctoRelacionadoDetailModel>((PagosPagoDoctoRelacionadoDetailModel p) => p.IdDocumentoDR == documento.IdDocumentoDR);
                    if (exists == null) {
                        this.DoctoRelacionados.Add(documento);
                        return true;
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            return false;
        }

        public void AddIdComprobante(int indice) {
            this.IdComprobanteP=indice;
            if (this.DoctoRelacionados != null) {
                for (int i = 0; i < this.DoctoRelacionados.Count; i++) {
                    this.DoctoRelacionados[i].IdComprobanteP = indice;
                }
            }
        }
    }
}