﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    /// <summary>
    /// clase para la vista de complemento de pago y documentos relacionados
    /// </summary>
    public class ComplementoPagoMergeDoctoModel : ComplementoPagoModel, IComplementoPagoModel, IComplementoPagoMergeDoctoModel, IDataErrorInfo {
        #region declaraciones
        private string idDocumentoField;
        private int numParcialidadField;
        private decimal impSaldoAntField;
        private decimal impPagadoField;
        private decimal impSaldoInsolutoField;
        private string idDocumentoP;
        private string receptorNombreP;
        private string receptorRFCP;
        private string emisorNombreP;
        private string emisorRFCP;
        private DateTime fechaEmisionP;
        private string statusP;
        private string folioP;
        private string serieP;
        private string estadoP;
        private int idComprobanteC;
        private int _IdSubTipo;
        private string tipoComprobanteText;
        #endregion

        public ComplementoPagoMergeDoctoModel() : base() {

        }

        #region informacion del comprobante fiscal de recepcion de pagos
        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get { return this.tipoComprobanteText; }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string SerieP {
            get {
                return this.serieP;
            }
            set {
                this.serieP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string FolioP {
            get {
                return this.folioP;
            }
            set {
                this.folioP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital.", Length = 36)]
        public string IdDocumentoP {
            get {
                return this.idDocumentoP;
            }
            set {
                this.idDocumentoP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// status del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string StatusP {
            get {
                return this.statusP;
            }
            set {
                this.statusP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmisionP {
            get {
                return this.fechaEmisionP;
            }
            set {
                this.fechaEmisionP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFCP {
            get {
                return this.emisorRFCP;
            }
            set {
                this.emisorRFCP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombreP {
            get {
                return this.emisorNombreP;
            }
            set {
                this.emisorNombreP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFCP {
            get {
                return this.receptorRFCP;
            }
            set {
                this.receptorRFCP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombreP {
            get {
                return this.receptorNombreP;
            }
            set {
                this.receptorNombreP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string EstadoP {
            get {
                return this.estadoP;
            }
            set {
                this.estadoP = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region informacion del comprobante relacionado al complemento
        /// <summary>
        /// obtener o establecer el indice del comprobante relacionado al complemento del pago (CFDI)
        /// </summary>
        [DataNames("_cmppgd_sbid")]
        [SugarColumn(ColumnName = "_cmppgd_cfdi_id", ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi)")]
        public int IdComprobanteR {
            get { return this.idComprobanteC; }
            set {
                this.idComprobanteC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cmppgd_doc_id")]
        [SugarColumn(ColumnName = "_cmppgd_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", IsNullable = true, DefaultValue = "0")]
        public int IdSubTipo {
            get { return this._IdSubTipo; }
            set {
                this._IdSubTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
        }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        [DataNames("_cmppgd_uuid")]
        [SugarColumn(ColumnName = "_cmppgd_uuid", ColumnDescription = "identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital.", Length = 36)]
        public string IdDocumentoDR {
            get {
                return this.idDocumentoField;
            }
            set {
                this.idDocumentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        [DataNames("_cmppgd_nmpar")]
        [SugarColumn(ColumnName = "_cmppgd_nmpar", ColumnDescription = "numero de parcialidad que corresponde al pago.", IsNullable = true)]
        public int NumParcialidad {
            get {
                return this.numParcialidadField;
            }
            set {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_sldant")]
        [SugarColumn(ColumnName = "_cmppgd_sldant", ColumnDescription = "saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe contener el importe total del documento relacionado.", Length = 14, DecimalDigits = 4, IsNullable = true)]
        public decimal ImpSaldoAnt {
            get {
                return this.impSaldoAntField;
            }
            set {
                this.impSaldoAntField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_imppgd")]
        [SugarColumn(ColumnName = "_cmppgd_imppgd", ColumnDescription = "importe pagado para el documento relacionado.", Length = 14, DecimalDigits = 4)]
        public decimal ImpPagado {
            get {
                return this.impPagadoField;
            }
            set {
                this.impPagadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        [DataNames("_cmppgd_sldins")]
        [SugarColumn(ColumnName = "_cmppgd_sldins", ColumnDescription = "diferencia entre el importe del saldo anterior y el monto del pago.", Length = 14, DecimalDigits = 4)]
        public decimal ImpSaldoInsoluto {
            get {
                return this.impSaldoInsolutoField;
            }
            set {
                this.impSaldoInsolutoField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region validaciones
        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (this.IdComprobanteR == 0) {
                    return "No se tiene relación con un comprobante.";
                }
                return string.Empty;
            }
        }

        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (this.IdComprobanteR == 0) {
                    return "No se tiene relación con un comprobante.";
                }
                return string.Empty;
            }
        }
        #endregion
    }
}
