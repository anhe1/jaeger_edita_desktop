﻿using System;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using Jaeger.Domain.Abstractions;
// version anterior
namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Complemento para el Comprobante Fiscal Digital por Internet (CFDI) para registrar información sobre la recepción de pagos. El emisor de este complemento para recepción de pagos debe 
    /// ser quien las leyes le obligue a expedir comprobantes por los actos o actividades que realicen, por los ingresos que se perciban o por las retenciones de contribuciones que efectúen.
    /// </summary>
    [JsonObject]
    public class ComplementoPagos : BasePropertyChangeImplementation {
        private BindingList<ComplementoPagosPago> pagoField;
        private PagosTotales totalesField;
        private string versionField;
        private int _IdPago;
        private bool _Activo;

        public ComplementoPagos() {
            this.versionField = "1.0";
        }

        [JsonIgnore]
        public int IdPago {
            get { return this._IdPago; }
            set { this._IdPago = value;
                this.OnPropertyChanged();
            }
        }

        public bool Activo {
            get { return this._Activo; }
            set { this._Activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido que indica la versión del complemento para recepción de pagos
        /// </summary>
        [JsonProperty("ver")]
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("totales")]
        public PagosTotales Totales {
            get { return this.totalesField; }
            set { this.totalesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Elemento requerido para incorporar la información de la recepción de pagos
        /// </summary>
        [JsonProperty("pago")]
        public BindingList<ComplementoPagosPago> Pago {
            get {
                return this.pagoField;
            }
            set {
                this.pagoField = value;
                this.OnPropertyChanged();
            }
        }

        #region Serializacion
        public string Json() {
            return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        }

        public static ComplementoPagos Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoPagos>(inputJson);
            } catch {
                return null;
            }
        }
        #endregion
    }
}

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Elemento requerido para incorporar la información de la recepción de pagos.
    /// </summary>
    public partial class ComplementoPagosPago : BasePropertyChangeImplementation {
        #region declaraciones
        private BindingList<ComplementoPagoDoctoRelacionado> doctoRelacionadoField;
        private BindingList<ComplementoPagoImpuestos> impuestosField;
        private DateTime? fechaPagoField;
        private ComplementoPagoFormaPago formaDePagoPField; //c_FormaPago
        private string monedaPField; //c_Moneda
        private decimal tipoCambioPField;
        private decimal montoField;
        private string numOperacionField;
        private string rfcEmisorCtaOrdField;
        private string nomBancoOrdExtField;
        private string ctaOrdenanteField;
        private string rfcEmisorCtaBenField;
        private string ctaBeneficiarioField;
        private string tipoCadPagoField; //c_TipoCadenaPago
        private string certPagoField; // byte[]
        private string cadPagoField;
        private string selloPagoField; // byte[]
        private bool autoSumaField;
        private decimal totalSaldoSaldoAnteriorField;
        private decimal totalSaldoImportePagadoField;
        private decimal totalSaldoInsolutoField;
        private decimal contadorField;
        #endregion

        public ComplementoPagosPago() {
            this.monedaPField = "MXN";
            this.autoSumaField = false;
            this.FechaPago = DateTime.Now;
            this.formaDePagoPField = new ComplementoPagoFormaPago();
            this.doctoRelacionadoField = new BindingList<ComplementoPagoDoctoRelacionado>() { RaiseListChangedEvents = true };
            this.doctoRelacionadoField.AddingNew += new AddingNewEventHandler(this.DoctoRelacionadoField_AddingNew);
            this.doctoRelacionadoField.ListChanged += new ListChangedEventHandler(this.DoctoRelacionadoField_ListChanged);
        }

        /// <summary>
        /// Atributo requerido para expresar la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        [JsonProperty("fechaPago")]
        public DateTime? FechaPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaPagoField >= firstGoodDate) {
                    return this.fechaPagoField;
                }
                return null;
            }
            set {
                this.fechaPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave de la forma en que se realiza el pago.
        /// </summary>
        [JsonProperty("formaPago")]
        public ComplementoPagoFormaPago FormaDePagoP {
            get {
                return this.formaDePagoPField;
            }
            set {
                this.formaDePagoPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para identificar la clave de la moneda utilizada para realizar el pago, cuando se usa moneda nacional se registra MXN. El atributo Pagos:Pago:Monto y los atributos TotalImpuestosRetenidos, TotalImpuestosTrasladados, Traslados:Traslado:Importe y Retenciones:Retencion:Importe del nodo Pago:Impuestos deben ser expresados en esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [JsonProperty("moneda")]
        public string MonedaP {
            get {
                return this.monedaPField;
            }
            set {
                this.monedaPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        [JsonProperty("tipoCambio")]
        public decimal TipoCambioP {
            get {
                return this.tipoCambioPField;
            }
            set {
                this.tipoCambioPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar el importe del pago
        /// </summary>
        [JsonProperty("monto")]
        public decimal Monto {
            get {
                return Math.Round(this.montoField, 2);
            }
            set {
                this.montoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de cheque, número de autorización, número de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia análogo que identifique la operación que ampara el pago efectuado
        /// </summary>
        [JsonProperty("numOperacion")]
        public string NumOperacion {
            get {
                return this.numOperacionField;
            }
            set {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc., en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty("rfcEmisorCtaOrd")]
        public string RfcEmisorCtaOrd {
            get {
                return this.rfcEmisorCtaOrdField;
            }
            set {
                this.rfcEmisorCtaOrdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago
        /// </summary>
        [JsonProperty("nomBancoOrdExt")]
        public string NomBancoOrdExt {
            get {
                return this.nomBancoOrdExtField;
            }
            set {
                this.nomBancoOrdExtField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para incorporar el número de la cuenta con la que se realizó el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago
        /// </summary>
        [JsonProperty("ctaOrdenante")]
        public string CtaOrdenante {
            get {
                return this.ctaOrdenanteField;
            }
            set {
                this.ctaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty("rfcEmisorCtaBen")]
        public string RfcEmisorCtaBen {
            get {
                return this.rfcEmisorCtaBenField;
            }
            set {
                this.rfcEmisorCtaBenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para incorporar el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty("ctaBeneficiario")]
        public string CtaBeneficiario {
            get {
                return this.ctaBeneficiarioField;
            }
            set {
                this.ctaBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para identificar la clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [JsonProperty("tipoCadPago")]
        public string TipoCadPago {
            get {
                return this.tipoCadPagoField;
            }
            set {
                this.tipoCadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional que sirve para incorporar el certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        [JsonProperty("certPago")]
        public string CertPago {
            get {
                return this.certPagoField;
            }
            set {
                this.certPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago contenga información
        /// </summary>
        [JsonProperty("cadPago")]
        public string CadPago {
            get {
                return this.cadPagoField;
            }
            set {
                this.cadPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para integrar el sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de dicho comprobante, este sello digital es el que se debe registrar en este campo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.
        /// </summary>
        [JsonProperty("selloPago")]
        public string SelloPago {
            get {
                return this.selloPagoField;
            }
            set {
                this.selloPagoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar la lista de documentos relacionados con los pagos diferentes de anticipos. Por cada documento que se relacione se debe generar un nodo DoctoRelacionado.
        /// </summary>
        [JsonProperty("doctoRelacionado")]
        public BindingList<ComplementoPagoDoctoRelacionado> DoctoRelacionado {
            get {
                return this.doctoRelacionadoField;
            }
            set {
                if (this.doctoRelacionadoField != null) {
                    this.doctoRelacionadoField.AddingNew -= new AddingNewEventHandler(this.DoctoRelacionadoField_AddingNew);
                    this.doctoRelacionadoField.ListChanged -= new ListChangedEventHandler(this.DoctoRelacionadoField_ListChanged);
                }
                this.doctoRelacionadoField = value;
                if (this.doctoRelacionadoField != null) {
                    this.doctoRelacionadoField.AddingNew += new AddingNewEventHandler(this.DoctoRelacionadoField_AddingNew);
                    this.doctoRelacionadoField.ListChanged += new ListChangedEventHandler(this.DoctoRelacionadoField_ListChanged);
                }
                this.doctoRelacionadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para expresar el resumen de los impuestos aplicables cuando este documento sea un anticipo
        /// </summary>
        [JsonProperty("impuestos")]
        public BindingList<ComplementoPagoImpuestos> Impuestos {
            get {
                return this.impuestosField;
            }
            set {
                this.impuestosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtiene o establece si el monto total del pago debera actualizarse con la suma de los montos del pago
        /// </summary>
        [JsonIgnore]
        public bool AutoSuma {
            get {
                return this.autoSumaField;
            }
            set {
                this.autoSumaField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public decimal TotalSaldoSaldoAnterior {
            get {
                return this.totalSaldoSaldoAnteriorField;
            }
            set {
                this.totalSaldoSaldoAnteriorField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public decimal TotalSaldoImportePagado {
            get {
                return this.totalSaldoImportePagadoField;
            }
            set {
                this.totalSaldoImportePagadoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public decimal TotalSaldoInsoluto {
            get {
                return this.totalSaldoInsolutoField;
            }
            set {
                this.totalSaldoInsolutoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public decimal Contador {
            get {
                return this.contadorField;
            }
            set {
                this.contadorField = value;
                this.OnPropertyChanged();
            }
        }

        private void DoctoRelacionadoField_AddingNew(object sender, AddingNewEventArgs e) {
            e.NewObject = new ComplementoPagoDoctoRelacionado { ImpPagado = this.Monto };
        }

        private void DoctoRelacionadoField_ListChanged(object sender, ListChangedEventArgs e) {
            if (this.AutoSuma) {
                if (this.Monto > 0) {
                    if (e.ListChangedType == ListChangedType.ItemAdded) {
                        decimal diferencia = this.doctoRelacionadoField.Sum((ComplementoPagoDoctoRelacionado p) => p.ImpPagado);
                        decimal saldoNuevo = this.Monto - diferencia;
                        if (saldoNuevo == 0) {
                            this.doctoRelacionadoField[e.NewIndex].ImpPagado = 0;
                        } else {
                            if (saldoNuevo > this.doctoRelacionadoField[e.NewIndex].ImpSaldoAnt) {
                                this.doctoRelacionadoField[e.NewIndex].ImpPagado = this.doctoRelacionadoField[e.NewIndex].ImpSaldoAnt;
                            } else {
                                this.doctoRelacionadoField[e.NewIndex].ImpPagado = saldoNuevo;
                            }
                        }
                    }
                } else {
                    if (e.ListChangedType == ListChangedType.ItemAdded) {
                        this.doctoRelacionadoField[e.NewIndex].ImpPagado = this.doctoRelacionadoField[e.NewIndex].ImpSaldoAnt;
                    }
                    this.Monto = this.doctoRelacionadoField.Sum((ComplementoPagoDoctoRelacionado p) => p.ImpPagado);
                }
            }
            this.TotalSaldoSaldoAnterior = this.doctoRelacionadoField.Sum((ComplementoPagoDoctoRelacionado p) => p.ImpSaldoAnt);
            this.TotalSaldoImportePagado = this.doctoRelacionadoField.Sum((ComplementoPagoDoctoRelacionado p) => p.ImpPagado);
            this.TotalSaldoInsoluto = this.doctoRelacionadoField.Sum((ComplementoPagoDoctoRelacionado p) => p.ImpSaldoInsoluto);
            this.Contador = this.doctoRelacionadoField.Count();
        }

        #region metodos

        /// <summary>
        /// para buscar en la lista un uuid para evitar incluir dos comprobantes iguales
        /// </summary>
        public ComplementoPagoDoctoRelacionado Buscar(string uuid) {
            return this.doctoRelacionadoField.FirstOrDefault<ComplementoPagoDoctoRelacionado>((ComplementoPagoDoctoRelacionado p) => p.IdDocumento == uuid);
        }

        public string Json() {
            return JsonConvert.SerializeObject(this, 0);
        }

        public static ComplementoPagosPago Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoPagosPago>(inputJson);
            } catch {
                return null;
            }
        }
        #endregion
    }
}

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    [JsonObject("item")]
    public partial class ComplementoPagoFormaPago : BasePropertyChangeImplementation {
        private string claveField;
        private string descripcionField;

        public ComplementoPagoFormaPago() {
        }

        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("desc")]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Nodo requerido para la información detallada de un traslado de impuesto específico.
    /// </summary>
    [JsonObject]
    public partial class ComplementoPagoImpuestosTraslado : BasePropertyChangeImplementation {
        private string impuestoField; //c_Impuesto
        private string tipoFactorField; //c_TipoFactor
        private decimal tasaOCuotaField;
        private decimal importeField;
        private decimal baseField;

        /// <summary>
        /// Atributo requerido para señalar la clave del tipo de impuesto trasladado
        /// </summary>
        [JsonProperty("impuesto")]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto.
        /// </summary>
        [JsonProperty("tipoFactor")]
        public string TipoFactor {
            get {
                return this.tipoFactorField;
            }
            set {
                this.tipoFactorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para señalar el valor de la tasa o cuota del impuesto que se traslada.
        /// </summary>
        [JsonProperty("tasaOCuota")]
        public decimal TasaOCuota {
            get {
                return this.tasaOCuotaField;
            }
            set {
                this.tasaOCuotaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para señalar el importe del impuesto trasladado. No se permiten valores negativos
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("base")]
        public decimal Base {
            get { return this.baseField; }
            set { this.baseField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Nodo condicional para capturar los impuestos retenidos aplicables
    /// </summary>
    [JsonObject]
    public partial class ComplementoPagoImpuestosRetencion : BasePropertyChangeImplementation {
        private string impuestoField; //c_Impuesto
        private decimal importeField;

        /// <summary>
        /// Nodo requerido para registrar la información detallada de una retención de impuesto específico
        /// </summary>
        [JsonProperty("impuesto")]
        public string Impuesto {
            get {
                return this.impuestoField;
            }
            set {
                this.impuestoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para señalar el importe o monto del impuesto retenido. No se permiten valores negativos.
        /// </summary>
        [JsonProperty("importe")]
        public decimal Importe {
            get {
                return this.importeField;
            }
            set {
                this.importeField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Nodo condicional para expresar el resumen de los impuestos aplicables cuando este documento sea un anticipo
    /// </summary>
    [JsonObject]
    public partial class ComplementoPagoImpuestos : BasePropertyChangeImplementation {
        private BindingList<ComplementoPagoImpuestosRetencion> retencionesField;
        private BindingList<ComplementoPagoImpuestosTraslado> trasladosField;
        private decimal totalImpuestosRetenidosField;
        private decimal totalImpuestosTrasladadosField;

        /// <summary>
        /// Nodo condicional para capturar los impuestos retenidos aplicables.
        /// </summary>
        [JsonProperty("retenciones")]
        public BindingList<ComplementoPagoImpuestosRetencion> Retenciones {
            get {
                return this.retencionesField;
            }
            set {
                this.retencionesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo condicional para capturar los impuestos trasladados aplicables.
        /// </summary>
        [JsonProperty("traslados")]
        public BindingList<ComplementoPagoImpuestosTraslado> Traslados {
            get {
                return this.trasladosField;
            }
            set {
                this.trasladosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el total de los impuestos retenidos que se desprenden del pago. No se permiten valores negativos
        /// </summary>
        [JsonProperty("totalImpuestosRetenidos")]
        public decimal TotalImpuestosRetenidos {
            get {
                return this.totalImpuestosRetenidosField;
            }
            set {
                this.totalImpuestosRetenidosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el total de los impuestos trasladados que se desprenden del pago. No se permiten valores negativos.
        /// </summary>
        [JsonProperty("totalImpuestosTrasladados")]
        public decimal TotalImpuestosTrasladados {
            get {
                return this.totalImpuestosTrasladadosField;
            }
            set {
                this.totalImpuestosTrasladadosField = value;
                this.OnPropertyChanged();
            }
        }
    }
}