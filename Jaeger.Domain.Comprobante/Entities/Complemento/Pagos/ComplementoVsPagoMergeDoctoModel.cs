﻿using SqlSugar;
using System;
using System.ComponentModel;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    public class ComplementoVsPagoMergeDoctoModel : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private int idDirectorio;
        private bool activo;
        private int subTipoInt;
        private string tipoComprobanteText;
        private string status;
        private string version;
        private string serie;
        private string folio;
        private string estado;
        private string emisorRFC;
        private string emisorNombre;
        private string receptorRFC;
        private string receptorNombre;
        private string receptorNumRegIdTrib;
        private string receptorResidenciaFiscal;
        private string claveMoneda;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveUsoCFDI;
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado = null;
        private DateTime? fechaCancela = null;
        private DateTime? fechaValidacion = null;
        private decimal retencionIEPS;
        private decimal trasladoIEPS;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal acumulado;
        private decimal importePagado;
        private string fileXML;
        private string filePDF;
        private string lugarExpedicion;
        private decimal retencionISR;
        private decimal retencionIVA;
        private decimal trasladoIVA;
        private string idDocumento;
        private string regimenFiscalReceptor;
        private string domicilioFiscalReceptor;
        private string exportacionField;
        private decimal _TipoCambio;
        private int indice;
        private string versionPField;
        private int idComprobanteP;
        private DateTime? fechaPagoPField;
        private string formaDePagoPField;
        private string monedaPField;
        private decimal tipoCambioPField;
        private decimal montoField;
        private string numOperacionField;
        private string rfcEmisorCtaOrdField;
        private string ctaOrdenanteField;
        private string nomBancoOrdExtField;
        private string rfcEmisorCtaBenField;
        private string ctaBeneficiarioField;
        private int _NumParcialidad;
        private decimal _ImporteSaldoAnterior;
        private decimal _ImportePagado;
        private decimal _ImporteSaldoInsoluto;
        #endregion

        public ComplementoVsPagoMergeDoctoModel() : base() { }

        #region informacion del comprobante de ingreso
        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio {
            get { return this.idDirectorio; }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdSubTipo {
            get { return this.subTipoInt; }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get { return this.tipoComprobanteText; }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get { return this.status; }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cfdi_ver", ColumnDescription = "version del comprobante fiscal", IsNullable = true, Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado {
            get { return this.estado; }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DisplayName("Emisor RFC")]
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre {
            get { return this.emisorNombre; }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante ampara una operación de exportación.
        /// </summary>
        [DataNames("_cfdi_clvexp")]
        [SugarColumn(ColumnName = "_cfdi_clvexp", ColumnDescription = "expresar si el comprobante ampara una operación de exportación.", Length = 2)]
        public string ClaveExportacion {
            get {
                return this.exportacionField;
            }
            set {
                this.exportacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_nmreg", ColumnDescription = "numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get { return this.receptorNumRegIdTrib; }
            set {
                this.receptorNumRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del país de residencia para efectos fiscales del receptor del comprobante, cuando se trate de un extranjero, y que es conforme con la especificación ISO 3166-1 alpha-3. 
        /// Es requerido cuando se incluya el complemento de comercio exterior o se registre el atributo NumRegIdTrib.
        /// </summary>
        [DataNames("_cfdi_resfis")]
        [SugarColumn(ColumnName = "_cfdi_resfis", ColumnDescription = "clave del país de residencia para efectos fiscales del receptor del comprobante", Length = 20, IsNullable = false)]
        public string ResidenciaFiscal {
            get { return this.receptorResidenciaFiscal; }
            set {
                this.receptorResidenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_domfis")]
        [SugarColumn(ColumnName = "_cfdi_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante
        /// </summary>
        [DataNames("_cfdi_rgfsc")]
        [SugarColumn(ColumnName = "_cfdi_rgfsc", ColumnDescription = "clave de regimen fiscal", Length = 3, IsNullable = false)]
        public string ClaveRegimenFiscal {
            get { return this.regimenFiscalReceptor; }
            set {
                this.regimenFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio FIX conforme con la moneda usada. Es requerido cuando la clave de moneda es distinta de MXN y de XXX. El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad 
        /// de la divisa señalada en el atributo moneda. Si el valor está fuera del porcentaje aplicable a la moneda tomado del catálogo c_Moneda, el emisor debe obtener del PAC que vaya a timbrar el CFDI, de manera no automática, 
        /// una clave de confirmación para ratificar que el valor es correcto e integrar dicha clave en el atributo Confirmacion.
        /// </summary>
        [DataNames("_cfdi_tcam")]
        [SugarColumn(ColumnName = "_cfdi_tcam", ColumnDescription = "tipo de cambio FIX conforme con la moneda usada", Length = 255, IsNullable = false)]
        public decimal TipoCambio {
            get { return this._TipoCambio; }
            set {
                this._TipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A 
        /// fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Articulo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la 
        /// forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [DataNames("_cfdi_usocfdi")]
        [SugarColumn(ColumnName = "_cfdi_usocfdi", ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3)]
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get { return this.fechaEmision; }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecval")]
        [SugarColumn(ColumnName = "_cfdi_fecval", ColumnDescription = "fecha de validacion en el caso de los comprobantes recibidos", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion >= firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                this.fechaValidacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        [DataNames("_cfdi_lgrexp")]
        [SugarColumn(ColumnName = "_cfdi_lgrexp", ColumnDescription = "indicar llugar de expedición del comprobante fiscal", Length = 50, IsNullable = true)]
        public string LugarExpedicion {
            get { return this.lugarExpedicion; }
            set {
                this.lugarExpedicion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr", ColumnDescription = "importe total del impuesto retenido ISR", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get { return this.retencionISR; }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva", ColumnDescription = "importe total del impuesto retenido IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get { return this.retencionIVA; }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva", ColumnDescription = "importe total del impuesto trasladado IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps", ColumnDescription = "importe total del impuesto retenido IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps", ColumnDescription = "importe total del impuesto trasladado IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get { return this.trasladoIEPS; }
            set {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl", ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt", ColumnDescription = "para representar el importe de los descuentos aplicables antes de impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get { return this.acumulado; }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        [DataNames("_cfdi_cbrdp")]
        [SugarColumn(ColumnName = "_cfdi_cbrdp", ColumnDescription = "monto cobrado o pagado con respecto a los comprobantes de pago", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImportePagado {
            get { return this.importePagado; }
            set {
                this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnName = "_cfdi_url_xml", ColumnDescription = "url de descarga para el archivo xml", ColumnDataType = "Text")]
        public string UrlFileXML {
            get { return this.fileXML; }
            set {
                this.fileXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnName = "_cfdi_url_pdf", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get { return this.filePDF; }
            set {
                this.filePDF = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region informacion del complemento de pagos asociado al comprobante de ingreso
        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        [DataNames("idDocumentoP")]
        public string IdDocumentoPG { get; set; }

        /// <summary>
        /// obtener o establecer indice de la tabla de pagos
        /// </summary>
        [DataNames("_cmppg_id")]
        [SugarColumn(ColumnName = "_cmppg_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdPago {
            get { return this.indice; }
            set {
                this.indice = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer registro activo
        /// </summary>
        [DataNames("_cmppg_a")]
        [SugarColumn(ColumnName = "_cmppg_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la versión del complemento para recepción de pagos.
        /// </summary>
        [DataNames("_cmppg_ver")]
        [SugarColumn(ColumnName = "_cmppg_ver", ColumnDescription = "Version del complemento de pagos", DefaultValue = "2.0", Length = 5)]
        public string VersionP {
            get {
                return this.versionPField;
            }
            set {
                this.versionPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer indice de relacion con el comprobante de pagos (cfdi) al que pertenece este complemento
        /// </summary>
        [DataNames("_cmppg_cfd_id")]
        [SugarColumn(ColumnName = "_cmppg_cfd_id", ColumnDescription = "indice de relacion del comprobante fiscal")]
        public int IdComprobanteP {
            get { return this.idComprobanteP; }
            set {
                this.idComprobanteP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la 
        /// especificación ISO 8601.En caso de no contar con la hora se debe registrar 12:00:00.
        /// </summary>
        [DataNames("_cmppg_fecpg")]
        [SugarColumn(ColumnName = "_cmppg_fecpg", ColumnDescription = "fecha y hora en la que el beneficiario recibe el pago")]
        public DateTime? FechaPagoP {
            get { if (this.fechaPagoPField >= new DateTime(1900, 1, 1))
                return this.fechaPagoPField;
                return null;
            }
            set {
                this.fechaPagoPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        [DataNames("_cmppg_frmpg")]
        [SugarColumn(ColumnName = "_cmppg_frmpg", ColumnDescription = "clave de forma en que se realiza el pago", Length = 50)]
        public string FormaDePagoP {
            get {
                return this.formaDePagoPField;
            }
            set {
                this.formaDePagoPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para realizar el pago conforme a la especificación ISO 4217. Cuando se usa moneda nacional se registra MXN. 
        /// El atributo Pagos:Pago:Monto debe ser expresado en la moneda registrada en este atributo.
        /// </summary>
        [DataNames("_cmppg_mndp")]
        [SugarColumn(ColumnName = "_cmppg_mndp", ColumnDescription = "clave de moneda", Length = 5)]
        public string MonedaP {
            get {
                return this.monedaPField;
            }
            set {
                this.monedaPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una 
        /// unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.
        /// </summary>
        [DataNames("_cmppg_tpcmb")]
        [SugarColumn(ColumnName = "_cmppg_tpcmb", ColumnDescription = "tipo de cambio de la moneda a la fecha en que se realizo el pago", DecimalDigits = 6, DefaultValue = "1")]
        public decimal TipoCambioP {
            get {
                return this.tipoCambioPField;
            }
            set {
                this.tipoCambioPField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe del pago.
        /// </summary>
        [DataNames("_cmppg_monto")]
        [SugarColumn(ColumnName = "_cmppg_monto", ColumnDescription = "importe del pago", Length = 18, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Monto {
            get {
                return this.montoField;
            }
            set {
                this.montoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia 
        /// análogo que identifique la operación que ampara el pago efectuado.
        /// </summary>
        [DataNames("_cmppg_nmope")]
        [SugarColumn(ColumnName = "_cmppg_nmope", ColumnDescription = "numero de cheque, numero de autorización, numero de referencia, clave de rastreo en caso de ser SPEI, linea de captura o algun numero de referencia", Length = 100, IsNullable = true)]
        public string NumOperacion {
            get {
                return this.numOperacionField;
            }
            set {
                this.numOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc.,
        /// en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("_cmppg_rfcems")]
        [SugarColumn(ColumnName = "_cmppg_rfcems", ColumnDescription = "RFC de la entidad emisora de la cuenta de origen, es decir, la peradora, el banco, etc.", Length = 13, IsNullable = true)]
        public string RfcEmisorCtaOrd {
            get {
                return this.rfcEmisorCtaOrdField;
            }
            set {
                this.rfcEmisorCtaOrdField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de 
        /// acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("_cmppg_nmbnc")]
        [SugarColumn(ColumnName = "_cmppg_nmbnc", ColumnDescription = "nombre del banco ordenante", Length = 300, IsNullable = true)]
        public string NomBancoOrdExt {
            get {
                return this.nomBancoOrdExtField;
            }
            set {
                this.nomBancoOrdExtField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de la cuenta con la que se realizo el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con 
        /// el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("_cmppg_ctord")]
        [SugarColumn(ColumnName = "_cmppg_ctord", ColumnDescription = "numero de la cuenta con la que se realizo el pago", Length = 50, IsNullable = true)]
        public string CtaOrdenante {
            get {
                return this.ctaOrdenanteField;
            }
            set {
                this.ctaOrdenanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. 
        /// Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("_cmppg_rfcb")]
        [SugarColumn(ColumnName = "_cmppg_rfcben", ColumnDescription = "RFC de la entidad operadora de la cuenta destino", Length = 13, IsNullable = true)]
        public string RfcEmisorCtaBen {
            get {
                return this.rfcEmisorCtaBenField;
            }
            set {
                this.rfcEmisorCtaBenField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo 
        /// con el catálogo catCFDI:c_FormaPago.
        /// </summary>
        [DataNames("_cmppg_ctben")]
        [SugarColumn(ColumnName = "_cmppg_ctben", ColumnDescription = "numero de cuenta en donde se recibio el pago.", Length = 20, IsNullable = true)]
        public string CtaBeneficiario {
            get {
                return this.ctaBeneficiarioField;
            }
            set {
                this.ctaBeneficiarioField = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        [DataNames("_cmppgd_nmpar")]
        [SugarColumn(ColumnName = "_cmppgd_nmpar", ColumnDescription = "numero de parcialidad que corresponde al pago.", IsNullable = true)]
        public int NumParcialidad {
            get {
                return this._NumParcialidad;
            }
            set {
                this._NumParcialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_sldant")]
        [SugarColumn(ColumnName = "_cmppgd_sldant", ColumnDescription = "saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe contener el importe total del documento relacionado.", Length = 14, DecimalDigits = 4, IsNullable = true, DefaultValue = "0")]
        public decimal ImpSaldoAnt {
            get {
                return this._ImporteSaldoAnterior;
            }
            set {
                this._ImporteSaldoAnterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_imppgd")]
        [SugarColumn(ColumnName = "_cmppgd_imppgd", ColumnDescription = "importe pagado para el documento relacionado.", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal ImpPagado {
            get {
                return this._ImportePagado;
            }
            set {
                this._ImportePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        [DataNames("_cmppgd_sldins")]
        [SugarColumn(ColumnName = "_cmppgd_sldins", ColumnDescription = "diferencia entre el importe del saldo anterior y el monto del pago.", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImpSaldoInsoluto {
            get {
                return this._ImporteSaldoInsoluto;
            }
            set {
                this._ImporteSaldoInsoluto = value;
                this.OnPropertyChanged();
            }
        }
    }
}
