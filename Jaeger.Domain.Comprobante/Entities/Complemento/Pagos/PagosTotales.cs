﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    [JsonObject]
    public class PagosTotales : BasePropertyChangeImplementation {
        #region declaraciones
        private decimal totalRetencionesIVAField;
        private bool totalRetencionesIVAFieldSpecified;
        private decimal totalRetencionesISRField;
        private bool totalRetencionesISRFieldSpecified;
        private decimal totalRetencionesIEPSField;
        private bool totalRetencionesIEPSFieldSpecified;
        private decimal totalTrasladosBaseIVA16Field;
        private bool totalTrasladosBaseIVA16FieldSpecified;
        private decimal totalTrasladosImpuestoIVA16Field;
        private bool totalTrasladosImpuestoIVA16FieldSpecified;
        private decimal totalTrasladosBaseIVA8Field;
        private bool totalTrasladosBaseIVA8FieldSpecified;
        private decimal totalTrasladosImpuestoIVA8Field;
        private bool totalTrasladosImpuestoIVA8FieldSpecified;
        private decimal totalTrasladosBaseIVA0Field;
        private bool totalTrasladosBaseIVA0FieldSpecified;
        private decimal totalTrasladosImpuestoIVA0Field;
        private bool totalTrasladosImpuestoIVA0FieldSpecified;
        private decimal totalTrasladosBaseIVAExentoField;
        private bool totalTrasladosBaseIVAExentoFieldSpecified;
        private decimal montoTotalPagosField;
        #endregion

        /// <remarks/>
        [JsonProperty("retencionesIVA")]
        public decimal TotalRetencionesIVA {
            get {
                return this.totalRetencionesIVAField;
            }
            set {
                this.totalRetencionesIVAField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalRetencionesIVASpecified {
            get {
                return this.totalRetencionesIVAFieldSpecified;
            }
            set {
                this.totalRetencionesIVAFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("retencionesISR")]
        public decimal TotalRetencionesISR {
            get {
                return this.totalRetencionesISRField;
            }
            set {
                this.totalRetencionesISRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalRetencionesISRSpecified {
            get {
                return this.totalRetencionesISRFieldSpecified;
            }
            set {
                this.totalRetencionesISRFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("retencionesIEPS")]
        public decimal TotalRetencionesIEPS {
            get {
                return this.totalRetencionesIEPSField;
            }
            set {
                this.totalRetencionesIEPSField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalRetencionesIEPSSpecified {
            get {
                return this.totalRetencionesIEPSFieldSpecified;
            }
            set {
                this.totalRetencionesIEPSFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosBaseIVA16")]
        public decimal TotalTrasladosBaseIVA16 {
            get {
                return this.totalTrasladosBaseIVA16Field;
            }
            set {
                this.totalTrasladosBaseIVA16Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty]
        public bool TotalTrasladosBaseIVA16Specified {
            get {
                return this.totalTrasladosBaseIVA16FieldSpecified;
            }
            set {
                this.totalTrasladosBaseIVA16FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosImpuestoIVA16")]
        public decimal TotalTrasladosImpuestoIVA16 {
            get {
                return this.totalTrasladosImpuestoIVA16Field;
            }
            set {
                this.totalTrasladosImpuestoIVA16Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosImpuestoIVA16Specified {
            get {
                return this.totalTrasladosImpuestoIVA16FieldSpecified;
            }
            set {
                this.totalTrasladosImpuestoIVA16FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosBaseIVA8")]
        public decimal TotalTrasladosBaseIVA8 {
            get {
                return this.totalTrasladosBaseIVA8Field;
            }
            set {
                this.totalTrasladosBaseIVA8Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosBaseIVA8Specified {
            get {
                return this.totalTrasladosBaseIVA8FieldSpecified;
            }
            set {
                this.totalTrasladosBaseIVA8FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosImpuestoIVA8")]
        public decimal TotalTrasladosImpuestoIVA8 {
            get {
                return this.totalTrasladosImpuestoIVA8Field;
            }
            set {
                this.totalTrasladosImpuestoIVA8Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosImpuestoIVA8Specified {
            get {
                return this.totalTrasladosImpuestoIVA8FieldSpecified;
            }
            set {
                this.totalTrasladosImpuestoIVA8FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosBaseIVA0 ")]
        public decimal TotalTrasladosBaseIVA0 {
            get {
                return this.totalTrasladosBaseIVA0Field;
            }
            set {
                this.totalTrasladosBaseIVA0Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosBaseIVA0Specified {
            get {
                return this.totalTrasladosBaseIVA0FieldSpecified;
            }
            set {
                this.totalTrasladosBaseIVA0FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosImpuestoIVA0")]
        public decimal TotalTrasladosImpuestoIVA0 {
            get {
                return this.totalTrasladosImpuestoIVA0Field;
            }
            set {
                this.totalTrasladosImpuestoIVA0Field = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosImpuestoIVA0Specified {
            get {
                return this.totalTrasladosImpuestoIVA0FieldSpecified;
            }
            set {
                this.totalTrasladosImpuestoIVA0FieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("trasladosBaseIVAExento")]
        public decimal TotalTrasladosBaseIVAExento {
            get {
                return this.totalTrasladosBaseIVAExentoField;
            }
            set {
                this.totalTrasladosBaseIVAExentoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonIgnore]
        public bool TotalTrasladosBaseIVAExentoSpecified {
            get {
                return this.totalTrasladosBaseIVAExentoFieldSpecified;
            }
            set {
                this.totalTrasladosBaseIVAExentoFieldSpecified = value;
                this.OnPropertyChanged();
            }
        }

        /// <remarks/>
        [JsonProperty("montoTotalPagos")]
        public decimal MontoTotalPagos {
            get {
                return this.montoTotalPagosField;
            }
            set {
                this.montoTotalPagosField = value;
                this.OnPropertyChanged();
            }
        }
    }
}