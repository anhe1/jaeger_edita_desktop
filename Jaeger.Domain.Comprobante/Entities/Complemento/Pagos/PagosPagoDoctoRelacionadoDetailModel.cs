﻿using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    /// <summary>
    /// Documento relacionado del complemento de pagos
    /// </summary>
    [SugarTable("_cmppgd", "CFDI complemento pagos: documento relacionado")]
    public class PagosPagoDoctoRelacionadoDetailModel : PagosPagoDoctoRelacionadoModel, IPagosPagoDoctoRelacionadoDetailModel, IDataErrorInfo {

        public PagosPagoDoctoRelacionadoDetailModel() : base() {
        }

        [SugarColumn(IsIgnore = true)]
        public CFDIObjetoImpuestoEnum ObjetoDeImpuesto {
            get {
                if (base.ObjetoImpDR == "01")
                    return CFDIObjetoImpuestoEnum.No_objeto_de_impuesto;
                else if (base.ObjetoImpDR == "02")
                    return CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto;
                return CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto_y;
            }
            set {
                if (value == CFDIObjetoImpuestoEnum.No_objeto_de_impuesto)
                    this.ObjetoImpDR = "01";
                else if (value == CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto)
                    this.ObjetoImpDR = "02";
                else if (value == CFDIObjetoImpuestoEnum.Si_objeto_de_impuesto_y)
                    this.ObjetoImpDR = "03";
                this.OnPropertyChanged();
            }
        }

        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string this[string columnName] {
            get {
                if (this.IdComprobanteR == 0) {
                    return "No se tiene relación con un comprobante.";
                }

                if (columnName == "ImpPagado" && this.ImpPagado == 0) {
                    return "El importe pagado del comprobante no puede ser cero";
                }

                if (columnName == "ImpSaldoAnt" && this.ImpSaldoAnt > this.Total) {
                    return "El importe del saldo anterior supera el total del comprobante";
                }

                if (columnName == "ImpPagado" && this.ImpPagado > this.Total) {
                    return "El importe del pagado supera el total del comprobante";
                }
                return string.Empty;
            }
        }

        [Browsable(false)]
        [SugarColumn(IsIgnore = true)]
        public string Error {
            get {
                if (this.IdComprobanteR == 0 | this.ImpPagado == 0) {
                    if (this.IdComprobanteR == 0) {
                        return "No se tiene relación con un comprobante.";
                    }

                    if (this.ImpPagado == 0) {
                        return "El importe pagado del comprobante no puede ser cero";
                    }

                    if (this.ImpSaldoAnt > this.Total) {
                        return "El importe del saldo anterior supera el total del comprobante";
                    }

                    if (this.ImpPagado > this.Total) {
                        return "El importe del pagado supera el total del comprobante";
                    }
                }
                return string.Empty;
            }
        }
    }
}
