﻿using Newtonsoft.Json;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    [JsonObject]
    public partial class PagosPagoDoctoRelacionadoImpuestosDRRetencionDR : Abstractions.TipoImpuestoModel, IPagosPagoDoctoRelacionadoImpuestosDRRetencionDR {
        public PagosPagoDoctoRelacionadoImpuestosDRRetencionDR() {
            this.Tipo = ValueObjects.TipoImpuestoEnum.Retencion;
        }
    }
}
