﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    /// <summary>
    /// Documento relacionado del complemento de pagos del comprobante fiscal
    /// </summary>
    [SugarTable("_cmppgd", "CFDI complemento pagos: documento relacionado")]
    public class PagosPagoDoctoRelacionadoModel : Base.Abstractions.BasePropertyChangeImplementation, IPagosPagoDoctoRelacionadoModel {
        #region declaraciones
        private int _IdRelacion;
        private bool _RegistroActivo;
        private int _IdComprobanteR;
        private string _IdDocumento;
        private string _Serie;
        private string _Folio;
        private string _MonedaDR;
        private decimal _EquivalenciaDR;
        private int _NumParcialidad;
        private decimal _ImporteSaldoAnterior;
        private decimal _ImportePagado;
        private decimal _ImporteSaldoInsoluto;
        private decimal _Total;
        private string _ObjetoImpDR;
        private BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR> _Retenciones;
        private BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR> _Traslados;
        private int _IdPago;
        private string _RFC;
        private string _Nombre;
        private DateTime _FechaEmision;
        private string _FormaDePagoP;
        private string _MetodoPago;
        private int _IdSubTipo;
        private int _IdComprobanteP;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public PagosPagoDoctoRelacionadoModel() {
            this.Traslados = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR>() { RaiseListChangedEvents = true };
            this.Traslados.ListChanged += Traslados_ListChanged;
            this.Traslados.AddingNew += Traslados_AddingNew;
            this.Retenciones = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR>() { RaiseListChangedEvents = true };
            this.Retenciones.ListChanged += Retenciones_ListChanged;
            this.Retenciones.AddingNew += Retenciones_AddingNew;
            this.Activo = true;
        }

        /// <summary>
        /// obtener o establecer el indice de relacion de la tabla
        /// </summary>
        [DataNames("_cmppgd_id")]
        [SugarColumn(ColumnName = "_cmppgd_id", ColumnDescription = "indice de la tabla", IsPrimaryKey = true, IsIdentity = true)]
        public int IdRelacion {
            get { return this._IdRelacion; }
            set { this._IdRelacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obener o establecer registro activo
        /// </summary>
        [DataNames("_cmppgd_a")]
        [SugarColumn(ColumnName = "_cmppgd_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this._RegistroActivo; }
            set { this._RegistroActivo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con la tabla de pagos
        /// </summary>
        [DataNames("_cmppgd_idpg")]
        [SugarColumn(ColumnName = "_cmppgd_idpg", ColumnDescription = "indice de relacion con la tabla de pagos", DefaultValue = "0")]
        public int IdPago {
            get { return this._IdPago; }
            set { this._IdPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del comprobante fiscal de pagos
        /// </summary>
        [DataNames("_cmppgd_sbid")]
        [SugarColumn(ColumnName = "_cmppgd_sbid", ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi) del comprobante de pago")]
        public int IdComprobanteP {
            get { return this._IdComprobanteP; }
            set {
                this._IdComprobanteP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cmppgd_doc_id")]
        [SugarColumn(ColumnName = "_cmppgd_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", IsNullable = true, DefaultValue = "0")]
        public int IdSubTipo {
            get { return this._IdSubTipo; }
            set {
                this._IdSubTipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del comprobante relacionado al complemento del pago (CFDI) (_cmppgd_cfdi_id)
        /// </summary>
        [DataNames("_cmppgd_sbid")]
        [SugarColumn(ColumnName = "_cmppgd_cfdi_id", ColumnDescription = "indice de relacion con la tabla de comprobantes fiscales (_cfdi)")]
        public int IdComprobanteR {
            get { return this._IdComprobanteR; }
            set { this._IdComprobanteR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien 
        /// el numero de operacion de un documento digital.
        /// </summary>
        [DataNames("_cmppgd_uuid")]
        [SugarColumn(ColumnName = "_cmppgd_uuid", ColumnDescription = "identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electronica o bien el numero de operacion de un documento digital.", Length = 36)]
        public string IdDocumentoDR {
            get {
                return this._IdDocumento;
            }
            set {
                this._IdDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [DataNames("_cmppgd_serie")]
        [SugarColumn(ColumnName = "_cmppgd_serie", ColumnDescription = "serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.", Length = 25, IsNullable = true)]
        public string SerieDR {
            get {
                return this._Serie;
            }
            set {
                this._Serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.
        /// </summary>
        [DataNames("_cmppgd_folio")]
        [SugarColumn(ColumnName = "_cmppgd_folio", ColumnDescription = "folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.", Length = 40, IsNullable = true)]
        public string Folio {
            get {
                return this._Folio;
            }
            set {
                this._Folio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [DataNames("_cmppgd_rfc")]
        [SugarColumn(ColumnName = "_cmppgd_rfc", ColumnDescription = "rfc receptor del comprobante, esta es informacion adicional no se incluye en el complemento..", Length = 14, IsNullable = true)]
        public string RFC {
            get {
                return this._RFC;
            }
            set {
                this._RFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del receptor del comprobante, esta es informacion adicional no se incluye en el complemento.
        /// </summary>
        [DataNames("_cmppgd_nom")]
        [SugarColumn(ColumnName = "_cmppgd_nom", ColumnDescription = "receptor del comprobante, esta es informacion adicional no se incluye en el complemento.", Length = 255, IsNullable = true)]
        public string Nombre {
            get {
                return this._Nombre;
            }
            set {
                this._Nombre = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cmppgd_fecems")]
        [SugarColumn(ColumnName = "_cmppgd_fecems", ColumnDescription = "fecha de emision del comprobante", IsNullable = true)]
        public DateTime FechaEmision {
            get {
                return this._FechaEmision;
            }
            set {
                this._FechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la forma en que se realiza el pago.
        /// </summary>
        [DataNames("_cmppgd_frmpg")]
        [SugarColumn(ColumnName = "_cmppgd_frmpg", ColumnDescription = "clave de forma en que se realiza el pago", Length = 50, IsNullable = true)]
        public string FormaDePagoP {
            get {
                return this._FormaDePagoP;
            }
            set {
                this._FormaDePagoP = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para expresar la clave del método de pago que se registró en el documento relacionado
        /// </summary>
        [DataNames("_cmppgd_mtdpg")]
        [SugarColumn(ColumnName = "_cmppgd_mtdpg", ColumnDescription = "clave del método de pago que se registró en el documento relacionado", Length = 50, IsNullable = true)]
        public string MetodoPago {
            get {
                return this._MetodoPago;
            }
            set {
                this._MetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento 
        /// relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto”
        /// de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cmppgd_clvmnd")]
        [SugarColumn(ColumnName = "_cmppgd_clvmnd", ColumnDescription = "clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado", Length = 5, IsNullable = true)]
        public string MonedaDR {
            get {
                return this._MonedaDR;
            }
            set {
                this._MonedaDR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento 
        /// relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que 
        /// equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD. El pago se realiza por 100 EUR. Este 
        /// atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD.
        /// </summary>
        [DataNames("_cmppgd_equi")]
        [SugarColumn(ColumnName = "_cmppgd_equi", ColumnDescription = "tipo de cambio conforme con la moneda registrada en el documento relacionado.", DecimalDigits = 6, IsNullable = true)]
        public decimal EquivalenciaDR {
            get {
                return this._EquivalenciaDR;
            }
            set {
                this._EquivalenciaDR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el numero de parcialidad que corresponde al pago.
        /// </summary>
        [DataNames("_cmppgd_nmpar")]
        [SugarColumn(ColumnName = "_cmppgd_nmpar", ColumnDescription = "numero de parcialidad que corresponde al pago.", IsNullable = true)]
        public int NumParcialidad {
            get {
                return this._NumParcialidad;
            }
            set {
                this._NumParcialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe total del comprobante relacionado
        /// </summary>
        [DataNames("_cmppgd_total")]
        [SugarColumn(ColumnName = "_cmppgd_total", ColumnDescription = "importe total del comprobante", IsNullable = true, DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this._Total; }
            set { this._Total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer monto del saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe 
        /// contener el importe total del documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_sldant")]
        [SugarColumn(ColumnName = "_cmppgd_sldant", ColumnDescription = "saldo insoluto de la parcialidad anterior. En el caso de que sea la primer parcialidad este atributo debe contener el importe total del documento relacionado.", Length = 14, DecimalDigits = 4, IsNullable = true, DefaultValue = "0")]
        public decimal ImpSaldoAnt {
            get {
                return this._ImporteSaldoAnterior;
            }
            set {
                this._ImporteSaldoAnterior = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer importe pagado para el documento relacionado.
        /// </summary>
        [DataNames("_cmppgd_imppgd")]
        [SugarColumn(ColumnName = "_cmppgd_imppgd", ColumnDescription = "importe pagado para el documento relacionado.", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal ImpPagado {
            get {
                return this._ImportePagado;
            }
            set {
                this._ImportePagado = value;
                foreach (var item in this.Traslados) {
                    item.Base = value;
                }
                foreach (var item in this.Retenciones) {
                    item.Base = value;
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer diferencia entre el importe del saldo anterior y el monto del pago.
        /// </summary>
        [DataNames("_cmppgd_sldins")]
        [SugarColumn(ColumnName = "_cmppgd_sldins", ColumnDescription = "diferencia entre el importe del saldo anterior y el monto del pago.", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImpSaldoInsoluto {
            get {
                return this._ImporteSaldoInsoluto;
            }
            set {
                this._ImporteSaldoInsoluto = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el pago del documento relacionado es objeto o no de impuesto.
        /// </summary>
        [DataNames("_cmppgd_clvobj")]
        [SugarColumn(ColumnName = "_cmppgd_clvobj", ColumnDescription = "el pago del documento relacionado es objeto o no de impuesto.", Length = 3, IsNullable = true)]
        public string ObjetoImpDR {
            get {
                return this._ObjetoImpDR;
            }
            set {
                this._ObjetoImpDR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objeto json con información de los impuestos retenciones aplicables
        /// </summary>
        [DataNames("_cmppgd_jret")]
        [SugarColumn(ColumnName = "_cmppgd_jret", ColumnDescription = "objeto json con información de los impuestos retenciones aplicables", ColumnDataType = "Text", IsNullable = true)]
        public string JRetenciones {
            get { if (this.Retenciones != null)
                    if (this.Retenciones.Count > 0)
                        return JsonConvert.SerializeObject(this.Retenciones);
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.Retenciones = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR>(JsonConvert.DeserializeObject<BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR>>(value));
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer objeto json con información de los impuestos traslados aplicables
        /// </summary>
        [DataNames("_cmppgd_jtra")]
        [SugarColumn(ColumnName = "_cmppgd_jtra", ColumnDescription = "objeto json con información de los impuestos traslados aplicables", ColumnDataType = "Text", IsNullable = true)]
        public string JTraslados {
            get {
                if (this.Traslados != null)
                    if (this.Traslados.Count > 0)
                        return JsonConvert.SerializeObject(this.Traslados);
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.Traslados = new BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR>(JsonConvert.DeserializeObject<BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR>>(value));
                    this.OnPropertyChanged();
                }
            }
        }

        #region propiedades ignoradas
        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SaldoInsoluto {
            get { return this.ImpSaldoAnt - this.ImpPagado; }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<PagosPagoDoctoRelacionadoImpuestosDRRetencionDR> Retenciones {
            get { if (this._Retenciones != null)
                    return this._Retenciones;
                return null;
            }
            set {
                if (value != null) {
                    this._Retenciones = value;
                    this.OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        [SugarColumn(IsIgnore = true)]
        public BindingList<PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR> Traslados {
            get { if (this._Traslados != null)
                    return this._Traslados;
                return null;
            }
            set {
                if (value != null) {
                    this._Traslados = value;
                    this.OnPropertyChanged();
                }
            }
        }
        #endregion

        #region metodos
        private void Retenciones_AddingNew(object sender, AddingNewEventArgs e) {
            //e.NewObject = new PagosPagoDoctoRelacionadoImpuestosDRRetencionDR() { Base = this.ImpPagado };
        }

        private void Retenciones_ListChanged(object sender, ListChangedEventArgs e) {

        }

        private void Traslados_AddingNew(object sender, AddingNewEventArgs e) {
            //e.NewObject = new PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR() { Base = this.ImpPagado };
        }

        private void Traslados_ListChanged(object sender, ListChangedEventArgs e) {

        }
        #endregion
    }
}
