﻿using Newtonsoft.Json;
using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities.Complemento.Pagos {
    [JsonObject]
    public partial class PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR : Abstractions.TipoImpuestoModel, IPagosPagoDoctoRelacionadoImpuestosDRTrasladoDR {
        public PagosPagoDoctoRelacionadoImpuestosDRTrasladoDR() {
            this.Tipo = ValueObjects.TipoImpuestoEnum.Traslado;
            this.TipoFactor = ValueObjects.FactorEnum.Tasa;
            this.Impuesto = Domain.Base.ValueObjects.ImpuestoEnum.IVA;
        }
    }
}
