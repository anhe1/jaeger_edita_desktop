using System;
using Newtonsoft.Json;
// version anterior
namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Nodo condicional para expresar la lista de documentos relacionados con los pagos diferentes de anticipos. Por cada documento que se relacione se debe generar un nodo DoctoRelacionado.
    /// </summary>
    public class ComplementoPagoDoctoRelacionado : ComplementoDoctoRelacionadoBase {
        #region declaraciones
        private int numParcialidadField;
        private decimal impSaldoAntField;
        private decimal impPagadoField;
        private decimal impSaldoInsolutoField;
        private int precisonDecimalField;
        private bool autoCalcularField;
        private string objetoImpDRField;
        private decimal equivalenciaDRField;
        #endregion

        public ComplementoPagoDoctoRelacionado() {
            this.precisonDecimalField = 2;
            this.autoCalcularField = false;
        }

        [JsonProperty("equivalenciaDR")]
        public decimal EquivalenciaDR {
            get {
                return this.equivalenciaDRField;
            }
            set {
                this.equivalenciaDRField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("numParcialidad")]
        public int NumParcialidad {
            get {
                return this.numParcialidadField;
            }
            set {
                this.numParcialidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el monto del saldo insoluto de la parcialidad anterior. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.En el caso de que sea la primer parcialidad este campo debe contener el importe total del documento relacionado.
        /// </summary>
        [JsonProperty("impSaldoAnt")]
        public decimal ImpSaldoAnt {
            get {
                return Math.Round(this.impSaldoAntField, this.precisonDecimalField);
            }
            set {
                this.impSaldoAntField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar el importe pagado para el documento relacionado. Es obligatorio cuando exista más de un documento relacionado o cuando existe un documento relacionado y el TipoCambioDR tiene un valor.
        /// </summary>
        [JsonProperty("impPagado")]
        public decimal ImpPagado {
            get {
                return Math.Round(this.impPagadoField, this.precisonDecimalField);
            }
            set {
                this.impPagadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo condicional para expresar la diferencia entre el importe del saldo anterior y el monto del pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.
        /// </summary>
        [JsonProperty("impSaldoInsoluto")]
        public decimal ImpSaldoInsoluto {
            get {
                if (autoCalcularField) {
                    return Math.Round((this.impSaldoAntField - this.impPagadoField), this.precisonDecimalField);//impSaldoInsolutoField;
                }
                else {
                    return this.impSaldoInsolutoField;
                }
            }
            set {
                this.impSaldoInsolutoField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("objetoImpDR")]
        public string ObjetoImpDR {
            get {
                return this.objetoImpDRField;
            }
            set {
                this.objetoImpDRField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("precision")]
        public int PrecisionDecimal {
            get {
                return this.precisonDecimalField;
            }
            set {
                this.precisonDecimalField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public bool AutoCalcular {
            get {
                return this.autoCalcularField;
            }
            set {
                this.autoCalcularField = value;
                this.OnPropertyChanged();
            }
        }
    }
}