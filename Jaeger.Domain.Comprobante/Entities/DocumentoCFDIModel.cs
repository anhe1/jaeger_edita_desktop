﻿using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class DocumentoCFDIModel : Domain.Base.Abstractions.BaseSingleTipoModel {
        private CFDITipoComprobanteEnum _tipoComprobanteEnum;

        public DocumentoCFDIModel() {
            this.Concepto = null;
        }

        public DocumentoCFDIModel(string id, string descripcion, CFDITipoComprobanteEnum tipoComprobanteEnum) : base(id, descripcion) {
            this.TipoComprobante = tipoComprobanteEnum;
            this.Concepto = null;
        }

        public CFDITipoComprobanteEnum TipoComprobante {
            get { return this._tipoComprobanteEnum; }
            set {
                this._tipoComprobanteEnum = value;
                this.OnPropertyChanged();
            }
        }

        public ComprobanteConceptoDetailModel Concepto {
            get;set;
        }
    }
}