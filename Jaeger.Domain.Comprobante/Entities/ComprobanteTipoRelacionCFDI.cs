﻿using Jaeger.Domain.Comprobante.Contracts;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Tipo de relacion entre CFDI
    /// </summary>
    public class ComprobanteTipoRelacionCFDI : Base.Abstractions.BaseSingleTipoModel, IComprobanteTipoRelacionCFDI {
        public ComprobanteTipoRelacionCFDI(string id, string descripcion) : base(id, descripcion) {
        }
    }
}
