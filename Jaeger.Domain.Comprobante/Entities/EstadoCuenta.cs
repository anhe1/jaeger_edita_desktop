﻿using System;
using SqlSugar;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.Domain.Comprobante.Entities {
    public class EstadoCuenta : Base.Abstractions.BasePropertyChangeImplementation, IEstadoCuenta {
        #region declaraciones
        private int index;
        private int idDirectorio;
        private string emisorRFC;
        private string emisorNombre;
        private string receptorRFC;
        private string receptorNombre;
        private decimal retencionISR;
        private decimal retencionIVA;
        private decimal trasladoIVA;
        private decimal retencionIEPS;
        private decimal trasladoIEPS;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal acumulado;
        private DateTime? fechaUltimoPago;
        private DateTime fechaEmision;
        #endregion

        public EstadoCuenta() { }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio {
            get { return this.idDirectorio; }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre {
            get { return this.emisorNombre; }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr", ColumnDescription = "importe total del impuesto retenido ISR", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get { return this.retencionISR; }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva", ColumnDescription = "importe total del impuesto retenido IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get { return this.retencionIVA; }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva", ColumnDescription = "importe total del impuesto trasladado IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this.trasladoIVA; }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps", ColumnDescription = "importe total del impuesto retenido IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps", ColumnDescription = "importe total del impuesto trasladado IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get { return this.trasladoIEPS; }
            set {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl", ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this.subTotal; }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt", ColumnDescription = "para representar el importe de los descuentos aplicables antes de impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this.descuento; }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this.total; }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get { return this.acumulado; }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get { return this.fechaEmision; }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc", ColumnDescription = "fecha del ultimo pago o cobro de los recibos de pagoo cobro", IsNullable = true)]
        public DateTime? FechaUltimoPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                this.fechaUltimoPago = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("contador")]
        [SugarColumn(IsIgnore = true)]
        public decimal Contador { get; set; }

        [SugarColumn(IsIgnore = true)]
        public decimal PorCobrar { get; set; }

        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get { return this.Total - this.Acumulado; }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
    }
}
