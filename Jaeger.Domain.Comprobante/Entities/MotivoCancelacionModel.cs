﻿namespace Jaeger.Domain.Comprobante.Entities {
    public class MotivoCancelacionModel : Base.Abstractions.BaseSingleTipoModel {
        public MotivoCancelacionModel() {
        }

        public MotivoCancelacionModel(string id, string descripcion) : base(id, descripcion) {
        }
    }
}
