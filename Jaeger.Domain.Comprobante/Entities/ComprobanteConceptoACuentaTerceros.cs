﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using System;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Nodo opcional para registrar información del contribuyente Tercero, a cuenta del que se realiza la operación.
    /// </summary>
    [JsonObject]
    public class ComprobanteConceptoACuentaTerceros : BasePropertyChangeImplementation {
        private string rfcACuentaTercerosField;
        private string nombreACuentaTercerosField;
        private string regimenFiscalACuentaTercerosField;
        private string domicilioFiscalACuentaTercerosField;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteConceptoACuentaTerceros() {

        }

        /// <summary>
        /// obtener o establecer Clave del Registro Federal de Contribuyentes del contribuyente Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        [JsonProperty("rfc")]
        public string RfcACuentaTerceros {
            get {
                return this.rfcACuentaTercerosField;
            }
            set {
                this.rfcACuentaTercerosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre, denominación o razón social del contribuyente Tercero correspondiente con el Rfc, a cuenta del que se realiza la operación.
        /// </summary>
        [JsonProperty("nombre")]
        public string NombreACuentaTerceros {
            get {
                return this.nombreACuentaTercerosField;
            }
            set {
                this.nombreACuentaTercerosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del régimen del contribuyente Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        [JsonProperty("regFiscal")]
        public string RegimenFiscalACuentaTerceros {
            get {
                return this.regimenFiscalACuentaTercerosField;
            }
            set {
                this.regimenFiscalACuentaTercerosField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer codigo postal del domicilio fiscal del Tercero, a cuenta del que se realiza la operación.
        /// </summary>
        [JsonProperty("domFiscal")]
        public string DomicilioFiscalACuentaTerceros {
            get {
                return this.domicilioFiscalACuentaTercerosField;
            }
            set {
                this.domicilioFiscalACuentaTercerosField = value;
                this.OnPropertyChanged();
            }
        }

        public string Json() {
            return JsonConvert.SerializeObject(this);
        }

        public static ComprobanteConceptoACuentaTerceros Json(string inString) {
            if (!string.IsNullOrEmpty(inString)) {
                return JsonConvert.DeserializeObject<ComprobanteConceptoACuentaTerceros>(inString);
            }
            return null;
        }
    }
}
