﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Complemento para el uso de SPEI Tercero a Tercero
    /// </summary>
    [JsonObject("spei")]
    public partial class ComplementoSPEI : BasePropertyChangeImplementation {
        private BindingList<ComplementoSpeiTercero> sPeiTerceroField;

        /// <summary>
        /// Estándar aplicable a operaciones de SPEI a terceros
        /// </summary>
        [JsonProperty("tercero")]
        public BindingList<ComplementoSpeiTercero> SpeiTercero {
            get {
                return this.sPeiTerceroField;
            }
            set {
                this.sPeiTerceroField = value;
                this.OnPropertyChanged();
            }
        }

        #region metodos

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComplementoSPEI Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoSPEI>(inputJson);
            } catch {
                return null;
            }
        }
        #endregion
    }
}

namespace Jaeger.Domain.Comprobante.Entities {
    public partial class ComplementoSpeiTercero : BasePropertyChangeImplementation, IDataErrorInfo {
        private ComplementoSpeiTerceroOrdenante ordenanteField;
        private ComplementoSpeiTerceroBeneficiario beneficiarioField;
        private DateTime? fechaOperacionField;
        private System.DateTime horaField;
        private string claveSpeiField;
        private string selloField;
        private string numeroCertificadoField;
        private string cadenaCdaField;

        /// <summary>
        /// Elemento para describir los datos del ordenante del SPEI
        /// </summary>
        [JsonProperty("ordenante")]
        public ComplementoSpeiTerceroOrdenante Ordenante {
            get {
                return this.ordenanteField;
            }
            set {
                this.ordenanteField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Elemento para describir los datos del beneficiario del SPEI
        /// </summary>
        [JsonProperty("beneficiario")]
        public ComplementoSpeiTerceroBeneficiario Beneficiario {
            get {
                return this.beneficiarioField;
            }
            set {
                this.beneficiarioField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Fecha de operación con formato. Debe ser la misma que la fecha de operación del sistema.
        /// </summary>
        [JsonProperty("fechaOperacion")]
        public DateTime? FechaOperacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaOperacionField >= firstGoodDate) {
                    return this.fechaOperacionField;
                }
                return null;
            }
            set {
                this.fechaOperacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// hora del acreditamiento
        /// </summary>
        [JsonProperty("hora")]
        public DateTime Hora {
            get {
                return this.horaField;
            }
            set {
                this.horaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Clave SPEI del Participante Emisor.
        /// </summary>
        [JsonProperty("claveSpei")]
        public string ClaveSpei {
            get {
                return this.claveSpeiField;
            }
            set {
                this.claveSpeiField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para contener el sello digital del comprobante de pago. El sello deberá ser expresado cómo una cadena de texto en formato Base 64.
        /// </summary>
        [JsonProperty("sello")]
        public string Sello {
            get {
                return this.selloField;
            }
            set {
                this.selloField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la identificación del certificado de seguridad utilizado para el sello digital.
        /// </summary>
        [JsonProperty("numCertificado")]
        public string NumeroCertificado {
            get {
                return this.numeroCertificadoField;
            }
            set {
                this.numeroCertificadoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo que contiene la información del CDA fidedigna que la institución ha enviado a Banco de México.
        /// </summary>
        [JsonProperty("cadenaCDA")]
        public string CadenaCda {
            get {
                return this.cadenaCdaField;
            }
            set {
                this.cadenaCdaField = value;
                this.OnPropertyChanged();
            }
        }

        public string Error {
            get {
                return string.Empty;
            }
        }

        public string this[string columnName] {
            get {
                return string.Empty;
            }
        }
    }
}

namespace Jaeger.Domain.Comprobante.Entities {
    public partial class ComplementoSpeiTerceroOrdenante : BasePropertyChangeImplementation {
        private string bancoEmisorField;
        private string nombreField;
        private decimal tipoCuentaField;
        private decimal cuentaField;
        private string rFCField;

        /// <summary>
        /// Atributo requerido para expresar el nombre del Banco o Institución Financiera emisora del SPEI
        /// </summary>
        [JsonProperty("bancoEmisor")]
        public string BancoEmisor {
            get {
                return this.bancoEmisorField;
            }
            set {
                this.bancoEmisorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre de la persona física o moral que ordena el envío del pago.
        /// </summary>
        [JsonProperty("nombre")]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Categoría de la Cuenta a la que se efectuará el cargo por la transferencia electrónica de fondos.
        /// </summary>
        [JsonProperty("tipoCuenta")]
        public decimal TipoCuenta {
            get {
                return this.tipoCuentaField;
            }
            set {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Cuenta que deberá estar ligada al Tipo de Cuenta del Ordenante, donde serán cargados los fondos.
        /// </summary>
        [JsonProperty("cuenta")]
        public decimal Cuenta {
            get {
                return this.cuentaField;
            }
            set {
                this.cuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Corresponde al registro federal de contribuyentes o clave única de registro de población del ordenante. Se pondrá ND en caso de no tenerlo disponible 
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
                this.OnPropertyChanged();
            }
        }
    }
}

namespace Jaeger.Domain.Comprobante.Entities {
    public partial class ComplementoSpeiTerceroBeneficiario : BasePropertyChangeImplementation {
        private string bancoReceptorField;
        private string nombreField;
        private decimal tipoCuentaField;
        private decimal cuentaField;
        private string rFCField;
        private string conceptoField;
        private decimal iVAField;
        private decimal montoPagoField;

        /// <summary>
        /// Atributo requerido para expresar el nombre del Banco o Institución Financiera Receptora del SPEI
        /// </summary>
        [JsonProperty("bancoReceptor")]
        public string BancoReceptor {
            get {
                return this.bancoReceptorField;
            }
            set {
                this.bancoReceptorField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre de la persona física o moral receptora del pago.
        /// </summary>
        [JsonProperty("nombre")]
        public string Nombre {
            get {
                return this.nombreField;
            }
            set {
                this.nombreField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Categoría de la cuenta a la que se efectuará el abono por la transferencia electrónica de fondos. Consultar Catálogo de Tipos de Cuenta.
        /// </summary>
        [JsonProperty("tipoCuenta")]
        public decimal TipoCuenta {
            get {
                return this.tipoCuentaField;
            }
            set {
                this.tipoCuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Esta cuenta deberá estar ligada al campo Tipo de Cuenta del Beneficiario, donde son abonados los fondos.
        /// </summary>
        [JsonProperty("cuenta")]
        public decimal Cuenta {
            get {
                return this.cuentaField;
            }
            set {
                this.cuentaField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo requerido para la expresión del registro federal de contribuyentes del beneficiario. Se pondrá ND en caso de no estar disponible
        /// </summary>
        [JsonProperty("rfc")]
        public string RFC {
            get {
                return this.rFCField;
            }
            set {
                this.rFCField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Descripción del motivo por el que el ordenante hace el pago al beneficiario.
        /// </summary>
        [JsonProperty("concepto")]
        public string Concepto {
            get {
                return this.conceptoField;
            }
            set {
                this.conceptoField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Importes de IVA correspondientes al pago. El monto debe ser mayor a cero y menor o igual a 9,999,999,999,999,999.99
        /// </summary>
        [JsonProperty("iva")]
        public decimal IVA {
            get {
                return this.iVAField;
            }
            set {
                this.iVAField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Atributo obligatorio para la expresión del monto de la operación. Se trata de un entero positivo
        /// </summary>
        [JsonProperty("montoPago")]
        public decimal MontoPago {
            get {
                return this.montoPagoField;
            }
            set {
                this.montoPagoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}