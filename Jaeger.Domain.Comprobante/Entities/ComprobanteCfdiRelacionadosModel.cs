﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Entities.Complemento;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Nodo opcional para precisar la información de los comprobantes relacionados.
    /// </summary>
    public class ComprobanteCfdiRelacionadosModel : BasePropertyChangeImplementation {
        private BindingList<ComprobanteCfdiRelacionadosCfdiRelacionadoModel> cfdiRelacionadoField;
        private ComprobanteCfdiRelacionadoRelacion tipoRelacionField; //c_TipoRelacion

        public ComprobanteCfdiRelacionadosModel() {
            this.tipoRelacionField = new ComprobanteCfdiRelacionadoRelacion();
            this.cfdiRelacionadoField = new BindingList<ComprobanteCfdiRelacionadosCfdiRelacionadoModel>() { RaiseListChangedEvents = true };
        }

        /// <summary>
        /// Atributo requerido para indicar la clave de la relación que existe entre éste que se esta generando y el o los CFDI previos.
        /// </summary>
        [JsonProperty("tipoRelacion")]
        public ComprobanteCfdiRelacionadoRelacion TipoRelacion {
            get {
                return this.tipoRelacionField;
            }
            set {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nodo requerido para precisar la información de los comprobantes relacionados.
        /// </summary>
        [JsonProperty("cfdiRelacionado")]
        public BindingList<ComprobanteCfdiRelacionadosCfdiRelacionadoModel> CfdiRelacionado {
            get {
                return this.cfdiRelacionadoField;
            }
            set {
                if (this.cfdiRelacionadoField != null) {
                }
                this.cfdiRelacionadoField = value;
                if (this.cfdiRelacionadoField != null) {
                }
                this.OnPropertyChanged();
            }
        }

        #region metodos
        public string Json() {
            var conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.SerializeObject(this, conf);
        }

        public static ComprobanteCfdiRelacionadosModel Json(string jsonIn) {
            if (!(jsonIn == null))
                return JsonConvert.DeserializeObject<ComprobanteCfdiRelacionadosModel>(jsonIn);
            return null;
        }
        #endregion
    }
}
