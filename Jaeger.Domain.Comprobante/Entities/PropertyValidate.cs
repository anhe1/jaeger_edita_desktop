﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    [JsonObject]
    public class PropertyValidate {
        public PropertyValidate() {
        }

        /// <summary>
        /// obtener o establecer el tipo de propiedad
        /// </summary>
        [JsonProperty("type")]
        public PropertyTypeEnum Type {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el codigo de la validacion
        /// </summary>
        [JsonProperty("code")]
        public string Codigo {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el nombre de la propieddad
        /// </summary>
        [JsonProperty("name")]
        public string Propiedad {
            get;set;
        }

        /// <summary>
        /// obtener o establecer el valor de la propieddad
        /// </summary>
        [JsonProperty("value")]
        public string Valor {
            get;set;
        }

        /// <summary>
        /// propiedad valida
        /// </summary>
        [JsonProperty("valid")]
        public bool Valido {
            get;set;
        }

        /// <summary>
        /// obtener o establecer lista de errores de la validacion
        /// </summary>
        [JsonProperty("errors")]
        public List<PropertyValidateSingle> Errors {
            get;set;
        }
    }
}
