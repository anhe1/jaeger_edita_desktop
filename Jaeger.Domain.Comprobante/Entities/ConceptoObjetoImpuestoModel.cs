﻿namespace Jaeger.Domain.Comprobante.Entities {
    public class ConceptoObjetoImpuestoModel : Domain.Base.Abstractions.BaseSingleTipoModel {
        public ConceptoObjetoImpuestoModel() {
        }

        public ConceptoObjetoImpuestoModel(string id, string descripcion) : base(id, descripcion) {
        }
    }
}
