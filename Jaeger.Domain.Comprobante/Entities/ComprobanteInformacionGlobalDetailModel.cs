﻿using SqlSugar;
using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Nodo condicional para precisar la informacion relacionada con el comprobante global.
    /// </summary>
    [SugarTable("_cfdigbl", "Comprobantes fiscales")]
    public class ComprobanteInformacionGlobalDetailModel : ComprobanteInformacionGlobalModel {
        public ComprobanteInformacionGlobalDetailModel() : base() {
            
        }

        public string Json() {
            return JsonConvert.SerializeObject(this);
        }

        public static ComprobanteInformacionGlobalDetailModel Json(string inString) {
            if (!string.IsNullOrEmpty(inString)) {
                return JsonConvert.DeserializeObject<ComprobanteInformacionGlobalDetailModel>(inString);
            }
            return null;
        }
    }
}
