﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalModel : BasePropertyChangeImplementation, IComprobanteFiscalModel {
        #region declaraciones
        private int index;
        private int idDirectorio;
        private bool activo;
        private int subTipoInt;
        private int idAddenda;
        private int idSerie;
        private string documento;
        private string tipoComprobanteText;
        private string status;
        private string version;
        private string serie;
        private string folio;
        private string estado;
        private string emisorRFC;
        private string emisorNombre;
        private int emisorIdDomicilio;
        private string receptorRFC;
        private string receptorNombre;
        private int receptorIdDomicilio;
        private string cuentaPago;
        private string receptorNumRegIdTrib;
        private string receptorResidenciaFiscal;
        private string claveMoneda;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveUsoCFDI;
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado = null;
        private DateTime? fechaEstado = null;
        private DateTime? fechaCancela = null;
        private DateTime? fechaEntrega = null;
        private DateTime? fechaUltimoPago = null;
        private DateTime? fechaRecepcionPago = null;
        private DateTime? fechaValidacion = null;
        private decimal retencionIEPS;
        private decimal trasladoIEPS;
        private decimal totalPecepcion;
        private decimal totalDeduccion;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal acumulado;
        private decimal importePagado;
        private string notas;
        private string result;
        private string fileXML;
        private string filePDF;
        private string fileAccuse;
        private string fileAccuseXML;
        private string fileAccusePDF;
        private bool sincronizado;
        private string creo;
        private string modifica;
        private string condicionPago;
        private string noCertificado;
        private string motivoDescuento;
        private string lugarExpedicion;
        private int diasDeVence;
        private int precisionDecimal;
        private int parcialidad;
        private decimal retencionISR;
        private decimal retencionIVA;
        private decimal trasladoIVA;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica = null;
        private string xml;
        private string rfcProveedorCertificacion;
        private string idDocumento;
        private string jComplementos;
        private string jValidacion;
        private string jAccuse;
        private string jAddenda;
        private string regimenFiscalReceptor;
        private string domicilioFiscalReceptor;
        private string exportacionField;
        private ComprobanteInformacionGlobalDetailModel _InformacionGlobal = null;
        private ComprobanteCfdiRelacionadosModel _ComprobantesRelacionados;
        private ComplementoNomina complementoNomina;
        private decimal _TipoCambio;
        #endregion
        public event EventHandler<int> DecimalsChange;

        public void OnDecimalsChange(int decimals) {
            if (this.DecimalsChange != null) {
                this.DecimalsChange(this, decimals);
            }
        }

        public ComprobanteFiscalModel() {
            this.activo = true;
            this.version = "4.0";
            this.TipoCambio = new decimal(1);
            this.ClaveMoneda = "MXN";
            this.FechaEmision = DateTime.Now;
            this.FechaNuevo = DateTime.Now;
            this.precisionDecimal = 2;
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla.
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice de relacion con el directorio
        /// </summary>
        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio {
            get { return this.idDirectorio; }
            set {
                this.idDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdi_a")]
        [SugarColumn(ColumnName = "_cfdi_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this.activo; }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdSubTipo {
            get { return this.subTipoInt; }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_idaden")]
        [SugarColumn(ColumnName = "_cfdi_idaden", ColumnDescription = "ID de adenda para el modo edición", IsNullable = true, DefaultValue = "0")]
        public int IdAddenda {
            get { return this.idAddenda; }
            set {
                this.idAddenda = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_idserie")]
        [SugarColumn(ColumnName = "_cfdi_idserie", ColumnDescription = "ID de serie para el modo edición", IsNullable = true)]
        public int IdSerie {
            get { return this.idSerie; }
            set {
                this.idSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de documento en modo texto (_cfdi_doc)
        /// </summary>
        [DataNames("_cfdi_doc")]
        [SugarColumn(ColumnName = "_cfdi_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36)]
        public string Documento {
            get { return this.documento; }
            set {
                this.documento = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get { return this.tipoComprobanteText; }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer staus del comprobante (0-Cancelado,1-Importado,2-Por Pagar
        /// </summary>
        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get { return this.status; }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer version del estandar bajo el que se encuentra expresado el comprobante
        /// </summary>
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cfdi_ver", ColumnDescription = "version del comprobante fiscal", IsNullable = true, Length = 3)]
        public string Version {
            get { return this.version; }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get { return this.serie; }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get { return this.folio; }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado {
            get { return this.estado; }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get { return this.emisorRFC; }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre {
            get { return this.emisorNombre; }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del emisor del comprobante
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_domie", ColumnDescription = "indice del domicilio del emisor")]
        public int EmisorIdDomicilio {
            get { return this.emisorIdDomicilio; }
            set {
                this.emisorIdDomicilio = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get { return this.receptorRFC; }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get { return this.receptorNombre; }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el indice del domicilio del receptor
        /// </summary>
        [DataNames("_cfdi_domir")]
        [SugarColumn(ColumnName = "_cfdi_domir", ColumnDescription = "indice del domicilio del receptor")]
        public int ReceptorIdDomicilio {
            get { return this.receptorIdDomicilio; }
            set {
                this.receptorIdDomicilio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante ampara una operación de exportación.
        /// </summary>
        [DataNames("_cfdi_clvexp")]
        [SugarColumn(ColumnName = "_cfdi_clvexp", ColumnDescription = "expresar si el comprobante ampara una operación de exportación.", Length = 2)]
        public string ClaveExportacion {
            get {
                return this.exportacionField;
            }
            set {
                this.exportacionField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer al menos los cuatro ultimos digitos del numero de cuenta con la que se realizo el pago
        /// </summary>
        [DataNames("_cfdi_cntpg")]
        [SugarColumn(ColumnName = "_cfdi_cntpg", Length = 21, ColumnDescription = "para incorporar al menos los cuatro ultimos digitos del numero de cuenta con la que se realizo el pago")]
        public string CuentaPago {
            get { return this.cuentaPago; }
            set {
                this.cuentaPago = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_nmreg", ColumnDescription = "numero de registro de identidad fiscal del receptor cuando sea residente en el extranjero. Es requerido cuando se incluya el complemento de comercio exterior.", Length = 40, IsNullable = true)]
        public string NumRegIdTrib {
            get { return this.receptorNumRegIdTrib; }
            set {
                this.receptorNumRegIdTrib = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del país de residencia para efectos fiscales del receptor del comprobante, cuando se trate de un extranjero, y que es conforme con la especificación ISO 3166-1 alpha-3. 
        /// Es requerido cuando se incluya el complemento de comercio exterior o se registre el atributo NumRegIdTrib.
        /// </summary>
        [DataNames("_cfdi_resfis")]
        [SugarColumn(ColumnName = "_cfdi_resfis", ColumnDescription = "clave del país de residencia para efectos fiscales del receptor del comprobante", Length = 20, IsNullable = false)]
        public string ResidenciaFiscal {
            get { return this.receptorResidenciaFiscal; }
            set {
                this.receptorResidenciaFiscal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el código postal del domicilio fiscal del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_domfis")]
        [SugarColumn(ColumnName = "_cfdi_domfis", ColumnDescription = "codigo postal del domicilio fiscal del receptor del comprobante", Length = 5, IsNullable = false)]
        public string DomicilioFiscal {
            get { return this.domicilioFiscalReceptor; }
            set {
                this.domicilioFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante
        /// </summary>
        [DataNames("_cfdi_rgfsc")]
        [SugarColumn(ColumnName = "_cfdi_rgfsc", ColumnDescription = "clave de regimen fiscal", Length = 3, IsNullable = false)]
        public string ClaveRegimenFiscal {
            get { return this.regimenFiscalReceptor; }
            set { this.regimenFiscalReceptor = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio FIX conforme con la moneda usada. Es requerido cuando la clave de moneda es distinta de MXN y de XXX. El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad 
        /// de la divisa señalada en el atributo moneda. Si el valor está fuera del porcentaje aplicable a la moneda tomado del catálogo c_Moneda, el emisor debe obtener del PAC que vaya a timbrar el CFDI, de manera no automática, 
        /// una clave de confirmación para ratificar que el valor es correcto e integrar dicha clave en el atributo Confirmacion.
        /// </summary>
        [DataNames("_cfdi_tcam")]
        [SugarColumn(ColumnName = "_cfdi_tcam", ColumnDescription = "tipo de cambio FIX conforme con la moneda usada", Length = 255, IsNullable = false)]
        public decimal TipoCambio {
            get { return this._TipoCambio; }
            set { this._TipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get { return this.claveMoneda; }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A 
        /// fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Articulo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get { return this.claveMetodoPago; }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la 
        /// forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get { return this.claveFormaPago; }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [DataNames("_cfdi_usocfdi")]
        [SugarColumn(ColumnName = "_cfdi_usocfdi", ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3)]
        public string ClaveUsoCFDI {
            get { return this.claveUsoCFDI; }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get { return this.fechaEmision; }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get { return this.idDocumento; }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdi_fecedo")]
        [SugarColumn(ColumnName = "_cfdi_fecedo", ColumnDescription = "última fecha en que fué consultado el estado del comprobante", IsNullable = true)]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstado >= firstGoodDate)
                    return this.fechaEstado;
                return null;
            }
            set {
                this.fechaEstado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecent")]
        [SugarColumn(ColumnName = "_cfdi_fecent", ColumnDescription = "fecha de entrega o recepcion del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate) {
                    return this.fechaEntrega;
                }
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc", ColumnDescription = "fecha del ultimo pago o cobro de los recibos de pagoo cobro", IsNullable = true)]
        public DateTime? FechaUltimoPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                this.fechaUltimoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha del comprobante fiscal de repecion de pago
        /// </summary>
        [DataNames("_cfdi_fecpgr")]
        [SugarColumn(ColumnName = "_cfdi_fecpgr", ColumnDescription = "fecha del ultimo pago del comprobante de recepcion pagos, emitidos o recibidos", IsNullable = true)]
        public DateTime? FechaRecepcionPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionPago >= firstGoodDate) {
                    return this.fechaRecepcionPago;
                }
                return null;
            }
            set {
                this.fechaRecepcionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecval")]
        [SugarColumn(ColumnName = "_cfdi_fecval", ColumnDescription = "fecha de validacion en el caso de los comprobantes recibidos", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion >= firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                this.fechaValidacion = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer atributo condicional para expresar las condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"/>
        /// </summary>
        [DataNames("_cfdi_cndpg")]
        [SugarColumn(ColumnName = "_cfdi_cndpg", ColumnDescription = "condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000", Length = 254)]
        public string CondicionPago {
            get { return this.condicionPago; }
            set { this.condicionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer numero de certificado del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nocert")]
        [SugarColumn(ColumnName = "_cfdi_nocert", ColumnDescription = "numero de certificado del emisor del comprobante", IsNullable = true)]
        public string NoCertificado {
            get { return this.noCertificado; }
            set { this.noCertificado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el RFC del proveedor de certificación de comprobantes fiscales digitales que genera el timbre fiscal digital.
        /// </summary>
        [DataNames("_cfdi_pac")]
        [SugarColumn(ColumnName = "_cfdi_pac", ColumnDescription = "representa el rfc del pac que certifica el comprobante", IsNullable = true)]
        public string RFCProvCertif {
            get {
                if (!(string.IsNullOrEmpty(rfcProveedorCertificacion)))
                    return this.rfcProveedorCertificacion;
                return null;
            }
            set {
                if (!(string.IsNullOrEmpty(value)))
                    this.rfcProveedorCertificacion = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// motivo del descuento del comprobante
        /// </summary>
        [DataNames("_cfdi_desct")]
        [SugarColumn(ColumnName = "_cfdi_desct", ColumnDescription = "para expresar el motivo del descuento aplicable", Length = 64, IsNullable = true)]
        public string MotivoDescuento {
            get { return this.motivoDescuento; }
            set { this.motivoDescuento = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).
        /// </summary>
        [DataNames("_cfdi_lgrexp")]
        [SugarColumn(ColumnName = "_cfdi_lgrexp", ColumnDescription = "indicar llugar de expedición del comprobante fiscal", Length = 50, IsNullable = true)]
        public string LugarExpedicion {
            get { return this.lugarExpedicion; }
            set { this.lugarExpedicion = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// dias de vencimiento del comprobante
        /// </summary>
        [DataNames("_cfdi_vence")]
        [SugarColumn(ColumnName = "_cfdi_vence", ColumnDescription = "Dias de vencimiento del pagare", IsNullable = true)]
        public int DiasDeVence {
            get { return this.diasDeVence; }
            set { this.diasDeVence = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer la precision decimal utilizada para el comprobante
        /// </summary>
        [DataNames("_cfdi_prec")]
        [SugarColumn(ColumnName = "_cfdi_prec", ColumnDescription = "precision decimal", DefaultValue = "2")]
        public int PrecisionDecimal {
            get { return this.precisionDecimal; }
            set { this.precisionDecimal = value;
                this.OnPropertyChanged();
                //this.DecimalsChange(this.precisionDecimal, this.precisionDecimal);
                this.OnDecimalsChange(this.precisionDecimal);
            }
        }
        
        /// <summary>
        /// numero de parcialidad
        /// </summary>
        [DataNames("_cfdi_par")]
        [SugarColumn(ColumnName = "_cfdi_par", ColumnDescription = "numero de parcialidad en el caso de ser un comprobante de pago", DefaultValue = "0")]
        public int NumParcialidad {
            get { return this.parcialidad; }
            set { this.parcialidad = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr", ColumnDescription = "importe total del impuesto retenido ISR", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get { return this.retencionISR; }
            set { this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva", ColumnDescription = "importe total del impuesto retenido IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get { return this.retencionIVA; }
            set { this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva", ColumnDescription = "importe total del impuesto trasladado IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get { return this.trasladoIVA; }
            set { this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps", ColumnDescription = "importe total del impuesto retenido IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get { return this.retencionIEPS; }
            set { this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps", ColumnDescription = "importe total del impuesto trasladado IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get { return this.trasladoIEPS; }
            set { this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o estblacer el valor total de las percepciones en el caso de nomina
        /// </summary>
        [DataNames("_cfdi_per")]
        [SugarColumn(ColumnName = "_cfdi_per", ColumnDescription = "importe total de las percepciones en el caso de nomina", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TotalPecepcion {
            get { return this.totalPecepcion; }
            set { this.totalPecepcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_dec")]
        [SugarColumn(ColumnName = "_cfdi_dec", ColumnDescription = "importe total de las deducciones en el caso de la nomina", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TotalDeduccion {
            get { return this.totalDeduccion; }
            set { this.totalDeduccion = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl", ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get { return this.subTotal; }
            set { this.subTotal = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt", ColumnDescription = "para representar el importe de los descuentos aplicables antes de impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get { return this.descuento; }
            set { this.descuento = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get { return this.total; }
            set { this.total = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get { return this.acumulado; }
            set { this.acumulado = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        [DataNames("_cfdi_cbrdp")]
        [SugarColumn(ColumnName = "_cfdi_cbrdp", ColumnDescription = "monto cobrado o pagado con respecto a los comprobantes de pago", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImportePagado {
            get { return this.importePagado; }
            set { this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_obsrv")]
        [SugarColumn(ColumnName = "_cfdi_obsrv", ColumnDescription = "observaciones", Length = 255)]
        public string Notas {
            get { return this.notas; }
            set { this.notas = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// resultado de la descarga del comprobante
        /// </summary>
        [DataNames("_cfdi_rslt")]
        [SugarColumn(ColumnName = "_cfdi_rslt", ColumnDescription = "resultado de la descarga del comprobante", Length = 25)]
        public string Result {
            get { return this.result; }
            set { this.result = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnName = "_cfdi_url_xml", ColumnDescription = "url de descarga para el archivo xml", ColumnDataType = "Text")]
        public string UrlFileXML {
            get { return this.fileXML; }
            set { this.fileXML = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnName = "_cfdi_url_pdf", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get { return this.filePDF; }
            set { this.filePDF = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        [DataNames("_cfdi_url_acu")]
        [SugarColumn(ColumnName = "_cfdi_url_acu", ColumnDescription = "url de descarga de la representacio impresa del acusde de cancelacion del comprobante", ColumnDataType = "Text")]
        public string UrlFileAccuse {
            get { return this.fileAccuse; }
            set { this.fileAccuse = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_url_xmlacu")]
        [SugarColumn(ColumnName = "_cfdi_url_xmlacu", ColumnDescription = "url de descarga del archivo xml del acuse de cancelacion del comprobante", ColumnDataType = "Text")]
        public string FileAccuseXML {
            get { return this.fileAccuseXML; }
            set { this.fileAccuseXML = value;
                this.OnPropertyChanged();
            }
        }
        
        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_url_pdfacu")]
        [SugarColumn(ColumnName = "_cfdi_url_pdfacu", ColumnDescription = "url de descarga del archivo pdf del acuse de cancelacion del comprobante", ColumnDataType = "Text")]
        public string FileAccusePDF {
            get { return this.fileAccusePDF; }
            set { this.fileAccusePDF = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("_cfdi_compl")]
        [SugarColumn(ColumnName = "_cfdi_compl", ColumnDescription = "listado de complemento (json)", ColumnDataType = "Text")]
        public string JComplementos {
            get {
                return this.jComplementos;
            }
            set {
                this.jComplementos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel", ColumnDescription = "comprobantes relacionados (json)", ColumnDataType = "Text")]
        public string JCfdiRelacionados {
            get { if (this._ComprobantesRelacionados != null)
                    return this._ComprobantesRelacionados.Json();
                return null;
            }
            set { if (!string.IsNullOrEmpty(value)) {
                    this._ComprobantesRelacionados = ComprobanteCfdiRelacionadosModel.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer objeto de CFDI relacionados
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionadosModel CfdiRelacionados {
            get {
                return this._ComprobantesRelacionados;
            }
            set {
                this._ComprobantesRelacionados = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer objeto de complemento nomina
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoNomina Nomina {
            get {
                return this.complementoNomina;
            }
            set {
                this.complementoNomina = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_nomina")]
        [SugarColumn(ColumnName = "_cfdi_nomina", ColumnDescription = "complemento de nomina", ColumnDataType = "Text")]
        public string JComplementoNomina {
            get { if (this.Nomina != null)
                    return this.Nomina.Json();
                return null;
            }
            set {
                if (!string.IsNullOrEmpty(value)) {
                    this.Nomina = ComplementoNomina.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer el acuse de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_val")]
        [SugarColumn(ColumnName = "_cfdi_val", ColumnDescription = "validación del comprobante fiscal", ColumnDataType = "Text")]
        public string JValidacion {
            get {
                return jValidacion;
            }
            set {
                jValidacion = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("_cfdi_pagos")]
        [SugarColumn(ColumnName = "_cfdi_pagos", ColumnDescription = "complemento pagos", ColumnDataType = "Text")]
        public string JComplementoPagos {
            get { //if (this._ComplementoPagos != null)
            //        return this._ComplementoPagos.Json();
                return null;
            }
            set { //if (!string.IsNullOrEmpty(value)) {
                  //  this._ComplementoPagos = ComplementoPagos.Json(value);
                  
                    this.OnPropertyChanged();
              //  }
            }
        }
        
        /// <summary>
        /// obtener o establecer el contenido del archivo del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_acuse")]
        [SugarColumn(ColumnName = "_cfdi_acuse", ColumnDescription = "acusde de cancelacion", ColumnDataType = "Text")]
        public string JAccuse {
            get {
                return this.jAccuse;
            }
            set {
                this.jAccuse = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("_cfdi_adenda")]
        [SugarColumn(ColumnName = "_cfdi_adenda", ColumnDescription = "addenda del documento", ColumnDataType = "Text")]
        public string JAddenda {
            get {
                return this.jAddenda;
            }
            set {
                this.jAddenda = value;
                this.OnPropertyChanged();
            }
        }
        
        [DataNames("_cfdi_infgrl")]
        [SugarColumn(ColumnName = "_cfdi_infgrl", ColumnDescription = "informacion general", ColumnDataType = "Text")]
        public string JInformacionGeneral {
            get { if (this.InformacionGlobal != null)
                    return this.InformacionGlobal.Json();
                return null;
            }
            set { if (!string.IsNullOrEmpty(value)) {
                    this.InformacionGlobal = ComprobanteInformacionGlobalDetailModel.Json(value);
                    this.OnPropertyChanged();
                }
            }
        }
        
        /// <summary>
        /// Nodo condicional para precisar la información relacionada con el comprobante global.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteInformacionGlobalDetailModel InformacionGlobal {
            get { if (this._InformacionGlobal != null)
                    return this._InformacionGlobal;
                return null;
            }
            set { if (value != null) {
                    this._InformacionGlobal = value;
                    this.OnPropertyChanged();
                } else {
                    this._InformacionGlobal = null;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer bandera de sincronizacion
        /// </summary>
        [DataNames("_cfdi_sync")]
        [SugarColumn(ColumnName = "_cfdi_sync", ColumnDescription = "bandera de sincronizacion", IsNullable = true)]
        public bool Sincronizado {
            get { return this.sincronizado; }
            set { this.sincronizado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave de usuario que crea el registro
        /// </summary>
        [DataNames("_cfdi_usr_n")]
        [SugarColumn(ColumnName = "_cfdi_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get { return this.creo; }
            set { this.creo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer clave del ultimo usuario que modifica el registro
        /// </summary>
        [DataNames("_cfdi_usr_m")]
        [SugarColumn(ColumnName = "_cfdi_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get { return this.modifica; }
            set { this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de creacion del registro
        /// </summary>
        [DataNames("_cfdi_fn")]
        [SugarColumn(ColumnName = "_cfdi_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get { return this.fechaNuevo; }
            set { this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el ultima fecha de modificacion del registro
        /// </summary>
        [DataNames("_cfdi_fm")]
        [SugarColumn(ColumnName = "_cfdi_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaModifica = value;
            }
        }
        
        /// <summary>
        /// obtener o establecer el contenido del archivo XML
        /// </summary>
        [SugarColumn(ColumnName = "_cfdi_save", ColumnDescription = "XML Completo en formato JSON (Al guardar en S3 se borra)", IsNullable = true)]
        [DataNames("_cfdi_save")]
        public string XML {
            get { return this.xml; }
            set { this.xml = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SaldoPagos {
            get {
                return this.Total - this.Acumulado;
            }
        }
    }
}
