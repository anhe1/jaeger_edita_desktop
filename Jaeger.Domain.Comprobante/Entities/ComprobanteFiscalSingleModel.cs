﻿using System;
using SqlSugar;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Services.Mapping;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Comprobante.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalSingleModel : BasePropertyChangeImplementation, IComprobanteFiscalSingleModel, IComprobanteSingle {
        #region declaraciones
        private int index;
        private bool activo;
        private int subTipoInt;
        private string documento;
        private string tipoComprobanteText;
        private string status;
        private string version;
        private string serie;
        private string folio;
        private string estado;
        private string emisorRFC;
        private string emisorNombre;
        private string receptorRFC;
        private string receptorNombre;
        private string claveMoneda;
        private string claveMetodoPago;
        private string claveFormaPago;
        private string claveUsoCFDI;
        private DateTime fechaEmision;
        private DateTime? fechaTimbrado = null;
        private DateTime? fechaEstado = null;
        private DateTime? fechaCancela = null;
        private DateTime? fechaEntrega = null;
        private DateTime? fechaUltimoPago = null;
        private DateTime? fechaRecepcionPago = null;
        private DateTime? fechaValidacion = null;
        private decimal retencionIEPS;
        private decimal trasladoIEPS;
        private decimal totalPecepcion;
        private decimal totalDeduccion;
        private decimal subTotal;
        private decimal descuento;
        private decimal total;
        private decimal acumulado;
        private decimal importePagado;
        private string fileXML;
        private string filePDF;
        private string fileAccuse;
        private string fileAccuseXML;
        private string fileAccusePDF;
        private string creo;
        private string modifica;
        private string condicionPago;
        private int parcialidad;
        private decimal retencionISR;
        private decimal retencionIVA;
        private decimal trasladoIVA;
        private DateTime fechaNuevo;
        private DateTime? fechaModifica = null;
        private string idDocumento;
        private string jValidacion;
        private string jCfdiRelacionado;
        private string jComplementoPagos;
        private decimal _TipoCambio;
        private decimal importeNota;
        private int _IdDirectorio;
        #endregion

        public ComprobanteFiscalSingleModel() {
            this.activo = true;
            this._TipoCambio = 1;
        }

        /// <summary>
        /// obtener ó establecer el indice de la tabla. (cfdi_id)
        /// </summary>
        [DataNames("_cfdi_id")]
        [SugarColumn(ColumnName = "_cfdi_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener ó establecer si el registro es activo.
        /// </summary>
        [DataNames("_cfdi_a")]
        [SugarColumn(ColumnName = "_cfdi_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Tipo de Comprobante: Ingreso, Egreso, Traslado, Nomina ó Pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDITipoComprobanteEnum TipoComprobante {
            get {
                if (TipoComprobanteText == "Ingreso" || TipoComprobanteText == "I") {
                    return CFDITipoComprobanteEnum.Ingreso;
                } else if (TipoComprobanteText == "Egreso" || TipoComprobanteText == "E") {
                    return CFDITipoComprobanteEnum.Egreso;
                } else if (TipoComprobanteText == "Traslado" || TipoComprobanteText == "T") {
                    return CFDITipoComprobanteEnum.Traslado;
                } else if (TipoComprobanteText == "Nomina" || TipoComprobanteText == "N") {
                    return CFDITipoComprobanteEnum.Nomina;
                } else if (TipoComprobanteText == "Pagos" || TipoComprobanteText == "P") {
                    return CFDITipoComprobanteEnum.Pagos;
                } else {
                    return CFDITipoComprobanteEnum.Ingreso;
                }
            }
            set { }
        }

        /// <summary>
        /// para indicar el tipo de comprobante, si es emitido, recibido ó es un recibo de nomina (_cfdi_doc_id)
        /// </summary>
        [DataNames("_cfdi_doc_id")]
        [SugarColumn(ColumnName = "_cfdi_doc_id", ColumnDescription = "tipo de documento (1=emitido, 2=recibido, 3=nomina)", DefaultValue = "0")]
        public int IdSubTipo {
            get {
                return this.subTipoInt;
            }
            set {
                this.subTipoInt = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Sub tipo de comprobante 
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
        }

        /// <summary>
        /// documento (factura, nota de credito,nota de cargo)
        /// </summary>
        [DataNames("_cfdi_doc")]
        [SugarColumn(ColumnName = "_cfdi_doc", ColumnDescription = "documento (factura, nota de credito,nota de cargo)", Length = 36)]
        public string Documento {
            get {
                return this.documento;
            }
            set {
                this.documento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener la clasificacion del comprobante si es emitido, recibido o es un comprobante de nomina
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string SubTipoText {
            get {
                return Enum.GetName(typeof(CFDISubTipoEnum), this.subTipoInt);
            }
        }

        /// <summary>
        /// version del comprobante
        /// </summary>
        [DataNames("_cfdi_ver")]
        [SugarColumn(ColumnName = "_cfdi_ver", ColumnDescription = "version del comprobante fiscal", IsNullable = true, Length = 3)]
        public string Version {
            get {
                return this.version;
            }
            set {
                this.version = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// tipo de comprobante en modo texto
        /// </summary>
        [DataNames("_cfdi_efecto")]
        [SugarColumn(ColumnName = "_cfdi_efecto", ColumnDescription = "para expresar el efecto del comprobante fiscal para el contribuyente emisor. Ingreso/egreso/traslado", Length = 10, IsNullable = true)]
        public string TipoComprobanteText {
            get {
                return this.tipoComprobanteText;
            }
            set {
                this.tipoComprobanteText = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres., 
        /// Maximo de 25 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,25}"/>
        /// </summary>
        [DataNames("_cfdi_serie")]
        [SugarColumn(ColumnName = "_cfdi_serie", ColumnDescription = "serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.", Length = 25)]
        public string Serie {
            get {
                return this.serie;
            }
            set {
                this.serie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres. 
        /// Maximo de 40 caracteres
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,40}"/>
        /// </summary>
        [DataNames("_cfdi_folio")]
        [SugarColumn(ColumnName = "_cfdi_folio", ColumnDescription = "atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.", Length = 21)]
        public string Folio {
            get {
                return this.folio;
            }
            set {
                this.folio = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_status")]
        [SugarColumn(ColumnName = "_cfdi_status", ColumnDescription = "status del comprobante (0-Cancelado,1-Importado,2-Por Pagar", Length = 10, IsNullable = true)]
        public string Status {
            get {
                return this.status;
            }
            set {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el estado del comprobante del servicio del SAT
        /// </summary>
        [DataNames("_cfdi_estado")]
        [SugarColumn(ColumnName = "_cfdi_estado", ColumnDescription = "estado del comprobante (correcto,error!,cancelado,vigente)", Length = 25, IsNullable = true)]
        public string Estado {
            get {
                return this.estado;
            }
            set {
                this.estado = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_drctr_id")]
        [SugarColumn(ColumnName = "_cfdi_drctr_id", ColumnDescription = "id del directorio", IsNullable = true)]
        public int IdDirectorio {
            get { return this._IdDirectorio; }
            set { this._IdDirectorio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer RFC del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfce")]
        [SugarColumn(ColumnName = "_cfdi_rfce", ColumnDescription = "rfc del emisor del comprobante", Length = 16, IsNullable = true)]
        public string EmisorRFC {
            get {
                return this.emisorRFC;
            }
            set {
                this.emisorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer nombre del emisor del comprobante
        /// </summary>
        [DataNames("_cfdi_nome")]
        [SugarColumn(ColumnName = "_cfdi_nome", ColumnDescription = "identidad fiscal del receptor o emisor del comprobante fiscal", Length = 255, IsNullable = true)]
        public string EmisorNombre {
            get {
                return this.emisorNombre;
            }
            set {
                this.emisorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el registro federal del contribuyentes del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_rfcr")]
        [SugarColumn(ColumnName = "_cfdi_rfcr", ColumnDescription = "registro federal del contribuyentes del receptor del comprobante", Length = 16)]
        public string ReceptorRFC {
            get {
                return this.receptorRFC;
            }
            set {
                this.receptorRFC = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el nombre del receptor del comprobante
        /// </summary>
        [DataNames("_cfdi_nomr")]
        [SugarColumn(ColumnName = "_cfdi_nomr", ColumnDescription = "nombre del receptor del comprobante", Length = 255)]
        public string ReceptorNombre {
            get {
                return this.receptorNombre;
            }
            set {
                this.receptorNombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establacer los 36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122
        /// </summary>
        [DataNames("_cfdi_uuid")]
        [SugarColumn(ColumnName = "_cfdi_uuid", ColumnDescription = "36 caracteres del folio fiscal (UUID) de la transacción de timbrado conforme al estándar RFC 4122", IsNullable = true, Length = 36)]
        public string IdDocumento {
            get {
                return this.idDocumento;
            }
            set {
                this.idDocumento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// numero de parcialidad
        /// </summary>
        [DataNames("_cfdi_par")]
        [SugarColumn(ColumnName = "_cfdi_par", ColumnDescription = "numero de parcialidad en el caso de ser un comprobante de pago", DefaultValue = "0")]
        public int NumParcialidad {
            get {
                return this.parcialidad;
            }
            set {
                this.parcialidad = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para la expresión de la fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.
        /// </summary>
        [DataNames("_cfdi_fecems")]
        [SugarColumn(ColumnName = "_cfdi_fecems", ColumnDescription = "fecha y hora de expedición del Comprobante Fiscal Digital por Internet. Se expresa en la forma AAAA-MM-DDThh:mm:ss y debe corresponder con la hora local donde se expide el comprobante.")]
        public DateTime FechaEmision {
            get {
                return this.fechaEmision;
            }
            set {
                this.fechaEmision = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de certificación
        /// </summary>
        [DataNames("_cfdi_feccert")]
        [SugarColumn(ColumnName = "_cfdi_feccert", ColumnDescription = "fecha de certificacion", IsNullable = true)]
        public DateTime? FechaTimbre {
            get {
                var firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaTimbrado >= firstGoodDate)
                    return this.fechaTimbrado;
                else
                    return null;
            }
            set {
                this.fechaTimbrado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha del estado del comprobante (SAT)
        /// </summary>
        [DataNames("_cfdi_fecedo")]
        [SugarColumn(ColumnName = "_cfdi_fecedo", ColumnDescription = "última fecha en que fué consultado el estado del comprobante", IsNullable = true)]
        public DateTime? FechaEstado {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEstado >= firstGoodDate)
                    return this.fechaEstado;
                return null;
            }
            set {
                this.fechaEstado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la fecha de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_feccnc")]
        [SugarColumn(ColumnName = "_cfdi_feccnc", ColumnDescription = "fecha de cancelacion", IsNullable = true)]
        public DateTime? FechaCancela {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaCancela >= firstGoodDate)
                    return this.fechaCancela;
                return null;
            }
            set {
                this.fechaCancela = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de entrega o recepcion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecent")]
        [SugarColumn(ColumnName = "_cfdi_fecent", ColumnDescription = "fecha de entrega o recepcion del comprobante", IsNullable = true)]
        public DateTime? FechaEntrega {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaEntrega >= firstGoodDate) {
                    return this.fechaEntrega;
                }
                return null;
            }
            set {
                this.fechaEntrega = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de ultimo pago o cobro del comprobante
        /// </summary>
        [DataNames("_cfdi_fecupc")]
        [SugarColumn(ColumnName = "_cfdi_fecupc", ColumnDescription = "fecha del ultimo pago o cobro de los recibos de pagoo cobro", IsNullable = true)]
        public DateTime? FechaUltimoPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaUltimoPago >= firstGoodDate) {
                    return this.fechaUltimoPago;
                }
                return null;
            }
            set {
                this.fechaUltimoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha del comprobante fiscal de repecion de pago
        /// </summary>
        [DataNames("_cfdi_fecpgr")]
        [SugarColumn(ColumnName = "_cfdi_fecpgr", ColumnDescription = "fecha del ultimo pago del comprobante de recepcion pagos, emitidos o recibidos", IsNullable = true)]
        public DateTime? FechaRecepcionPago {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaRecepcionPago >= firstGoodDate) {
                    return this.fechaRecepcionPago;
                }
                return null;
            }
            set {
                this.fechaRecepcionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// fecha de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_fecval")]
        [SugarColumn(ColumnName = "_cfdi_fecval", ColumnDescription = "fecha de validacion en el caso de los comprobantes recibidos", IsNullable = true)]
        public DateTime? FechaValidacion {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaValidacion >= firstGoodDate)
                    return this.fechaValidacion;
                return null;
            }
            set {
                this.fechaValidacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.
        /// </summary>
        [DataNames("_cfdi_moneda")]
        [SugarColumn(ColumnName = "_cfdi_moneda", ColumnDescription = "clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.", Length = 3)]
        public string ClaveMoneda {
            get {
                return this.claveMoneda;
            }
            set {
                this.claveMoneda = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el tipo de cambio FIX conforme con la moneda usada. Es requerido cuando la clave de moneda es distinta de MXN y de XXX. El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad 
        /// de la divisa señalada en el atributo moneda. Si el valor está fuera del porcentaje aplicable a la moneda tomado del catálogo c_Moneda, el emisor debe obtener del PAC que vaya a timbrar el CFDI, de manera no automática, 
        /// una clave de confirmación para ratificar que el valor es correcto e integrar dicha clave en el atributo Confirmacion.
        /// </summary>
        [DataNames("_cfdi_tcam")]
        [SugarColumn(ColumnName = "_cfdi_tcam", ColumnDescription = "tipo de cambio FIX conforme con la moneda usada", DefaultValue = "1", Length = 14, DecimalDigits = 6, IsNullable = false)]
        public decimal TipoCambio {
            get { return this._TipoCambio; }
            set {
                this._TipoCambio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.
        /// </summary>
        [DataNames("_cfdi_mtdpg")]
        [SugarColumn(ColumnName = "_cfdi_mtdpg", ColumnDescription = "Atributo condicional para precisar la clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.", Length = 3)]
        public string ClaveMetodoPago {
            get {
                return this.claveMetodoPago;
            }
            set {
                this.claveMetodoPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.
        /// </summary>
        [DataNames("_cfdi_frmpg")]
        [SugarColumn(ColumnName = "_cfdi_frmpg", ColumnDescription = "obtener o establecer atributo condicional para expresar la clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.", Length = 2)]
        public string ClaveFormaPago {
            get {
                return this.claveFormaPago;
            }
            set {
                this.claveFormaPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.
        /// </summary>
        [DataNames("_cfdi_usocfdi")]
        [SugarColumn(ColumnName = "_cfdi_usocfdi", ColumnDescription = "atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.", Length = 3)]
        public string ClaveUsoCFDI {
            get {
                return this.claveUsoCFDI;
            }
            set {
                this.claveUsoCFDI = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo condicional para expresar las condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000
        /// <xs:pattern value="([A-Z]|[a-z]|[0-9]| |Ñ|ñ|!|&quot;|%|&amp;|'|´|-|:|;|&gt;|=|&lt;|@|_|,|\{|\}|`|~|á|é|í|ó|ú|Á|É|Í|Ó|Ú|ü|Ü){1,1000}"/>
        /// </summary>
        [DataNames("_cfdi_cndpg")]
        [SugarColumn(ColumnName = "_cfdi_cndpg", ColumnDescription = "condiciones comerciales aplicables para el pago del comprobante fiscal digital por Internet. Este atributo puede ser condicionado mediante atributos o complementos. Maximo de caracteres 1000", Length = 254)]
        public string CondicionPago {
            get {
                return this.condicionPago;
            }
            set {
                this.condicionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor total del impuesto retenido ISR
        /// </summary>
        [DataNames("_cfdi_retisr")]
        [SugarColumn(ColumnName = "_cfdi_retisr", ColumnDescription = "importe total del impuesto retenido ISR", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionISR {
            get {
                return this.retencionISR;
            }
            set {
                this.retencionISR = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IVA
        /// </summary>
        [DataNames("_cfdi_retiva")]
        [SugarColumn(ColumnName = "_cfdi_retiva", ColumnDescription = "importe total del impuesto retenido IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIVA {
            get {
                return this.retencionIVA;
            }
            set {
                this.retencionIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del traslado de IVA
        /// </summary>
        [DataNames("_cfdi_trsiva")]
        [SugarColumn(ColumnName = "_cfdi_trsiva", ColumnDescription = "importe total del impuesto trasladado IVA", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIVA {
            get {
                return this.trasladoIVA;
            }
            set {
                this.trasladoIVA = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto retenido IEPS
        /// </summary>
        [DataNames("_cfdi_retieps")]
        [SugarColumn(ColumnName = "_cfdi_retieps", ColumnDescription = "importe total del impuesto retenido IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal RetencionIEPS {
            get {
                return this.retencionIEPS;
            }
            set {
                this.retencionIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el valor del impuesto traslado IEPS
        /// </summary>
        [DataNames("_cfdi_trsieps")]
        [SugarColumn(ColumnName = "_cfdi_trsieps", ColumnDescription = "importe total del impuesto trasladado IEPS", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TrasladoIEPS {
            get {
                return this.trasladoIEPS;
            }
            set {
                this.trasladoIEPS = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o estblacer el valor total de las percepciones en el caso de nomina
        /// </summary>
        [DataNames("_cfdi_per")]
        [SugarColumn(ColumnName = "_cfdi_per", ColumnDescription = "importe total de las percepciones en el caso de nomina", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TotalPecepcion {
            get {
                return this.totalPecepcion;
            }
            set {
                this.totalPecepcion = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_dec")]
        [SugarColumn(ColumnName = "_cfdi_dec", ColumnDescription = "importe total de las deducciones en el caso de la nomina", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal TotalDeduccion {
            get {
                return this.totalDeduccion;
            }
            set {
                this.totalDeduccion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el subTotal del comprobante
        /// </summary>
        [DataNames("_cfdi_sbttl")]
        [SugarColumn(ColumnName = "_cfdi_sbttl", ColumnDescription = "representar la suma de los importes antes de descuentos e impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal SubTotal {
            get {
                return this.subTotal;
            }
            set {
                this.subTotal = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del descuento aplicado
        /// </summary>
        [DataNames("_cfdi_dscnt")]
        [SugarColumn(ColumnName = "_cfdi_dscnt", ColumnDescription = "para representar el importe de los descuentos aplicables antes de impuestos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Descuento {
            get {
                return this.descuento;
            }
            set {
                this.descuento = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto del comprobante
        /// </summary>
        [DataNames("_cfdi_total")]
        [SugarColumn(ColumnName = "_cfdi_total", ColumnDescription = "representa la suma del subtotal, menos los descuentos aplicables, mas los impuestos, menos los impuestos retenidos", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal Total {
            get {
                return this.total;
            }
            set {
                this.total = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// monto cobrado o pagado del comprobante
        /// </summary>
        [DataNames("_cfdi_cbrd")]
        [SugarColumn(ColumnName = "_cfdi_cbrd", ColumnDescription = "monto cobrado o bien lo que se a pagado", Length = 14, DecimalDigits = 4, DefaultValue = "0")]
        public decimal Acumulado {
            get {
                return this.acumulado;
            }
            set {
                this.acumulado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// importe del pago del comprobante de recepcion de pagos
        /// </summary>
        [DataNames("_cfdi_cbrdp")]
        [SugarColumn(ColumnName = "_cfdi_cbrdp", ColumnDescription = "monto cobrado o pagado con respecto a los comprobantes de pago", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImportePagado {
            get {
                return this.importePagado;
            }
            set {
                this.importePagado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el importe aplicado de una nota de credito relacionada al comprobante
        /// </summary>
        [DataNames("_cfdi_dscntc")]
        [SugarColumn(ColumnName = "_cfdi_dscntc", ColumnDescription = "monto de los descuentos aplicados a el comprobante fiscal (nota de credito)", DefaultValue = "0", Length = 14, DecimalDigits = 4)]
        public decimal ImporteNota {
            get { return this.importeNota; }
            set {
                this.importeNota = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url del archivo XML del comprobante fiscal
        /// </summary>
        [DataNames("_cfdi_url_xml")]
        [SugarColumn(ColumnName = "_cfdi_url_xml", ColumnDescription = "url de descarga para el archivo xml", ColumnDataType = "Text")]
        public string UrlFileXML {
            get {
                return this.fileXML;
            }
            set {
                this.fileXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url de la representacion impresa (PDF)
        /// </summary>
        [DataNames("_cfdi_url_pdf")]
        [SugarColumn(ColumnName = "_cfdi_url_pdf", ColumnDescription = "url de descarga para la representación impresa", ColumnDataType = "Text")]
        public string UrlFilePDF {
            get {
                return this.filePDF;
            }
            set {
                this.filePDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la url del archivo de acuse de cancelacion
        /// </summary>
        [DataNames("_cfdi_url_acu")]
        [SugarColumn(ColumnName = "_cfdi_url_acu", ColumnDescription = "url de descarga de la representacio impresa del acusde de cancelacion del comprobante", ColumnDataType = "Text")]
        public string FileAccuse {
            get {
                return this.fileAccuse;
            }
            set {
                this.fileAccuse = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo xml del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_url_xmlacu")]
        [SugarColumn(ColumnName = "_cfdi_url_xmlacu", ColumnDescription = "url de descarga del archivo xml del acuse de cancelacion del comprobante", ColumnDataType = "Text")]
        public string FileAccuseXML {
            get {
                return this.fileAccuseXML;
            }
            set {
                this.fileAccuseXML = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer url de descarga del archivo pdf del acuse de cancelacion del comprobante
        /// </summary>
        [DataNames("_cfdi_url_pdfacu")]
        [SugarColumn(ColumnName = "_cfdi_url_pdfacu", ColumnDescription = "url de descarga del archivo pdf del acuse de cancelacion del comprobante", ColumnDataType = "Text")]
        public string FileAccusePDF {
            get {
                return this.fileAccusePDF;
            }
            set {
                this.fileAccusePDF = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el acuse de validacion del comprobante
        /// </summary>
        [DataNames("_cfdi_val")]
        [SugarColumn(ColumnName = "_cfdi_val", ColumnDescription = "validación del comprobante fiscal", ColumnDataType = "Text")]
        public string JValidacion {
            get {
                return jValidacion;
            }
            set {
                jValidacion = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer la información de los comprobantes relacionados.
        /// </summary>
        [DataNames("_cfdi_comrel")]
        [SugarColumn(ColumnName = "_cfdi_comrel", ColumnDescription = "comprobantes relacionados (json)", ColumnDataType = "Text")]
        public string JCfdiRelacionados {
            get {
                return jCfdiRelacionado;
            }
            set {
                this.jCfdiRelacionado = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// CFDI relacionados al comprobante fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteCfdiRelacionadosModel CfdiRelacionados {
            get {
                if (this.JCfdiRelacionados != null) {
                    try {
                        var result = ComprobanteCfdiRelacionadosModel.Json(this.JCfdiRelacionados);
                        return result;
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
                return null;
            }
        }

        [DataNames("_cfdi_pagos")]
        [SugarColumn(ColumnName = "_cfdi_pagos", ColumnDescription = "complemento pagos", ColumnDataType = "Text")]
        public string JComplementoPagos {
            get {
                return jComplementoPagos;
            }
            set {
                jComplementoPagos = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_usr_n")]
        [SugarColumn(ColumnName = "_cfdi_usr_n", ColumnDescription = "clave de usuario que crea el registro", Length = 10, IsOnlyIgnoreUpdate = true)]
        public string Creo {
            get {
                return this.creo;
            }
            set {
                this.creo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_usr_m")]
        [SugarColumn(ColumnName = "_cfdi_usr_m", ColumnDescription = "clave del ultimo usuario que modifico el registro", Length = 10, IsOnlyIgnoreInsert = true, IsNullable = true)]
        public string Modifica {
            get {
                return this.modifica;
            }
            set {
                this.modifica = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_fn")]
        [SugarColumn(ColumnName = "_cfdi_fn", ColumnDescription = "fecha de creacion del registro")]
        public DateTime FechaNuevo {
            get {
                return this.fechaNuevo;
            }
            set {
                this.fechaNuevo = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("_cfdi_fm")]
        [SugarColumn(ColumnName = "_cfdi_fm", ColumnDescription = "ultima fecha de modificacion del registro", IsNullable = true, IsOnlyIgnoreInsert = true)]
        public DateTime? FechaModifica {
            get {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (this.fechaModifica >= firstGoodDate)
                    return this.fechaModifica.Value;
                return null;
            }
            set {
                DateTime firstGoodDate = new DateTime(1900, 1, 1);
                if (value >= firstGoodDate)
                    this.fechaModifica = value;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }

        public string KeyName() {
            string nombre;
            if (!string.IsNullOrEmpty(this.IdDocumento)) {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.IdDocumento, "-", this.FechaTimbre.Value.ToString("yyyyMMddHHmmss"));
            } else {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }
    }
}
