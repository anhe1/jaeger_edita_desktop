﻿namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteTotalPrinter {
        public string Leyenda { get; set; }
        public decimal Total { get; set; }
    }
}
