﻿/// develop:
/// purpose:
using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// configuracion de opciones de CFDI (key:cfdi)
    /// </summary>
    public class EmpresaCFDIConfiguracion {
        [JsonProperty("general")]
        public General General { get; set; }

        [JsonProperty("domicilioFiscal")]
        public DomicilioFiscal DomicilioFiscal { get; set; }

        [JsonProperty("ocultarDeducciones")]
        public string[] OcultarDeducciones { get; set; }

        [JsonProperty("ocultarPercepciones")]
        public string[] OcultarPercepciones { get; set; }

        public static EmpresaCFDIConfiguracion Json(string inputString) {
            JsonSerializerSettings conf = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return JsonConvert.DeserializeObject<EmpresaCFDIConfiguracion>(inputString, conf);
        }
    }
}
