﻿using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities.Complemento {
    /// <summary>
    /// Atributo requerido para indicar la clave de la relación que existe entre éste que se está generando y el o los CFDI previos.
    /// </summary>
    [JsonObject("tipoDeRelacion")]
    public class ComprobanteCfdiRelacionadoRelacion : BasePropertyChangeImplementation {
        private string claveField;
        private string descripcionField;

        public ComprobanteCfdiRelacionadoRelacion() {
        }

        /// <summary>
        /// Atributo requerido para indicar la clave de la relación que existe entre éste que se está generando y el o los CFDI previos.
        /// </summary>
        [JsonProperty("clave")]
        public string Clave {
            get {
                return this.claveField;
            }
            set {
                this.claveField = value;
                this.OnPropertyChanged();
            }
        }

        [JsonProperty("desc")]
        public string Descripcion {
            get {
                return this.descripcionField;
            }
            set {
                this.descripcionField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
