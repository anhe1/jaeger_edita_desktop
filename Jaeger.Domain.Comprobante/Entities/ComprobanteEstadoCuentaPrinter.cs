﻿using System.Collections.Generic;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteEstadoCuentaPrinter {

        public ComprobanteEstadoCuentaPrinter(List<ComprobanteFiscalDetailSingleModel> conceptos, bool vertical = false) {
            this.Vertical = vertical;
            this.Conceptos = conceptos;
        }

        public bool Vertical { get; set; }

        public string RazonSocial { get; set; }

        public string RFC { get; set; }

        public List<ComprobanteFiscalDetailSingleModel> Conceptos { get; set; }
    }
}
