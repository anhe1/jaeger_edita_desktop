﻿using System;
using System.ComponentModel;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteFiscalPrinter : ComprobanteFiscalModel, IComprobanteFiscalModel, IComprobanteFiscalPrinter {
        private string _EmisorRegimenFiscal;
        public ComprobanteFiscalPrinter() : base() {

        }

        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)base.IdSubTipo;
            }
            set {
                base.IdSubTipo = (int)value;
            }
        }

        public new string TipoComprobanteText {
            get { return base.TipoComprobanteText; }
            set {
                if (value.ToUpper().StartsWith("I"))
                    base.TipoComprobanteText = "Ingreso";
                else if (value.ToUpper().StartsWith("E"))
                    base.TipoComprobanteText = "Egreso";
                else if (value.ToUpper().StartsWith("N"))
                    base.TipoComprobanteText = "Nómina";
                else if (value.ToUpper().StartsWith("P"))
                    base.TipoComprobanteText = "Pagos";
                else if (value.ToUpper().StartsWith("T"))
                    base.TipoComprobanteText = "Traslado";
                else
                    base.TipoComprobanteText = value;
            }
        }

        public CFDITipoComprobanteEnum TipoComprobante {
            get {
                if (base.TipoComprobanteText == "Ingreso" || base.TipoComprobanteText == "I")
                    return CFDITipoComprobanteEnum.Ingreso;
                else if (base.TipoComprobanteText == "Egreso" || base.TipoComprobanteText == "E")
                    return CFDITipoComprobanteEnum.Egreso;
                else if (base.TipoComprobanteText == "Traslado" || base.TipoComprobanteText == "T")
                    return CFDITipoComprobanteEnum.Traslado;
                else if (base.TipoComprobanteText == "Nomina" || base.TipoComprobanteText == "N")
                    return CFDITipoComprobanteEnum.Nomina;
                else if (base.TipoComprobanteText == "Pagos" || base.TipoComprobanteText == "P")
                    return CFDITipoComprobanteEnum.Pagos;
                else
                    return CFDITipoComprobanteEnum.Ingreso;
            }
            set {
                base.TipoComprobanteText = Enum.GetName(typeof(CFDITipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        public string EmisorRegimenFiscal {
            get { return this._EmisorRegimenFiscal; }
            set {
                this._EmisorRegimenFiscal = value;
                this.OnPropertyChanged();
            }
        }

        public decimal TotalTraslados {
            get { return this.TrasladoIVA + this.TrasladoIEPS; }
        }

        public decimal TotalRetenciones {
            get {
                return this.RetencionIVA + this.RetencionISR + this.RetencionIEPS;
            }
        }

        public string Confirmacion { get; set; }

        public string ReceptorDomicilioFiscal { get; set; }

        public BindingList<ComprobanteConceptoDetailModel> Conceptos { get; set; }

        public BindingList<ComplementoPagoDetailModel> RecepcionPago { get; set; }

        public CartaPorteDetailModel CartaPorte { get; set; }

        public ComplementoTimbreFiscal TimbreFiscal { get; set; }

        public BindingList<ComprobanteTotalPrinter> Impuestos { get; set; }

        public string QR { get; set; }

        public string CantidadEnLetra { get; set; }

        public string CadenaOriginalOff { get; set; }
    }
}
