﻿using System;
using System.Linq;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;
using Jaeger.Domain.Comprobante.Entities.Complemento.Nomina;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalDetailModel : ComprobanteFiscalModel, IComprobanteFiscalDetailModel, IComprobanteFiscalModel {
        #region declaraciones
        private BindingList<ComprobanteConceptoDetailModel> _Conceptos;
        private BindingList<ComplementoPagoDetailModel> _ComplementoPagos;
        private CartaPorteDetailModel _ComplementoCartaPorte;
        private string claveRegimenFiscal;
        #endregion

        public ComprobanteFiscalDetailModel() : base() {
            this.Exportacion = CFDIExportacionEnum.No_Aplica;
            this._Conceptos = new BindingList<ComprobanteConceptoDetailModel>() { RaiseListChangedEvents = true };
            this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
            this.DecimalsChange += this.ComprobanteFiscalDetailModel_DecimalsChange;
        }

        private void ComprobanteFiscalDetailModel_DecimalsChange(object sender, int e) {
            if (this.Conceptos != null) {
                if (this.Conceptos.Count > 0) {
                    for (int i = 0; i < this.Conceptos.Count; i++) {
                        this.Conceptos[i].PresionDecimal = e;
                    }
                }
            }
        }

        #region propiedades
        /// <summary>
        /// obtener o establecer enumeracion del subtipo de comprobante (emitido, recibo o nomina)
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDISubTipoEnum SubTipo {
            get {
                return (CFDISubTipoEnum)this.IdSubTipo;
            }
            set {
                this.IdSubTipo = (int)value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer tipo de comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDITipoComprobanteEnum TipoComprobante {
            get {
                if (base.TipoComprobanteText == "Ingreso" || base.TipoComprobanteText == "I")
                    return CFDITipoComprobanteEnum.Ingreso;
                else if (base.TipoComprobanteText == "Egreso" || base.TipoComprobanteText == "E")
                    return CFDITipoComprobanteEnum.Egreso;
                else if (base.TipoComprobanteText == "Traslado" || base.TipoComprobanteText == "T")
                    return CFDITipoComprobanteEnum.Traslado;
                else if (base.TipoComprobanteText == "Nomina" || base.TipoComprobanteText == "N")
                    return CFDITipoComprobanteEnum.Nomina;
                else if (base.TipoComprobanteText == "Pagos" || base.TipoComprobanteText == "P")
                    return CFDITipoComprobanteEnum.Pagos;
                else
                    return CFDITipoComprobanteEnum.Ingreso;
            }
            set {
                base.TipoComprobanteText = Enum.GetName(typeof(CFDITipoComprobanteEnum), value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer atributo requerido para expresar si el comprobante ampara una operación de exportación.
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CFDIExportacionEnum Exportacion {
            get {
                if (base.ClaveExportacion == "02")
                    return CFDIExportacionEnum.Definitiva;
                else if (base.ClaveExportacion == "03")
                    return CFDIExportacionEnum.Temporal;
                return CFDIExportacionEnum.No_Aplica;
            }
            set {
                if (value == CFDIExportacionEnum.No_Aplica)
                    base.ClaveExportacion = "01";
                else if (value == CFDIExportacionEnum.Definitiva)
                    base.ClaveExportacion = "02";
                else if (value == CFDIExportacionEnum.Temporal)
                    base.ClaveExportacion = "03";
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// objeto emisor
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteContribuyenteModel Emisor {
            get {
                return new ComprobanteContribuyenteModel { RFC = this.EmisorRFC, Nombre = this.EmisorNombre, RegimenFiscal = this.claveRegimenFiscal };
            }
            set {
                if (value != null) {
                    this.EmisorRFC = value.RFC;
                    this.EmisorNombre = value.Nombre;
                    this.claveRegimenFiscal = value.RegimenFiscal;
                }
            }
        }

        /// <summary>
        /// objeto receptor
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComprobanteContribuyenteModel Receptor {
            get {
                return new ComprobanteContribuyenteModel {
                    RFC = this.ReceptorRFC,
                    Nombre = this.ReceptorNombre,
                    NumRegIdTrib = this.NumRegIdTrib,
                    ResidenciaFiscal = this.ResidenciaFiscal,
                    ClaveUsoCFDI = this.ClaveUsoCFDI,
                    DomicilioFiscal = this.DomicilioFiscal,
                    RegimenFiscal = this.ClaveRegimenFiscal
                };
            }
            set {
                if (value != null) {
                    this.ReceptorRFC = value.RFC;
                    this.ReceptorNombre = value.Nombre;
                    this.NumRegIdTrib = value.NumRegIdTrib;
                    this.ResidenciaFiscal = value.ResidenciaFiscal;
                    this.ClaveUsoCFDI = value.ClaveUsoCFDI;
                    this.ClaveRegimenFiscal = value.RegimenFiscal;
                    this.DomicilioFiscal = value.DomicilioFiscal;
                }
            }
        }

        /// <summary>
        /// lista de conceptos del comprobante
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComprobanteConceptoDetailModel> Conceptos {
            get {
                return this._Conceptos;
            }
            set {
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged -= new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this._Conceptos = value;
                if (this._Conceptos != null) {
                    this._Conceptos.ListChanged += new ListChangedEventHandler(this.Conceptos_ListChanged);
                }
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer complemento de recepcion de pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComplementoPagoDetailModel> RecepcionPago {
            get { return this._ComplementoPagos; }
            set {
                this._ComplementoPagos = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer complemento de carta porte
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CartaPorteDetailModel CartaPorte {
            get { return this._ComplementoCartaPorte; }
            set {
                this._ComplementoCartaPorte = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public ComplementoNominaDetailModel Nomina12 { get; set; }

        /// <summary>
        /// complemento del timbre fiscal
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public ComplementoTimbreFiscal TimbreFiscal {
            get {
                if (ValidacionService.UUID(this.IdDocumento))
                    return new ComplementoTimbreFiscal {
                        UUID = this.IdDocumento,
                        NoCertificadoSAT = this.NoCertificado,
                        FechaTimbrado = this.FechaTimbre,
                        RFCProvCertif = this.RFCProvCertif
                    };
                return null;
            }
            set {
                if (value != null) {
                    this.IdDocumento = value.UUID;
                    this.RFCProvCertif = value.RFCProvCertif;
                    this.NoCertificado = value.NoCertificadoSAT;
                    this.FechaTimbre = value.FechaTimbrado;
                    this.OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// obtener o establecer si el comprobante es editable
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public bool IsEditable {
            get {
                return !ValidacionService.UUID(this.IdDocumento);
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Tag { get; set; }
        #endregion

        #region metodos publicos
        /// <summary>
        /// establecer el contenido del comprobante con las opciones por default
        /// </summary>
        public void Default() {
            if (this.SubTipo == CFDISubTipoEnum.Emitido) {
                this.ClaveUsoCFDI = "G01";
                this.ClaveMoneda = "MXN";
                this.CondicionPago = null;

                var _concepto = new ComprobanteConceptoDetailModel();
                if (this.TipoComprobante == CFDITipoComprobanteEnum.Nomina) {
                    this.ClaveFormaPago = "99";
                    this.ClaveMetodoPago = "PUE";
                    _concepto = new ComprobanteConceptoDetailModel() { ClaveProdServ = "84111505", Cantidad = 1, ClaveUnidad = "ACT", Descripcion = "Pago de nómina", ValorUnitario = 0, Importe = 0 };
                } else if (this.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                    base.ClaveUsoCFDI = "CP01";
                    base.PrecisionDecimal = 0;
                    this.ClaveMoneda = "XXX";
                    this.ClaveMetodoPago = null;
                    _concepto = new ComprobanteConceptoDetailModel(1, 0, "84111506", "ACT", "Pago", CFDIObjetoImpuestoEnum.No_objeto_de_impuesto);
                    this.RecepcionPago = new BindingList<ComplementoPagoDetailModel>() {
                        new ComplementoPagoDetailModel {
                            FechaPagoP = DateTime.Now, DoctoRelacionados = new BindingList<PagosPagoDoctoRelacionadoDetailModel>() } };
                } else if (this.TipoComprobante == CFDITipoComprobanteEnum.Traslado) {
                    this.ClaveMoneda = "XXX";
                    this.ClaveUsoCFDI = "G01";
                } else {
                    _concepto = null;
                    this._Conceptos.Clear();
                }

                if (_concepto != null) {
                    if (this.Search(_concepto) == null) {
                        this._Conceptos.Clear();
                        this.Conceptos.Add(_concepto);
                    }
                }
            }
        }

        public string KeyName() {
            string nombre;
            if (this.TimbreFiscal != null) {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.TimbreFiscal.UUID, "-", this.TimbreFiscal.FechaTimbrado.Value.ToString("yyyyMMddHHmmss"));
            } else {
                nombre = string.Concat("CFDI-", this.EmisorRFC, "-", this.ReceptorRFC, "-", this.Serie, "-", this.Folio, DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            return nombre;
        }

        /// <summary>
        /// crear copia de la clase actual
        /// </summary>
        public void Clonar() {
            if (this.SubTipo == CFDISubTipoEnum.Emitido) {
                // datos generales
                this.Id = 0;
                base.Folio = null;
                this.FechaEmision = DateTime.Now;
                this.FechaEntrega = null;
                this.FechaEstado = null;
                this.FechaTimbre = null;
                this.FechaModifica = null;
                this.FechaNuevo = DateTime.Now;
                this.Creo = null;
                this.Modifica = null;
                this.FechaUltimoPago = null;
                this.FechaRecepcionPago = null;
                this.NoCertificado = null;
                this.NumParcialidad = 0;
                this.FechaValidacion = null;
                this.UrlFilePDF = null;
                this.UrlFileXML = null;
                //this.UrlAccuse = null;
                this.Estado = null;
                this.IdDocumento = null;
                this.Status = "Pendiente";
                this.Acumulado = 0;
                this.XML = null;
                // eliminar el id relacionado en las partidas
                foreach (ComprobanteConceptoDetailModel item in this._Conceptos) {
                    item.Id = 0;
                    item.SubId = 0;
                    item.Cantidad = item.Cantidad;
                    if (item.Activo == false)
                        this._Conceptos.Remove(item);
                }
                // por la version 4.0
                if (this.TipoComprobante == CFDITipoComprobanteEnum.Pagos) {
                    this.ClaveUsoCFDI = "CP01";
                }
                this.TimbreFiscal = null;
            }
        }

        /// <summary>
        /// sustituir comprobante, se relaciona la información del comprobante actual
        /// </summary>
        public void Sustituir() {
            if (this.SubTipo == CFDISubTipoEnum.Emitido) {
                this.CfdiRelacionados = new ComprobanteCfdiRelacionadosModel();
                this.CfdiRelacionados.TipoRelacion = new ComprobanteCfdiRelacionadoRelacion { Clave = "04" };
                this.CfdiRelacionados.CfdiRelacionado.Add(new ComprobanteCfdiRelacionadosCfdiRelacionadoModel { Folio = this.Folio, Serie = this.Serie, Total = this.Total, Nombre = this.Receptor.Nombre, RFC = this.ReceptorRFC, IdDocumento = this.TimbreFiscal.UUID });
                this.Clonar();
            }
        }

        /// <summary>
        /// buscar un objeto concepto dentro de la lista de conceptos del comprobante, retorna el objeto encontrado o nulo si no existe
        /// </summary>
        public ComprobanteConceptoDetailModel Search(ComprobanteConceptoDetailModel objeto) {
            ComprobanteConceptoDetailModel search;
            if (this._Conceptos == null)
                search = null;
            else {
                objeto = this._Conceptos.FirstOrDefault<ComprobanteConceptoDetailModel>((ComprobanteConceptoDetailModel p) => p.ClaveProdServ == objeto.ClaveProdServ);
                search = objeto;
            }
            return search;
        }
        #endregion

        #region metodos privados
        private void Conceptos_ListChanged(object sender, ListChangedEventArgs e) {
            if (sender != null) {
                var d = sender as ComprobanteConceptoDetailModel;
                if (d != null) {
                    d.PresionDecimal = this.PrecisionDecimal;
                }
            }
            this.SubTotal = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.Importe);
            this.Descuento = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.Descuento);
            this.TrasladoIVA = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.TrasladoIVA);
            this.TrasladoIEPS = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.TrasladoIEPS);
            this.RetencionIVA = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.RetencionIVA);
            this.RetencionISR = this._Conceptos.Where((ComprobanteConceptoDetailModel p) => p.Activo == true).Sum((ComprobanteConceptoDetailModel p) => p.RetencionISR);
            this.Total = (this.SubTotal - this.Descuento) + (this.TrasladoIVA + this.TrasladoIEPS) - (this.RetencionIVA + this.RetencionISR);
        }
        #endregion
    }
}
