﻿using Jaeger.Domain.Services.Mapping;
using System;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteBackup : Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private string index;
        private string subindex;
        private string name;
        private string type = "Folder";
        private int year;
        private int month;
        private int count;
        #endregion

        public ComprobanteBackup() {

        }

        #region propiedades
        [DataNames("Indice")]
        public string Index {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("ParentId")]
        public string Parent {
            get {
                return this.subindex;
            }
            set {
                this.subindex = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Descripcion")]
        public string Descripcion {
            get {
                return this.name;
            }
            set {
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        public string Name {
            get {
                if (this.Parent != "0")
                    return Enum.GetName(typeof(Domain.Base.ValueObjects.MesesEnum), this.month);
                else
                    return this.Descripcion;
            }
        }

        public string Type {
            get {
                return this.type;
            }
            set {
                this.type = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Ejercicio")]
        public int Year {
            get {
                return this.year;
            }
            set {
                this.year = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Periodo")]
        public int Month {
            get {
                return this.month;
            }
            set {
                this.month = value;
                this.OnPropertyChanged();
            }
        }

        [DataNames("Contador")]
        public int Count {
            get {
                return this.count;
            }
            set {
                this.count = value;
                this.OnPropertyChanged();
            }
        }
        #endregion
    }
}
