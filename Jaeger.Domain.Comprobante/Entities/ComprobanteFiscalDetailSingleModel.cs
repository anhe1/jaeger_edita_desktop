﻿using System;
using System.ComponentModel;
using SqlSugar;
using Jaeger.Domain.Comprobante.Contracts;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Comprobante.Entities.Complemento.Pagos;
using Jaeger.Domain.Comprobante.Entities.Complemento.CartaPorte;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_cfdi", "Comprobantes fiscales")]
    public class ComprobanteFiscalDetailSingleModel : ComprobanteFiscalSingleModel, IComprobanteFiscalSingleModel, IComprobanteFiscalDetailSingleModel, IComprobanteSingle {
        #region declaraciones
        private BindingList<ComplementoPagoDetailModel> _ComplementoRecepcionPago;
        private CartaPorteDetailModel _CartaPorte;
        #endregion

        public ComprobanteFiscalDetailSingleModel() : base() {

        }

        #region propiedades
        /// <summary>
        /// importe del comprobante de tipo egreso
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public decimal Total1 {
            get {
                if (this.TipoComprobante == CFDITipoComprobanteEnum.Egreso)
                    return (base.Total * -1);
                return base.Total;
            }
            set {
                base.Total = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal Saldo {
            get {
                return this.Total - this.Acumulado;
            }
        }

        [SugarColumn(IsIgnore = true)]
        public decimal SaldoPagos {
            get {// revisar por saldos de tipos de comprobantes
                if (this.TipoComprobante == CFDITipoComprobanteEnum.Egreso)
                    return 0;
                    return (this.Total - this.Acumulado);
            }
        }

        /// <summary>
        /// Dias trascurridos desde la entrega o recepcion para revision de cobranza o pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public int DiasTranscurridos {
            get {
                if (this.FechaEntrega != null) {
                    if (this.Status == "PorCobrar" | this.Status == "PorPagar") {
                        return DateTime.Now.Subtract(this.FechaEntrega.Value).Days + 1;
                    } else {
                        if (this.FechaUltimoPago != null)
                            return this.FechaUltimoPago.Value.Subtract(this.FechaEntrega.Value).Days + 1;
                    }
                }
                return 0;
            }
        }

        /// <summary>
        /// obtener o establecer objeto del complemento de pagos
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public BindingList<ComplementoPagoDetailModel> RecepcionPago {
            get {
                return _ComplementoRecepcionPago;
            }
            set {
                this._ComplementoRecepcionPago = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer complemento de carta porte
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public CartaPorteDetailModel CartaPorte {
            get { return this._CartaPorte; }
            set {
                this._CartaPorte = value;
                this.OnPropertyChanged();
            }
        }

        [SugarColumn(IsIgnore = true)]
        public object Complemento { get; set; }
        #endregion
    }
}
