﻿using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities {
    [JsonObject]
    public class ComprobanteConceptoImpuestos : BasePropertyChangeImplementation {
        private BindingList<ComprobanteConceptoImpuesto> conceptosImpuestosField;

        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteConceptoImpuestos() {
            this.conceptosImpuestosField = new BindingList<ComprobanteConceptoImpuesto>();
            this.OnPropertyChanged();
        }

        #region propiedades

        [JsonProperty()]
        public BindingList<ComprobanteConceptoImpuesto> ConceptosImpuestos {
            get {
                return this.conceptosImpuestosField;
            }
            set {
                this.conceptosImpuestosField = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region metodos

        public string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static ComprobanteConceptoImpuestos Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComprobanteConceptoImpuestos>(inputJson);
            }
            catch {
                return null;
            }
        }
        #endregion
    }
}