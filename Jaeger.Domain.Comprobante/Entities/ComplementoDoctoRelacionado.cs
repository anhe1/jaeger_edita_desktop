﻿using Newtonsoft.Json;
using Jaeger.Domain.Comprobante.Entities.Complemento;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComplementoDoctoRelacionado : ComplementoDoctoRelacionadoBase {
        private ComprobanteCfdiRelacionadoRelacion tipoRelacionField;

        public ComplementoDoctoRelacionado() {
            this.tipoRelacionField = new ComprobanteCfdiRelacionadoRelacion();
        }

        [JsonProperty("tipoRelacion")]
        public ComprobanteCfdiRelacionadoRelacion TipoRelacion {
            get {
                return this.tipoRelacionField;
            }
            set {
                this.tipoRelacionField = value;
                this.OnPropertyChanged();
            }
        }

        public new string Json(Formatting objFormat = 0) {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public new static ComplementoDoctoRelacionado Json(string inputJson) {
            try {
                return JsonConvert.DeserializeObject<ComplementoDoctoRelacionado>(inputJson);
            } catch {
                return null;
            }
        }
    }
}
