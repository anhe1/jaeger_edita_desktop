﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteValidacionPrinter : ComprobanteValidacionDetailModel {
        public ComprobanteValidacionPrinter() : base() {
        }

        public new static ComprobanteValidacionPrinter Json(string inputJson) {
            return JsonConvert.DeserializeObject<ComprobanteValidacionPrinter>(inputJson);
        }

        public string KeyName() {
            return string.Format("CFDI-{0}-{1}-{2}-{3}_validacion.pdf", this.EmisorRFC, this.ReceptorRFC, this.IdDocumento, this.FechaValidacion.ToString("yyyyMMddHHmmss"));
        }
    }
}
