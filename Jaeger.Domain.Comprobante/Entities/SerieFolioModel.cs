﻿using SqlSugar;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// configuracion de series y folios
    /// </summary>
    [SugarTable("_srs", "configuracion de series y folios")]
    public class SerieFolioModel : BasePropertyChangeImplementation {
        private int index;
        private bool activo;
        private string tipo;
        private string nombre;
        private int consecutivoSerie;
        private int consecutivoFolio;
        private string formato;

        public SerieFolioModel() {
            this.activo = true;
        }

        /// <summary>
        /// Desc:ID de serie
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_id", ColumnDescription = "indice de la serie", IsPrimaryKey = true, IsIdentity = true, Length = 11)]
        public int Id {
            get {
                return this.index;
            }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:IsActive
        /// Default:1
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_a", ColumnDescription = "registro activo", Length = 1, DefaultValue = "1")]
        public bool Activo {
            get {
                return this.activo;
            }
            set {
                this.activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Tipo de documento
        /// Default:
        /// Nullable:False
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_tipo", ColumnDescription = "tipo de documento", Length = 50, IsNullable = false)]
        public string Tipo {
            get {
                return this.tipo;
            }
            set {
                this.tipo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Nombre
        /// Default:
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_nm", ColumnDescription = "nombre de la serie", Length = 25, IsNullable = true)]
        public string Nombre {
            get {
                return this.nombre;
            }
            set {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Consecutivo Serie
        /// Default:0
        /// Nullable:True
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_conss", ColumnDescription = "consecutivo serie", Length = 11, IsNullable = true)]
        public int ConsecutivoSerie {
            get {
                return this.consecutivoSerie;
            }
            set {
                this.consecutivoSerie = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Consecutivo Folio
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_consf", ColumnDescription = "consecutivo folio", Length = 11, IsNullable = true)]
        public int ConsecutivoFolio {
            get {
                return this.consecutivoFolio;
            }
            set {
                this.consecutivoFolio = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Desc:Formato
        /// </summary>           
        [SugarColumn(ColumnName = "_srs_format", ColumnDescription = "formato", ColumnDataType = "TEXT", IsNullable = true)]
        public string Formato {
            get {
                return this.formato;
            }
            set {
                this.formato = value;
                this.OnPropertyChanged();
            }
        }

    }
}
