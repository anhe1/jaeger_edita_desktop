using System;
using Jaeger.Domain.Comprobante.ValueObjects;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteConceptoImpuesto : Abstractions.TipoImpuestoModel {
        /// <summary>
        /// constructor
        /// </summary>
        public ComprobanteConceptoImpuesto() {
            this.Tipo = TipoImpuestoEnum.Traslado;
            this.Impuesto = ImpuestoEnum.IVA;
            this.TasaOCuota = new decimal(0.16);
        }

        /// <summary>
        /// Tipo Traslado, Impuesto IVA, Tasa al 16%
        /// </summary>
        /// <param name="baseT">base</param>
        public ComprobanteConceptoImpuesto(decimal baseT) {
            this.Tipo = TipoImpuestoEnum.Traslado;
            this.Impuesto = ImpuestoEnum.IVA;
            this.TasaOCuota = new decimal(0.16);
            this.Base = baseT;
        }
    }
}