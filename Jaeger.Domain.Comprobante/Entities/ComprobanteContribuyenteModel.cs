﻿using System.Collections.Generic;
using SqlSugar;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Domain.Comprobante.Entities {
    [SugarTable("_drctr")]
    public class ComprobanteContribuyenteModel : ContribuyenteModel, IComprobanteContribuyente, IContribuyenteModel {
        public ComprobanteContribuyenteModel() : base() {
            this.Relaciones = new List<IRelacionComercialModel>();
        }

        [SugarColumn(IsIgnore = true)]
        public List<IRelacionComercialModel> Relaciones { get; set; }
    }
}
