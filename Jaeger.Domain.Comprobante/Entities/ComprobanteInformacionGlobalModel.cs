﻿using SqlSugar;
using Jaeger.Domain.Services.Mapping;
using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    /// <summary>
    /// Nodo condicional para precisar la informacion relacionada con el comprobante global.
    /// </summary>
    [SugarTable("_cfdigbl", "CFDI: informacion relacionada con el comprobante global")]
    public class ComprobanteInformacionGlobalModel : Domain.Base.Abstractions.BasePropertyChangeImplementation {
        #region declaraciones
        private int index;
        private bool _activo;
        private string periodicidadField;
        private string mesesField;
        private int añoField;
        #endregion

        public ComprobanteInformacionGlobalModel() {
            this._activo = true;
        }

        [JsonIgnore]
        [DataNames("_cfdigbl_id")]
        [SugarColumn(ColumnName = "_cfdigbl_id", ColumnDescription = "indice de la tabla", IsIdentity = true, IsPrimaryKey = true)]
        public int IdGlobal {
            get { return this.index; }
            set {
                this.index = value;
                this.OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [DataNames("_cfdigbl_a")]
        [SugarColumn(ColumnName = "_cfdigbl_a", ColumnDescription = "registro activo", DefaultValue = "1")]
        public bool Activo {
            get { return this._activo; }
            set {
                this._activo = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el periodo al que corresponde la informacion del comprobante global
        /// </summary>
        [JsonProperty("periodicidad")]
        [DataNames("_cfdigbl_perido")]
        [SugarColumn(ColumnName = "_cfdigbl_periodo", ColumnDescription = "periodo al que corresponde la informacion del comprobante global", Length = 2)]
        public string Periodicidad {
            get {
                return this.periodicidadField;
            }
            set {
                this.periodicidadField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el mes o los meses al que corresponde la informacion del comprobante global.
        /// </summary>
        [JsonProperty("meses")]
        [DataNames("_cfdigbl_mes")]
        [SugarColumn(ColumnName = "_cfdigbl_mes", ColumnDescription = "mes o los meses al que corresponde la informacion del comprobante global.", Length = 2)]
        public string ClaveMeses {
            get {
                return this.mesesField;
            }
            set {
                this.mesesField = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// obtener o establecer el año al que corresponde la informacion del comprobante global.
        /// </summary>
        [JsonProperty("anio")]
        [DataNames("_cfdigbl_anio")]
        [SugarColumn(ColumnName = "_cfdigbl_anio", ColumnDescription = "año al que corresponde la informacion del comprobante global", Length = 4)]
        public int Anio {
            get {
                return this.añoField;
            }
            set {
                this.añoField = value;
                this.OnPropertyChanged();
            }
        }
    }
}
