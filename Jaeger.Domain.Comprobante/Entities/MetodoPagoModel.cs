﻿namespace Jaeger.Domain.Comprobante.Entities {
    public class MetodoPagoModel : Base.Abstractions.BaseSingleTipoModel {
        public MetodoPagoModel() { }

        public MetodoPagoModel(string id, string descripcion) : base(id, descripcion) { }
    }
}
