﻿namespace Jaeger.Domain.Comprobante.Entities {
    public class ExportacionModel : Base.Abstractions.BaseSingleTipoModel {
        public ExportacionModel() { }

        public ExportacionModel(string id, string descripcion) : base(id, descripcion) { }
    }
}
