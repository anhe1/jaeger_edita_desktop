﻿using Newtonsoft.Json;

namespace Jaeger.Domain.Comprobante.Entities {
    public class General {
        public string Clave { get; set; }

        [JsonProperty("razonSocial")]
        public string RazonSocial { get; set; }

        [JsonProperty("rfc")]
        public string RFC { get; set; }

        [JsonProperty("regimenFiscal")]
        public string RegimenFiscal { get; set; }

        [JsonProperty("registroPatronal")]
        public string RegistroPatronal { get; set; }
    }
}
