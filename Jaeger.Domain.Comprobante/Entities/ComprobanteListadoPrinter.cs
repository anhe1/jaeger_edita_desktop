﻿using System.Collections.Generic;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteListadoPrinter {

        public ComprobanteListadoPrinter(List<ComprobanteFiscalDetailSingleModel> conceptos, bool vertical = false) {
            this.Vertical = vertical;
            this.Conceptos = conceptos;
        }

        public bool Vertical { get; set; }

        public List<ComprobanteFiscalDetailSingleModel> Conceptos { get; set; }
    }
}
