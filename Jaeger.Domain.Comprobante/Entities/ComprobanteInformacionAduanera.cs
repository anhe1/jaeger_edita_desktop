﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Domain.Comprobante.Entities {
    public class ComprobanteInformacionAduanera : BasePropertyChangeImplementation, IDataErrorInfo {
        private string numeroPedimentoField;

        /// <summary>
        /// Atributo requerido para expresar el número del pedimento que ampara la importación del bien que se expresa en el siguiente formato: últimos 2 dígitos del año de validación seguidos por dos espacios, 2 dígitos de la aduana de despacho seguidos por dos espacios, 4 dígitos del número de la patente seguidos por dos espacios, 1 dígito que corresponde al último dígito del año en curso, salvo que se trate de un pedimento consolidado iniciado en el año inmediato anterior o del pedimento original de una rectificación, seguido de 6 dígitos de la numeración progresiva por aduana.
        /// </summary>
        public string NumeroPedimento {
            get {
                return this.numeroPedimentoField;
            }
            set {
                this.numeroPedimentoField = value;
                this.OnPropertyChanged();
            }
        }

        public string this[string columnName] {
            get {
                if (columnName == "NumeroPedimento" && !this.RegexValido(this.numeroPedimentoField, "[0-9]{2}  [0-9]{2}  [0-9]{4}  [0-9]{7}")) {
                    return "Formato de Número de Pedimento no válido";
                }
                return string.Empty;
            }
        }

        public string Error {
            get {
                if (!(this.RegexValido(this.numeroPedimentoField, "[0-9]{2}  [0-9]{2}  [0-9]{4}  [0-9]{7}"))) {
                    return "Por favor ingrese datos válidos en esta fila!";
                }
                return string.Empty;
            }
        }

        private bool RegexValido(string valor, string patron) {
            if (!(valor == null)) {
                bool flag = Regex.IsMatch(valor, string.Concat("^", patron, "$"));
                return flag;
            }
            return false;
        }
    }
}
